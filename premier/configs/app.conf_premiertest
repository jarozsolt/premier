<?php
/**
 *---------------------------------------------------------------
 * app.conf
 *
 * Configuration file for the framework
 *
 * @package Kron
 * @subpackage configurations
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *---------------------------------------------------------------
 */


/**
 * FrameworkConfig class
 *
 * Configuration class for the framework
 *
 * @package Kron
 * @subpackage configuration classes
 * @todo Revise comments
 * @todo The init_language function not works
 */
class FrameworkConfig
{
/*
|===============================================================
| START OF USER CONFIGURABLE SETTINGS
|===============================================================
*/

	/**
	 * @var string configuration setting study name and folder
	 */
	Const study_folder   		= 'premiertest';
	
	/**
	 * @var string configuration setting for the framework version
	 * This version number also included to the name of jsapp_folder!
	 */
	Const version_number		= '1.2.0';
	
	/**
	 * @var bool configuration setting for the debug mode
	 * Change this value to 'false' when the site goes live.
	 */
//	Const debug_mode			= true;
	Const debug_mode			= false;
	
	/**
	 * @var bool configuration setting for the demo mode
	 * Change this value to 'false' for the 'production' site.
	 */
	Const demo_mode			= true;
	//Const demo_mode			= false;
	
	/**
	 * @var bool configuration setting for the server
	 * maintenance mode. Change this value to 'true'
	 * to display the 'Server under maintenance' screen.
	 */
	Const maintenance_mode		= false;
	
	
	/**
	 * @var string configuration setting for default language
	 * The possible values are: hu, en, de, fr
	 */
	Const default_language 			= 'hu_HU';
	//Const default_language 		= 'en_US';
	
	/**
	 * @var string configuration setting for the public folder
	 */
	Const public_folder   		= 'public_html';
	
	/**
	 * @var string configuration setting for the subdomain folder
	 */
	Const subdomain_folder   		= 'study';

	/**
	 * @var string configuration setting for the include path
	 * separator (; or :)
	 */
	Const include_path_separator = ':';
	
	/**
	 * @var string configuration setting for SAE email recipients
	 * 
	 */
	Const sae_email_recipients = 'judit.garami@aurinsoft.hu, csl@aurinsoft.hu, zsolt.jaro@gmail.com';
	//Const sae_email_recipients = 'judit.garami@aurinsoft.hu, csl@aurinsoft.hu';
	
	
/**************************************************************/
		
	/**
	 * @var string constants for the userclass values
	 */
	Const USERGROUP_ADMIN = 'admin';
	Const USERGROUP_SYSTEM = 'system'; 
	Const USERGROUP_MONITOR = 'monitor';
	Const USERGROUP_INVESTIGATOR = 'investigator';
	Const USERGROUP_PRINCIPALINVESTIGATOR = 'principalinv';
	Const USERGROUP_SPONSOR = 'sponsor';
	Const USERGROUP_NURSE = 'nurse';
	Const USERGROUP_PHARMACOVIGILANCE = 'pharmacov';
	
	/**
	 * @var string constants for the field status values
	 */
	Const FIELDSTATUS_OPEN = 'O';
	Const FIELDSTATUS_QUERIED = 'Q'; // törölni?
	Const FIELDSTATUS_ANSWERED = 'A'; // törölni?
	Const FIELDSTATUS_CHANGED = 'C'; // törölni?
	Const FIELDSTATUS_MONITOR_APPROVED = 'M';
	Const FIELDSTATUS_REOPENED = 'R'; // törölni?
	
	/**
	 * @var string constants for the field action values
	 */
	Const FIELDACTION_SETVALUE = 'V';
	Const FIELDACTION_DELETEVALUE = 'D';	
	Const FIELDACTION_MONITOR_APPROVE = 'M';
	Const FIELDACTION_DELETEQUERY = 'K';
	Const FIELDACTION_DELETECOMMENT = 'L';
	Const FIELDACTION_OPENQUERY = 'Q';
	Const FIELDACTION_COMMENT = 'C';
	Const FIELDACTION_CLOSEQUERY = 'F';
	Const FIELDACTION_REOPEN = 'R';
	Const FIELDACTION_ANSWER_QUERY = 'A';
	Const FIELDACTION_REOPEN_QUERY = 'B';
	
	/**
	 * @var string constants for Query Types
	 */
	Const QUERYTYPE_QUERY = 'Q';
	Const QUERYTYPE_COMMENT = 'C';
	
	/**
	 * @var string constants forstatus
	 */
	Const QUERYSTATUS_OPEN = 'O';
	Const QUERYSTATUS_CLOSED = 'C';
	Const QUERYSTATUS_ANSWERED = 'A';
	Const QUERYSTATUS_REOPENED = 'R';
	
	/**
	 * @var string constant array of country ids used for patientid
	 * You can add more items easily....
	 */
	
	private static $countryCodes = array("","HU","A ");
    public static function COUNTRY_CODES($key){
        return self::$countryCodes[$key];
    }
	
	/**
	 * @var string constant array of visit names
	 */
	
	private static $visitNames = array( 'Visit1', 'Visit2', 'Visit3', 'Visit4');
    public static function VISIT_NAMES($key){
        return self::$visitNames[$key];
    }

	/**
	 * @var int constant - number of planned visits for a patients
	 */
	const NUMBER_OF_VISITS = 4; //count( self::$visitNames);
	
	/**
	 * @var string constant for visit status
	 */
	const VISITSTATUS_NEW = "N";
	const VISITSTATUS_EMPTY = "E";
	const VISITSTATUS_STORED = "S";
	const VISITSTATUS_APPROVED = "A";
	const VISITSTATUS_MISSING = "M";
	const VISITSTATUS_COMPLETED = "C";
	const VISITSTATUS_CANCELLED = "K";

	
	/**
	 * @var integer constant array of visit date offsets to the enrollment date
	 * 1 day = 24*60*60 = 86400
	 * 30 days  = 2592000, 
	 * nr of items in the array must be equal to NUMBER_OF_VISITS
	 */
	
	private static $visitDateOffsets = array( 0, 2592000, 15552000,  15552000);
    public static function VISITDATE_OFFSET($key){
        return self::$visitDateOffsets[$key];
    }

	
	/**
	 * @var string constant for patient status
	 */
	const PATIENTSTATUS_ONGOING = "O"; 
	const PATIENTSTATUS_COMPLETED = "C";
	const PATIENTSTATUS_FAILURE = "F";
	const PATIENTSTATUS_DELETED = "D";
	const PATIENTSTATUS_DROPOUT = "D";

	
/*
|===============================================================
| END OF USER CONFIGURABLE SETTINGS
|===============================================================
*/

	/*
	 *---------------------------------------------------------------
	 * The folder structure:
	 *	-- system folder
	 *   |-- study folder
     *     |-- configs folder for the configuration files
	 *       |-- app.conf configuration file
     *     |-- documents_folder
	 *     |-- log folder
	 *     |-- printouts folder
	 *	 |-- public_folder
	 *     |-- study folder
	 *       |-- js folder for the javascript files
	 *         |-- application folder for the application specific js files
	 *         |-- ext folder containing the ExtJS
	 *	     |-- phpapp folder
     *	     |-- resources folder
	 *         |-- css folder
	 *         |-- images folder
	 *         |-- locales folder
	 *       |-- index.php
	 *---------------------------------------------------------------
	 */
	
	/**
	 * @var string configuration setting for the php application folder
	 */
	Const phpapp_folder   		= 'php';
	
	/**
	 * @var string configuration setting for the config folder
	 */
	Const config_folder   		= 'configs';
	
	/**
	 * @var string configuration setting for the documents folder
	 */
	Const documents_folder   	= 'documents';
	
	/**
	 * @var string configuration setting for the temporary printout folder
	 */
	Const printouts_folder   	= 'printouts';
	
	/**
	 * @var string configuration setting for the resources folder
	 */
	Const resources_folder   		= 'resources';
	
	/**
	 * @var string configuration setting for the locales folder
	 */
	Const locales_folder   		= 'locales';
	
	/**
	 * @var string configuration setting for the css folder
	 */
	Const css_folder   			= 'css';
	
	/**
	 * @var string configuration setting for the img folder
	 */
	Const image_folder   		= 'images';
	
	/**
	 * @var string configuration setting for the extjs folder
	 *
	 */
	// Const extjs_folder   		= 'ext-4.0.7-gpl';
	Const extjs_folder   		= 'ext';
	
	
	/**
	 * @var string configuration setting for the javascript folder
	 */
	Const javascript_folder   	= 'js';
	
	/**
	 * @var string configuration setting for the javascript app folder
	 */
	Const jsapp_folder   		= 'app';
	
	/**
	 * @var string configuration setting for the javascript core folder
	 */
	Const jscore_folder   		= 'core';
	
	
	 // Helper function for calculating the relative paths
	static public function relativePath($from, $to, $ps = '/')
	{
	  $arFrom = explode($ps, rtrim($from, $ps));
	  $arTo = explode($ps, rtrim($to, $ps));
	  while(count($arFrom) && count($arTo) && ($arFrom[0] == $arTo[0]))
	  {
	    array_shift($arFrom);
	    array_shift($arTo);
	  }
	  $relative = str_pad("", count($arFrom) * 3, '..'.$ps).implode($ps, $arTo);
	  if ($relative == "")
	  {
	  	$relative = ".";
	  }
	  $relative = rtrim($relative,"/");
	  return $relative;
	}
}

/**
 * LoggerConfig class
 *
 * Configuration class for the logging
 *
 * @package Kron
 * @subpackage configuration classes
 * @todo Revise comments
 */
class LoggerConfig
{
/*
|===============================================================
| START OF USER CONFIGURABLE SETTINGS
|===============================================================
*/
	
	/**
	 * @var bool configuration setting for using FirePHP as output
	 * it is only available in debug mode (see FrameworkConfig::debug_mode)
	 */
	Const use_firephp = true;
	//Const use_firephp = false;
	
	/**
	 * @var bool write debug trace into the log
	 * it is only available in debug mode
	 */
	Const write_trace = true;
	
	/**
	 * @var string configuration setting for the log file directory
	 */
	Const log_dir = 'log';
	
	
	/**
	 * @var string configuration setting for the log file prefix
	 */
	Const log_prefix = 'premierlog_';
	
/*
|===============================================================
| END OF USER CONFIGURABLE SETTINGS
|===============================================================
*/
}

/**
 * DatabaseConfig class
 *
 * Configuration class for the database
 *
 * @package Kron
 * @subpackage configuration classes
 * @todo Revise comments
 */
class DatabaseConfig
{
/*
|===============================================================
| START OF USER CONFIGURABLE SETTINGS
|===============================================================
*/
	
	/**
	 * @var string configuration setting for database host name
	 */
	Const db_host     		= 'localhost';
	
	/**
	 * @var string configuration setting for database username
	 */
	Const db_username 		= 'krondbadmin';
	
	/**
	 * @var string configuration setting for database password
	 */
	Const db_password 		= 'zRmGgz5g(9sX';
	
	/**
	 * @var string configuration setting for database name
	 */
	Const db_name     		= 'premiertest';
	
	/**
	 * @var string configuration setting for database type
	 */
	Const db_dbms	  		= 'mysql';
	
	/**
	 * @var string configuration setting for table prefix
	 */
	Const db_tableprefix 	= '';
	
/*
|===============================================================
| END OF USER CONFIGURABLE SETTINGS
|===============================================================
*/
}

/**
 * AuthenticationConfig class
 *
 * Configuration class for the user authentication
 *
 * @package Kron
 * @subpackage configuration classes
 * @todo Revise comments
 */
class AuthenticationConfig
{
/*
|===============================================================
| START OF USER CONFIGURABLE SETTINGS
|===============================================================
*/
	
	/**
	 * @var int configuration setting for the minimal length of
	 * the username
	 */
	Const min_username_size = 8;
	
	/**
	 * @var int configuration setting for the minimal length of
	 * the password
	 */
	Const min_password_size = 8;
	
/*
|===============================================================
| END OF USER CONFIGURABLE SETTINGS
|===============================================================
*/
}

if (FrameworkConfig::debug_mode)
{// In debug mode report all errors
	error_reporting(E_ALL | E_STRICT);
	ini_set("display_errors", 1);
}
else
{// We supress the errors in the production version
	error_reporting(NULL);
}


// Add phpapp_folder to PHP's include path
set_include_path(get_include_path().FrameworkConfig::include_path_separator.$base_path.'/'.FrameworkConfig::public_folder.'/'.FrameworkConfig::subdomain_folder.'/'.FrameworkConfig::study_folder.'/'.FrameworkConfig::phpapp_folder);

// Add config_folder to PHP's include path
set_include_path(get_include_path() . FrameworkConfig::include_path_separator . $base_path . '/' . FrameworkConfig::study_folder .'/' . FrameworkConfig::config_folder);

// Calculating paths relative to the caller php
$caller_path_relative = str_replace("\\", "/", dirname($_SERVER['PHP_SELF']));
$document_root = rtrim(str_replace("\\", "/", $_SERVER['DOCUMENT_ROOT']), '/');
$caller_path = $document_root.$caller_path_relative;
$config_path = str_replace("\\", "/", realpath(dirname(__FILE__)));
$base_study_path = join(array_slice(explode( "/" ,$config_path), 0, -1), "/");
$base_path = join(array_slice(explode( "/" ,$base_study_path), 0, -1), "/");
$base_folder = basename ($base_path ,"/");
$base_folders_full = '/'.FrameworkConfig::relativePath($_SERVER['DOCUMENT_ROOT'],$base_path);
$documents_path = $base_study_path.'/'.FrameworkConfig::documents_folder;
$printouts_path = $base_study_path.'/'.FrameworkConfig::printouts_folder;
$public_path = $base_path.'/'.FrameworkConfig::public_folder;
$subdomain_path = $public_path.'/'.FrameworkConfig::subdomain_folder;
$public_study_path = $subdomain_path.'/'.FrameworkConfig::study_folder;
$resources_path = $public_study_path.'/'.FrameworkConfig::resources_folder;
$phpapp_path = $public_study_path.'/'.FrameworkConfig::phpapp_folder;
$locales_path = $resources_path.'/'.FrameworkConfig::locales_folder;
$css_path = $resources_path.'/'.FrameworkConfig::css_folder;
$image_path = $resources_path.'/'.FrameworkConfig::image_folder;
$javascript_path = $public_study_path.'/'.FrameworkConfig::javascript_folder;
$extjs_path = $javascript_path.'/'.FrameworkConfig::extjs_folder;
$jsapp_path = $javascript_path.'/'.FrameworkConfig::jsapp_folder;
$jscore_path = $jsapp_path.'/'.FrameworkConfig::jscore_folder;
$base_path_relative = FrameworkConfig::relativePath($caller_path, $base_path);
$base_study_path_relative = FrameworkConfig::relativePath($caller_path, $base_study_path);
$config_path_relative = FrameworkConfig::relativePath($caller_path, $config_path);
$documents_path_relative = FrameworkConfig::relativePath($caller_path, $documents_path);
$printouts_path_relative = FrameworkConfig::relativePath($caller_path, $printouts_path);
$public_path_relative = FrameworkConfig::relativePath($caller_path, $public_path);
$public_study_path_relative = FrameworkConfig::relativePath($caller_path, $public_study_path);
$locales_path_relative = FrameworkConfig::relativePath($caller_path, $locales_path);
$phpapp_path_relative = FrameworkConfig::relativePath($caller_path, $phpapp_path);
$css_path_relative = FrameworkConfig::relativePath($caller_path, $css_path);
$image_path_relative = FrameworkConfig::relativePath($caller_path, $image_path);
$javascript_path_relative = FrameworkConfig::relativePath($caller_path, $javascript_path);
$extjs_path_relative = FrameworkConfig::relativePath($caller_path, $extjs_path);
$jsapp_path_relative = FrameworkConfig::relativePath($caller_path, $jsapp_path);
$jscore_path_relative = FrameworkConfig::relativePath($caller_path, $jscore_path);
/*
|---------------------------------------------------------------
| DEFINE PATH CONSTANTS
|---------------------------------------------------------------
|
| BASE_FOLDER			- The name of the "system" folder
| BASE_PATH				- The full server path to the "system" folder
| BASE_PATH_REL			- The relative path to the "system" folder
| BASE_STUDY_PATH		- The full server path to the private "study" folder (in the "system" folder)
| BASE_STUDY_PATH_REL	- The relative path to the private "study" folder (in the "system" folder)
| CONFIG_PATH			- The full server path to the "config" folder
| PUBLIC_PATH			- The full server path to the "public" folder
| PUBLIC_STUDY_PATH		- The full server path to the public "study" folder (in the "public" folder)
| PHPAPP_PATH			- The full server path to the "phpapp" folder
| CSS_PATH				- The full server path to the "css" folder
| IMAGE_PATH			- The full server path to the "image" folder
| JAVASRIPT_PATH		- The full server path to the "javascript" folder
| EXTJS_PATH			- The full server path to the "extjs" folder
| JSAPP_PATH			- The full server path to the "jsapp" folder
|
*/
define('BASE_FOLDER', $base_folder);
define('BASE_PATH', $base_path . '/');
define('BASE_PATH_REL', $base_path_relative . '/');
define('CURRENT_PATH', $caller_path);
define('BASE_FOLDER_REL', $base_folders_full . '/');
define('BASE_STUDY_PATH', $base_study_path . '/');
define('BASE_STUDY_PATH_REL', $base_study_path_relative . '/');
define('CONFIG_PATH', $config_path . '/');
define('CONFIG_PATH_REL', $config_path_relative . '/');
define('DOCUMENTS_PATH', $documents_path . '/');
define('DOCUMENTS_PATH_REL', $documents_path_relative . '/');
define('PRINTOUTS_PATH', $printouts_path . '/');
define('PRINTOUTS_PATH_REL', $printouts_path_relative . '/');
define('PUBLIC_PATH', $public_path . '/');
define('PUBLIC_PATH_REL', $public_path_relative . '/');
define('LOCALES_PATH', $locales_path . '/');
define('LOCALES_PATH_REL', $locales_path_relative . '/');
define('PHPAPP_PATH', $phpapp_path . '/');
define('PHPAPP_PATH_REL', $phpapp_path_relative . '/');
define('CSS_PATH', $css_path . '/');
define('CSS_PATH_REL', $css_path_relative . '/');
define('IMAGE_PATH', $image_path . '/');
define('IMAGE_PATH_REL', $image_path_relative . '/');
define('JAVASRIPT_PATH', $javascript_path . '/');
define('JAVASCRIPT_PATH_REL', $javascript_path_relative . '/');
define('EXTJS_PATH', $extjs_path . '/');
define('EXTJS_PATH_REL', $extjs_path_relative . '/');
define('JSAPP_PATH', $jsapp_path . '/');
define('JSAPP_PATH_REL', $jsapp_path_relative . '/');
define('JSCORE_PATH', $jscore_path . '/');
define('JSCORE_PATH_REL', $jscore_path_relative . '/');
?>