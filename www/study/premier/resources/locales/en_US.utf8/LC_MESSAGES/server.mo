��          �            x  h   y  %   �  
          $     )   D     n  (   v     �     �  D   �       -   #     Q     a  /  x  @   �     �  	          &        ?     Z      c     �     �  O   �     �          &  	   4               
                      	                                          A felhasználói fiókja letiltásra került, kérjük lépjen kapcsolatba a rendszeradminisztrátorral! A régi jelszavát hibásan adta meg! Befejezett Folyamatban KRON - Klinikai Adatkezelő Rendszer Kattintson ide az újra bejelentkezéshez Kiesett Nem sikerült kapcsolódni a szerverhez! Sikeres bejelentkezés! Sikeres kijelentkezés Sikeresen kijelentkezett a KRON - Klinikai Adatkezelő Rendszerből. Téves szűrés Érvénytelen felhasználónév vagy jelszó! Összes ország Összes vizsgálóhely Project-Id-Version: KRON 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-03 12:34+0100
PO-Revision-Date: 2013-07-03 12:38+0100
Last-Translator: Csizmadia László <laszlo.csizmadia@datanova.hu>
Language-Team: Aurinsoft <csl@aurinsoft.hu>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: C:\Dev\Web\Wamp\www\kron\www\study\keplat
X-Poedit-Language: English
X-Poedit-Country: UNITED STATES
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: ./php
 Your account disabled, please contact your system administrator! The old password is invalid! Completed Ongoing KRON - Clinical Data Management System Click here to log in again Drop-out Unable to connect to the server! Login successful! Logout Successful  You have been successfully logged out of KRON - Clinical Data Management System Screen failure Invalid username or password! All countries All sites 