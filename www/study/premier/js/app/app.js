
//@requires Ext.direct.Manager,Ext.direct.RemotingProvider, Ext.tip.QuickTip, Core.login.LoginWindow

// Ext.direct.*,Ext.data.proxy.Direct,layout.border,series.column,axis.numeric,axis.category,Ext.ux.statusbar.StatusBar,Kron.store.*,Kron.controller.*,Kron.view.Viewport,Kron.view.userprefs.ChangePasswordWindow,Core.login.LoginWindow,Kron.core.UserManagement,Ext.tip.QuickTip
 
/*javascript validation settings, see: http://www.jslint.com/lint.html*/
/*jslint nomen: true*/
/*global console: false, Kron: false, Ext: false*/

/**
 *---------------------------------------------------------------
 * app.js
 *
 * The application
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

        // Start the mask on the body and get a reference to the mask
        var sixtyDays, stateProvider, userDetails,
        splashscreen = Ext.getBody().mask(_("Alkalmazás betöltése..."), 'splashscreen');

        // Add a new class to this mask as we want it to look different from the default.
        splashscreen.addCls('splashscreen');
    
        // Insert a new div before the loading icon where we can place our logo.
        Ext.DomHelper.insertFirst(Ext.query('.x-mask-msg')[0], {
            cls: 'x-splash-icon'
        });
//<debug>   
        // Create namespace (Kron)
   //     Ext.namespace('Kron');
        
        // Set the real application path
        //Ext.Loader.setPath('Kron', Ext.Loader.getPath('kron') + '/app');

        //Specify a list of classes your application needs
    //    Ext.require(['Ext.direct.Manager', 'Ext.direct.RemotingProvider', 'Core.login.LoginWindow', 'Kron.core.UserManagement']);
        
        //Specify a list of classes your application needs
    //    Ext.require(['Kron.view.Viewport','Kron.view.userprefs.ChangePasswordWindow']);
//</debug>
        // create state provider to persist grid states in cookies
        sixtyDays = new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 60));
        stateProvider = Ext.create('Ext.state.CookieProvider', {
            expires : sixtyDays
        });
        Ext.state.Manager.setProvider(stateProvider);
    
//---        Ext.create('Ext.app.Application', {
        Ext.application({
               name : 'Kron',
               requires: ['Kron.model.Form','Ext.direct.*', 'Ext.data.proxy.Direct', 'layout.border','series.column', 'axis.numeric','axis.category', 'Ext.ux.statusbar.StatusBar','Kron.model.*','Kron.store.*', 'Kron.controller.*', 'Kron.view.Viewport', 'Kron.view.userprefs.ChangePasswordWindow', 'Core.login.LoginWindow', 'Kron.core.UserManagement'],
    
            //autoCreateViewport: true,
            //<debug>
            // a fenti azért kell mert a buildelt esetben ez nemkell
            appFolder : Ext.Loader.getPath('Kron'),
            //</debug>
            models : ['Addressee', 'Contact', 'Message', 'Patient', 'Query', 'QuerySummary', 'SentMessage', 'Document', 'Calendardate', 'Calendarvisit', 'Form', 'Field', 'FieldHistory', 'FieldQuery', 'Report'],
            stores : ['Addressee', 'Contacts', 'Messages', 'OpenQueries', 'PatientQueries', 'Patients', 'ReceivedMessages', 'SentMessages', 'Documents', 'Calendardates', 'Forms', 'FieldHistories', 'FormFields', 'FieldQueries', 'Reports'],
            controllers : ['AdverseEvents', 'Contacts', 'Documents', 'MedicalHistory', 'MainMenu', 'Medications', 'Messages', 'Patient', 'Patients', 'PatientDialog', 'Queries', 'Todos', 'Visits', 'ToSignature', 'UserPrefs', 'FormsOverview'],
    
            userDetails : null,
    
            launch : function() {
                var loginWindow, statusIconCls, pwdWindow, itemParams,
                // Setup a task to fadeOut the splashscreen
                task = Ext.create('Ext.util.DelayedTask', function() {
                    // Fade out the body mask
                    splashscreen.fadeOut({
                        duration: 1000,
                        remove:true
                    });
                    // Fade out the icon and message
                    splashscreen.next().fadeOut({
                        duration: 1000,
                        remove:true,
                         callback: function(){
                        	Kron.startingMaskRemoved = true;
                        }
                       /*listeners: {
                            scope: this,
                            aftereanimate: function() {
                                // Set the body as unmasked after the animation
                                if (Ext.getBody().isMasked()) {
                                    Ext.getBody().unmask();
                                }
                            }
                        }*/
                    });
                });
                // Run the fade 500 milliseconds after launch.
                task.delay(500);
    /*
                Kron.config = {
                    labelMandatoryMark : ' <span class="req" style="color:red">*</span>'
                };
    */
                //Initialize the ExtDirect API
                Ext.direct.Manager.addProvider(Kron.APIDesc);
                // Create the Login window
                loginWindow = Ext.create('Core.login.LoginWindow', {
                    id : 'wndLogin'
                });
                // Catch the event of the user management, which requests for a login
                Ext.Direct.on('ext-direct-login', function(e) {
                    loginWindow.body.unmask();
                    if (e.type === 'event' && e.status === true && e.result.data !== undefined) {
                        statusIconCls = 'x-status-error';
                        if (e.result.data.loggedin === true) {
                            statusIconCls = 'x-status-valid';
                            loginWindow.setStatus({
                                text : e.data.message,
                                iconCls : statusIconCls
                            });
                            window.location.replace('index.php');
                        } else {
                            loginWindow.setStatus({
                                text : e.data.message,
                                iconCls : statusIconCls
                            });
                        }
    
                    } else {
                        loginWindow.alertConnectionError();
                    }
                });
                Kron.UserManagement.getUserDetails(function(provider, response) {
                    if (response === undefined || response.result === undefined || response.status === false || response.result.loggedin === false) {// no response from the server
                        if (response.result.name !== undefined) {
                            loginWindow.setUserName(response.result.name);
                        }
                        loginWindow.show();
                    } else {// Create the app's viewport
                        var userDetails, infoBar, userPrefs, sid, pid, ftype, fid, record, patientView;
    
                        userDetails = response.result;
                        Kron.core.UserManagement.setPermissions(userDetails.permissions);
                        
                        Ext.create('Kron.view.Viewport');
                        //var viewport = Ext.getCmp('viewport');
                        //var loadMask = new Ext.LoadMask(viewport, {msg:_("Betöltés...")});
                        //loadMask.show();
    
                        infoBar = Ext.getCmp('infobar');
                        infoBar.addEvents('infoBarChanged');
                        infoBar.updateDetail(userDetails);
    
                        userPrefs = Ext.getCmp('userprefs');
                        userPrefs.updateDetail(userDetails);
                        
                        // A felhasználónak fell kell hozni egy formot? pl. pharmacov sae-t néz
                        if (userDetails.showForm !== undefined && userDetails.showForm !== null && userDetails.showForm.pid !== undefined && 
                            userDetails.showForm.sid !== undefined && userDetails.showForm.ftype !== undefined && userDetails.showForm.fid !== undefined &&
                            Kron.core.UserManagement.hasPermission("Forms", "getForms")) {
                            
                            record = Ext.create('Kron.model.Form');
                            patientView = Ext.getCmp('patientview');
                            record.set('patientid', userDetails.showForm.pid);
                            record.set('siteid', userDetails.showForm.sid);
                            
                            patientView.setPatient(record, userDetails.showForm.ftype, userDetails.showForm.fid);
                            //loadMask.destroy();
                        }
                        // A felhasználónak meg kell változtatnia a jelszavát?
                        else if (userDetails.passwordchangerequired === true)
                        {
                            //loadMask.destroy();
                            // Van hozzá jogosultsága?
                            if (Kron.core.UserManagement.hasPermission("UserManagement", "changePassword")) {
                                // Létrehozunk egy ChangePasswordWindow ablakot
                                pwdWindow = Ext.create('Kron.view.userprefs.ChangePasswordWindow', {
                                    hintText : _("Kérjük változtassa meg ideiglenes jelszavát!")+'<br>'+_("A megváltoztatáshoz adja meg az új és régi jelszavát is.")
                                });
                                pwdWindow.show();
                            }
                        }
                        //loadMask.destroy();
                    }
                });
            }
        });
//---    });
//---}());
