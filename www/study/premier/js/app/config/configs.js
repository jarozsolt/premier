
/*javascript validation settings, see: http://www.jslint.com/lint.html*/
/*jslint nomen: true*/
/*global console: false, Kron: false, Ext: false*/

/**
 *---------------------------------------------------------------
 * configs.js
 *
 * A config beállítások helye van itten
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

var gt = new Gettext({
    'domain' : 'kron'
});

function _(msgid) {"use strict";
    return gt.gettext(msgid);
}
// The local text array
var localeTexts = [];
localeTexts.en_US = {
    "flduser" : "Username",
    "fldpass" : "Password",
    "wndtitle" : "Please Login",
    "btnlogin" : "Login",
    "msgready" : "Ready",
    "msgerror" : "Error",
    "msgconerr" : "Unable to connect to the server!",
    "btnselect" : "Language"
};
localeTexts.hu_HU = {
    "flduser" : "Felhasználónév",
    "fldpass" : "Jelszó",
    "wndtitle" : "Bejelentkezés",
    "btnlogin" : "Bejelentkezés",
    "msgready" : "Kész",
    "msgerror" : "Hiba",
    "msgconerr" : "Nem sikerült kapcsolódni a szerverhez!",
    "btnselect" : "Nyelv"
};
localeTexts.de_DE = {
    "flduser" : "Benutzername",
    "fldpass" : "Passwort",
    "wndtitle" : "Bitte Anmelden",
    "btnlogin" : "Anmelden",
    "msgready" : "Bereit",
    "msgerror" : "Fehler",
    "msgconerr" : "Keine Serververbindung!",
    "btnselect" : "Sprache"
};
localeTexts.fr_FR = {
    "flduser" : "Nom d'utilisateur",
    "fldpass" : "Mot de passe",
    "wndtitle" : "Login",
    "btnlogin" : "Login",
    "msgready" : "Disposé",
    "msgerror" : "Erreur",
    "msgconerr" : "Impossible de se connecter au serveur!",
    "btnselect" : "Langue"
};
mailtoClicked = false;
window_beforeUnload = function(e) {"use strict";
    if (!mailtoClicked) {
        var message = _("Valóban el kívánja hagyni az oldalt? Ebben az esetben a nem mentett adatai elveszhetnek!");
        e = e || e.event;
    
        if (e) {
            e.returnValue = message;
        }
        return message;
    }
    mailtoClicked = false;
};

window_unload = function(e) {"use strict";
    // A browserablak bezárásakor megpróbáljuk kijelentkeztetni a felhasználót
    var params = {
        'normallogout' : false
    };
    Kron.UserManagement.logout(params, function(provider, response) {});
};

// Attaching event listeners for Back button, Refresh, etc.
if (window.attachEvent) {
    window.attachEvent('onbeforeunload', window_beforeUnload);
} else if (window.addEventListener) {
    window.addEventListener('beforeunload', window_beforeUnload, false);
} else {
    throw "Cannot attach event handler";
}

if (window.attachEvent) {
    window.attachEvent('onunload', window_unload);
} else if (window.addEventListener) {
    window.addEventListener('unload', window_unload, false);
} else {
    throw "Cannot attach event handler";
}


Ext.namespace('Kron');   
Kron.config = {
                    labelMandatoryMark : ' <span class="req" style="color:red">*</span>'
                };   
// Betöltéskori               
Kron.startingMaskRemoved = false;
                 