//@define Core.login
//@tag DELETEME

//@requires Ext.direct.Manager
//@requires Ext.direct.RemotingProvider
//@requires Ext.tip.QuickTip
//@requires Core.login.LoginWindow
/**
 *---------------------------------------------------------------
 * login.js
 *
 * Initiates a user login
 *
 * @package Login
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 * @todo Add 'Server under maintenance' message.
 * 
 *---------------------------------------------------------------
 */
/*javascript validation settings, see: http://www.jslint.com/lint.html*/
/*global console: false, Kron: false, Ext: false*/
/*jslint nomen: true*/

var gt = new Gettext({
    'domain' : 'core'
});

function _(msgid) {"use strict";
    return gt.gettext(msgid);
}

(function() {"use strict";
    // Create namespace (Core)
    Ext.namespace('Core');
        //<debug>
    // Specify a list of classes your application needs
    Ext.require(['Ext.direct.Manager', 'Ext.direct.RemotingProvider', 'Ext.tip.QuickTip', 'Core.login.LoginWindow']);
        //</debug>
    // For the compatibility with gettext, which is not supported in the login
    /*_ = function(text) {
     return text;
     };*/

    
    //=============================================================================
    
    Ext.onReady(function() {
        var waitIndicator, loginWindow, statusIconCls;
        // Hide the 'wait_indicator' animated gif
        waitIndicator = Ext.get('wait_indicator');
        if (waitIndicator !== undefined && waitIndicator !== null) {
            waitIndicator.setVisibilityMode(Ext.Element.DISPLAY);
            waitIndicator.hide();
        }
        // Init the singleton.  Any tag-based quick tips will start working.
        Ext.QuickTips.init();
        // Initialize the ExtDirect API
        Ext.direct.Manager.addProvider(Kron.APIDesc);
        // Create the Login window with language selection
        loginWindow = Ext.create('Core.login.LoginWindow', {
            id : 'wndLogin',
            languageSelect : true,
            x : 550,
            y : 100
        });
    
        loginWindow.show();
    
        // Catch the event of the user management, which requests for a login
        Ext.Direct.on('ext-direct-login', function(e) {
            loginWindow.body.unmask();
            if (e.type === 'event' && e.status === true && e.result.data !== undefined) {
                statusIconCls = 'x-status-error';
                if (e.result.data.loggedin === true) {
                    statusIconCls = 'x-status-valid';
                    loginWindow.setStatus({
                        text : e.data.message,
                        iconCls : statusIconCls
                    });
                   // window.location.replace('index.php');
                    window.location.reload(true);
                } else {
                    loginWindow.setStatus({
                        text : e.data.message,
                        iconCls : statusIconCls
                    });
                }
            } else {
                loginWindow.alertConnectionError();
            }
        });
    });
}());
