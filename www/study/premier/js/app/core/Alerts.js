/**
 *---------------------------------------------------------------
 * Alerts.js
 *
 * Alert-ekhez  helper class
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define("Kron.core.Alerts", {
    singleton : true,
 
    connectionError : function() {"use strict";
        Ext.Msg.alert(_("Hiba történt!"), _("Nem sikerült kapcsolódni a szerverhez!<br>Ellenőrizze az internet kapcsolatot majd próbálja meg újra!"));
    },
    connectionErrorShort : function() {"use strict";
        Ext.Msg.alert(_("Hiba történt!"), _("Nem sikerült kapcsolódni a szerverhez!<br>Ellenőrizze az internet kapcsolatot!"));
    },
    connectionErrorFormSave : function() {"use strict";
        Ext.Msg.alert(_("Hiba történt!"), _("Nem sikerült kapcsolódni a szerverhez!<br>Ellenőrizze az internet kapcsolatot! <br> Ha a kapcsolat helyreállt, nyissa meg majd zárja be az utoljára szerkesztett űrlapot, hogy a változtatásai eltárolásra kerüljenek!"));
    }
    
});