/**
 *---------------------------------------------------------------
 * UserManagement.js
 *
 * UserManagement helper class
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define("Kron.core.UserManagement", {
    singleton : true,
    permissions : [],
    //options : {},

    setPermissions : function(permissions) {"use strict";
        if (permissions !== undefined && permissions !== null) {
            this.permissions = permissions;
        }
    },
    
    hasPermission : function(directclass, method) {"use strict";
        var i, permission,
        permissionclass = this.permissions[directclass];
        if (permissionclass !== undefined && permissionclass !== null) {          
            for (i = 0 ; i < permissionclass.length; i += 1) {
                if (permissionclass[i] === method)
                {
                    return true;
                }
            }
        }
        return false;
    }
});