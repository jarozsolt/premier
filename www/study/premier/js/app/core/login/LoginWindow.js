/**
 *---------------------------------------------------------------
 * LoginWindow.js
 *
 * This file contains the Core.login.LoginWindow Extension Class
 *
 * @package Core
 * @subpackage usermanagement
 * @version 1.2.0
 * @class Core.login.LoginWindow
 * @extends Ext.Window
 * @constructor
 * @param {Object} config Configuration options
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 * @todo On logon success set a wait message (in statusbar)
 *---------------------------------------------------------------
 */

    // Create namespace (Core)
    Ext.namespace('Core');

    // The local text array
    var localeTexts = [];
    localeTexts.en_US = {
        "flduser" : "Username",
        "fldpass" : "Password",
        "wndtitle" : "Please Login",
        "btnlogin" : "Login",
        "msgready" : "Ready",
        "msgerror" : "Error",
        "msgconerr" : "Unable to connect to the server!",
        "btnselect" : "Language"
    };
    localeTexts.hu_HU = {
        "flduser" : "Felhasználónév",
        "fldpass" : "Jelszó",
        "wndtitle" : "Bejelentkezés",
        "btnlogin" : "Bejelentkezés",
        "msgready" : "Kész",
        "msgerror" : "Hiba",
        "msgconerr" : "Nem sikerült kapcsolódni a szerverhez!",
        "btnselect" : "Nyelv"
    };
    localeTexts.de_DE = {
        "flduser" : "Benutzername",
        "fldpass" : "Passwort",
        "wndtitle" : "Bitte Anmelden",
        "btnlogin" : "Anmelden",
        "msgready" : "Bereit",
        "msgerror" : "Fehler",
        "msgconerr" : "Keine Serververbindung!",
        "btnselect" : "Sprache"
    };
    localeTexts.fr_FR = {
        "flduser" : "Nom d'utilisateur",
        "fldpass" : "Mot de passe",
        "wndtitle" : "Login",
        "btnlogin" : "Login",
        "msgready" : "Disposé",
        "msgerror" : "Erreur",
        "msgconerr" : "Impossible de se connecter au serveur!",
        "btnselect" : "Langue"
    };

    Ext.define('Core.login.LoginWindow', {
        extend : 'Ext.Window',
        alias : 'kron.loginwindow',
        requires : ['Ext.window.*', 'Ext.tab.*', 'Ext.form.Panel', 'Ext.ux.statusbar.StatusBar', 'Core.util.MD5'],
        language : 'hu_HU',
        width : 360,
        height : 170,
        //   layout: 'fit',
        closable : false,
        resizable : false,
        iconCls : 'icon-login-wnd',
        languageSelect : false,
        plain : true,
        border : false,
        setStatus : function(status) { "use strict";
            this.m_statusBar.setStatus({
                text : status.text,
                iconCls : status.iconCls
            });
        },
        alertConnectionError : function() { "use strict";
            this.setStatus({
                text : localeTexts[this.language].msgconerr,
                iconCls : 'x-status-error'
            });
            Ext.Msg.alert(localeTexts[this.language].msgerror, localeTexts[this.language].msgconerr);

        },
        setLanguage : function(language) { "use strict";
            var prevlanguage, usernamefield, passwordfield, loginbutton, statusText;
            prevlanguage = this.language;
            this.language = language;
            // Set the window title
            this.setTitle(localeTexts[this.language].wndtitle);
            // Set the text of the 'Username' field
            usernamefield = this.m_formPanel.getForm().findField('loginUsername');
            usernamefield.setFieldLabel(localeTexts[language].flduser);
            // Set the text of the 'Password' field
            passwordfield = this.m_formPanel.getForm().findField('loginPassword');
            passwordfield.setFieldLabel(localeTexts[language].fldpass);
            // Set the text of the 'Login' button
            loginbutton = Ext.getCmp('loginButton');
            loginbutton.setText(localeTexts[language].btnlogin);
            // Set the text of the statusbar
            statusText = this.m_statusBar.getText();
            if (statusText === localeTexts[prevlanguage].msgconerr) {
                this.m_statusBar.setText(localeTexts[language].msgconerr);
            } else {
                this.m_statusBar.setText(localeTexts[language].msgready);
            }
            if (this.languageSelect) {
                this.languageSelectButton.setText(localeTexts[language].btnselect);
                this.languageSelectButton.setIconCls('icon-flag-' + language);
            }
        },
        onLanguageSelect : function(button, state) { "use strict";
            if (this.language !== button.name) {
                this.setLanguage(button.name);
            }
        },
        setUserName : function(name) { "use strict";
            var usernamefield = this.m_formPanel.getForm().findField('loginUsername');
            usernamefield.setValue(name);
        },
        handleResponse : function(provider, response) { "use strict";
            var loginWnd, statusIconCls;
            loginWnd = Ext.getCmp(this.id);
            loginWnd.body.unmask();
            if (response === undefined || response.result === undefined || response.result.data === undefined || response.status === false) {// no response from the server
                loginWnd.alertConnectionError();
            } else {
                statusIconCls = 'x-status-error';
                if (response.result.data.loggedin === true) {
                    statusIconCls = 'x-status-valid';
                    loginWnd.setStatus({
                        text : response.result.data.message,
                        iconCls : statusIconCls
                    });
                   // window.location.replace('index.php');
                   window.location.reload(true);
                } else {
                    loginWnd.setStatus({
                        text : response.result.data.message,
                        iconCls : statusIconCls
                    });
                }
            }
        },
        initComponent : function() { "use strict";
            this.title = localeTexts[this.language].wndtitle;
            this.m_formPanel = new Ext.form.Panel({
                header: false,
                labelWidth : 150,
                width : 350,
                height : 112,
                bodyStyle : 'padding: 10px',
                frame : true,
                defaultType : 'textfield',
                iconCls : 'icon-login-wnd',
                monitorValid : true,
                buttonAlign : 'center',
                id : 'frmLogin',
                //rbar : this.m_languageBar,
                items : [{
                    xtype : 'textfield',
                    name : 'loginUsername',
               //     value : 'testinv',// do not forget to delete this line!
                    fieldLabel : localeTexts[this.language].flduser,
                    msgTarget : 'side',
                    enableKeyEvents : true,
                    allowBlank : false,
                    listeners : {
                        specialkey : function(field, el) {
                            if (el.getKey() === Ext.EventObject.ENTER)
                            {
                                Ext.getCmp('loginButton').fireEvent('click');
                            }
                        }
                    }
                }, {
                    xtype : 'textfield',
                    name : 'loginPassword',
      			//	value : // do not forget to delete this line!
                    fieldLabel : localeTexts[this.language].fldpass,
                    inputType : 'password',
                    msgTarget : 'side',
                    allowBlank : false,
                    listeners : {
                        specialkey : function(field, el) {
                            if (el.getKey() === Ext.EventObject.ENTER)
                            {
                                Ext.getCmp('loginButton').fireEvent('click');
                            }
                        }
                    }
                }],
                buttons : [{
                    text : localeTexts[this.language].btnlogin,
                    id : 'loginButton',
                    scale : 'small',
                    iconCls : 'icon-login-btn_small',
                    ref : 'your_reference',
                    formBind : true,
                    // Function that fires when user clicks the button
                    listeners : {
                        scope : this,
                        click : function() {
                            var loginWnd, loginForm, loginStatusBar, language, password, params;
                            loginWnd = Ext.getCmp(this.id);
                            loginForm = loginWnd.m_formPanel;
                            loginStatusBar = loginWnd.m_statusBar;
                            if (loginForm.getForm().isValid()) {
                                language = loginWnd.language;
                                // encrypt password here
                                password = Core.util.MD5(loginForm.items.items[1].getValue(), false, true);
                                // show busy status
                                loginWnd.body.mask();
                                loginStatusBar.showBusy();
                                setTimeout("Ext.getCmp('" + this.id + "').handleResponse(undefined,undefined);", 60000);
                                params = {
                                    'username' : loginForm.items.items[0].getValue(),
                                    'password' : password,
                                    'language' : loginWnd.language
                                };
                                Kron.UserManagement.login(params, function(provider, response) {
                                    loginWnd.handleResponse(provider, response);
                                });
                            }
                        }
                    }
                }]

            });

            this.languageSelectButton = new Ext.button.Button({
                id : 'languageSelect',

                iconCls : 'icon-flag-' + this.language,
                text : localeTexts[this.language].btnselect,
                tooltip : localeTexts[this.language].btnselect,
                menuAlign : 'tr-br',
                menu : {
                    xtype : 'menu',
                    plain : true,
                    items : [{
                        name : 'en_US',
                        iconCls : 'icon-flag-en_US',
                        handler : this.onLanguageSelect,
                        text : 'English [EN]',
                        scope : this

                    }, {
                        name : 'hu_HU',
                        iconCls : 'icon-flag-hu_HU',
                        handler : this.onLanguageSelect,
                        text : 'Magyar [HU]',
                        scope : this

                    }, {
                        name : 'de_DE',
                        iconCls : 'icon-flag-de_DE',
                        handler : this.onLanguageSelect,
                        text : 'Deutsch [DE]',
                        scope : this
                    }, {
                        name : 'fr_FR',
                        iconCls : 'icon-flag-fr_FR',
                        handler : this.onLanguageSelect,
                        text : 'Français [FR]',
                        scope : this
                    }],
                    listeners : {
                        afterrender : function(component) {
                            // ***** ExtJS error patch *****
                            // Hide menu and then re-show so that alignment is correct.
                            component.hide();
                            component.show();
                        }
                    }
                }
            });

            this.m_statusBar = Ext.create('Ext.ux.StatusBar', {
                text : localeTexts[this.language].msgready,
                id : 'sbLogin',
                items : this.languageSelect ? [this.languageSelectButton] : []
            });

            Ext.apply(this, {
                items : [this.m_formPanel, this.m_statusBar]
            });

            Core.login.LoginWindow.superclass.initComponent.apply(this, arguments);
        }// end of initComponent
    });
    // end of define

// end of file
