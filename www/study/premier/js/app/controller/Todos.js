/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Todos.js
 *
 * A Todo panel controller-e.
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, DatDatanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.Todos', {
	extend : 'Ext.app.Controller',

	refs : [{
		ref : 'WeekView',
		selector : '#weekview'
	}, {
		ref : 'Week2View',
		selector : '#week2view'
	}, {
		ref : 'OverdueVisits',
		selector : '#overduevisits'
	}, {
		ref : 'DatePicker',
		selector : '#datepicker'
	}],

	stores : ['Calendardates', 'Overduedates'],

	init : function() {"use strict";
		this.control({
			'#overduevisits' : {
				afterrender : this.onOverdueVisitsRendered
			},
			'#weekview' : {
				afterrender : this.onWeekViewRendered
			},
			'#week2view' : {
				afterrender : this.onWeek2ViewRendered
			},
			'#datepicker' : {
				select : this.onDatePickerSelected,
				afterrender : this.onDatePickerRendered
			},
			'#infobar' : {
				infoBarChanged : this.onInfoBarChanged
			},
			 '#todoview' : {
                reloadStore : this.onInfoBarChanged
            }
		});

	},
	/**
	 * Ha változik a kiválasztott country vagy site akkor újratöltöm a store-t az aktuális nappal
	 */
	onInfoBarChanged : function() {"use strict";
		var datepicker = this.getDatePicker();
		if(datepicker) {
			datepicker.selectToday();
		}
		this.reloadData();
	},

	/**
	 * Induláskor meghívja a #reloadData metódust, hogy letöltődhessen az első két hét adata
	 */
	onLaunch : function() {"use strict";
		this.reloadData();
	},
	/**
	 * Törli, majd újra letölti a CalendardatesStore-t az aktuális dátumtol kezdve 14 nappal
	 */
	reloadData : function() {"use strict";
		var me = this, calendardatesStore = this.getCalendardatesStore(), overduedatesStore = this.getOverduedatesStore(), today = new Date(), startDate = new Date();

		startDate.setFullYear(today.getFullYear(), today.getMonth(), today.getDate() - (today.getDay() === 0 ? 6 : today.getDay() - 1));
		startDate.setTime(Ext.Date.clearTime(startDate).getTime());

		if (calendardatesStore !== null) {
			calendardatesStore.proxy.setExtraParam('startDate', startDate);
			calendardatesStore.proxy.setExtraParam('numberOfDays', 14);
			calendardatesStore.load(function(records, operation, success) {
				if (success) {
					var weekView = me.getWeekView(), week2View = me.getWeek2View();
					if (weekView !== undefined && weekView !== null) {
						weekView.updateDetail(me.getOneWeek(startDate));
					} else {
						me.startDate = startDate;
					}
					if (week2View !== undefined && week2View !== null) {
						week2View.updateDetail(me.getOneWeek(Ext.Date.add(startDate, Ext.Date.DAY, 7)));
					} else {
						me.startDate = startDate;
					}
					var todoView = Ext.getCmp('todoview');
                    if (todoView) {
                        todoView.setStoreDirty(false);
                    } 
				}
			});
		}
		if (overduedatesStore !== null) {
			overduedatesStore.load(function(records, operation, success) {
				if (success) {
					var overdueVisits = me.getOverdueVisits();
					if (overdueVisits !== undefined && overdueVisits !== null) {
						overdueVisits.updateDetail(Ext.Array.map(overduedatesStore.getRange(), function(rec) {
							return rec.getData(true);
						}));
					}
				}
			});
		}

	},
	/**
	 * Visszaadja egy hét adatát a template formázáshoz szükséges adatokkal kiegészítve
	 * Ha nem talál a CalendardatesStore-ban adott napot, akkor is visszaadja azt üresen, hogy jó lehessen a formázás
	 * @param {Date} date  A kezdő dátum
	 * @return {Object} Egy heti adat
	 */
	getOneWeek : function(date) {"use strict";
		var oneWeek = [], oneDay = {}, today = new Date(), calendardatesStore = this.getCalendardatesStore(), record, i, k, actDate, color;
		if (calendardatesStore !== null) {
			for ( i = 0; i < 7; i = i + 1) {
				actDate = Ext.Date.add(date, Ext.Date.DAY, i);
				record = calendardatesStore.findRecord('calendardate', actDate);
				if (record !== null) {
					oneDay = record.getData(true);
					for ( k = 0; k < oneDay.calendarvisits.length; k = k + 1) {
						color = 'white';
						if (oneDay.calendarvisits[k].status === Kron.visitStatus.Stored) {
							color = 'yellow';
						} else if (oneDay.calendarvisits[k].status === Kron.visitStatus.Completed) {
							color = 'green';
						} else if (oneDay.calendarvisits[k].status === Kron.visitStatus.Missing) {
							color = 'red';
						}
						oneDay.calendarvisits[k].color = color;// ((oneDay.calendarvisits[k].status === 'E' && today > actDate ) ? 'yellow' : 'green');
					}
				} else {
					oneDay = {
						'calendardate' : actDate,
						'calendarvisits' : []
					};

				}
				oneDay.date = actDate.getDate();
				oneDay.month = Ext.Date.monthNames[actDate.getMonth()];
				oneDay.year = actDate.getUTCFullYear();
				oneDay.weekday = Ext.Date.dayNames[actDate.getDay()];
				oneDay.day = actDate.getDay();
				//CSL ez okozhatja a TypeError: date.getTime is not a function
				oneWeek.push(Ext.clone(oneDay));
			}
		}
		return oneWeek;
	},
	/**
	 * Ez a metódus kezeli le, ha új dátumra kattint a user a datepicker-en és frissíti a Weekview paneleket.
	 * Ha nincs letöltve a kiválasztott dátum hetének első napja a store-ban, letölti azt. Ugyanígy a következő hétre is.
	 * @param {Object} datePicker A Datepicker object
	 * @param {Object} newdate A kiválasztott dátum
	 */
	onDatePickerSelected : function(datePicker, newdate) {"use strict";
		var startDate = new Date(), start2Date, calendardatesStore = this.getCalendardatesStore(), record, me = this, weekView = this.getWeekView(), week2View = this.getWeek2View();

		startDate.setFullYear(newdate.getFullYear(), newdate.getMonth(), newdate.getDate() - (newdate.getDay() === 0 ? 6 : newdate.getDay() - 1));
		startDate.setTime(Ext.Date.clearTime(startDate).getTime());

		if (calendardatesStore !== null) {
			record = calendardatesStore.findRecord('calendardate', startDate);
			if (record === null) {
				calendardatesStore.proxy.setExtraParam('startDate', startDate);
				calendardatesStore.proxy.setExtraParam('numberOfDays', 7);

				calendardatesStore.load({
					addRecords : true,
					callback : function(records, operation, success) {
						if (success && weekView !== null) {
							weekView.updateDetail(me.getOneWeek(startDate));
						}
					}
				});
			} else {
				if (weekView !== null) {
					weekView.updateDetail(this.getOneWeek(startDate));
				}
			}
			// masodik heti nezet
			start2Date = Ext.Date.add(startDate, Ext.Date.DAY, 7);
			record = calendardatesStore.findRecord('calendardate', start2Date);
			if (record === null) {
				calendardatesStore.proxy.setExtraParam('startDate', start2Date);
				calendardatesStore.proxy.setExtraParam('numberOfDays', 7);

				calendardatesStore.load({
					addRecords : true,
					callback : function(records, operation, success) {
						if (success && week2View !== null) {
							week2View.updateDetail(me.getOneWeek(start2Date));
						}
					}
				});
			} else {
				if (week2View !== null) {
					week2View.updateDetail(this.getOneWeek(start2Date));
				}
			}
		}
	},
	/**
	 * Induláskor, render után 1 sec-kel kijelöli a megjelenített két hetet a DatePicker-en
	 * @param {Object} picker
	 */
	onDatePickerRendered : function(picker) {"use strict";
		var me = this;
		Ext.Function.defer(function() {
			picker = me.getDatePicker();
			var cDate = new Date(), actWeek;
			actWeek = Ext.Date.getWeekOfYear(cDate);
			picker.cells.each(function(c) {
				var dt = c.dom.firstChild.dateValue;
				cDate.setTime(dt);
				if (actWeek <= Ext.Date.getWeekOfYear(cDate) && Ext.Date.getWeekOfYear(cDate) <= actWeek + 1) {
					c.addCls('x-mydate-selected');
				} else {
					c.removeCls('x-mydate-selected');
				}
			});
		}, 1000);
	},
	/**
	 * WeekView afterrender-t lekezelő metódus.
	 * Ha előbb lejön a CalendardatesStore első feltöltése, mint ahogy a weekView render elkészülne, akkor a #reloadData metódus nem frissíti a weekview-kat
	 * amit itt most megtörténi, ha a Week2View panel is renderelt és van a store-ban adat.
	 */
	onWeekViewRendered : function() {"use strict";
		var calendardatesStore = this.getCalendardatesStore(), startDate = new Date();
		if (this.getWeek2View().rendered && calendardatesStore !== null && calendardatesStore.getCount() > 0) {
			this.onDatePickerSelected(this.getDatePicker(), startDate);
		}
	},
	/**
	 * Hasonló a #onWeekViewRendered metódushoz, csak ha ez render-elődik később, ez frissíti a weekview-kat.
	 */
	onWeek2ViewRendered : function() {"use strict";
		var calendardatesStore = this.getCalendardatesStore(), startDate = new Date();
		if (this.getWeekView().rendered && calendardatesStore !== null && calendardatesStore.getCount() > 0) {
			this.onDatePickerSelected(this.getDatePicker(), startDate);
		}
	},

	onOverdueVisitsRendered : function() {"use strict";
		var overduedatesStore = this.getOverduedatesStore();
		if (overduedatesStore !== null && overduedatesStore.getCount() > 0) {
			this.getOverdueVisits().updateDetail(Ext.Array.map(overduedatesStore.getRange(), function(rec) {
				return rec.getData(true);
			}));
		}
	}
});
