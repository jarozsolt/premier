/*jslint nomen: true*/
/*global console: false, Kron: false, Ext: false, _:false*/

/**
 *---------------------------------------------------------------
 * Queries.js
 *
 * Queries view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
Ext.define('Kron.controller.Queries', {
	extend : 'Ext.app.Controller',
	requires : ['Kron.view.queries.OpenQueries', 'Kron.view.queries.PatientQueries'],

	refs : [{
		ref : 'OpenQueries',
		selector : 'OpenQueries'
	}, {
		ref : 'PatientQueries',
		selector : 'PatientQueries'
	},{
		ref : 'queriesview',
		selector : 'queriesview'
	}],

	stores : ['OpenQueries', 'PatientQueries'],

	init : function() {"use strict";
		this.control({
			'OpenQueries' : {
				itemdblclick : this.onQueryDblClick
			},
			'PatientQueries' : {
				itemdblclick : this.onPatientQueryDblClick,
				afterrender : this.onPatientQueryAfterRender
			},
			'#infobar' : {
				infoBarChanged : this.reloadData
			},
             '#queriesview' : {
                reloadStore : this.reloadData
            }
		});
	},
	onPatientQueryAfterRender: function() {"use strict";
		var myMask = new Ext.LoadMask(this.getPatientQueries().getEl(), {
			msg:_("Kérem várjon..."),
			store: this.getPatientQueriesStore() });
	},
	/**
	 * Feltölti az openQueriesStore-t az nyitott vagy megválaszolt query-k ill. az értéket módosított mezők számával.
	 * Ha volt legalább 1 betegnek ilyen, feldob egy figyelmeztető ablakot.
	 */
	reloadData : function() {"use strict";
		var me = this, openQueriesStore;
		openQueriesStore = this.getOpenQueriesStore();
		openQueriesStore.load(function(records, operation, success) {
    		if (success) {
    		     var queriesView = Ext.getCmp('queriesview');
                if (queriesView) {
                    queriesView.setStoreDirty(false);
                }
    		    if (records.length > 0) {
                    me.notification(_("Értesítés"), _("{0} páciensnél vannak nyitott query-k"), records.length);
                }
    		}	
    			
		});
	},
	/**
	 * Induláskor meghívja a reloadData metódust, hogy feltöltse az openQueriesStore-t
	 */
	onLaunch : function() {"use strict";
		this.reloadData();
	},

	/**
	 * Az open queries grid egy sorának dupplaklikk-jét kezeli le.
	 * A kiválasztott patientre letölti az összes query-t
	 * @param {Object} grid A grid
	 * @param {Object} queryRecord A kiválasztott record
	 */
	onQueryDblClick : function(grid, queryRecord) {"use strict";
		var title, PatientQueriesStore = this.getPatientQueriesStore();
		PatientQueriesStore.removeAll();
		this.getQueriesview().selectedPatientId = queryRecord.get('patientid');
		PatientQueriesStore.proxy.setExtraParam('patientid', queryRecord.get('patientid'));
		PatientQueriesStore.load();
		// TODO: lefordítani
		title = Ext.String.format(_("A {0} páciens query-jei"), queryRecord.get('patientid'));
		this.getQueriesview().setTitle( title);
		this.getQueriesview().getLayout().setActiveItem('PatientQueries');
	},
	
	/**
	 * A patient queries grid egy sorának dupplaklikk-jét kezeli le.
	 * Meghívja a patientView.setPatient metódusát, hogy jelenítse az adot formot.
	 * @param {Object} grid A grid
	 * @param {Object} queryRecord A kiválasztott record
	 */
	onPatientQueryDblClick : function(grid, queryRecord) {"use strict";
		var record = Ext.create('Kron.model.Form'), patientView = Ext.getCmp('patientview'), showQueries = true;
		record.set('patientid', queryRecord.get('patientid'));
		record.set('siteid', queryRecord.get('siteid'));
        patientView.setPatient(record, queryRecord.get('formtype'), queryRecord.get('instanceid'), queryRecord.get('fieldname'), showQueries);
	},

	/**
	 * A  notification függvénynek a message container-e
	 */
	msgCt : null,

	/**
	 * Segédfüggvény figyelmeztető buborékhoz
	 * @param {Object} title cím
	 * @param {Object} format formátum
	 */
	notification : function(title, format) {"use strict";
		var note, text;
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				id : 'msg-div'
			}, true);
		}
		text = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
		note = Ext.DomHelper.append(this.msgCt, '<div class="msg"><h3>' + title + '</h3><p>' + text + '</p></div>', true);
		note.hide();
		note.slideIn('b', {
			duration : 1000,
			delay : 4000
		}).ghost("b", {
			delay : 7000,
			remove : true
		});
	}
});

