/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Messages.js
 *
 * Messages view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.Messages', {
	extend : 'Ext.app.Controller',
	requires : ['Kron.view.messages.NewMessageWindow', 'Kron.core.Alerts'],
	refs : [{
		ref : 'ReceivedMessages',
		selector : 'ReceivedMessages'
	}, {
		ref : 'ReceivedMessageDetails',
		selector : 'ReceivedMessageDetails'
	}, {
		ref : 'SentMessages',
		selector : 'SentMessages'
	}, {
		ref : 'SentMessageDetails',
		selector : 'SentMessageDetails'
	}, {
		ref : 'Addressee',
		selector : 'Addressee'
	}],

	stores : ['ReceivedMessages', 'SentMessages', 'Addressee'],

	init : function() {"use strict";
		// Start listening for events on views
		this.control({
			'ReceivedMessages' : {
				selectionchange : this.onReceivedMessageSelect,
				itemdblclick : this.onReceivedMessagesDbclick
			},
			'SentMessages' : {
				selectionchange : this.onSentMessageSelect,
				itemdblclick : this.onSentMessagesDbclick
			},
			'#CreateNewMessage' : {
				click : this.onCreateNewMessage
			},
			'Addressee' : {
				checkchange : this.updateAddresse
			},
			'#RefreshMessages' : {
				click : this.onRefreshMessages
			},
			'#DeleteMessages' : {
				click : this.onDeleteMessages
			}
		});
	},

	onLaunch : function() {"use strict";
		var me = this;
		var receivedMessagesStore = this.getReceivedMessagesStore();
		receivedMessagesStore.load(function(records, operation, success) {
			var unreadCount = 0;
			Ext.each(records, function(item) {
				if(item.data.status === 'Unread') {
					unreadCount += 1;
				}
			});
			if(unreadCount > 0) {
				me.notification(_("Értesítés"), _("Önnek {0} új üzenete van."), unreadCount);
			}
		});

		var sentMessagesStore = this.getSentMessagesStore();
		sentMessagesStore.load();

		var addresseeStore = this.getAddresseeStore();
		addresseeStore.load();
	},

	onReceivedMessagesDbclick : function (selModel, selection) {"use strict";
		var win, window;
		win = Ext.ComponentQuery.query('MessageDetailsWindow');
		if(!win[0]) {
			window = Ext.create('widget.MessageDetailsWindow');
			
		} else {
			window=win[0];
		}
		window.getLayout().setActiveItem(0);
		window.show();
		var panel=Ext.getCmp('ReceivedMessageDetailsWindow');
		panel.updateDetail(selection.data);
	},
	
	onSentMessagesDbclick : function (selModel, selection) {"use strict";
		var win, window;
		win = Ext.ComponentQuery.query('MessageDetailsWindow');
		if(!win[0]) {
			window = Ext.create('widget.MessageDetailsWindow');	
		} else {
			window=win[0];
		}
		window.getLayout().setActiveItem(1);
		window.show();
		var panel=Ext.getCmp('SentMessageDetailsWindow');
		panel.updateDetail(selection.data);
	},
	
	onReceivedMessageSelect : function(selModel, selection) {"use strict";
		if (Ext.isDefined(selection[0])) {
			this.getReceivedMessageDetails().updateDetail(selection[0].data);
		}
	},

	onSentMessageSelect : function(selModel, selection) {"use strict";
		if(Ext.isDefined(selection[0])) {
			this.getSentMessageDetails().updateDetail(selection[0].data);
		}
	},

	onRefreshMessages : function() {"use strict";
		this.getReceivedMessagesStore().load();
	},

	onDeleteMessages : function() {"use strict";
		var receivedMessagesGrid = Ext.ComponentQuery.query('panel #ReceivedMessages');
		var selectedReceivedMessages = receivedMessagesGrid[0].getSelectionModel().getSelection();
		var inboxids = Array();

		Ext.each(selectedReceivedMessages, function(item) {
			inboxids.push(item.data.iid);
		});

		var sentMessagesGrid = Ext.ComponentQuery.query('panel #SentMessages');
		var selectedSentMessages = sentMessagesGrid[0].getSelectionModel().getSelection();
		var msgids = Array();

		Ext.each(selectedSentMessages, function(item) {
			msgids.push(item.data.msgid);
		});
		var params = {
			'inboxids' : inboxids,
			'msgids' : msgids
		}

		if(selectedSentMessages.length > 0 || selectedReceivedMessages.length > 0) {
		    // TODO: lefordítható állapotba hozni a szövegeket
			var msgText = _("Do You want to delete");
			if(selectedReceivedMessages.length > 0) {
				msgText += ' ' + selectedReceivedMessages.length + ' ' + _("received");
				if(selectedSentMessages.length > 0) {
					msgText += ' ' + _("and") + ' ' + selectedSentMessages.length + ' ' + _("sent");
				}
			} else {
				if(selectedSentMessages.length > 0) {
					msgText += ' ' + selectedSentMessages.length + ' ' + _("sent");
				}
			}

			if(selectedReceivedMessages.length + selectedSentMessages.length === 1) {
				msgText += ' ' + _("message") + '?';
			} else {
				msgText += ' ' + _("messages") + '?';
			}

			Ext.Msg.show({
				title : _("Törli az üzenetet?"),
				msg : msgText,
				buttons : Ext.Msg.YESNOCANCEL,
				icon : Ext.Msg.QUESTION,
				fn : function(btn) {
					if(btn === 'yes') {

						Kron.Messages.deleteMessages(params, function(provider, response) {
							if(response === undefined || response.result.data === undefined || response.status === false) {// no response from the server
							    Kron.core.Alerts.connectionErrorShort();
							} else {
								var SentMessagesStore = Ext.data.StoreManager.lookup('SentMessages');
								SentMessagesStore.remove(selectedSentMessages);
								var ReceivedMessagesStore = Ext.data.StoreManager.lookup('ReceivedMessages');
								ReceivedMessagesStore.remove(selectedReceivedMessages);
							}
						});
					}
				}
			});
		}
	},

	onCreateNewMessage : function() {"use strict";
		var win;
		win = Ext.ComponentQuery.query('NewMessageWindow');
		if(!win[0]) {
			win = Ext.create('widget.NewMessageWindow');
			win.show();
		} else {
			win[0].show();
		}
		var tree = Ext.ComponentQuery.query('#Addressee');
		tree[0].updateChecked();
	},

	updateAddresse : function(data) {"use strict";
		var tree = Ext.ComponentQuery.query('#Addressee');
		var records = tree[0].getView().getChecked(), names = [];
		Ext.Array.each(records, function(rec) {
			if(rec.get('leaf')) {
				names.push(rec.get('name'));
			}
		});
		var toField = Ext.ComponentQuery.query('#NewMessageDetailsTo');
		toField[0].setValue(names.join(', '));
	},

	msgCt : null,

	notification : function(title, format) {"use strict";
		if(!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				id : 'msg-div'
			}, true);
		}
		var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
		var m = Ext.DomHelper.append(this.msgCt, '<div class="msg"><h3>' + title + '</h3><p>' + s + '</p></div>', true);
		m.hide();
		m.slideIn('b', {
			duration : 1000
		}).ghost("b", {
			delay : 2000,
			remove : true
		});
	}
});
