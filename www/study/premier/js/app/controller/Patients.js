/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Patients.js
 *
 * Patients view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.Patients', {
    extend : 'Ext.app.Controller',

    //views : ['Viewport'],

    refs : [{
        ref : 'viewport',
        selector : 'viewport'
    }, {
        ref : 'center',
        selector : 'viewport > #center'
    }, {
        ref : 'patientview',
        selector : 'viewport > #center > #patientview'
    }, {
        ref : 'patientsview',
        selector : 'viewport > #center > #patientsview'
    }, {
        ref : 'patientsgrid',
        selector : 'viewport > #center > #patientsview > #patientsgrid'
    }, {
        ref : 'patientmenu',
        selector : 'viewport > #center > #patientview > #patientmenu'
    }, {
        ref : 'patientsprintbutton',
        selector : 'toolbar #patientstoolbar-btn-print'
    }, {
        ref : 'signbutton',
        selector : 'viewport > #center > #patientview > #patientmenu > toolbar #patientmenu-btn-sign'
    }, {
        ref : 'patientcenter',
        selector : 'viewport > #center > #patientview> #patientview-center'
    }, {
        ref : 'addpatientdlg',
        selector : '#addpatientdlg'
    }],

    stores : ['Patients'],

    init : function() {"use strict";
        // Start listening for events on views
        this.control({
            'patientsgrid' : {
                itemdblclick : this.onPatientDblClick,
                selectionchange : this.onPatientRowSelected
            },
            'toolbar #patientstoolbar-btn-new' : {
                click : this.onAddNewPatientBtnClicked
            },
            'toolbar #patientstoolbar-btn-print' : {
                click : this.onPrintPatientBtnClicked
            },
            '#infobar' : {
                infoBarChanged : this.reloadData
            },
             '#patientsview' : {
                reloadStore : this.reloadData
            }
        });
    },

    reloadData : function() {"use strict";
        var patientsStore = this.getPatientsStore();
        patientsStore.load({
            scope: this,
            callback: function(records, operation, success) {
                if (success) {
                    var patientsView = Ext.getCmp('patientsview');
                    if (patientsView) {
                        patientsView.setStoreDirty(false);
                    }
                }
            }
        });

    },

    onLaunch : function() {"use strict";
        this.reloadData();
    },

    onAddNewPatientBtnClicked : function(button, e, eOpts) {"use strict";
        var addPatientDlgList, addPatientDlg;
        addPatientDlgList = Ext.ComponentQuery.query('addpatientdlg');
        if (!addPatientDlgList[0]) {
            addPatientDlg = Ext.create('Kron.view.patients.AddPatientDialog');
            addPatientDlg.show();
        } else {
            addPatientDlgList[0].show();
        }

    },

    onPrintPatientBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "printForm"))
        {
            var patientsGrid, patientRecord, selectionModel, patientId;
            patientsGrid = this.getPatientsgrid();
            if (patientsGrid !== undefined && patientsGrid !== null) {
                selectionModel = patientsGrid.getSelectionModel();
                if (selectionModel !== undefined && selectionModel !== null) {
                    if (selectionModel.hasSelection()) {
                        patientRecord = selectionModel.getLastSelected();
                        if (patientRecord !== undefined && patientRecord !== null) {
                            patientId = patientRecord.get('patientid');
                            if (patientId !== undefined && patientId !== null)
                            {
                                Kron.view.printer.Printer.forcePDFSaveAs = true;
                                Kron.view.printer.Printer.printPatientToPDF(patientId);
                            }
                        }
                    }
                }
            }
        }
    },

    onPatientDblClick : function(grid, record) {"use strict";
        this.getPatientview().setPatient(record, "visitview", null);
    },

    onPatientRowSelected : function(grid, record, options) {"use strict";
        var patientRecord, selectionModel,
        // A nyomtatás letíltva, amíg nem gyorsítunk rajta
        printButton, //printButton = this.getPatientsprintbutton(),
        patientsGrid = this.getPatientsgrid(),
        buttondisabled = true;
        if (patientsGrid !== undefined && patientsGrid !== null) {
            selectionModel = patientsGrid.getSelectionModel();
            if (selectionModel !== undefined && selectionModel !== null) {
                // Van kiválasztott sor?
                if (selectionModel.hasSelection()) {
                    patientRecord = selectionModel.getLastSelected();
                    // Ha van kiválasztva sor engedélyezzük az edit, view és delete gombokat
                    if (patientRecord !== undefined && patientRecord !== null) {
                        buttondisabled = false;
                    }
                }
            }
        }
        // Nincs kiválasztva sor, letíltjuk a print gombot
        if (printButton !== undefined) {
            printButton.setDisabled(buttondisabled);
        }
    }
});
