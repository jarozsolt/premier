/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Visits.js
 *
 * Visits view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.Visits', {
	extend : 'Ext.app.Controller',
	requires : ['Kron.core.Alerts'],
	//views : ['Viewport'],

	refs : [{
		ref : 'center',
		selector : 'viewport > #center'
	}, {
		ref : 'patientview',
		selector : 'viewport > #center > #patientview'
	}, {
		ref : 'patientmenu',
		selector : 'viewport > #center > #patientview> #patientmenu'
	}, {
		ref : 'patientcenter',
		selector : 'viewport > #center > #patientview> #patientview-center'
	}, {
		ref : 'visitdlg',
		selector : '#visitdlg'
	}, {
		ref : 'visitgrid',
		selector : '#visitgrid'
	}, {
		ref : 'unsvisitform',
		selector : '#UnscheduledVisitForm'
	}, {
		ref : 'visitdeletebutton',
		selector : 'toolbar #visittoolbar-btn-delete'
	}, {
		ref : 'visiteditbutton',
		selector : 'toolbar #visittoolbar-btn-edit'
	}, {
        ref : 'visitviewbutton',
        selector : 'toolbar #visittoolbar-btn-view'
    }, {
        ref : 'visitprintbutton',
        selector : 'toolbar #visittoolbar-btn-print'
    }, {
		ref : 'patientdlg',
		selector : '#patientdlg'
	}, {
		ref : 'queryForm',
		selector : '#queryform'
	}, {
		ref : 'querydlg',
		selector : '#querydlg'
	}],

	stores : ['Forms'],

	init : function() {"use strict";
		// Start listening for events on views
		this.control({
			'visitgrid' : {
				itemdblclick : this.onVisitDblClick,
				selectionchange : this.onVisitRowSelected
			},
			'toolbar #visittoolbar-btn-new' : {
				click : this.onNewVisitBtnClicked
			},
			'toolbar #visittoolbar-btn-edit' : {
				click : this.onEditVisitBtnClicked
			},
			'toolbar #visittoolbar-btn-view' : {
                click : this.onViewVisitBtnClicked
            },
			'toolbar #visittoolbar-btn-delete' : {
				click : this.onDeleteVisitBtnClicked
			},
            'toolbar #visittoolbar-btn-print' : {
                click : this.onPrintVisitBtnClicked
            }
		});
	},

	// New unscheduled visit
	onNewVisitBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "createForm"))
        {
            var patientDlg = this.getPatientdlg(),
				store = this.getFormsStore(),
				record = Ext.create('Kron.model.Form', 
					{
						formtype : 'UnscheduledVisit',
		                patientid : this.getPatientview().getPatient(),
		                status : 'N',
		                istmp : 1
					}),
				newRecords;
				
          
			// autosave miatt adom hozzá a store-hoz és sync-elem
			newRecords=store.add(record); // tömböt ad vissza!!!!
			store.sync({
                failure: function() { Kron.core.Alerts.connectionError();
                }
            });
			
			patientDlg.setRecord(newRecords[0], '', false);
        }
	},

    onEditVisit : function(record) {"use strict";
        if (record.get('status') !== 'D') {// not deleted
            var patientDlg = this.getPatientdlg();
            
            
            patientDlg.setRecord(record, '', false);
        }
    },
    
    onViewVisit : function(record) {"use strict";
        if (record.get('status') !== 'D') {// not deleted
            var patientDlg = this.getPatientdlg();
            patientDlg.setRecord(record, '', true, ['monitorcheck'] );
        }
    },


	onDeleteVisitBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "deleteForm"))
        {
            var visitGrid, visitRecord, idx;
            visitGrid = this.getVisitgrid();
            if (visitGrid.getSelectionModel().hasSelection()) {
                visitRecord = visitGrid.getSelectionModel().getLastSelected();
                if ( visitRecord.get('formtype') === 'UnscheduledVisit')// only 'UnscheduledVisitForm' can be deleted
                {
                    // not deleted
                    Ext.Msg.show({
                        title : _("Megerősítés"),
                        msg : _("Biztosan törölni akarja ezt a vizitet?") + '<br><b>' + visitRecord.getFormName() + '<b><br>',
                        buttons : Ext.Msg.YESNO,
                        icon : Ext.MessageBox.WARNING,
                        scope : this,
                        width : 400,
                        fn : function(btn, ev) {
                            if (btn === 'yes') {
                                var store = this.getFormsStore();
                            
                                    store.remove(visitRecord);
                                    store.sync({
                                        failure: function() { Kron.core.Alerts.connectionError();
                                        }
                                    });

                            }
                        }
                    });
				}
            }
        }
	},

    onPrintVisitBtnClicked : function(button, e, eOpts) {"use strict";  
        if (Kron.core.UserManagement.hasPermission("Forms", "printForm"))
        {
            var visitGrid, visitRecord, selectionModel, formId;
            visitGrid = this.getAegrid();
            if (visitGrid !== undefined && visitGrid !== null) {
                selectionModel = visitGrid.getSelectionModel();
                if (selectionModel !== undefined && selectionModel !== null) {
                    if (selectionModel.hasSelection()) {
                        visitRecord = selectionModel.getLastSelected();
                        if (visitRecord !== undefined && visitRecord !== null) {
                            formId = visitRecord.get('instanceid');
                            if (formId > 0)
                            {
                                Kron.view.printer.Printer.forcePDFSaveAs = true;
                                Kron.view.printer.Printer.printFormToPDF(formId);
                            }
                        }
                    }
                }
            }
        }
    },
    
	onEditVisitBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "updateForm"))
        {
            var visitGrid, visitRecord;
            visitGrid = this.getVisitgrid();
            if (visitGrid.getSelectionModel().hasSelection()) {
                visitRecord = visitGrid.getSelectionModel().getLastSelected();
                this.onEditVisit(visitRecord);
            }
        }
	},
	
	onViewVisitBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms"))
        {
            var visitGrid, visitRecord;
            visitGrid = this.getVisitgrid();
            if (visitGrid.getSelectionModel().hasSelection()) {
                visitRecord = visitGrid.getSelectionModel().getLastSelected();
                this.onViewVisit(visitRecord);
            }
        }
    },

	onVisitDblClick : function(grid, record) {"use strict";
	    var visitGrid, visitRecord, permissionClass;
        visitGrid = this.getVisitgrid();
		if (visitGrid.getSelectionModel().hasSelection()) {
			visitRecord = visitGrid.getSelectionModel().getLastSelected();
			permissionClass = "Forms";
			if (Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
			{
			    this.onEditVisit(visitRecord);
			}
			else if (Kron.core.UserManagement.hasPermission(permissionClass, "getForms"))
			{
                this.onViewVisit(visitRecord);
            }
		}
	},

    onVisitRowSelected : function(grid, record, options) {"use strict";
        var visitRecord, selectionModel,
        visitGrid = this.getVisitgrid(),
        editButton = this.getVisiteditbutton(),
        viewButton = this.getVisitviewbutton(),
        deleteButton = this.getVisitdeletebutton(),
        printButton = this.getVisitprintbutton(),
        buttondisabled = true;

        if (visitGrid !== undefined && visitGrid !== null) {
            selectionModel = visitGrid.getSelectionModel();
            if (selectionModel !== undefined && selectionModel !== null) {
                // Van kiválasztott sor?
                if (selectionModel.hasSelection()) {
                    visitRecord = selectionModel.getLastSelected();
                    // Ha van kiválasztva sor engedélyezzük az edit, view, print és delete gombokat
                    if (visitRecord !== undefined && visitRecord !== null) {
                        if (deleteButton !== undefined) {
                            if (visitRecord.isUnscheduledVisit()) {
                                deleteButton.setDisabled(false);
                            } else {
                                deleteButton.setDisabled(true);
                            }
                        }
                        if (editButton !== undefined) {
                            editButton.setDisabled(false);
                        }
                        if (viewButton !== undefined) {
                            viewButton.setDisabled(false);
                        }
                        if (printButton !== undefined) {
                            printButton.setDisabled(true);
                        }
                        return;
                    }
                }
            }
        }
        // ha nincs kiválasztva semmi,le kell tiltani a modosit, töröl, megnez gombokat
        if (editButton !== undefined) {
            editButton.setDisabled(buttondisabled);
        }
        if (viewButton !== undefined) {
            viewButton.setDisabled(buttondisabled);
        }
        if (deleteButton !== undefined) {
            deleteButton.setDisabled(buttondisabled);
        }
        if (printButton !== undefined) {
            printButton.setDisabled(buttondisabled);
        }
    }
});
