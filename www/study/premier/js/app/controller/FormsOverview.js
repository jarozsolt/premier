/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FormsOverview.js
 *
 * Forms Overview view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 * @sample http://www.cparker15.com/code/examples/javascript/extjs4-dynamic-grid-example/
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.FormsOverview', {
    extend: 'Ext.app.Controller',
    
  //  views: [            'Viewport'        ],

    refs: [	{ ref: 'overviewView', selector: '#overviewview' },
		    { ref: 'overviewGrid', selector: '#overviewGrid' },
			{ ref: 'patientdlg', selector: '#patientdlg'},
			{ ref: 'queryForm', selector: '#queryForm' },
			{ ref: 'querydlg', selector: '#querydlg' }], 

	//stores: ['Visits','Medications', 'AdverseEvents'],
    
    init: function() {"use strict";
       // Start listening for events on views
        this.control({
            'toolbar #overviewtoolbar-btn-sign': {
                click: this.onOverviewBtnClicked
            },
            '#overviewGrid': {
                itemdblclick: this.onItemDblClick
            }    
        });
       
    },
      
	
	onItemDblClick: function(grid, record) {"use strict";
		if (record){
			var store = Ext.data.StoreManager.lookup(record.get('storeid')),
			originalRecord=store.getById(record.get('id')),
			patientdlg= this.getPatientdlg(),   
			 queryForm = this.getQueryForm(),
			queryDlg=this.getQuerydlg();

            patientdlg.setRecord(originalRecord, '');
            patientdlg.setReadOnly(true);
            patientdlg.show();
			
			
            if (queryForm !== undefined) {
                queryForm.getForm().reset();
            }
			
			if (queryDlg !== undefined) {
                queryDlg.show();
                queryDlg.hide();
            }
		}
	},
	
	onOverviewBtnClicked: function(button, e, eOpts) {"use strict";
	}
});
