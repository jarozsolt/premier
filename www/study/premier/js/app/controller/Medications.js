/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Medications.js
 *
 * Medication view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 * @sample http://www.cparker15.com/code/examples/javascript/extjs4-dynamic-grid-example/
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.Medications', {
    extend : 'Ext.app.Controller',
	requires : ['Kron.core.Alerts'],
    //views : ['Viewport'],

    refs : [{
        ref : 'center',
        selector : 'viewport > #center'
    }, {
        ref : 'patientview',
        selector : 'viewport > #center > #patientview'
    }, {
        ref : 'patientmenu',
        selector : 'viewport > #center > #patientview> #patientmenu'
    }, {
        ref : 'patientcenter',
        selector : 'viewport > #center > #patientview> #patientview-center'
    }, {
        ref : 'patientdlg',
        selector : '#patientdlg'
    }, {
        ref : 'medicationgrid',
        selector : '#medicationgrid'
    }, {
        ref : 'medicationform',
        selector : '#MedicationForm'
    }, {
        ref : 'medicationdeletebutton',
        selector : 'toolbar #medicationtoolbar-btn-delete'
    }, {
        ref : 'medicationeditbutton',
        selector : 'toolbar #medicationtoolbar-btn-edit'
    }, {
        ref : 'medicationviewbutton',
        selector : 'toolbar #medicationtoolbar-btn-view'
    }, {
        ref : 'medicationprintbutton',
        selector : 'toolbar #medicationtoolbar-btn-print'
    }, {
        ref : 'queryForm',
        selector : '#queryform'
    }, {
        ref : 'querydlg',
        selector : '#querydlg'
    }],

    stores : ['Forms'],

    init : function() {"use strict";
        // Start listening for events on views
        this.control({
            'medicationgrid' : {
                itemdblclick : this.onMedicationDblClick,
                selectionchange : this.onMedicationRowSelected
            },
            'toolbar #medicationtoolbar-btn-new' : {
                click : this.onNewMedicationBtnClicked
            },
            'toolbar #medicationtoolbar-btn-edit' : {
                click : this.onEditMedicationBtnClicked
            },
            'toolbar #medicationtoolbar-btn-view' : {
                click : this.onViewMedicationBtnClicked
            },
            'toolbar #medicationtoolbar-btn-delete' : {
                click : this.onDeleteMedicationBtnClicked
            },
            'toolbar #medicationtoolbar-btn-print' : {
                click : this.onPrintMedicationBtnClicked
            }
        });

    },


    onNewMedicationBtnClicked : function(button, e, eOpts) { "use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "createForm"))
        {
            var store = this.getFormsStore(),
				patientDlg= this.getPatientdlg(),
				record= Ext.create('Kron.model.Form',
					{
		                formtype : 'Medication',
		                patientid : this.getPatientview().getPatient(),
		                status : 'N',
		                istmp : 1
		            }),
				newRecords;

			 


			// autosave miatt adom hozzá a store-hoz és sync-elem
			newRecords=store.add(record); // tömböt ad vissza!!!!
			store.sync({
                failure: function() { Kron.core.Alerts.connectionError();
                }
            });
			
			patientDlg.setRecord(newRecords[0], '',false);

        }
    },

    onEditMedication : function(record) { "use strict";
        if (record.get('status') !== 'D') {// not deleted
            var patientDlg = this.getPatientdlg();
            patientDlg.setRecord(record, '', false);

        }
    },
    
    onViewMedication : function(record) { "use strict";
        if (record.get('status') !== 'D') {// not deleted
            var patientDlg = this.getPatientdlg();
            patientDlg.setRecord(record, '', true, ['monitorcheck'] );

        }
    },

    onDeleteMedicationBtnClicked : function(button, e, eOpts) { "use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "deleteForm"))
        {
            var medicationGrid, medicationRecord, idx;
            medicationGrid = this.getMedicationgrid();
            if (medicationGrid.getSelectionModel().hasSelection()) {
                medicationRecord = medicationGrid.getSelectionModel().getLastSelected();
    
                    Ext.Msg.show({
                        title : _("Megerősítés"),
                        msg : medicationRecord.getDeleteMedicationQuestion(),
                        buttons : Ext.Msg.YESNO,
                        icon : Ext.MessageBox.WARNING,
                        scope : this,
                        width : 400,
                        fn : function(btn, ev) {
                            if (btn === 'yes') {
    
                                var store = this.getFormsStore();
       
                                    store.remove(medicationRecord);
                                    store.sync({
                                        failure: function() { Kron.core.Alerts.connectionError();
                                        }
                                    });

                            }
                        }
                    });

            }
        }
    },
    
    onPrintMedicationBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "printForm"))
        {
            var medicationGrid, medicationRecord, selectionModel, formId;
            medicationGrid = this.getMedicationgrid();
            if (medicationGrid !== undefined && medicationGrid !== null) {
                selectionModel = medicationGrid.getSelectionModel();
                if (selectionModel !== undefined && selectionModel !== null) {
                    if (selectionModel.hasSelection()) {
                        medicationRecord = selectionModel.getLastSelected();
                        if (medicationRecord !== undefined && medicationRecord !== null) {
                            formId = medicationRecord.get('instanceid');
                            if (formId > 0)
                            {
                                Kron.view.printer.Printer.forcePDFSaveAs = true;
                                Kron.view.printer.Printer.printFormToPDF(formId);
                            }
                        }
                    }
                }
            }
        }
    },

    onEditMedicationBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "updateForm"))
        {
            var medicationGrid, medicationRecord;
            medicationGrid = this.getMedicationgrid();
            if (medicationGrid.getSelectionModel().hasSelection()) {
                medicationRecord = medicationGrid.getSelectionModel().getLastSelected();
                this.onEditMedication(medicationRecord);
            }
        }
    },
    
    
    onViewMedicationBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms"))
        {
            var medicationGrid, medicationRecord;
            medicationGrid = this.getMedicationgrid();
            if (medicationGrid.getSelectionModel().hasSelection()) {
                medicationRecord = medicationGrid.getSelectionModel().getLastSelected();
                this.onViewMedication(medicationRecord);
            }
        }
    },

    onMedicationDblClick : function(grid, record) {"use strict";
        var medicationGrid, medicationRecord, permissionClass;
        medicationGrid = this.getMedicationgrid();
        if (medicationGrid.getSelectionModel().hasSelection()) {
            medicationRecord = medicationGrid.getSelectionModel().getLastSelected();
            permissionClass = "Forms";
            if (Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
            {
                this.onEditMedication(medicationRecord);
            }
            else if (Kron.core.UserManagement.hasPermission(permissionClass, "getForms"))
            {
                this.onViewMedication(medicationRecord);
            }
        }
    },

    onMedicationRowSelected : function(grid, record, options) {"use strict";
        var medicationRecord, selectionModel,
        medicationGrid = this.getMedicationgrid(),
        editButton = this.getMedicationeditbutton(),
        viewButton = this.getMedicationviewbutton(),
        deleteButton = this.getMedicationdeletebutton(),
        printButton = this.getMedicationprintbutton(),
        buttondisabled = true;
        if (medicationGrid !== undefined && medicationGrid !== null) {
            selectionModel = medicationGrid.getSelectionModel();
            if (selectionModel !== undefined && selectionModel !== null) {
                // Van kiválasztott sor?
                if (selectionModel.hasSelection()) {
                    medicationRecord = selectionModel.getLastSelected();
                    // Ha van kiválasztva sor engedélyezzük az edit, view és delete gombokat
                    if (medicationRecord !== undefined && medicationRecord !== null) {
                        buttondisabled = false;
                    }
                }
            }
        }
        // Nincs kiválasztva sor, letíltjuk az edit, view, print és delete gombokat
        if (editButton !== undefined) {
            editButton.setDisabled(buttondisabled);
        }
        if (viewButton !== undefined) {
            viewButton.setDisabled(buttondisabled);
        }
        if (deleteButton !== undefined) {
            deleteButton.setDisabled(buttondisabled);
        }
        if (printButton !== undefined) {
            printButton.setDisabled(buttondisabled);
        }
    }

}); 

