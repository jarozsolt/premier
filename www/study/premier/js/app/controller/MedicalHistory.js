/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MedicalHistory.js
 *
 * Medical history view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.MedicalHistory', {
    extend : 'Ext.app.Controller',
	requires : ['Kron.core.Alerts'],
    //views : ['Viewport'],

    refs : [{
        ref : 'center',
        selector : 'viewport > #center'
    }, {
        ref : 'patientview',
        selector : 'viewport > #center > #patientview'
    }, {
        ref : 'patientmenu',
        selector : 'viewport > #center > #patientview> #patientmenu'
    }, {
        ref : 'patientcenter',
        selector : 'viewport > #center > #patientview> #patientview-center'
    }, {
        ref : 'patientdlg',
        selector : '#patientdlg'
    }, {
        ref : 'medicalhistorydlg',
        selector : '#medicalhistorydlg'
    }, {
        ref : 'medicalhistorygrid',
        selector : '#medicalhistorygrid'
    }, {
        ref : 'medicalhistoryform',
        selector : '#MedicalHistoryForm'
    }, {
        ref : 'medicalhistorydeletebutton',
        selector : 'toolbar #medicalhistorytoolbar-btn-delete'
    }, {
        ref : 'medicalhistoryeditbutton',
        selector : 'toolbar #medicalhistorytoolbar-btn-edit'
    }, {
        ref : 'medicalhistoryviewbutton',
        selector : 'toolbar #medicalhistorytoolbar-btn-view'
    }, {
        ref : 'medicalhistoryprintbutton',
        selector : 'toolbar #medicalhistorytoolbar-btn-print'
    }, {
        ref : 'queryForm',
        selector : '#queryForm'
    }, {
        ref : 'querydlg',
        selector : '#querydlg'
    }],

    stores : ['Forms'],

    init : function() {"use strict";
        // Start listening for events on views
        this.control({
            'medicalhistorygrid' : {
                itemdblclick : this.onMedicalHistoryDblClick,
                selectionchange : this.onMedicalHistoryRowSelected
            },
            'toolbar #medicalhistorytoolbar-btn-new' : {
                click : this.onNewMedicalHistoryBtnClicked
            },
            'toolbar #medicalhistorytoolbar-btn-edit' : {
                click : this.onEditMedicalHistoryBtnClicked
            },
            'toolbar #medicalhistorytoolbar-btn-view' : {
                click : this.onViewMedicalHistoryBtnClicked
            },
            'toolbar #medicalhistorytoolbar-btn-print' : {
                click : this.onPrintMedicalHistoryBtnClicked
            },
            'toolbar #medicalhistorytoolbar-btn-delete' : {
                click : this.onDeleteMedicalHistoryBtnClicked
            }
        });
    },

    onNewMedicalHistoryBtnClicked : function(button, e, eOpts) {"use strict";
        var store, patientDlg, record, newRecords;
        if (Kron.core.UserManagement.hasPermission("Forms", "createForm"))
        {
            store = this.getFormsStore();
            patientDlg= this.getPatientdlg();
			record = Ext.create('Kron.model.Form',
				{
	                formtype : 'MedicalHistory',
	                patientid : this.getPatientview().getPatient(),
	                status : 'N',
	                istmp : 1
	            });

			// autosave miatt adom hozzá a store-hoz és sync-elem
			newRecords = store.add(record); // tömböt ad vissza!!!!
			store.sync({
			    failure: function() { Kron.core.Alerts.connectionError();
                }
            });
			patientDlg.setRecord(newRecords[0], '', false);
        }
    },

    onEditMedicalHistory : function(record) {"use strict";
       if (record.get('status') !== 'D') {// not deleted
            var patientDlg = this.getPatientdlg();
            patientDlg.setRecord(record, '', false);

        }
    },
    
    onViewMedicalHistory : function(record) {"use strict";
		if (record.get('status') !== 'D') {// not deleted
            var patientDlg = this.getPatientdlg();
            patientDlg.setRecord(record, '', true, ['monitorcheck'] );
        }
    },

    onDeleteMedicalHistoryBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "deleteForm"))
        {
            var medicalhistoryGrid, record;
            medicalhistoryGrid = this.getMedicalhistorygrid();
            if (medicalhistoryGrid.getSelectionModel().hasSelection()) {
                record = medicalhistoryGrid.getSelectionModel().getLastSelected();

                Ext.Msg.show({
                    title : _("Megerősítés"),
                    msg : _("Biztosan törölni akarja ezt a kórelőzményt, egyéb betegséget?") + '<br><b>' + record.getFieldValue('diagnosis') + '<b><br>',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.WARNING,
                    scope : this,
                    width : 400,
                    fn : function(btn, ev) {
                        if (btn === 'yes') {
                            var store, idx;
                            store = this.getFormsStore();

                                store.remove(record);
                                store.sync({
                                    failure: function() { Kron.core.Alerts.connectionError();
                                    }
                                });

                        }
                    }
                });

            }
        }
    },
    
    
    onPrintMedicalHistoryBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "printForm"))
        {
            var medicalhistoryGrid, medicalhistoryRecord, selectionModel, formId;
            medicalhistoryGrid = this.getMedicalhistorygrid();
            if (medicalhistoryGrid !== undefined && medicalhistoryGrid !== null) {
                selectionModel = medicalhistoryGrid.getSelectionModel();
                if (selectionModel !== undefined && selectionModel !== null) {
                    if (selectionModel.hasSelection()) {
                        medicalhistoryRecord = selectionModel.getLastSelected();
                        if (medicalhistoryRecord !== undefined && medicalhistoryRecord !== null) {
                            formId = medicalhistoryRecord.get('instanceid');
                            if (formId > 0)
                            {
                                Kron.view.printer.Printer.forcePDFSaveAs = true;
                                Kron.view.printer.Printer.printFormToPDF(formId);
                            }
                        }
                    }
                }
            }
        }
    },

    onEditMedicalHistoryBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "updateForm"))
        {
            var medicalhistoryGrid, record;
            medicalhistoryGrid = this.getMedicalhistorygrid();
            if (medicalhistoryGrid.getSelectionModel().hasSelection()) {
                record = medicalhistoryGrid.getSelectionModel().getLastSelected();
                this.onEditMedicalHistory(record);
            }
        }
    },
    
    
    onViewMedicalHistoryBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms"))
        {
            var medicalhistoryRecord,
            medicalhistoryGrid = this.getMedicalhistorygrid();
            if (medicalhistoryGrid.getSelectionModel().hasSelection()) {
                medicalhistoryRecord = medicalhistoryGrid.getSelectionModel().getLastSelected();
                this.onViewMedicalHistory(medicalhistoryRecord);
            }
        }
    },

    onMedicalHistoryDblClick : function(grid, record) {"use strict";
        var medicalhistoryGrid, medicalhistoryRecord, permissionClass;
        medicalhistoryGrid = this.getMedicalhistorygrid();
        if (medicalhistoryGrid.getSelectionModel().hasSelection()) {
            medicalhistoryRecord = medicalhistoryGrid.getSelectionModel().getLastSelected();
            if (medicalhistoryRecord !== undefined && medicalhistoryRecord !== null) {    
                permissionClass = "Forms";
                if (Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
                {
                    this.onEditMedicalHistory(medicalhistoryRecord);
                }
                else if (Kron.core.UserManagement.hasPermission(permissionClass, "getForms"))
                {
                    this.onViewMedicalHistory(medicalhistoryRecord);
                }
            }
        }
    },

    onMedicalHistoryRowSelected : function(grid, record, options) {"use strict";
       var medicalhistoryRecord, selectionModel,
        medicalhistoryGrid = this.getMedicalhistorygrid(),
        editButton = this.getMedicalhistoryeditbutton(),
        viewButton = this.getMedicalhistoryviewbutton(),
        deleteButton = this.getMedicalhistorydeletebutton(),
        printButton = this.getMedicalhistoryprintbutton(),
        buttondisabled = true;
        if (medicalhistoryGrid !== undefined && medicalhistoryGrid !== null) {
            selectionModel = medicalhistoryGrid.getSelectionModel();
            if (selectionModel !== undefined && selectionModel !== null) {
                // Van kiválasztott sor?
                if (selectionModel.hasSelection()) {
                    medicalhistoryRecord = selectionModel.getLastSelected();
                    // Ha van kiválasztva sor engedélyezzük az edit, view, print és delete gombokat
                    if (medicalhistoryRecord !== undefined && medicalhistoryRecord !== null) {
                        buttondisabled = false;
                    }
                }
            }
        }
        // Nincs kiválasztva sor, letíltjuk az edit, view és delete gombokat
        if (editButton !== undefined) {
            editButton.setDisabled(buttondisabled);
        }
        if (viewButton !== undefined) {
            viewButton.setDisabled(buttondisabled);
        }
        if (deleteButton !== undefined) {
            deleteButton.setDisabled(buttondisabled);
        }
        if (printButton !== undefined) {
            printButton.setDisabled(buttondisabled);
        }
    }
}); 

