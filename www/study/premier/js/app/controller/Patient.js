/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Patient.js
 *
 * Patient view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.Patient', {
    extend : 'Ext.app.Controller',

    //views : ['Viewport'],

    refs : [{
        ref : 'center',
        selector : 'viewport > #center'
    }, {
        ref : 'patientview',
        selector : 'viewport > #center > #patientview'
    }, {
        ref : 'patientmenu',
        selector : 'viewport > #center > #patientview> #patientmenu'
    }, {
        ref : 'patientcenter',
        selector : 'viewport > #center > #patientview> #patientview-center'
    }],

    stores : ['Patients'],

    init : function() {"use strict";
        // Start listening for events on views
        this.control({
            'toolbar #patientmenu-btn-visits' : {
                click : this.onVisitsBtnClicked
            },
            'toolbar #patientmenu-btn-medication' : {
                click : this.onMedicationBtnClicked
            },
            'toolbar #patientmenu-btn-ae-sae' : {
                click : this.onAEBtnClicked
            },
            'toolbar #patientmenu-btn-medicalhistory' : {
                click : this.onMedicalHistoryBtnClicked
            },
            'toolbar #patientmenu-btn-overview' : {
                click : this.onOverviewBtnClicked
            },
            'toolbar #patientmenu-btn-sign' : {
                click : this.onSignBtnClicked
            }
        });
    },

    /*onLaunch : function() {

     },*/

    onVisitsBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms"))
        {
            var grid=Ext.getCmp('visitgrid') ,
				store = grid.getStore();
            store.clearFilter(true);
            store.filter([{
				filterFn: function(record, id){
					return (record.get('formtype').indexOf(grid.formtypeFilter) != -1 ? true : false);
				}
			}]);
			this.getPatientcenter().getLayout().setActiveItem('visitview');
		}
    },

    onMedicationBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms"))
        {
            var grid=Ext.getCmp('medicationgrid');
            var store = grid.getStore();
            store.clearFilter(true);
            store.filter([{
				filterFn: function(record, id){
					return (record.get('formtype').indexOf(grid.formtypeFilter) != -1 ? true : false);
				}
			}]);
			this.getPatientcenter().getLayout().setActiveItem('medicationview');
        }
    },

    onAEBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms"))
        {
            var grid=Ext.getCmp('aegrid');
            var store = grid.getStore();
            store.clearFilter(true);
            store.filter([{
				filterFn: function(record, id){
					return (record.get('formtype').indexOf(grid.formtypeFilter) != -1 ? true : false);
				}
			}]);
	
			if (store.sorters.getCount()===1) {
				store.sorters.add(new Ext.util.Sorter({
					 sorterFn : function(rec1, rec2) {
			            var result, rec1_startDate, rec1_parentId, rec1_iid, rec1_date, rec1_id, rec2_startDate, rec2_parentId, rec2_iid, rec2_date, rec2_id, realParentRecord;
			            var store = Ext.data.StoreManager.lookup('Forms');
			            
			            result = 0;
			            rec1_startDate=rec1.getFieldValue('startDate');
			            rec2_startDate=rec2.getFieldValue('startDate');
                        rec1_parentId = rec1.get('parentid');
			            rec2_parentId = rec2.get('parentid');
			            rec1_iid=rec1.get('instanceid');
			            rec2_iid=rec2.get('instanceid');
			            
			            if (rec1_parentId === null || rec1_parentId === undefined || rec1_parentId === 0 ) { // szülő
							rec1_date = rec1_startDate;
							rec1_id=parseInt(rec1_iid, 10);
						} else {// gyerek
							realParentRecord = store.getAt(store.find('instanceid', rec1_parentId));
							if (realParentRecord)  {
								rec1_date = realParentRecord.getFieldValue('startDate');
							} else {
								rec1_date = '';
							}
							
							rec1_id=parseInt(rec1_parentId, 10);
						}
						if (rec2_parentId === null || rec2_parentId === undefined || rec2_parentId === 0 ) { // szülő
							rec2_date = rec2_startDate;
							rec2_id=parseInt(rec2_iid, 10);
						} else {// gyerek
							realParentRecord = store.getAt(store.find('instanceid', rec2_parentId));
							if (realParentRecord)  {
								rec2_date = realParentRecord.getFieldValue('startDate');
							} else {
								rec2_date = '';
							}
							rec2_id=parseInt(rec2_parentId, 10);
						}
			            
			
						if (rec1_date < rec2_date) { // nem rokonok
							result = -1;
                        } else if 	(rec1_date > rec2_date ) {// nem rokonok
							result = 1; 
						} else { // uazon a napon startolt a parent
							if (rec2_iid == 0 &&  (rec2_parentId === null || rec2_parentId === undefined || rec2_parentId === 0 )) { // egynaposak, mégsem rokonok + új szülő felvitel (nincs iid)
								result = -1;
							} else if(rec1_iid == 0 &&  (rec1_parentId === null || rec1_parentId === undefined || rec1_parentId === 0 ) ) { // egynaposak, mégsem rokonok + új szülő felvitel (nincs még iid)
								result = 1;
							} else if (rec1_id < rec2_id) { // mégsem rokonok
								result = -1;
							} else if (rec1_id > rec2_id) { // mégsem rokonok
								result = 1;
							} else { // egy család
								if (rec1_parentId === null || rec1_parentId === undefined || rec1_parentId === 0 ) { // 1. a szülő
									result = -1; 
								} else if (rec2_parentId === null || rec2_parentId === undefined || rec2_parentId === 0 ) { // 2. a szülő
									result = 1;
								} else { // testvérek
									if (rec1_startDate < rec2_startDate)  {
										result = -1;
									} else if (	rec1_startDate > rec2_startDate)  {
										result = 1;
									} else {
										result = 0;
									}
								}	
							}
						}	
			
			            return result;
			        }
				}));
			}
			store.sort();
			this.getPatientcenter().getLayout().setActiveItem('aeview');
        }
    },

    onMedicalHistoryBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms"))
        {

            var grid=Ext.getCmp('medicalhistorygrid');
            var store = grid.getStore();
            store.clearFilter(true);
            store.filter([{
				filterFn: function(record, id){
					return (record.get('formtype').indexOf(grid.formtypeFilter) != -1 ? true : false);
				}
			}]);
			this.getPatientcenter().getLayout().setActiveItem('medicalhistoryview');
        }
    },

    onSignBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "signForms"))
        {
			var grid=Ext.getCmp('signaturegrid'),
				store = grid.getStore();
            store.clearFilter(true);
            store.filter([{
                filterFn: function(record, id){
					return (record.isFormValid() && !record.hasOpenOrAnsweredQuery() && !record.isSigned() ? true : false);
				}
			}]);
            
            this.getPatientcenter().getLayout().setActiveItem('signatureview');
        }
    },
    
	onOverviewBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms"))
        {
			var grid=Ext.getCmp('overviewgrid'),
				store = grid.getStore();
		//	console.log('overviewGrid store');console.log(store);
            store.clearFilter(false);
            this.getPatientcenter().getLayout().setActiveItem('overviewview');
        }
   }
});
