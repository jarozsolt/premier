/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Contacts.js
 *
 * Contacts view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.Contacts', {
    extend : 'Ext.app.Controller',

    refs : [{
        ref : 'ContactsView',
        selector : 'ContactsView'
    }, {
        ref : 'ContactsDataView',
        selector : 'ContactsDataView'
    }],

    stores : ['Contacts']

});
