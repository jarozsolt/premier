/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * PatientDialog.js
 *
 * PatientDialog's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
*/

Ext.define('Kron.controller.PatientDialog', {
    extend : 'Ext.app.Controller',
	requires : ['Kron.core.Alerts'],
    //views : ['Viewport'],

    refs : [{
        ref : 'patientView',
        selector : '#patientview'
    },{
        ref : 'patientdlg',
        selector : '#patientdlg'
    }, {
        ref : 'querydlg',
        selector : '#querydlg'
    }, {
        ref : 'queryForm',
        selector : '#queryform'
    }, {
        ref : 'medicationgrid',
        selector : '#medicationgrid'
    }, {
        ref : 'aegrid',
        selector : '#aegrid'
    }, {
        ref : 'medicalhistorygrid',
        selector : '#medicalhistorygrid'
    }, {
        ref : 'visitgrid',
        selector : '#visitgrid'
    }, {
        ref : 'signWindow',
        selector : '#signwindow'
    }, {
        ref : 'querygrid',
        selector : '#querygrid'
    }],

    stores : ['Forms', 'FieldHistories', 'FieldQueries', 'Patients'],

    init : function() {"use strict";
        // Start listening for events on views
        this.control({
            'patientdlg #dlgAccept' : {
                click : this.onDlgAcceptBtnClicked
            },
            'patientdlg #dlgCancel' : {
                click : this.onDlgCancelBtnClicked
            },
            'patientdlg #dlgClose' : {
                click : this.onDlgCloseBtnClicked
            },
            'patientdlg #dlgSign' : {
                click : this.onDlgSignBtnClicked
            },
            'patientdlg #dlgMonitorAll' : {
                click : this.onDlgMonitorAllBtnClicked
            },
            'patientdlg monitorcheck' : {
                click : this.onFieldMonitorCheckClicked
            },
            'patientdlg fielddetailsbtn' : {
                click : this.onFieldDetailsBtnClicked
            },
            'patientdlg' : {
                beforeclose: this.beforeCloseForm
            },
            'patientdlg form' : {
                fielderrorchange: this.onfielderrorchange
            },
            '#patientdlg field' : {
                change : {
                         fn: this.onFieldChange,
                         scope: this,
                         buffer: 3000
                 }
                  
            },
            '#addquerybtn' : {
                click : this.onAddQueryBtnClicked
            },
            '#addcommentbtn' : {
                click : this.onAddCommentBtnClicked
            },
            '#closequerybtn' : {
                click : this.onCloseQueryBtnBtnClicked
            },
            '#deletequerybtn' : {
                click : this.onDeleteQueryBtnClicked
            },
             '#querygrid' : {
                select : this.onQuerySelected,
                afterlayout : this.onQueryGridAfterLayout
            }
        });

    },

	onfielderrorchange: function ( form, field, errorMessage, eOpts ){"use strict";
		var panel = field.up('panel'),
			signButton = Ext.getCmp('dlgSign');    	
		//console.log('onfielderrorchange updatetabicon hívás', field);
		form.updateTabIcon(panel);

		// aláírógomb leszedése, ha kell
		if (signButton !== undefined && signButton !== null  && signButton.isVisible() ) {
		   	signButton.hide();
		}
	
	},
    /*onLaunch : function() {

    },*/
   /* 
   onAfterRender : function( ){"use strict";
   	console.log('onAfterRendervvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv');
   	var form = this.getPatientdlg().getLayout().getActiveItem();//.getForm();
   	console.log('form', form);
   	console.log('form items', form.items);
  	form.cascade(function(c){
                
            if (c.fdbutton && !c.isAligned ) {
            	console.log('fdbutton id', c.id);
            	if (c.getEl()) {
            		//console.log('target el', Ext.getCmp(c.targetId).getEl());
    			    //       c.getEl().alignTo(Ext.getCmp(c.targetId).getEl(), "tr?",[10,0]);
    			           c.getEl().anchorTo(Ext.getCmp(c.targetId).getEl(), "r?",[10,0]);
				/*	var xy = c.getEl().getAlignToXY(Ext.getCmp(c.targetId).getEl(), "r",[0,0]);
					console.log('Ext.getCmp(c.targetId).getEl()', Ext.getCmp(c.targetId).getEl(),'xy',xy);
					xy[0]=650;
					xy[1]=xy[1]-53;
					console.log('xy',xy);
					c.getEl().setXY(xy);*//*
					c.isAligned = true;
            	}

            }
        });
   },*/
      
    onQueryGridAfterLayout :function () {"use strict";
	    if (!this.getQuerygrid().getSelectionModel().hasSelection()) {
	        // select first query if exist
	        if (this.getQuerygrid().getStore().getCount() >= 1 ){
	            this.getQuerygrid().getSelectionModel().select(0);       
	            this.showQueryActionBtns(this.getQuerygrid().getStore().getAt(0), 0);
	        } else {
	        	this.hideQueryActionBtns();
	        }
	    }
        
    },

      
    onQuerySelected : function( rowModel, record, index, eOpts ) {"use strict";
        if (record.get('action') === Kron.queryAction.Query) {
            this.showQueryActionBtns(record, index);
        } else {
            this.hideQueryActionBtns();
        }
    },

    showQueryActionBtns : function(record, index){"use strict";
        var addCommentBtn = Ext.getCmp('addcommentbtn'),
            closeQueryBtn = Ext.getCmp('closequerybtn'),
            deleteQueryBtn = Ext.getCmp('deletequerybtn');
        var patientView=Ext.getCmp('patientview');
       	if (patientView!== undefined) {
       		var patientRecord = patientView.getPatientRecord()	
       	}
        if (patientRecord !== undefined ) { 
        	if ( patientRecord.get('isclosed')  ) {
        	 	var isPatientClosed = true;
            } 
        }   
        if ( !isPatientClosed && (Kron.core.UserManagement.hasPermission("Queries", "X_addQuery") || ( Kron.core.UserManagement.hasPermission("Queries", "X_addComment") && (record.get('status') === Kron.queryStatus.Reopened || record.get('status') === Kron.queryStatus.Open || record.get('status') === Kron.queryStatus.Answered ) )) ) {
            if (addCommentBtn.isHidden()){
                addCommentBtn.show();
            }
            addCommentBtn.selectedRowIndex = index;
        } else {
            if (addCommentBtn.isVisible()) {
                addCommentBtn.hide();
            }
            addCommentBtn.selectedRowIndex = null;
        }
        if ( !isPatientClosed && (Kron.core.UserManagement.hasPermission("Queries", "X_closeQuery") && record.get('status') !== Kron.queryStatus.Closed ) ) {
            if (closeQueryBtn.isHidden()){
                closeQueryBtn.show();
            }
            closeQueryBtn.selectedRowIndex = index;
        } else {
            if (closeQueryBtn.isVisible()) {
                closeQueryBtn.hide();
            }
            closeQueryBtn.selectedRowIndex = null;
        }
         if ( !isPatientClosed && Kron.core.UserManagement.hasPermission("Queries", "X_deleteQuery") ) {
            if (deleteQueryBtn.isHidden()){
                deleteQueryBtn.show();
            }
            deleteQueryBtn.selectedRowIndex = index;
        } else  {
           if (deleteQueryBtn.isVisible()) {
                deleteQueryBtn.hide();
            }
            deleteQueryBtn.selectedRowIndex = null; 
        } 
    },
    
    hideQueryActionBtns : function(){"use strict";
        var addCommentBtn = Ext.getCmp('addcommentbtn'),
            closeQueryBtn = Ext.getCmp('closequerybtn'),
            deleteQueryBtn = Ext.getCmp('deletequerybtn');
            
        if (addCommentBtn.isVisible()) {
            addCommentBtn.hide();
        }
        addCommentBtn.selectedRowIndex = null;
        if (closeQueryBtn.isVisible()) {
            closeQueryBtn.hide();
        }
        closeQueryBtn.selectedRowIndex = null;
        if (deleteQueryBtn.isVisible()) {
            deleteQueryBtn.hide();
        }
        deleteQueryBtn.selectedRowIndex = null;
    },
    
    onAddCommentBtnClicked : function(button, e, eOpts) {"use strict";
       // console.log('onAddCommentBtnClicked clicked');

        this.getQuerygrid().onAddQueryComment(null, button.selectedRowIndex, null);
    }, 
    
     
    onCloseQueryBtnBtnClicked : function(button, e, eOpts) {"use strict";
       // console.log('onCloseQueryBtnBtnClicked clicked');

        this.getQuerygrid().closeQuery(null, button.selectedRowIndex, null);
    },
    
    
    onDeleteQueryBtnClicked : function(button, e, eOpts) {"use strict";
       // console.log('onDeleteQueryBtnClicked clicked');

        this.getQuerygrid().onDeleteQuery(null, button.selectedRowIndex, null);
    },  

  
  
    
    onFieldMonitorCheckClicked :function(button, e, eOpts) {"use strict";
        var valuefield, fieldName='', panel, fieldDetailsBtn;
       /* if (button.referencedField !== undefined && button.referencedField!== null) {
        	//valuefield = Ext.getCmp(button.referencedField );
			panel = button.up('panel');
			if (panel !== undefined && panel !== null) {
				valuefield = panel.down('[name=' + button.referencedField + ']');
			} else {
				valuefield = button.up('form').down('[name=' + button.referencedField + ']');
			}
        }
        if (valuefield === undefined  || valuefield === null) {
       		valuefield = button.previousSibling();
       	}

        if (valuefield !== undefined && valuefield !== null) {
        	fieldName = valuefield.getName();
        }*/
       
       	fieldDetailsBtn = button.nextSibling('fielddetailsbtn');
       	if (fieldDetailsBtn!== undefined && fieldDetailsBtn !== null ) {
       		fieldName = fieldDetailsBtn.getFieldName();
       	}
        if (fieldName !='') {     
        	this.approveFields([fieldName]);
        }
    },
	    
	onFieldDetailsBtnClicked : function(button, e, eOpts) {"use strict";
			//console.log('onFieldDetailsBtnClicked', button, e, eOpts);
		var patientDlg = this.getPatientdlg(),
            queryGrid,
			Qform,
			record = patientDlg.getRecord(),
			formData,
			store,
			fieldStatusText = '',
			fieldStatus = '',
			me = this,
			historyLabel,
			queryDlg,addQueryBtn,closeQueryBtn,deleteQueryBtn,addCommentBtn,
			historyGrid, 
			panel,
			monitorbtn = button.previousSibling();

		
		var valuefield, fieldName='';

		fieldName = button.getFieldName();
		valuefield = button.getReferencedField();

		
	/*	if (button.referencedField !== undefined && button.referencedField !== null) {
			panel = button.up('panel');
			if (panel !== undefined && panel !== null) {
				valuefield = panel.down('[name=' + button.referencedField + ']');
			} else {
				valuefield = button.up('form').down('[name=' + button.referencedField + ']');
			}
		}

		
        if (valuefield === undefined  || valuefield === null) {
     	   valuefield = monitorbtn.previousSibling();       
	    }

        if (valuefield !== undefined && valuefield !== null) { 
        	fieldName = valuefield.getName();
        }
      */  
    //  console.log(' onFieldDetailsBtnClicked fieldName', fieldName);
  //    console.log(' onFieldDetailsBtnClicked valuefield', valuefield);

		if (fieldName !='') {
			fieldStatus = record.getFieldStatus(fieldName);
				
			queryDlg = Ext.getCmp('querydlg');
			if(!queryDlg) {
				queryDlg = Ext.create('Kron.view.patient.QueryWindow');
			}
		        
	        historyGrid = Ext.getCmp('fieldhistorygrid');
            if (historyGrid) {
                store = historyGrid.getStore();
                store.removeAll();
                store.proxy.setExtraParam('form_instanceid',record.get('instanceid'));
                store.proxy.setExtraParam('fieldname',fieldName);
                store.load();
            }   
                
			queryGrid = Ext.getCmp('querygrid');
			if (queryGrid) {
				store = queryGrid.getStore();
				store.clearFilter(true);
				store.filterBy(function(record, id){
					var r= false;
					if (record.get('fieldname') == fieldName ) {
						r= true;
					} 
					return r;
				});
				store.sort();
			}	
				
			queryDlg.setFieldStatusText(fieldStatus);
				
		
			/*	formData = {
		            status : fieldStatusText
		            };
		        Qform = this.getQueryForm();
		        if (Qform !== undefined) {
		            Qform.getForm().reset();
		            Qform.getForm().setValues(formData);
		        }*/
		/*        if (eOpts === 'H') {
		            queryGrid.up('tabpanel').setActiveTab(1);// history tab
		        } else {
		            queryGrid.up('tabpanel').setActiveTab(0);// queries tab
		        }*/
		    queryGrid.up('tabpanel').setActiveTab(0);// queries tab
		    
            queryDlg.setSelectedFieldNames([fieldName]); // @TODO  queryDlg.setSelectedFieldNames függvény inputját átírni tönbről stringre, mindig egy mezővel hivjuk!

           	if (valuefield.historyLabel !== undefined) {
           		historyLabel = valuefield.historyLabel;
            } else if(valuefield.fieldLabel !== undefined){
            	historyLabel = valuefield.fieldLabel;
            } else if(valuefield.boxLabel !== undefined){
            	historyLabel = valuefield.boxLabel;
            } else {
            	historyLabel ='';
            }		
            queryDlg.setHistoryLabel(historyLabel);
         //   queryDlg.setSelectedBtnContainer(button);
         //   queryDlg.setReferencedField(valuefield);
        //    queryDlg.setMonitorBtn(monitorbtn); 
            queryDlg.setFieldDetailsBtn(button);   
              //  if (eOpts !== 'A') {
            queryDlg.show(); 
            
            
			var patientView=Ext.getCmp('patientview');
	       	if (patientView!== undefined) {
	       		var patientRecord = patientView.getPatientRecord()	
	       	}
	        if (patientRecord !== undefined ) { 
	        	if ( patientRecord.get('isclosed')  ) {
	        	 	var isPatientClosed = true;
	            } 
	        }
            // show addQueryBtn 
            addQueryBtn = Ext.getCmp('addquerybtn');
            if (Kron.core.UserManagement.hasPermission("Queries", "X_addQuery") && !isPatientClosed) {                       
                if (addQueryBtn && addQueryBtn.isHidden()) {
                    addQueryBtn.show();
                }
            } else {
                if (addQueryBtn && addQueryBtn.isVisible()) {
                    addQueryBtn.hide();
                }
            }
          /*          closeQueryBtn = Ext.getCmp('closequerybtn');
                    if (Kron.core.UserManagement.hasPermission("Queries", "X_closeQuery") && (store.find('status', Kron.queryStatus.Open )>=0 || store.find('status', Kron.fieldStatus.Reopened )>=0 )) {                       
                        if (closeQueryBtn && closeQueryBtn.isHidden()) {
                            closeQueryBtn.show();
                        }
                    } else {
                        if (closeQueryBtn && closeQueryBtn.isVisible()) {
                            closeQueryBtn.hide();
                        }
                    }
                    
                    deleteQueryBtn = Ext.getCmp('deletequerybtn');
                    if (Kron.core.UserManagement.hasPermission("Queries", "X_deleteQuery") && store.count()>0 ) {                       
                        if (deleteQueryBtn && deleteQueryBtn.isHidden()) {
                            deleteQueryBtn.show();
                        }
                    } else {
                        if (deleteQueryBtn && deleteQueryBtn.isVisible()) {
                            deleteQueryBtn.hide();
                        }
                    }
                    
                    addCommentBtn = Ext.getCmp('addcommentbtn');
                    if (Kron.core.UserManagement.hasPermission("Queries", "X_addComment") && store.count()>0 ) {                       
                        if (addCommentBtn && addCommentBtn.isHidden()) {
                            addCommentBtn.show();
                        }
                    } else {
                        if (addCommentBtn && addCommentBtn.isVisible()) {
                            addCommentBtn.hide();
                        }
                    }*/
                   
   //             } else {
     //               this.onAddQueryBtnClicked();
       //         }

            
        }

    },
    

    
	
    onDlgMonitorAllBtnClicked : function(button, e, eOpts) {//"use strict";
        var fieldDetailsBtns, monitorBtn, fieldName, fieldNames,btnPanel,valuefield,
        dlg = this.getPatientdlg(),
        record = dlg.getRecord(),
        activeForm = dlg.getLayout().getActiveItem(),
        form = activeForm.getForm(),
        panel = activeForm.down('panel[tab]'),
        tabPanel = activeForm.down('tabpanel'),
        activeTab = tabPanel && tabPanel.getActiveTab(),
        monitorableFieldNames = [];
  
        if (activeTab) {
            fieldDetailsBtns = activeTab.query('fielddetailsbtn[disabled=false]');
        }
        else {
            fieldDetailsBtns = activeForm.query('fielddetailsbtn[disabled=false]');
        }
        Ext.each(fieldDetailsBtns, function(btn) {
            if (btn !== undefined) {
            	
            	fieldName = btn.getFieldName();
            	
            	monitorBtn = btn.getMonitorCheckBtn();
            	
               	if (fieldName !== undefined && fieldName !== null && fieldName !== '' && monitorBtn !== undefined && monitorBtn !== null ) {
                    if (record.isFieldMonitorable(fieldName) && monitorBtn.isDisabled() === false)
                    {
                        monitorableFieldNames = monitorableFieldNames.concat(fieldName);
                        
                        monitorBtn.toggle(true);
                    }
                }    
            }
        }, this);
        if (monitorableFieldNames.length>0) {
            this.approveFields(monitorableFieldNames);
        }
    },
    
    
    onDlgAcceptBtnClicked : function(button, e, eOpts) {"use strict";
        var activeForm, form,
        dlg = this.getPatientdlg(),
        record = dlg.getRecord();
        activeForm = dlg.getLayout().getActiveItem();
        form = activeForm.getForm();
        // form mentés előtti warningok kiírása
        activeForm.alertBeforeSave();
        
        // ellenőrzön, hogy lehet-e menteni
        if (!activeForm.canSave())
        {
            activeForm.alertSaveError();
        }
        else
        {
            if (form.isValid()) {
                this.saveForm();
            } else {
            /*	form.getFields().each(function(item){
            		if (!item.isValid() ) {
            			console.log('Nem valid mező: ', item.name, item);
            		}
            	});*/
                Ext.MessageBox.prompt({
                    title : _("Figyelmeztetés"),
                    // TODO: lefordítani
                    msg :  _("Az adatlap kitöltése nem megfelelő, ezért nem lehet aláírni.") + '<br>' + _("Mindenképp el akarja így menteni?"),
                    buttons : Ext.Msg.OKCANCEL,
                    buttonText: {ok: _("Mindenképp Ment"), cancel: _("Mégsem")},
                    icon : Ext.MessageBox.WARNING,
                    animateTarget: 'dlgAccept',
                    scope : this,
                    fn : function(btn, ev) {
                        if (btn === 'ok') {
                            this.saveForm();
                        }
                    }
                });
            }
        }

        
    },
    
	onFieldChange: function(field, newValue, oldValue) {"use strict";
		//console.log('onFieldChange called');

		var patientDlg = this.getPatientdlg(), 
			record = patientDlg.getRecord(),
			activeForm = field.up('form'),
			fieldValues,intervalMinute,
			userPrefsView = Ext.getCmp('userprefs');

		if ( activeForm !== undefined && activeForm !== null ) {
			// autosave temp field mentés

	        if (!patientDlg.isDialogReadOnly() && patientDlg.getAutosaveEnabled() && userPrefsView.isAutosaveEnabled() && Kron.core.UserManagement.hasPermission("Forms", "createForm")) {// biztos, ami biztos....) { // ha read only, nem csinálok semmit
		        // összeszedem a field értékeket
		        fieldValues= activeForm.getFormFieldValues(true);
			//	console.log('formból kiszedett fieldValues');console.log(fieldValues);
				record.saveTempValues(fieldValues);
				// ha 0 az autosave_frequency akkor rögtön sync-elek is (ilyenkor nem indul el a timer)
	            intervalMinute = userPrefsView.getAutosaveFrequency();
	            if (intervalMinute !== undefined && intervalMinute == 0) {
	                 record.formfields().sync({
	                    failure: function() { Kron.core.Alerts.connectionErrorShort();
	                    }
	                });
	            }
	        }
	    }
	},
	
	saveForm : function() {"use strict";
       //console.log(' saveform called');
       if (Kron.core.UserManagement.hasPermission("Forms", "createForm")) {// biztos, ami biztos....
           var activeForm, form, fields, grid, 
    			patientDlg = this.getPatientdlg(), 
    			record = patientDlg.getRecord(),
    			fieldStore = record.getFieldStore(),
    			formsStore = this.getFormsStore(),
    			iid,
    			syncDone = false,
    			me = this,
    			parentName = null,
    			fieldValues,
    			visitRecords,
    			visit_status = '',
    			newDueDate = new Date(),
    			offset, visitDate, dueDate, studyCourseValue, patientNewStatus = '',
    			n, patientRecord;
    		
    		Ext.suspendLayouts();
    		patientDlg.setAutosaveEnabled(false);
            patientDlg.stopAutosaveTimer();
    	//	console.log('record.formfields()');console.log(record.formfields());
         //   if (!record.isDeleted()) {
			activeForm = patientDlg.getLayout().getActiveItem();
			if (!activeForm.canSave()) {
                activeForm.alertSaveError();
                return;
            }
			form = activeForm.getForm();

			if ((form.isDirty() || activeForm.isFormDirty ) && patientDlg.isDlgReadOnly === false) {
		        // összeszedem a field értékeket
		        fieldValues= activeForm.getFormFieldValues();
			//	console.log('formból kiszedett fieldValues');console.log(fieldValues);
	
				// törlöm a temp tételeket
				record.cancelTempItems();  
				
	////////////// innen vannak az egyedi form field érték beállítások kezelése  ////////////////////////
			      
				// Ha vizit1-en más visitDate-et állít be, akkor módosítanom kell a többi vizit duedate-jét
				
				if (activeForm.getId() === 'Visit1Form' && fieldValues.visitDate !== undefined && !Ext.Date.isEqual(fieldValues.visitDate, record.get('duedate'))) {
					visitRecords = formsStore.queryBy(function(record) {
						return (record.get('formtype').indexOf('Visit') != -1  && record.get('duedate') !== null ? true : false);
					});
					visitRecords.sort('duedate', 'ASC');
					visitDate = fieldValues.visitDate;
					dueDate = record.get('duedate');
					if (visitDate !== undefined && visitDate !== null &&
					    dueDate !== undefined && dueDate !== null) {
	    				// itt nme kell a cleratime, mert egy napon belül is akarok soorendet tartani ( visit3/end of study )
	    				//offset = Ext.Date.clearTime(visitDate).getTime() - Ext.Date.clearTime(dueDate).getTime();
	    				offset = visitDate.getTime() - dueDate.getTime();
	    				visitRecords.each(function(rec){
	    					newDueDate.setTime( rec.get('duedate').getTime() + offset);
	    					rec.set('duedate', Ext.Date.clone(newDueDate));                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
	    				});
	                }
				
				}
				// Ha az EOS -en a studyCourse értéke alapjűn kell beállítani a beteg státusát:
				//   1 - Kron.patientStatus.Completed
				//   2 vagy 3 - Kron.patientStatus.Dropout
				//   ilyen a premierben nincs: 4 - Kron.patientStatus.Screenfailure
				if (activeForm.getId() === 'Visit4Form'){
				    studyCourseValue = fieldValues.studyCourse;
				    if ( studyCourseValue !== undefined) { 
	                    if ( studyCourseValue == 1) {
	                       patientNewStatus = Kron.patientStatus.Completed;
	                    } else if ( studyCourseValue == 2 || studyCourseValue == 3) {
	                       patientNewStatus = Kron.patientStatus.Dropout; 
	              //      }  else if (studyCourseValue == 4) {
	              //         patientNewStatus = Kron.patientStatus.Screenfailure;  
	                    }
	                    
	                    n = this.getPatientsStore().find('patientid', this.getPatientView().getPatient());
	                    if (n>=0) {
	                        patientRecord = this.getPatientsStore().getAt(n);
	                        if (patientRecord && patientNewStatus !== '') {
	                            patientRecord.set('status', patientNewStatus );
	                            this.getPatientsStore().sync({
	                                failure: function() { Kron.core.Alerts.connectionError();
	                                }
	                            });
	                        }
	                        // ha idő előtt lezárjuk a beteget  és van jövőbe esedékes  empty visit, azoknak a duedate-jét ki kell nullázni, hogy ne pirosodjanak ki
	                        if ( (patientRecord.get('status') === Kron.patientStatus.Dropout || patientRecord.get('status') === Kron.patientStatus.Screenfailure ) && fieldValues.visitDate !== undefined ) {
	                            visitRecords = formsStore.queryBy(function(record) {
	                                return (record.get('formtype').indexOf('Visit') != -1  ? true : false);
	                            });
	                               if ( Ext.isDate(fieldValues.visitDate)) {
	                                visitRecords.each(function(rec){
	                                    if ((rec.get('status') === Kron.visitStatus.Empty || rec.get('status') === Kron.visitStatus.Missing) && Ext.isDate(rec.get('duedate')) &&  Ext.Date.clearTime(rec.get('duedate')).getTime() >= Ext.Date.clearTime(fieldValues.visitDate).getTime()) {
	                                        rec.set('status',  Kron.visitStatus.Cancelled);
	                                    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
	                                });
	                            }
	                        }
	
				        }
				        
				    }
				}
				
				
	////////////// idáig vannak az egyedi form field érték beállítások kezelése  ////////////////////////
				// elmentem az újakat (ha törölni kell, akozat is kitörli a store-ból)
				record.saveFinalValues(fieldValues);
	             
		 //       console.log('record formfields'); console.log(record.formfields().getRange());
				
				if (form.isValid()) {
	                record.setFormValid(true);
	            } else {
	                record.setFormValid(false);
	            }
				
				record.setTmp(false);
				if (!record.isStored()) {
	                record.set('status', 'S');               
				}
				if (record.isSigned()) {
	                record.set('signed_by', null);
	                record.set('signed', null);
				}
				
				// update patient record visit status
				if(record.get('formtype').indexOf('Visit')!= -1) {
					visitRecords = formsStore.queryBy(function(record) {
						return (record.get('formtype').indexOf('Visit') != -1 ? true : false);
					});
					visitRecords.sort('duedate', 'ASC');
					visitRecords.each(function(record){
						if (record.isFormValid()) {
							visit_status = visit_status + Kron.visitStatus.Completed;
						} else {
							visit_status = visit_status + record.get('status');
						}
						
					});
				//	this.getPatientView().getPatientRecord().set('visit_status', visit_status);
				}
				// frissítem a patientQueries grid tartalmát is - törlöm ha kell a rekordott belőle 
				 
	            if (activeForm.getId() === 'AdverseEventForm') {
	                grid = this.getAegrid();
	
	            } else if (activeForm.getId() === 'MedicationForm') {
	                grid = this.getMedicationgrid();
	            } else if (activeForm.getId() === 'MedicalHistoryForm') {
	                grid = this.getMedicalhistorygrid();
	            } else {// visits + unscheduled visits
	                grid = this.getVisitgrid();
	            }
	
	            // select the newly created record via view
	            grid.getView().select(record);
	
	
				
	
	            formsStore.sync({
	                failure: function() { Kron.core.Alerts.connectionErrorFormSave();
	                }
	            });
	            fieldStore.sync({
	                success: function() {
	                    // a lenti azért kell, hogy újratöltödjenek majd a store-ok, ha kell
	                    var todoView = Ext.getCmp('todoview');
	                    if (todoView) {
	                        todoView.setStoreDirty(true);
	                    } 
	                    var patientsView = Ext.getCmp('patientsview');
	                    if (patientsView) {
	                        patientsView.setStoreDirty(true);
	                    } 
	                    
	                },
	                failure: function() {
	                   Kron.core.Alerts.connectionErrorFormSave();
	                },
	                scope: this
	            });
				
	            this.updateOpenQueryStatuses(record);
				
	            
	            
	        }
	       	Ext.resumeLayouts(true);  
	    }
		this.hideForm();
    },

	cancelFormChanges :  function() {"use strict";
  //      console.log('cancelFormChanges called');
        var patientDlg = this.getPatientdlg(), 
			record = patientDlg.getRecord(),
			fieldStore = record.getFieldStore(),
			formsStore = this.getFormsStore(),
			activeForm = patientDlg.getLayout().getActiveItem(),
			form = activeForm.getForm(),
			fields =form.getFields();


		// törlöm a temp tételeket
		record.cancelTempItems();  
		fieldStore.sync({
            failure: function() { Kron.core.Alerts.connectionError();
            }
        });
        
		// ha még a form rekord is temp volt - azt is törlöm
		if (record.isNew()) {
			formsStore.remove(record);
			formsStore.sync({
                failure: function() { Kron.core.Alerts.connectionError();
                }
            });
		}    

		this.hideForm();
	// megnyitasnal törlöm is
	//	this.resetForm();     
	},


    updateOpenQueryStatuses : function(record) {
    //	console.log('updateOpenQueryStatuses called');
        var fieldName, index, field, queryStatus,
            fieldQueriesStore = Ext.data.StoreManager.lookup('FieldQueries'),
            queryRecordsToUpdate = [],
            fieldRecordsToUpdate = []; 
        if (fieldQueriesStore) {
        	fieldQueriesStore.clearFilter(true);
            fieldQueriesStore.each(function(queryRec){
                if ( queryRec.get('action') == Kron.queryAction.Query && ( queryRec.get('status') == Kron.queryStatus.Open || queryRec.get('status') == Kron.queryStatus.Reopened)) {                    
                    fieldName = queryRec.get('fieldname');
                    index =  record.formfields().findBy(function(rec) {
                        return ((rec.get('fieldname') == fieldName && rec.get('istmp') == 0 ) ? true: false);
                    });
                    // ha megvan a rekord, akkor megnézem változott-e. Ha igen, módosítot a query státusát
                    if  (index !== undefined && index >= 0 ) {
                        field = record.formfields().getAt(index);
                        if (field !== undefined) {
                            if ( field.get('fieldvalue') != queryRec.get('fieldvalue') || field.get('fieldtype') != queryRec.get('fieldtype')) {
                                
                                queryRecordsToUpdate.push(queryRec);
                                fieldRecordsToUpdate.push(field);
                                 
                             }
                        }
                    } else {
                    	// nincs már ilyen rekord - azaz törölve lett - ilyenkor akkor kell a query státusát állítani, ha abban volt érték
                    	// ha eleve üres mezőre ment a query, akkor nem állítok státust
                    	if ( queryRec.get('fieldvalue') !== '' &&  queryRec.get('fieldvalue') !== undefined &&  queryRec.get('fieldvalue') !== null) {
                               // console.log(' üres field mező query volt, query értéke:', queryRec.get('fieldvalue') );
                                queryRecordsToUpdate.push(queryRec);
                             //   fieldRecordsToUpdate.push(field);
                                 
                         }
                    	
                    }
                 }
            }); 
            
           // console.log('queryRecordsToUpdate',queryRecordsToUpdate);
            //console.log('fieldRecordsToUpdate',fieldRecordsToUpdate);
            
            queryRecordsToUpdate.forEach(function(queryRec){
            	queryRec.set('status', Kron.queryStatus.Answered);
            });
            fieldRecordsToUpdate.forEach(function(field){
            	queryStatus = '';
                fieldQueriesStore.each(function(queryItem){
                    if ( queryItem.get('fieldname') == field.get('fieldname')) {
                        queryStatus = queryStatus + queryItem.get('status');
                    }
                }); 
                field.set('querystatus',queryStatus);
            });
            
            fieldQueriesStore.sync({
                success: function() {
                    // a lenti azért kell, hogy újratöltödjenek majd a store-ok, ha kell
                    var pGrid, queriesView = Ext.getCmp('queriesview');
                    if (queriesView) {
                        queriesView.setStoreDirty(true);
                        // ha aktív  a patientsqueries, frissítem azt is
                        pGrid=queriesView.getLayout().getActiveItem();
                        if ( pGrid && pGrid.itemId === 'PatientQueries' ){
                            pGrid.getStore().load();
                        }
                    }
                },
                failure: function() {
                     Kron.core.Alerts.connectionError();
                },
                scope: this
            });
        }
    },

    hideForm : function() {"use strict";
        var patientDlg=this.getPatientdlg(), grid;
        if(patientDlg) {
			patientDlg.hide();
			grid=Ext.getCmp('patientview-center').getLayout().getActiveItem().down('grid');
			grid.getView().refresh();
        }
        if (this.getQuerydlg()){
            this.getQuerydlg().hide();
        }
    },
    
    resetForm : function() {"use strict";
        var patientDlg = this.getPatientdlg(),
        activeForm = patientDlg.getLayout().getActiveItem();
      //  console.time('resetForm');
        if (activeForm ) {
        	
        	Ext.suspendLayouts(); 
            activeForm.getForm().getFields().each(function(f){
                if (f.xtype !== 'displayfield' && !f.doNotSave) {
                    if (f.xtype !== 'radiofield') {
                        f.setValue(null);
                    } else {
                        f.setValue(false); 
                    }
                }
            });
            Ext.resumeLayouts(true);
        }
      //  console.timeEnd('resetForm');
    },
    
    beforeCloseForm : function() {"use strict";
        var activeForm, form, dlg = this.getPatientdlg(), record = dlg.getRecord(), tabPanels, l, tabPanel, panels;
	

        if (!record.isDeleted() && dlg.getLayout().getActiveItem().id !== 'formfieldsgrid') {
       		activeForm = dlg.getLayout().getActiveItem();	
            form = activeForm.getForm();
 
            
            if (form.isDirty() || activeForm.isFormDirty ) {
                Ext.MessageBox.prompt({
                    title : _("Néhány mező megváltozott"),
                    msg : _("El akarja menteni a változásokat?"),
                    width : 300,
                    buttons : Ext.Msg.YESNOCANCEL,
                    buttonText: {yes: _("Mentés"), no: _("Nem Ment"), cancel: _("Mégsem")},
                    icon : Ext.MessageBox.QUESTION,
                    scope : this,
                    fn : function(btn, ev) {
                        if (btn === 'yes') {
                            this.onDlgAcceptBtnClicked();
                            return false;
                        }
                        if (btn === 'no') {
							this.cancelFormChanges();
							dlg.stopAutosaveTimer();
                            return true;
                        }
                        // Cancel
                        return false;
                    }
                });
                return false;
            }
            this.cancelFormChanges();
        }
        dlg.stopAutosaveTimer();
        return true;
    },


    onDlgCancelBtnClicked : function(button, e, eOpts) {"use strict";
        this.beforeCloseForm();
    },
    
    onDlgCloseBtnClicked : function(button, e, eOpts) {"use strict";
        
        this.hideForm();
		// megnyitasnal törlöm...
      //  this.resetForm();       
    },

    
    onDlgSignBtnClicked : function(button, e, eOpts) {"use strict";
        var dlg = this.getPatientdlg(),
            record = dlg.getRecord(),
            win = this.getSignWindow(),
        	activeForm = dlg.getLayout().getActiveItem(),        	
        	form = activeForm.getForm();
        	
        if (form.isValid()) {
	        if (!win) {
	            win = Ext.create('Kron.view.patient.SignWindow');
	        }
	
	     	win.setFormids([record.get('instanceid')]);
	        win.show();
	   	}
   },
   
   // @Todo - kell ez még??
   onAddQueryBtnClicked : function(button, e, eOpts) {"use strict";
        Ext.Msg.show({
            title: _("Query hozzáadása"),
            msg: _("Kérem írja be a query szövegét:"),
            width: 400,
            buttons: Ext.Msg.OKCANCEL,
            multiline: true,
            fn: function(btn, text){
                    if (btn === 'ok'){
                        var grid = Ext.getCmp('querygrid');
                        grid.addQuery(text);
                    }
                },
            scope: this,
            icon: Ext.window.MessageBox.INFO
        });
    },     
	
	approveFields : function(fieldNames) {"use strict";
		var dlg = Ext.getCmp('patientdlg'),
			record = dlg.getRecord(),
			params = {
		            	'form_instanceid' : record.get('instanceid'),
		            	'fieldnames' : fieldNames
		            };
		//console.log('approveFields', params);
        Kron.Queries.approveField(params, function(provider, response) {
            if (response === undefined || response.result === undefined || response.result.data === undefined || response.result.success === undefined || response.status === false) {// no response from the server
                //console.log("approveField: Connection Error");
                 Kron.core.Alerts.connectionError();
            } else if (response.result.success){
				var patientDlg=Ext.getCmp('patientdlg'),
					queryDlg= Ext.getCmp('querydlg'),
					queryGrid = Ext.getCmp('querygrid'),
				//	approveBtn = Ext.getCmp('approveFieldBtn'),
					newStatus;
 			//console.log('approveFields result', response.result);
                newStatus=response.result.data.newstatus;
                record.setGroupStatus(fieldNames,newStatus);

                if (queryDlg!== undefined && queryDlg.isVisible()) {
					queryDlg.setFieldStatusText(newStatus);

				}
				if (queryGrid) {
	                queryGrid.reloadFieldHistoryStore();			    
				}
             } else {
            	// @TODO: másképp megoldani a hibaüzenetet
            	console.log ("approveField: not succeed!");
            }
        });
 	} 

});
