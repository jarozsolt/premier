/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * AdverseEvents.js
 *
 * Adverse Event view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.AdverseEvents', {
	extend : 'Ext.app.Controller',
	
	requires : ['Kron.view.printer.Printer','Kron.core.Alerts'],
	//views : ['Viewport'],

	refs : [{
		ref : 'center',
		selector : 'viewport > #center'
	}, {
		ref : 'patientview',
		selector : 'viewport > #center > #patientview'
	}, {
		ref : 'patientmenu',
		selector : 'viewport > #center > #patientview> #patientmenu'
	}, {
		ref : 'patientcenter',
		selector : 'viewport > #center > #patientview> #patientview-center'
	}, {
		ref : 'patientdlg',
		selector : '#patientdlg'
	}, {
		ref : 'aedlg',
		selector : '#aedlg'
	}, {
		ref : 'aegrid',
		selector : '#aegrid'
	}, {
		ref : 'aeform',
		selector : '#AdverseEventForm'
	}, {
		ref : 'aedeletebutton',
		selector : 'toolbar #aetoolbar-btn-delete'
	}, {
		ref : 'aeeditbutton',
		selector : 'toolbar #aetoolbar-btn-edit'
	}, {
        ref : 'aeviewbutton',
        selector : 'toolbar #aetoolbar-btn-view'
    }, {
		ref : 'aefollowupbutton',
		selector : 'toolbar #aetoolbar-btn-followup'
    }, {
		ref : 'maccefollowupbutton',
		selector : 'toolbar #aetoolbar-btn-maccefollowup'
	}, {
        ref : 'aeprintbutton',
        selector : 'toolbar #aetoolbar-btn-print'
    }, {
		ref : 'queryForm',
		selector : '#queryform'
	}, {
		ref : 'querydlg',
		selector : '#querydlg'
	}],

	stores : ['Forms'],

	init : function() {
	    "use strict";
		// Start listening for events on views
		this.control({
			'aegrid' : {
				itemdblclick : this.onAdverseEventDblClick,
				selectionchange : this.onAdverseEventRowSelected
			},
			'toolbar #aetoolbar-btn-new' : {
				click : this.onNewAdverseEventBtnClicked
			},
			'toolbar #aetoolbar-btn-new-macce' : {
				click : this.onNewMacceBtnClicked
			},
			'toolbar #aetoolbar-btn-edit' : {
				click : this.onEditAdverseEventBtnClicked
			},
            'toolbar #aetoolbar-btn-view' : {
                click : this.onViewAdverseEventBtnClicked
            },
			'toolbar #aetoolbar-btn-followup' : {
				click : this.onFollowUpBtnClicked
            },
			'toolbar #aetoolbar-btn-maccefollowup' : {
				click : this.onMacceFollowUpBtnClicked
			},
			'toolbar #aetoolbar-btn-delete' : {
				click : this.onDeleteAdverseEventBtnClicked
			},
            'toolbar #aetoolbar-btn-print' : {
                click : this.onPrintAdverseEventBtnClicked
            }
		});
	},

    onNewAdverseEventBtnClicked : function(button, e, eOpts) { "use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "createForm"))
        {
            var patientDlg = this.getPatientdlg(),
                store = this.getFormsStore(),
				record = Ext.create('Kron.model.Form',
		            {
		                formtype : 'AdverseEvent',
		                patientid : this.getPatientview().getPatient(),
		                status : 'N',
		                istmp : 1
		            }),
				newRecords;

			newRecords=store.add(record); // tömböt ad vissza!!!!
			store.sync({
                failure: function() { Kron.core.Alerts.connectionError();
                }
            });
			
			patientDlg.setRecord(newRecords[0], '', false);
       }
	},

	onNewMacceBtnClicked : function(button, e, eOpts) { "use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "createForm"))
        {
            var patientDlg = this.getPatientdlg(),
                store = this.getFormsStore(),
				record = Ext.create('Kron.model.Form',
		            {
		                formtype : 'AdverseEventMACCE',
		                patientid : this.getPatientview().getPatient(),
		                status : 'N',
		                istmp : 1
		            }),
				newRecords;

			newRecords=store.add(record); // tömböt ad vissza!!!!
			store.sync({
                failure: function() { Kron.core.Alerts.connectionError();
                }
            });
			
			patientDlg.setRecord(newRecords[0], '', false);
        }
	},

	onEditAdverseEvent : function(record) {"use strict";
		if (record.get('status') !== 'D') {// not deleted record
			var patientDlg = this.getPatientdlg();
            // Change the title of the dialog
			if (record.get('parentid') !== record.get('instanceid')) {
				patientDlg.setTitle(_("Follow-Up"));
				patientDlg.setIconCls('icon-followup-dlg_small');
			} else if(record.getFieldValue('ae_serious') === true) {
				patientDlg.setTitle(_("Súlyos Nemkívánatos Esemény (SAE)"));
				patientDlg.setIconCls('icon-adverseevent-dlg_small');
			} else {
				patientDlg.setTitle(_("Nemkívánatos Esemény (AE)"));
				patientDlg.setIconCls('icon-adverseevent-dlg_small');
			}
			patientDlg.setRecord(record, '', false);


		}

	},

    onViewAdverseEvent : function(record) {"use strict";
        if (record.get('status') !== 'D') {// not deleted record
            var patientDlg = this.getPatientdlg();
            // Change the title of the dialog
           if (record.get('parentid') !== record.get('instanceid')) {
                patientDlg.setTitle(_("Utánkövetés"));
                patientDlg.setIconCls('icon-followup-dlg_small');
            } else if(record.getFieldValue('ae_serious') === true) {
                patientDlg.setTitle(_("Súlyos Nemkívánatos Esemény (SAE)"));
                patientDlg.setIconCls('icon-adverseevent-dlg_small');
            } else {
                patientDlg.setTitle(_("Nemkívánatos Esemény (AE)"));
                patientDlg.setIconCls('icon-adverseevent-dlg_small');
            }
            patientDlg.setRecord(record,'', true, ['monitorcheck'] );

        }
    },
    

    onDeleteAdverseEventBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "deleteForm")) {
            var me= this, store, aeGrid, record, idx;
            aeGrid = this.getAegrid();
            if (aeGrid.getSelectionModel().hasSelection()) {
                record = aeGrid.getSelectionModel().getLastSelected();
				Ext.Msg.show({
                    title : _("Megerősítés"),
                    msg : record.getDeleteAEQuestion(),
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.WARNING,
                    scope : this,
                    width : 400,
                    fn : function(btn, ev) {
                        if (btn === 'yes') {
                            store = me.getFormsStore();
                            store.remove(record);
                            store.sync({
                                failure: function() { Kron.core.Alerts.connectionError();
                                }
                            });
                        }
                    }
                });
            }
        }
    },


    onPrintAdverseEventBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "printForm"))
        {
            var aeGrid, aeRecord, selectionModel, formId;
            aeGrid = this.getAegrid();
            if (aeGrid !== undefined && aeGrid !== null) {
                selectionModel = aeGrid.getSelectionModel();
                if (selectionModel !== undefined && selectionModel !== null) {
                    if (selectionModel.hasSelection()) {
                        aeRecord = selectionModel.getLastSelected();
                        if (aeRecord !== undefined && aeRecord !== null) {
                            formId = aeRecord.get('instanceid');
                            if (formId > 0)
                            {
                                Kron.view.printer.Printer.forcePDFSaveAs = true;
                                Kron.view.printer.Printer.printFormToPDF(formId);
                            }
                        }
                    }
                }
            }
        }
    },

    onEditAdverseEventBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "updateForm")) {
            var aeGrid, record;
            aeGrid = this.getAegrid();
            if (aeGrid.getSelectionModel().hasSelection()) {
                record = aeGrid.getSelectionModel().getLastSelected();
                this.onEditAdverseEvent(record);
            }
        }
    },

    onViewAdverseEventBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms")) {
            var aeGrid, record;
            aeGrid = this.getAegrid();
            if (aeGrid.getSelectionModel().hasSelection()) {
                record = aeGrid.getSelectionModel().getLastSelected();
                this.onViewAdverseEvent(record);
            }
        }
    },

    // Add Follow-Up button pressed on the toolbar
    onFollowUpBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms")) {
            var title, aeGrid, parentRecord, parentId, parentParentId, patientDlg, realParentRecord, parentTitle, record, queryDlg, newRecords, store;
            aeGrid = this.getAegrid();
            if (aeGrid.getSelectionModel().hasSelection()) {
                parentRecord = aeGrid.getSelectionModel().getLastSelected();
                parentId = parentRecord.get('instanceid');
                parentParentId = parentRecord.get('parentid');
                if (parentParentId > 0 ) {// click was on followup row
                    parentId = parentParentId;
                }

                // Temporary hack for presentation
                realParentRecord = parentRecord;
                if (parentId !== null && parentId !== '') {
                    patientDlg = this.getPatientdlg();
                    //realParentRecord = aeGrid.store.getAt(aeGrid.store.find('instanceid', parentId));
              //      if (realParentRecord !== undefined && realParentRecord !== null) {
                  /*      parentTitle = realParentRecord.getFieldValue('diagnosis');
                        title = Ext.String.format(_("Új {0} Follow-Up"), parentTitle);
                        patientDlg.setTitle(title);
                        patientDlg.setIconCls('icon-followup-dlg_small');
                        patientDlg.width = 600;
                        patientDlg.minWidth = 600;*/
						store = this.getFormsStore();
                        record = Ext.create('Kron.model.Form', {
                            formtype : 'AdverseEvent',
			                patientid : this.getPatientview().getPatient(),
			                status : 'N',
			                istmp : 1,
			                parentid : parentId
                        });

                        newRecords=store.add(record); // tömböt ad vissza!!!!
						store.sync({
                            failure: function() { Kron.core.Alerts.connectionError();
                            }
                        });
						
						patientDlg.setRecord(newRecords[0], '', false);
                //    }
                }
            }
        }
    },
    
	onMacceFollowUpBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms")) {
            var title, aeGrid, parentRecord, parentId, parentParentId, patientDlg, realParentRecord, parentTitle, record, queryDlg, newRecords, store;
            aeGrid = this.getAegrid();
            if (aeGrid.getSelectionModel().hasSelection()) {
                parentRecord = aeGrid.getSelectionModel().getLastSelected();
                parentId = parentRecord.get('instanceid');
                parentParentId = parentRecord.get('parentid');
                if (parentParentId > 0 ) {// click was on followup row
                    parentId = parentParentId;
                }

                // Temporary hack for presentation
                realParentRecord = parentRecord;
                if (parentId !== null && parentId !== '') {
                    patientDlg = this.getPatientdlg();
                    //realParentRecord = aeGrid.store.getAt(aeGrid.store.find('instanceid', parentId));
                 //   if (realParentRecord !== undefined && realParentRecord !== null) {
                  /*      parentTitle = realParentRecord.getFieldValue('diagnosis');
                        title = Ext.String.format(_("Új {0} Follow-Up"), parentTitle);
                        patientDlg.setTitle(title);
                        patientDlg.setIconCls('icon-followup-dlg_small');
                        patientDlg.width = 600;
                        patientDlg.minWidth = 600;*/
						store = this.getFormsStore();
                        record = Ext.create('Kron.model.Form', {
                            formtype : 'AdverseEventMACCE',
			                patientid : this.getPatientview().getPatient(),
			                status : 'N',
			                istmp : 1,
			                parentid : parentId
                        });

                        newRecords=store.add(record); // tömböt ad vissza!!!!
						store.sync({
                            failure: function() { Kron.core.Alerts.connectionError();
                            }
                        });
						
						patientDlg.setRecord(newRecords[0], '', false);
                   // }
                }
            }
        }
    },

	onAdverseEventDblClick : function(grid, record) { "use strict";
	    var aeGrid, selectedRecord, permissionClass;
		aeGrid = this.getAegrid();
		if (aeGrid.getSelectionModel().hasSelection()) {
			selectedRecord = aeGrid.getSelectionModel().getLastSelected();
			permissionClass = "Forms";
            if (Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
            {
                this.onEditAdverseEvent(record);
            }
            else if (Kron.core.UserManagement.hasPermission(permissionClass, "getForms"))
            {
                this.onViewAdverseEvent(record);
            }
		}
	},

	// Change the state of the buttons on the row selection
	onAdverseEventRowSelected : function(grid, record, options) {"use strict";
		var aeRecord, selectionModel,
        aeGrid = this.getAegrid(),
        editButton = this.getAeeditbutton(),
        followupButton = this.getAefollowupbutton(),
        macceFollowupButton = this.getMaccefollowupbutton(),
        viewButton = this.getAeviewbutton(),
        deleteButton = this.getAedeletebutton(),
        printButton = this.getAeprintbutton(),
        buttondisabled = true;
        if (aeGrid !== undefined && aeGrid !== null) {
            selectionModel = aeGrid.getSelectionModel();
            if (selectionModel !== undefined && selectionModel !== null) {
                // Van kiválasztott sor?
                if (selectionModel.hasSelection()) {
                    aeRecord = selectionModel.getLastSelected();
                    // Ha van kiválasztva sor engedélyezzük az edit, view, print és delete gombokat
                    if (aeRecord !== undefined && aeRecord !== null) {
                        buttondisabled = false;
                    }
                }
            }
        }
        // Nincs kiválasztva sor, letíltjuk az edit, followup, view és delete gombokat
        if (editButton !== undefined) {
            editButton.setDisabled(buttondisabled);
        }
        if (followupButton !== undefined) {
            followupButton.setDisabled(buttondisabled);
        }
        if (macceFollowupButton !== undefined) {
            macceFollowupButton.setDisabled(buttondisabled);
        }
        if (viewButton !== undefined) {
            viewButton.setDisabled(buttondisabled);
        }
        if (deleteButton !== undefined) {
            deleteButton.setDisabled(buttondisabled);
        }
        if (printButton !== undefined) {
            printButton.setDisabled(buttondisabled);
        }
	}
});
