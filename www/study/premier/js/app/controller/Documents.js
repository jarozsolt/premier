/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Documents.js
 *
 * Documents view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.Documents', {
    extend : 'Ext.app.Controller',

    refs : [{
        ref : 'DocumentsView',
        selector : 'DocumentsView'
    }],

    stores : ['Documents']
}); 

