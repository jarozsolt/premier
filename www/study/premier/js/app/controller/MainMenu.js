/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MainMenu.js
 *
 * Controller for the main menubar
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 * @todo Alert connection error on logout, when no response received
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.MainMenu', {
    extend : 'Ext.app.Controller',

    //views : ['Viewport'],

    refs : [{
        ref : 'center',
        selector : 'viewport > #center'
    },{
		ref : 'queriesview',
		selector : 'queriesview'
	}],

    init : function() {"use strict";
        this.control({
            'toolbar #mainmenu-btn-todo' : {
                click : this.onTodoBtnClicked
            },
            'toolbar #mainmenu-btn-patients' : {
                click : this.onPatientsBtnClicked
            },
            'toolbar #mainmenu-btn-queries' : {
                click : this.onQueriesBtnClicked
            },
            'toolbar #mainmenu-btn-messages' : {
                click : this.onMessagesBtnClicked
            },
            'toolbar #mainmenu-btn-documents' : {
                click : this.onDocumentsBtnClicked
            },
            'toolbar #mainmenu-btn-contacts' : {
                click : this.onContactsBtnClicked
            },
            'toolbar #mainmenu-btn-reports' : {
                click : this.onReportsBtnClicked
            },
            'toolbar #mainmenu-btn-preferences' : {
                click : this.onPreferencesBtnClicked
            },
            'mainmenu *' : {
                logout : this.onSignoutBtnClicked
            }
        });
    },

    onTodoBtnClicked : function(button, e, eOpts) {"use strict";
        var todoView = Ext.getCmp('todoview');
        if (todoView && todoView.isStoreDirty()){
            todoView.fireEvent('reloadStore');
        }
        this.getCenter().getLayout().setActiveItem('todoview');
        
    },

    onPatientsBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Patients", "getPatients"))
        {
            var patientsView = Ext.getCmp('patientsview');
	        if (patientsView && patientsView.isStoreDirty()){
                patientsView.fireEvent('reloadStore');
            }
            this.getCenter().getLayout().setActiveItem('patientsview');
        }
    },

    onQueriesBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Queries", "getOpenQueries"))
        {
            var queriesView = Ext.getCmp('queriesview');
            if (queriesView && queriesView.isStoreDirty()){
                queriesView.fireEvent('reloadStore');
            }
            
            this.getCenter().getLayout().setActiveItem('queriesview');
            this.getQueriesview().getLayout().setActiveItem('OpenQueries');
            this.getQueriesview().setTitle(_("Nyitott query-k"));
        }
    },

    onMessagesBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Messages", "getReceivedMessages") ||
            Kron.core.UserManagement.hasPermission("Messages", "getSentMessages"))
        {
            this.getCenter().getLayout().setActiveItem('MessagesView');
        }
    },

    onDocumentsBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("Documents", "getDocuments"))
        {
            this.getCenter().getLayout().setActiveItem('DocumentsView');
        }
    },

    onContactsBtnClicked : function(button, e, eOpts) {"use strict";
        this.getCenter().getLayout().setActiveItem('ContactsView');
    },

    onReportsBtnClicked : function(button, e, eOpts) {"use strict";
        this.getCenter().getLayout().setActiveItem('ReportsView');
    },

    onPreferencesBtnClicked : function(button, e, eOpts) {"use strict";
        this.getCenter().getLayout().setActiveItem('UserPrefsView');
    },

    onSignoutBtnClicked : function(button, e, eOpts) {"use strict";
        if (Kron.core.UserManagement.hasPermission("UserManagement", "logout"))
        {
            // Ask the user to confirm
            Ext.Msg.show({
                title : _("Kijelentkezés"),
                msg : _("Biztosan kijelentkezik az alkalmazásból?"),
                buttons : Ext.Msg.OKCANCEL,
                buttonText: {ok: _("Kijelentkezés"), cancel: _("Mégsem")},
                fn : this.processLogout,
                icon : Ext.MessageBox.QUESTION
            });
        }
    },

    processLogout : function(btn) {"use strict";
        var params;
        if (btn === 'ok') {
            // call normal logout from menu/button
            params = {
                'normallogout' : true
            };
            Kron.UserManagement.logout(params, function(provider, response) {
                if (response === undefined || response.result === undefined || response.status === false) {// no response from the server
                    Ext.Msg.alert(_("Hiba"), _("A kijelentkezés nem volt sikeres. Nem sikerült kapcsolódni a szerverhez!"));
                } else {
                    if (response.result.success === true) {
                        if (response.result.redirect !== undefined && response.result.redirect !== null) {
                            //Remove the event handlers for Back button, Refresh, etc.
                            if (window.detachEvent ) {
                                window.detachEvent ('onbeforeunload', window_beforeUnload);
                            } else if (window.removeEventListener) {
                                window.removeEventListener('beforeunload', window_beforeUnload, false);
                            }
                            if (window.detachEvent ) {
                                window.detachEvent ('onunload', window_unload);
                            } else if (window.removeEventListener) {
                                window.removeEventListener('unload', window_unload, false);
                            }
                            var redirect = response.result.redirect;
                            window.location = redirect;
                        }
                    }
                }
            });
        }
    }
});
