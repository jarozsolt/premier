/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * ToSignature.js
 *
 * Signature view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 * @sample http://www.cparker15.com/code/examples/javascript/extjs4-dynamic-grid-example/
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.ToSignature', {
    extend: 'Ext.app.Controller',
    requires : ['Core.util.MD5', 'Kron.core.Alerts'],
 //   views: [            'Viewport'        ],

    refs: [	{ ref: 'signatureView', selector: '#signatureview' },
		    { ref: 'signatureGrid', selector: '#signaturegrid' },
		    { ref: 'signWindow', selector: '#signwindow' },
			{ ref: 'patientdlg', selector: '#patientdlg'},
			{ ref: 'queryForm', selector: '#queryForm' },
			{ ref: 'querydlg', selector: '#querydlg' },
			{ ref : 'patientcenter', selector : 'viewport > #center > #patientview> #patientview-center' }], 

	stores: ['Forms'],
    
    init: function() {
       // Start listening for events on views
        this.control({
            'toolbar #signaturetoolbar-btn-sign': {
                click: this.onSignBtnClicked
            },
            '#signaturegrid': {
                itemdblclick: this.onItemDblClick
            },
            '#overviewgrid': {
                itemdblclick: this.onItemDblClick
            },
             '#signformsignbtn' : {
                click : this.onSignFormSignBtnClicked 
            }
        });
       
    },
      
	onSignFormSignBtnClicked : function (button) {
		var me = this,
			form = button.up('form'),
			signatureGrid = me.getSignatureGrid();
			
		if (form && form.getForm().isValid()) {
			// ha lassu lenne, kirakok egy maskot
			if( me.signatureLoadMask === undefined || me.signatureLoadMask === null) {
     			me.signatureLoadMask = new Ext.LoadMask(signatureGrid, {msg:_("Betöltés...")});
     		}
            me.signatureLoadMask.show();		
            // encrypt password here
            var password = Core.util.MD5(form.getForm().getFields().get('signform_password').getValue(), false, true);

            var params = {
                'uiid' : form.getForm().getFields().get('signerscombo').getValue(),
                'password' : password,
                'formsToSign' : button.up('window').getFormids()
            };
            
			
            Kron.Forms.signForms(params, function(provider, response) {
                if (response === undefined || response.result === undefined || response.result.data === undefined || response.status === false) {
                    // no response from the server
                    // console.log("signForms: Connection Error");
                     Kron.core.Alerts.connectionError();
                } else {
					if (response.result.data.signedForms) {
						var store = me.getFormsStore(),
							signedFields,
							iid,
							filters;
						signedFields = store.queryBy(function(record) {
							return (Ext.Array.contains(response.result.data.signedForms, record.get('instanceid').toString()) ? true: false);
						});
						if (signedFields) {
							signedFields.each(function(item, index, length){
								item.set('signed', response.result.data.signed);
								item.set('signed_by', response.result.data.signed_by);
							});
							// a signature gridet újra szűrni kell
							
							if (me.getPatientcenter().getLayout().getActiveItem().itemId === 'signatureview') {
								store.filter();
							}
						}
					} else {
						Ext.Msg.alert(_('Hiba az aláírás során'), _('Az aláírás során hiba történt. Kérem próbálja meg újra!'));
					}
                }
                if( me.signatureLoadMask !== undefined && me.signatureLoadMask !== null) {
					me.signatureLoadMask.hide();	
				}
            });
            form.getForm().reset();
            button.up('window').hide();
            this.getPatientdlg().hide();
        }
	},
	onItemDblClick: function(grid, record) {
		if (record){
			var patientdlg = this.getPatientdlg(),
				formId;
			
			if (patientdlg) {
				formId = record.get('formtype') + 'Form';
			}
            patientdlg.setRecord(record, '', false);

		}
	},
	
	onSignBtnClicked: function(button, e, eOpts) {
		var formids= new Array();
		//var summary= '';
		//var i= 1;
		var signatureGrid = this.getSignatureGrid();
	//	var sumField;
		if (signatureGrid.getSelectionModel().hasSelection()){
			var records = signatureGrid.getSelectionModel().selected;
			records.each(function(item){
					formids.push(item.get('instanceid'));
					//summary+= i++ + " - " + item.get('formtype') + ': ' + item.get('content') +'\n';
			});
		//	this.getSignatureView().setSummary(summary);// kell ez?
			
			var win=this.getSignWindow();
			if (!win) {
				 win = Ext.create('Kron.view.patient.SignWindow');
            } 			
		//	sumField=Ext.getCmp('signaturesummary');
		//	sumField.setValue(summary);
		//	sumField.show();

			win.setFormids(formids);
			win.show();   
        }
	}
});
