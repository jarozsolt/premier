/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * UserPrefs.js
 *
 * UserPrefs view's controller
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.controller.UserPrefs', {
    extend : 'Ext.app.Controller',
	requires : ['Kron.view.userprefs.ChangePasswordWindow', 'Kron.view.userprefs.ChangeUserDetailsWindow','Core.util.MD5', 'Kron.core.Alerts'],
    //views : ['Viewport'],

    

    init : function() {"use strict";
        // Start listening for events on views
        this.control({
            '#btnUserPrefsPassword' : {
                click : this.onUserPrefsPasswordClicked
            },
			'#btnUserPrefsDetails' : {
                click : this.onUserPrefsDetailsClicked
			},
            /*'#btnChangePasswordSave': {
                click : this.onChangePasswordSave
            },*/
            '#btnChangeUserDetailsSave': {
                click : this.onChangeUserDetailsSave
            }
        });
        
    },

/*    onLaunch : function() {"use strict";
        this.reloadData();
    },
*/
/**
 * User preferencies panel jelszó változtatás ablakát jeleníti meg
 * @param {Object} button
 * @param {Object} e
 * @param {Object} eOpts
 */
    onUserPrefsPasswordClicked : function(button, e, eOpts) {"use strict";
        var window = Ext.getCmp('changepasswordwindow'), form;
		if (!window) {
			window = Ext.create('Kron.view.userprefs.ChangePasswordWindow');	
		} 
		form = Ext.getCmp('changepasswordform');
		if (form) {
			form.getForm().reset();
		}
		window.show();
	
    },
/**
 * User preferencies panel részletes adatainak megváltoztatására szolgáló ablakot jeleníti meg, feltöltve az aktuális adatokkal
 * @param {Object} button
 * @param {Object} e
 * @param {Object} eOpts
 */    
    onUserPrefsDetailsClicked : function(button, e, eOpts) {"use strict";
        var window = Ext.getCmp('changeuserdetailswindow'),
			UserPrefsView = Ext.getCmp('userprefs');
		if(!window) {
			window = Ext.create('widget.ChangeUserDetailsWindow');	
		} 
		window.show();
		window.updateDetail(UserPrefsView.getUserDetails());
   },


/**
 * A felhasználó adatainak megváltoztatására szolgál 
 * @param {Object} button
 * @param {Object} e
 * @param {Object} eOpts
 */
   onChangeUserDetailsSave : function(button, e, eOpts) {"use strict";
		var fields, form=Ext.getCmp('changeuserdetailsform');
        if (form.getForm().isValid()) {
			fields=form.getForm().getValues();
			if(fields.autosave_enabled !== undefined) {
			    fields.autosave_enabled = '1';
			} else {
			    fields.autosave_enabled = '0';
            }

			Kron.UserManagement.changeUserDetails(fields, function(provider, response) {
				if(response === undefined || response.result === undefined || response.status === false) {// no response from the server
					//console.log("changeUserDetails: Connection Error");
					Kron.core.Alerts.connectionError();
				} else {
					if (response.result.success === true) {
						var window= Ext.getCmp('changeuserdetailswindow'), userPrefs;
						if (window) {
							window.hide();
						}
					
						Ext.Msg.alert(_("Státusz"), _("Adatait sikeresen megváltoztatta."));
						
						if (response.result.userDetails) {
							userPrefs = Ext.getCmp('userprefs');
							userPrefs.updateDetail(response.result.userDetails);
						}
					
					} else {
					    // TODO : translate, ez milyen hibára vonatkozik?
						console.log(_("Hiba"), _("Nem sikerült kapcsolódni a szerverhez!"));
					}
				}
            });
        }
   }
    
    
});
