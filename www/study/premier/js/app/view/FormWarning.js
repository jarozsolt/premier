/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FormWarning.js
 *
 * Warning component for form messages
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.FormWarning', {
    extend : 'Ext.Component',
    alias : 'widget.formwarning',

    groupWarnings : false,
    hideDelay : 5000,
    messages : null,
    warnCt : null,
    
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.ownInitComponent();
        this.callParent(arguments);
        
    },
    ownInitComponent : function() {"use strict";
        if (this.messages === null) {
            this.messages = new Ext.util.MixedCollection();
        }
        if (!this.warnCt){
            this.warnCt = Ext.DomHelper.insertFirst(document.body, {id:'warning-div'}, true);
        }
        this.warnCt.anchorTo(document, 'tr-tr', [-20, 10]);
    },
    
    createBox : function(t, s){"use strict";
        return ['<div class="warning">',
                '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
                '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3>', t, '</h3>', s, '</div></div></div>',
                '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
                '</div>'].join('');
    },

    createToolTip : function(type, message, title, closable){"use strict";
        return ['<div class="tooltip-wrapper">',
            '<img src="resources/images/', type, '.png" alt="', type, '" height="32" width="32" />',
            '<div class="tooltip ', type, '">',
            (title ? '<em>' + title + '</em>' : ''),
            message,
            '</div>',
            (closable ? this.getCloseButton() : ''),
            '</div>'].join('');
    },

    getCloseButton : function(){"use strict";
        return ['<a href="#" id="tooltip-close" onclick="Ext.getCmp(',
            "'"+"formwarning"+"'",
            ').removeAllTips();" class="close-img">',
            '<img src="resources/images/close.gif" ',
            'width="15px" height="15px" ',
            'alt=',
            _("Bezárás"),
            ' />'].join('');
    },
    
    showWarning : function(id, message){"use strict";
        var me = this, warndiv,
        title = _("Figyelmeztetés"),
        type = this.WARNING;
        
        if (me.groupWarnings) {
            if (me.messages !== null) {
                if (!me.messages.get(id)) {
                    me.messages.add(id, {'type':type, 'message':message, 'title':title});
                }
            }
        }
        else {
            if (message !== undefined && message !== null && message !== '') {
                me.removeAllTips();
                warndiv = Ext.DomHelper.append(this.warnCt, me.createToolTip(type, message, title, true), true);
                warndiv.hide();
                warndiv.slideIn('t', {
                    duration : 1000
                }).ghost("t", {
                    delay : me.hideDelay,
                    remove : true
                });
            }
        }
    },
    
    startWarningGroup : function(){"use strict";
        var me = this;
        if (me.groupWarnings !== true) {
            me.groupWarnings = true;
            if (me.messages !== null) {
                me.messages.clear();
            }
        }
    },
    
    stopWarningGroup : function(){"use strict";
        var warndiv, me = this, warning = '',
        hideDelay = me.hideDelay*me.messages.getCount();
        if (me.groupWarnings === true) {
            me.groupWarnings = false;
            if (me.messages !== null) {
                me.messages.each(function(item) {
                    if (warning !== '') {
                        warning += '<hr>';
                    }
                    warning += item.message + '<br>';
                });
                if (warning !== '') {
                    me.removeAllTips();              
                    warndiv = Ext.DomHelper.append(this.warnCt, me.createToolTip(me.WARNING, warning, _("Figyelmeztetés"), true), true);
                    warndiv.hide();
                    warndiv.slideIn('t', {
                        duration : 1000
                    }).ghost("t", {
                        delay : hideDelay,
                        remove : true
                    });
                }
            }
        }
    },
    
    /**
     * remove all tooltips
     */
    removeAllTips: function(){"use strict";
        Ext.select('.tooltip-wrapper, .warning').remove();
    },

    /**
     * Tooltip types (only the WARNING used)
     */
    CRITICAL: 'critical',
    WARNING: 'warning',
    INFO: 'info',
    HELP: 'help'
}); 
