/*jslint nomen: true*/
/*global console: false, Kron: false, _:false, Core: false*/
/**
 *---------------------------------------------------------------
 * ChangePasswordWindow.js
 *
 * Change Password window in UserPrefs view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

// Add the additional 'advanced' VTypes
Ext.apply(Ext.form.field.VTypes, {
    password: function(val, field) {"use strict";
        if (field.initialPassField) {
            var pwd = field.up('form').down('#' + field.initialPassField);
            return (val === pwd.getValue());
        }
        return true;
    },

    passwordText: _("A jelszavak nem egyeznek")
});

Ext.define('Kron.view.userprefs.ChangePasswordWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.ChangePasswordWindow',
    requires : ['Ext.ux.form.PasswordMeter'],
	title : _("Jelszó megváltoztatása"),
	closable : true,
	iconCls : 'icon-changepassword-wnd',
	//closeAction : 'hide',
	width : 400,
	height : 300,
	modal : true,
	additionalText : '',
	hintText : _("A megváltoztatáshoz adja meg az új és régi jelszavát is."),
	id : 'changepasswordwindow',
	
	initComponent : function() { // Do not use "use strict" on functions containing callParent!
        
        this.ownInitComponent();
        this.callParent(arguments);
    },
    ownInitComponent : function() { "use strict";
        this.items = [{
            xtype : 'panel',
            //cls: 'form-changepassword-header',
            baseCls: 'x-plain',
            region: 'north',
            layout: {
                type: 'hbox'
            },
            items : [{
                xtype : 'displayfield',
                //style : 'color: #04468c;',
                fieldBodyCls: 'color:#04468c;vertical-align:middle;',
                cls: 'color:#04468c;vertical-align:middle;',
                padding : '10 10 10 10',
                flex : 1,
                height: 50,
                value : '<b>'+this.hintText+'</b>'
            }, {
                xtype : 'component',
                margin : '10 10 10 10',
                width : 50,
                autoEl: {tag: 'img', src: 'resources/images/lock_48.png'}
            }]
        }, {
            xtype : 'form',
            id: 'changepasswordform',
            layout : {
                type: 'vbox'
            },
            bodyPadding : 10,
            fieldDefaults : {
                labelAlign : 'left',
                labelWidth : 170,
                labelStyle : 'font-weight:bold',
                width : 350
            },
            defaults : {
                msgTarget : 'side',
                margin : '10 5 10 5'
            },
            items : [{
                xtype: 'passwordmeterfield',
                fieldLabel : _("Új jelszó"),
                //inputType: 'password',
                name : 'newpassword',
                itemId: 'newpassword',
                allowBlank: false,
                minLength : 6,
                minLengthText : _("A jelszó minimális hossza {0} karakter")
            }, {
                xtype: 'passwordmeterfield',
                fieldLabel : _("Új jelszó még egyszer"),
                inputType: 'password',
                name : 'confirmpassword',
                vtype: 'password',
                initialPassField: 'newpassword', // id of the initial password field
                allowBlank: false,
                minLength : 6,
                minLengthText : _("A jelszó minimális hossza {0} karakter")
            }, {
                xtype: 'textfield',
                fieldLabel : _("Régi jelszó"),
                inputType: 'password',
                name : 'oldpassword',
                allowBlank: false
            }] 
        }];
        this.dockedItems = [{
            //id : 'dockedForm',
            dock : 'bottom',
            xtype : 'toolbar',
            items : [{
                text : _("Megváltoztat"),
                scale : 'medium',
                minWidth : 90,
                iconCls : 'icon-save-btn_medium',
                //d : 'btnChangePasswordSave',
                formBind : true,
                handler : function() {
                    this.up('window').changePassword();
                }
            }, {
                text : _("Mégsem"),
                minWidth : 90,
                scale : 'medium',
                iconCls : 'icon-cancel-btn_medium',
                handler : function() {
                    this.up('window').close();
                }
            }]
	   }];
    },
	changePassword : function() {"use strict";
        var fields, params, userPrefs,
        form = Ext.getCmp('changepasswordform');
        if (form.getForm().isValid()) {
            fields = form.getForm().getValues();
            if (fields.newpassword !== fields.confirmpassword) {
                Ext.Msg.alert(_("Hiba"), _("Az 'Új jelszó' és az 'Új jelszó még egyszer' nem egyezik meg!"));
            }
            else {
                if (fields.newpassword === fields.oldpassword) {
                    Ext.Msg.alert(_("Hiba"), _("Az 'Új jelszó' és a 'Régi jelszó' nem lehet ugyanaz!"));
                }
                else {
                    params = {
                        'newpassword' : Core.util.MD5(fields.newpassword, false, true),
                        'oldpassword' : Core.util.MD5(fields.oldpassword, false, true)
                    };
                    Kron.UserManagement.changePassword(params, function(provider, response) {
                        if (response === undefined || response.result === undefined || response.status === false) {// no response from the server
                            Ext.Msg.alert(_("Hiba"), _("Nem sikerült kapcsolódni a szerverhez!"));
                        } else {
                            if (response.result.success === true) {
                                if (response.result.userDetails) {
                                    userPrefs = Ext.getCmp('userprefs');
                                    userPrefs.updateDetail(response.result.userDetails);
                                }
                                form.up('window').close();
                                Ext.Msg.alert(_("Információ"), _("Jelszavát sikeresen megváltoztatta."));
                            } else if (response.result.errortxt) {
                                Ext.Msg.alert(_("Hiba"), response.result.errortxt);
                            }
                        }
                    });
                }
            }  
        }
    }
});
