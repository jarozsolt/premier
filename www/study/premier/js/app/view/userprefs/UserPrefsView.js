/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * UserprefsView.js
 *
 * UserprefsView form panel
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.view.userprefs.UserPrefsView', {
    extend : 'Ext.panel.Panel',
    alias : 'widget.UserPrefsView',
    id : 'userprefs',
    userDetails : '',

    title : _("Saját adatok"),

    tplMarkup : ['<div class="userprefsview"><br/>', '<h1>{name}</h1>', '<p>{userroledescription}</p>', '<div class="todo-visit">', '<table class="userprefsview-data" style="cursor:pointer;"  cellspacing="0" cellpadding="0" border="0" ><tbody>', '<tr>', '<td class="userprefsview-data-leftheader">' + _("Felhasználónév: ") + '</td>', '<td > {username} </td>', '</tr>', '<tr>', '<td class="userprefsview-data-leftheader">' + _("Telefon: ") + '</td>', '<td> {phone} </td>', '</tr>', '<tr>', '<td class="userprefsview-data-leftheader">' + _("E-mail: ") + '</td>', '<td> {email} </td>', '</tr>', '<tr>', '<td class="userprefsview-data-leftheader">' + _("Utolsó bejelentkezés: ") + '</td>', '<td>{last_logged} </td>', '</tr>', '</tbody></table>', '</div', '</div>'],

    emptyText : _("Nincs adat"),

    initComponent : function() {// Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },

    strictInitComponent : function() {"use strict";
        this.tpl = Ext.create('Ext.XTemplate', this.tplMarkup);
        this.html = this.emptyText;
        this.bodyStyle = {
            background : '#ffffff'
        };
    },

    listeners : {
        afterrender : function(dview) {"use strict";
        }
    },

    dockedItems : [{
        dock : 'top',
        xtype : 'toolbar',
        items : [{
            text : "<b>" + _("Saját beállítások") + "</b>",
            scale : 'medium',
            minWidth : 90,
            tooltip : _("Saját adatok megváltoztatása"),
            iconCls : 'icon-userdetails-btn_medium',
            id : 'btnUserPrefsDetails',
            formBind : true
        }, {
            text : "<b>" + _("Jelszó megváltoztatása") + "</b>",
            minWidth : 90,
            scale : 'medium',
            tooltip : _("Jelszó megváltoztatása"),
            iconCls : 'icon-changepassword-btn_medium',
            id : 'btnUserPrefsPassword'
        }]
    }],

    /**
     * A user panel-t frissíti
     * @param {Object} userDetails
     */
    updateDetail : function(userDetails) {"use strict";
        if (userDetails !== null) {
            this.userDetails = userDetails;
            this.tpl.overwrite(this.body, userDetails);
        }
    },

    /**
     * Visszaadja a bejelentkezett felhasználó usergroup-ját
     */
    getUserGroup : function() {"use strict";
        return this.userDetails.usergroup;
    },

    /**
     * Visszaadja a bejelentkezett felhasználó összes adatát (userDetails)
     */
    getUserDetails : function() {"use strict";
        return this.userDetails;
    },

    /**
     * Visszaadja a bejelentkezett felhasználó nevét
     */
    getName : function() {"use strict";
        return this.userDetails.name;
    },
    // @TODO: Kiszedni az autosave-et
    isAutosaveEnabled : function () {"use strict";
        return false;
    },
    // @TODO: Kiszedni az autosave-et
    getAutosaveFrequency : function () {"use strict";
        return parseInt(this.userDetails.autosave_frequency, 10);
    }
});

