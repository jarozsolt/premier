/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * ChangeUserDetailsWindow.js
 *
 * Change UserDetails window in UserPrefs view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.userprefs.ChangeUserDetailsWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.ChangeUserDetailsWindow',

	title : _("Saját adatok megváltoztatása"),
	closable : true,
	closeAction : 'hide',
	width : 350,

	modal : true,
	id : 'changeuserdetailswindow',
	items: [{
        xtype : 'form',
        id: 'changeuserdetailsform',
	    layout : {
	        type: 'vbox'
	    },
	    bodyPadding : 10,

	    items : [ {
			    fieldLabel : _("Név"),
			    labelWidth : 100,
			    width : 300,
			    name : 'name',
			    xtype: 'textfield'
	        }, {
	            fieldLabel: _("Nyelv"),
	            labelWidth : 100,
	            width : 300,
			    name: 'language',
			    xtype: 'combobox',
				editable: false,
				triggerAction: 'all',
				forceSelection: true,
				allowBlank: false,
				hidden : true,
				enableKeyEvents: true,
				store: new Ext.data.Store({
	                id: 0,
	                fields : ['value', 'lang'],
	                data: [{
						value: 'hu_HU',
						lang: _("Magyar")
	                }, {
						value: 'en_US',
						lang: _("Angol")
	                }]
	            }),
			    valueField: 'value',
			    displayField: 'lang',
			    mode: 'local',
			    listeners: {
                // megakadályozza a backspace billentyűre a browser back-et
                    keydown: function(obj, e) {"use strict";
                        if (e.getCharCode() === e.BACKSPACE) {
                            e.preventDefault();
                        }
                    }
                }
	        }, {
                xtype: 'textfield',
                labelWidth : 100,
                width : 300,
			    fieldLabel: _("Telefon"),
			    name: 'phone'
	        }, {
			    xtype: 'textfield',
			    labelWidth : 100,
			    width : 300,
			    fieldLabel: _("E-mail"),
			    name: 'email'
			},{
				xtype:'fieldset',
				title: _("Automatikus mentés"), 
                padding : '5 5 5 5',
				columnWidth: 0.5,
				checkboxToggle: true,
				checkboxName : 'autosave_enabled',
				width : 300,
				hidden: true,
			//	collapsed: true, // fieldset initially collapsed
				layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
				items :[{
				    xtype: 'numberfield',
					fieldLabel: _("Mentési gyakoriság"),
					name: 'autosave_frequency',
					labelWidth : 140,
					width: 200,
                   // anchor: '100%',
                    maxValue: 120,
                    minValue: 0
				}, {
                    xtype: 'label',
                    text: _("perc"),
                    padding :'0 5'
                }]
			}]
	}],
	dockedItems : [{
		//id : 'dockedForm',
		dock : 'bottom',
		xtype : 'toolbar',
		items : [{
			text : _("Mentés"),
			scale : 'medium',
			minWidth : 90,
			iconCls : 'icon-save-btn_medium',
			id : 'btnChangeUserDetailsSave',
			formBind : true
		},{
			text : _("Mégsem"),
			minWidth : 90,
			scale : 'medium',
			iconCls : 'icon-cancel-btn_medium',
			handler : function() {"use strict";
				this.up('window').hide();
			}

		}]	
	}],
	
	/**
	 * A felhasználói adatok módosítása formot tölti fel
	 * @param {Object} userDetails
	 */	
	updateDetail: function(userDetails) {"use strict";
		if (userDetails !== null ) {
			this.down('form').getForm().setValues(userDetails);
		}
	}
});
