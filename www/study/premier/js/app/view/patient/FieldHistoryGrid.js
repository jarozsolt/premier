/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FieldHistoryGrid.js
 *
 * A field history = audit trail gridje
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.FieldHistoryGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.fieldhistorygrid',
	id : 'fieldhistorygrid',
	store : 'FieldHistories',
	
	renderAction : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		return  Kron.fieldActionTexts[value];
	},
	
	
	renderText : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var patientDlg = Ext.getCmp('patientdlg'),
		  returnvalue;
		if ( record.get('action') === Kron.fieldAction.DeleteQuery || record.get('action') === Kron.fieldAction.CloseQuery ||record.get('action') === Kron.fieldAction.Query ||record.get('action') === Kron.fieldAction.Answer ) {
		    returnvalue = value;
		} else {
		    returnvalue = patientDlg.getFormattedFieldValue(record.get('fieldname'), value);
		}
		
		return returnvalue;

	},		
	
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },

	strictInitComponent : function() { "use strict";
		this.columns = [{
			xtype : 'datecolumn',
			format : 'Y.m.d H:i:s',
			header : _("Dátum"),
			dataIndex : 'datetime',
			width : 120
		}, {
			header : _("Név"),
			dataIndex : 'user',
			width : 120
		//	renderer : this.renderEndDate
		}, {
			header : _("Esemény"),
			dataIndex : 'action',
			width : 150,
			renderer : this.renderAction
		}, {
			header : _("Tartalom"),
			dataIndex : 'text',
			renderer : this.renderText,
			shrinkWrap : 2,
			flex : 1
		}];
	}
}); 