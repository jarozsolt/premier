/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * SignatureView.js
 *
 * View containing the valid forms, available to sign
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.SignatureView', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.signatureview',
	id : 'signatureview',
	requires : ['Kron.view.patient.SignatureToolbar', 'Kron.view.patient.SignatureGrid'],

	investigators : '',

	layout : 'fit',

	items : [{
		xtype : 'signaturegrid'
	}],

	dockedItems : [{
		dock : 'bottom',
		xtype : 'signaturetoolbar'
	}],

	//summary : '',

	
    initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent : function() { "use strict";
		
	}
}); 