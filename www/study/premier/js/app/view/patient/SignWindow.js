/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * SignWindow.js
 *
 * Sign functionality
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.SignWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.signwindow',
	title : _("Aláírás"),
	requires : ['Kron.view.patient.forms.SignForm'],

	closable : true,
	closeAction : 'hide',
	width : 400,
	//    height: 300,
	modal : true,
	bodyStyle : 'padding: 5px;',
	id : 'signwindow',

	items : [{
		xtype : 'signform',
		itemId : 'signform'
	}],

	formids : [],

	setFormids : function(formids) {"use strict";
		this.formids = formids;
	},

	getFormids : function() {"use strict";
		return this.formids;
	}
});
