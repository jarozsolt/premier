/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * SignForm.js
 *
 * Sign form - form panel in Sign window
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.SignForm', {
    extend : 'Ext.form.Panel',
    alias : 'widget.signform',
    id : 'signform',
    url : 'save-form.php',
    
    layout : {
        type : 'vbox',
        align : 'stretch'
    },

    border : false,
    bodyPadding : 10,

    fieldDefaults : {
        labelAlign : 'left',
        labelWidth : 100,
        labelStyle : 'font-weight:bold'
    },
    defaults : {
        margins : '0 0 0 0'
    },

    frame : true,

    initComponent : function() {
        this.strictInitComponent();
        this.callParent(arguments);
    },

    strictInitComponent : function() {"use strict";
        var signersStore = Ext.data.StoreManager.lookup('Signers');

        var signerDefaultValue = '';
        var userprefs = Ext.getCmp('userprefs');
        if (Kron.core.UserManagement.hasPermission("Forms", "signForms")) {
            signerDefaultValue = userprefs.userDetails.uiid;
        }

        //	var sum = Ext.getCmp('signatureview').getSummary();
        this.items = [{
            xtype : 'combobox',
            id : 'signerscombo',
            editable : false,
            enableKeyEvents: true,
            triggerAction : 'all',
            forceSelection : true,
            emptyText : _("Válasszon aláírót..."),
            allowBlank : false,
            store : signersStore,
            valueField : 'uiid',
            displayField : 'name',
            mode : 'local',
            fieldLabel : _("Aláíró"),
            value : signerDefaultValue,
            listeners: {
            // megakadályozza a backspace billentyűre a browser back-et
                keydown: function(obj, e) {
                    if (e.getCharCode() === e.BACKSPACE) {
                        e.preventDefault();
                    }
                }
            }
        }, {
            fieldLabel : _("Jelszó"),
            xtype : 'textfield',
            name : 'password',
            inputType : 'password',
            allowBlank : false,
            id : 'signform_password'
        }];
        this.buttons = [{
            text : _("Aláír"),
            id : 'signformsignbtn'

        /*    handler : function() {
                if (this.up('form').getForm().isValid()) {
                    // encrypt password here

                    var password = Core.util.MD5(this.up('form').getForm().getFields().get('signform_password').getValue(), false, true);

                    var params = {
                        'uiid' : this.up('form').getForm().getFields().get('signerscombo').getValue(),
                        'password' : password,
                        'formsToSign' : this.up('window').getFormids()
                    };

                    Kron.Forms.signForms(params, function(provider, response) {
                        if (response === undefined || response.result.data === undefined || response.status === false) {
                            // no response from the server
                            console.log("signForms: Connection Error");
                        } else {
                            // Todo: update local stores
                            var patientView = Ext.getCmp('patientview');
                            patientView.updateStores();
                            
                        }
                    });
                    this.up('form').getForm().reset();
                    this.up('window').hide();
                }
            }*/
        }, {
            text : _("Mégsem"),
            handler : function() {
                this.up('form').getForm().reset();
                this.up('window').hide();
            }
        }];
    }
});
