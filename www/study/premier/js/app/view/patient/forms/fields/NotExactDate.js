/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * NotExactDate.js
 *
 * Control for a non-exact date
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.apply(Ext.form.field.VTypes, {
    notexactfuturedate:  function(v) {
        var date, datestring, utilDate = Ext.Date,
        re = /^\d{4}(\.\d{2}){0,2}$/;
        if (re.test(v))
        {
            if (v.length === 10) {
                date = utilDate.parse(v, 'Y.m.d', true);
                if (!date) {
                    return false;
                }
            }
            else if (v.length === 7) {
                date = utilDate.parse(v+'.01', 'Y.m.d', true);
                if (!date) {
                    return false;
                }
            } 
            return true;
        }
        return false;
       //return /^\d{4}(\.\d{2}){0,2}$/.test(v);
    },
    notexactfuturedateText: _("Érvénytelen dátumformátum - a helyes formátum éééé.hh.nn vagy éééé.hh vagy éééé"),
    notexactfuturedateMask: /[\d\.]/i
});
Ext.apply(Ext.form.field.VTypes, {
    notexactdate:  function(v) {
        var date, datestring, utilDate = Ext.Date, actDate = new Date(),
        re = /^\d{4}(\.\d{2}){0,2}$/;
        if (re.test(v))
        {
            if (v.length === 10) {
                date = utilDate.parse(v, 'Y.m.d', true);
                if (!date) {
                    return false;
                }
            }
            else if (v.length === 7) {
                date = utilDate.parse(v+'.01', 'Y.m.d', true);
                if (!date) {
                    return false;
                }
            } 
            
            date = Ext.Date.parse(v, "Y.m.d");
    		if (!Ext.isDate(date)) {
    			date = Ext.Date.parse(v + ".01",  "Y.m.d");
    		}
    		if (!Ext.isDate(date)) {
    			date = Ext.Date.parse(v + ".01.01",  "Y.m.d");
    		}
    		if (Ext.isDate(date) && Ext.Date.clearTime(date).getTime() < Ext.Date.clearTime(actDate).getTime()) {
				 return true;			
			} else {
				return false;
			}
            
           
        }
        return false;
       //return /^\d{4}(\.\d{2}){0,2}$/.test(v);
    },
    notexactdateText: _("Érvénytelen dátumformátum vagy jövőbeli dátum - a helyes formátum éééé.hh.nn vagy éééé.hh vagy éééé"),
    notexactdateMask: /[\d\.]/i
});

Ext.define('Kron.view.patient.forms.fields.NotExactDate', {
    extend : 'Ext.form.field.Text',
    alias : 'widget.notexactdate',
    format : 'Y.m.d',
    altFormats: 'Y|Y.m|Y.m.d',
    dateSeparator : '.',
    width : 136,
    
    
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    strictInitComponent : function() { "use strict";
        var me = this;
        me.vtype = me.futureDate===false?'notexactdate':'notexactfuturedate';
    },
    
     listeners: {
                'change': function (field, newValue, oldValue, eOpts) {"use strict";
                	var form,
		            	me = this,
		            	actDate,
		            	endDateString,
		            	startDateString,
		            	endDateField;
		            	
					if (me.alertEndDateConflict !== undefined && me.alertEndDateConflict !== null) {
					
						if (field.isValid()) {
				            form= this.up('form');
			            	actDate = new Date();
			            	endDateField = form.down('[name=' + me.alertEndDateConflict + ']');
				            	
				            if (endDateField !== undefined && endDateField.isValid()) {
				            	endDateString = endDateField.getValue();
				            	if (endDateString=== "o.g.") {
				            		endDateString = Ext.Date.format(actDate, "Y.m.d");
				            	}
				                if (endDateString !== '' && endDateString !== undefined && me.getValue() !== '' && me.getValue() !== undefined) {
			    		            
				                	startDateString = me.getDateValue();
				                    if (startDateString > endDateString) {
				                            form.showStartEndDateConflictWarning();
				                    }
				                }
				            }
				        }
                   	}
                }
	},
    
    getDateValue: function() {"use strict";
    	var field = this, dateValue;
    	
		if (field.isValid()) {
    		dateValue = Ext.Date.parse(field.getValue(), "Y.m.d");
    		if (!Ext.isDate(dateValue)) {
    			dateValue = Ext.Date.parse(field.getValue()+ ".01",  "Y.m.d");
    		}
    		if (!Ext.isDate(dateValue)) {
    			dateValue = Ext.Date.parse(field.getValue() + ".01.01",  "Y.m.d");
    		}
    		if (Ext.isDate(dateValue)) {
    			return dateValue;
    		} else {
    			return undefined;
    		}
    	}
	}
    /*invalidFormatText : _("{0} érvénytelen dátumformátum - a helyes formátum éééé.hh.nn vagy éééé.hh vagy éééé"),
    invalidText : _("Érvénytelen dátum"),
    getErrors: function(value) { // Do not use "use strict" on functions containing callParent!
        var valueTokens, i, me = this, invalidFormat = false,
        utilDate = Ext.Date,
        errors = me.callParent(arguments);
            
        if (value === null || value.length < 1) { // if it's blank and textfield didn't flag it then it's valid
             return errors;
        }
        
        var re = /^([1-2][0-9][0-9][0-9])(.[0-1][1-9])?(.[0-3][1-9])?$/;

        
        if (!re.test(value)) {
            errors.push(Ext.String.format(me.invalidFormatText, value));
            return errors;
        }

        
        utilDate.format = 'Y.m.d';
        date = utilDate.parseDate(value);
        if (!date) {
            errors.push(me.invalidText);
            return errors;
        }
        
        return errors;
    }*/
}); 

