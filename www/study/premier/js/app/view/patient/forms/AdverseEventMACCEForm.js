/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * AdverseEventForm.js
 *
 * MACCE form - MACCE form
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.AdverseEventMACCEForm', {
    extend : 'Kron.view.patient.forms.GeneralPatientForm',
    alias : 'widget.adverseeventmacceform',
    id : 'AdverseEventMACCEForm',
    formWidth : 760,
    formHeight : 600,
    formTitle :Kron.formNames.AdverseEventMACCE,
    formIconCls : 'icon-adverseevent-dlg_small',
    autoScroll : true,
    layout : 'fit',
    bodyPadding : 5,
    
    // We can save the form only if the diagnosis field is not empty
    canSave : function() {"use strict";
        var fieldMacceType = this.down('[name=macceType]'),
        	fieldMacceTypeValue; 
        if (fieldMacceType !== undefined && fieldMacceTypeValue !== null ) {
        	fieldMacceTypeValue = fieldMacceType.getValue(); 
            if ( Ext.Object.getSize(fieldMacceTypeValue) >0) {
                return true;
            }
        }
        return false;
    },
    
    // Alert an error message if the diagnosis is not filled
    alertSaveError : function() {"use strict";
        Ext.Msg.show({
            title : _("Hiba"),
            msg : _("A 'MACCE típusa' mezőt kötelező kitölteni a MACCE elmentéséhez!"),
            buttons : Ext.Msg.OK,
            icon : Ext.MessageBox.ERROR,
            animateTarget: 'dlgAccept'});
    },
 	initForm : function(fieldValues) { "use strict";
		//console.log('initForm called');
		var subPanels =this.query('panel[subPanel= true]');
		subPanels.forEach(function(panel){
			if (panel !== undefined && panel.tab !== undefined ) {
				if (!panel.tab.hasCls('icon-tab-noicon')) {
					panel.tab.addCls('icon-tab-noicon');	
				}
			}
		});
		

		return true;
	},
    // form érték betöltés utáni ellenőrzéseket , amelyekre alertet kell doni, lehet ide tenni.
	onAfterFormLoad : function(fieldValues) { "use strict";
		var panels;
		// első tab legyen aktív
		this.down('panel').setActiveTab(0);
		// a panel show eventre állítom majd be, hogy scrollozon fel (ilyenkor még nem működik)
		panels = this.down('tabpanel').query('panel');
		panels.forEach(function(panel){
			if(panel.xDoScrollUp === false ) {
				panel.xDoScrollUp = true; 
			}
		});
	},
	    
    items : [{
        xtype : 'tabpanel',
        layout : 'fit',
        activeTab : 0,
        plain : true,
        items : [{
            xtype : 'panel',
            title : _("MACCE általános jellemzők"),
            bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    }  
           	},
           	xDoScrollUp : true,
           	listeners : {
                'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{
		       	xtype : 'fieldset',
		        title : _("A MACCE kezdete és vége"),
		       	defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : false,
			 	 	msgTarget : 'side',
		            labelWidth : 170
		        },
		        items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					width: 32,
					margin : '0 0 0 0'
				},{
		            xtype : 'datefield',
		            name : 'startDate',
		            width : 300,
		            format : Kron.defaultDateFormat,
		            fieldLabel : _("Esemény kezdete"),
		            maxValue: new Date(),
		            isPrintable : true
		        }, {
		            xtype: 'monitorcheck'
		        }, {
		            xtype: 'fielddetailsbtn'
		        }, {
		            xtype : 'ongoingdate',
		            name : 'endDate',
		            fieldLabel : _("Esemény vége"),
		            width: 605,
		
		            margin : '5 0 5 0',
		            dateFormat : Kron.defaultDateFormat,
		            futureDate : false,
		            ongoingboxLabel : _("folyamatban"),
		            validateMessage : _("Kérem töltse ki a 'Vége' dátumot vagy állítsa 'folyamatban'-ra."),      
		            isPrintable : true
		        },{
		            xtype: 'monitorcheck'
		        },{
		            xtype: 'fielddetailsbtn'
		       	}]
		    },{
		    	xtype : 'fieldset',
		        title : _("MACCE típusa") + Kron.config.labelMandatoryMark,
		       	defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : false,
			 	 	msgTarget : 'side',
		            labelWidth : 170
		        },
		        items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					width: 32,
					margin : '0 0 0 0'
				},{
				    xtype : 'radiogroup',
			        historyLabel : _("MACCE típusa"),
			        name : 'macceType',     
			        columns: [120,200, 200],
		            width : 590,
		            margin : '5 0 5 0',   
			        items : [{
			        	xtype : 'radiofield',
	                    boxLabel : _("Stroke"),
	                    name : 'macceType',
	                    inputValue : 1,
	                    listeners : {
	                        change : function(rb, newValue, oldValue, options) {"use strict";
	                            var form = rb.up('form'),
	                            	strokePanel = rb.up('panel').nextSibling('panel');
	                            form.setLinkedFieldsState(['strokeType'], newValue);
	                        }
	                    }
			      	},{
	                    xtype : 'radiofield',
	                    boxLabel : _("Miokardiális infarktus (MI)"),
	                    name : 'macceType',
	                    inputValue : 2,
	                    listeners : {
	                        change : function(rb, newValue, oldValue, options) {"use strict";
	                            var form = rb.up('form'),
	                            	miPanel = rb.up('panel').nextSibling('panel').nextSibling('panel');
	                            form.setLinkedFieldsState([	
	                            	'MILokalizacioja',
	    							'celLezioKapcsolat',
	    							'stentTrombozis',
	    							'vervetelTortent',
	    							'szivgyogyszer' ], newValue);	                            
	                        }
	                    }  	
	             	},{
				        xtype : 'radiofield',
	                    boxLabel : _("Ismételt revaszkularizáció"),
	                    name : 'macceType',
	                    inputValue : 3,
	                    listeners : {
	                        change : function(rb, newValue, oldValue, options) {"use strict";
	                            var form = rb.up('form'),
	                            	irvPanel = rb.up('panel').nextSibling('panel').nextSibling('panel').nextSibling('panel');
	                            form.setLinkedFieldsState([
	                            	'iszkemiasstatus',
	                            	'revaszkularizacioTipus',
	                            	'ismeteltRevaszkularizacioDisplayfield',
									'anginasStatusz',
									'pozitivStressz',
									'EKGvaltozas',
									'ismeteltAngiografia',
									'egyebRevaszkAlap',
									'celLezioErintettseg',
									'oldalagiStenozisAtmero',
									'foagiStenozisAtmero'], newValue);
	                        }
	                    }	
			       }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				}]
			},{
				xtype : 'fieldset',
		        title : _("A MACCE kapcsolata a beültetett eszközzel"),
		        defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : false,
			 	 	msgTarget : 'side'
		       	},
		        items : [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
			 	},{
					xtype : 'radiogroup',
	             	historylabel : _("A MACCE kapcsolata az eszközzel"),
	                labelWidth : 200,
	                width: 590,
	                columns: [120,120, 120, 120],
	                name: 'MACCEKapcsolataAzEszkozzel',
	                items : [{
	                    name : 'MACCEKapcsolataAzEszkozzel',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nincs kapcsolat"),
	                    inputValue : 1
	                },{
	                    name : 'MACCEKapcsolataAzEszkozzel',
	                    xtype : 'radiofield',
	                    boxLabel : _("Lehetséges"),
	                    inputValue : 2 
	                },{
	                    name : 'MACCEKapcsolataAzEszkozzel',
	                    xtype : 'radiofield',
	                    boxLabel : _("Valószínűsíthető"),
	                    inputValue : 3 
	                },{
	                    name : 'MACCEKapcsolataAzEszkozzel',
	                    xtype : 'radiofield',
	                    boxLabel : _("Egyértelmű"),
	                    inputValue : 4 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
		        }]
		    },{
		        xtype : 'fieldset',
		        title : _("A MACCE kapcsolata a beavatkozással (beültetéssel):"),
		        defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : false,
			 	 	msgTarget : 'side'
		       	},
		        items : [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
			 	},{
					xtype : 'radiogroup',
	             	historylabel : _("A MACCE kapcsolata a beavatkozással (beültetéssel)"),
	                labelWidth : 200,
	                width: 590,
	                columns: [120,120,120,120],
	                name: 'MACCEKapcsolataABeavatkozassal',
	                items : [{
	                    name : 'MACCEKapcsolataABeavatkozassal',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nincs kapcsolat"),
	                    inputValue : 1
	                },{
	                    name : 'MACCEKapcsolataABeavatkozassal',
	                    xtype : 'radiofield',
	                    boxLabel : _("Lehetséges"),
	                    inputValue : 2 
	                },{
	                    name : 'MACCEKapcsolataABeavatkozassal',
	                    xtype : 'radiofield',
	                    boxLabel : _("Valószínűsíthető"),
	                    inputValue : 3 
	                },{
	                    name : 'MACCEKapcsolataABeavatkozassal',
	                    xtype : 'radiofield',
	                    boxLabel : _("Egyértlemű"),
	                    inputValue : 4 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
	
		        }]
		    },{
				xtype : 'fieldset',
		        title : _("Az eszköz alkalmazásának kezdete"),
		        defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : false,
			 	 	msgTarget : 'side'
		       	},
		        items : [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
		            xtype : 'ongoingdate',
		            name : 'firstAdministrationDate',
		            fieldLabel : _("Kezdő dátum"),
		            labelWidth : 170,
		            width : 560,
		            dateFormat : Kron.defaultDateFormat,
		            futureDate : false,
		            ongoingboxLabel : _("nem kezdte el alkalmazni az eszközt"),
		            validateMessage : _("Kérem töltse ki a dátumot vagy jelölje, hogy a beteg nem kezdte el alkalmazni az eszközt."),
		            isPrintable : true
		        },{
		            xtype: 'monitorcheck'
		        },{
		            xtype: 'fielddetailsbtn'
		        }]
		    },{
				xtype : 'fieldset',
		        title : _("Súlyos nemkívánatos esemény"),
		        defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : false,
			 	 	msgTarget : 'side'
		       	},   
		        items : [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 36,
					height : 1,
					margin : '0 0 0 0'
				},{
		            xtype : 'radiogroup',
		            fieldLabel : _("Súlyos nemkívánatos esemény?"),
		            labelWidth : 305,
		            width: 500,
		            name : 'ae_serious',
	           		allowBlank : false,
		            msgTarget : 'side',
		            isPrintable : true,
		            items : [{
		                name : 'ae_serious',
		                xtype : 'radiofield',
		                boxLabel : _("Igen"),
		                inputValue : 1,
		                listeners : {
		                    change : function(rb, newValue, oldValue, options) {"use strict";
		                        var firstAdministrationDate, firstPatchUseDate,
		                        form = rb.up('form'), radioGroup;
		                        form.setLinkedFieldsState(['saeTipus'], newValue, true);
		                        radioGroup = form.down('radiogroup[name=saeTipus]');
	                            if (newValue) {
	                            	radioGroup.allowBlank = false;
	                            } else {
	                            	radioGroup.allowBlank = true;
	                            }
	                            radioGroup.validate();
			                    }
		                }
		            }, {
		                name : 'ae_serious',
		                xtype : 'radiofield',
		                boxLabel : _("Nem"),
		                inputValue : 2
		                
		            }]
		        }, {
		            xtype: 'monitorcheck'
		        }, {
		            xtype: 'fielddetailsbtn'
			 	},{
					xtype : 'radiogroup',
	           		historyLabel: _('Súlyos nemkívánatos esemény fajtája'),
	           		colspan : 3,
	                labelWidth : 200,
	                allowBlank : true,
	                width: 560,
	                height: 320,
	                name: 'saeTipus',
	                defaults : {
		                margin : '5 0 5 0',
		                labelWidth : 190,
		                maxWidth : 585,
		                allowBlanks : false
		            },
		            layout: {
			            type: 'table',
			            columns: 3
			        },
	                width : 685,
	                msgTarget : 'side',
	                items : [{
						xtype: 'component',
						width: 585,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						width: 32,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						height : 1,
						margin : '0 0 0 0'
				    },{
	                    name : 'saeTipus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Halál"),
	                    disabled : true,
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
	                            var form = field.up('form');
	                            form.setLinkedFieldsState([/*'halalokadisplayfield',*/'saeHalalOka'], this.getValue());                          
	               		}
	                },{
			            xtype: 'monitorcheck',
			            disabled : true,
			            referencedField : 'saeTipus'
			        },{
						xtype: 'fielddetailsbtn',
						referencedField : 'saeTipus'
				 	},{

						xtype : 'radiogroup',
		     			fieldLabel : _("A halál oka"),
		                labelWidth : 200,
		                padding : '0 0 0 12',
		                width: 560,
		                columns: [120,120,100],
		                disabled : true,
		                name: 'saeHalalOka',
		                items : [{
		                    name : 'saeHalalOka',
		                    xtype : 'radiofield',
		                    boxLabel : _("Kardiális"),
		                    inputValue : 1,
		                    handler : function(field) {"use strict";
		                            var form = field.up('form');
		                            form.setLinkedFieldsState(['saeHalalStentTrombozisa'], this.getValue());                          
		               		}
		                },{
		                    name : 'saeHalalOka',
		                    xtype : 'radiofield',
		                    boxLabel : _("Nem kardiális"),
		                    inputValue : 2 ,
		                    handler : function(field) {"use strict";
		                            var form = field.up('form');
		                            form.setLinkedFieldsState(['saeHalalNemKardialis'], this.getValue());                          
		               		}
		                },{
		                    name : 'saeHalalOka',
		                    xtype : 'radiofield',
		                    boxLabel : _("Ismeretlen"),
		                    inputValue : 3 
		                }]
				   	},{
			            xtype: 'monitorcheck',
			            disabled :true
			        },{
						xtype: 'fielddetailsbtn'
				 	},{
						xtype : 'radiogroup',
		                fieldLabel : _("Stent trombózis"),
		                labelWidth : 200,
		                disabled : true,
		                width: 560,
		                padding : '0 0 0 12',
		                columns: [120,120],
		                name: 'saeHalalStentTrombozisa',
		                items : [{
		                    name : 'saeHalalStentTrombozisa',
		                    xtype : 'radiofield',
		                    boxLabel : _("Igen"),
		                    inputValue : 1
		                },{
		                    name : 'saeHalalStentTrombozisa',
		                    xtype : 'radiofield',
		                    boxLabel : _("Nem"),
		                    inputValue : 2 
		                }]
				   	},{
			            xtype: 'monitorcheck',
			            disabled :true
			        },{
						xtype: 'fielddetailsbtn'
		           },{
		            	xtype : 'textarea',
	                    name : 'saeHalalNemKardialis',
	                    fieldLabel: _('Ha nem kardiális'),
	                    labelWidth : 200,
	                    emptyText : _("Kérem részletezze..."),
	                    maxLengthText : _("max 300 karakter"),
	                    validateMessage : _("Kérem adja meg a részleteket."),
	                    margin : '10 0 5 12',
	                    width : 560,
	                    height : 48,
	                    maxLength : 300,
	                    disabled : true,
	                    isPrintable : true,
	                    msgTarget: 'qtip'
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
					},{
	                    name : 'saeTipus',
	                    xtype : 'radiofield',
	                    disabled : true,
	                    boxLabel : _("Kórházi tartózkodást vagy a kórházi tartózkodás meghosszabbítását vonja maga után"),
	                    inputValue : 2,
	                    colspan : 3
					},{
	                    name : 'saeTipus',
	                    xtype : 'radiofield',
	                    disabled : true,
	                    boxLabel : _("Olyan esemény, amely az életet veszélyezteti"),
	                    inputValue : 3,
	                    colspan : 3
					},{
	                    name : 'saeTipus',
	                    xtype : 'radiofield',
	                    disabled : true,
	                    boxLabel : _("Tartós vagy jelentős cselekvőképtelenséget eredményez"),
	                    inputValue : 4,
	                    colspan : 3
					},{
	                    name : 'saeTipus',
	                    xtype : 'radiofield',
	                    disabled : true,
	                    boxLabel : _("Egyéb jelentős orvosi esemény"),
	                    inputValue : 5,
	                    colspan : 3
					},{
	                    name : 'saeTipus',
	                    xtype : 'radiofield',
	                    disabled : true,
	                    boxLabel : _("Születési rendellenesség"),
	                    inputValue : 6,
	                    colspan : 3
		            }]
		       	}]
		    },{
				xtype : 'fieldset',
		        title : _("A MACCE kimenetele"),
		        defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : false,
			 	 	msgTarget : 'side'
		       	},   
		        items : [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
		            xtype: 'radiogroup',
		            name: 'outcome',
		            historyLabel : _('A MACCE kimenetele'),
		            width: 605,
		            columns: 1,
		            defaults : {
		                margin : '8 0 10 0'
		            },
		            items: [{
		                xtype: 'radiofield',
		                boxLabel: _("megoldódott / befejeződött"),
		                name: 'outcome',
		                inputValue: 1
		            },{
		            	xtype: 'radiofield',
		            	boxLabel:  _("javul"),
		            	name: 'outcome',
		            	inputValue: 2
					},{
						xtype: 'radiofield',
						boxLabel: _("folyamatban"),
						name: 'outcome',
						inputValue: 3
					},{
						xtype: 'radiofield',
						boxLabel: _("végzetes"),
						name: 'outcome',
						inputValue: 4
					},{
						xtype: 'radiofield',
						boxLabel: _("ismeretlen"),
						name: 'outcome',
						inputValue: 5
					},{
		                xtype: 'radiofield',
		                boxLabel: _("megoldódott, de következménye van"),
		                name: 'outcome',
		                inputValue: 6,
		                listeners : {
		                    change : function(rb, newValue, oldValue, options) {"use strict";
		                        var form = rb.up('form');
		                        form.setLinkedFieldsState(['outcomewithConsequenceDescription'], newValue);
		                    }
		                }
		            }],
		            allowBlank : false,
		            msgTarget : 'side',
		            isPrintable : true
		        },{
		            xtype: 'monitorcheck'
		        },{
		            xtype: 'fielddetailsbtn'
		        },{
		            xtype : 'textarea',
		            name : 'outcomewithConsequenceDescription',
		            fieldLabel : _("Következmény leírása"),
		            labelWidth : 150,
		            emptyText : _("Kérjük jelezze..."),
		            maxLengthText : _("max 300 karakter"),
		            validateMessage : _("Kérem adja meg a következmény leírását."),
		            padding : '0 0 0 12',
		            width : 550,
		            height : 48,
		            maxLength : 300,
		            disabled : true,
		            isPrintable : true
		        },{
		            xtype: 'monitorcheck',
	              	disabled : true
		        },{
		           xtype: 'fielddetailsbtn'
		        }]
		    },{
		        xtype : 'fieldset',
		        title : _("További információk, megjegyzések, az esemény leírása"),
		     	defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : true,
			 	 	msgTarget : 'side'
		       	},
		        items : [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
		            xtype : 'textarea',
		            name : 'comments',
		            fieldLabel : _("Megjegyzések, információk"),
		            margin : '5 0 5 0',
		            labelWidth: 170,
		            width : 550,
		            height : 48,
		            maxLength : 300,
		            maxLengthText : _("max 300 karakter"),
		            isPrintable : true
		        }, {
		            xtype: 'monitorcheck'
		        }, {
					xtype: 'fielddetailsbtn'
			    }]
		    }]
		},{ 
            xtype : 'panel',
            title : _("Stroke"),
            subPanel : true,
            bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    }  
           	},
           	xDoScrollUp : true,
           	listeners : {
                'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{	
				xtype : 'fieldset',
				name : 'strokefieldset',
		        title : _("Stroke"),
		       	defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : false,
			 	 	msgTarget : 'side',
		            labelWidth : 170
		        },
		        items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					width: 32,
					margin : '0 0 0 0'
				},{
					xtype : 'radiogroup',
		            name : 'strokeType',
                    fieldLabel : _("Stroke típus"),
		            msgTarget : 'side',
		            isPrintable : true,
		            disabled : true,
		            columns: 1,
		            items : [{
		                name : 'strokeType',
		                xtype : 'radiofield',
		                boxLabel : _("Iszkémiás"),
		                inputValue : 1
		            },{
		                name : 'strokeType',
		                xtype : 'radiofield',
		                boxLabel : _("Hemorrhagiás"),
		                inputValue : 2
		            },{
		                name : 'strokeType',
		                xtype : 'radiofield',
		                boxLabel : _("Silent"),
		                inputValue : 3
					},{
		                name : 'strokeType',
		                xtype : 'radiofield',
		                boxLabel : _("Nem ismert"),
		                inputValue : 4
		            }]

		        },{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
		            xtype: 'fielddetailsbtn'
		        }]
		    }]
		},{
			xtype : 'panel',
            title : _("Miokardiális infarktus"),
            subPanel : true,
            bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    }  
           	},
           	xDoScrollUp : true,
           	listeners : {
                'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{	
				xtype : 'fieldset',
		        title : _("Miokardiális infarktus"),
		        name : 'mifieldset',
		       	defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : false,
			 	 	msgTarget : 'side',
		            labelWidth : 170
		        },
		        items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					width: 32,
					margin : '0 0 0 0'
				},{
	           		xtype : 'radiogroup',
		            name : 'MILokalizacioja',
		            fieldLabel : _("MI lokalizációja"),
		            margin : '5 0 5 10',
		            msgTarget : 'side',
		            isPrintable : true,
		            disabled : true,
		            columns: [120,120,100],
		            items : [{
		                name : 'MILokalizacioja',
		                xtype : 'radiofield',
		                boxLabel : _("Anterior"),
		                inputValue : 1
		            },{
		                name : 'MILokalizacioja',
		                xtype : 'radiofield',
		                boxLabel : _("Posterior"),
		                inputValue : 2
		            },{
		                name : 'MILokalizacioja',
		                xtype : 'radiofield',
		                boxLabel : _("Laterális"),
		                inputValue : 3
		            },{
		                name : 'MILokalizacioja',
		                xtype : 'radiofield',
		                boxLabel : _("Inferior"),
		                inputValue : 4
		            },{
		                name : 'MILokalizacioja',
		                xtype : 'radiofield',
		                boxLabel : _("Nem azonosítható"),
		                inputValue : 5
		            }]

		        },{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
		            xtype: 'fielddetailsbtn'
				},{
	           		xtype : 'radiogroup',
		            name : 'celLezioKapcsolat',
		            fieldLabel : _("Cél lézióval kapcsolatba hozható?"),
		            margin : '5 0 5 10',
		            msgTarget : 'side',
		            isPrintable : true,
		            disabled : true,
		            columns: [120,120,100],
		            items : [{
		                name : 'celLezioKapcsolat',
		                xtype : 'radiofield',
		                boxLabel : _("Igen"),
		                inputValue : 1
		            },{
		                name : 'celLezioKapcsolat',
		                xtype : 'radiofield',
		                boxLabel : _("Nem"),
		                inputValue : 2
		            },{
		                name : 'celLezioKapcsolat',
		                xtype : 'radiofield',
		                boxLabel : _("Nem ismert"),
		                inputValue : 3
		            }]
		        },{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
		            xtype: 'fielddetailsbtn'
				},{
	           		xtype : 'radiogroup',
		            name : 'stentTrombozis',
		            fieldLabel : _("Stent trombózis"),
		            margin : '5 0 5 10',
		            msgTarget : 'side',
		            isPrintable : true,
		            disabled : true,
		            columns: [120,120,100],
		            items : [{
		                name : 'stentTrombozis',
		                xtype : 'radiofield',
		                boxLabel : _("Igen"),
		                inputValue : 1
		            },{
		                name : 'stentTrombozis',
		                xtype : 'radiofield',
		                boxLabel : _("Nem"),
		                inputValue : 2
		            },{
		                name : 'stentTrombozis',
		                xtype : 'radiofield',
		                boxLabel : _("Nem ismert"),
		                inputValue : 3
		            }]
		        },{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
		            xtype: 'fielddetailsbtn'
				},{
	           		xtype : 'radiogroup',
		            name : 'vervetelTortent',
		            fieldLabel : _("Vérvétel történt"),
		            margin : '5 0 5 10',
		            msgTarget : 'side',
		            isPrintable : true,
		            disabled : true,
		            columns: [120,120],
		            items : [{
		                name : 'vervetelTortent',
		                xtype : 'radiofield',
		                boxLabel : _("Igen"),
		                inputValue : 1,
		                listeners : {
	                        change : function(rb, newValue, oldValue, options) {"use strict";
	                            var form = rb.up('form');
	                            form.setLinkedFieldsState([	'laborDatum',
	                            							'laborheader_displayfield',
	                            							'laborCk',
	                            							'laborCkmb',
	                            							'laborTroponinI',
	                            							'laborTroponinT'], newValue);
	                        }
	                    }
		            },{
		                name : 'vervetelTortent',
		                xtype : 'radiofield',
		                boxLabel : _("Nem"),
		                inputValue : 2
		            }]
		        },{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
		            xtype: 'fielddetailsbtn'
				},{
                	xtype: 'datetimefield',
		           	name : 'laborDatum',
		           	fieldLabel : _("Vérvétel dátuma") ,
		           	disabled : true,
		           	margin : '5 0 5 20',
	               	labelWidth : 180,
	              	width : 500,
	               	format : Kron.defaultDateFormat,
	              	maxValue: new Date()
                   
                },{
		            xtype: 'monitorcheck',
		            disabled : true
		        }, {
					xtype: 'fielddetailsbtn'
				},{
					xtype : 'displayfield',
                    name : 'laborheader_displayfield',
                    colspan: 3,
                    value : _("Legmagasabb laborértékek:"),
                    padding : '0 0 0 12',
                    disabled:true
				},{
					xtype : 'vitalsignfield',
			        name: 'laborCk',
			        fieldLabel: _("CK"),
		        	margin : '5 0 5 20',
	               	labelWidth : 180,
                   	width: 540,
                    valueMin : 0,
                    valueMax : 9999,
                    decimalPrecision : 2,
                    unit : _("IU/L"),
                    disabled : true,
                    isPrintable : true
               	},{
 		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
				},{
					xtype : 'vitalsignfield',
			        name: 'laborCkmb',
			        fieldLabel: _("CKMB"),
                   	width: 540,
                    valueMin : 0,
                    valueMax : 9999,
                    decimalPrecision : 2,
                    unit : _("IU/L"),
                    isPrintable : true,
                    disabled : true,
                  	margin : '5 0 5 20',
	               	labelWidth : 180
               	},{
 		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
				},{
					xtype : 'vitalsignfield',
			        name: 'laborTroponinI',
			        fieldLabel: _("Troponin I"),
                   	width: 540,
                    valueMin : 0,
                    valueMax : 9999,
                    decimalPrecision : 2,
                    unit : _("ng/ml"),
                    isPrintable : true,
                  	margin : '5 0 5 20',
                  	disabled : true,
	               	labelWidth : 180
               	},{
 		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
				},{
	           		xtype : 'radiogroup',
		            name : 'laborTroponinT',
		            fieldLabel : _("Troponin T"),
		            margin : '5 0 5 20',
		            msgTarget : 'side',
		            isPrintable : true,
		            disabled : true,
		            labelWidth : 180,
		            columns: [120,120,100],
		            items : [{
		                name : 'laborTroponinT',
		                xtype : 'radiofield',
		                boxLabel : _("Pozitív"),
		                inputValue : 1
		            },{
		                name : 'laborTroponinT',
		                xtype : 'radiofield',
		                boxLabel : _("Negatív"),
		                inputValue : 2
		            },{
		                name : 'laborTroponinT',
		                xtype : 'radiofield',
		                boxLabel : _("Nem történt"),
		                inputValue : 3
		            }]
		        },{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
		            xtype: 'fielddetailsbtn'
     			},{
           			xtype : 'radiogroup',
	                fieldLabel : _("Szed-e a beteg valamilyen szívgyógyszert jelenleg"),
	                labelWidth : 320,
	                columns: [120,60],
	                name: 'szivgyogyszer',
	                disabled : true,
	                msgTarget : 'side',
	                items : [{
	                    name : 'szivgyogyszer',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form'),checkBoxGroup;
                            form.setLinkedFieldsState([
                            	'szivgyogyszerdisplayfield',
                            	'aszpirin',
                            	'clopidogrel',
                            	'prasugrel',
                            	'ticlopidine',
                            	'egyebtrombocitaaggregaciogatlo',
                            	'acegatlo',
                            	'sztatin',
                            	'antikoagulans',
                            	'betablokkolo',
                            	'angiotensiniireceptorblokkolo',
                            	'nemsztatinlipidcsokkento',
                            	'egyebszivgyogyszer'
                            	], this.getValue());
                            checkBoxGroup = form.down('checkboxgroup[name=szivgyogyszercheckboxgroup]');
                            if (this.getValue()) {
                            	checkBoxGroup.allowBlank = false;
                            } else {
                            	checkBoxGroup.allowBlank = true;
                            }
                            checkBoxGroup.validate();
                       	}
	                },{
	                    name : 'szivgyogyszer',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
     			},{
           			xtype : 'displayfield',
           			name : 'szivgyogyszerdisplayfield',
                    value : _("Kérjük, jelöljön minden szívgyógyszert, amit a beteg aktuálisan szed!"),
                    colspan: 3,
                    disabled: true
     			},{
     				xtype: 'checkboxgroup',
               	    name : 'szivgyogyszercheckboxgroup',
 					colspan: 3,
	           		width : 660,
	           		allowBlank : true,
	        		msgTarget : 'side',
		            defaults : {
		                margin : '5 0 5 0',
		                maxWidth : 585
		            },
		            layout: {
			            type: 'table',
			            columns: 3
			        },
		            items: [{
						xtype: 'component',
						width: 590,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						width: 32,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						height : 1,
						margin : '0 0 0 0'
					},{	
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Aszpirin"),
	               		name: 'aszpirin',
	               		padding : '0 0 0 12',
	               		disabled : true
	               		
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Clopidogrel"),
	               		name: 'clopidogrel',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Prasugrel"),
	               		name: 'prasugrel',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Ticlopidine"),
	               		name: 'ticlopidine',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Egyéb trombocita aggregációt gátló gyógyszer"),
	               		name: 'egyebtrombocitaaggregaciogatlo',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("ACE gátló"),
	               		name: 'acegatlo',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Sztatin"),
	               		name: 'sztatin',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Antikoaguláns"),
	               		name: 'antikoagulans',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Béta-blokkoló"),
	               		name: 'betablokkolo',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Angiotensin II receptor blokkoló"),
	               		name: 'angiotensiniireceptorblokkolo',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Nem-sztatin lipidcsökkentő szer"),
	               		name: 'nemsztatinlipidcsokkento',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Egyéb, részletezze"),
	               		name: 'egyebszivgyogyszer',
	               		padding : '0 0 0 12',
	               		disabled : true,
	               		handler : function(field) {"use strict";
	                            var form = field.up('form');
	                            form.setLinkedFieldsState(['egyebSzivGyogyszerReszletek'], this.getValue());
	                   	}
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	              	},{
		            	xtype : 'textarea',
	                    name : 'egyebSzivGyogyszerReszletek',
	                    historyLabel : _('Egyéb szívgyógyszer részletezése'),
	                    emptyText : _("Kérem részletezze..."),
	                    maxLengthText : _("max 300 karakter"),
	                    validateMessage : _("Kérem adja meg a részleteket."),
	                    margin : '10 0 5 0',
	                    width : 560,
	                    height : 48,
	                    maxLength : 300,
	                    disabled : true,
	                    isPrintable : true,
	               		allowBlank: false,
	                    msgTarget: 'qtip'

					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
			        
			        }]
			    }]
			}]
		},{
			xtype : 'panel',
            title : _("Ismételt revaszkularizáció"),
            subPanel : true,
            bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    }  
           	},
           	xDoScrollUp : true,
           	listeners : {
                'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{	
				xtype : 'fieldset',
				name : 'irvfieldset',
		        title : _("Ismételt revaszkularizáció"),
		       	defaults : {
		        	margin : '5 0 5 0',
		    	 	maxWidth : 590,
		    	 	allowBlank : false,
			 	 	msgTarget : 'side',
		            labelWidth : 170
		        },
		        items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					width: 32,
					margin : '0 0 0 0'
				},{
					xtype : 'radiogroup',
	                fieldLabel : _("Iszkémiás státusz "),
	                columns: [100,100,100],
	                name: 'iszkemiasstatus',
	                disabled : true,
	                items : [{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Stabil angina"),
	                    inputValue : 1
	                },{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Instabil angina"),
	                    inputValue : 2, 
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['instabilangina'], this.getValue());
                       	}
	                },{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("STEMI"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
	           },{
					xtype : 'radiogroup',
	                fieldLabel : _("Troponin"),
	                width: 560,
	                columns: 2,
	                name: 'instabilangina',
	                disabled : true,
	                items : [{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("Pozitív"),
	                    inputValue : 1
	                },{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("Negatív"),
	                    inputValue : 2 
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
			 	},{
					xtype : 'radiogroup',
	                fieldLabel : _("Revaszkularizáció típusa"),
	                labelWidth : 200,
	                width: 560,
	                columns: [120,120, 100],
	                disabled :true,
	                name: 'revaszkularizacioTipus',
	                items : [{
	                    name : 'revaszkularizacioTipus',
	                    xtype : 'radiofield',
	                    boxLabel : _("CABG"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['surgosCABG'], this.getValue());
                   		}
	                },{
	                    name : 'revaszkularizacioTipus',
	                    xtype : 'radiofield',
	                    boxLabel : _("PCI"),
	                    inputValue : 2 
	                },{
	                    name : 'revaszkularizacioTipus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
			 	},{
					xtype : 'radiogroup',
	                fieldLabel : _("Sürgős CABG"),
	                labelWidth : 200,
	                width: 560,
	                columns: [120,120,100],
	                disabled :true,
	                name: 'surgosCABG',
	                items : [{
	                    name : 'surgosCABG',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['surgosCABGOka'], this.getValue());
                   		}
	                },{
	                    name : 'surgosCABG',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                },{
	                    name : 'surgosCABG',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
			 	},{
					xtype : 'radiogroup',
	                fieldLabel : _("Oka"),
	                labelWidth : 80,
	                width: 560,
	                columns: [120,120,150,60],
	                disabled :true,
	                name: 'surgosCABGOka',
	                items : [{
	                    name : 'surgosCABGOka',
	                    xtype : 'radiofield',
	                    boxLabel : _("Ér disszekció"),
	                    inputValue : 1
	                },{
	                    name : 'surgosCABGOka',
	                    xtype : 'radiofield',
	                    boxLabel : _("Ér elzáródás"),
	                    inputValue : 2 
	                },{
	                    name : 'surgosCABGOka',
	                    xtype : 'radiofield',
	                    boxLabel : _("Kezelés sikertelensége"),
	                    inputValue : 3 
	                },{
	                    name : 'surgosCABGOka',
	                    xtype : 'radiofield',
	                    boxLabel : _("Egyéb"),
	                    inputValue : 4 
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
     			},{
           			xtype : 'displayfield',
           			name: 'ismeteltRevaszkularizacioDisplayfield',
                    value : _("Ismételt revaszkularizáció az alábbiakon alapul (jelölje az összest):"),
                    padding : '0 0 0 12',
                    disabled : true,
                    colspan: 3
               
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Anginás státusz"),
               		name: 'anginasStatusz',
               		padding : '0 0 0 12',
               		disabled : true
               		
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("pozitív stressz teszt vagy azzal egyenértékű"),
               		name: 'pozitivStressz',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("EKG változás"),
               		name: 'EKGvaltozas',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Ismételt angiográfia"),
               		name: 'ismeteltAngiografia',
               		padding : '0 0 0 12',
               		disabled : true
 				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
    			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Egyéb, részletezze"),
               		name: 'egyebRevaszkAlap',
               		padding : '0 0 0 12',
               		disabled : true,
               		handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['egyebRevaszkAlapReszletek'], this.getValue());
                   	}
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
              	},{
              		xtype : 'textarea',
                    name : 'egyebRevaszkAlapReszletek',
                    historyLabel : _('Egyéb részletezés'),
                    emptyText : _("Kérem részletezze..."),
                    maxLengthText : _("max 300 karakter"),
                    validateMessage : _("Kérem adja meg a részleteket."),
                    margin : '10 0 5 0',
                    width : 560,
                    height : 48,
                    maxLength : 300,
                    disabled : true,
                    isPrintable : true,
                    msgTarget: 'qtip'

				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
			 	},{
					xtype : 'radiogroup',
	                fieldLabel : _("Cél lézió érintettsége"),
	                labelWidth : 200,
	                width: 560,
	                columns: [120,120,100],
	                disabled :true,
	                name: 'celLezioErintettseg',
	                items : [{
	                    name : 'celLezioErintettseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'celLezioErintettseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                },{
	                    name : 'celLezioErintettseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype : 'vitalsignfield',
                    name : 'oldalagiStenozisAtmero',
                    fieldLabel :  _("% átmérő oldalági sztenózis"),
                    labelWidth : 200,
                    width: 550,
                    valueMin : 0,
                    valueMax : 100,
                    unit : _("%"),
                    isPrintable : true,
                    disabled :true,
                    allowBlank : false	
           		},{
		            xtype: 'monitorcheck',
		            disabled : true
		      	},{
					xtype: 'fielddetailsbtn'
               	},{
               		xtype : 'vitalsignfield',
                    name : 'foagiStenozisAtmero',
                    fieldLabel :  _("% átmérő főági sztenózis"),
                    labelWidth : 200,
                    width: 550,
                    valueMin : 0,
                    valueMax : 100,
                    unit : _("%"),
                    isPrintable : true,
                    disabled :true,
                    allowBlank : false	
           		},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
 
				}]
            }]
		}]
	}]

});
