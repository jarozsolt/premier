/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MedicalHistoryForm.js
 *
 * Medical History form - panel in Patient view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 * @todo Decrease the height of the dialog
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.MedicalHistoryForm', {
	extend : 'Kron.view.patient.forms.GeneralPatientForm',
	requires:  [  'Kron.view.patient.forms.GeneralPatientForm' ],
	alias : 'widget.medicalhistoryForm',
	id : 'MedicalHistoryForm',
    formWidth : 720,
    formHeight : 400,
    formTitle : Kron.formNames.MedicalHistory,
    formIconCls : 'icon-medicalhistory-dlg_small',
    autoScroll : true,
	bodyStyle:'background-color:#ffffff;padding:10px',
// Ha nincs tab panel, vagy nem akarom ott jelezni, ha van nem valid field, akkor ezt true-ra kell állítani
    blockErrorMarkingOnTab : true, 
    
    defaults : {
        width: 685,
        labelWidth: 400,
        layout: {
            type: 'table',
            columns: 3
        }
    },  
    
    // We can save the form only if the diagnosis field is not empty
    canSave : function() {"use strict";
        var fieldDiagnosis = this.down('[name=diagnosis]'); 
        if (fieldDiagnosis) {
            if (fieldDiagnosis.getValue() !== null && fieldDiagnosis.getValue() !== '') {
                return true;
            }
        }
        return false;
    },
    
    // Alert an error message if the diagnosis is not filled
    alertSaveError : function() {"use strict";
        Ext.Msg.show({
            title : _("Hiba"),
            msg : _("A 'Diagnózis' mezőt kötelező kitölteni az űrlap elmentéséhez!"),
            buttons : Ext.Msg.OK,
            icon : Ext.MessageBox.ERROR,
            animateTarget: 'dlgAccept'});
    },
    
	items : [{
   		xtype : 'displayfield',
   		colspan: 3,
        doNotSave : true,
        fieldCls : 'forms-message-info',
        padding : '2 2 2 2',
        value : _("Kérjük, hogy a jelenleg is fennálló összes, valamint az elmúlt 3 hónapban fennállt, a beteg és a vizsgálat szempontjából jelentős orvosi betegségeket, tüneteket tüntesse fel a beteg kórelőzménye és egyéb társbetegségei között!")
	},{
        xtype:'fieldset',
        border: 0,
		defaults : {
        	margin : '10 0 5 0',
    	 	maxWidth : 590,
    	 	allowBlank : false,
    	 	labelWidth : 200,
	 	 	msgTarget : 'side'
       	},
        items: [{
			xtype: 'component',
			width: 590,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			width: 32,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			height : 1,
			margin : '0 0 0 0'
        },{ 
			xtype : 'textfield',
            fieldLabel : _("Diagnózis") + Kron.config.labelMandatoryMark,
            width : 585,
            name : 'diagnosis',
            isPrintable : true
        }, {
            xtype: 'monitorcheck'
        }, {
            xtype: 'fielddetailsbtn'
        }, {
            xtype : 'notexactdate',
            name : 'startDate',
            fieldLabel : _("Kezdete"),
            width : 310,
            format : Kron.defaultDateFormat,    
            isPrintable : true,
            futureDate : false,
            alertEndDateConflict : 'endDate'
            
           
        }, {
            xtype: 'monitorcheck'
        }, {
            xtype: 'fielddetailsbtn'
        }, {
            xtype : 'ongoingdate',
            name : 'endDate',
            fieldLabel : _("Vége"),
            width : 560,
            ongoingboxLabel : _("folyamatban"),
            futureDate : false,
            alertStartDateConflict : 'startDate',
            validateMessage : _("Kérem töltse ki a 'Vége' dátumot vagy állítsa 'folyamatban'-ra."),      
            isPrintable : true
            
        }, {
            xtype: 'monitorcheck'
        }, {
            xtype: 'fielddetailsbtn'
        }, {
            xtype : 'radiogroup',
            name : 'serious',
            fieldLabel : _("Súlyosság"),
            labelWidth : 150,
            columns : [90,90,90,130],
            items : [{
                name : 'serious',
                xtype : 'radiofield',
                boxLabel : _("enyhe"),
                inputValue : 1
            }, {
                name : 'serious',
                xtype : 'radiofield',
                boxLabel : _("mérsékelt"),
                inputValue : 2
            }, {
                name : 'serious',
                xtype : 'radiofield',
                boxLabel : _("súlyos"),
                inputValue : 3
            }, {
                name : 'serious',
                xtype : 'radiofield',
                boxLabel : _("nem alkalmazható"),
                inputValue : 4
            }],
            msgTarget : 'side',
            isPrintable : true
        }, {
            xtype: 'monitorcheck'
        }, {
            xtype: 'fielddetailsbtn'
        }, {
            xtype : 'radiogroup',     
            name : 'requireTreatment',
            fieldLabel : _("Kezelést igényel?"),
            labelWidth : 150,
            columns : [90,90],
            items : [{
                name : 'requireTreatment',
                xtype : 'radiofield',
                boxLabel : _("Igen"),
                inputValue : 1,
                listeners : {
                    change : function(rb, newValue, oldValue, options) {"use strict";
                        if (newValue === true)
                        {
                            var form = rb.up('form');
                            if (form !== undefined) {
                                form.showMedicationWarning();
                            }
                        }
                    }
                }
            }, {
                name : 'requireTreatment',
                xtype : 'radiofield',
                boxLabel : _("Nem"),
                inputValue : 2
            }],
            msgTarget : 'side',
            isPrintable : true
        }, {
            xtype: 'monitorcheck'
        }, {
            xtype: 'fielddetailsbtn'
        }]
    }]
}); 