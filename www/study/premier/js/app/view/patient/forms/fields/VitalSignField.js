/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * VitalSignField.js
 *
 * Container for a Vital Sign fields
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.VitalSignField', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.vitalsignfield',
    layout : 'hbox',
    validateMessage : _("Kérem töltse ki a mezőt vagy állítsa 'Nem vizsgált'-ra."),
    isXFormField : true,
    defaults : {
        hideLabel : true,
        padding : '5 0 5 0'
    },
    items : [],

    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    strictInitComponent : function() { "use strict";
        var me = this;
        
        me.items = [{
            xtype : 'numberfield',
            doNotSave : true,
            width : 50,
            hideTrigger : true,
            disabled : me.disabled,
            minValue : me.valueMin,
            maxValue : me.valueMax,
            allowBlank : me.allowBlank,
            decimalPrecision : me.decimalPrecision,
            isOptional : (me.allowBlank===undefined||me.allowBlank===true)?true:false,
            listeners: {
                'change': function(field, newValue) {
                    var parent = field.up('fieldcontainer');
                    if (parent.onValueChanged !== undefined) {
                        parent.onValueChanged(field, newValue);
                    }
                }
            }
            // TODO: a validálást megoldani, jelenleg nem pirosít, ha nincs kitöltve
            /*,
            getErrors : function(value) {//Nem használhatunk strict módot a callParent miatt
                var parent = this.up('fieldcontainer'),
                checkField = this.nextSibling('checkboxfield');
                if (parent.allowBlank === false) {
                    if ((value === null || value === '') && checkField.getValue() === false) {
                        Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
                        return [parent.validateMessage];
                    }
                    Ext.form.Field.prototype.clearInvalid.call(this);
                }
                // Meghívjuk az eredeti hibakezelőt a min-max érték ellenőrzés miatt
                //return this.callParent(arguments);
                return [];
            }*/
   //     }, {
    //        xtype: 'splitter'
        }, {
            xtype : 'displayfield',
            width : 70,
            doNotSave : true,
            unitvalue : me.unit,
            padding : '5 0 5 5',
            listeners : {
                //the ExtJS API says the render event will be called with one parameter:
                //   (Ext.Component this)
                'render' : function(thisCmp) {
                    if (thisCmp.unitvalue !== undefined) {
                        thisCmp.setValue(thisCmp.unitvalue);
                    }
                }
            } 
   //     }, {
   //         xtype : 'splitter',
     //       flex : 1
        }, {
            xtype: 'checkboxfield',
            boxLabel: me.naboxLabel?me.naboxLabel:_("Nem vizsgált"),
            doNotSave : true,
            disabled : me.disabled,
            width : 130,
            inputValue: 'n.a.',
            /*getErrors : function(value) {"use strict";
                var parent = this.up('fieldcontainer'),
                valueField = this.previousSibling('numberfield')
                if (parent.allowBlank === false) {
                    if ((valueField.getValue() === null || valueField.getValue() === '') && value === false) {
                        Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
                        return [parent.validateMessage];;
                    }
                    Ext.form.Field.prototype.clearInvalid.call(this);
                }
                return [];
            },*/
            handler : function(field) {"use strict";
                var valueField = this.previousSibling('numberfield'),
                displayField = this.previousSibling('displayfield');
                if (this.getValue())
                {
                    valueField.setValue('');
                    valueField.disable(true);
                    valueField.allowBlank = true;
                }
                else
                {
                    valueField.enable(true);
                    valueField.allowBlank = valueField.isOptional;
                    valueField.validate();
                }
            }
        },{
            xtype: 'hiddenfield',
            name : me.name,
       //     disabled : me.disabled,
            parentContainer : me,
            setValue : function (value) { this.parentContainer.setValue(value); },
            getValue : function () { return this.parentContainer.getValue(); },
            isValid : function(){return this.parentContainer.isValid(); }
        }];
    },
    
    getValue: function() {"use strict";
        var me = this, value,
        valueField = me.down('numberfield'),
        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            return 'n.a.';
        }
        return valueField.getValue();
    },
    setValue: function(value) {"use strict";
        var me = this,
        valueField = me.down('numberfield'),
        checkField = me.down('checkboxfield');
        if (value === 'n.a.')
        {
            checkField.setValue(true);
            valueField.setValue(null);
            valueField.disable(true);
            valueField.clearInvalid();
        }
        else if (value !== undefined)
        {
            checkField.setValue(false);
            valueField.setValue(value);
           
        }
        if (me.onValueChanged !== undefined) {
            me.onValueChanged(valueField, value);
        }
        return me;
    },
    
    isValid: function() {"use strict";
        var me = this,
        valueField = me.down('numberfield'),
        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            return true;
        } else {
        	
        	return valueField.isValid();
        }
    },
     getFormattedValue: function(fieldValue) {"use strict";
        var me = this, value,
        valueField = me.down('numberfield'),
        checkField = me.down('checkboxfield');
        
        if (fieldValue !== undefined && fieldValue !== null ) {
        	if (fieldValue ==='n.a.') {
        		 return 'n.a.';
        	} else {
        		return fieldValue;
        	}
        } else {
        	if (checkField.getValue() === true)
	        {
	            return 'n.a.';
	        }
	    }
        return valueField.getValue();
    },
    
	getName: function(){"use strict";
    	if (this.name != undefined && this.name !=null ) {
    		return this.name;
    	} else {
    		return null;
    	}
    	
    }
    

    /*
    setDisabled : function(disabled) {
   		var me = this,
	        valueField = me.down('numberfield'),
	        checkField = me.down('checkboxfield'),
	        hiddenField = me.down('hiddenfield');
	    valueField.setDisabled(disabled);
	    checkField.setDisabled(disabled);
	    hiddenField.setDisabled(disabled);
        return this[disabled ? 'disable': 'enable']();
    
   	},
   	validate : function() {
   		var me = this,
	        valueField = me.down('numberfield'),
	        checkField = me.down('checkboxfield'),
	        hiddenField = me.down('hiddenfield');
	   	valueField.validate();
	   	checkField.validate();
	   	hiddenField.validate();
   	},
   	reset: function() {"use strict";
        var me = this,
	        valueField = me.down('numberfield'),
	        checkField = me.down('checkboxfield'),
	        hiddenField = me.down('hiddenfield');
       	valueField.reset();
	   	checkField.reset();
	   	hiddenField.reset();
        return me;
	}*/
   
});
