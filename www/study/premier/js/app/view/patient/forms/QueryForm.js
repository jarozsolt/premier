/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * QueryForm.js
 *
 * Query form - panel in Message view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.QueryForm', {
    extend : 'Ext.form.Panel',
    alias : 'widget.queryForm',

    layout : {
        type : 'vbox',
        align : 'stretch'
    },
    border : false,
    fieldDefaults : {
        labelWidth : 150
    },
    defaults : {
        margins : '0 0 0 0'
    },
	frame : true,
    
	
    
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },

	strictInitComponent: function() { "use strict";
 //       this.historyTpl = Ext.create('Ext.XTemplate', this.TplMarkup);
    },

    items : [{
        xtype : 'fieldset',
        title : _("Kiválasztott mező"),
        layout : {
            type : 'vbox',
            align : 'stretch'
        },
        items : [{
            xtype: 'displayfield',
            fieldLabel: '',
            labelWidth: 400,
            width: 300,
            id : 'selectedfield'
      
        }]
    }, {
        xtype: 'fieldcontainer',
        layout: 'hbox',        
        items : [{
            xtype: 'splitter',
            width : 20
        },{
            fieldLabel : _("Státusz"),
            xtype : 'displayfield',
            name : 'status',
            id : 'queryform_field_status',
            allowBlank : true,
            readOnly : true
        }/*, {
            xtype: 'splitter',
            width : 50
        },{
            xtype: 'button',
            text : _("Jóváhagyás"),
            id : 'approveFieldBtn',
            tooltip : _("Mező jóváhagyása"),
            scale : 'medium'
        }*/]
    }]

    
}); 
