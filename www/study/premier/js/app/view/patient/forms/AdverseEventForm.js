/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * AdverseEventForm.js
 *
 * AE form - Adverse events form
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 * @todo Cancel-re és Close-ra kerdest feltenni, ha valtozott valami a dialoguson
 * @todo Nem engedni elmenteni, ha legalabb a diagnozist nem toltotte ki
 * @todo Figyelmeztetés Save-re, ha nem toltott ki minen mezot
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.AdverseEventForm', {
    extend : 'Kron.view.patient.forms.GeneralPatientForm',
    alias : 'widget.adverseeventform',
    id : 'AdverseEventForm',
    formWidth : 760,
    formHeight : 600,
    formTitle :Kron.formNames.AdverseEvent,
    formIconCls : 'icon-adverseevent-dlg_small',
    autoScroll : true,
    bodyStyle:'background-color:#ffffff;padding:10px',
    frame : true,
    monitorValid : true,
    buttonAlign : 'center',
    bodyPadding : 5,
   // Ha nincs tab panel, vagy nem akarom ott jelezni, ha van nem valid field, akkor ezt true-ra kell állítani
    blockErrorMarkingOnTab : true,

    defaults : {
        width: 685,
        labelWidth: 400,
        layout: {
            type: 'table',
            columns: 3
        }
    },
    // We can save the form only if the diagnosis field is not empty
    canSave : function() {"use strict";
        var fieldDiagnosis = this.down('[name=diagnosis]');
        if (fieldDiagnosis) {
            if (fieldDiagnosis.getValue() !== null && fieldDiagnosis.getValue() !== '') {
                return true;
            }
        }
        return false;
    },

    // Alert an error message if the diagnosis is not filled
    alertSaveError : function() {"use strict";
        Ext.Msg.show({
            title : _("Hiba"),
            msg : _("A 'Diagnózis vagy tünet' mezőt kötelező kitölteni a Nemkívánatos Esemény elmentéséhez!"),
            buttons : Ext.Msg.OK,
            icon : Ext.MessageBox.ERROR,
            animateTarget: 'dlgAccept'});
    },


    items : [{
        xtype : 'fieldset',
        title : _("A nemkívánatos esemény diagnózisa vagy tünete (amennyiben lehetséges adja meg a diagnózist)"),
        defaults : {
        	margin : '5 0 5 0',
    	 	maxWidth : 590,
    	 	allowBlank : false,
	 	 	msgTarget : 'side'
       	},
        items: [{
			xtype: 'component',
			width: 590,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			width: 32,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			height : 1,
			margin : '0 0 0 0'
		},{
            xtype : 'textfield',
            fieldLabel : _("Diagnózis vagy tünet") + Kron.config.labelMandatoryMark,
            name : 'diagnosis',
            labelWidth: 170,
            width : 605,
            margin : '5 0 5 0',
            allowBlank : false,
            isPrintable : true
        }, {
            xtype: 'monitorcheck'
        }, {
            xtype: 'fielddetailsbtn'
        }]
    }, {
        xtype : 'fieldset',
        title : _("A nemkívánatos esemény kezdete és vége"),
       	defaults : {
        	margin : '5 0 5 0',
    	 	maxWidth : 590,
    	 	allowBlank : false,
	 	 	msgTarget : 'side',
            labelWidth : 170
       	},
        items: [{
			xtype: 'component',
			width: 590,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			width: 32,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			height : 1,
			margin : '0 0 0 0'
		},{
            xtype : 'datefield',
            name : 'startDate',
            width : 301,
            format : Kron.defaultDateFormat,
            fieldLabel : _("Kezdete"),
            maxValue: new Date(),
            isPrintable : true
        }, {
            xtype: 'monitorcheck'
        }, {
            xtype: 'fielddetailsbtn'
        }, {
            xtype : 'ongoingdate',
            name : 'endDate',
            fieldLabel : _("Vége"),
            width: 605,

            margin : '5 0 5 0',
            dateFormat : Kron.defaultDateFormat,
            futureDate : false,
            ongoingboxLabel : _("folyamatban"),
            validateMessage : _("Kérem töltse ki a 'Vége' dátumot vagy állítsa 'folyamatban'-ra."),
            isPrintable : true
        }, {
            xtype: 'monitorcheck'
        }, {
            xtype: 'fielddetailsbtn'
        }]
    }, {
        xtype : 'fieldset',
        title : _("Súlyosság"),
        defaults : {
        	margin : '5 0 5 0',
    	 	maxWidth : 590,
    	 	allowBlank : false,
	 	 	msgTarget : 'side'
       	},
        items : [{
 				xtype: 'component',
				width: 590,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				width: 32,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				height : 1,
				margin : '0 0 0 0'
			},{
           		xtype : 'radiogroup',
           		width: 605,
	            name : 'severity',
	            items : [{
	                name : 'severity',
	                xtype : 'radiofield',
	                boxLabel : _("enyhe"),
	                inputValue : 1
	            }, {
	                name : 'severity',
	                xtype : 'radiofield',
	                boxLabel : _("közepesen súlyos"),
	                inputValue : 2
	            }, {
	                name : 'severity',
	                xtype : 'radiofield',
	                boxLabel : _("súlyos"),
	                inputValue : 3
	            }],
	            msgTarget : 'side',
	            isPrintable : true
	        }, {
	            xtype: 'monitorcheck'
	        }, {
	            xtype: 'fielddetailsbtn'
        	}]
    	},{
	        xtype : 'fieldset',
	        title : _("Kapcsolat a beültetett eszközzel"),
	        defaults : {
	        	margin : '5 0 5 0',
	    	 	maxWidth : 590,
	    	 	allowBlank : false,
		 	 	msgTarget : 'side'
	       	},
	        items : [{
				xtype: 'component',
				width: 590,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				width: 32,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				height : 1,
				margin : '0 0 0 0'
			},{
		        xtype : 'radiogroup',
		        name : 'relation',
	            width: 605,
	            items : [{
	                name : 'relation',
	                xtype : 'radiofield',
	                boxLabel : _("nincs kapcsolat"),
	                inputValue : 1
	            }, {
	                name : 'relation',
	                xtype : 'radiofield',
	                boxLabel : _("lehetséges"),
	                inputValue : 2
	            }, {
	                name : 'relation',
	                xtype : 'radiofield',
	                boxLabel : _("valószínűsíthető"),
	                inputValue : 3
	            }, {
	                name : 'relation',
	                xtype : 'radiofield',
	                boxLabel : _("egyértelmű"),
	                inputValue : 4
	            }],
	            msgTarget : 'side',
	            isPrintable : true
	        },{
	            xtype: 'monitorcheck'
	        },{
	            xtype: 'fielddetailsbtn'
	        }]
	    },{
	        xtype : 'fieldset',
	        title : _("Megtett intézkedések"),
	       	defaults : {
	    	 	allowBlank : true,
		 	 	msgTarget : 'side'
	       	},
	       	layout: 'vbox',
	        items : [{
	            xtype: 'checkboxgroup',
	            width : 660,
	            defaults : {
	                margin : '5 0 5 0',
	                maxWidth : 585
	            },
	            layout: {
		            type: 'table',
		            columns: 3
		        },
	            items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
	            	xtype: 'checkboxfield',
	            	boxLabel:  _("A beültetett eszköz eltávolítása"),
	            	name: 'actionTaken-DeviceRemoved'
	            },{
	       			xtype: 'monitorcheck'
				},{
					xtype: 'fielddetailsbtn'
				},{
					xtype: 'checkboxfield',
					boxLabel: _("A beültetett eszköz változatlan"),
					name: 'actionTaken-DeviceNotChanged'
	            },{
	       			xtype: 'monitorcheck'
				},{
					xtype: 'fielddetailsbtn'
				},{
					xtype: 'checkboxfield',
					boxLabel: _("Ismeretlen"),
					name: 'actionTaken-Unknown'
	            },{
	       			xtype: 'monitorcheck'
				},{
					xtype: 'fielddetailsbtn'
				},{
					xtype: 'checkboxfield',
					boxLabel: _("Nem alkalmazható, a beteg nem kapta meg az eszközt"),
					name: 'actionTaken-NotApplicable'
	            },{
	       			xtype: 'monitorcheck'
				},{
					xtype: 'fielddetailsbtn'
				},{
	                xtype: 'checkboxfield',
	                boxLabel: _("Egyéb kezelést is kapott a beteg"),
	                name: 'actionTaken-OtherMedication',
	                listeners : {
	                    change : function(rb, newValue, oldValue, options) {"use strict";
	                        if (newValue === true) {
	                            var form = rb.up('form');
	                            if (form !== undefined) {
	                                form.showMedicationWarning();
	                            }
	                        }
	                    }
	                }
	            },{
	       			xtype: 'monitorcheck'
				},{
					xtype: 'fielddetailsbtn'
				},{
	                xtype: 'checkboxfield',
	                boxLabel: _("Egyéb"),
	                name: 'actionTaken-Other',
	                handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['otherActionDetails'], this.getValue());
                   	}
	            },{
	       			xtype: 'monitorcheck'
				},{
					xtype: 'fielddetailsbtn'
               },{
	            	xtype : 'textarea',
                    name : 'otherActionDetails',
                    historyLabel : _('Egyéb intézkedés részletezése'),
                    emptyText : _("Kérem részletezze..."),
                    maxLengthText : _("max 300 karakter"),
                    validateMessage : _("Kérem adja meg a részleteket."),
                    margin : '10 0 5 14',
                    width : 560,
                    height : 48,
                    maxLength : 300,
                    disabled : true,
                    isPrintable : true,
                    msgTarget: 'qtip',
                    allowBlank : false
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
	            }],
		        allowBlank : false,
		        msgTarget : 'side',
		        isPrintable : true
		       	}]

	    },{
	        xtype : 'fieldset',
	        title : _("Az esemény kimenetele"),
	        defaults : {
	        	margin : '5 0 5 0',
	    	 	maxWidth : 590,
	    	 	allowBlank : false,
		 	 	msgTarget : 'side'
	       	},
	        items : [{
				xtype: 'component',
				width: 590,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				width: 32,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				height : 1,
				margin : '0 0 0 0'
			},{
	            xtype: 'radiogroup',
	            name: 'outcome',
	            historyLabel : _('Az esemény kimenetele'),
	            width: 605,
	            columns: 1,
	            defaults : {
	                margin : '8 0 10 0'
	            },
	            items: [{
	                xtype: 'radiofield',
	                boxLabel: _("megoldódott / befejeződött"),
	                name: 'outcome',
	                inputValue: 1
	            },{
	            	xtype: 'radiofield',
	            	boxLabel:  _("javul"),
	            	name: 'outcome',
	            	inputValue: 2
				},{
					xtype: 'radiofield',
					boxLabel: _("folyamatban"),
					name: 'outcome',
					inputValue: 3
				},{
					xtype: 'radiofield',
					boxLabel: _("végzetes"),
					name: 'outcome',
					inputValue: 4
				},{
					xtype: 'radiofield',
					boxLabel: _("ismeretlen"),
					name: 'outcome',
					inputValue: 5
				},{
	                xtype: 'radiofield',
	                boxLabel: _("megoldódott, de következménye van"),
	                name: 'outcome',
	                inputValue: 6,
	                listeners : {
	                    change : function(rb, newValue, oldValue, options) {"use strict";
	                        var form = rb.up('form');
	                        form.setLinkedFieldsState(['outcomewithConsequenceDescription'], newValue);
	                    }
	                }
	            }],
	            allowBlank : false,
	            msgTarget : 'side',
	            isPrintable : true
	        },{
	            xtype: 'monitorcheck'
	        },{
	            xtype: 'fielddetailsbtn'
	        },{
	            xtype : 'textarea',
	            name : 'outcomewithConsequenceDescription',
	            fieldLabel : _("Következmény leírása"),
	            labelWidth : 150,
	            emptyText : _("Kérjük jelezze..."),
	            maxLengthText : _("max 300 karakter"),
	            validateMessage : _("Kérem adja meg a következmény leírását."),
	            padding : '0 0 0 12',
	            width : 550,
	            height : 48,
	            maxLength : 300,
	            disabled : true,
	            isPrintable : true
	        }, {
	            xtype: 'monitorcheck'
	        }, {
	            xtype: 'fielddetailsbtn'
	        }]
	    },{
	        xtype : 'fieldset',
	        title : _("Súlyos nemkívánatos esemény"),
	        defaults : {
	        	margin : '5 0 5 0',
	    	 	maxWidth : 590,
	    	 	allowBlank : false,
		 	 	msgTarget : 'side'
	       	},
	        items : [{
				xtype: 'component',
				width: 590,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				width: 32,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				width: 36,
				height : 1,
				margin : '0 0 0 0'
			},{
	            xtype : 'radiogroup',
	            fieldLabel : _("Súlyos nemkívánatos esemény?"),
	            labelWidth : 305,
	            width: 660,
	            name : 'ae_serious',
	            items : [{
	                name : 'ae_serious',
	                xtype : 'radiofield',
	                boxLabel : _("Igen"),
	                inputValue : 1,
	                listeners : {
	                    change : function(rb, newValue, oldValue, options) {"use strict";
	                        var form = rb.up('form'),radioGroup;
	                        form.setLinkedFieldsState(['seriousType'], newValue, true);
	                        radioGroup = form.down('radiogroup[name=seriousType]');
                            if (newValue) {
                            	radioGroup.allowBlank = false;
                            } else {
                            	radioGroup.allowBlank = true;
                            }
                            radioGroup.validate();
	                    }
	                }
	            }, {
	                name : 'ae_serious',
	                xtype : 'radiofield',
	                boxLabel : _("Nem"),
	                inputValue : 2

	            }],
	            allowBlank : false,
	            msgTarget : 'side',
	            isPrintable : true
	        }, {
	            xtype: 'monitorcheck'
	        }, {
	            xtype: 'fielddetailsbtn'
	        }, {
	           xtype : 'fieldset',
	            title : _("Súlyosság kritériumai"),
				layout : 'vbox',
	            width : 660,
	            maxWidth : 680,
	            colspan: 3,

	            items : [{
	                xtype : 'radiogroup',
	                name : 'seriousType',
	                historyLabel : _('A súlyosság kritériuma'),
		           	defaults : {
		                margin : '5 0 5 0',
		                maxWidth : 580
		            },
		            layout: {
			            type: 'table',
			            columns: 3
			        },
	                width : 635,
	                height : 341,
	                msgTarget : 'side',
	                items : [{
						xtype: 'component',
						width: 575,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						width: 32,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						height : 1,
						margin : '0 0 0 0'
					},{
	                    xtype : 'radiofield',
	                    boxLabel : _("hospitalizáció vagy annak meghosszabbodása"),
	                    name : 'seriousType',
	                    disabled : true,
	                    inputValue : 1
		           	},{
		       			xtype: 'monitorcheck',
		       			referencedField: 'seriousType'
					},{
						xtype: 'fielddetailsbtn',
						referencedField: 'seriousType'
	               	},{
	                    xtype : 'radiofield',
	                    boxLabel : _("tartós vagy jelentős rokkantság/munkaképtelenség"),
	                    name : 'seriousType',
	                    disabled : true,
	                    inputValue : 2
		 			},{
						xtype: 'component'
					},{
						xtype: 'component'
	               	},{
	                    xtype : 'radiofield',
	                    boxLabel : _("veleszületett rendellenesség / születési károsodás"),
	                    name : 'seriousType',
	                    disabled : true,
	                    inputValue : 3
		 			},{
						xtype: 'component'
					},{
						xtype: 'component'
	                },{
	                    xtype : 'radiofield',
	                    boxLabel : _("jelentős orvosi esemény"),
	                    name : 'seriousType',
	                    disabled : true,
	                    inputValue : 4,
	                    listeners : {
	                        change : function(rb, newValue, oldValue, options) {"use strict";
	                            var form = rb.up('form');
	                            form.setLinkedFieldsState(['medicalEventDescription'], newValue);
	                        }
	                    }
	 	 			},{
						xtype: 'component'
					},{
						xtype: 'component'
	               	},{
	                    xtype : 'textarea',
	                    name : 'medicalEventDescription',
	                    emptyText : _("Kérem adja meg a jelentős orvosi esemény leírását."),
	                    maxLengthText : _("max 300 karakter"),
	                    validateMessage : _("Kérem adja meg a jelentős orvosi esemény leírását."),
	                    fieldLabel : _("Esemény leírása"),
	                    labelWidth : 150,
	                    width : 550,
	                    height : 48,
	                    maxLength : 300,
	                    allowBlank : false,
	                    disabled : true
		           	},{
						xtype: 'monitorcheck',
		       			disabled : true
					},{
						xtype: 'fielddetailsbtn'
	                },{
	                    xtype : 'radiofield',
	                    boxLabel : _("életveszélyes állapot"),
	                    name : 'seriousType',
	                    disabled : true,
	                    inputValue : 5
		 			},{
						xtype: 'component'
					},{
						xtype: 'component'
	                },{
	                    xtype : 'radiofield',
	                    boxLabel : _("halál"),
	                    name : 'seriousType',
	                    disabled : true,
	                    inputValue : 6,
	                    listeners : {
	                        change : function(rb, newValue, oldValue, options) {"use strict";
	                            var fieldOutcomeFatal,
	                            form = rb.up('form');
	                            if (form !== undefined) {
	                            	form.setLinkedFieldsState(['dateofDeath', 'causeofDeath'], newValue);
	                            }
	                        }
	                    }
		 			},{
						xtype: 'component'
					},{
						xtype: 'component'
	                }, {
	                    xtype : 'datefield',
	                    name : 'dateofDeath',
	                    disabled : true,
	                    labelWidth : 150,
	                    fieldLabel : _("Halál dátuma"),
	                    validateMessage : _("Kérem adja meg a halál pontos dátumát."),
	                    width : 270,
	                    allowBlank : false,
	                    format : Kron.defaultDateFormat,
	                    maxValue: new Date(),
	                    isPrintable : true
	 	           	},{
		       			xtype: 'monitorcheck',
		       			disabled : true
					},{
						xtype: 'fielddetailsbtn'
	               	},{
	                    xtype : 'textarea',
	                    name : 'causeofDeath',
	                    labelWidth : 150,
	                    fieldLabel : _("Halál okának leírása"),
	                    emptyText : _("Kérem adja meg az okot..."),
	                    maxLengthText : _("max 300 karakter"),
	                    validateMessage : _("Kérem adja meg az halál okát."),
	                    height : 48,
	                    maxLength : 300,
	                    width : 550,
	                    disabled : true,
	                    allowBlank : false,
	                    isPrintable : true
	 	           	},{
		       			xtype: 'monitorcheck',
		       			disabled : true
					},{
						xtype: 'fielddetailsbtn'
	               }],
	                isPrintable : true
	            }]
	       }]
	    },{
	        xtype : 'fieldset',
	        title : _("Az eszköz alkalmazásának kezdete"),
	        defaults : {
	        	margin : '5 0 5 0',
	    	 	maxWidth : 590,
	    	 	allowBlank : false,
		 	 	msgTarget : 'side'
	       	},
	        items : [{
				xtype: 'component',
				width: 590,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				width: 32,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				height : 1,
				margin : '0 0 0 0'
			},{
	            xtype : 'ongoingdate',
	            name : 'firstAdministrationDate',
	            fieldLabel : _("Kezdő dátum"),
	            labelWidth : 170,
	            width : 560,
	            dateFormat : Kron.defaultDateFormat,
	            futureDate : false,
	            ongoingboxLabel : _("nem kezdte el alkalmazni az eszközt"),
	            validateMessage : _("Kérem töltse ki a dátumot vagy jelölje, hogy a beteg nem kezdte el alkalmazni az eszközt."),
	            isPrintable : true
	        },{
	            xtype: 'monitorcheck'
	        },{
	            xtype: 'fielddetailsbtn'
	        }]
	    }, {
	        xtype : 'fieldset',
	        title : _("További információk, megjegyzések, az esemény leírása"),
	     	defaults : {
	        	margin : '5 0 5 0',
	    	 	maxWidth : 590,
	    	 	allowBlank : true,
		 	 	msgTarget : 'side'
	       	},
	        items : [{
				xtype: 'component',
				width: 590,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				width: 32,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				height : 1,
				margin : '0 0 0 0'
			},{
	            xtype : 'textarea',
	            name : 'comments',
	            fieldLabel : _("Megjegyzések, információk"),
	            margin : '5 0 5 0',
	            labelWidth: 170,
	            width : 560,
	            height : 48,
	            maxLength : 300,
	            maxLengthText : _("max 300 karakter"),
	            isPrintable : true
	        }, {
	            xtype: 'monitorcheck'
	        }, {
	            xtype: 'fielddetailsbtn'
	        }]
	    },{
	        xtype : 'displayfield',
	        fieldCls : 'forms-message-info',
	        padding : '2 2 2 2',
	        value : _("Amennyiben a nemkívánatos esemény során gyógyszeres kezelés, műtét, vagy egyéb kezelés történt az esemény megoldása érdekében, kérjük jelölje a „Egyéb kezelések” oldalon")
	    }]
});
