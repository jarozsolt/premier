/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FieldDetailsBtn.js
 *
 * Control for check/unchek the field's monitored status
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */


Ext.define('Kron.view.patient.forms.fields.FieldDetailsBtn', {
    extend : 'Ext.button.Button',
    alias : 'widget.fielddetailsbtn',
    iconCls : 'icon-fieldhistory-btn_small',
    disabledCls : 'fielddetailsbutton-disabled',
    margin: '0 0 0 5',
    width : 22,
    height : 22,

/*    initComponent : function() { // Do not use "use strict" on functions containing callParent!
       this.strictInitComponent();
       this.callParent(arguments);

    },
    
    strictInitComponent : function() {"use strict";
    	this.setIconCls('fielddetailsbutton');
   },
  */
	getReferencedField : function() {"use strict";
		var panel,
			rfield, 
			monitorbtn;
		
		if (this.referencedField !== undefined && this.referencedField !== null) {
			panel = this.up('panel');
			if (panel !== undefined && panel !== null) {
				rfield = panel.down('[name=' + this.referencedField + ']');
			} else {
				rfield = this.up('form').down('[name=' + this.referencedField + ']');
			}
			if (rfield.xtype === 'radiofield' ) {
				rfield = rfield.up('radiogroup[name=' + this.referencedField + ']');
				console.log('radiofield-et talált a name-re componens keresés');
			}
		}
        if (rfield === undefined  || rfield === null) {
        	monitorbtn = this.previousSibling();
        	if (monitorbtn !== undefined  && monitorbtn !== null ) {
     	   		rfield = monitorbtn.previousSibling();     
     	   	}  
	    }
		return rfield;       
	},
	
	
	getFieldName : function (){"use strict";
		var rfield = this.getReferencedField(),
			fieldName;
			
	 	if (rfield !== undefined && rfield !== null) { 
        	fieldName = rfield.getName();
        }
        return fieldName;
   	},
   	
   	getMonitorCheckBtn : function() {"use strict";
		var monitorbtn = this.previousSibling();
		if (monitorbtn !== undefined  && monitorbtn !== null && monitorbtn.xtype == 'monitorcheck') {
 	   		return monitorbtn;   
 	   	} else {
 	   		return undefined;
    	}
	}
    
}); 


