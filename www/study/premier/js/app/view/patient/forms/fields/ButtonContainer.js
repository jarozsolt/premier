/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * ButtonContainer.js
 *
 * Container for the form field's monitorcheck and details buttons
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.ButtonContainer', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.btncontainer',
	requires : ['Kron.view.patient.forms.fields.MonitorCheck', 'Kron.view.patient.forms.fields.FieldDetailsBtn'],
	buttonsWidth : 70,
    layout : 'hbox',
    padding : '0 10 0 0',
    margin : '0 0 10 0',
    buttonsEnabled : true,
    listeners: {
        'onfielddisabled': function(field, eOpts) {"use strict";
            this.onControlFieldDisabled();
        },
        'onfieldenabled': function(field, eOpts) {"use strict";
            this.onControlFieldEnabled();
        },
        'aftercontrolmove': function(field, eOpts) {"use strict";
            //alert("aftercontrolmove");
            /*var field, positionFrom, positionTo;
            if (this.alignField !== undefined) {
                field = Ext.getCmp(this.alignField);
                if (field !== undefined && field !== null)
                {
                    positionFrom = this.getPosition();
                    positionTo = field.getPosition();
                    alert(positionTo);
                    alert(positionFrom);
                    //this.setPosition(field.getPosition());
                }
            }*/
        }
    },
    /*beforeSetPosition: function (x, y, animate) {
        var field, positionTo,
            pos, x0;

        // decode the position arguments:
        if (!x || Ext.isNumber(x)) {
            pos = { x: x, y : y, anim: animate };
        } else if (Ext.isNumber(x0 = x[0])) { // an array of [x, y]
            pos = { x : x0, y : x[1], anim: y };
        } else {
            pos = { x: x.x, y: x.y, anim: y }; // already an object w/ x & y properties
        }

        
        if (this.alignField !== undefined) {
            field = Ext.getCmp(this.alignField);
            if (field !== undefined && field !== null)
            {
                positionTo = field.getPosition();
                console.log("-------------------positionTo",positionTo);
                pos.y = positionTo[1];
                 console.log("-------------------pos.y",field);
            }
        }
       // xy = me.getPosition();
       //     xy[0] += vector[0];
       //     xy[1] += vector[1];
      //      me.setPosition(xy);
        pos.hasX = Ext.isNumber(pos.x);
        pos.hasY = Ext.isNumber(pos.y);

        // store the position as specified:
        this.x = pos.x;
        this.y = pos.y;
       
        return (pos.hasX || pos.hasY) ? pos : null;
    },*/
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },
    strictInitComponent : function() { "use strict";
        var setFieldIds = [], field, firstItem,
        permissionClass = "Forms";
       this.add({
            xtype : 'splitter',
            flex : 1
        });
        //CSL:ideiglenes kiszedve a teszteléshez
      //  if (Kron.core.UserManagement.hasPermission(permissionClass, "monitorForm"))
        if (true) // mindig kell, mert ez jelzi, mi van monitor által jóváhagyva (disabled lesz persze ha nincs rá joga)
        {
            this.add({
                xtype : 'monitorcheck',
                margin : this.buttonMargin?this.buttonMargin:'0',
                tabIndex: -1
            });
            this.add({
                xtype : 'splitter',
                itemId : 'flexfiller',
                width : 0
            });
        }
        else {
  	         this.add({
                xtype : 'splitter',
                width : 22
            });
        }
        this.add({
            xtype : 'splitter'
        });
        if (Kron.core.UserManagement.hasPermission(permissionClass, "getFieldHistories"))
        {
           this.add({
                xtype : 'fielddetailsbtn',
                margin : this.buttonMargin?this.buttonMargin:'0',
                tabIndex: -1
            });
        }
        // Ha van olyan mező, aminek az állapotváltozásaira reagálni kell,
        // akkor ráakaszkodunk az eventjeire.
 
 
  
         if (this.controlField !== undefined) {
            this.setControlField(this.controlField);
        }
 
 
        // Ehhez a mezőhöz igazodnak vertikálisan a gombok, általában a controlField
        /*if (this.alignField === undefined) {
            this.alignField = this.controlField;
        }
        if (this.alignField !== undefined) {
            field = Ext.getCmp(this.alignField);
            if (field !== undefined && field !== null)
            {                
                field.btnContainer = this;
                field.onPosition = function () { alert("ionposition"); };
                field.on('move', function ( cmp, x, y ) {
                    if (this.btnContainer != undefined) {
                        this.btnContainer.fireEvent('aftercontrolmove', this);
                        xy = this.btnContainer.getPosition();
                        xy[1] = y;
                        this.btnContainer.setPosition(xy);
                    }
                });
            }
        }*/
    },
    setControlField: function (controlFieldId) {"use strict";
        var field, form;
        this.controlField = controlFieldId;
        field = Ext.getCmp(this.controlField);
        if (field !== undefined)
        {
            field.btnContainer = this;
            if (field.disabled) {
                this.onControlFieldDisabled();
            }
            else {
                this.onControlFieldEnabled();
            }
            this.addEvents(['onfielddisabled','onfieldenabled']);
            //field.on('enable', function () { this.btnContainer.onControlFieldEnabled();});
            //field.on('disable', function () { this.btnContainer.onControlFieldDisabled();});
            field.on('enable', function () { this.btnContainer.fireEvent('onfieldenabled', this);});
            field.on('disable', function () { this.btnContainer.fireEvent('onfielddisabled', this);});
            // TODO: Ez a tabos formoknál nem működik valami miatt!!!
            //this.addEvents('aftercontrolmove');
            //field.on('move', function () { this.btnContainer.fireEvent('aftercontrolmove', this); });
        }
    },
    onControlFieldDisabled: function () {"use strict";
        if (this.buttonsEnabled === true) {
            this.buttonsEnabled = false;
        }
        var monitorButton = this.down('monitorcheck'),
        flexSplitter = this.getComponent('flexfiller');
        if (monitorButton !== undefined && monitorButton !== null &&
            flexSplitter !== undefined && flexSplitter !== null) {
            monitorButton.width = 0;
            flexSplitter.width = 22;
            monitorButton.hide();
        }
        
    },
    onControlFieldEnabled: function () {"use strict";
        
    //    if (Kron.core.UserManagement.hasPermission("Forms", "monitorForm")) {
            if (this.buttonsEnabled === false) {    
                this.buttonsEnabled = true;
            } 
            var monitorButton = this.down('monitorcheck'),
            flexSplitter = this.getComponent('flexfiller');
            if (monitorButton !== undefined && monitorButton !== null &&
                flexSplitter !== undefined && flexSplitter !== null) {
                monitorButton.width = 22;
                flexSplitter.width = 0;
                monitorButton.show();
            }
   
    //    }
        
    },
    getFieldNames: function () {"use strict";
        var item, form;
        if (this.fieldNames === undefined) {
            this.fieldNames = [];
            if (this.controlField !== undefined) {
                item = Ext.getCmp(this.controlField);
                if (item !== undefined) {
                    if (item.name !== undefined)
                    {
                        this.fieldNames = [].concat(item.name);
                    }
                }
                else {
                    console.warn("ButtonContainer: a {0} controlField mező nem létezik!", this.controlField, this);
                }   
            }
            else {
                console.warn("ButtonContainer: nincs controlField megadva!", this);
            }
        }
        if (this.fieldNames.length === 0) {
            console.warn("ButtonContainer: a fieldNames tömb üres!", this);
        }
        return this.fieldNames;
    },
    getHistoryLabel: function () {"use strict";
        // Ha nincs megadva, megpróbáljuk kitalálni
        var item, parent, form;
        if (this.historyLabel === undefined) {
            this.historyLabel = '';
            if (this.controlField !== undefined) {
                item = Ext.getCmp(this.controlField);
                if (item !== undefined) {
                    // Ha van labelje, legyen az a historyLabel
                    if (item.fieldLabel !== undefined && item.fieldLabel !== '') {
                        this.historyLabel = item.fieldLabel;
                    } else {
                        parent = item.up();
                        if (parent !== undefined) {
                            // Ha a parent fieldset
                            if (parent.xtype === 'fieldset') {
                                this.historyLabel = parent.title;
                            }
                        }
                    }
                }
                else {
                    console.warn("ButtonContainer: a {0} controlField mező nem létezik!", this.controlField, this);
                }
            }
            else {
                console.warn("ButtonContainer: nincs controlField megadva!", this);
            }
        }
        if (this.historyLabel === '') {
            console.warn("ButtonContainer: a historyLabel üres!", this);
        }
        return this.historyLabel;
    }
});
