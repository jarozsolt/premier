/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * OngoingDateField.js
 *
 * Container for a date and ongoing check
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.OngoingDateField', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.ongoingdate',
    layout : 'hbox',
    isXFormField : true,
    //combineErrors : true,
    defaults : {
        hideLabel : true
    },
    items : [],
    
    

    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    strictInitComponent : function() { "use strict";
        var me = this;
        
        me.items = [{
            xtype : 'datefield',
            doNotSave : true,
            width : 126,
            format : Kron.defaultDateFormat,
            allowBlank : me.allowBlank,
            maxValue: me.futureDate===false?new Date():undefined,
            isOptional : (me.allowBlank===undefined||me.allowBlank===true)?true:false,
            // TODO: a rossz dátumformátumra nem figyelmeztet
            /*getErrors : function(value) {"use strict";
                var parent = this.up('fieldcontainer'),
                checkField = this.nextSibling('checkboxfield');
                if (parent.allowBlank === false) {
                    if ((value === null || value === '') && checkField.getValue() === false) {
                        Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
                        return [parent.validateMessage];
                    }
                    Ext.form.Field.prototype.clearInvalid.call(this);
                }
                return [];
            }*/
           	listeners: {
                        'change': function (field, newValue, oldValue, eOpts) {"use strict";
                        	var form,
				            	actDate,
				            	endDateString,
				            	startDateString,
				            	startDateField;
				            	
                        	if (me.alertStartDateConflict !== undefined && me.alertStartDateConflict !== null) {
                        		if (field.isValid()) {
				                    
				                    form = me.up('form');
					            	actDate = new Date();
					            	startDateField = form.down('[name=' + me.alertStartDateConflict + ']');
						            	
						            if (startDateField !== undefined && startDateField.isValid()) {
						            	startDateString = startDateField.getValue()
						                if (startDateString !== '' && startDateString !== undefined && me.getValue() !== '' && me.getValue() !== undefined) {
					    		            if (me.getValue() === "o.g.") {
							            		endDateString = Ext.Date.format(actDate, "Y.m.d");
							            	} else {
							            		endDateString = Ext.Date.format(me.getValue(), "Y.m.d" );
							            	}
						                	
						                    if (startDateString > endDateString) {
						                            form.showStartEndDateConflictWarning();
						                    }
						                }
						            }   
				                }
                        	}
                        }
                    }
        }, {
                xtype : 'splitter'
        }, {
            xtype : 'checkboxfield',
            doNotSave : true,
            padding : '2 0 0 20',
            boxLabel: me.ongoingboxLabel?me.ongoingboxLabel:_("Folyamatban"),
            /*getErrors : function(value) {"use strict";
                var parent = this.up('fieldcontainer'),
                fieldDate = this.previousSibling('datefield');
                if (parent.allowBlank === false) {
                    if ((fieldDate.getValue() === '' || fieldDate.getValue() === null) && value === false) {
                        Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
                        return [parent.validateMessage];
                    }
                    Ext.form.Field.prototype.clearInvalid.call(this);
                }
                return [];
            },*/
            handler : function(field) {
                var fieldDate = this.previousSibling('datefield');
                if (this.getValue())
                {
                    fieldDate.setValue('');
                    fieldDate.disable(true);
                    fieldDate.allowBlank = true;
                }
                else
                {
                    fieldDate.enable(true);
                    fieldDate.allowBlank = fieldDate.isOptional;
                }
            }
        },{
            xtype: 'hiddenfield',
            name : me.name,
            parentContainer : me,
            setValue : function (value) { this.parentContainer.setValue(value); },
            getValue : function () { return this.parentContainer.getValue(); }
        }];
    },

    getValue: function() {"use strict";
        var me = this,
        dateField = me.down('datefield'),
        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            return 'o.g.';
        }
        return dateField.getValue();
    },
    
    getFormattedValue : function(fieldValue) {"use strict";
    	var me = this,
        dateField = me.down('datefield'),
        checkField = me.down('checkboxfield');
        if (fieldValue !== undefined && fieldValue !== null ) {
        	if (fieldValue ==='o.g.') {
        		return 'o.g.'; 
        	} else {
        		return Ext.Date.format(Ext.Date.parse(fieldValue, "Y-m-d H:i:s"), "Y.m.d" ); 
        	}
        	
        } else {
	        if (checkField.getValue() === true)
	        {
	            return 'o.g.';
	        }
	        return Ext.Date.format(dateField.getValue(), "Y.m.d" );       	
        }
        
    },
    
    setValue: function(value) {"use strict";
        var me = this,
        dateField = this.down('datefield'),
        checkField = this.down('checkboxfield');
        if (value === 'o.g.')
        {
            checkField.setValue(true);
            dateField.setValue('');
        }
        else if (value !== undefined)
        {
            checkField.setValue(false);
            dateField.setValue(value);
        }
        return me;
    },
    
    getName: function(){"use strict";
    	if (this.name != undefined && this.name !=null ) {
    		return this.name;
    	} else {
    		return null;
    	}
    	
    },
    
    onChange : function(){"use strict";
   	},
   	
    isValid : function(){"use strict";
    	var me = this,
	        dateField = me.down('datefield'),
	        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            return true;
        }
        return dateField.isValid();
    }
});
