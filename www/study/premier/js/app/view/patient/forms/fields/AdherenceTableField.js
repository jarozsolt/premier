/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * AdherenceTableField.js
 *
 * Field for Keplat and Flector adherence table
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.AdherenceTableField', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.adherencetablefield',
    layout : 'vbox',
    isXFormField : true,
    validateMessage : _("Kérem töltse ki a mezőket vagy állítsa 'Nincs adat'-ra."),
    //cls: 'adherencetable',
    //anchor: '-18',
    /*layout: {
        type: 'table',
        columns: 15,
        tdAttrs: {
            valign: 'center',
            align: 'center'
        }
    },*/
    valueSeparator :  '|',
    doNotSave : true,
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    strictInitComponent : function() { "use strict";
        var me = this;
        
        me.items = [{
            xtype : 'displayfield',
            itemId : 'adherencetablewarning',
            fieldCls : 'forms-message-warning',
            doNotSave : true,
            width : me.width-5,
            padding : '2 2 2 2',
            value : _("Kérjük töltse ki a kezelés típusát az előző viziten!")
        }, {
            xtype: 'radiogroup',
            itemId : 'keplattable',
            cls: 'adherencetable',
            //hidden : true,
            combineErrors : true,
            width : me.width-10,
            anchor: '-18',
            msgTarget : 'side',
            doNotSave : true,
            layout: {
                type: 'table',
                columns: 8,
                tdAttrs: {
                    valign: 'center',
                    align: 'center'
                }
            },
            getErrors : function(value) {
                var parent = this.up('fieldcontainer'),
                tablevalue, day, valueTokens,
                errors = [];
                if (parent.activeTable === 1) { // Keplat
                    tablevalue = parent.getKeplatValue();
                    valueTokens = tablevalue.split(parent.valueSeparator);
                    for (day = 1; day < 8; day+=1) {
                        if (valueTokens[day] === '') {
                            return [parent.validateMessage];
                        }
                    }
                }
                return [];
            },
            items: [{xtype: 'component', html: _("KEPLAT")},
                {xtype: 'displayfield', value: _("1. nap"), fieldCls: 'adherencetable-smallheader', width : 60, bodyCls: "{align: 'center'}"},
                {xtype: 'displayfield', value: _("2. nap"), fieldCls: 'adherencetable-smallheader', width : 60, tdAttrs: {align: 'center'}},
                {xtype: 'displayfield', value: _("3. nap"), fieldCls: 'adherencetable-smallheader', width : 60, tdAttrs: {align: 'center'}},
                {xtype: 'displayfield', value: _("4. nap"), fieldCls: 'adherencetable-smallheader', width : 60, tdAttrs: {align: 'center'}},
                {xtype: 'displayfield', value: _("5. nap"), fieldCls: 'adherencetable-smallheader', width : 60, tdAttrs: {align: 'center'}},
                {xtype: 'displayfield', value: _("6. nap"), fieldCls: 'adherencetable-smallheader', width : 60, tdAttrs: {align: 'center'}},
                {xtype: 'displayfield', value: _("7. nap"), fieldCls: 'adherencetable-smallheader', width : 60, tdAttrs: {align: 'center'}},
                {xtype: 'displayfield', value: _("A tapasz gyengén tapadt"), fieldCls: 'adherencetable-smallheader', tdAttrs: {align: 'left'}},
                {xtype: 'radiofield', name: 'kadherenceDay1', doNotSave: true, itemId: 'kday1', inputValue: 1},
                {xtype: 'radiofield', name: 'kadherenceDay2', doNotSave: true, itemId: 'kday2', inputValue: 1},
                {xtype: 'radiofield', name: 'kadherenceDay3', doNotSave: true, itemId: 'kday3', inputValue: 1},
                {xtype: 'radiofield', name: 'kadherenceDay4', doNotSave: true, itemId: 'kday4', inputValue: 1},
                {xtype: 'radiofield', name: 'kadherenceDay5', doNotSave: true, itemId: 'kday5', inputValue: 1},
                {xtype: 'radiofield', name: 'kadherenceDay6', doNotSave: true, itemId: 'kday6', inputValue: 1},
                {xtype: 'radiofield', name: 'kadherenceDay7', doNotSave: true, itemId: 'kday7', inputValue: 1},                        
                {xtype: 'displayfield', value: _("A tapasz nem tapadt tökéletesen"), fieldCls: 'adherencetable-smallheader', tdAttrs: {align: 'left'}},
                {xtype: 'radiofield', name: 'kadherenceDay1', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'kadherenceDay2', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'kadherenceDay3', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'kadherenceDay4', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'kadherenceDay5', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'kadherenceDay6', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'kadherenceDay7', doNotSave: true, inputValue: 2}, 
                {xtype: 'displayfield', value: _("A tapasz tökéletesen tapadt"), fieldCls: 'adherencetable-smallheader', tdAttrs: {align: 'left'}},
                {xtype: 'radiofield', name: 'kadherenceDay1', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'kadherenceDay2', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'kadherenceDay3', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'kadherenceDay4', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'kadherenceDay5', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'kadherenceDay6', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'kadherenceDay7', doNotSave: true, inputValue: 3},
                {xtype: 'displayfield', value: _("Nincs adat"), fieldCls: 'adherencetable-smallheader', tdAttrs: {align: 'left'}},
                {xtype: 'radiofield', name: 'kadherenceDay1', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'kadherenceDay2', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'kadherenceDay3', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'kadherenceDay4', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'kadherenceDay5', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'kadherenceDay6', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'kadherenceDay7', doNotSave: true, inputValue: 4
            }]
        }, {
            xtype: 'radiogroup',
            itemId : 'flectortable',
            cls: 'adherencetable',
            combineErrors : true,
            //hidden : true,
            anchor: '-18',
            width : me.width-10,
            doNotSave: true,
            msgTarget : 'side',
            layout: {
                type: 'table',
                columns: 15,
                tdAttrs: {
                    valign: 'center',
                    align: 'center'
                }
            },
            getErrors : function(value) {
                var parent = this.up('fieldcontainer'),
                tablevalue, day, dayvalue, nightvalue, valueTokens,
                errors = [];
                if (parent.activeTable === 2) { // Flector
                    tablevalue = parent.getFlectorValue();
                    valueTokens = tablevalue.split(parent.valueSeparator);
                    for (day = 1; day < 8; day += 1) {
                        dayvalue = valueTokens[(day*2)-1];
                        nightvalue = valueTokens[day*2];
                        if (dayvalue === '' || nightvalue === '') {
                            return [parent.validateMessage];
                        }
                    }
                }
                
                return [];
            },
            items: [{xtype: 'component', html: _("FLECTOR"), rowspan: 2},
                {xtype: 'displayfield', value: _("1. nap"), colspan: 2, fieldCls: 'adherencetable-smallheader'},
                {xtype: 'displayfield', value: _("2. nap"), colspan: 2, fieldCls: 'adherencetable-smallheader'},
                {xtype: 'displayfield', value: _("3. nap"), colspan: 2, fieldCls: 'adherencetable-smallheader'},
                {xtype: 'displayfield', value: _("4. nap"), colspan: 2, fieldCls: 'adherencetable-smallheader'},
                {xtype: 'displayfield', value: _("5. nap"), colspan: 2, fieldCls: 'adherencetable-smallheader'},
                {xtype: 'displayfield', value: _("6. nap"), colspan: 2, fieldCls: 'adherencetable-smallheader'},
                {xtype: 'displayfield', value: _("7. nap"), colspan: 2, fieldCls: 'adherencetable-smallheader'},                       
                {xtype: 'displayfield', value: _("Reggel"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Este"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Reggel"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Este"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Reggel"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Este"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Reggel"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Este"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Reggel"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Este"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Reggel"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Este"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Reggel"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("Este"), fieldCls: 'adherencetable-verysmallheader', width : 30},
                {xtype: 'displayfield', value: _("A tapasz gyengén tapadt"), fieldCls: 'adherencetable-smallheader', tdAttrs: {align: 'left'}},
                {xtype: 'radiofield', name: 'fadherenceDay1', itemId: 'fday1', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceNight1', itemId: 'fnight1', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceDay2', itemId: 'fday2', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceNight2', itemId: 'fnight2', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceDay3', itemId: 'fday3', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceNight3', itemId: 'fnight3', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceDay4', itemId: 'fday4', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceNight4', itemId: 'fnight4', doNotSave: true, inputValue: 1},                       
                {xtype: 'radiofield', name: 'fadherenceDay5', itemId: 'fday5', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceNight5', itemId: 'fnight5', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceDay6', itemId: 'fday6', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceNight6', itemId: 'fnight6', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceDay7', itemId: 'fday7', doNotSave: true, inputValue: 1},
                {xtype: 'radiofield', name: 'fadherenceNight7', itemId: 'fnight7', doNotSave: true, inputValue: 1},
                {xtype: 'displayfield', value: _("A tapasz nem tapadt tökéletesen"), fieldCls: 'adherencetable-smallheader', tdAttrs: {align: 'left'}},
                {xtype: 'radiofield', name: 'fadherenceDay1', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceNight1', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceDay2', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceNight2', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceDay3', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceNight3', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceDay4', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceNight4', doNotSave: true, inputValue: 2},                       
                {xtype: 'radiofield', name: 'fadherenceDay5', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceNight5', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceDay6', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceNight6', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceDay7', doNotSave: true, inputValue: 2},
                {xtype: 'radiofield', name: 'fadherenceNight7', doNotSave: true, inputValue: 2},
                {xtype: 'displayfield', value: _("A tapasz tökéletesen tapadt"), fieldCls: 'adherencetable-smallheader', tdAttrs: {align: 'left'}},
                {xtype: 'radiofield', name: 'fadherenceDay1', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceNight1', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceDay2', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceNight2', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceDay3', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceNight3', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceDay4', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceNight4', doNotSave: true, inputValue: 3},                       
                {xtype: 'radiofield', name: 'fadherenceDay5', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceNight5', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceDay6', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceNight6', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceDay7', doNotSave: true, inputValue: 3},
                {xtype: 'radiofield', name: 'fadherenceNight7', doNotSave: true, inputValue: 3},
                {xtype: 'displayfield', value: _("Nincs adat"), fieldCls: 'adherencetable-smallheader', tdAttrs: {align: 'left'}},
                {xtype: 'radiofield', name: 'fadherenceDay1', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceNight1', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceDay2', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceNight2', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceDay3', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceNight3', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceDay4', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceNight4', doNotSave: true, inputValue: 4},                       
                {xtype: 'radiofield', name: 'fadherenceDay5', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceNight5', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceDay6', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceNight6', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceDay7', doNotSave: true, inputValue: 4},
                {xtype: 'radiofield', name: 'fadherenceNight7', doNotSave: true, inputValue: 4}]
        }, {
            xtype: 'hiddenfield',
            name : me.name,
            parentContainer : me,
            setValue : function (value) { this.parentContainer.setValue(value); },
            getValue : function () { return this.parentContainer.getValue(); }//,
            //getErrors: function(value) { return this.parentContainer.getErrors(value); }
        }];
    },
    resetTable: function(table) {"use strict";
        if (table !== undefined) {
            var hadError = table.hasActiveError(),
            preventMark = table.preventMark;
            table.preventMark = true;
            table.batchChanges(function() {
                var boxes = table.getBoxes(),
                    b,
                    bLen  = boxes.length;
    
                for (b = 0; b < bLen; b += 1) {
                    boxes[b].reset();
                    boxes[b].setValue(false);
                }
            });
            table.preventMark = preventMark;
            table.unsetActiveError();
            if (hadError) {
                table.updateLayout();
            }
        }
    },
    reset: function() {"use strict";
        var me = this,
        keplatTable = me.getComponent('keplattable'),
        flectorTable = me.getComponent('flectortable');
        me.resetTable(keplatTable);
        me.resetTable(flectorTable);
    },
    getFlectorValue: function() {"use strict";
        var me = this, radiovalue, name, value, day, item,
        flectorTable = me.getComponent('flectortable');
        if (flectorTable !== undefined) {
            value = 'f';
            for (day = 1; day < 8; day += 1) {
                value += me.valueSeparator;
                name = 'fday'+day.toString();
                item = flectorTable.getComponent(name);
                if (item === undefined) {
                    radiovalue = '';
                }
                else {
                    radiovalue = item.getGroupValue();
                    if (radiovalue === undefined || radiovalue === null) {
                        radiovalue = '';
                    }
                }
                value += radiovalue + me.valueSeparator;
                name = 'fnight'+day.toString();
                item = flectorTable.getComponent(name);
                if (item === undefined) {
                    radiovalue = '';
                }
                else {
                    radiovalue = item.getGroupValue();
                    if (radiovalue === undefined || radiovalue === null) {
                        radiovalue = '';
                    }
                }
                value += radiovalue;
            }
        }
        return value;
    },
    setFlectorValue: function(value) {"use strict";
        var me = this, valueTokens, radios, len, name, numvalue, dayvalue, nightvalue, first, formId, day, i,
        flectorTable = me.getComponent('flectortable');
        if (flectorTable !== undefined) {
            me.resetTable(flectorTable);
            first = flectorTable.getComponent('fday1');
            formId = first ? first.getFormId() : null;
            if (value !== undefined && value !== null && value.split !== undefined)
            {
                valueTokens = value.split(me.valueSeparator);
                for (day = 1; day < 8; day += 1) {
                    dayvalue = valueTokens[(day*2)-1];
                    nightvalue = valueTokens[day*2];
                    //day
                    if (dayvalue !== '') {
                        numvalue = parseInt(dayvalue,10);
                        name = 'fadherenceDay'+day.toString();     
                        radios = Ext.form.RadioManager.getWithValue(name, numvalue, formId).items;
                        len = radios.length;
                        for (i = 0; i < len; ++i) {
                            radios[i].setValue(true);
                        }
                    }
                    //night
                    if (nightvalue !== '') {
                        name = 'fadherenceNight'+day.toString();
                        numvalue = parseInt(nightvalue,10);
                        radios = Ext.form.RadioManager.getWithValue(name, numvalue, formId).items;
                        len = radios.length;
                        for (i = 0; i < len; ++i) {
                            radios[i].setValue(true);
                        }
                    }
                }
            }
        }
        return me;
    },
    getKeplatValue: function() {"use strict";
        var me = this, radiovalue, name, value, day, item,
        keplatTable = me.getComponent('keplattable');
        if (keplatTable !== undefined) {
            value = 'k';
            for (day = 1; day < 8; day += 1) {
                value += me.valueSeparator;
                name = 'kday'+day.toString();
                item = keplatTable.getComponent(name);
                if (item === undefined) {
                    radiovalue = '';
                }
                else {
                    radiovalue = item.getGroupValue();
                    if (radiovalue === undefined || radiovalue === null) {
                        radiovalue = '';
                    }
                }
                value += radiovalue;
            }
        }
        return value;
    },
    setKeplatValue: function(value) {"use strict";
        var me = this, valueTokens, radios, len, name, numvalue, first, formId, day, i,
        keplatTable = me.getComponent('keplattable');
        if (keplatTable !== undefined) {
            me.resetTable(keplatTable);
            first = keplatTable.getComponent('kday1');
            formId = first ? first.getFormId() : null;
            if (value !== undefined && value !== null && value.split !== undefined)
            {
                valueTokens = value.split(me.valueSeparator);
                for (day = 1; day < 8; day+=1) {
                    if (valueTokens[day] !== '') {
                        name = 'kadherenceDay'+day.toString();
                        numvalue = parseInt(valueTokens[day],10);
                        radios = Ext.form.RadioManager.getWithValue(name, numvalue, formId).items;
                        len = radios.length;
                        for (i = 0; i < len; ++i) {
                            radios[i].setValue(true);
                        }
                    }
                }
            }
        }
        return me;
    },
    showWarning: function() {"use strict";
        var me = this,
        warning = me.getComponent('adherencetablewarning'),
        keplatTable = me.getComponent('keplattable'),
        flectorTable = me.getComponent('flectortable');
        me.activeTable = undefined;
        if (keplatTable !== undefined) {
            me.resetTable(keplatTable);
            keplatTable.allowBlank = true;
            keplatTable.setVisible(false);   
        }
        if (flectorTable !== undefined) {
            me.resetTable(flectorTable);
            flectorTable.allowBlank = true;
            flectorTable.setVisible(false);
        }
        if (warning !== undefined) {
            warning.setVisible(true);
        }
    },
    showKeplat: function() {"use strict";
        var me = this,
        warning = me.getComponent('adherencetablewarning'),
        keplatTable = me.getComponent('keplattable'),
        flectorTable = me.getComponent('flectortable');
        me.activeTable = 1; // Keplat
        if (keplatTable !== undefined) {
            keplatTable.allowBlank = false;
            keplatTable.setVisible(true);   
        }
        if (flectorTable !== undefined) {
            me.resetTable(flectorTable);
            flectorTable.allowBlank = true;
            flectorTable.setVisible(false);
        }
        if (warning !== undefined) {
            warning.setVisible(false);
        }       
    },
    showFlector: function() {"use strict";
        var me = this,
        warning = me.getComponent('adherencetablewarning'),
        keplatTable = me.getComponent('keplattable'),
        flectorTable = me.getComponent('flectortable');
        me.activeTable = 2; // Flector
        if (keplatTable !== undefined) {
            me.resetTable(keplatTable);
            keplatTable.allowBlank = true;
            keplatTable.setVisible(false);   
        }
        if (flectorTable !== undefined) {
            flectorTable.allowBlank = false;
            flectorTable.setVisible(true);
        }
        if (warning !== undefined) {
            warning.setVisible(false);
        }
        
    },
    getValue: function() {"use strict";
        var value,
        me = this;
        if (me.activeTable === 1) { // Keplat
            value = me.getKeplatValue();
        }
        else if (me.activeTable === 2) { // Flector
            value = me.getFlectorValue();
        }
        return value;
    },
    setValue: function(value) {"use strict";
        var me = this;
        if (value !== undefined && value !== null) {
            // Keplat?
            if (value.charAt(0) === 'k') {
                me.showKeplat();
                me.setKeplatValue(value);
                
            }
            else if (value.charAt(0) === 'f') { // Flector?
                me.showFlector();
                me.setFlectorValue(value);
                
            }
            else {
                me.showWarning();
            }
        }
        else {
            me.showWarning();
        }
        return me;
    }
    /*validator : function(value) {"use strict";
                            var fieldFatal = Ext.getCmp('seriousType_fatal'),
                            fieldsetSerious = Ext.getCmp('ae_saeyes');
                            if ('' === value && fieldFatal.getValue() === true && fieldsetSerious.getValue() === true) {// fatal and serious checked
                                Ext.form.Field.prototype.markInvalid.call(this, this.validateMessage);
                                return this.validateMessage;
                            }
                            Ext.form.Field.prototype.clearInvalid.call(this);
                            return true;
                        }*/
    /*getErrors: function(value) {"use strict";
        var me = this, currValue, day, valueTokens, dayvalue, nightvalue,
        keplatTable = me.getComponent('keplattable'),
        flectorTable = me.getComponent('flectortable'),
        tablevalue = me.getValue(),
        errors = [];
        
        if (me.activeTable === 1) { // Keplat
            valueTokens = tablevalue.split(me.valueSeparator);
            for (day = 1; day < 8; day+=1) {
                alert(valueTokens[day]);
                if (valueTokens[day] === '') {
                    errors.push(me.validateMessage);
                    Ext.form.Field.prototype.markInvalid.call(keplatTable, me.validateMessage);
                    return errors;
                }
            }
        }
        else if (me.activeTable === 2) { // Flector
            valueTokens = tablevalue.split(me.valueSeparator);
            for (day = 1; day < 8; day += 1) {
                dayvalue = valueTokens[(day*2)-1];
                nightvalue = valueTokens[day*2];
                if (dayvalue === '' || nightvalue === '') {
                    errors.push(me.validateMessage);
                    Ext.form.Field.prototype.markInvalid.call(flectorTable, me.validateMessage);
                    return errors;
                }
            }
        }
        if (errors.length === 0) {
            Ext.form.Field.prototype.clearInvalid.call(this);
        }
        return errors;
    }*/
});
