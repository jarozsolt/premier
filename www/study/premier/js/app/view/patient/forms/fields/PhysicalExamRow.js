/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * PhysicalExamRow.js
 *
 * Fizikális vizsgálat egy sora
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

// TODO: az id beállítás nem működik!!!!! Fontos javítani!!!! Ha másképp nem megy szét kell szedni a mezőt és a btncontainert
Ext.define('Kron.view.patient.forms.fields.PhysicalExamRow', {
	extend : 'Ext.container.Container',
	alias : 'widget.physicalexamrow',
	isXFormField : true,
	width : 685,
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        
        this.ownInitComponent();
        this.callParent(arguments);
    },
    ownInitComponent : function() { "use strict";
        this.items = [{
            xtype: 'fieldcontainer',
            layout : 'hbox',
            width : 685,
            items : [{
                xtype : 'fieldset',
                layout : 'anchor',
                title : this.radioGroupLabel,
                width : 615,
                items : [{
                    xtype : 'radiogroup',
                    labelWidth : 250,
                    id : this.radioGroupId,
                    name : this.radioGroupName,
                    fieldLabel : this.radioGroupLabel,
                    items : [{
                        xtype : 'radiofield',
                        name : this.radioGroupName,
                        boxLabel : _("Eltérés nélküli"),
                        inputValue : 1,
                        listeners : {
                            change : function(rb, newValue, oldValue, options) {
                                var radiogroup, textField; 
                                if (newValue === true) {
                                   radiogroup = this.up('radiogroup');
                                    if (radiogroup !== undefined) {
                                        textField = radiogroup.nextSibling('textarea');
                                        if (textField !== undefined) {
                                            textField.reset();
                                            textField.disable(true);
                                            textField.allowBlank = true;
                                        }
                                    }
                                }
                            }
                        }
                    }, {
                        xtype : 'radiofield',
                        name : this.radioGroupName,
                        boxLabel : _("Eltéréssel"),
                        inputValue : 2,
                        listeners : {
                            change : function(rb, newValue, oldValue, options) {
                                var radiogroup, textField; 
                                if (newValue === true) {
                                   radiogroup = this.up('radiogroup');
                                    if (radiogroup !== undefined) {
                                        textField = radiogroup.nextSibling('textarea');
                                        if (textField !== undefined) {
                                            textField.enable(true);
                                            textField.allowBlank = false;
                                        }
                                    }
                                }
                            }
                        }
                    }, {
                        xtype : 'radiofield',
                        name : this.radioGroupName,
                        boxLabel : _("Nem vizsgált"),
                        inputValue : 3,
                        listeners : {
                            change : function(rb, newValue, oldValue, options) {
                                var radiogroup, textField; 
                                if (newValue === true) {
                                   radiogroup = this.up('radiogroup');
                                    if (radiogroup !== undefined) {
                                        textField = radiogroup.nextSibling('textarea');
                                        if (textField !== undefined) {
                                            textField.reset();
                                            textField.disable(true);
                                            textField.allowBlank = true;
                                        }
                                    }
                                }
                            }
                        }
                    }],
                    allowBlank : false,
                    msgTarget : 'side',
                    isPrintable : true
                }, {
                    xtype : 'textarea',
                    id : this.descriptionId,
                    name : this.descriptionName,
                    emptyText : _("Kérem részletezze az eltérést..."),
                    maxLengthText : _("max 300 karakter"),
                    validateMessage : _("Kérem adja meg az eltérés leírását."),
                    margin : '10 0 5 0',
                    width : 590,
                    height : 48,
                    maxLength : 300,
                    disabled : true,
                    isPrintable : true
                }]
            }, {
                xtype: 'fieldcontainer',
                layout : 'vbox',
                items : [{
                    xtype : 'btncontainer',
                    controlField : this.radioGroupId,
                    historyLabel : this.radioGroupLabel,
                    buttonMargin : '7 0 0 0'
                }, {
                    xtype : 'btncontainer',
                    controlField : this.descriptionId,
                    historyLabel : Ext.String.format(_("{0} - Eltérés leírása"), this.radioGroupLabel),
                    buttonMargin : '20 0 0 0'
                }]
            }]
        }];
        //var btnContainer1 = this.down('btncontainer'),
        //btnContainer2 = btnContainer1.nextSibling('btncontainer'),
        //radioGroup = this.down('radiogroup'),
       // radioField1 = this.down('radiofield'),
        //radioField2 = radioField1.nextSibling('radiofield'),
       // radioField3 = radioField2.nextSibling('radiofield'),
       // fieldSet = this.down('fieldset'),
       // textArea = this.down('textarea');
      //  radioGroup.id = this.radioGroupId;
      //  radioGroup.name = this.radioGroupName;
       // radioGroup.fieldLabel = this.radioGroupLabel;
       // radioField1.name = this.radioGroupName;
      //  radioField2.name = this.radioGroupName;
      //  radioField3.name = this.radioGroupName;
        //btnContainer1.setControlField(this.radioGroupId);
        //btnContainer1.historyLabel = radioGroup.fieldLabel;
        //fieldSet.width = this.width-btnContainer1.buttonsWidth;
        //fieldSet.title = this.radioGroupLabel;
        //textArea.id = this.descriptionId;
        //textArea.name = this.descriptionName;
        //btnContainer2.setControlField(this.descriptionId);
        //btnContainer2.historyLabel = Ext.String.format(_("{0} - Eltérés leírása"), radioGroup.fieldLabel);
    }
});
