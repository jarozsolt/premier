/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Visit4Form.js
 *
 * Visit 4 form - panel in Visit view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.Visit4Form', {
    extend : 'Kron.view.patient.forms.GeneralPatientForm',
    alias : 'widget.visit4Form',
    id : 'Visit4Form',
    formWidth : 760,
    formHeight : 600,
    formTitle : Kron.formNames.Visit4,
    formIconCls : 'icon-normalvisit-dlg_small',
    autoScroll : true,
    bodyStyle:'background-color:#ffffff;padding:10px',
    // Ha nincs tab panel, vagy nem akarom ott jelezni, ha van nem valid field, akkor ezt true-ra kell állítani
    blockErrorMarkingOnTab : true, 

    defaults : {
        width: 685,
        labelWidth: 400,
        layout: {
            type: 'table',
            columns: 3
        }
    },  
    // We can save the form only if the VisitDate field is not empty
    canSave : function() {"use strict";
        var fieldVisitDate =  this.down('[name=visitDate]');
        if (fieldVisitDate) {
            if (fieldVisitDate.getValue() !== null && fieldVisitDate.getValue() !== '') {
                return true;
            }
        }
        return false;
    },
    
    // Alert an error message if the diagnosis is not filled
    alertSaveError : function() {"use strict";
        Ext.Msg.show({
            title : _("Hiba"),
            msg : _("A 'Vizsgálat végének dátuma' mezőt kötelező kitölteni az oldal elmentéséhez!"),
            buttons : Ext.Msg.OK,
            icon : Ext.MessageBox.ERROR,
            animateTarget: 'dlgAccept'});
    },
    items : [{
        xtype:'fieldset',
        border: 0,
	    defaults : {
        	margin : '5 0 5 0',
    	 	maxWidth : 590,
    	 	allowBlank : false,
	 	 	msgTarget : 'side'
       	},
        items: [{
			xtype: 'component',
			width: 590,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			width: 32,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			height : 1,
			margin : '0 0 0 0'
		},{
            name : 'visitDate',
            xtype : 'datefield',
            fieldLabel : _("Vizsgálat végének dátuma") + Kron.config.labelMandatoryMark,
            labelWidth: 240,
            format : Kron.defaultDateFormat,
            maxValue: new Date(),
            width: 366,
            allowBlank : false,
            isPrintable : true
        }, {
            xtype: 'monitorcheck'
        }, {
            xtype: 'fielddetailsbtn'
        }]
    }, {
        xtype : 'fieldset',
        title : _("A vizsgálat lefolyása a beteg részéről"),
	    defaults : {
        	margin : '5 0 5 0',
    	 	maxWidth : 590,
    	 	allowBlank : false,
	 	 	msgTarget : 'side'
       	},
        items: [{
			xtype: 'component',
			width: 590,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			width: 32,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			height : 1,
			margin : '0 0 0 0'
		},{
            xtype: 'radiogroup',
            name: 'studyCourse',
            historyLabel : _("A vizsgálat lefolyása a beteg részéről"),
            width: 400,
            columns: 1,
            defaults : {
                margin : '5 0 5 0'
            },
            items: [{
                xtype: 'radiofield',
                boxLabel:  _("befejezte a protokoll szerinti időpontban"),
                name: 'studyCourse',
                inputValue: 1,
                handler : function(field) {"use strict";
                    var form = field.up('form');
                    if (this.getValue()){
                   		form.showFillVisit3Warning();
                    }
               	} 
            }, {
                xtype: 'radiofield',
                boxLabel: _("idő előtt fejezte be"),
                name: 'studyCourse',
                inputValue: 2,
               	handler : function(field) {"use strict";
                    var form = field.up('form');
                    form.setLinkedFieldsState(['reasonofDiscontinuation'], this.getValue());
                    if (this.getValue()){
                    	form.showFillLastVisitWarning();
                    }
               	} 
            }, {
                xtype: 'radiofield',
                id : 'visit4_studycoursesother',
                boxLabel: _("egyéb"),
                name: 'studyCourse',
                inputValue: 3,
               	handler : function(field) {"use strict";
                    var form = field.up('form');
                    form.setLinkedFieldsState(['otherStudyCourseDescription'], this.getValue());
                    if (this.getValue()){
                    	form.showFillLastVisitWarning();
                    }
               	} 
            }],
            allowBlank : false,
            msgTarget : 'side',
            isPrintable : true
        }, {
            xtype: 'monitorcheck'
        }, {
            xtype: 'fielddetailsbtn'
        }, {
            xtype : 'textarea',
            name : 'otherStudyCourseDescription',
            emptyText : _("Kérem részletezze..."),
            maxLengthText : _("max 300 karakter"),
            validateMessage : _("Kérem adja meg az 'egyéb lefolyás' leírását."),
            fieldLabel : _("Egyéb leírása"),
            padding : '0 0 0 12',
            labelWidth : 100,
            width : 560,
            height : 48,
            maxLength : 300,
            disabled : true,
            isPrintable : true
        }, {
            xtype: 'monitorcheck',
            disabled : true
        }, {
            xtype: 'fielddetailsbtn'
        }]
    }, {
        xtype : 'fieldset',
        title : _("Ha idő előtt fejezte be, kérjük adja meg az okot!"),
       	layout: 'vbox',
         defaults : {
        	margin : '5 0 5 0',
    	 	allowBlank : false,
	 	 	msgTarget : 'side',
	 	 	labelWidth : 240
       	},
        items : [{
            xtype: 'radiogroup',
            name: 'reasonofDiscontinuation',
            historyLabel : _('Ha idő előtt fejezte be, kérjük adja meg az okot!'),
            width : 650,
            disabled : true,
            defaults : {
                margin : '5 0 5 0',
                maxWidth : 590
            },
            layout: {
	            type: 'table',
	            columns: 3
	        },
           	msgTarget : 'side',
 			isPrintable : true,
            items: [{
				xtype: 'component',
				width: 590,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				width: 32,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				height : 1,
				margin : '0 0 0 0'
			},{
            	xtype: 'radiofield',
                boxLabel:  _("visszavonta a beleegyezését"),
                name: 'reasonofDiscontinuation',
                inputValue: 1
            },{
       			xtype: 'monitorcheck',
       			referencedField: 'reasonofDiscontinuation'
			},{
				xtype: 'fielddetailsbtn',
				referencedField: 'reasonofDiscontinuation'
			},{
                xtype: 'radiofield',
                boxLabel: _("eltérés a protokolltól"),
                name: 'reasonofDiscontinuation',
                inputValue: 2,
                handler : function(field) {"use strict";
                    var form = field.up('form');
                    form.setLinkedFieldsState(['otherProtocolDeviationDiscontinuationDescription'], this.getValue());
               	}
			},{
				xtype: 'component'
			},{
				xtype: 'component'
			},{
                xtype : 'textarea',
                name : 'otherProtocolDeviationDiscontinuationDescription',
                emptyText : _("Kérem részletezze..."),
                maxLengthText : _("max 300 karakter"),
                validateMessage : _("Kérem adja meg az 'eltérés a protokolltól' leírását."),
                fieldLabel : _("Eltérés leírása"),
                padding : '0 0 0 12',
                labelWidth : 100,
                width : 400,
                height : 48,
                maxLength : 300,
                disabled : true,
                isPrintable : true,
                allowBlank : false
           	},{
       			xtype: 'monitorcheck',
       			disabled : true
			},{
				xtype: 'fielddetailsbtn'
			},{
                xtype: 'radiofield',
                boxLabel: _("nemkívánatos esemény"),
                id : 'visit4_reasonsae',
                name: 'reasonofDiscontinuation',
                inputValue: 3,
                listeners : {
                    change : function(rb, newValue, oldValue, options) {"use strict";
                        if (newValue === true)
                        {
                            var form = rb.up('form');
                            if (form !== undefined) {
                                form.showAEWarning();
                            }
                        }
                    }
                }
            },{
				xtype: 'component'
			},{
				xtype: 'component'
			},{
                xtype: 'radiofield',
                boxLabel: _("halál"),
                id : 'visit4_reasondeath',
                name: 'reasonofDiscontinuation',
                inputValue: 4,
               	handler : function(field) {"use strict";
                    var form = field.up('form');
                    form.setLinkedFieldsState(['dateofDeath', 'causeofDeath'], this.getValue());
                     if (this.getValue() === true)
                        {
                            form.showAEWarning();
                        }
               	}
			},{
				xtype: 'component'
			},{
				xtype: 'component'
			},{
                xtype : 'datefield',
                name : 'dateofDeath',
                padding : '0 0 0 12',
                disabled : true,
                labelWidth : 100,
                fieldLabel : _("Halál dátuma"),
                validateMessage : _("Kérem adja meg a halál pontos dátumát."),
                width : 220,
                format : Kron.defaultDateFormat,
                maxValue: new Date(),
                isPrintable : true,
                allowBlank : false
            },{
       			xtype: 'monitorcheck',
       			disabled : true
			},{
				xtype: 'fielddetailsbtn'
			},{
                xtype : 'textarea',
                name : 'causeofDeath',
                padding : '0 0 0 12',
                labelWidth : 100,
                fieldLabel : _("Halál okának leírása"),
                emptyText : _("Kérem adja meg az okot..."),
                maxLengthText : _("max 300 karakter"),
                validateMessage : _("Kérem adja meg az halál okát."),
                height : 48,
                maxLength : 300,
                width : 400,
                disabled : true,
                allowBlank : false,
                isPrintable : true              
			},{
       			xtype: 'monitorcheck',
       			disabled : true
			},{
				xtype: 'fielddetailsbtn'
            },{
                xtype: 'radiofield',
                id : 'visit4_medicalreasondiscontinuation',
                boxLabel: _("a vizsgáló egyéb orvosi ok miatt kizárta"),
                name: 'reasonofDiscontinuation',
                inputValue: 5,
               	handler : function(field) {"use strict";
                    var form = field.up('form');
                    form.setLinkedFieldsState(['otherMedicalReasonDiscontinuationDescription'], this.getValue());
               	}
			},{
				xtype: 'component'
			},{
				xtype: 'component'
			},{
                xtype : 'textarea',
                name : 'otherMedicalReasonDiscontinuationDescription',
                emptyText : _("Kérem részletezze..."),
                maxLengthText : _("max 300 karakter"),
                validateMessage : _("Kérem adja meg az 'egyéb orvosi ok' leírását."),
                fieldLabel : _("Orvosi ok leírása"),
                padding : '0 0 0 12',
                labelWidth : 100,
                width : 400,
                height : 48,
                maxLength : 300,
                disabled : true,
                isPrintable : true,
                allowBlank : false
			},{
       			xtype: 'monitorcheck',
       			disabled : true
			},{
				xtype: 'fielddetailsbtn'
			},{
				xtype: 'radiofield',
				boxLabel: _("a beteg a vizsgálat számára eltűnt"),
				id : 'visit4_reason6',
				name: 'reasonofDiscontinuation',
				inputValue: 6
			},{
				xtype: 'component'
			},{
				xtype: 'component'
			},{
				xtype: 'radiofield',
				boxLabel: _("a vizsgálatot a megbízó befejezte"),
				id : 'visit4_reason7',
				name: 'reasonofDiscontinuation',
				inputValue: 7
			},{
				xtype: 'component'
			},{
				xtype: 'component'
			},{
				xtype: 'radiofield',
				boxLabel: _("a beteg további terápia céljából más ellátó intézménybe került"),
				name: 'reasonofDiscontinuation',
				inputValue: 8
			},{
				xtype: 'component'
			},{
				xtype: 'component'
			},{
				xtype: 'radiofield',
				boxLabel: _("a beteg nem működik közre"),
				name: 'reasonofDiscontinuation',
				inputValue: 9
			},{
				xtype: 'component'
			},{
				xtype: 'component'
			},{
                xtype: 'radiofield',
                id : 'visit4_otherdiscontinuation',
                boxLabel: _("egyéb"),
                name: 'reasonofDiscontinuation',
                inputValue: 10,
               	handler : function(field) {"use strict";
                    var form = field.up('form');
                    form.setLinkedFieldsState(['otherDiscontinuationDescription'], this.getValue());
               	}
            },{
				xtype: 'component'
			},{
				xtype: 'component'
			},{

                xtype : 'textarea',
                name : 'otherDiscontinuationDescription',
                emptyText : _("Kérem részletezze..."),
                maxLengthText : _("max 300 karakter"),
                validateMessage : _("Kérem adja meg az 'egyéb befejezés' leírását."),
                fieldLabel : _("Egyéb leírása"),
                padding : '0 0 0 12',
                labelWidth : 100,
                width : 400,
                height : 48,
                maxLength : 300,
                disabled : true,
                isPrintable : true,
                allowBlank : false
        	},{
        		xtype: 'monitorcheck',
       			disabled : true
			},{
				xtype: 'fielddetailsbtn'

            }]

		}]
	},{
 		xtype:'fieldset',
		title: _("Nemkívánatos események"),
		defaults : {
        	margin : '5 0 5 0',
    	 	maxWidth : 590,
	 	 	msgTarget : 'side',
	 	 	allowBlank : false
       	},	    
		items: [{
			xtype: 'component',
			width: 590,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			width: 32,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			height : 1,
			margin : '0 0 0 0'
		},{
    		xtype : 'radiogroup',
            fieldLabel : _("Történt-e rosszabbodás a társbetegségekben vagy Nemkívánatos Esemény a teljes vizsgálati időszakban?"),
            labelWidth : 400,
            columns: [80,80],
            name: 'aequestionradiogroup',
            items : [{
                name : 'aequestionradiogroup',
                xtype : 'radiofield',
                boxLabel : _("Igen"),
                inputValue : 1,
                listeners : {
                    change : function(rb, newValue, oldValue, options) {
                        if (newValue === true) {
                            var form = this.up('form');
                            if (form !== undefined) {
                                form.showAEWarning();
                            }
                        }
                    }
                }
            },{
                name : 'aequestionradiogroup',
                xtype : 'radiofield',
                boxLabel : _("Nem"),
                inputValue : 2
            }]
		},{
            xtype: 'monitorcheck'
        },{
			xtype: 'fielddetailsbtn'
        }]
	},{
 		xtype:'fieldset',
		title: _("Egyéb kezelések"),
        defaults : {
        	margin : '5 0 5 0',
    	 	maxWidth : 590,
	 	 	msgTarget : 'side',
	 	 	allowBlank : false
       	},
	    
		items: [{
			xtype: 'component',
			width: 590,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			width: 32,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype : 'radiogroup',
            fieldLabel : _("Volt-e a teljes vizsgálati időszakban egyéb kezelése a betegnek?"),
            labelWidth : 400,
            columns: [80,80],
            name: 'mediquestionradiogroup',
            items : [{
                name : 'mediquestionradiogroup',
                xtype : 'radiofield',
                boxLabel : _("Igen"),
                inputValue : 1,
                listeners : {
                    change : function(rb, newValue, oldValue, options) {
                        if (newValue === true ) {
                            var form = this.up('form');
                            if (form !== undefined) {
                                form.showMedicationWarning();
                            }
                        }
                    }
                }
            },{
                name : 'mediquestionradiogroup',
                xtype : 'radiofield',
                boxLabel : _("Nem"),
                inputValue : 2
            }]
		},{
            xtype: 'monitorcheck'
        },{
			xtype: 'fielddetailsbtn'    
		}]
    }]		
});

