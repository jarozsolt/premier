
/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Visit2Form.js
 *
 * Visit 2 form - panel in Visit view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.Visit2Form', {
    extend : 'Kron.view.patient.forms.GeneralPatientForm',
    alias : 'widget.visit2Form',
    id : 'Visit2Form',
    formWidth : 760,
    formHeight : 600,
    formTitle : Kron.formNames.Visit2,
    formIconCls : 'icon-normalvisit-dlg_small',
    autoScroll : true,
    layout : 'fit',
    bodyPadding : 5,

    // We can save the form only if the VisitDate field is not empty
    canSave : function() {"use strict";
        var fieldVisitDate = this.down('[name=visitDate]');
        if (fieldVisitDate) {
            if (fieldVisitDate.getValue() !== null && fieldVisitDate.getValue() !== '') {
                return true;
            }
        }
        return false;
    },
    
    alertSaveError : function() {"use strict";
        Ext.Msg.show({
            title : _("Hiba"),
            msg : _("A 'Vizit dátuma' mezőt kötelező kitölteni a vizit elmentéséhez!"),
            buttons : Ext.Msg.OK,
            icon : Ext.MessageBox.ERROR,
            animateTarget: 'dlgAccept'});
    },
  
   	initForm : function() { "use strict";

    },
    // form érték betöltés utáni ellenőrzéseket , amelyekre alertet kell doni, lehet ide tenni.
	onAfterFormLoad : function(fieldValues) { "use strict";
		var panels;
		
		this.checkMedicationChange(fieldValues);
		// első tab legyen aktív
		this.down('panel').setActiveTab(0);
		// a panel show eventre állítom majd be, hogy scrollozon fel (ilyenkor még nem működik)
		panels = this.down('tabpanel').query('panel');
		panels.forEach(function(panel){
			if(panel.xDoScrollUp === false ) {
				panel.xDoScrollUp = true; 
			}
		});
	},
	
    checkMedicationChange : function(fieldValues) { "use strict";
		var field1Names=[	'aszpirin_elbocsataskor',
                        	'clopidogrel_elbocsataskor',
                        	'prasugrel_elbocsataskor',
                        	'ticlopidine_elbocsataskor',
                        	'egyebantiplatelet_elbocsataskor',
                        	'acegatlo_elbocsataskor',
                        	'sztatin_elbocsataskor',
                        	'antikoagulans_elbocsataskor',
                        	'betablokkolo_elbocsataskor',
                        	'angiotensiniireceptorblokkolo_elbocsataskor',
                        	'nemsztatinlipidcsokkento_elbocsataskor',
                        	'antidiabetikum_elbocsataskor'],
            field2Names=[	'aszpirin',
	                    	'clopidogrel',
	                    	'prasugrel',
	                    	'ticlopidine',
	                    	'egyebantiplatelet',
	                    	'acegatlo',
	                    	'sztatin',
	                    	'antikoagulans',
	                    	'betablokkolo',
	                    	'angiotensiniireceptorblokkolo',
	                    	'nemsztatinlipidcsokkento',
	                    	'antidiabetikum'],           
           	i, field1Value , field2, changed = false;
        
        if (this.getFormFieldValue('Visit1', 'gyogyszer_elbocsataskor') !== undefined && fieldValues['gyogyszer'] !== undefined ) {   	
	       	for (i=0; i<field1Names.length;i++){
	       		if ( this.getFormFieldValue('Visit1', field1Names[i]) != fieldValues[field2Names[i]] ) {
					changed = true;
				}
	       	}    
		if (changed) {
	        	this.showMedicationsChangedWarning2();
	        } 
		}
	},
	
	
    
    items : [{
        xtype : 'tabpanel',
        layout : 'fit',
        activeTab : 0,
        plain : true,
        items : [{
            xtype : 'panel',
            title : _("ISZKÉMIÁS STÁTUSZ"),
            bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    }  
           	},
           	xDoScrollUp : true,
           	listeners : {
                'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{
             	xtype:'fieldset',
			    border: 0,
			    defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
            	 	allowBlank : false,
        	 	 	msgTarget : 'side'
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
                    name : 'visitDate',
                    xtype : 'datefield',
                    fieldLabel : _("Vizit dátuma") + Kron.config.labelMandatoryMark,
                    labelWidth : 200,
                    width : 310,
                    format : Kron.defaultDateFormat,
                    maxValue: new Date(),
        /*            checkDate: function () {"use strict";
                        var form,
                        informDate = this.up('form').down('[name=informDate]');
                        if (informDate !== undefined) {
                            if (informDate.getValue() !== '' && informDate.getValue() !== null && this.getValue() !== '' && this.getValue() !== null) {
                                if (this.getValue() < informDate.getValue()) {
                                    form = this.up('form');
                                    if (form !== undefined) {
                                        form.showVisitDateWarning();
                                    }
                                }
                            }
                        }
                    },
                    listeners: {
                        'change': function (field, newValue, oldValue, eOpts) {"use strict";
                            if (this.isValid()) {
                                this.checkDate();
                            }
                        }
                    },*/
                    isPrintable : true,
                    msgTarget : 'qtip'
                },{
		            xtype: 'monitorcheck'
		        },{
		        	xtype: 'fielddetailsbtn'
				},{
					xtype : 'radiogroup',
	                fieldLabel : _("Vizit típusa"),
	                labelWidth : 200,
	                columns: [150,120],
	                name: 'vizittipus',
	                items : [{
	                    name : 'vizittipus',
	                    xtype : 'radiofield',
	                    boxLabel : _("személyes vizit"),
	                    inputValue : 1
	                },{
	                    name : 'vizittipus',
	                    xtype : 'radiofield',
	                    boxLabel : _("telefonos vizit"),
	                    inputValue : 2
	                }]
                },{
		            xtype: 'monitorcheck'		            
		        },{
					xtype: 'fielddetailsbtn'
				}]
             },{
             	xtype:'fieldset',
       			title: _("Iszkémiás státusz"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
            	 	allowBlank : false,
        	 	 	msgTarget : 'side',
	                labelWidth : 150
               	},
             	items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
                  	xtype : 'radiogroup',
	                fieldLabel : _("Iszkémiás státusz "),
	                columns: [100,100,100,100],
	                name: 'iszkemiasstatus',
	                items : [{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Stabil angina"),
	                    inputValue : 1
	                }, {
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Instabil angina"),
	                    inputValue : 2, 
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['instabilangina'], this.getValue());
                       	}
	                },{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("STEMI"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        }, {
					xtype: 'fielddetailsbtn'
	           },{
					xtype : 'radiogroup',
	                fieldLabel : _("Troponin"),
	                width: 560,
	                columns: 2,
	                name: 'instabilangina',
	                disabled : true,
	                items : [{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("Pozitív"),
	                    inputValue : 1
	                },{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("Negatív"),
	                    inputValue : 2 
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'

	           	}]
 			}]
		}, {  /*************************************************************************************************************/
                
            xtype : 'panel',
            title : _("GYÓGYSZEREK"),
           	bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    },  
			    defaults : {
                	margin : '5 0 5 0'
               	}
           	},
           	xDoScrollUp : true,
           	listeners : {
                'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{
            	xtype:'fieldset',
       			title: _("Gyógyszerek"),
 				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Szed-e a beteg valamilyen gyógyszert jelenleg"),
	                columns: [80,60],
	                name: 'gyogyszer',
	                allowBlank : false,
	                items : [{
	                    name : 'gyogyszer',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form'), checkBoxGroup;
                            form.setLinkedFieldsState([
                            	'gyogyszerdisplayfield',
                            	'aszpirin',
                            	'clopidogrel',
                            	'prasugrel',
                            	'ticlopidine',
                            	'egyebantiplatelet',
                            	'acegatlo',
                            	'sztatin',
                            	'antikoagulans',
                            	'betablokkolo',
                            	'angiotensiniireceptorblokkolo',
                            	'nemsztatinlipidcsokkento',
                            	'antidiabetikum'], this.getValue());
                        	checkBoxGroup = form.down('checkboxgroup[name=gyogyszercheckboxgroup]');
                            if (this.getValue()) {
                            	checkBoxGroup.allowBlank = false;
                            } else {
                            	checkBoxGroup.allowBlank = true;
                            }
                            checkBoxGroup.validate();
                       	}
	                },{
	                    name : 'gyogyszer',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2, 
	                    allowBlank : false
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
					
           			xtype : 'displayfield',
           			name : 'gyogyszerdisplayfield',
                    value : _("Jelölje mindet, amit a beteg aktuálisan szed"),
                    disabled : true,
                    colspan: 3
               },{
               		xtype: 'checkboxgroup',
               	    name : 'gyogyszercheckboxgroup',
 					colspan: 3,
	           		width : 660,
		            defaults : {
		                margin : '5 0 5 0',
		                maxWidth : 585
		            },
		            layout: {
			            type: 'table',
			            columns: 3
			        },
		            items: [{
						xtype: 'component',
						width: 590,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						width: 32,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						height : 1,
						margin : '0 0 0 0'
					},{	
	               		xtype: 'checkboxfield',
	               		width: 570,
	               		boxLabel:  _("Aszpirin"),
	               		disabled : true,
	               		name: 'aszpirin',
	               		padding : '0 0 0 12'
	               		
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Clopidogrel"),
	               		name: 'clopidogrel',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Prasugrel"),
	               		name: 'prasugrel',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Ticlopidine"),
	               		name: 'ticlopidine',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Egyéb antiplatelet gyógyszer"),
	               		name: 'egyebantiplatelet',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("ACE gátló"),
	               		disabled : true,
	               		name: 'acegatlo',
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Sztatin"),
	               		name: 'sztatin',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Antikoaguláns"),
	               		name: 'antikoagulans',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Béta-blokkoló"),
	               		name: 'betablokkolo',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Angiotensin II receptor blokkoló"),
	               		name: 'angiotensiniireceptorblokkolo',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Nem-sztatin lipidcsökkentő szer"),
	               		name: 'nemsztatinlipidcsokkento',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Antidiabetikum"),
	               		name: 'antidiabetikum',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
					}]
				}]
 			}]
		}, {  /*************************************************************************************************************/
                
            xtype : 'panel',
            title : _("EGYÉB KOMPLIKÁCIÓ"),
           	bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    },  
			    defaults : {
                	margin : '5 0 5 0'
               	}
           	},
           	xDoScrollUp : true,
           	listeners : {
                'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{
             	xtype:'fieldset',
       			title: _("Komplikáció a beavatkozást követően"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
     				xtype : 'radiogroup',
	                fieldLabel : _("Történt-e valamilyen komplikáció a kórházi elbocsájtás óta"),
	                labelWidth : 400,
	                width: 560,
	                columns: [80,80],
	                allowBlank : false,
	                name: 'komplikacio',
	                items : [{
	                    name : 'komplikacio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form'), checkBoxGroup;
                            form.setLinkedFieldsState(['komplikacioheader',
                            	'halal',
								'stroke',
								'myocardialisinfarktus',
								'ismeteltrevaszkularizacio',
								'verzes',
								'definitivstenttrombozis',
								'egyebkomplikacio'
                            	], this.getValue());
                            if ( this.getValue() ) {
                            	form.showAEWarning();
                            }
                            checkBoxGroup = form.down('checkboxgroup[name=komplikaciocheckboxgroup]');
                            if (this.getValue()) {
                            	checkBoxGroup.allowBlank = false;
                            } else {
                            	checkBoxGroup.allowBlank = true;
                            }
                            checkBoxGroup.validate();
                       	}
	                },{
	                    name : 'komplikacio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
		        },{
           			xtype : 'displayfield',
                    name : 'komplikacioheader',
                    colspan: 3,
                    value : _("Jelölje a komplikáció(ka)t:"),
                    padding : '0 0 0 12',
                    disabled:true
               },{
               	    xtype: 'checkboxgroup',
               	    name : 'komplikaciocheckboxgroup',
 					colspan: 3,
	           		width : 660,
		            defaults : {
		                margin : '5 0 5 0',
		                maxWidth : 585
		            },
		            layout: {
			            type: 'table',
			            columns: 3
			        },
		            items: [{
						xtype: 'component',
						width: 590,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						width: 32,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						height : 1,
						margin : '0 0 0 0'
					},{
              
              
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Halál"),
	               		name: 'halal',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled :true
			        },{
						xtype: 'fielddetailsbtn'
	               },{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Stroke"),
	               		name: 'stroke',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled :true
			        },{
						xtype: 'fielddetailsbtn'
	               },{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Myocardiális infarktus"),
	               		name: 'myocardialisinfarktus',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck'
			        },{
						xtype: 'fielddetailsbtn'
	               },{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Ismételt revaszkularizáció"),
	               		name: 'ismeteltrevaszkularizacio',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled :true
			        },{
						xtype: 'fielddetailsbtn'
	               },{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Vérzés"),
	               		name: 'verzes',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled :true
			        },{
						xtype: 'fielddetailsbtn'
	               },{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Definitív stent trombózis (angiográfiával vagy patológiailag igazolt)"),
	               		name: 'definitivstenttrombozis',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled :true
			        },{
						xtype: 'fielddetailsbtn'
	               },{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Egyéb, részletezze"),
	               		name: 'egyebkomplikacio',
	               		padding : '0 0 0 12',
	               		disabled : true,
	               		handler : function(field) {"use strict";
	                            var form = field.up('form');
	                            form.setLinkedFieldsState(['egyebkomplikacioreszletek'], this.getValue());
	                       	}
					},{
			            xtype: 'monitorcheck',
			            disabled :true
			        },{
						xtype: 'fielddetailsbtn'
	               },{
	               		xtype : 'textarea',
	                    name : 'egyebkomplikacioreszletek',
	                    historyLabel : _('Egyéb komplikáció részletezése'),
	                    emptyText : _("Kérem részletezze..."),
	                    maxLengthText : _("max 300 karakter"),
	                    validateMessage : _("Kérem adja meg a részleteket."),
	                    margin : '10 0 5 14',
	                    width : 580,
	                    height : 48,
	                    maxLength : 300,
	                    disabled : true,
	                    allowBlank : false,
	                    isPrintable : true,
	                    msgTarget: 'qtip'
					},{
			            xtype: 'monitorcheck',
			            disabled :true
			        },{
						xtype: 'fielddetailsbtn'
	         		}]
	         	}]
         	},{
         		xtype:'fieldset',
       			title: _("Nemkívánatos események"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
            		xtype : 'radiogroup',
	                fieldLabel : _("Történt-e rosszabbodás a társbetegségekben vagy történ-e Nemkívánatos Esemény az előző vizit óta"),
	                labelWidth : 400,
	                columns: [80,80],
	                name: 'rosszabbodastarsbetegsegben_ae',
	                items : [{
	                    name : 'rosszabbodastarsbetegsegben_ae',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    listeners : {
                            change : function(rb, newValue, oldValue, options) {
                                if (newValue === true) {
                                    var form = this.up('form');
                                    if (form !== undefined) {
                                        form.showAEWarning();
                                    }
                                }
                            }
                        }
	                },{
	                    name : 'rosszabbodastarsbetegsegben_ae',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
		        }]
			},{
         		xtype:'fieldset',
       			title: _("Egyéb kezelések"),
                defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype : 'radiogroup',
	                fieldLabel : _("Történt-e változás az előző vizithez képest az egyéb kezelésekben"),
	                labelWidth : 400,
	                columns: [80,80],
	                name: 'valtozas_egyebkezelesekben',
	                items : [{
	                    name : 'valtozas_egyebkezelesekben',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    listeners : {
                            change : function(rb, newValue, oldValue, options) {
                                if (newValue === true ) {
                                    var form = this.up('form');
                                    if (form !== undefined) {
                                        form.showMedicationWarning();
                                    }
                                }
                            }
                        }
	                },{
	                    name : 'valtozas_egyebkezelesekben',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
			            
				}]
             }]
       }]
    }]
});
