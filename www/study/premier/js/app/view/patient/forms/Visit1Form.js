/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Visit1Form.js
 *
 * Visit 1 form - panel in Visit view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.Visit1Form', {
    extend : 'Kron.view.patient.forms.GeneralPatientForm',
    requires:  [  'Kron.view.patient.forms.GeneralPatientForm' , 'Kron.view.patient.forms.fields.*'],
    alias : 'widget.visit1Form',
    id : 'Visit1Form',
    formWidth : 760,
    formHeight : 600,
    formTitle : Kron.formNames.Visit1,
    formIconCls : 'icon-normalvisit-dlg_small',
    autoScroll : true,
    layout : 'fit',

    bodyPadding : 5,
    
    // We can save the form only if the informDate field is not empty
    canSave : function() {"use strict";
        var fieldInformDate = this.down('[name=informDate]'),
        	fieldVisitDate = this.down('[name=visitDate]');;
        if (fieldInformDate && fieldVisitDate) {
            if (fieldInformDate.getValue() !== null && fieldInformDate.getValue() !== '' && fieldVisitDate.getValue() !== null && fieldVisitDate.getValue() !== '') {
                return true;
            }
        }
        return false;
    },
    
    // Alert an error message if the patient inform date is not filled
    alertSaveError : function() {"use strict";
        Ext.Msg.show({
            title : _("Hiba"),
            msg : _("A 'Vizit dátuma' és a 'Betegtájékoztató és Beleegyező nyilatkozat aláírásának dátuma' mezőt kötelező kitölteni a vizit elmentéséhez!"),
            buttons : Ext.Msg.OK,
            icon : Ext.MessageBox.ERROR,
            animateTarget: 'dlgAccept'});
    },
    
 
/*
 * From inicializásál, formfield értékekkel
 */
    initForm : function(fieldValues) { "use strict";
    	
		this.deleteExtraFieldSets();

		this.addExtraFields(fieldValues);
		
		this.isFormDirty = false;

		return true;
	},
	// mentés előtti ellenőrzéseket, amelyekre alertet kell doni, lehet ide tenni
	alertBeforeSave : function(fieldValues) { "use strict";
	},
	// form érték betöltés utáni ellenőrzéseket , amelyekre alertet kell doni, lehet ide tenni.
	onAfterFormLoad : function(fieldValues) { "use strict";
		var panels;
		this.checkMedicationChange(fieldValues);
		
		// első tab legyen aktív
		this.down('panel').setActiveTab(0);
		// a panel show eventre állítom majd be, hogy scrollozon fel (ilyenkor még nem működik)
		panels = this.down('tabpanel').query('panel');
		panels.forEach(function(panel){
			if(panel.xDoScrollUp === false ) {
				panel.xDoScrollUp = true; 
			}
		});
		
		// ráállok az első mezőre 
		this.down('[name=visitDate]').focus();
	},
	
	checkMedicationChange : function(fieldValues) { "use strict";
		/*var field1Names=['aszpirin',
                	'clopidogrel',
                	'egyebtrombocitaaggregaciogatlo',
                	'acegatlo',
                	'sztatin',
                	'antikoagulans',
                	'betablokkolo',
                	'angiotensiniireceptorblokkolo',
                	'nemsztatinlipidcsokkento'],
        field2Names=['aszpirin_elbocsataskor',
                	'clopidogrel_elbocsataskor',
                	'egyebtrombocitaaggregaciogatlo_elbocsataskor',
                	'acegatlo_elbocsataskor',
                	'sztatin_elbocsataskor',
                	'antikoagulans_elbocsataskor',
                	'betablokkolo_elbocsataskor',
                	'angiotensiniireceptorblokkolo_elbocsataskor',
                	'nemsztatinlipidcsokkento_elbocsataskor'],
       	i, field1, field2, changed = false, formwarning;
         if ( fieldValues['szivgyogyszer'] !== undefined && fieldValues['szivgyogyszer_elbocsataskor'] !== undefined) {
        	for (i=0; i<field1Names.length;i++){
				if ( fieldValues[field1Names[i]] != fieldValues[field2Names[i]] ) {
					changed = true;
				}
	       	}    	
	        if (changed) {
	            this.showMedicationsChangedWarning1();
	        } 
        }	
       	*/
	},

	// A gyerkőcnél kell definiálni egy "extraFields" nevű object-et, aminek az attrinutumai a hozzáadó fv-ek, a kulcsok a extrafield suffix nevek
	addExtraFields : function (fieldValues) {"use strict";
		var valueTokens=[],
			extraFieldIds=[],
			extraFieldContainer,
			separator = '_',
			me = this,
			ballonField,
			deleteBtn;
			
		Ext.Object.each(this.extraFields, function(key, value, myself) {
			// összegyűjtön az extraField Id kat
			extraFieldIds=[];
			Ext.Object.getKeys(fieldValues).forEach(function (element, index, array) {
			//	console.log("a[" + index + "] = " + element);
				valueTokens = element.split(separator);
				if (valueTokens[0] == key) {
					if (extraFieldIds.indexOf(valueTokens[1]) == -1) {
						extraFieldIds.push(valueTokens[1]);
					}
				}
			});
			extraFieldContainer=me.down('[name=' + key + 'container]');
			if (extraFieldIds.length>0) {
				extraFieldIds.sort(function(a,b) {
					var ia = parseInt(a),ib = parseInt(b);
					if (ia < ib) {
						return -1;
					} else if (ia > ib) {
					   return 1;
					} else {   
						return 0;
					}
				});
				
				Ext.suspendLayouts();	
				Ext.Array.each(extraFieldIds, function(element, index, array){
					value(extraFieldContainer, element);
				});
				Ext.resumeLayouts(true);
			} 
			
		});
		
		return true;
	},

	/*
	* kitörli az összes extra fieldsetet
	*/
	deleteExtraFieldSets : function () {"use strict";
		var extraFieldContainerNames = [ 'ppstentcontainer', 'predilballoncontainer', 'postdilballoncontainer'],
			container, success= true, me = this;
		Ext.suspendLayouts();	
		Ext.Array.each(extraFieldContainerNames, function(name, index, countriesItSelf) {
			container = me.down('[name=' + name + ']');
			if (container !== undefined && container !== null) {
				container.removeAll();	
			} else {
				success = false;
			}
		});
		Ext.resumeLayouts(true);
		return success;
	},
	
	/*
	* extraFieldContainer - container to add field sets
	* id - id of added field set
	*/
	extraFields : { 
		'predilballon' : function(extraFieldContainer, id) {"use strict";
			var i=0;
			if (extraFieldContainer) {
				if (id !== undefined &&  id !== null) {
					i=id;
				} else {
					extraFieldContainer.items.each(function(item, index, len) {
						if (item.extraFieldId >= i) {
							i = parseInt(item.extraFieldId) + 1 ;
						}
					});
				}
	
				extraFieldContainer.add({
									xtype: 'fieldset',
									title : _("Predil Ballon"),
									extraFieldId : i,
									width : 655,
									labelWidth: 300,
									layout: {
											type: 'table',
											columns: 2
									},
									
									items: [{
										xtype: 'container',
										width: 50,
										items : [{
											xtype : 'button',
											buttonType : 'extrafield',
											hideMode : 'visibility',
											width: 50,
											text : _("Törlés"),
											handler : function(button) {"use strict";
												var extraFieldSet = button.up('fieldset'),
													ballonContainer = extraFieldSet.up('fieldcontainer');
											
												Ext.MessageBox.prompt({
								                    title : _("Figyelmeztetés"),
								                    msg :  _("Biztos, hogy törölni kívánja a ballont?"),
								                    buttons : Ext.Msg.OKCANCEL,
								                    buttonText: {ok: _("Töröl"), cancel: _("Mégsem")},
								                    icon : Ext.MessageBox.WARNING,
								                    animateTarget: 'dlgAccept',
								                    scope : this,
								                    fn : function(btn, ev) {
								                        if (btn === 'ok') {
								                            button.up('form').isFormDirty = true;		
															ballonContainer.remove(extraFieldSet);
															ballonContainer.focus();
								                        }
								                    }
								                });	
								           }
										}]
									
									},{
										xtype: 'fieldset',
										border: 0,
										width : 603,
										labelWidth: 200,
										margin : '5 0 0 0',
										layout: {
											type: 'table',
											columns: 3
										},
										
										items: [{
											xtype: 'component',
											width: 514,
											height : 1,
											margin : '0 0 0 0'
										},{
											xtype: 'component',
											width: 32,
											height : 1,
											margin : '0 0 0 0'
										},{
											xtype: 'component',
											height : 1,
											width: 32,
											margin : '0 0 0 0'
										},{
											xtype: 'ballonfield',
											name: 'predilballon_' + i + '_field',
											historyLabel : _('Predil Ballon adatai'),
											width : 514,
											allowBlank : false
						               	},{
								            xtype: 'monitorcheck',
								            disabled : true
								   		},{
											xtype: 'fielddetailsbtn'
										}]
									}]
									
				})
				extraFieldContainer.down('[name=predilballon_' + i + '_field]').focus();
				extraFieldContainer.down('[name=predilballon_' + i + '_field]').validate();
				
			}
		},
		'postdilballon' : function(extraFieldContainer, id) {"use strict";
			var i=0;
			if (extraFieldContainer) {
				if (id !== undefined &&  id !== null) {
					i=id;
				} else {
					extraFieldContainer.items.each(function(item, index, len) {
						if (item.extraFieldId >= i) {
							i = parseInt(item.extraFieldId) + 1 ;
						}
					});
				}
	
				extraFieldContainer.add({
									xtype: 'fieldset',
									title : _("Postdil Ballon"),
									extraFieldId : i,
									width : 655,
									labelWidth: 300,
									layout: {
											type: 'table',
											columns: 2
									},
									
									items: [{
										xtype: 'container',
										width: 50,
										items : [{
											xtype : 'button',
											buttonType : 'extrafield',
											hideMode : 'visibility',
											width: 50,
											text : _("Törlés"),
											handler : function(button) {"use strict";
												var extraFieldSet = button.up('fieldset'),
													ballonContainer = extraFieldSet.up('fieldcontainer');
											
												Ext.MessageBox.prompt({
								                    title : _("Figyelmeztetés"),
								                    msg :  _("Biztos, hogy törölni kívánja a ballont?"),
								                    buttons : Ext.Msg.OKCANCEL,
								                    buttonText: {ok: _("Töröl"), cancel: _("Mégsem")},
								                    icon : Ext.MessageBox.WARNING,
								                    animateTarget: 'dlgAccept',
								                    scope : this,
								                    fn : function(btn, ev) {
								                        if (btn === 'ok') {
								                            button.up('form').isFormDirty = true;		
															ballonContainer.remove(extraFieldSet);
															ballonContainer.focus();
								                        }
								                    }
								                });	
								           }
										}]
									
									},{
										xtype: 'fieldset',
										border: 0,
										width : 603,
										labelWidth: 200,
										margin : '5 0 0 0',
										layout: {
											type: 'table',
											columns: 3
										},
										
										items: [{
											xtype: 'component',
											width: 514,
											height : 1,
											margin : '0 0 0 0'
										},{
											xtype: 'component',
											width: 32,
											height : 1,
											margin : '0 0 0 0'
										},{
											xtype: 'component',
											height : 1,
											width: 32,
											margin : '0 0 0 0'
										},{
											xtype: 'ballonfield',
											name: 'postdilballon_' + i + '_field',
											historyLabel : _('Postdil Ballon adatai'),
											width : 514,
											allowBlank : false
						               	},{
								            xtype: 'monitorcheck',
								            disabled : true
								   		},{
											xtype: 'fielddetailsbtn'
										}]
									}]
									
				})
				extraFieldContainer.down('[name=postdilballon_' + i + '_field]').focus();
				extraFieldContainer.down('[name=postdilballon_' + i + '_field]').validate();
				
			}
		},
		'ppstent' : function(extraFieldContainer, id) {"use strict";
			var i=0;
			if (extraFieldContainer) {
				if (id !== undefined &&  id !== null) {
					i=id;
				} else {
					extraFieldContainer.items.each(function(item, index, len) {
						if (item.extraFieldId >= i) {
							i = parseInt(item.extraFieldId) + 1 ;
						}
					});
				}
	
				extraFieldContainer.add({
									xtype: 'fieldset',
									title : _("Stent"),
									extraFieldId : i,
									width : 655,
									labelWidth: 300,
									layout: {
											type: 'table',
											columns: 2
									},
									
									items: [{
										xtype : 'button',
										buttonType : 'extrafield',
										width: 50,
										text : _("Törlés"),
										handler : function(button) {"use strict";
											var extraFieldSet = button.up('fieldset'),
												stentContainer = extraFieldSet.up('fieldcontainer');
											Ext.MessageBox.prompt({
							                    title : _("Figyelmeztetés"),
							                    msg :  _("Biztos, hogy törölni kívánja a stentet?"),
							                    buttons : Ext.Msg.OKCANCEL,
							                    buttonText: {ok: _("Töröl"), cancel: _("Mégsem")},
							                    icon : Ext.MessageBox.WARNING,
							                    animateTarget: 'dlgAccept',
							                    scope : this,
							                    fn : function(btn, ev) {
							                        if (btn === 'ok') {
														button.up('form').isFormDirty = true;	
														stentContainer.remove(extraFieldSet);
														stentContainer.focus();
							                        }
							                    }
							                });	

										}
									
									},{
										xtype: 'fieldset',
										border: 0,
										width : 603,
										labelWidth: 200,
										layout: {
											type: 'table',
											columns: 3
										},
										items: [{
											xtype: 'component',
											width: 514,
											height : 1,
											margin : '0 0 0 0'
										},{
											xtype: 'component',
											width: 32,
											height : 1,
											margin : '0 0 0 0'
										},{
											xtype: 'component',
											height : 1,
											width: 32,
											margin : '0 0 0 0'
										},{
											xtype: 'fieldcontainer',
											width : 350,
											layout: 'hbox',
											items: [{
												xtype: 'combobox',
												labelWidth : 229,
												width: 300,
												name: 'ppstent_' + i + '_hossz',
												fieldLabel:'Hossz',
												queryMode:'local',
												store:['8', '12', '16', '20', '24', '28', '32', '38','n.a.'],
												forceSelection: true,
		       									allowBlank : false		
										   },{
												xtype : 'displayfield',
												width : 50,
												doNotSave : true,
												padding : '0 0 0 5',
												value : 'mm'
										   }]
										},{
											xtype: 'monitorcheck',
											referencedField: 'ppstent_' + i + '_hossz',
							            	disabled : true
										},{
											xtype: 'fielddetailsbtn',
											referencedField: 'ppstent_' + i + '_hossz'
										},{
											xtype: 'fieldcontainer',
											width : 350,
											layout: 'hbox',
											items: [{
												xtype: 'combobox',
												labelWidth : 229,
												width: 300,
												name:  'ppstent_' + i + '_atmero',
												fieldLabel:'Átmérő',
												queryMode:'local',
												store:['2.25', '2.5', '2.75', '3.0', '3.5', '4.0', '4.5','n.a.'],
												forceSelection: true,
		       									allowBlank : false
										   },{
												xtype : 'displayfield',
												width : 50,
												doNotSave : true,
												padding : '0 0 0 5',
												value : 'mm'
										   }]
										},{
											xtype: 'monitorcheck',
											referencedField: 'ppstent_' + i + '_atmero',
							            	disabled : true
										},{
											xtype: 'fielddetailsbtn',
											referencedField: 'ppstent_' + i + '_atmero'
										},{
						               		xtype : 'displayfield',
						               		colspan: 3,
								            doNotSave : true,
								            width : 505,
								            value : _("Stent beültetés leírása")
						               },{
						               		xtype : 'radiogroup',
							                fieldLabel : _("A lézió elő-tágítása megtörtént"),
							                labelWidth : 229,
							                columns: [80,70],
							                allowBlank : false,
							                msgTarget: 'side',
							                name: 'ppstent_' + i + '_lezioelotagitas',
							                items : [{
							                    name : 'ppstent_' + i + '_lezioelotagitas',
							                    xtype : 'radiofield',
							                    boxLabel : _("Igen"),
							                    inputValue : 1
							                },{
							                    name : 'ppstent_' + i + '_lezioelotagitas',
							                    xtype : 'radiofield',
							                    boxLabel : _("Nem"),
							                    inputValue : 2 
							                }]
										},{
								            xtype: 'monitorcheck',
							            	disabled : true
								        },{
											xtype: 'fielddetailsbtn'
										},{
											xtype : 'vitalsignfield',
						                    name : 'ppstent_' + i + '_maxinflaciosnyomas',
						                    fieldLabel : _("Maximális inflációs nyomás"),
						                    width: 500,
						                    labelWidth : 229,
						                    valueMin : 0,
						                    valueMax : 99,
						                    unit : _("atm"),
						                    isPrintable : true,
						                    allowBlank : false
						           		},{
								            xtype: 'monitorcheck',
								            referencedField: 'ppstent_' + i + '_maxinflaciosnyomas',
							            	disabled : true
								        },{
											xtype: 'fielddetailsbtn',
											referencedField: 'ppstent_' + i + '_maxinflaciosnyomas'
										},{
						               		xtype : 'radiogroup',
							                fieldLabel : _("A stent teljesen kifeszült vizuálisan"),
											labelWidth : 229,
							                columns: [80,70],
							                allowBlank : false,
							                msgTarget: 'side',
							                name: 'ppstent_' + i + '_stentkifeszult',
							                items : [{
							                    name : 'ppstent_' + i + '_stentkifeszult',
							                    xtype : 'radiofield',
							                    boxLabel : _("Igen"),
							                    inputValue : 1
							                },{
							                    name : 'ppstent_' + i + '_stentkifeszult',
							                    xtype : 'radiofield',
							                    boxLabel : _("Nem"),
							                    inputValue : 2 
							                }]
										},{
								            xtype: 'monitorcheck',
							            	disabled : true
								        },{
											xtype: 'fielddetailsbtn'
										}]			
									}]
								});
				
				extraFieldContainer.down('[name=ppstent_' + i + '_hossz]').focus();
				extraFieldContainer.down('[name=ppstent_' + i + '_hossz]').isValid();
				extraFieldContainer.down('[name=ppstent_' + i + '_atmero]').isValid();
				extraFieldContainer.down('radiogroup[name=ppstent_' + i + '_lezioelotagitas]').validate();
				extraFieldContainer.down('[name=ppstent_' + i + '_maxinflaciosnyomas]').isValid();
				extraFieldContainer.down('radiogroup[name=ppstent_' + i + '_stentkifeszult]').validate();
			}
		 
		}	
	},
	
   // Itt indul a for definíció ***********************************************************************************************************
    
    items : [{
        xtype : 'tabpanel',
       	layout : 'fit',
        activeTab : 0,
        plain : true,
        items : [{
            xtype : 'panel',
            title : _("ANAMNÉZIS"),
            bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            xDoScrollUp : true,
           	listeners : {
                'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    }  
           	},
            items : [{
             	xtype:'fieldset',
			    border: 0,
			    defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
            	 	allowBlank : false,
        	 	 	msgTarget : 'side'
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
                    name : 'visitDate',
                    xtype : 'datefield',
                    fieldLabel : _("Vizit dátuma") + Kron.config.labelMandatoryMark,
                    labelWidth : 400,
                    width : 510,
                    format : Kron.defaultDateFormat,
                    maxValue: new Date(),
                    checkDate: function () {"use strict";
                        var form,
                        informDate = this.up('form').down('[name=informDate]');
                        if (informDate !== undefined) {
                            if (informDate.getValue() !== '' && informDate.getValue() !== null && this.getValue() !== '' && this.getValue() !== null) {
                                if (this.getValue() < informDate.getValue()) {
                                    form = this.up('form');
                                    if (form !== undefined) {
                                        form.showVisitDateWarning();
                                    }
                                }
                            }
                        }
                    },
                    listeners: {
                        'change': function (field, newValue, oldValue, eOpts) {"use strict";
                            if (this.isValid()) {
                                this.checkDate();
                            }
                        }
                    },
                    isPrintable : true,
                    msgTarget : 'qtip'
                },{
		            xtype: 'monitorcheck'
		        },{
		        	xtype: 'fielddetailsbtn'
				},{
                    name : 'informDate',
                    xtype : 'datefield',
                    fieldLabel : _("Betegtájékoztató és Beleegyező nyilatkozat aláírásának dátuma") + Kron.config.labelMandatoryMark,
                  	labelWidth : 400,
                    width : 510,
                    format : Kron.defaultDateFormat,
                    maxValue: new Date(),
                    checkDate: function () {"use strict";
                        var form = this.up('form'),
                        	visitDate = form.down('[name=visitDate]');
                        if (visitDate !== undefined ) {
                            if (visitDate.getValue() !== '' && visitDate.getValue() !== null && this.getValue() !== '' && this.getValue() !== null) {
                                if ( Ext.Date.clearTime(this.getValue()).getTime() > Ext.Date.clearTime(visitDate.getValue()).getTime() ) {
                                    if (form !== undefined) {
                                        form.showVisitDateWarning();
                                    }
                                }
                            }
                        }
                    },
                    listeners: {
                        'change': function (field, newValue, oldValue, eOpts) {"use strict";
                            if (this.isValid()) {
                                this.checkDate();
                            }
                        }
                    },
                    msgTarget : 'qtip',
                    isPrintable : true
                },{
		            xtype: 'monitorcheck'		            
		        },{
					xtype: 'fielddetailsbtn'
				}]
             },{
             	xtype:'fieldset',
       			title: _("Demográfia"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
            	 	allowBlank : false,
        	 	 	msgTarget : 'side'
               	},
             	items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
                    // TODO: a dátumformátum ellenőrzése nem fut le, csak a speciális ellenőrzés
                    name : 'birthDate',
                    xtype : 'datefield',
                    fieldLabel : _("Születési idő"),
                    labelWidth: 400,
                    width : 510,
                    format : Kron.defaultDateFormat,
                    maxValue: new Date(),
                    msgTarget : 'qtip',
                    isPrintable : true
                },{
		            xtype: 'monitorcheck',
		            id:'errorteszt_01'
		        },{
					xtype: 'fielddetailsbtn'
				},{
                    xtype : 'radiogroup',
                    fieldLabel : _("Nem "),
                    labelWidth : 400,
                    columns: [60, 60],
                    name: 'gender',
                    items : [{
                        name : 'gender',
                        xtype : 'radiofield',
                        boxLabel : _("nő"),
                        inputValue : 1
                        
                    },{
                        name : 'gender',
                        xtype : 'radiofield',
                        boxLabel : _("férfi"),
                        inputValue : 2
                       
                    }],
                    isPrintable : true
                },{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				}]
             	
            },{
             	xtype:'fieldset',
       			title: _("Vitális jelek"),
             	defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
            	 	allowBlank : false,
        	 	 	msgTarget : 'side',
        	 	 	labelWidth : 300
               	},
             	items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
                    xtype : 'displayfield',
                    doNotSave : true,
                    value : _("Vérnyomás (ülő helyzetben)"),
                    colspan : 3
                 },{
         		 	xtype : 'vitalsignfield',
                    name : 'vernyomassystoles',
                    fieldLabel : _("Szisztolés"),
                    width: 520,
                    valueMin : 50,
                    valueMax : 200,
                    unit : _("Hgmm"),
                    isPrintable : true,
                    onValueChanged : function(field, newValue) {"use strict";
                        var diastoles, fieldDiastoles,
                        	form = this.up('form');
                        if (newValue !== '' && newValue > 140)
                        {
                            form.showBloodPressureWarning();
                        }
                    }
                },{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn' 
				},{
         		 	xtype : 'vitalsignfield',
                    name : 'vernyomasdiastoles',
                    fieldLabel : _("Diasztolés"),
                    width: 520,
                    valueMin : 50,
                    valueMax : 200,
                    unit : _("Hgmm"),
                    isPrintable : true,
                    onValueChanged : function(field, newValue) {"use strict";
                        var systoles, fieldSystoles,
                        	form = this.up('form');
                        if (newValue !== '' && newValue > 95)
                        {
                            form.showBloodPressureWarning();
                        }

                    }
                },{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
         		 	xtype : 'vitalsignfield',
                    name : 'height',
                    fieldLabel : _("Magasság"),
                    width: 520,
                    valueMin : 100,
                    valueMax : 210,
                    unit : _("cm"),

                    isPrintable : true
               },{	
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
                    xtype : 'vitalsignfield',
                    name : 'weight',
                    fieldLabel : _("Testsúly"),
                   	width: 520,
                    valueMin : 35,
                    valueMax : 150,
                    unit : _("kg"),
   
                    isPrintable : true
                },{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				}]
         	},{
             	xtype:'fieldset',
       			title: _("Kórelőzmény / Rizikófaktorok"),

             	defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
            	 	allowBlank : false,
        	 	 	msgTarget : 'side',
        	 	 	labelWidth : 400
               	},
             	items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
                    xtype : 'radiogroup',
                    fieldLabel : _("Dohányzás "),
                    labelWidth : 300,
                    columns: 1,
                    name: 'smoking',
                    items : [{
                        name : 'smoking',
                        xtype : 'radiofield',
                        boxLabel : _("soha sem dohányzott"),
                        inputValue : 1

                    },{
                        name : 'smoking',
                        xtype : 'radiofield',
                        boxLabel : _("korábban dohányzott, abbahagyta"),
                        inputValue : 2,
                        handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['smoking_stopped'], this.getValue());
                       	} 

                    },{
                        name : 'smoking',
                        xtype : 'radiofield',
                        boxLabel : _("jelenleg is dohányzik"),
                        inputValue : 3
                     }],

                    isPrintable : true
                },{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
	            },{// év
                	xtype: 'numberfield',
					disabled : true,
					width: 510,
			        name: 'smoking_stopped',
			        fieldLabel: _("Dohányzás abbahagyásának éve"),
			        maxValue: 2020,
			        minValue: 1900,
			        msgTarget: 'qtip'                	
	            },{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
	            },{
	                xtype : 'radiogroup',
	                fieldLabel : _("Diabétesz mellitus"),
	                labelWidth : 300,
	                columns: [100,80],
	                name: 'diabetesz',
	                items : [{
	                    name : 'diabetesz',
	                    xtype : 'radiofield',
	                    boxLabel : _("igen"),
	                    inputValue : 1,
	                	handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['diabetesztipus'], this.getValue());
                       	}
	                },{
	                    name : 'diabetesz',
	                    xtype : 'radiofield',
	                    boxLabel : _("nem"),
	                    inputValue : 2 
	                }]
			   },{
		            xtype: 'monitorcheck'
		       },{
					xtype: 'fielddetailsbtn'
	           },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Diabétesz típusa"),
					disabled : true,
					labelWidth : 300,
	                columns : [100,80],
	                name: 'diabetesztipus',
	                items : [{
	                    name : 'diabetesztipus',
	                    xtype : 'radiofield',
	                    boxLabel : _("I. Típus"),
	                    inputValue : 1
	                },{
	                    name : 'diabetesztipus',
	                    xtype : 'radiofield',
	                    boxLabel : _("II. Típus"),
	                    inputValue : 2 
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
	            },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("A családban van-e kardiovaszkuláris betegség"),
	                columns: 3,
	                width: 580,
	                labelWidth : 300,
	                name: 'ardiovaszk',
	                items : [{
	                    name : 'ardiovaszk',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'ardiovaszk',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                },{
	                    name : 'ardiovaszk',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
	            },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Perifériás érbetegség a kórelőzményben"),
	                columns: 3,
	                width: 580,
	                labelWidth : 300,
	                name: 'periferiaserbetegseg',
	                items : [{
	                    name : 'periferiaserbetegseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'periferiaserbetegseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                },{
	                    name : 'periferiaserbetegseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem Ismert"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
	            },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Megelőző stroke vagy tranziens iszkémiás attak (TIA)"),
	                columns: 3,
	                width: 580,
	                labelWidth : 300,
	                name: 'stroke',
	                items : [{
	                    name : 'stroke',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'stroke',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                },{
	                    name : 'stroke',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
	            },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Megelőző miokardiális infarktus (MI)"),
	                columns: 3,
	                width: 580,
	                labelWidth : 300,
	                name: 'miokardialisinfarktus',
	                items : [{
	                    name : 'miokardialisinfarktus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['miokardialisinfarktusdetails'], this.getValue());
                       	}
	                },{
	                    name : 'miokardialisinfarktus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                },{
	                    name : 'miokardialisinfarktus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
	           	},{
			   		xtype : 'textarea',
                    name : 'miokardialisinfarktusdetails',
                    historyLabel : _('Megelőző miokardiális infarktus (MI) részletezése'),
                    emptyText : _("Kérem részletezze..."),
                    maxLengthText : _("max 300 karakter"),
                    validateMessage : _("Kérem adja meg a részleteket."),
                    margin : '10 0 5 0',
                    width : 580,
                    height : 48,
                    maxLength : 300,
                    disabled : true,
                    isPrintable : true,
                    msgTarget: 'qtip'
			   	},{
		            xtype: 'monitorcheck',
		            disabled : true
		       },{
					xtype: 'fielddetailsbtn'
	           },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Korábbi CABG"),
	                columns: 3,
	                width: 580,
	                labelWidth : 300,
	                name: 'korabbiCABG',
	                items : [{
	                    name : 'korabbiCABG',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'korabbiCABG',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                },{
	                    name : 'korabbiCABG',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		       },{
					xtype: 'fielddetailsbtn'
	           },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Korábbi Perkután Koronária Intervenció (PCI)"),
	                columns: 3,
	                width: 580,
	                labelWidth : 300,
	                name: 'perkutankoronariainervencio',
	                items : [{
	                    name : 'perkutankoronariainervencio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'perkutankoronariainervencio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                },{
	                    name : 'perkutankoronariainervencio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3
	                }]
			   },{
		            xtype: 'monitorcheck'
		       },{
					xtype: 'fielddetailsbtn'	           
	           },{
                    xtype : 'displayfield',
                    doNotSave : true,
                    value : _("Egyéb társbetegségek:"),
                    colspan : 3
              },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Hipertónia (RR > 140/90 Hgmm legalább 2 alkalommal, vagy kezelést igényel"),
	                columns: 3,
	                width: 580,
	                labelWidth : 300,
	                name: 'hipertonia',
	                items : [{
	                    name : 'hipertonia',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'hipertonia',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                },{
	                    name : 'hipertonia',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		       },{
					xtype: 'fielddetailsbtn'
	           },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Hiperkoleszterinémia (összkoleszterin > 5,2 mmmol/l  , vagy kezelést igényel)"),
	                columns: 3,
	                width: 580,
	                labelWidth : 300,
	                name: 'hiperkoleszterinemia',
	                items : [{
	                    name : 'hiperkoleszterinemia',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'hiperkoleszterinemia',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                },{
	                    name : 'hiperkoleszterinemia',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3
	                }]
			   },{
		            xtype: 'monitorcheck'
		       },{
					xtype: 'fielddetailsbtn'
	           },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Kongesztív szívelégtelenség"),
	                columns: 3,
	                width: 580,
	                labelWidth : 300,
	                name: 'kongesztivszivelegtelenseg',
	                items : [{
	                    name : 'kongesztivszivelegtelenseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'kongesztivszivelegtelenseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                },{
	                    name : 'kongesztivszivelegtelenseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3 
	                }]
			   },{
		            xtype: 'monitorcheck'
		       },{
					xtype: 'fielddetailsbtn'
	           },{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Korábbi veseelégtelenség"),
	                columns: 3,
	                width: 580,
	                labelWidth : 300,
	                name: 'veseelegtelenseg',
	                items : [{
	                    name : 'veseelegtelenseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'veseelegtelenseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                },{
	                    name : 'veseelegtelenseg',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem ismert"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
	           }]
            }]
		},{  /*************************************************************************************************************/
                
            xtype : 'panel',
            title : _("ISZKÉMIÁS STÁTUSZ"),
           	bodyStyle : 'padding:10px;', 
            border : true,
            autoScroll : true,
            xDoScrollUp : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    },  
			    defaults : {
                	margin : '5 0 5 0'
               	}
           	},
           	listeners : {
                 'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{
            	xtype:'fieldset',
       			title: _("Iszkémiás státusz"),   
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
            	 	allowBlank : false,
        	 	 	msgTarget : 'side',
        	 	 	labelWidth : 200
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
                    name : 'iszkemiasDate',
                    xtype : 'datefield',
                    fieldLabel : _("Aktuális dátum") ,
                    width : 310,
                    format : Kron.defaultDateFormat,
                    maxValue: new Date(),
                    checkDate: function () {"use strict";
                    },
                    listeners: {
                        'change': function (field, newValue, oldValue, eOpts) {"use strict";
                            if (this.isValid()) {
                                this.checkDate();
                            }
                        }
                    },
                    msgTarget : 'qtip',
                    isPrintable : true
                },{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
					xtype : 'radiogroup',
	                fieldLabel : _("Iszkémiás státusz "),
	                columns: [100,100,100],
	                name: 'iszkemiasstatus',
	                items : [{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Stabil angina"),
	                    inputValue : 1
	                },{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Instabil angina"),
	                    inputValue : 2, 
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['instabilangina'], this.getValue());
                       	}
	                },{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("STEMI"),
	                    inputValue : 3 
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
	           },{
					xtype : 'radiogroup',
	                fieldLabel : _("Troponin"),
	                width: 560,
	                columns: 2,
	                name: 'instabilangina',
	                disabled : true,
	                items : [{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("Pozitív"),
	                    inputValue : 1
	                },{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("Negatív"),
	                    inputValue : 2 
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
	           }]
            },{
            	xtype:'fieldset',
       			title: _("Szívgyógyszerek"),
                defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	labelWidth : 400
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Szed-e a beteg valamilyen szívgyógyszert jelenleg"),
	                columns: [80,60],
	                allowBlank : false,
	                name: 'szivgyogyszer',
	                items : [{
	                    name : 'szivgyogyszer',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form'), checkBoxGroup;
                            form.setLinkedFieldsState([
                            	'szivgyogyszerdisplayfield',
                            	'aszpirin',
                            	'clopidogrel',
                            	'prasugrel',
                            	'ticlopidine',
                            	'egyebtrombocitaaggregaciogatlo',
                            	'acegatlo',
                            	'sztatin',
                            	'antikoagulans',
                            	'betablokkolo',
                            	'angiotensiniireceptorblokkolo',
                            	'nemsztatinlipidcsokkento'], this.getValue());
                    		checkBoxGroup = form.down('checkboxgroup[name=szivgyogyszercheckboxgroup]');
                            if (this.getValue()) {
                            	checkBoxGroup.allowBlank = false;
                            } else {
                            	checkBoxGroup.allowBlank = true;
                            }
                            checkBoxGroup.validate();

                       	}
	                },{
	                    name : 'szivgyogyszer',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
     			},{
           			xtype : 'displayfield',
           			name: 'szivgyogyszerdisplayfield',
                    value : _("Jelölje mindet, amit a beavatkozás előtt szed:"),
                    padding : '0 0 0 12',
                    disabled : true,
                    colspan: 3
               
               },{
               	    xtype: 'checkboxgroup',
               	    name : 'szivgyogyszercheckboxgroup',
 					colspan: 3,
	           		width : 660,
		            defaults : {
		                margin : '5 0 5 0',
		                maxWidth : 585
		            },
		            layout: {
			            type: 'table',
			            columns: 3
			        },
		            items: [{
						xtype: 'component',
						width: 590,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						width: 32,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						height : 1,
						margin : '0 0 0 0'
					},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Aszpirin"),
	               		name: 'aszpirin',
	               		padding : '0 0 0 12',
	               		disabled : true
	               		
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Clopidogrel"),
	               		name: 'clopidogrel',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Prasugrel"),
	               		name: 'prasugrel',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Ticlopidine"),
	               		name: 'ticlopidine',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Egyéb trombocita aggregációt gátló gyógyszer"),
	               		name: 'egyebtrombocitaaggregaciogatlo',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("ACE gátló"),
	               		name: 'acegatlo',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Sztatin"),
	               		name: 'sztatin',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Antikoaguláns"),
	               		name: 'antikoagulans',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Béta-blokkoló"),
	               		name: 'betablokkolo',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Angiotensin II receptor blokkoló"),
	               		name: 'angiotensiniireceptorblokkolo',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Nem-sztatin lipidcsökkentő szer"),
	               		name: 'nemsztatinlipidcsokkento',
	               		padding : '0 0 0 12',
	               		disabled : true
					},{
			            xtype: 'monitorcheck',
			            disabled : true
			        },{
						xtype: 'fielddetailsbtn'
					}]
				}]
            },{
            	xtype:'fieldset',
       			title: _("Laboratóriumi értékek; kardiális enzimek"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 120
               	},
			    height : 211,
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'fieldcontainer',
	            	width : 580,
	            	labelWidth: 100,
	            	colspan: 3,
					layout: 'hbox',
					items: [{
	           			xtype : 'displayfield',
	                    value:  _("Nem történt"),
	                    padding : '0 0 0 100'
	               },{
	           			xtype : 'displayfield',
	                    value:  _("Pozitív"),
	                    padding : '0 0 0 30'
	               },{
	           			xtype : 'displayfield',
	                    value:  _("Negatív"),
	                    padding : '0 0 0 40'
	               },{
	           			xtype : 'displayfield',
	                    value:  _("Klinikailag jelentős eltérés"),
	                    padding : '0 0 0 90'
	               }]
                },{
    				xtype : 'laborfield',
	                fieldLabel : _("CK"),
	                name: 'labor_ck',
	                width : 560
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
		        },{
     				xtype : 'laborfield',
	                fieldLabel : _("CKMB"),
	                name: 'labor_ckmb',
	                width : 560
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
    				xtype : 'laborfield',
	                fieldLabel : _("Troponin I"),
	                name: 'labor_toponini',
	                width : 560
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
    				xtype : 'laborfield',
	                fieldLabel : _("Troponin T"),
	                name: 'labor_troponint',
	                width : 560
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				
				}]
			}]
            	
        },{
        	/*********************************************************************************************************************************************************/
            xtype : 'panel',
            title : _("BEAVATKOZÁS"),
			bodyStyle : 'padding:10px;', 
            border : true,
            autoScroll : true,
            xDoScrollUp : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    },  
			    defaults : {
                	margin : '5 0 5 0'
               	}
           	},
           	listeners : {
                 'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{
            	xtype:'fieldset',
       			title: _("ANGIOGRÁFIÁS BEAVATKOZÁS"),
                defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
                    name : 'angiografiasbeavatkozasdate',
                    xtype : 'datefield',
                    fieldLabel : _("Beavatkozás dátuma") ,
                    width : 410,
                    format : Kron.defaultDateFormat,
                    maxValue: new Date(),
                    checkDate: function () {"use strict";
                        var form= this.up('form'),
                        	visitDate = form.down('[name=visitDate]');
                        if (visitDate !== undefined) {
                            if (visitDate.getValue() !== '' && visitDate.getValue() !== null && this.getValue() !== '' && this.getValue() !== null) {
                                if (this.getValue() < visitDate.getValue()) {
                                    form.showAngiographDateWarning();
                                }
                            }
                        }
                    },
                    listeners: {
                        'change': function (field, newValue, oldValue, eOpts) {"use strict";
                            if (this.isValid()) {
                                this.checkDate();
                            }
                        }
                    },
                    msgTarget : 'qtip',
                    isPrintable : true
                },{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
     				xtype : 'radiogroup',
	                fieldLabel : _("LVEF (egy éven belüli)"),
	                labelWidth : 140,
	                //width: [210,210],
	                columns: [210,210],
	                name: 'lvef',
	                items : [{
	                    name : 'lvef',
	                    xtype : 'radiofield',
	                    boxLabel : _("Normál (>50%)"),
	                    inputValue : 1
	                },{
	                    name : 'lvef',
	                    xtype : 'radiofield',
	                    boxLabel : _(" közepesen csökkent (31-40%)"),
	                    inputValue : 2 
	                },{
	                    name : 'lvef',
	                    xtype : 'radiofield',
	                    boxLabel : _("mérsékelten csökkent (41-50%)"),
	                    inputValue : 3 
	                },{
	                    name : 'lvef',
	                    xtype : 'radiofield',
	                    boxLabel : _(" súlyosan csökkent (<30%)"),
	                    inputValue : 4 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
     				xtype : 'radiogroup',
	                fieldLabel : _("Károsodott artériák száma (artériák száma ahol a lézió > 50%DS)"),
	                width: 550,
	                columns: 4,
	                name: 'karosodott_arteriak',
	                items : [{
	                    name : 'karosodott_arteriak',
	                    xtype : 'radiofield',
	                    boxLabel : _("1"),
	                    inputValue : 1
	                },{
	                    name : 'karosodott_arteriak',
	                    xtype : 'radiofield',
	                    boxLabel : _("2"),
	                    inputValue : 2 
	                },{
	                    name : 'karosodott_arteriak',
	                    xtype : 'radiofield',
	                    boxLabel : _("3"),
	                    inputValue : 3 
	                },{
	                    name : 'karosodott_arteriak',
	                    xtype : 'radiofield',
	                    boxLabel : _(">3"),
	                    inputValue : 4 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
     				xtype : 'radiogroup',
	                fieldLabel : _("Behatolás helye"),
	                width: 550,
	                columns: 2,
	                name: 'behatolas_helye',
	                items : [{
	                    name : 'behatolas_helye',
	                    xtype : 'radiofield',
	                    boxLabel : _("transzradiális"),
	                    inputValue : 1
	                },{
	                    name : 'behatolas_helye',
	                    xtype : 'radiofield',
	                    boxLabel : _("transzfemorális"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
     				xtype : 'radiogroup',
	                fieldLabel : _("Fő ér"),
	                labelWidth : 140,
	                width: 550,
	                columns: 3,
	                name: 'foer',
	                items : [{
	                    name : 'foer',
	                    xtype : 'radiofield',
	                    boxLabel : _("LM"),
	                    inputValue : 1
	                },{
	                    name : 'foer',
	                    xtype : 'radiofield',
	                    boxLabel : _("LAD"),
	                    inputValue : 2 
	                },{
	                    name : 'foer',
	                    xtype : 'radiofield',
	                    boxLabel : _("Diag"),
	                    inputValue : 3 
	                },{
	                    name : 'foer',
	                    xtype : 'radiofield',
	                    boxLabel : _("CX"),
	                    inputValue : 4 
	                },{
	                    name : 'foer',
	                    xtype : 'radiofield',
	                    boxLabel : _("RCA"),
	                    inputValue : 5 
	                },{
	                    name : 'foer',
	                    xtype : 'radiofield',
	                    boxLabel : _("n.a."),
	                    inputValue : 6 	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
	            },{
	            	xtype: 'combobox',
					width: 372,
			        name: 'beavatkozaselottitimiaramlas',
		       		fieldLabel: _("Beavatkozás előtti TIMI áramlás"),
		       		queryMode:'local',
		       		store:['0','1','2','3','n.a.'],
		       		displayField:'beavatkozaselottitimiaramlas',
		       		forceSelection: true,
		       		msgTarget : 'qtip'
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
	            	xtype: 'combobox',
					width: 372,
			        name: 'beavatkozasutanitimiaramlas',
		       		fieldLabel: _("Beavatkozás utáni TIMI áramlás"),
		       		queryMode:'local',
		       		store:['0','1','2','3','n.a.'],
		       		displayField:'beavatkozasutanitimiaramlas',
		       		forceSelection: true,
		       		msgTarget : 'qtip'
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
               		xtype : 'displayfield',
               		colspan: 3,
		            doNotSave : true,
		            value : _("Lézió Típusa")
		        },{
               		xtype : 'radiogroup',
	                fieldLabel : _("Bifurkáció"),
	                columns: [80,70],
	                name: 'bifurkacio',
	                items : [{
	                    name : 'bifurkacio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'bifurkacio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("Orificium"),
	                columns: [80,70],
	                name: 'orificium',
	                items : [{
	                    name : 'orificium',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'orificium',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("CTO"),
	                columns: [80,70],
	                name: 'cto',
	                items : [{
	                    name : 'cto',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'cto',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("ISR"),
	                columns: [80,70],
	                name: 'isr',
	                items : [{
	                    name : 'isr',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'isr',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("Meszesedés"),
	                columns: [80,70],
	                name: 'meszesedes',
	                items : [{
	                    name : 'meszesedes',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'meszesedes',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("Ér tortuositás "),
	                columns: [80,70],
	                name: 'ertortuositas',
	                items : [{
	                    name : 'ertortuositas',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'ertortuositas',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("Trombus jelenléte"),
	                columns: [80,70],
	                name: 'trombusjelenlet',
	                items : [{
	                    name : 'trombusjelenlet',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'trombusjelenlet',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				}]
			},{
            	xtype:'fieldset',
       			title: _("ESZKÖZÖK"),
                defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					width: 32,
					margin : '0 0 0 0'
                },{
					xtype: 'fieldcontainer',
					colspan: 3,
					layout: 'vbox',    
				    name: 'predilballoncontainer', 
	                width : 655,
	                maxWidth: 655,
	                items:[]

				},{
					xtype: 'fieldcontainer',
					colspan: 3,
	            	width : 580,
					layout: 'hbox',
					items: [{                   
		                    xtype : 'button',
		                    buttonType : 'extrafield',
		                    width: 150,
		                    colspan: 3,
		                    text : _("Predil Ballon hozzáadása"),
		                    handler : function(button) {
								var container = button.up('fieldset').down('[name=predilballoncontainer]'),
									form = button.up('form');
								form.extraFields['predilballon'](container);
							}
		              }] 
               },{
					xtype: 'fieldcontainer',
					colspan: 3,
					layout: 'vbox',    
				    name: 'postdilballoncontainer', 
	                width : 655,
	                maxWidth: 655,
	                items:[]

				},{
					xtype: 'fieldcontainer',
					colspan: 3,
	            	width : 580,
					layout: 'hbox',
					items: [{                   
		                    xtype : 'button',
		                    buttonType : 'extrafield',
		                    width: 150,
		                    colspan: 3,
		                    text : _("Postdil Ballon hozzáadása"),
		                    handler : function(button) {
								var container = button.up('fieldset').down('[name=postdilballoncontainer]'),
									form = button.up('form');
								form.extraFields['postdilballon'](container);
							}
		              }] 
						
				}]
           },{
           		xtype:'fieldset',
       			title: _("PROMUS PREMIER stent a cél lézión:"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					width: 32,
					margin : '0 0 0 0'
				},{
               		xtype: 'fieldcontainer',
	            	width : 550,
					layout: 'hbox',
					items: [{
	           			xtype: 'combobox',
	           			labelWidth : 300,
						width: 400,
				        name: 'promuspremierstenthossz',
			       		fieldLabel:'Hossz',
			       		queryMode:'local',
			       		store:['8', '12', '16', '20', '24', '28', '32', '38','n.a.'],
			       		displayField:'promuspremierstenthossz',
			       		forceSelection: true,
			       		allowBlank : false
	               },{
			            xtype : 'displayfield',
			            width : 50,
			            doNotSave : true,
			            padding : '0 0 0 5',
			            value : 'mm'
			       }]
               	},{
		            xtype: 'monitorcheck',
		            referencedField: 'promuspremierstenthossz'
		        },{
					xtype: 'fielddetailsbtn',
					referencedField: 'promuspremierstenthossz'
               },{
               		xtype: 'fieldcontainer',
	            	width : 550,
	            	labelWidth: 300,
					layout: 'hbox',
					items: [{
	           			xtype: 'combobox',
	           			labelWidth : 300,
						width: 400,
				        name: 'promuspremierstentatmero',
			       		fieldLabel:'Átmérő',
			       		queryMode:'local',
			       		store:['2.25', '2.5', '2.75', '3.0', '3.5', '4.0', '4.5','n.a.'],
			       		displayField:'promuspremierstentatmero',
			       		forceSelection: true,
			       		allowBlank : false
	               },{
			            xtype : 'displayfield',
			            width : 50,
			            doNotSave : true,
			            padding : '0 0 0 5',
			            value : 'mm'
			       }]
               	},{
		            xtype: 'monitorcheck',
		            referencedField: 'promuspremierstentatmero'
		        },{
					xtype: 'fielddetailsbtn',
					referencedField: 'promuspremierstentatmero'
		        },{
               		xtype : 'displayfield',
               		colspan: 3,
		            doNotSave : true,
		            value : _("Stent beültetés leírása")
               },{
               		xtype : 'radiogroup',
	                fieldLabel : _("A lézió elő-tágítása megtörtént"),
	                columns: [80,70],
	                name: 'lezioelotagitas',
	                items : [{
	                    name : 'lezioelotagitas',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'lezioelotagitas',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
					xtype : 'vitalsignfield',
                    name : 'maxinflaciosnyomas',
                    fieldLabel : _("Maximális inflációs nyomás"),
                    labelWidth : 300,
                    width: 520,
                    valueMin : 0,
                    valueMax : 99,
                    unit : _("atm"),
                    isPrintable : true
           		},{
		            xtype: 'monitorcheck',
		            referencedField: 'maxinflaciosnyomas'
		        },{
					xtype: 'fielddetailsbtn',
					referencedField: 'maxinflaciosnyomas'
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("A stent teljesen kifeszült vizuálisan"),

	                columns: [80,70],
	                name: 'stentkifeszult',
	                items : [{
	                    name : 'stentkifeszult',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'stentkifeszult',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
					xtype: 'fieldcontainer',
					colspan: 3,
					layout: 'vbox',    
				    name: 'ppstentcontainer', 
	                width : 655,
	                maxWidth: 655,
	                items:[]
				},{
               		xtype : 'displayfield',
               		colspan : 3,
		            doNotSave : true,
		            width : 580,
		            value : _("Ha több PROMUS PREMIER stent beültetése történt, kérjük, adja meg a stentek hosszát és átmérőjét a lenti gombra kattintással!")
               	},{
					xtype: 'fieldcontainer',
					colspan: 3,
	            	width : 580,
					layout: 'hbox',
					items: [{                   
		                    xtype : 'button',
		                    buttonType : 'extrafield',
		                    width: 250,
		                    colspan: 3,
		                    text : _("További Promus Premier stent hozzáadása"),
		                    handler : function(button) {
								var container = button.up('fieldset').down('[name=ppstentcontainer]'),
									form = button.up('form');
								form.extraFields['ppstent'](container);
							}
		              }] 
						
				}]	
			},{
				/////////////////////////////////////////////////////////////////////////////////////////////////////
				xtype:'fieldset',
       			title: _("További stentek:"),
				defaults : {
                	margin : '5 0 5 0',
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'fieldcontainer',
					colspan: 3,
					layout: 'vbox',    
				    name: 'nemppstentcontainer', 
					
	                width : 655,
	                items:[]
				},{
               		xtype : 'displayfield',
               		colspan : 3,
		            doNotSave : true,
		            width: 580,
		            value : _("Amennyiben további nem PROMUS PREMIER stent beültetése történt, kérjük jelezze")
               	
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("BMS"),
	                columns: [80,70],
	                name: 'bms',
	                items : [{
	                    name : 'bms',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'bms',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("DES"),
	                columns: [80,70],
	                name: 'des',
	                items : [{
	                    name : 'des',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'des',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				}]
			},{
				xtype:'fieldset',
       			title: _("UTÓTÁGÍTÁS"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("Kissing tágítás történt"),
	                columns: [80,70],
	                name: 'kissingtagitas',
	                items : [{
	                    name : 'kissingtagitas',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'kissingtagitas',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
               		xtype : 'radiogroup',
	                fieldLabel : _("Nem fedett disszekció?"),
	                columns: [80,70],
	                name: 'dissectio',
	                items : [{
	                    name : 'dissectio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1
	                },{
	                    name : 'dissectio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2 
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'				
               }]		
			},{
				xtype:'fieldset',
       			title: _("A BEÜLTETÉS KOMPLIKÁCIÓI"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 400
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype : 'radiogroup',
	                fieldLabel : _("Volt-e valamilyen komplikáció a beavatkozás során"),
	                columns: [80,70],
	                name: 'komplikacio',
	                items : [{
	                    name : 'komplikacio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['komplikaciodisplayfield',
                            	'embolizalt',
                            	'levalt',
                            	'kompresszio',
                            	'ballonmegrepedt', 
                            	'ballonnemfujodottfel',
                            	'ballonnemeresztettle',
                            	'stentnyeltorese',
                            	'stentetnemlehettlevinniacelleziohoz',
                            	'egyebkomplikacio'], this.getValue());
                            
                       	}
	                },{
	                    name : 'komplikacio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
           			xtype : 'displayfield',
           			colspan : 3,
                 	name: 'komplikaciodisplayfield',
                    disabled : true,
                    value : _("Jelölje meg a komplikációt:"),
                    padding : '0 0 0 12'
                },{
                	xtype: 'checkboxfield',
               		boxLabel:  _("A stent a betegben embolizált"),
               		name: 'embolizalt',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
                },{
                	xtype: 'checkboxfield',
               		boxLabel:  _("A stent levált a ballonról a bevezetés előtt"),
               		name: 'levalt',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
                },{
                	xtype: 'checkboxfield',
               		boxLabel:  _("Stent kompresszió"),
               		name: 'kompresszio',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
                	xtype: 'checkboxfield',
               		boxLabel:  _("Stentet szállító ballon megrepedt"),
               		name: 'ballonmegrepedt',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
                	xtype: 'checkboxfield',
               		boxLabel:  _("Stent ballon nem fújódott fel"),
               		name: 'ballonnemfujodottfel',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
                	xtype: 'checkboxfield',
               		boxLabel:  _("Stent ballon nem eresztett le"),
               		name: 'ballonnemeresztettle',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
                	xtype: 'checkboxfield',
               		boxLabel:  _("Stent nyél törése"),
               		name: 'stentnyeltorese',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
                	xtype: 'checkboxfield',
               		boxLabel:  _("Stentet nem lehetett levinni a cél lézióhoz"),
               		name: 'stentetnemlehettlevinniacelleziohoz',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true   
		        },{
					xtype: 'fielddetailsbtn'
               },{
                	xtype: 'checkboxfield',
               		boxLabel:  _("Egyéb, részletezze"),
               		name: 'egyebkomplikacio',
               		padding : '0 0 0 12',
               		disabled : true,
               		handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['egyebkomplikacioreszletek'], this.getValue());
                   	}
		        },{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
	            	xtype : 'textarea',
                    name : 'egyebkomplikacioreszletek',
                    historyLabel : _('Egyéb komplikáció részletezése'),
                    emptyText : _("Kérem részletezze..."),
                    maxLengthText : _("max 300 karakter"),
                    validateMessage : _("Kérem adja meg a részleteket."),
                    margin : '10 0 5 14',
                    width : 580,
                    height : 48,
                    maxLength : 300,
                    disabled : true,
                    isPrintable : true,
                    msgTarget: 'qtip'
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
				}]

			}]
		
       },{/*************************************************************************************************************/
            xtype : 'panel',
            title : _("KÓRHÁZI ELBOCSÁJTÁS"), 
			bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            xDoScrollUp : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    },  
			    defaults : {
                	margin : '5 0 5 0'
               	}
           	},
           	listeners : {
                'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{
	          	xtype:'fieldset',
       			title: _("Gyógyszerek"),
 				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
	           		xtype : 'radiogroup',
	                fieldLabel : _("Szed-e a beteg valamilyen gyógyszert jelenleg"),
	                columns: [80,60],
	                name: 'gyogyszer_elbocsataskor',
	                allowBlank : false,
	                items : [{
	                    name : 'gyogyszer_elbocsataskor',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form'), checkBoxGroup;
                            form.setLinkedFieldsState([
                            	'gyogyszerdisplayfield_elbocsataskor',
                            	'aszpirin_elbocsataskor',
                            	'clopidogrel_elbocsataskor',
                            	'prasugrel_elbocsataskor',
                            	'ticlopidine_elbocsataskor',
                            	'egyebantiplatelet_elbocsataskor',
                            	'acegatlo_elbocsataskor',
                            	'sztatin_elbocsataskor',
                            	'antikoagulans_elbocsataskor',
                            	'betablokkolo_elbocsataskor',
                            	'angiotensiniireceptorblokkolo_elbocsataskor',
                            	'nemsztatinlipidcsokkento_elbocsataskor',
                            	'antidiabetikum_elbocsataskor'], this.getValue());
                        	checkBoxGroup = form.down('checkboxgroup[name=gyogyszercheckboxgroup_elbocsataskor]');
                            if (this.getValue()) {
                            	checkBoxGroup.allowBlank = false;
                            } else {
                            	checkBoxGroup.allowBlank = true;
                            }
                            checkBoxGroup.validate();
                       	}
	                },{
	                    name : 'gyogyszer_elbocsataskor',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2, 
	                    allowBlank : false
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				},{
					
           			xtype : 'displayfield',
           			name : 'gyogyszerdisplayfield_elbocsataskor',
                    value : _("Jelölje mindet, amit a beteg a kórházi elbocsájtás napján szed"),
                    disabled : true,
                    colspan: 3
               },{
               		xtype: 'checkboxgroup',
               	    name : 'gyogyszercheckboxgroup_elbocsataskor',
 					colspan: 3,
	           		width : 660,
		            defaults : {
		                margin : '5 0 5 0',
		                maxWidth : 585
		            },
		            layout: {
			            type: 'table',
			            columns: 3
			        },
		            items: [{
						xtype: 'component',
						width: 590,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						width: 32,
						height : 1,
						margin : '0 0 0 0'
					},{
						xtype: 'component',
						height : 1,
						margin : '0 0 0 0'
					},{	
	               		xtype: 'checkboxfield',
	               		width: 570,
	               		boxLabel:  _("Aszpirin"),
	               		disabled : true,
	               		name: 'aszpirin_elbocsataskor',
	               		padding : '0 0 0 12'
	               		
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Clopidogrel"),
	               		name: 'clopidogrel_elbocsataskor',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Prasugrel"),
	               		name: 'prasugrel_elbocsataskor',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Ticlopidine"),
	               		name: 'ticlopidine_elbocsataskor',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Egyéb antiplatelet gyógyszer"),
	               		name: 'egyebantiplatelet_elbocsataskor',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("ACE gátló"),
	               		disabled : true,
	               		name: 'acegatlo_elbocsataskor',
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Sztatin"),
	               		name: 'sztatin_elbocsataskor',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Antikoaguláns"),
	               		name: 'antikoagulans_elbocsataskor',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Béta-blokkoló"),
	               		name: 'betablokkolo_elbocsataskor',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Angiotensin II receptor blokkoló"),
	               		name: 'angiotensiniireceptorblokkolo_elbocsataskor',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Nem-sztatin lipidcsökkentő szer"),
	               		name: 'nemsztatinlipidcsokkento_elbocsataskor',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
	     			},{
	               		xtype: 'checkboxfield',
	               		boxLabel:  _("Antidiabetikum"),
	               		name: 'antidiabetikum_elbocsataskor',
	               		disabled : true,
	               		padding : '0 0 0 12'
					},{
			            xtype: 'monitorcheck',
	               		disabled : true
			        },{
						xtype: 'fielddetailsbtn'
					}]
				}]

	       }] 
        },{/*************************************************************************************************************/
            xtype : 'panel',
            title : _("KOMPLIKÁCIÓK"),// A KÓRHÁZI TARTÓZKODÁS ALATT"),
       		bodyStyle : 'padding:10px;', 
            border : true,
            autoScroll : true,
            xDoScrollUp : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    },  
			    defaults : {
                	margin : '5 0 5 0'
               	}
           	},
           	listeners : {
                 'show': function (panel, eOpts) {"use strict";
                 	if (panel.body && panel.xDoScrollUp) {
		            	panel.body.scrollTo("top", 0, false);
		            	panel.xDoScrollUp = false
		           	}
                }
            },
            items : [{
            	xtype:'fieldset',
       			title: _("Komplikáció a beavatkozás alatt"),
                defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
     				xtype : 'radiogroup',
	                fieldLabel : _("Volt-e valamilyen komplikáció a beavatkozás alatt"),
	                labelWidth : 400,
	                width: 560,
	                columns: [80,80],
	                name: 'komplikacio_beavatkozasalatt',
	                items : [{
	                    name : 'komplikacio_beavatkozasalatt',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['komplikacioheader_beavatkozaslatt',
                            	'halal_beavatkozasalatt',
								'stroke_beavatkozasalatt',
								'myocardialisinfarktus_beavatkozasalatt',
								'ismeteltrevaszkularizacio_beavatkozasalatt',
								'verzes_beavatkozasalatt',
								'definitivstenttrombozis_beavatkozasalatt',
								'egyebkomplikacio_beavatkozasalatt'
                            	], this.getValue());
                            if ( this.getValue() ) {
                            	form.showAEWarning();
                             }
                       	}
	                },{
	                    name : 'komplikacio_beavatkozasalatt',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
		        },{
           			xtype : 'displayfield',
                    name: 'komplikacioheader_beavatkozaslatt',
                    colspan: 3,
                    value : _("Jelölje a komplikáció(ka)t:"),
                    padding : '0 0 0 12',
                    disabled:true
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Halál"),
               		name: 'halal_beavatkozasalatt',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Stroke"),
               		name: 'stroke_beavatkozasalatt',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
               		disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Miokardiális infarktus"),
               		name: 'myocardialisinfarktus_beavatkozasalatt',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
               		disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Ismételt revaszkularizáció"),
               		name: 'ismeteltrevaszkularizacio_beavatkozasalatt',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
               		disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Vérzés"),
               		name: 'verzes_beavatkozasalatt',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
               		disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Definitív stent trombózis (angiográfiával vagy patológiailag igazolt)"),
               		name: 'definitivstenttrombozis_beavatkozasalatt',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
               		disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Egyéb, részletezze"),
               		name: 'egyebkomplikacio_beavatkozasalatt',
               		padding : '0 0 0 12',
               		disabled : true,
               		handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['egyebkomplikacioreszletek_beavatkozasalatt'], this.getValue());
                       	}
				},{
		            xtype: 'monitorcheck',
               		disabled : true
		        },{
					xtype: 'fielddetailsbtn'
               },{
	            	xtype : 'textarea',
                    name : 'egyebkomplikacioreszletek_beavatkozasalatt',
                    historyLabel : _('Egyéb beavatkozás alatti komplikáció részletezése'),
                    emptyText : _("Kérem részletezze..."),
                    maxLengthText : _("max 300 karakter"),
                    validateMessage : _("Kérem adja meg a részleteket."),
                    margin : '10 0 5 14',
                    width : 580,
                    height : 48,
                    maxLength : 300,
                    disabled : true,
                    isPrintable : true,
                    msgTarget: 'qtip'
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
         		}]
         	},{
            	xtype:'fieldset',
       			title: _("Komplikáció a beavatkozást követően"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
     				xtype : 'radiogroup',
	                fieldLabel : _("Volt-e valamilyen komplikáció a beavatkozást követően"),
	                labelWidth : 400,
	                width: 560,
	                columns: [80,80],
	                name: 'komplikacio_beavatkozatkovetoen',
	                items : [{
	                    name : 'komplikacio_beavatkozatkovetoen',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['komplikacioheader_beavatkozatkovetoen',
                            	'halal_beavatkozatkovetoen',
								'stroke_beavatkozatkovetoen',
								'myocardialisinfarktus_beavatkozatkovetoen',
								'ismeteltrevaszkularizacio_beavatkozatkovetoen',
								'verzes_beavatkozatkovetoen',
								'definitivstenttrombozis_beavatkozatkovetoen',
								'egyebkomplikacio_beavatkozatkovetoen'
                            	], this.getValue());
                            if ( this.getValue() ) {
                            	form.showAEWarning();
                             }
                       	}
	                },{
	                    name : 'komplikacio_beavatkozatkovetoen',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
		        },{
           			xtype : 'displayfield',
                    name : 'komplikacioheader_beavatkozatkovetoen',
                    colspan: 3,
                    value : _("Jelölje a komplikáció(ka)t:"),
                    padding : '0 0 0 12',
                    disabled:true
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Halál"),
               		name: 'halal_beavatkozatkovetoen',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Stroke"),
               		name: 'stroke_beavatkozatkovetoen',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Miokardiális infarktus"),
               		name: 'myocardialisinfarktus_beavatkozatkovetoen',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Ismételt revaszkularizáció"),
               		name: 'ismeteltrevaszkularizacio_beavatkozatkovetoen',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Vérzés"),
               		name: 'verzes_beavatkozatkovetoen',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Definitív stent trombózis (angiográfiával vagy patológiailag igazolt)"),
               		name: 'definitivstenttrombozis_beavatkozatkovetoen',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Egyéb, részletezze"),
               		name: 'egyebkomplikacio_beavatkozatkovetoen',
               		padding : '0 0 0 12',
               		disabled : true,
               		handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['egyebkomplikacioreszletek_beavatkozatkovetoen'], this.getValue());
                       	}
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
               },{
	            	xtype : 'textarea',
                    name : 'egyebkomplikacioreszletek_beavatkozatkovetoen',
                    historyLabel : _('Egyéb beavatkozást követő komplikáció részletezése'),
                    emptyText : _("Kérem részletezze..."),
                    maxLengthText : _("max 300 karakter"),
                    validateMessage : _("Kérem adja meg a részleteket."),
                    margin : '10 0 5 14',
                    width : 580,
                    height : 48,
                    maxLength : 300,
                    disabled : true,
                    isPrintable : true,
                    msgTarget: 'qtip'
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn'
         		}]
			}]
       }]
    }]
});
