/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MonitorCheck.js
 *
 * Control for check/unchek the field's monitored status
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.MonitorCheck', {
    extend : 'Ext.button.Button',
    alias : 'widget.monitorcheck',
    cls : 'monitorcheck-unpressed',
    margin: '0 0 0 5',
    // cls : 'monitorcheck-unpressed-base',
   // overCls : 'monitorcheck-unpressed',//
    pressedCls : 'monitorcheck-pressed',
    disabledCls : 'monitorcheck-disabled',
    width : 22,
    height : 22,
    enableToggle : true,
    setMonitored : function(checked) {"use strict";

        	//kell ez?
   /*     if (this.field !== undefined) {
            this.field.isMonitored = checked;
        }*/

		
        
    },
    
   
    
    toggleHandler : function(btn, pressed) {"use strict";
    //	console.log("you pressed me", "previous item", this.previousSibling());
        this.setMonitored(pressed);
       // console.log('monitorcheck - updatetabot hívom');
        this.up('form').updateTabIcon(this.up('panel'));
    }
}); 

