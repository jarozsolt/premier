﻿/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * GeneralPatientForm.js
 *
 * Parent form class for all Patient's forms
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

// A RadioGroup-oknál a validate függvényt felül kell definiálni, mert
// a valid státusz változásánál az egész formra hívta az updateLayout-ot
// nem csak magára, ez a scrollbar felugrását okozta.
Ext.form.CheckboxGroup.prototype.validate = function(){
        var me = this,
            errors,
            isValid,
            wasValid;

        if (me.disabled) {
            isValid = true;
        } else {
            errors = me.getErrors();
            isValid = Ext.isEmpty(errors);
            wasValid = me.wasValid;
            if (isValid) {
                me.unsetActiveError();
            } else {
                me.setActiveError(errors);
            }
        }
        if (isValid !== wasValid) {
            me.wasValid = isValid;
            me.fireEvent('validitychange', me, isValid);
            // Eredeti: me.updateLayout();
            // Ezt felül kellett írni, hogy csak magára hívódjon az updateLayout
            me.updateLayout({isRoot:true});
        }
        return isValid;
};


/* A form.field.Base-nál a setError függvényt felül kell definiálni az ugrálás miatt, mert
* @TODO - Még tesztelendő ez is, van-e nem kívánt mellékhatása...
*//*
Ext.form.field.Base.prototype.setError = function(active){
        var me = this,
            msgTarget = me.msgTarget,
            prop;
            
        if (me.rendered) {
            if (msgTarget == 'title' || msgTarget == 'qtip') {
                if (me.rendered) {
                    prop = msgTarget == 'qtip' ? 'data-errorqtip' : 'title';
                }
                me.getActionEl().dom.setAttribute(prop, active || '');
            } else {
             // Eredeti:    me.updateLayout();
            	me.updateLayout({isRoot:true});
            }
        }
    };

*/

// Ez azért kell, hogy a datefield-eknél a naptárból való választásra is lefusson a 'change' event
/*Ext.form.DateField.prototype.menuListeners.select = function(menu, value) {
    if( String(value) != String(this.getValue()) ) {
        this.setValue(value);
        this.fireEvent('change', this, value, this.startValue );
        this.startValue = value;
    }
}*/
 
Ext.define('Kron.view.patient.forms.GeneralPatientForm', {
	extend : 'Ext.form.Panel',
	alias : 'widget.generalPatientForm',
    requires : ['Kron.view.patient.forms.fields.MonitorCheck',
        'Kron.view.FormWarning',
        'Kron.view.patient.forms.fields.NotExactDate',
        'Kron.view.patient.forms.fields.FieldDetailsBtn',
        'Kron.view.patient.forms.fields.ButtonContainer',
        'Kron.view.patient.forms.fields.OngoingDateField',
        'Kron.view.patient.forms.fields.DoseField',
        'Kron.view.patient.forms.fields.VASField',
        'Kron.view.patient.forms.fields.VitalSignField',
        'Kron.view.patient.forms.fields.ParacetamolField',
        'Kron.view.patient.forms.fields.AdherenceTableField',
        'Kron.view.patient.forms.fields.AEQuestionRow',
        'Kron.view.patient.forms.fields.MediQuestionRow',
        'Kron.view.patient.forms.fields.PhysicalExamRow',
        'Kron.view.patient.forms.fields.LaborField',
        'Kron.view.patient.forms.fields.BallonField',
        'Kron.view.patient.forms.fields.DateTimeField',
        'Kron.view.patient.forms.fields.naNumberField'
     /*  , 'Kron.view.patient.forms.fields.xDateTimeField'*/],
        
	//bodyStyle : 'padding: 10px',
	frame : true,
	monitorValid : true,
	buttonAlign : 'center',
	defaults : {
		msgTarget : 'side',
		width : 400,
		labelWidth : 120
		//isQueryable : true,
	//	overCls : 'overfield'
	},
	// ez azért kell, hogy jelezni tudjam, ha a form bezáráskor kell rákérdezés a mentésre, de a gyári isDirty nem jelez (pl.extra fieldek törlése után)
	isFormDirty : false,
	
	constructor : function(config) {// Do not use "use strict" on functions containing callParent!
        config = config || {};
        config.trackResetOnLoad = true;
        this.callParent([config]);
    },
    
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
    strictInitComponent : function() { "use strict";
	//	var container, childitems, hasPanel= false, me = this;
		
    },
    // ezt felüldefiniálva lehet a form betöltés előtti dolgokkal
    initForm : function(fieldValues) { "use strict";
       return true;
	},
	// ezt felüldefiniálva lehet a form betöltés utáni dolgokkal
	onAfterFormLoad : function(fieldValues) { "use strict";
		return true;
	},
	// ezt felüldefiniálva lehet a mentés előtt figyelmeztetéseket kiírni
	alertBeforeSave	: function(fieldValues) { "use strict";
       return true;
	},
   
    
    showAngiographDateWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(1, _("A Beavatkozás dátuma nem lehet korábbi, mint a vizit dátuma. Kérjük, ellenőrizze!"));
        }
    },
    
    showAEWarning : function() {"use strict";
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki az 'Nemkívánatos események' oldalt!"), _("Figyelmeztetés"));
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(1, _("Kérjük, töltse ki az 'Nemkívánatos események' oldalt!"));
        }
    },
    
    showSAEWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(2, _("Kérjük, töltse ki a 'Nemkívánatos Esemény / Súlyos Nemkívánatos Esemény' oldalt!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki a 'Nemkívánatos Esemény / Súlyos Nemkívánatos Esemény' oldalt!"), _("Figyelmeztetés")); 
    },
    
    showMedicationWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(3, _("Kérjük, töltse ki az 'Egyéb kezelések' oldalt!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki az 'Egyéb kezelések' oldalt!"), _("Figyelmeztetés")); 
    },
    
    showMedicalHistoryWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(4, _("Kérjük jelölje a „Kórelőzmény, egyéb betegségek” oldalon!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük jelölje a „Kórelőzmény, egyéb betegségek” oldalon!"), _("Figyelmeztetés")); 
    },
    
    showBloodPressureWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(5, _("A betegnek esetleges kontrollálatlan magas vérnyomása van, kérjük, ellenőrizze!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("A betegnek esetleges kontrollálatlan magas vérnyomása van, kérjük, ellenőrizze!"), _("Figyelmeztetés")); 
    },
    
    showPatchWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(6, _("A tapaszok száma nem egyezik meg a kiadott tapaszok számával! Kérjük adja meg az okát!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("A tapaszok száma nem egyezik meg a kiadott tapaszok számával! Kérjük adja meg az okát!"), _("Figyelmeztetés")); 
    },
    
    showTabletsWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(7, _("A tabletták száma nem egyezik meg a kiadott tabletták számával. Kérjük kérdezzen rá a betegtől a különbség okára és jegyezze fel azt a beteg dokumentációjában!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("A tabletták száma nem egyezik meg a kiadott tabletták számával. Kérjük kérdezzen rá a betegtől a különbség okára és jegyezze fel azt a beteg dokumentációjában!"), _("Figyelmeztetés")); 
    },
    
    showVisit2KneeWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(8, _("A 2. viziten nem ezt a térdet jelölte meg, kérjük javítsa!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("A 2. viziten nem ezt a térdet jelölte meg, kérjük javítsa!"), _("Figyelmeztetés")); 
    },
    
    showPrevVisitKneeWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(9, _("Az előző viziten nem ezt a térdet jelölte meg, kérjük javítsa!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Az előző viziten nem ezt a térdet jelölte meg, kérjük javítsa!"), _("Figyelmeztetés")); 
    },
    
    showPrevVisitKneeQuestion : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(10, _("Az előző viziten nem ezt a térdet jelölte meg! Biztos, hogy folytatja?"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Az előző viziten nem ezt a térdet jelölte meg! Biztos, hogy folytatja?"), _("Figyelmeztetés")); 
    },
    
    showPregnacyWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(11, _("Terhes nők nem vehetnek részt a vizsgálatban, ellenőrizze a kizárási feltétel 8. pontját!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Terhes nők nem vehetnek részt a vizsgálatban, ellenőrizze a kizárási feltétel 8. pontját!"), _("Figyelmeztetés")); 
    },
    
    showAgeWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(12, _("A beteg nincs 45 éves, nem vonható be a vizsgálatba!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("A beteg nincs 45 éves, nem vonható be a vizsgálatba!"), _("Figyelmeztetés")); 
    },
    
    showVisitDateWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(13, _("A vizit dátuma korábbi, mint a betegtájékoztatás dátuma. Biztos, hogy folytatja?"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("A vizit dátuma korábbi, mint a betegtájékoztatás dátuma. Biztos, hogy folytatja?"), _("Figyelmeztetés")); 
    },
    
    
    
    showNotEligibleWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(14, _("A beteg nem vonható be a vizsgálatba!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, , _("Figyelmeztetés")); 
    },
    
    showVASWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(15, _("A beteg nem választható be, a VAS érték legalább 55 mm kell, hogy legyen!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("A beteg nem választható be, a VAS érték legalább 55 mm kell, hogy legyen!"), _("Figyelmeztetés")); 
    },
    
    showPrevVisitTreatmentQuestion : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(16, _("Az első periódusban ugyanezt a kezelést jelölte meg! Biztos, hogy folytatja?"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Az első periódusban ugyanezt a kezelést jelölte meg! Biztos, hogy folytatja?"), _("Figyelmeztetés")); 
    },
    
    showBPExclusionWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(17, _("A betegnek esetleges kontrollálatlan magas vérnyomása van, kérjük, ellenőrizze a kizárási kritérium 4-es pontját!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("A betegnek esetleges kontrollálatlan magas vérnyomása van, kérjük, ellenőrizze a kizárási kritérium 4-es pontját!"), _("Figyelmeztetés")); 
    },
    
    showFillVisit5Warning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(18, _("Kérjük, töltse ki a Vizit 5 kérdéseit is!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki a Vizit 5 kérdéseit is!"), _("Figyelmeztetés")); 
    },
    
    showFillLastVisitWarning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(19, _("Kérjük, töltse ki a beteg legutolsó megjelenésekor esedékes vizit kérdéseit is!"));
        }
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki a beteg legutolsó megjelenésekor esedékes vizit kérdéseit is!"), _("Figyelmeztetés")); 
    },
    
    showFillVisit1Warning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(20, _("Kérjük, töltse ki a Vizit 1 és a vizit 2 kérdéseit is!"));
        }
    },
    
    showFillVisit3Warning : function() {"use strict";
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(20, _("Kérjük, töltse ki a Vizit 3 kérdéseit is!"));
        }
    },
    
    showLaborDateWarning : function() {"use strict";
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki az 'Nemkívánatos események' oldalt!"), _("Figyelmeztetés"));
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(21, _("A vérvétel dátuma nem lehet korábbi, mint a vizit dátuma. Kérem, ellenőrizze!"));
        }
    },
    showMedicationsChangedWarning1 : function() {"use strict";
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki az 'Nemkívánatos események' oldalt!"), _("Figyelmeztetés"));
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(22, _("Eltérés van a kórházba érkezés alkalmával jelölt szívgyógyszerekhez képest. Kérem, ellenőrizze, ha szükséges, töltse ki az egyéb kezelések oldalt!"));
        }
    },
    showMedicationsChangedWarning2 : function() {"use strict";
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki az 'Nemkívánatos események' oldalt!"), _("Figyelmeztetés"));
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(23, _("Eltérés van a vizit 1 során a kórházi elbocsájtáskor alkalmazott a gyógyszerekhez képest. Kérem, ellenőrizze, ha szükséges, töltse ki az egyéb kezelések oldalt!"));
        }
    },
    
    showMedicationsChangedWarning3 : function() {"use strict";
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki az 'Nemkívánatos események' oldalt!"), _("Figyelmeztetés"));
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(24, _("Eltérés van a vizit 2 során alkalmazott a gyógyszerekhez képest. Kérem, ellenőrizze, ha szükséges, töltse ki az egyéb kezelések oldalt!"));
        }
    }, 
    showStartEndDateConflictWarning : function() {"use strict";
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki az 'Nemkívánatos események' oldalt!"), _("Figyelmeztetés"));
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(25, _("A vége dátum nem lehet korábbi a kezdete dátumnál!"));
        }
    }, 
    showFutureStartDateWarning : function() {"use strict";
        //Ext.Tips.popup(Ext.Tips.WARNING, _("Kérjük, töltse ki az 'Nemkívánatos események' oldalt!"), _("Figyelmeztetés"));
        var formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.showWarning(25, _("A kezdete dátum nem lehet jövőbeni dátum!"));
        }
    }, 
    /*warnCt : null,
    showWarning : function(title, format) {"use strict";
        if (this.warnCt === null) {
            this.warnCt = Ext.DomHelper.insertFirst(document.body, {
                id : 'warning-div'
            }, true);
        }
        var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
        var m = Ext.DomHelper.append(this.warnCt, '<div class="warning"><h3>' + title + '</h3><p>' + s + '</p></div>', true);
        m.hide();
        m.slideIn('b', {
            duration : 1000
        }).ghost("b", {
            delay : 2000,
            remove : true
        });
    },*/

	

    clearFields : function(fields) { "use strict";  
        Ext.Array.each(fields, function(field) {
            if (field !== undefined && field !== null && field.xtype != 'displayfield')
            {
            	// a doNotSave -es mezőket nem törlöm
                if (field.xtype === 'radiogroup' ) {
                	field.items.each(function(item, index, len){
                		if (item.xtype === 'radiofield') {
                			item.setValue(false);
                		}
                	});
                	Ext.form.Field.prototype.clearInvalid.call(field);
                	
                } else if (field.doNotSave !== true) {   
                	           	
                	if (field.setValue !== undefined) {
	                    field.setValue(null);
	                }
	                
	                Ext.form.Field.prototype.clearInvalid.call(field);
                }
                /* ez nem is kell sztem
                // a doNotSave-es mezőket is resetelem
                if (field.reset !== undefined) {
                    field.reset();
                }*/
 
            }
        });
    },
    
    setFieldsMandatory : function(fields) { "use strict";
        this.setFieldsMandatoryState(true, fields);
    },
    
    setFieldsOptional: function(fields) { "use strict";
        this.setFieldsMandatoryState(false, fields);
    },
    
    setFieldsMandatoryState : function(mandatory, fields) { "use strict";
        Ext.Array.each(fields, function(field) {
            if (field !== undefined && field !== null)
            { 
                field.allowBlank = !mandatory;
            }
        });
    },
    
    setFieldsVisible : function(fieldNames) { "use strict";
        this.setFieldsVisibleState(true, fieldNames);
    },
    
    setFieldsHidden: function(fieldNames) { "use strict";
        this.setFieldsVisibleState(false, fieldNames);
    },
    
    setFieldsVisibleState: function(show, fieldNames) { "use strict";
        var setfieldNames = [], field, fieldName, me = this;
        if (fieldNames !== undefined && fieldNames !== null)
        {
            setfieldNames = [].concat(fieldNames);
        }
        Ext.suspendLayouts();
        Ext.Array.each(setfieldNames, function(fieldName) {
            field =me.down('[name=' + fieldName + ']');// Ext.getCmp(fieldName);
            if (field !== undefined && field !== null)
            {
                field.setVisible(show);
            }
        });
        Ext.resumeLayouts(true);
    },
    
    setFieldsEnabled : function( fields, onlyRadioFields) { "use strict"; 
    	this.setFieldsEnabledState(true, fields, onlyRadioFields);
    },
    setFieldsDisabled : function( fields, onlyRadioFields) { "use strict"; 
    	this.setFieldsEnabledState(false, fields, onlyRadioFields);
    },
    setFieldsEnabledState : function(doEnable, fields, onlyRadioFields) { "use strict"; 
        var me = this, fieldName, nextField, 
        	monitorChkBtn, fieldDetailsBtn;
		//console.log('setFieldsEnabledState called', doEnable, fields);
        Ext.suspendLayouts();
        Ext.Array.each(fields, function(field) {
            if (field !== undefined && field !== null)
            { 
                if(field.xtype === 'radiogroup' && onlyRadioFields) {
                	field.cascade(function(item){
	            		if ( item.xtype === 'radiofield' && item.name == field.name) {
	            			item.setDisabled(!doEnable);
	           			}
	            	});
                } else {
                	field.setDisabled(!doEnable);
                }
                
                
                
                
                // ha saját custom field van, akkor aa szülő fieldcontainert is meg kell nézni ,és enabled-ezni, ha kell
                if ( field.up().isXFormField ) {
                	 field.up().setDisabled(!doEnable);
                }
                fieldName = field.name;
                if (fieldName !== undefined && fieldName !== null)  { // ha van neve, akkor megkeresem a fielddetailsbtn-t is...
                	fieldDetailsBtn = null;
	                nextField = field.nextSibling('fielddetailsbtn'); // először megnézem a szomszédjait (sima fieldnél)
	               	if (nextField  !== undefined && nextField !== null && nextField.getFieldName() == fieldName ) {
               			fieldDetailsBtn=nextField;
	               	}
               		if (fieldDetailsBtn === null) {
               			nextField = field.up().nextSibling('fielddetailsbtn'); // majd a szülő szomszédjait (saját custom fieldnél)
		               	if (nextField  !== undefined && nextField !== null && nextField.getFieldName() == fieldName ) {
	               			fieldDetailsBtn=nextField;
		               	}
               		}
	               	if (fieldDetailsBtn === null) { // végső esetben jön a panel összes fielddetailsbtn-ja
	               		field.up('panel').cascade(function(item){
	               			if (item.xtype === 'fielddetailsbtn' && item.getFieldName() == fieldName){
	               				fieldDetailsBtn = item;
	               				return false;
	               			}
	               		});
	               	}
	               	if (fieldDetailsBtn !== null) {

               		//jzs:	fieldDetailsBtn.setDisabled(!doEnable);
						
	               		monitorChkBtn = fieldDetailsBtn.getMonitorCheckBtn();
	               		if (Kron.core.UserManagement.hasPermission("Forms", "monitorForm") || !doEnable ){
			           		
			           		if (monitorChkBtn ) {
			           			monitorChkBtn.setDisabled(!doEnable);
			           		//	console.log('setFieldsEnabledState updatetabicon hívás');
			           			me.updateTabIcon(monitorChkBtn.up('panel'));
			           		}
	               		} 
	               		
	               	}       		
                }   	
            }
        });
        Ext.resumeLayouts(true);
    },
    
   /* setFieldsDisabled : function(fieldNames) { "use strict";
        var setfieldNames = [], field, fieldName, nextField, me = this,
        	monitorChkBtn, fieldDetailsBtn;
        if (fieldNames !== undefined && fieldNames !== null)
        {
            setfieldNames = [].concat(fieldNames);
        }
        Ext.suspendLayouts();
        Ext.Array.each(setfieldNames, function(fieldName) {
            field = me.down('[name=' + fieldName + ']');//Ext.getCmp(fieldName);
            if (field !== undefined && field !== null)
            {
              //  field.disable(true);
              	field.setDisabled(true);
                fieldDetailsBtn = null;
                nextField = field.nextSibling();
               	if (nextField  !== undefined && nextField !== null ) {
               		nextField = nextField.nextSibling();               		
               		if (nextField !== null && nextField.xtype == 'fielddetailsbtn' &&  nextField.getFieldName() == fieldName ) {
               			fieldDetailsBtn=nextField;
               		} 
               	}
               	if (fieldDetailsBtn === null) {
               		field.up('panel').cascade(function(item){
               			if (item.xtype === 'fielddetailsbtn' && item.getFieldName() == fieldName){
               				fieldDetailsBtn = item;
               				return false;
               			}
               		});

               	}
               	if (fieldDetailsBtn !== null) {
               		if (!fieldDetailsBtn.isDisabled()) {
               			fieldDetailsBtn.disable(true);
               		} 
               		monitorChkBtn = fieldDetailsBtn.getMonitorCheckBtn();
		           	if (monitorChkBtn && monitorChkBtn.isDisabled()) {
	           			monitorChkBtn.disable(true);
	           		} 
	           	monitorChkBtn.updateTabIcon();
               	}       	

            }
        });
        Ext.resumeLayouts(true);
    },
    
*/
/*
 * Ez kiszedi az összes formfieldet (saját xformfield-jeinknél a belső formfieldeket is, hogy pl. disabled-re tudjam tenni őket)
 * Kivéve a radiofieldeket, mert ott elég a radiogroup (checkboxgoupnál kell a checkfield is, mert az lehet külön is...)
 * És a fieldvalue függvényében engedélyezi avgy disabled-eli
 * Ilyenkor törli 
 * onlyRadioFields - ha true akkor a radiogroup helyett a radiofieldeket disabled-eli
 */
	
	setLinkedFieldsState : function(linkedfieldNames, fieldValue, onlyRadioFields){ "use strict";
		var me = this, 
			field,
			fields = [];
			//console.log('setLinkedFieldsState linkedfieldNames', linkedfieldNames)  ;   
		// éegyújtöm a field-eket. Ha fieldcontainer van fieldNames-ben, akkor kiszedem belőle is a fieldeket
        Ext.Array.each(linkedfieldNames, function(fieldName) {
        	field = me.down('[name=' + fieldName + ']');//Ext.getCmp(fieldName);
            if (field !== undefined && field !== null ) {
            	
            	if ( field.xtype === 'fieldcontainer' || field.xtype === 'fieldset' || field.xtype === 'panel' || field.isXFormField ) {
	        		field.cascade(function(item){
	            		if (item.isFormField  && item.xtype !== 'radiofield') {
	            			fields.push(item);
	           			}
	            	});
	        	} else if (field.isFormField  && field.xtype !== 'radiofield' ){
	        		fields.push(field);
	        	}
	       	}
        });	
       // console.log('setLinkedFieldsState fields', fieldValue, fields)  ;                         
        if (fields.length>0) {
	        if (fieldValue) {
	            this.setFieldsEnabled(fields, onlyRadioFields);
	           // Kell ez? disabled field mindig valid! : this.setFieldsMandatory(fields);
				this.validateFields(fields); // ez azért kell, hogy jelezzem a kitöltendőséget
	        } else {
	           	//this.setFieldsDisabled(fields);	
			// Kell ez? disabled field mindig valid! :	this.setFieldsOptional(fields);
	       		this.clearFields(fields); // ez töröl 
	       		this.setFieldsDisabled(fields, onlyRadioFields);
	       	}
       }
        
	},

	
	validateFields : function (fields) {"use strict";
        Ext.suspendLayouts();
        Ext.Array.each(fields, function(field) {
            if (field !== undefined && field !== null)
            {
	           	if (field.validate) {
	            	field.validate();	            	
	           	}
            }
        });
        Ext.resumeLayouts(true);
	},
	
    getFormFieldValue : function(formName, fieldName) { "use strict";
        var index, formRecord, fieldsStore, fieldRecord, value,
        formsStore = Ext.data.StoreManager.lookup('Forms');
        if (formsStore !== undefined) {
            index = formsStore.findExact('formtype', formName);
            if (index !== undefined && index !== -1) {
                formRecord = formsStore.getAt(index);
                if (formRecord !== undefined)
                {
                    value = formRecord.getFieldValue(fieldName);
                }
            }
        }
        return value;
    },

	
	getNextSibling : function(field) {"use strict";
        var nextfield = field.nextSibling();
        if (nextfield !== undefined && nextfield !== null) {
            if (nextfield.xtype === 'monitorcheck')
            {
                nextfield = nextfield.nextSibling();
            }
        }
        return nextfield;
    },
    
    getPreviousSibling : function(field) {"use strict";
        var prevfield = field.previousSibling();
        if (prevfield !== undefined && prevfield !== null) {
            if (prevfield.xtype === 'monitorcheck')
            {
                prevfield = prevfield.previousSibling();
            }
        }
        return prevfield;
    },

	getFormFieldValues : function(dirtyOnly) {"use strict";
		// ezek amiatt vannak, hogy ne nagyon kelljen átírni a gyári getValues functiont
		var asString= false,
			includeEmptyText = false,
			useDataValues = true;
			
        var values  = {},
            fields  = this.getForm().getFields().items,
            f,
            fLen    = fields.length,
            isArray = Ext.isArray,
            field, data, val, bucket, name;

        for (f = 0; f < fLen; f+=1) {
            field = fields[f];
			// itt jön a saját szűrés if-je:
	        if (field.getXType() != 'displayfield' && field.doNotSave !== true){
	            if (!dirtyOnly || field.isDirty()) {
	                data = field[useDataValues ? 'getModelData' : 'getSubmitData'](includeEmptyText);
	
	                if (Ext.isObject(data)) {
	                    for (name in data) {
	                        if (data.hasOwnProperty(name)) {
	                            val = data[name];
	
	                            if (includeEmptyText && val === '') {
	                                val = field.emptyText || '';
	                            }
	
	                            if (values.hasOwnProperty(name)) {
	                                bucket = values[name];
	
	                                if (!isArray(bucket)) {
	                                    bucket = values[name] = [bucket];
	                                }
	
	                                if (isArray(val)) {
	                                    values[name] = values[name] = bucket.concat(val);
	                                } else {
	                                    bucket.push(val);
	                                }
	                            } else {
	                                values[name] = val;
	                            }
	                        }
	                    }
	                }
	            }
	        }
        }

        if (asString) {
            values = Ext.Object.toQueryString(values);
        }
    //    console.log('values', values);
        return values;
       
    },
    
    updateTabIcon: function ( panel ){"use strict";
		var hasPanelError = false,
			hasEnabledMonitorBtn = false,
			hasUncheckedMonitorBtn = false,
			hasPanelOpenQuery = false;
        //	emptyBtns=[], iconChanged = false;
			
		//console.log('updateTabIcon called erre a panelra:', panel);
		if (panel && !this.blockErrorMarkingOnTab) {
			
			if (!panel.isDisabled()) {
				
				// JZS teszt
				if (!panel.tab.hasCls('icon-tab-noicon')) {
					panel.tab.addCls('icon-tab-noicon');	
				}
				 
				
				
				panel.cascade(function(item){
					if (item.isFormField && item.hasActiveError() ) {
						hasPanelError = true;
						return false;
					} 
				});
	
				if (hasPanelError) {
					if (panel.tab.hasCls('icon-tab-allmonitored')) {
						panel.tab.removeCls('icon-tab-allmonitored');	
					//	iconChanged = true;
					}
					if (!panel.tab.hasCls('icon-tab-errorfield')) {
						panel.tab.addCls('icon-tab-errorfield');
					//	iconChanged = true;
					}
	
				} else {
					if (panel.tab.hasCls('icon-tab-errorfield')	) {
						panel.tab.removeCls('icon-tab-errorfield')	;	
					//	iconChanged = true;
					}
					panel.cascade(function(item){
				//jzs:		if (item.xtype === 'fielddetailsbtn' && item.isDisabled() === false) {
				//	if (item.xtype === 'fielddetailsbtn' && item.getReferencedField()!== undefined && item.getReferencedField().isDisabled() === false) {
						if (item.xtype === 'fielddetailsbtn' ){
							if (item.iconCls !== 'icon-fieldhistory-btn_small') {
								hasPanelOpenQuery = true;
								return false;
							}
							if ( item.getMonitorCheckBtn() !== undefined && item.getMonitorCheckBtn().isDisabled() === false) {
								hasEnabledMonitorBtn = true;
								if ( item.getMonitorCheckBtn().pressed === false) {
									//emptyBtns = emptyBtns.concat(item.getMonitorCheckBtn());
									hasUncheckedMonitorBtn = true;
									return false;
								}
							}
						}
						
					});
					//console.log('emptyBtns',emptyBtns);
					if (/*emptyBtns.length>0 || */!hasEnabledMonitorBtn || hasPanelOpenQuery || hasUncheckedMonitorBtn) {
						if (panel.tab.hasCls('icon-tab-allmonitored')) {
							panel.tab.removeCls('icon-tab-allmonitored');	
							//iconChanged = true;		
						}
					} else {
						if (!panel.tab.hasCls('icon-tab-allmonitored')) {
							panel.tab.addCls('icon-tab-allmonitored');	
							//iconChanged = true;
						}
					}	
			    }

			} else { //Ha disabled, törlök mindent
				if (panel.tab.hasCls('icon-tab-allmonitored')) {
					panel.tab.removeCls('icon-tab-allmonitored');
					//iconChanged = true;	
				}
				if (panel.tab.hasCls('icon-tab-errorfield')) {
					panel.tab.removeCls('icon-tab-errorfield');
					//iconChanged = true;

				}
			}	
	//		if (iconChanged ) {
	//			panel.tab.updateLayout();
	//		}
		}	
	}

});
