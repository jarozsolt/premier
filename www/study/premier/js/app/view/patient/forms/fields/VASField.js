/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * VASField.js
 *
 * Container for a Visual Analogue Scale fields
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.VASField', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.vasfield',
    layout : 'hbox',
    validateMessage : _("Kérem töltse ki a mezőt vagy állítsa 'Nem vizsgált'-ra."),
    isXFormField : true,
    defaults : {
        hideLabel : true,
        padding : '5 0 5 0'
    },
    items : [{
        xtype : 'sliderfield',
        doNotSave : true,
        width: 300,
        tipText: function(thumb){"use strict";
            return String(thumb.value) + ' mm';
        },
        listeners: {
            'change': function(field, newValue) {"use strict";
                var valueField = this.nextSibling('numberfield');
                if (valueField !== undefined) {
                    if (valueField.getValue() != newValue)
                    {
                        if (newValue === 0) {
                            valueField.setValue('');
                        }
                        else {
                            valueField.setValue(newValue);
                        }
                    }
                }
            }
        }
    }, {
        xtype: 'splitter',
        minWidth : 20
    }, {
        xtype : 'numberfield',
        doNotSave : true,
        width : 50,
        hideTrigger : true,
        minValue : 0,
        maxValue : 100,
        listeners: {
            'change': function(field, newValue) {"use strict";
                var fieldSlider = this.previousSibling('sliderfield');
                if (fieldSlider) {
                    if (fieldSlider.getValue() != newValue)
                    {
                        fieldSlider.setValue(newValue);
                    }
                }
            }
        },
        getErrors : function(value) {"use strict";
            var parent = this.up('fieldcontainer'),
            checkField = this.nextSibling('checkboxfield');
            if (parent.allowBlank === false) {
                if ((value === null || value === '') && checkField.getValue() === false) {
                    Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
                    return [parent.validateMessage];
                }
                Ext.form.Field.prototype.clearInvalid.call(this);
            }
            return [];
        }
    }, {
        xtype: 'splitter'
    }, {
        xtype : 'displayfield',
        doNotSave : true,
        value : _("mm")
    }, {
        xtype : 'splitter',
        minWidth : 20,
        flex : 1
    }, {
        xtype: 'checkboxfield',
        boxLabel: _("Nem vizsgált"),
        doNotSave : true,
        width : 100,
        inputValue: 'n.a.',
        getErrors : function(value) {"use strict";
            var parent = this.up('fieldcontainer'),
            valueField = this.previousSibling('numberfield');
            if (parent.allowBlank === false) {
                if ((valueField.getValue() === null || valueField.getValue() === '') && value === false) {
                    Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
                    return [parent.validateMessage];
                }
                Ext.form.Field.prototype.clearInvalid.call(this);
            }
            return [];
        },
        handler : function(field) {"use strict";
            var valueField = this.previousSibling('numberfield'),
            sliderField = this.previousSibling('sliderfield');
            if (this.getValue())
            {
                valueField.setValue('');
                valueField.disable(true);
                sliderField.setValue(null);
                sliderField.disable(true);
            }
            else
            {
                valueField.enable(true);
                sliderField.enable(true);
            }
        }
    },{
        xtype: 'hiddenfield'
    }],

    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },
    strictInitComponent : function() { "use strict";
        var me = this,
        checkField = this.down('checkboxfield'),
        hiddenField = this.down('hiddenfield');
        hiddenField.name = me.name;
        hiddenField.parentContainer = me;
        hiddenField.setValue = function (value) { this.parentContainer.setValue(value); };
        hiddenField.getValue = function () { return this.parentContainer.getValue(); };
        if (this.naboxLabel !== undefined) {
            checkField.boxLabel = this.naboxLabel;
        }
    },
    getValue: function() {"use strict";
        var me = this, value,
        valueField = me.down('numberfield'),
        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            return 'n.a.';
        }
        return valueField.getValue();
    },
    setValue: function(value) {"use strict";
        var me = this,
        valueField = me.down('numberfield'),
        sliderField = me.down('sliderfield'),
        checkField = me.down('checkboxfield');
        if (value === 'n.a.')
        {
            checkField.setValue(true);
            valueField.setValue('');
            sliderField.setValue(null);
        }
        else if (value !== undefined)
        {
            checkField.setValue(false);
            valueField.setValue(value);
            sliderField.setValue(value);
        }
        return me;
    }
});
