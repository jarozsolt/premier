/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * DateTimeField.js
 *
 * Container for a date and time field
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.DateTimeField', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.datetimefield',
    layout : 'hbox',
    isXFormField : true,
    //combineErrors : true,
    defaults : {
        hideLabel : true
    },
    items : [],

    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    strictInitComponent : function() { "use strict";
        var me = this;
        
        me.items = [{
            xtype : 'datefield',
            doNotSave : true,
            width : 126,
            padding : '5 0 5 0',
          //  format : Kron.defaultDateFormat,
            allowBlank : me.allowBlank,
            disabled : me.disabled,
            format : me.format,
            maxValue: me.maxValue, // me.futureDate===false?new Date():undefined,
            isOptional : (me.allowBlank===undefined||me.allowBlank===true)?true:false

            // TODO: a rossz dátumformátumra nem figyelmeztet
            /*getErrors : function(value) {"use strict";
                var parent = this.up('fieldcontainer'),
                checkField = this.nextSibling('checkboxfield');
                if (parent.allowBlank === false) {
                    if ((value === null || value === '') && checkField.getValue() === false) {
                        Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
                        return [parent.validateMessage];
                    }
                    Ext.form.Field.prototype.clearInvalid.call(this);
                }
                return [];
            }*/
       
        },{
            xtype : 'displayfield',
            width : 50,
            doNotSave : true,
            value : _('időpont:'),
            padding : '5 0 5 10'
        },{
            xtype: 'timefield',
	     //   minValue: '6:00 AM',
	     //   maxValue: '8:00 PM',
	        format: 'H:i',
	        increment: 30,
			width : 80,
            doNotSave : true,
            padding : '5 0 5 20',
            disabled : me.disabled,
            allowBlank : me.allowBlank
            /*getErrors : function(value) {"use strict";
                var parent = this.up('fieldcontainer'),
                fieldDate = this.previousSibling('datefield');
                if (parent.allowBlank === false) {
                    if ((fieldDate.getValue() === '' || fieldDate.getValue() === null) && value === false) {
                        Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
                        return [parent.validateMessage];
                    }
                    Ext.form.Field.prototype.clearInvalid.call(this);
                }
                return [];
            },*/
        },{
            xtype: 'hiddenfield',
            name : me.name,
            parentContainer : me,
            disabled : me.disabled,
            setValue : function (value) { this.parentContainer.setValue(value); },
            getValue : function () { return this.parentContainer.getValue(); },
            getFormattedValue : function (fieldValue) { return this.parentContainer.getFormattedValue(fieldValue); },
            isValid : function () { return this.parentContainer.isValid(); }
       		//reset : function () { this.parentContainer.reset(); },
    		//getXType: function() {return me.xtype;}
        }];
    },

    getValue: function() {"use strict";
        var me = this,
	        dateField = me.down('datefield'),
	        timeField = me.down('timefield'),
			dateValue = dateField.getValue(),
            timeValue = timeField.getValue();
        if (dateValue!== undefined && dateValue !== null) {
	        if (timeValue!== undefined && timeValue !== null) {
	        	dateValue.setHours(timeValue.getHours(),timeValue.getMinutes());
	        } 
	    } 
        return dateValue;
    },
    
    setValue: function(value) {"use strict";
        var me = this,
        	dateField = me.down('datefield'),
	        timeField = me.down('timefield');
	        
		if (value !== undefined )
        {
            dateField.setValue(value);
            timeField.setValue(value);
        }
        return me;
    },
    
	isValid : function() {
   		var dateField = this.down('datefield'),
	   		timeField = this.down('timefield');
	   	return dateField.isValid() && timeField.isValid();
   	}, 
    
	getFormattedValue : function(fieldValue) {"use strict";
		var me = this,
			formattedValue='',
	        dateField = me.down('datefield'),
	        timeField = me.down('timefield'),
	        dateValue = dateField.getValue(),
            timeValue = timeField.getValue();
            
        if (dateValue!== undefined && dateValue !== null) {
	        if (timeValue!== undefined && timeValue !== null) {
	        	dateValue.setHours(timeValue.getHours(),timeValue.getMinutes());
	        	formattedValue =Ext.Date.format(dateValue, "Y.m.d");
	        } 
	    } 
        return formattedValue;
	},
	
	getName: function() {"use strict";
   		return this.name;
   	}
   	
   	
	/*
	reset: function() {"use strict";
        var me = this,
	        dateField = me.down('datefield'),
	        timeField = me.down('timefield');
       	dateField.reset();
        timeField.reset();
        return me;
	},
   	
   	getName: function() {"use strict";
   		return this.name;
   	},

   	setDisabled : function(disabled) {
   		var dateField = this.down('datefield'),
	        timeField = this.down('timefield');
	    dateField.setDisabled(disabled);
	    timeField.setDisabled(disabled);
        return this[disabled ? 'disable': 'enable']();
    
   	},
   	validate : function() {
   		var dateField = this.down('datefield'),
	   		timeField = this.down('timefield');
	   	dateField.validate();
	   	timeField.validate();
   	} ,*/
   
});
