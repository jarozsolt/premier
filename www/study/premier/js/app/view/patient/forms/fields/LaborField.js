/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * LaborField.js
 *
 * Container for a labor values
 * Két radiogroup kombinálva:
 * Nem történt/Pozitív / Negatív
 * Klinikailag jelentős eltérés: Igen/  Nem
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.LaborField', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.laborfield',
	isXFormField : true,
    layout : 'hbox',
    validateMessage : _("Kérem töltse ki a mezőt minkét részét"),
	msgTarget : 'side',
    valueSeparator :  '|',
    formattedValueSeparator : ' / ',
    valueFieldLabels : [_('Nem történt'), _('Pozitív'),_('Negatív')] ,
    secondFieldLabels : [_('Klinikailag jelentős'),_('Klinikailag nem jelentős') ],
    items : [],

    initComponent : function() { // Do not use "use strict" on functions containing callParent!
       
        this.strictInitComponent();
     	this.callParent(arguments);
    },
    strictInitComponent : function() { "use strict";
        var me = this;
        
        me.items = [{
			xtype : 'radiogroup',
			doNotSave : true,
			width : 210,
			height : 22,
            columns: [80,80,40],
            name:  me.name +'valuefield',
            disabled : me.disabled,
            allowBlank : me.allowBlank,
            items : [{
                name :  me.name + 'valuefield',
                doNotSave : true,
                xtype : 'radiofield',
                inputValue : 1,
               	handler : function(field) {
					var  secondField = field.up('radiogroup').nextSibling();
					secondField.setDisabled(field.getValue());
					if (field.getValue()) {
						secondField.allowBlank = true;
						secondField.clearInvalid();
						secondField.items.each(function(item, index, len){
	                		if (item.xtype === 'radiofield') {
	                			if (field.getValue()) {
		                			item.setValue(false);
	                			} 
	                		}
	                	});
	               	} else {
	               		secondField.allowBlank = false;
	               		secondField.validate();
	               	}
				}
            },{
                name : me.name + 'valuefield',
                doNotSave : true,
                xtype : 'radiofield',
                inputValue : 2 
            },{
                name : me.name + 'valuefield',
                doNotSave : true,
                xtype : 'radiofield',
                inputValue : 3
            }],
          	msgTarget : 'side'
       //    	hideTrigger : true,
/*	        getErrors : function(value) {"use strict";

	            var parent = this.up('fieldcontainer'),
	            	field1 = parent.down('radiogroup'),
        			field2 = field1.nextSibling('radiogroup'),
        			field1Value = field1.getValue(),
        			field2Value = field2.getValue();
        			console.log('valuefield geterror field1', field1, field1Value);
	            if (parent.allowBlank === false ) {
	                if ( Ext.Object.getSize(field1Value) == 0  && !field1.isDisabled() ) {
	                    Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);console.log('valuefield markInvalid');
	                    return [parent.validateMessage];
	                }
	                Ext.form.Field.prototype.clearInvalid.call(this);
	            }
	            return [];
	        }*/
		},{
			xtype : 'radiogroup',
            padding : '0 0 0 70',
            doNotSave : true,
            columns: [80,45],
            height : 22,
            width : 150,
            name: me.name + 'secondfield',
            disabled : me.disabled,
            allowBlank : me.allowBlank,
            items : [{
                name : me.name + 'secondfield',
                doNotSave : true,
                xtype : 'radiofield',
                boxLabel : _("Igen"),
                inputValue : 1
                
            },{
                name : me.name + 'secondfield',
                doNotSave : true,
                xtype : 'radiofield',
                boxLabel : _("Nem"),
                inputValue : 2 
              //  padding : '0 0 0 10'
            }],
          	msgTarget : 'side',
           	hideTrigger : true
	/*        getErrors : function(value) {"use strict";console.log('secondfield geterror');
	      		var parent = this.up('fieldcontainer'),
	            	field1 = parent.down('radiogroup'),
        			field2 = field1.nextSibling('radiogroup'),
        			field1Value = field1.getValue(),
        			field2Value = field2.getValue();
	            if (parent.allowBlank === false ) {
	                if (Ext.Object.getSize(field2Value) == 0  && !field2.isDisabled()) {console.log('secondfield markInvalid');
	                    Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
	                    return [parent.validateMessage];
	                }
	                Ext.form.Field.prototype.clearInvalid.call(this);
	            }
	            return [];
	        }*/
						
		},{
       		xtype: 'hiddenfield',
       		name : me.name,
       		parentContainer : me,
        	setValue : function (value) { this.parentContainer.setValue(value); },
        	getValue : function () { return this.parentContainer.getValue(); },
        	getFormattedValue : function (fieldValue) { return this.parentContainer.getFormattedValue(fieldValue); },
        	isValid : function(){return this.parentContainer.isValid(); }
     //   hiddenField.reset = function () { this.parentContainer.reset(); };
   // 	hiddenField.getXType= function() {return me.xtype;}
    	
    	}];
        
        /*if (this.validateMessage !== undefined) {
            checkField.validateMessage = this.validateMessage;
        }*/
    },
    getValue: function() {"use strict";
        var me = this, value='',
        field1 = me.down('radiogroup'),
        field2 = field1.nextSibling();
        if (field1.getValue()[this.name + 'valuefield'] !== undefined && field1.getValue()[this.name + 'valuefield'] !== null ) {
        	value = field1.getValue()[this.name + 'valuefield'];
        }
         if (field2.getValue()[this.name + 'secondfield'] !== undefined && field2.getValue()[this.name + 'secondfield'] !== null ) {
        	value = value +me.valueSeparator+field2.getValue()[this.name + 'secondfield'];
        }
        return value;
    },

   	getFormattedValue : function(fieldValue) {"use strict";
   		//console.log('getFormattedValue', fieldValue);
   		var me = this, formattedValue='',valueTokens,
        field1 = me.down('radiogroup'),
        field2 = field1.nextSibling();
        if (fieldValue !== undefined && fieldValue !== null && fieldValue.split !== undefined && fieldValue != '') { // ha meg van adca a fieldvalue, akkor azt mutatom meg
        	valueTokens = fieldValue.split(me.valueSeparator);
        	if (valueTokens[0] !== undefined)
            {
                formattedValue = this.valueFieldLabels[valueTokens[0]-1];
            }
            if (valueTokens[1] !== undefined)
            {
                formattedValue = formattedValue + me.formattedValueSeparator + this.secondFieldLabels[valueTokens[1]-1];
            }
        	
       	} else {// ekkor a form mező értékét veszem
        	 if (field1.getValue()[this.name + 'valuefield'] !== undefined && field1.getValue()[this.name + 'valuefield'] !== null && field1.getValue()[this.name + 'valuefield'] >= 0) {
	        	formattedValue = this.valueFieldLabels[field1.getValue()[this.name + 'valuefield']-1];
	        }
	         if (field2.getValue()[this.name + 'secondfield'] !== undefined && field2.getValue()[this.name + 'secondfield'] !== null ) {
	        	formattedValue = formattedValue + me.formattedValueSeparator + this.secondFieldLabels[field2.getValue()[this.name + 'secondfield']-1];
	        }
        } 
       
        return formattedValue;
   	},
    setValue: function(value) {"use strict";
        var me = this, valueTokens,
        field1 = me.down('radiogroup'), field2;
        if (field1) {
        	field2 = field1.nextSibling();
	   //         console.log('value', me,  value);
	   		value = '' + value;
	       	if (value === undefined || value == null || value == '' )
	        {
	            field1.items.each(function(item, index, len){
	        		item.setValue(false);
	        	});
	            field2.items.each(function(item, index, len){
	        		item.setValue(false);
	        	});
	        }
	        else if ( value.split !== undefined)
	        {
	            valueTokens = value.split(me.valueSeparator);
	        //     console.log('field1', field1);
	     //        console.log('field2', field2);
	      //       console.log('valueTokens', valueTokens);
	
	            field1.items.each(function(item, index, len){
	        		if (valueTokens[0] !== undefined && item.xtype == 'radiofield' && item.inputValue == valueTokens[0]) {
	                    item.setValue(true);
	                } else {
	                    item.setValue(false);
	                }
	        	});
	
	            field2.items.each(function(item, index, len){
	        		if (valueTokens[0] !== undefined && item.xtype == 'radiofield' && item.inputValue == valueTokens[1]) {
	                    item.setValue(true);
	                } else {
	                    item.setValue(false);
	                }
	        	});
	        }
        }
        
        return me;
    },
    
    isValid: function() {"use strict";
       	var me = this, 
       	field1 = me.down('radiogroup'),
        field2 = field1.nextSibling();
        
        return field1.isValid() && field2.isValid();
    },
 
   getName: function() {"use strict";
   		return this.name;
   }/*   reset: function() {"use strict";
        var me = this,
        field1 = me.down('radiogroup'),
        field2 = field1.nextSibling();
       	field1.reset();
        field2.reset();
        return me;
   },
   validate: function() {"use strict";
   		var field1 = this.down('radiogroup'),
        field2 = field1.nextSibling(),
        returnValue1 = false,
        returnValue2 = false;
	   	if (field1 !== undefined && field1 !== null) {
	   		returnValue1 = field1.validate();
	   	}
	   	if (field2 !== undefined && field2 !== null) {
	   		returnValue2 = field2.validate();
	   	}
   		return returnValue1 && returnValue2;
   }*/
});
