/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * UnscheduledVisitForm.js
 *
 * Unscheduled visit form - panel in Visit view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.UnscheduledVisitForm', {
    extend : 'Kron.view.patient.forms.GeneralPatientForm',
    alias : 'widget.unscheduledVisitForm',
    id : 'UnscheduledVisitForm',
    formWidth : 760,
    formHeight : 600,
    formTitle : Kron.formNames.UnscheduledVisit,
    formIconCls : 'icon-unscheduledvisit-dlg_small',
    autoScroll : true,
    layout : 'fit',
    bodyPadding : 5,
    
   	canSave : function() {"use strict";
        var fieldVisitDate = this.down('[name=visitDate]');
        if (fieldVisitDate) {
            if (fieldVisitDate.getValue() !== null && fieldVisitDate.getValue() !== '') {
                return true;
            }
        }
        return false;
    },
    
    alertSaveError : function() {"use strict";
        Ext.Msg.show({
            title : _("Hiba"),
            msg : _("A 'Vizit dátuma' mezőt kötelező kitölteni a vizit elmentéséhez!"),
            buttons : Ext.Msg.OK,
            icon : Ext.MessageBox.ERROR,
            animateTarget: 'dlgAccept'});
    },
  
   initForm : function() { "use strict";

    },
    
    items : [{
        xtype : 'tabpanel',
        layout : 'fit',
        activeTab : 0,
        plain : true,
        items : [{
            xtype : 'panel',
            title : _("ISZKÉMIÁS STÁTUSZ"),
            bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    }  
           	},
            items : [{
             	xtype:'fieldset',
			    border: 0,
			    defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
            	 	allowBlank : false,
        	 	 	msgTarget : 'side'
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
                    name : 'visitDate',
                    xtype : 'datefield',
                    fieldLabel : _("Vizit dátuma") + Kron.config.labelMandatoryMark,
                    labelWidth : 200,
                    width : 310,
                    format : Kron.defaultDateFormat,
                    maxValue: new Date(),
        /*            checkDate: function () {"use strict";
                        var form,
                        informDate = this.up('form').down('[name=informDate]');
                        if (informDate !== undefined) {
                            if (informDate.getValue() !== '' && informDate.getValue() !== null && this.getValue() !== '' && this.getValue() !== null) {
                                if (this.getValue() < informDate.getValue()) {
                                    form = this.up('form');
                                    if (form !== undefined) {
                                        form.showVisitDateWarning();
                                    }
                                }
                            }
                        }
                    },
                    listeners: {
                        'change': function (field, newValue, oldValue, eOpts) {"use strict";
                            if (this.isValid()) {
                                this.checkDate();
                            }
                        }
                    },*/
                    isPrintable : true,
                    msgTarget : 'qtip'
                },{
		            xtype: 'monitorcheck'
		        },{
		        	xtype: 'fielddetailsbtn'
				},{
					xtype : 'radiogroup',
	                fieldLabel : _("Vizit típusa"),
	                labelWidth : 200,
	                columns: [150,120],
	                name: 'vizittipus',
	                items : [{
	                    name : 'vizittipus',
	                    xtype : 'radiofield',
	                    boxLabel : _("személyes vizit"),
	                    inputValue : 1
	                },{
	                    name : 'vizittipus',
	                    xtype : 'radiofield',
	                    boxLabel : _("telefonos vizit"),
	                    inputValue : 2 
	                }]
                },{
		            xtype: 'monitorcheck'		            
		        },{
					xtype: 'fielddetailsbtn'
				}]
             },{
             	xtype:'fieldset',
       			title: _("Iszkémiás státusz"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
            	 	allowBlank : false,
        	 	 	msgTarget : 'side',
	                labelWidth : 150
               	},
             	items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
                  	xtype : 'radiogroup',
	                fieldLabel : _("Iszkémiás státusz "),
	                columns: [100,100,100,100],
	                name: 'iszkemiasstatus',
	                items : [{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nincs angina"),
	                    inputValue : 1
	                    
	                },{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Stabil angina"),
	                    inputValue : 2,
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['stabilangina'], this.getValue());
                       	}
	                }, {
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Instabil angina"),
	                    inputValue : 3, 
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['instabilangina'], this.getValue());
                       	}
	                },{
	                    name : 'iszkemiasstatus',
	                    xtype : 'radiofield',
	                    boxLabel : _("Néma Iszkémia "),
	                    inputValue : 4
	                }]
			   	},{
		            xtype: 'monitorcheck'
		        }, {
					xtype: 'fielddetailsbtn'
	           },{
					xtype : 'radiogroup',
	                fieldLabel : _("CCS"),
	                columns: [100,100,100,60],
	                name: 'stabilangina',
	                disabled : true,
	                items : [{
	                    name : 'stabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("I"),
	                    inputValue : 1
	                }, {
	                    name : 'stabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("II"),
	                    inputValue : 2 
	                },{
	                    name : 'stabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("III"),
	                    inputValue : 3
	                },{
	                    name : 'stabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("IV"),
	                    inputValue : 4 
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled : true
		        }, {
					xtype: 'fielddetailsbtn',
					disabled : true
	           },{
					xtype : 'radiogroup',
	                fieldLabel : _("Braunwald osztályozás"),
	                width: 560,
	                columns: 3,
	                name: 'instabilangina',
	                disabled : true,
	                items : [{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("IA"),
	                    inputValue : 1
	                }, {
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("IB"),
	                    inputValue : 2 
	                },{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("IC"),
	                    inputValue : 3
	                },{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("IIA"),
	                    inputValue : 4
	                }, {
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("IIB"),
	                    inputValue : 5
	                },{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("IIC"),
	                    inputValue : 6 
	                },{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("IIIA"),
	                    inputValue : 7
	                }, {
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("IIIB"),
	                    inputValue : 8
	                },{
	                    name : 'instabilangina',
	                    xtype : 'radiofield',
	                    boxLabel : _("IIIC"),
	                    inputValue : 9 
	                }]
			   	},{
		            xtype: 'monitorcheck',
		            disabled : true
		        }, {
					xtype: 'fielddetailsbtn',
					disabled : true
	           	}]
 			}]
		}, {  /*************************************************************************************************************/
                
            xtype : 'panel',
            title : _("SZÍVGYÓGYSZEREK"),
           	bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    },  
			    defaults : {
                	margin : '5 0 5 0'
               	}
           	},
            items : [{
            	xtype:'fieldset',
       			title: _("Szívgyógyszerek"),
 				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
           			xtype : 'displayfield',
           			name : 'szivgyogyszerdisplayfield',
                    doNotSave : true,
                    value : _("Kérjük, jelöljön minden szívgyógyszert, amit a beteg aktuálisan szed!"),
                    colspan: 3
               
               },{
               		xtype: 'checkboxfield',
               		width: 570,
               		boxLabel:  _("Aszpirin"),
               		name: 'aszpirin',
               		padding : '0 0 0 12'
               		
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Clopidogrel"),
               		name: 'clopidogrel',
               		padding : '0 0 0 12'
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Egyéb trombocita aggregációt gátló gyógyszer"),
               		name: 'egyebtrombocitaaggregaciogatlo',
               		padding : '0 0 0 12'
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("ACE gátló"),
               		name: 'acegatlo',
               		padding : '0 0 0 12'
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Sztatin"),
               		name: 'sztatin',
               		padding : '0 0 0 12'
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Antikoaguláns"),
               		name: 'antikoagulans',
               		padding : '0 0 0 12'
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Béta-blokkoló"),
               		name: 'betablokkolo',
               		padding : '0 0 0 12'
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Angiotensin II receptor blokkoló"),
               		name: 'angiotensiniireceptorblokkolo',
               		padding : '0 0 0 12'
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
     			},{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Nem-sztatin lipidcsökkentő szer"),
               		name: 'nemsztatinlipidcsokkento',
               		padding : '0 0 0 12'
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
				}]
 			}]
		}, {  /*************************************************************************************************************/
                
            xtype : 'panel',
            title : _("EGYÉB KOMPLIKÁCIÓ"),
           	bodyStyle : 'padding:10px;',
            border : true,
            autoScroll : true,
            defaults : {
                width : 685,
                layout: {
			        type: 'table',
			        columns: 3
			    },  
			    defaults : {
                	margin : '5 0 5 0'
               	}
           	},
            items : [{
             	xtype:'fieldset',
       			title: _("Komplikáció a beavatkozást követően"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false,
        	 	 	labelWidth : 300
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
     				xtype : 'radiogroup',
	                fieldLabel : _("Történt-e valamilyen komplikáció a kórházi elbocsájtás óta"),
	                labelWidth : 400,
	                width: 560,
	                columns: [80,80],
	                name: 'komplikacio',
	                items : [{
	                    name : 'komplikacio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['komplikacioheader',
                            	'halal',
								'stroke',
								'myocardialisinfarktus',
								'ismeteltrevaszkularizacio',
								'verzes',
								'definitivstenttrombozis',
								'egyebkomplikacio'
                            	], this.getValue());
                            if ( this.getValue() ) {
                            	form.showAEWarning();
                             }
                       	}
	                },{
	                    name : 'komplikacio',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
		        },{
           			xtype : 'displayfield',
                    doNotSave : true,
                    name : 'komplikacioheader',
                    colspan: 3,
                    value : _("Jelölje a komplikáció(ka)t:"),
                    padding : '0 0 0 12',
                    disabled:true
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Halál"),
               		name: 'halal',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn',
		            disabled :true
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Stroke"),
               		name: 'stroke',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn',
		            disabled :true
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Myocardiális infarktus"),
               		name: 'myocardialisinfarktus',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn',
		            disabled :true
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Ismételt revaszkularizáció"),
               		name: 'ismeteltrevaszkularizacio',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn',
		            disabled :true
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Vérzés"),
               		name: 'verzes',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn',
		            disabled :true
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Definitív stent trombózis (angiográfiával vagy patológiailag igazolt)"),
               		name: 'definitivstenttrombozis',
               		padding : '0 0 0 12',
               		disabled : true
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn',
		            disabled :true
               },{
               		xtype: 'checkboxfield',
               		boxLabel:  _("Egyéb, részletezze"),
               		name: 'egyebkomplikacio',
               		padding : '0 0 0 12',
               		disabled : true,
               		handler : function(field) {"use strict";
                            var form = field.up('form');
                            form.setLinkedFieldsState(['egyebkomplikacioreszletek'], this.getValue());
                       	}
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn',
		            disabled :true
               },{
               		xtype: 'textfield',
               		msgTarget : 'qtip',
	            	name: 'egyebkomplikacioreszletek',
					width: 560,
					disabled : true,
					margin: '0 0 0 14'
				},{
		            xtype: 'monitorcheck',
		            disabled :true
		        },{
					xtype: 'fielddetailsbtn',
		            disabled :true
         		}]
         	},{
         		xtype:'fieldset',
       			title: _("Nemkívánatos események"),
				defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
            		xtype : 'radiogroup',
	                fieldLabel : _("Történt-e rosszabbodás a társbetegségekben vagy történ-e Nemkívánatos Esemény az előző vizit óta"),
	                labelWidth : 400,
	                columns: [80,80],
	                name: 'rosszabbodastarsbetegsegben_ae',
	                items : [{
	                    name : 'rosszabbodastarsbetegsegben_ae',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    listeners : {
                            change : function(rb, newValue, oldValue, options) {
                                if (newValue === true) {
                                    var form = this.up('form');
                                    if (form !== undefined) {
                                        form.showAEWarning();
                                    }
                                }
                            }
                        }
	                },{
	                    name : 'rosszabbodastarsbetegsegben_ae',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
		        }]
			},{
         		xtype:'fieldset',
       			title: _("Egyéb kezelések"),
                defaults : {
                	margin : '5 0 5 0',
            	 	maxWidth : 590,
        	 	 	msgTarget : 'side',
        	 	 	allowBlank : false
               	},
			    
				items: [{
					xtype: 'component',
					width: 590,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					width: 32,
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype: 'component',
					height : 1,
					margin : '0 0 0 0'
				},{
					xtype : 'radiogroup',
	                fieldLabel : _("Történt-e változás az előző vizithez képest az egyéb kezelésekben"),
	                labelWidth : 400,
	                columns: [80,80],
	                name: 'valtozas_egyebkezelesekben',
	                items : [{
	                    name : 'valtozas_egyebkezelesekben',
	                    xtype : 'radiofield',
	                    boxLabel : _("Igen"),
	                    inputValue : 1,
	                    listeners : {
                            change : function(rb, newValue, oldValue, options) {
                            	//console.log('rb.isDirty()',rb.isDirty());
                                if (newValue === true ) {
                                    var form = this.up('form');
                                    if (form !== undefined) {
                                        form.showMedicationWarning();
                                    }
                                }
                            }
                        }
	                },{
	                    name : 'valtozas_egyebkezelesekben',
	                    xtype : 'radiofield',
	                    boxLabel : _("Nem"),
	                    inputValue : 2
	                }]
				},{
		            xtype: 'monitorcheck'
		        },{
					xtype: 'fielddetailsbtn'
			            
				}]
             }]
       }]
    }]
});
