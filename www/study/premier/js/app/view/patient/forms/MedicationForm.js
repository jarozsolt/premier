/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MedicationForm.js
 *
 * Medications form - panel in Patient view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 * @todo Decrease the height of the dialog
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.MedicationForm', {
	extend : 'Kron.view.patient.forms.GeneralPatientForm',
	alias : 'widget.medicationForm',
	id : 'MedicationForm',
    formWidth : 760,
    formHeight : 600,
    formTitle : Kron.formNames.Medication,
    formIconCls : 'icon-medication-dlg_small',
    autoScroll : true,
	bodyStyle:'background-color:#ffffff;padding:10px',
   // Ha nincs tab panel, vagy nem akarom ott jelezni, ha van nem valid field, akkor ezt true-ra kell állítani
    blockErrorMarkingOnTab : true, 
    
    defaults : {
        width: 685,
        labelWidth: 400,
        layout: {
            type: 'table',
            columns: 3
        }
    },  
    
    // We can save the form only if the trade name field is not empty
    canSave : function() {"use strict";
        var fieldTradeName = this.down('[name=tradename]'); 
        if (fieldTradeName) {
            if (fieldTradeName.getValue() !== null && fieldTradeName.getValue() !== '') {
                return true;
            }
        }
        return false;
    },
    
    // Alert an error message if the diagnosis is not filled
    alertSaveError : function() {"use strict";
        Ext.Msg.show({
            title : _("Hiba"),
            msg : _("A 'Kezelés / gyógyszer megnevezése' mezőt kötelező kitölteni az űrlap elmentéséhez!"),
            buttons : Ext.Msg.OK,
            icon : Ext.MessageBox.ERROR,
            animateTarget: 'dlgAccept'});
    },
    
/*
 * From inicializásál, formfield értékekkel
 */
    initForm : function(fieldValues) { "use strict";
		//console.log('initForm called');
		this.deleteExtraFieldSets();

		this.addExtraFields(fieldValues);
		
		this.isFormDirty = false;

		return true;
	},
	
	// form érték betöltés utáni ellenőrzéseket , amelyekre alertet kell doni, lehet ide tenni.
	onAfterFormLoad : function(fieldValues) { "use strict";
		var me = this;
		if (fieldValues['medicationType'] != 2 ) { 
			me.setLinkedFieldsState(['dosecontainer'], false);
			me.down('button[name=adddosebtn]').disable();
		}
	},
	
	
		// A gyerkőcnél kell definiálni egy "extraFields" nevű object-et, aminek az attrinutumai a hozzáadó fv-ek, a kulcsok a extrafield suffix nevek
	addExtraFields : function (fieldValues) {"use strict";
		//console.log('addExtraFields called');
		var valueTokens=[],
			extraFieldIds=[],
			extraFieldContainer,
			separator = '_',
			me = this,
			doseField1,
			doseField2,
			deleteBtn;
			
		Ext.Object.each(this.extraFields, function(key, value, myself) {
			// összegyűjtön az extraField Id kat
			extraFieldIds=[];
			Ext.Object.getKeys(fieldValues).forEach(function (element, index, array) {
				valueTokens = element.split(separator);
				if (valueTokens[0] == key) {
					if (extraFieldIds.indexOf(valueTokens[1]) == -1) {
						extraFieldIds.push(valueTokens[1]);
					}
				}
			});
			extraFieldContainer=me.down('[name=' + key + 'container]');
			if (extraFieldIds.length>0) {
				extraFieldIds.sort(function(a,b) {
					var ia = parseInt(a),ib = parseInt(b);
					if (ia < ib) {
						return -1;
					} else if (ia > ib) {
					   return 1;
					} else {   
						return 0;
					}
				});
				
				Ext.suspendLayouts();	
				Ext.Array.each(extraFieldIds, function(element, index, array){
					value(extraFieldContainer, element);
				});
				Ext.resumeLayouts(true);
			} else if ( key === 'dose') {
				// ha nincs egy elem sem a dosecontainerben, akkor is hozzáadok kettőt
				Ext.suspendLayouts();
				value(extraFieldContainer, 0);
				value(extraFieldContainer, 1);
				Ext.resumeLayouts(true);
			}
			// hide-olom a dosefield első két törlés gombját, és disabled-re állítom az elején	 
			if ( key === 'dose') { 
				doseField1 = me.down('[name=dose_0_field]');
				if (doseField1 !== undefined && doseField1 !== null) {
					deleteBtn = doseField1.up('fieldset').previousSibling().down('button');
					if (deleteBtn !== undefined && deleteBtn !== null) {
						deleteBtn.setVisible(false);
					}
				}
				doseField2 = me.down('[name=dose_1_field]');
				if (doseField2 !== undefined && doseField2 !== null) {
					deleteBtn = doseField2.up('fieldset').previousSibling().down('button');
					if (deleteBtn !== undefined && deleteBtn !== null) {
						deleteBtn.setVisible(false);
					}
				}
				
			}
		});
		
		return true;
	},

	/*
	* kitörli az összes extra fieldsetet
	*/
	deleteExtraFieldSets : function () {"use strict";
		var extraFieldContainerNames = [  'dosecontainer'],
			container, success= true, me = this;
		Ext.suspendLayouts();	
		Ext.Array.each(extraFieldContainerNames, function(name, index, countriesItSelf) {
			container = me.down('[name=' + name + ']');
			if (container !== undefined && container !== null) {
				container.removeAll();	
			} else {
				success = false;
			}
		});
		Ext.resumeLayouts(true);
		return success;
	},
	
	/*
	* extraFieldContainer - container to add field sets
	* id - id of added field set
	*/
	extraFields : { 
		'dose' : function(extraFieldContainer, id) {"use strict";
			var i=0;
			if (extraFieldContainer) {
				if (id !== undefined &&  id !== null) {
					i=id;
				} else {
					extraFieldContainer.items.each(function(item, index, len) {
						if (item.extraFieldId >= i) {
							i = parseInt(item.extraFieldId) + 1 ;
						}
					});
				}
	
				extraFieldContainer.add({
									xtype: 'fieldset',
									title : _("Hatóanyag"),
									extraFieldId : i,
									width : 655,
									labelWidth: 300,
									layout: {
											type: 'table',
											columns: 2
									},
									
									items: [{
										xtype: 'container',
										width: 50,
										items : [{
											xtype : 'button',
											buttonType : 'extrafield',
											hideMode : 'visibility',
											width: 50,
											text : _("Törlés"),
											handler : function(button) {"use strict";
												var extraFieldSet = button.up('fieldset'),
													doseContainer = extraFieldSet.up('fieldcontainer');
											
												Ext.MessageBox.prompt({
								                    title : _("Figyelmeztetés"),
								                    msg :  _("Biztos, hogy törölni kívánja a hatóanyagot?"),
								                    buttons : Ext.Msg.OKCANCEL,
								                    buttonText: {ok: _("Töröl"), cancel: _("Mégsem")},
								                    icon : Ext.MessageBox.WARNING,
								                    animateTarget: 'dlgAccept',
								                    scope : this,
								                    fn : function(btn, ev) {
								                        if (btn === 'ok') {
								                            button.up('form').isFormDirty = true;		
															doseContainer.remove(extraFieldSet);
															doseContainer.focus();
								                        }
								                    }
								                });	
								           }
										}]
									
									},{
										xtype: 'fieldset',
										border: 0,
										width : 603,
										margin : '5 0 0 0',
										layout: {
											type: 'table',
											columns: 3
										},
										
										items: [{
											xtype: 'component',
											width: 514,
											height : 1,
											margin : '0 0 0 0'
										},{
											xtype: 'component',
											width: 32,
											height : 1,
											margin : '0 0 0 0'
										},{
											xtype: 'component',
											height : 1,
											width: 32,
											margin : '0 0 0 0'
										},{
											xtype : 'textfield',
								            fieldLabel : _("Hatóanyag neve"),
								            labelWidth : 110,
								            width : 514,
								            allowBlank : false,
								            name : 'dose_' + i + '_ingredientname',
								            isPrintable : true
										},{
								            xtype: 'monitorcheck',
								            disabled : true
								   		},{
											xtype: 'fielddetailsbtn'
										},{
											xtype: 'dosefield',
											name: 'dose_' + i + '_field',
											fieldLabel: _("Teljes dózis"),
											labelWidth: 110,
											width : 514,
											allowBlank : false
						               	},{
								            xtype: 'monitorcheck',
								            disabled : true
								   		},{
											xtype: 'fielddetailsbtn'
										}]
									}]
									
				});
				extraFieldContainer.down('[name=dose_' + i + '_field]').focus();
				extraFieldContainer.down('[name=dose_' + i + '_field]').validate();
				extraFieldContainer.down('[name=dose_' + i + '_ingredientname]').validate();
			}
		}
	},
		    
	items : [{
	    xtype : 'displayfield',
   		colspan: 3,
        doNotSave : true,
        fieldCls : 'forms-message-info',
        padding : '2 2 2 2',
        value : _("Kérjük, hogy az összes jelenlegi kezelést, eljárást, valamint az elmúlt 3 hónapban alkalmazott, a beteg és a vizsgálat szempontjából lényeges kezeléseket, műtéteket, egyéb eljárásokat tüntesse fel a beteg kezelései között!")
	},{

        xtype:'fieldset',
        border: 0,
        width : 710,
     	defaults : {
        	margin : '5 0 5 0',
    	 	maxWidth : 590,
    	 	allowBlank : false,
	 	 	labelWidth: 120
       	},
        items: [{
			xtype: 'component',
			width: 590,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			width: 32,
			height : 1,
			margin : '0 0 0 0'
		},{
			xtype: 'component',
			height : 1,
			width: 32,
			margin : '0 0 0 0'
		},{
            xtype : 'textfield',
            fieldLabel : _("Kezelés / gyógyszer megnevezése") + Kron.config.labelMandatoryMark,
            labelWidth : 230,
            width : 565,
            allowBlank : false,
            name : 'tradename',
            isPrintable : true,
            isQueryable : true,
            isMonitorable : true
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        },{
            xtype : 'combobox',
            name : 'treatmentType',
            fieldLabel : _("Kezelés típusa"),
            padding : '10 0 0 0',
            labelWidth : 230,
            width : 565,
            editable : false,
            enableKeyEvents: true,
            triggerAction : 'all',
            forceSelection : true,
            emptyText : _("Kezelés típusa..."),
            allowBlank : false,
            store : Ext.create('Ext.data.SimpleStore', {
                id : 0,
                fields : ['treatmentTypeId', 'treatmentTypeText'],
                data : Kron.treatmentTypeTexts
            }),
            valueField : 'treatmentTypeId',
            displayField : 'treatmentTypeText',
            mode : 'local',
            listeners : {
                // megakadályozza a backspace billentyűre a browser back-et
                keydown: function(obj, e) {"use strict";
                    if (e.getCharCode() === e.BACKSPACE) {
                        e.preventDefault();
                    }
                },
                change : function(combo, newVal, oldVal) {"use strict";
		            var form = combo.up('form');
                    //@todo rename to constant instead of numeric
                    if (newVal === 3 || newVal === 4) {// other method or other
                        form.setLinkedFieldsState(['otherTreatmentTypeDescription'], newVal);
                    } else {
                        form.setLinkedFieldsState(['otherTreatmentTypeDescription'], false);
                    }
                }
            }
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        },{
            xtype : 'textarea',
            name : 'otherTreatmentTypeDescription',
            emptyText : _("Kérem részletezze..."),
            historyLabel : _('Egyéb kezelés részletei'),
            maxLengthText : _("max 300 karakter"),
            validateMessage : _("Kérem adja meg az egyéb kezelési típus leírását."),
            padding : '10 0 0 0',
            width : 565,
            height : 48,
            maxLength : 300,
            disabled : true,

            isPrintable : true
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        },{
            xtype : 'radiogroup',
            fieldLabel : _("A készítmény"),
            columns: [190,200,60],
            name: 'medicationType',
            msgTarget : 'side',
            width : 590,
            items : [{
                name : 'medicationType',
                xtype : 'radiofield',
                boxLabel : _("Egy hatóanyagot tartalmaz"),
                inputValue : 1,
            	handler : function(field) {"use strict";
                    var form = field.up('form');
                    form.setLinkedFieldsState(['simpleDose'], this.getValue());
               	}
            }, {
                name : 'medicationType',
                xtype : 'radiofield',
                boxLabel : _("Több hatóanyagot tartalmaz"),
                inputValue : 2,
                handler : function(field) {"use strict";
                    var form = field.up('form'), me = this,
                    	deleteBtns = form.query('button[buttonType=extrafield]');
                    form.setLinkedFieldsState(['dosecontainer'], this.getValue());
                    if (deleteBtns.length>0){
                    	deleteBtns.forEach(function(btn) {
                    		if (me.getValue()) {
		                    	btn.enable();
		                    } else {
		                    	btn.disable();
		                    }
                    	});
                    }
                    
               	}
            },{
                name : 'medicationType',
                xtype : 'radiofield',
                boxLabel : _("n.a."),
                inputValue : 3
            	
            }]
	   	},{
            xtype: 'monitorcheck'
        }, {
			xtype: 'fielddetailsbtn'
        },{
            xtype : 'dosefield',
            name : 'simpleDose',
            fieldLabel : _("Teljes dózis"),
            naboxLabel :  _("Nem alkalmazható"),
            width : 580,
            allowBlank : false,
            isPrintable : true,
            disabled : true
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
		},{
			xtype: 'fieldcontainer',
			colspan: 3,
			layout: 'vbox',    
		    name: 'dosecontainer', 
            width : 655,
            maxWidth: 655,
            items:[]
		},{
			xtype: 'fieldcontainer',
            name : 'adddosebtncontainer',
			colspan: 3,
        	width : 580,
			layout: 'hbox',
			items: [{                   
                    xtype : 'button',
                    name : 'adddosebtn',
                    buttonType : 'extrafield',
                    width: 200,
                    text : _("További hatóanyag hozzáadása"),
                    handler : function(button) {
						var container = button.up('fieldset').down('[name=dosecontainer]'),
							form = button.up('form');
						form.extraFields['dose'](container);
					}
              }] 			
		},{			
	        xtype : 'combobox',
	        fieldLabel : _("Gyakoriság"),
	        name : 'frequency',
	        width : 400,
	        editable : false,
	        enableKeyEvents: true,
	        triggerAction : 'all',
	        forceSelection : true,
	        emptyText : _("Gyakoriság..."),
	        store : new Ext.data.SimpleStore({
	            id : 0,
	            fields : ['frequencyId', 'frequencyText'],
	            data : Kron.doseFrequency
	        }),
	        valueField : 'frequencyId',
	        displayField : 'frequencyText',
	        mode : 'local',
	        listeners: {
	        // megakadályozza a backspace billentyűre a browser back-et
	            keydown: function(obj, e) {
	                if (e.getCharCode() == e.BACKSPACE) {
	                    e.preventDefault();
	                }
	            }
	        }
	   
	    },{
        	
           xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        },{
         	
        	
        	
            xtype : 'textfield',
            name : 'indication',
            fieldLabel : _("Indikáció"),
            padding : '10 0 0 0',
            width : 565,
            allowBlank : false,
            isPrintable : true
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        },{
            xtype : 'combobox',
            name : 'applicationType',
            fieldLabel : _("Alkalmazás módja"),
            padding : '10 0 0 0',
            width : 400,
            editable : false,
            enableKeyEvents: true,
            triggerAction : 'all',
            forceSelection : true,
            emptyText : _("Alkalmazás módja..."),
            allowBlank : false,
            store : Ext.create('Ext.data.SimpleStore', {
                id : 0,
                fields : ['applicationTypeId', 'applicationTypeText'],
                data : Kron.applicationTypeTexts
            }),
            valueField : 'applicationTypeId',
            displayField : 'applicationTypeText',
            mode : 'local',
            listeners : {
                // megakadályozza a backspace billentyűre a browser back-et
                keydown: function(obj, e) {"use strict";
                    if (e.getCharCode() === e.BACKSPACE) {
                        e.preventDefault();
                    }
                },
                change : function(combo, newVal, oldVal) {"use strict";
                    var form = combo.up('form');
                    //@todo rename to constant instead of numeric
                    if (newVal === 10) {// other
                        form.setLinkedFieldsState(['otherApplicationTypeDescription'], newVal);
                    } else {
                        form.setLinkedFieldsState(['otherApplicationTypeDescription'], false);
                    }
                }
            }
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        },{
            xtype : 'textarea',
            name : 'otherApplicationTypeDescription',
            historyLabel : _('Egyéb alkalmazási mód részletei'),
            emptyText : _("Kérem részletezze..."),
            maxLengthText : _("max 300 karakter"),
            validateMessage : _("Kérem adja meg az 'egyéb' alkalmazási mód leírását."),
            padding : '10 0 0 0',
            width : 565,
            height : 48,
            maxLength : 300,
            disabled : true,
            isPrintable : true
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        },{
            xtype : 'notexactdate',
            name : 'startDate',
            width : 240,
            format : Kron.defaultDateFormat,
            maxValue: new Date(),
            fieldLabel : _("Kezelés kezdete"),
            padding : '10 0 0 0',
            allowBlank : false,    
           	futureDate : false,
            alertEndDateConflict : 'endDate',
            isPrintable : true
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        },{
            xtype : 'ongoingdate',
            id : 'medication_enddate',
            name : 'endDate',
            fieldLabel : _("Kezelés vége"),
            padding : '10 0 0 0',
            width : 565,
            ongoingboxLabel : _("folyamatban"),
            futureDate : false,
            alertStartDateConflict : 'startDate',
            validateMessage : _("Kérem töltse ki a 'Kezelés vége' dátumot vagy állítsa 'folyamatban'-ra."),      
            allowBlank : false,
            isPrintable : true
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        },{
            xtype : 'combobox',
            name : 'treatmentReason',
            id : 'medication_treatmentreason',
            fieldLabel : _("Kezelés oka"),
            padding : '10 0 0 0',
            width : 400,
            editable : false,
            enableKeyEvents: true,
            triggerAction : 'all',
            forceSelection : true,
            emptyText : _("Kezelés oka..."),
            allowBlank : false,
            store : Ext.create('Ext.data.SimpleStore', {
                id : 0,
                fields : ['treatmentReasonId', 'treatmentReasonText'],
                data : Kron.treatmentReasonTexts
            }),
            valueField : 'treatmentReasonId',
            displayField : 'treatmentReasonText',
            mode : 'local',
            listeners : {
                // megakadályozza a backspace billentyűre a browser back-et
                keydown: function(obj, e) {"use strict";
                    if (e.getCharCode() === e.BACKSPACE) {
                        e.preventDefault();
                    }
                },
                change : function(combo, newVal, oldVal) {"use strict";
	                var form = combo.up('form');
	                if (newVal == Kron.treatmentReasonIds.other ) {
	                	form.setLinkedFieldsState(['otherTreatmentReasonDescription'], newVal);
	                } else {
	                	form.setLinkedFieldsState(['otherTreatmentReasonDescription'], false);
	                }
            	}       
            }
            
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        },{
            xtype : 'textarea',
            name : 'otherTreatmentReasonDescription',
            emptyText : _("Kérem részletezze..."),
            historyLabel : _('Egyéb kezelési ok részletei'),
            maxLengthText : _("max 300 karakter"),
            validateMessage : _("Kérem adja meg az egyéb kezelési ok leírását."),
            padding : '10 0 0 0',
            width : 565,
            height : 48,
            maxLength : 300,
            disabled : true,
            isPrintable : true
        },{
            xtype: 'monitorcheck'
        },{
            xtype: 'fielddetailsbtn'
        }]
    },{
	        xtype : 'fieldset',
	        title : _("További információk, megjegyzések, az esemény leírása"),
	     	defaults : {
	        	margin : '5 0 5 0',
	    	 	maxWidth : 590,
	    	 	allowBlank : true
	       	},
	        items : [{
				xtype: 'component',
				width: 590,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				width: 32,
				height : 1,
				margin : '0 0 0 0'
			},{
				xtype: 'component',
				height : 1,
				margin : '0 0 0 0'
			},{
	            xtype : 'textarea',
	            name : 'comments',
	            fieldLabel : _("Megjegyzések, információk"),
	            margin : '5 0 5 0',
	            labelWidth: 170,
	            width : 560,
	            height : 48,
	            maxLength : 300,
	            maxLengthText : _("max 300 karakter"),
	            isPrintable : true
	        }, {
	            xtype: 'monitorcheck'
	        }, {
	            xtype: 'fielddetailsbtn'
	        }]
	    }]
}); 