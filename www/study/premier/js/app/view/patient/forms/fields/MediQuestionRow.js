/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MediQuestionRow.js
 *
 * Történt-e változás az előző vizithez képest az egyéb kezelésekben?
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.MediQuestionRow', {
	extend : 'Ext.container.Container',
	alias : 'widget.mediquestionrow',
	isXFormField : true,
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.ownInitComponent();
        this.callParent(arguments);
    },
    ownInitComponent : function() { "use strict";
        var me = this;
        me.items = [{
            xtype : 'btncontainer',
            controlField : this.radioGroupId,
            buttonMargin : '7 0 0 0',
            width : this.width,
            items :[{
                xtype : 'fieldset',
                title : _("Egyéb kezelések"),
                layout : 'anchor',
                width : this.width-70,
                items : [{
                    xtype : 'radiogroup',
                    id : this.radioGroupId,
                    fieldLabel : me.question||_("Történt-e változás az előző vizithez képest az egyéb kezelésekben?"),
                    labelWidth : 355,
                    flex : 1,
                    name : 'medicationChanged',
                    msgTarget : 'side',
                    items : [{
                        name : 'medicationChanged',
                        xtype : 'radiofield',
                        boxLabel : _("Igen"),
                        inputValue : 1,
                        listeners : {
                            change : function(rb, newValue, oldValue, options) {
                                if (newValue === true) {
                                    var form = this.up('form');
                                    if (form !== undefined) {
                                        form.showMedicationWarning();
                                    }
                                }
                            }
                        }
                    }, {
                        name : 'medicationChanged',
                        xtype : 'radiofield',
                        boxLabel : _("Nem"),
                        inputValue : 2
                    }],
                    allowBlank : false,
                    isPrintable : true
                }]
            }]
        }];
    }
});
