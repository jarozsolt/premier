/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * ParacetamolField.js
 *
 * Container for a Vital Sign fields
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.ParacetamolField', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.paracetamolfield',
    layout : 'hbox',
    isXFormField : true,
    validateMessage : _("Kérem töltse ki a mezőt vagy állítsa 'Nincs adat'-ra."),
    margin: '5 0 10 0',
    defaults : {
        hideLabel : true
        //padding : '5 0 5 0'
    },
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    strictInitComponent : function() { "use strict";
        var me = this;
        
        me.items = [{
            xtype : 'numberfield',
            doNotSave : true,
            width : 50,
            minValue : me.valueMin,
            maxValue : me.valueMax,
            hideTrigger : true,
            allowBlank : me.allowBlank,
            isOptional : (me.allowBlank===undefined||me.allowBlank===true)?true:false,
            fieldStyle: 'text-align:right;',
            listeners: {
                'change': function(field, newValue) {
                    var form = field.up('form'),
                    parent = this.up('fieldcontainer');
                    /*fieldDose = field.nextSibling('#dose'),
                    dose = newValue*500;
                    if (fieldDose) {
                        if (fieldDose.getValue() !== dose)
                        {
                            fieldDose.setValue(dose);
                        }
                    }*/
                    if (form && parent) {
                        if (form.paracetamolValues && parent.dayIndex) {
                            if (parent.dayIndex <= 7) {
                                if (newValue === '') {
                                    form.paracetamolValues[parent.dayIndex-1] = null;
                                }
                                else{
                                    form.paracetamolValues[parent.dayIndex-1] = newValue;
                                }
                                if (form.updateParacetamolSum !== undefined) {
                                    form.updateParacetamolSum();
                                }
                            }
                        }
                    }
                },
                'blur': function(field, newValue) {
                    var form = field.up('form'),
                    parent = this.up('fieldcontainer');
                    if (form && parent) {
                        if (parent.dayIndex === undefined || parent.dayIndex > 7) {
                            if (form.updateParacetamolWarning !== undefined) {
                                form.updateParacetamolWarning();
                            }
                        }
                    }
                }
            }
        }, {
            xtype : 'splitter',
            width : 10
        }, {
            xtype : 'displayfield',
            value : _("db")
        }, {
            xtype: 'splitter',
            width : 50
        }, {
            xtype: 'checkboxfield',
            boxLabel: me.naboxLabel||_("Nincs adat"),
            doNotSave : true,
            //width : 120,
            inputValue: 'n.a.',
            handler : function(field) {
                var valueField = this.previousSibling('numberfield'),
                form = field.up('form'),
                parent = this.up('fieldcontainer'),
                displayField = this.previousSibling('displayfield');
                if (this.getValue())
                {
                    valueField.setValue('');
                    valueField.disable(true);
                    valueField.allowBlank = true;
                }
                else
                {
                    valueField.enable(true);
                    valueField.allowBlank = valueField.isOptional;
                }
                if (form && parent) {
                    if (form.paracetamolValues && parent.dayIndex) {
                        if (parent.dayIndex <= 7) {
                            if (this.getValue()) {
                                form.paracetamolValues[parent.dayIndex-1] = -1;
                            }
                            else {
                                form.paracetamolValues[parent.dayIndex-1] = null;
                            }
                            if (form.updateParacetamolSum !== undefined) {
                                form.updateParacetamolSum();
                            }
                        }
                    }
                }
            }
        }, {
            xtype : 'splitter',
            flex : 1
        }/*, {
            xtype : 'displayfield',
            doNotSave : true,
            itemId: 'dose',
            validateOnChange: false,
            minWidth : 50,
            style: 'text-align: right;',
            value : _("0")
        }, {
            xtype : 'splitter'
        }, {
            xtype : 'displayfield',
            doNotSave : true,
            value : _("mg")
        }*/, {
            xtype: 'hiddenfield',
            name : me.name,
            parentContainer : me,
            setValue : function (value) { this.parentContainer.setValue(value); },
            getValue : function () { return this.parentContainer.getValue(); }
        }];
    },
    getValue: function() {"use strict";
        var me = this, value,
        valueField = me.down('numberfield'),
        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            return 'n.a.';
        }
        return valueField.getValue();
    },
    setValue: function(value) {"use strict";
        var me = this,
        newValue = -1,
        valueField = me.down('numberfield'),
        checkField = me.down('checkboxfield'),
        form = me.up('form'),
        parent = me.up('fieldcontainer');
        if (value === 'n.a.')
        {
            checkField.setValue(true);
            valueField.setValue('');
        }
        else if (value !== undefined)
        {
            newValue = value;
            checkField.setValue(false);
            valueField.setValue(value);
        }

        if (form && parent) {
            if (parent.dayIndex === undefined || parent.dayIndex > 7) {
                if (form.updateParacetamolWarning !== undefined) {
                    form.updateParacetamolWarning();
                }
            }
            else {
                if (form.paracetamolValues) {
                    form.paracetamolValues[parent.dayIndex-1] = newValue;
                    if (form.updateParacetamolSum !== undefined) {
                        form.updateParacetamolSum();
                    }
                }
            }
        }
        return me;
    }
});
