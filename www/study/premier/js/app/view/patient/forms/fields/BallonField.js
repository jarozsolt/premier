/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * BallonField.js
 *
 * Container for a Ballon values
 * hossz - numberfield - egész
 * átmérő - numberfield - 2 tizedes
 * nyomás - numberfield - egész
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.BallonField', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.ballonfield',
	isXFormField : true,
    layout : 'hbox',
    validateMessage : _("Kérem töltse ki a mezőt minden részét"),
    //combineErrors : true,

    defaults : {
       margin: '0 0 0 10'
    },
    items : [],
    valueSeparator :  '|',
    formattedValueSeparator : ' / ',
 //  valueFieldLabels : [_('Nem történt'), _('Pozitív'),_('Negatív')] ,
 //   secondFieldLabels : [_('Klinikailag nem jelentős'),_('Klinikailag jelentős') ],
    

    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);

    },
    strictInitComponent : function() { "use strict";
      	var me = this;

		me.items = [{
    		xtype: 'numberfield',
    		doNotSave : true,
  			labelWidth : 40,
			width: 100,
	        name: 'hosszField',
	        fieldLabel: _("hossz"),
          	decimalPrecision : 0,
	        maxValue: 9999,
	        minValue: 0 ,
	        hideTrigger: true,
	        allowBlank : me.allowBlank,
	        isOptional : (me.allowBlank===undefined||me.allowBlank===true)?true:false,
	         listeners: {
                'change': function(field, newValue) {
                    var parent = field.up('fieldcontainer');
                    if (parent.onValueChanged !== undefined) {
                        parent.onValueChanged(field, newValue);
                    }
                }
            }
    	},{
			xtype : 'displayfield',
            doNotSave : true,
            value:  _(" mm")
       	},{
    		xtype: 'numberfield',
    		doNotSave : true,
  			labelWidth : 50,
			width: 110,
	        name: 'atmeroField',
	        fieldLabel: _("átmérő"),
	        hideTrigger: true,
	        allowBlank : me.allowBlank,
	        isOptional : (me.allowBlank===undefined||me.allowBlank===true)?true:false,
          //  padding : '0 0 0 12',
	        maxValue: 999,
	        minValue: 0 ,	
            decimalPrecision : 2,
            decimalSeparator : _(","),
            listeners: {
                'change': function(field, newValue) {
                    var parent = field.up('fieldcontainer');
                    if (parent.onValueChanged !== undefined) {
                        parent.onValueChanged(field, newValue);
                    }
                }
            }
    	},{
			xtype : 'displayfield',
            doNotSave : true,
            value:  _(" mm")
       	},{
    		xtype: 'numberfield',
    		doNotSave : true,
  			labelWidth : 50,
			width: 110,
	        name: 'nyomasField',
	        fieldLabel: _("nyomás"),
	        hideTrigger: true,
	        allowBlank : me.allowBlank,
	        isOptional : (me.allowBlank===undefined||me.allowBlank===true)?true:false,
         	decimalPrecision : 0,
	        maxValue: 9999,
	        minValue: 0 ,
	        listeners: {
                'change': function(field, newValue) {
                    var parent = field.up('fieldcontainer');
                    if (parent.onValueChanged !== undefined) {
                        parent.onValueChanged(field, newValue);
                    }
                }
            }	
    	},{
			xtype : 'displayfield',
            doNotSave : true,
            value:  _(" atm")
       	},{
       		xtype: 'checkboxfield',
	        boxLabel: _("n.a."),
	        doNotSave : true,
	        width : 120,
	        inputValue: 'n.a.',
			disabled: true,
			hidden : true,
   		   	handler : function(field) {"use strict";
	            var field1 = field.up('fieldcontainer').down('numberfield'),
			        field2 = field1.nextSibling('numberfield'),
			        field3 = field2.nextSibling('numberfield');
	            if (field.getValue())
	            {
	                field1.setValue('');
	                field1.disable(true);
	                field1.allowBlank = true;
	                field2.setValue('');
	                field2.disable(true);
	                field2.allowBlank = true;
	                field3.setValue('');
	                field3.disable(true);
	                field3.allowBlank = true;
	            }
	            else
	            {
	                field1.enable(true);
	                field1.allowBlank = field1.isOptional;
	                field1.validate();
	                field2.enable(true);
	                field2.allowBlank = field2.isOptional;
	                field2.validate();
	                field3.enable(true);
	                field3.allowBlank = field3.isOptional;
	                field3.validate();
	            }
	        }
       	},{
       		xtype: 'hiddenfield',
       		name : me.name,
            parentContainer : me,
            setValue : function (value) { if(value!== undefined ) {this.parentContainer.setValue(value);} },
            getValue : function () { return this.parentContainer.getValue(); },
            getFormattedValue : function (fieldValue) { return this.parentContainer.getFormattedValue(fieldValue); },
        	reset : function () { this.parentContainer.reset(); },
    		getXType: function() {return me.xtype;}
    }];
        /*if (this.validateMessage !== undefined) {
            checkField.validateMessage = this.validateMessage;
        }*/
    //   me.on("afterrender", me.onAfterRender, me);
    },
//    onAfterRender : function() {"use strict";
    //	this.validate();
 //   },
    getValue: function() {"use strict";
        var me = this, value='',
        field1 = me.down('numberfield'),
        field2 = field1.nextSibling('numberfield'),
        field3 = field2.nextSibling('numberfield'),
        checkField = me.down('checkboxfield');;
        
        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            value = 'n.a.';
        } else {
	        if (field1.getValue() !== undefined && field1.getValue() !== null ) {
	        	value = field1.getValue();
	        }
	        value += me.valueSeparator;
	        if (field2.getValue() !== undefined && field2.getValue() !== null ) {
	        	value += field2.getValue();
	        }
	        value += me.valueSeparator;
	        if (field3.getValue() !== undefined && field3.getValue() !== null ) {
	        	value += field3.getValue();
	        }
	        if (value == me.valueSeparator + me.valueSeparator ) {
	        	value = null;
	        }
        }
        return value;
    },

   	getFormattedValue : function(fieldValue) {"use strict";
   		//console.log('ballonfield getFormattedValue', fieldValue);
   		var me = this, formattedValue='',valueTokens=[],
        field1 = me.down('numberfield'),
        field2 = field1.nextSibling('numberfield'),
      	field3 = field2.nextSibling('numberfield'),
      	checkField = me.down('checkboxfield');
      	
         if (fieldValue === 'n.a.' || (( fieldValue === undefined || fieldValue === null ) && checkField.getValue() === true ))
        {
            formattedValue = 'n.a.';
        } else {
        	
      		if (fieldValue !== undefined && fieldValue !== null && fieldValue.split !== undefined && fieldValue != '') { 
      			// ha meg van adca a fieldvalue, akkor azt mutatom meg
	        	valueTokens = fieldValue.split(me.valueSeparator);
	        } else {
	        	// ekkor a form mező értékét veszem

	        	 if (field1.getValue() !== undefined && field1.getValue() !== null ) {
		        	valueTokens[0] =  field1.getValue();
		        }
		         if (field2.getValue() !== undefined && field2.getValue() !== null ) {
		        	valueTokens[1] =  field2.getValue();
		        } 
		        if (field3.getValue() !== undefined && field3.getValue() !== null ) {
		        	valueTokens[2] =  field3.getValue();
		        }
	        } 	
	        if (valueTokens !== undefined && valueTokens !== null)	{
		    	if (valueTokens[0] !== undefined && field1 !== undefined && field1 !== null)
		        {
		            formattedValue = field1.getFieldLabel() + ":" + valueTokens[0] + ' ';
		        }
		        if (valueTokens[1] !== undefined && field2 !== undefined && field2 !== null)
		        {
		            formattedValue += field2.getFieldLabel() + ":" + valueTokens[1] + ' ';
		        }
		    	 if (valueTokens[2] !== undefined && field3 !== undefined && field3 !== null)
		        {
		            formattedValue += field3.getFieldLabel() + ":" + valueTokens[2] + ' ';
		        }        	
		    }
        	if (formattedValue == me.valueSeparator + me.valueSeparator ) {
	        	formattedValue = '';
	        }
        }
        return formattedValue;
   	},
    setValue: function(value) {"use strict";
    	//console.log('ballon setvalue',value);
        var me = this, valueTokens,
		field1 = me.down('numberfield'),
		field2 = field1.nextSibling('numberfield'),
      	field3 = field2.nextSibling('numberfield'),
      	checkField = me.down('checkboxfield');
        if (value === 'n.a.') {
            checkField.setValue(true);
            field1.setValue('');
            field2.setValue('');
            field3.setValue('');
        } else if (value === null || value == '' || value == me.valueSeparator + me.valueSeparator) {
            field1.setValue('');
            field2.setValue('');
            field3.setValue('');
			checkField.setValue(false); // jzs
        }
        else if (value !== undefined && value.split !== undefined)
        {
            valueTokens = value.split(me.valueSeparator);
            
            if (valueTokens[0] !== undefined)
            {
                field1.setValue(valueTokens[0]);
            }
            if (valueTokens[1] !== undefined)
            {
                field2.setValue(valueTokens[1]);
            }
             if (valueTokens[2] !== undefined)
            {
                field3.setValue(valueTokens[2]);
            }
            checkField.setValue(false);
        }
        if (me.onValueChanged !== undefined) {
            me.onValueChanged(field1, valueTokens[0]);
            me.onValueChanged(field2, valueTokens[1]);
            me.onValueChanged(field3, valueTokens[2]);
        }
        return me;
    },
    reset: function() {"use strict";
        var me = this,
        field1 = me.down('numberfield'),
        field2 = field1.nextSibling('numberfield'),
      	field3 = field2.nextSibling('numberfield'),
      	checkField = me.down('checkboxfield');
      	
       	field1.reset();
        field2.reset();
        field3.reset();
        checkField.reset();
        return me;
   },
   getName: function() {"use strict";
   		return this.name;
   },
   
   isValid: function() {"use strict";
        var me = this,
        	field1 = me.down('numberfield'),
	        field2 = field1.nextSibling('numberfield'),
	        field3 = field2.nextSibling('numberfield'),
	        checkField = me.down('checkboxfield');

        if (checkField.getValue() === true)
        {
            return true;
        } else {
        	//console.log(field1.isValid() && field2.isValid() && field3.isValid() );
        	return field1.isValid() && field2.isValid() && field3.isValid() ;
        	
        }
    },
    
    validate : function() {"use strict";
        var me = this,
        	field1 = me.down('numberfield'),
	        field2 = field1.nextSibling('numberfield'),
	        field3 = field2.nextSibling('numberfield'),
	        checkField = me.down('checkboxfield'),
	        returnValue = true;

        if (checkField.getValue() === true)
        {
            return true;
        } else {

        	field1.validate();
        	field2.validate();
        	field3.validate();
        	//return returnValue;
        }
    }
});
