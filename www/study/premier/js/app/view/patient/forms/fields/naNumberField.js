/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * naNumberField.js
 *
 * Container for a number filed and a 'n.a' checkbox
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.naNumberField', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.nanumberfield',
    layout : 'hbox',
    validateMessage : _("Kérem töltse ki a mezőt vagy állítsa 'Nem vizsgált'-ra."),
    isXFormField : true,
    defaults : {
        hideLabel : true,
        padding : '5 0 5 0'
    },
    items : [],

    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    strictInitComponent : function() { "use strict";
        var me = this;
        
        me.items = [{
            xtype : 'numberfield',
            doNotSave : true,
            width : 50,
            hideTrigger : true,
            minValue : me.valueMin,
            maxValue : me.valueMax,
            allowBlank : me.allowBlank,
            decimalPrecision : me.decimalPrecision,
            isOptional : (me.allowBlank===undefined||me.allowBlank===true)?true:false,
            listeners: {
                'change': function(field, newValue) {
                    var parent = field.up('fieldcontainer');
                    if (parent.onValueChanged !== undefined) {
                        parent.onValueChanged(field, newValue);
                    }
                }
            }
            // TODO: a validálást megoldani, jelenleg nem pirosít, ha nincs kitöltve
            ,
            getErrors : function(value) {//Nem használhatunk strict módot a callParent miatt
                var parent = this.up('fieldcontainer'),
                checkField = this.nextSibling('checkboxfield');
                if (parent.allowBlank === false) {
                	//console.log('getErrorsvalue', value);
                    if ((value === null || value === '') && checkField.getValue() === false) {
                        Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
                        return [parent.validateMessage];
                    }
                    Ext.form.Field.prototype.clearInvalid.call(this);
                }
                // Meghívjuk az eredeti hibakezelőt a min-max érték ellenőrzés miatt
               // return this.callParent(arguments);
                return [];
            }

        }, {
            xtype : 'displayfield',
            width : 70,
            doNotSave : true,
            unitvalue : me.unit,
            padding : '5 0 5 5',
            listeners : {
                //the ExtJS API says the render event will be called with one parameter:
                //   (Ext.Component this)
                'render' : function(thisCmp) {
                    if (thisCmp.unitvalue !== undefined) {
                        thisCmp.setValue(thisCmp.unitvalue);
                    }
                }
            } 
        }, {
            xtype: 'checkboxfield',
            boxLabel: me.naboxLabel?me.naboxLabel:_("Nem vizsgált"),
            doNotSave : true,
            width : 130,
            inputValue: 'n.a.',
           	getErrors : function(value) {"use strict";
                var parent = this.up('fieldcontainer'),
                valueField = this.previousSibling('numberfield')
                if (parent.allowBlank === false) {
                    if ((valueField.getValue() === null || valueField.getValue() === '') && value === false) {
                        Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
                        return [parent.validateMessage];;
                    }
                    Ext.form.Field.prototype.clearInvalid.call(this);
                }
                return [];
            },
            handler : function(field) {"use strict";
                var valueField = this.previousSibling('numberfield'),
                displayField = this.previousSibling('displayfield');
                if (this.getValue())
                {
                    valueField.setValue('');
                    valueField.disable(true);
                    valueField.allowBlank = true;
                }
                else
                {
                    valueField.enable(true);
                    valueField.allowBlank = valueField.isOptional;
                }
            }
        },{
            xtype: 'hiddenfield',
            name : me.name,
            parentContainer : me,
            setValue : function (value) { this.parentContainer.setValue(value); },
            getValue : function () { return this.parentContainer.getValue(); },
            getFormattedValue : function (fieldValue) { return this.parentContainer.getFormattedValue(fieldValue); },
        	reset : function () { this.parentContainer.reset(); },
    		getXType : function() {return me.xtype;}
        }];
    },
    getValue: function() {"use strict";
        var me = this, value,
        valueField = me.down('numberfield'),
        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            return 'n.a.';
        }
        return valueField.getValue();
    },
    setValue: function(value) {"use strict";
        var me = this,
        valueField = me.down('numberfield'),
        checkField = me.down('checkboxfield');
        if (value === 'n.a.')
        {
            checkField.setValue(true);
            valueField.setValue('');
        }
        else if (value !== undefined)
        {
            checkField.setValue(false);
            valueField.setValue(value);
        }
        if (me.onValueChanged !== undefined) {
            me.onValueChanged(valueField, value);
        }
        return me;
    },
    
    getName: function(){"use strict";
    	if (this.name != undefined && this.name !=null ) {
    		return this.name;
    	} else {
    		return null;
    	}
    	
    },

   	getFormattedValue : function(fieldValue) {"use strict";
		var formattedValue;
   		if (fieldValue !== undefined && fieldValue !== null ){
   		 	formattedValue = fieldValue;
   		} else {
   		 	formattedValue = this.getValue();
   		}
   		return formattedValue;
   	},
   	
   	reset: function() {"use strict";
        var me = this,
        valueField = me.down('numberfield'),
        checkField = me.down('checkboxfield');
       	valueField.reset();
        checkField.reset();
        return me;
   }
});
