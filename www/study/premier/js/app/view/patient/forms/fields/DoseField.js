/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * DoseField.js
 *
 * Container for a dose value and dose unit fields
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.forms.fields.DoseField', {
	extend : 'Ext.form.FieldContainer',
	alias : 'widget.dosefield',
    layout : 'hbox',
    validateMessage : _("Kérem töltse ki a mezőt vagy állítsa 'Nem alkalmazható'-ra."),
    //combineErrors : true,
    valueSeparator :  '|',
    formattedValueSeparator : ' ',
    isXFormField : true,
    defaults : {
        hideLabel : true
    },
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);

    }, 
    strictInitComponent : function() { "use strict";
        var me = this,
			checkField;
        
        me.items = [{
	        xtype : 'numberfield',
	        doNotSave : true,
	        padding : '5 0 5 0',
	        width : 50,
	        hideTrigger : true,
	        getErrors : function(value) {"use strict";
	            var parent = this.up('fieldcontainer'),
	            checkField = this.nextSibling('checkboxfield');
	            if (parent.allowBlank === false) {
	                if ((value === null || value === '') && checkField.getValue() === false) {
	                    Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
	                    return [parent.validateMessage];
	                }
	                Ext.form.Field.prototype.clearInvalid.call(this);
	            }
	            return [];
	        }
	    
	    },{
	        xtype : 'combobox',
         	padding : '5 0 5 20',
	        doNotSave : true,
	        width : 100,
	        editable : true, 
	        enableKeyEvents: true,
	        triggerAction : 'all',
	        forceSelection : true,
	        emptyText : _("Egység..."),
	        store : new Ext.data.SimpleStore({
	            id : 0,
	            fields : ['unitId', 'unitText'],
	            data : Kron.doseUnits
	        }),
	        valueField : 'unitId',
	        displayField : 'unitText',
	        mode : 'local',
	        getErrors : function(value) {"use strict";
	            var error = false,
	            parent = this.up('fieldcontainer'),
	            checkField = this.nextSibling('checkboxfield');
	            if (parent.allowBlank === false) {
	                if ((value === null || value === '') && checkField.getValue() === false) {
	                    Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
	                    return [parent.validateMessage];
	                }
	                Ext.form.Field.prototype.clearInvalid.call(this);
	            }
	            return [];
	        },
	        listeners: {
	        // megakadályozza a backspace billentyűre a browser back-et
	            keydown: function(obj, e) {
	                if (e.getCharCode() == e.BACKSPACE) {
	                    e.preventDefault();
	                }
	            }
	        }
	   
	    }, {
	        xtype: 'checkboxfield',
	        boxLabel: _("Nem alkalmazható"),
	        doNotSave : true,
	        width : 120,
	        padding : '5 0 5 20',
	        inputValue: 'n.a.',
	        getErrors : function(value) {"use strict";
	            var parent = this.up('fieldcontainer'),
	            valueField = this.previousSibling('numberfield'),
	            unitField = this.previousSibling('combobox');
	            if (parent.allowBlank === false) {
	                if ((valueField.getValue() === null || valueField.getValue() === '' || unitField.getValue() === null || unitField.getValue() === '')
	                    && value === false) {
	                    Ext.form.Field.prototype.markInvalid.call(this, parent.validateMessage);
	                    return [parent.validateMessage];
	                }
	                Ext.form.Field.prototype.clearInvalid.call(this);
	            }
	            return [];
	        },
	        handler : function(field) {"use strict";
	            var valueField = this.previousSibling('numberfield'),
	            unitField = this.previousSibling('combobox');
	            if (this.getValue())
	            {
	                valueField.setValue('');
	                valueField.disable(true);
	                unitField.setValue(null);
	                unitField.disable(true);
	            }
	            else
	            {
	                valueField.enable(true);
	                unitField.enable(true);
	            }
	        }
	    },{
	        xtype: 'hiddenfield',
	        name : me.name,
            parentContainer : me,
            setValue : function (value) { this.parentContainer.setValue(value); },
            getValue : function () { return this.parentContainer.getValue(); },
            isValid : function(){return this.parentContainer.isValid(); },
            getFormattedValue : function (fieldValue) { return this.parentContainer.getFormattedValue(fieldValue); }
         //   reset : function () { this.parentContainer.reset(); }
	    }];
	},
   
  /*  strictInitComponent : function() { "use strict";
  //      var me = this,
        checkField = this.down('checkboxfield'),
        hiddenField = this.down('hiddenfield');
        hiddenField.name = me.name;
        hiddenField.parentContainer = me;
        hiddenField.setValue = function (value) { this.parentContainer.setValue(value); };
        hiddenField.getValue = function () { return this.parentContainer.getValue(); };
        hiddenField.reset = function () { this.parentContainer.reset(); };
        if (this.naboxLabel !== undefined) {
            checkField.boxLabel = this.naboxLabel;
        }
        /*if (this.validateMessage !== undefined) {
            checkField.validateMessage = this.validateMessage;
        }*//*
    },*/
    getValue: function() {"use strict";
        var me = this, value = '',
        valueField = me.down('numberfield'),
        unitField = me.down('combobox'),
        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            return 'n.a.';
        }
        if ( valueField.getValue() !== undefined && valueField.getValue() !== null) {
        	value = '' + valueField.getValue();
        }
        if ( unitField.getValue() !== undefined && unitField.getValue() !== null) {
        	value += me.valueSeparator+unitField.getValue();
        }
        return value;
    },
    setValue: function(value) {"use strict";
        var me = this, valueTokens, comboValue,
        valueField = me.down('numberfield'),
        unitField = me.down('combobox'),
        checkField = me.down('checkboxfield');
        if (value === 'n.a.')
        {
            checkField.setValue(true);
            valueField.setValue('');
            unitField.setValue(null);
        }
        else if (value === null  || value === '')
        {
            checkField.setValue(false);
            valueField.setValue('');
            unitField.setValue(null);
        }
        else if (value !== undefined) {
        	if ( value.split === undefined) {
        		value = '' + value;
        	}
            checkField.setValue(false);
            valueTokens = value.split(me.valueSeparator);
            if (valueTokens[0] !== undefined)
            {
                valueField.setValue(valueTokens[0]);
            }
            if (valueTokens[1] !== undefined)
            {
                comboValue = valueTokens[1];//parseInt(valueTokens[1], 10);
                unitField.setValue(comboValue);
            }
        }
        return me;
    },
    reset: function() {"use strict";
        var me = this,
        valueField = me.down('numberfield'),
        unitField = me.down('combobox'),
        checkField = me.down('checkboxfield');
        valueField.reset();
        unitField.reset();
        checkField.reset();
        return me;
    },
    
    getName: function(){"use strict";
    	if (this.name !== undefined && this.name !== null ) {
    		return this.name;
    	} else {
    		return null;
    	}
    	
    },
    isValid: function() {"use strict";
        var me = this,
        valueField = me.down('numberfield'),
        unitField = me.down('combobox'),
        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            return true;
        } else {
        	
        	return valueField.isValid() && unitField.isValid();
        }
    },
    validate: function() {"use strict";
        var me = this,
        valueField = me.down('numberfield'),
        unitField = me.down('combobox'),
        checkField = me.down('checkboxfield');
        if (checkField.getValue() === true)
        {
            return true;
        } else {
        	valueField.validate();
        	unitField.validate();
        }
    },
    getFormattedValue : function(fieldValue) {"use strict";
   		//console.log('getFormattedValue', fieldValue);
   		var me = this, formattedValue='',valueTokens, store, rec, unitText,
	        field1 = me.down('numberfield'),
	        field2 = me.down('combobox'),
	        checkField = me.down('checkboxfield');
	        
        store = field2.getStore(); 
        if (fieldValue !== undefined && fieldValue !== null && fieldValue.split !== undefined && fieldValue != '') { // ha meg van adca a fieldvalue, akkor azt mutatom meg
        	if (fieldValue === 'n.a.') {
        		 formattedValue = fieldValue;
        	} else {
	        	valueTokens = fieldValue.split(me.valueSeparator);
	        	if (valueTokens[0] !== undefined)
	            {
	                formattedValue = valueTokens[0];
	            }
	            if (valueTokens[1] !== undefined)
	            {
	            	rec = store.findRecord('unitId', valueTokens[1]);
	            	if (rec) {
	            		unitText = rec.get('unitText');
	            	} else {
	            		unitText = '';
	            	}
	                formattedValue = formattedValue + me.formattedValueSeparator + unitText;
	            }
        	}
       	} else {// ekkor a form mező értékét veszem
       		 
	        if (checkField.getValue() === true)
	        {
	            formattedValue = 'n.a.';
	        } else {
	        	if (field1.getValue() !== undefined && field1.getValue() !== null ) {
		        	formattedValue = field1.getValue();
		        }
		        if (field2.getValue() !== undefined && field2.getValue() !== null ) {
		        	rec = store.findRecord('unitId', field2.getValue());
	            	if (rec) {
	            		unitText = rec.get('unitText');
	            	} else {
	            		unitText = '';
	            	}
	                formattedValue = formattedValue + me.formattedValueSeparator + unitText;
		        }
		    }
        } 
       
        return formattedValue;
   	}
});
