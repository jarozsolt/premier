/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * PatientDialog.js
 *
 * PatientDialog- general dialog window for all visits
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

/*
Ext.require([
		'Kron.view.patient.forms.Visit1Form', 
		'Kron.view.patient.forms.Visit2Form',
		'Kron.view.patient.forms.Visit3Form',
		'Kron.view.patient.forms.Visit4Form',
		'Kron.view.patient.forms.UnscheduledVisitForm',
		'Kron.view.patient.forms.MedicationForm',
		'Kron.view.patient.forms.AdverseEventForm',
		'Kron.view.patient.forms.MedicalHistoryForm',
		'Kron.view.patient.forms.QueryForm']);*/

Ext.override(Ext.layout.Layout, {
	renderItem : function(item, target, position) {
		if(item && item.isFieldLabelable && !item.rendered && item.fieldLabel && item.allowBlank === false) {
			if (item.fieldLabel.lastIndexOf(Kron.config.labelMandatoryMark) === -1) {
				item.fieldLabel += Kron.config.labelMandatoryMark;
			}
		}
		this.callOverridden(arguments);
	}
});

Ext.define('Kron.view.patient.PatientDialog', {
	extend : 'Ext.window.Window',
	alias : 'widget.patientdlg',
	id : 'patientdlg',
	requires: [
        'Kron.view.patient.forms.MedicalHistoryForm',
        'Kron.view.patient.forms.Visit1Form',
        'Kron.view.patient.forms.Visit2Form',
        'Kron.view.patient.forms.Visit3Form',
        'Kron.view.patient.forms.Visit4Form',
        'Kron.view.patient.forms.UnscheduledVisitForm',
        'Kron.view.patient.forms.MedicationForm',
        'Kron.view.patient.forms.AdverseEventForm',  
        'Kron.view.patient.forms.AdverseEventMACCEForm',     
        'Kron.view.patient.forms.QueryForm',
        'Kron.view.patient.forms.GeneralPatientForm',
        'Kron.view.patient.FormFieldsGrid',
        'Kron.core.Alerts'
        ],
		
	modal : true,
	closable : true,
	closeAction : 'hide',
	resizable : true,
	plain : true,
	iconCls : 'icon-medication-dlg_small',
	width : 500,
//	height : 550,
	border : false,
	layout : 'card',
	activeItem : 0,

	deferredRender : true,

	blockErrorChange : false,
	//enableKeyEvents: true,
	
	/**
	 * ebben van az aktuális form-nak megfelelő forms store rekord 
	 */
	record : {},
	//formHistory : null, // ebben van eltárolva az aktív formra lekérdezett field history 
	/**
	 * Ha read only-ra állítom a dlg form mezőket, akkor állítom ezt true-ra
	 */
	isDlgReadOnly : false,
	/**
	 * Mentéskor kikapcsolom, hogy ha később ütne be az autosave-et indító event, ne fusson le
	 */
	autosaveEnabled : false,
	
	setAutosaveEnabled : function (value) {"use strict";
		this.autosaveEnabled = value;
	},
	
	getAutosaveEnabled : function () {"use strict";
		return this.autosaveEnabled;
	},
	
	
	dockedItems : [{
		id : 'buttonBar',
		dock : 'bottom',
		xtype : 'form',
		buttonAlign : 'left',
		buttons : [{
			xtype: 'tbfill',
			id : 'dummyfiller'
		}]
	}],
	

		// call selected form's aftershow function to mark queried fields
	afterShow : function(){
		if (this.record !== null && this.record.data.formtype !== null)
		{
			var currentForm = this.items.get(this.record.data.formtype + 'Form');
			currentForm.afterShow();
		}
	},
	
	
	initComponent : function() { // Do not use "use strict" on functions containing callParent!
	    this.callParent(arguments);
	    this.strictInitComponent();
    },
    
    strictInitComponent : function() { "use strict";
        var saveButton, cancelButton, closeButton, signButton, monitorallButton, permissionClass,patientView,patientRecord,
        	isPatientClosed = false,
        	dummy = Ext.getCmp('dummyfiller');
        	
        if (dummy !== undefined && dummy !== null)
        {
            this.buttonBar = dummy.ownerCt;
        }
        if (this.buttonBar !== undefined && this.buttonBar !== null)
        {
            //Meg kell néznem, lezárt-e a beteg, mert akkor nincs OK/Cancel gomb
            patientView=Ext.getCmp('patientview');
	       	if (patientView!== undefined) {
	       		patientRecord = patientView.getPatientRecord()	
	       	}
	        if (patientRecord !== undefined ) { 
	        	if ( patientRecord.get('isclosed')  ) {
	        		isPatientClosed = true;
	        	} else {
	        		isPatientClosed = false;
	        	}
	        }	
            //ha a beteg readonly, akkor hiába van joga a usernek módosítani, a close gomb kell, ezért létrehozom, és majd elhide-olom, ha nem kell
			closeButton = Ext.create('Ext.Button', {
                text : _("Bezárás"),
                minWidth : 90,
                scale : 'medium',
                iconCls : 'icon-cancel-btn_medium',
                id : 'dlgClose'
            });
            this.buttonBar.insert(0, closeButton);
            permissionClass = "Forms";
            if (Kron.core.UserManagement.hasPermission(permissionClass, "createForm") ||
                Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
            {
                saveButton = Ext.create('Ext.Button', {
                    text : _("OK"),
                    scale : 'medium',
                    minWidth : 90,
                    iconCls : 'icon-save-btn_medium',
                    id : 'dlgAccept',
                    formBind : true
                });
                cancelButton = Ext.create('Ext.Button', {
                    text : _("Mégsem"),
                    minWidth : 90,
                    scale : 'medium',
                    iconCls : 'icon-cancel-btn_medium',
                    id : 'dlgCancel'
                });
                this.buttonBar.insert(0, saveButton);
                this.buttonBar.insert(1, cancelButton);
                if (isPatientClosed) {
                	saveButton.hide();
                	cancelButton.hide();
                } else {
                	closeButton.hide();
                }
            }

            if (Kron.core.UserManagement.hasPermission(permissionClass, "X_signButton"))
            {
                signButton = Ext.create('Ext.Button', {
                    text : _("Aláírás"),
                    scale : 'medium',
                    minWidth : 90,
                    iconCls : 'icon-sign-btn_medium',
                    id : 'dlgSign'
                });
                this.buttonBar.add(signButton);
                if (isPatientClosed) {
                	saveButton.hide();
                }
            }
            if (Kron.core.UserManagement.hasPermission(permissionClass, "monitorForm")) {
                monitorallButton = Ext.create('Ext.Button', {
                    text : _("Monitorozva"),
                    minWidth : 100,
                    scale : 'medium',
                    iconCls : 'icon-monitorall-btn_medium',
                    id : 'dlgMonitorAll'
                });
                this.buttonBar.add(monitorallButton);
                if (isPatientClosed) {
                	monitorallButton.hide();
                }
            }
        }  
        ///////////////////////////////////////////////////////
        // @todo delete, ha már nem kell
        var map = new Ext.util.KeyMap({
            target: Ext.get(document),
            binding: [{
                key: "f",
                ctrl:true,
                shift:true,
                fn: function(){ 
                    var patientDlg = Ext.getCmp('patientdlg'), store;
                    if (patientDlg && patientDlg.getLayout().getActiveItem()) {
                        if (patientDlg.getLayout().getActiveItem().id !== 'formfieldsgrid') {
                            var grid = patientDlg.getComponent('formfieldsgrid');
                            if(!grid){
                                store = Ext.data.StoreManager.lookup('FormFields'); 
                                if (!store) {
                                    store = Ext.create('Kron.store.FormFields');
                                }
                                grid = Ext.create('Kron.view.patient.FormFieldsGrid');
                                patientDlg.items.add(grid);
                            }
                            
                            grid.getStore().loadRecords(patientDlg.getRecord().formfields().getRange());
                            patientDlg.getLayout().setActiveItem('formfieldsgrid');
                       } else {
                           var formId = patientDlg.getRecord().data.formtype + 'Form';
                           patientDlg.getLayout().setActiveItem(formId);
                       }
                    }
                }
            }]
        });
        ///////////////////////////////////

    },
    
    
    
	
	setRecord : function(record, fieldName, showQueries, isRO, exceptions) {"use strict";
		var alsoTempItems = false;
		this.record = record;
	//	console.log('PatientDialog setRecord record', record);
	//	console.log(record);
		
		if(record.get('instanceid') !== null && record.get('formtype') !== null) {
			if (record.hasTempField()) {
				 Ext.MessageBox.prompt({
	                title : _("Félbehagyott űrlap"),
	                msg : _("Az űrlap el nem mentett módosításokat tartalmaz. Betöltsem ezeket a módosításokat?"),
	                buttons : Ext.Msg.YESNOCANCEL,
	                buttonText: {yes: _("Igen"), no: _("Nem"), cancel: _("Mégsem")},
	                icon : Ext.MessageBox.QUESTION,
	                scope : this,
	                fn : function(btn, ev) {
	                    if (btn === 'yes') {
                            alsoTempItems = true;
	                        this.showDialog(record, fieldName, showQueries, alsoTempItems, isRO, exceptions);
	                        return false;
	                    }
	                    if (btn === 'no') {
							alsoTempItems = false;
							record.cancelTempItems();
	                        this.showDialog(record, fieldName, showQueries, alsoTempItems, isRO, exceptions);
	                        return false;
	                    }
	                    // Cancel
	                    return false;
	                }
	            });
			} else {
				this.showDialog(record, fieldName, showQueries, alsoTempItems, isRO, exceptions);
			}
		}
	},
	patientDialogLoadMask : null,
/**
 * a patientDialog-ot megjelenítő metódust hívja, előtte/utána a loading maskolást végzi
 */	
	showDialog : function(record, fieldName, showQueries, alsoTempItems, isRO, exceptions) {"use strict";
        var me = this,
        	viewport = Ext.getCmp('viewport');
		// a köv sor azért kell, hogy az errorchange event fv-ét elindítsam/leállítsam, mert ez akkor is jön ,amikor bezárom a dialogust
	//	this.blockErrorChange = false;

        // loading mask megjelenítése  
		if (Kron.startingMaskRemoved) {
			if (viewport) {
				if( me.patientDialogLoadMask === undefined || me.patientDialogLoadMask === null) {
         			me.patientDialogLoadMask = new Ext.LoadMask(viewport, {msg:_("Betöltés...")});
         		}
	            me.patientDialogLoadMask.show();				 	
			 }
		}
        // a defer azért kell, mert a javascript egy szálas, nem tud asszinkron menni,azaz ha szinkron hívom a maskolás után az időigényes DOM műveleteket, akkor csak az után jelenítené meg, nem előtte
        Ext.defer(function() {
            me.maskedShowDialog(record, fieldName, showQueries, alsoTempItems, isRO, exceptions)
			// loading mask leszedése
			if( me.patientDialogLoadMask !== undefined && me.patientDialogLoadMask !== null) {
				me.patientDialogLoadMask.hide();	
			}
        }, 10);
	},
 
/**
 * a patientDialog-ot jeleníti meg 
 */	  
	maskedShowDialog : function(record, fieldName, showQueries, alsoTempItems, isRO, exceptions) {"use strict";
	   //console.time('maskedshowDialog timer');
		var patientView, patientRecord, fieldValues, formwarning, field, isFromValid = false,
			formId, newForm, form, loadedStores = 0, tabPanel, fieldQueriesStoreLoaded = false,
        radioGrpValue = new Object(),
        fieldQueriesStore = Ext.data.StoreManager.lookup('FieldQueries');		
        fieldQueriesStore.proxy.setExtraParam('form_instanceid',record.get('instanceid'));
        fieldQueriesStore.load({
            scope: this,
            callback: function(records, operation, success) {
                if(success) {
                    if( showQueries && form ) {
                         this.clickFieldDetailsBtn(form, fieldName);
                    } else {
                        fieldQueriesStoreLoaded = true;
                    }
                    
                }
            }
        });		
        Ext.suspendLayouts();
        
		// FormId-t a formtype-ból képzem!!!
		formId = record.data.formtype + 'Form';
		if(!this.getComponent(formId)){
	//console.log('formId',formId);
			//console.time('form megkreálás');
			// nem használt: Ext.suspendLayouts();
			newForm = Ext.create('Kron.view.patient.forms.' + formId);
			// nem használt Ext.resumeLayouts(true);
			//console.timeEnd('form megkreálás');

			this.items.add(newForm);

		}
		
		this.getLayout().setActiveItem(formId);
		form = this.items.get(formId);
		
		fieldValues = record.getFieldValues(alsoTempItems);
		
		// ide jön az extra mezők kitörlése, majd hozzáadása a formhoz
		
		form.initForm(fieldValues);
	
		

        // Csoportosítjuk a Warningokat, hog yegyszerre jöjjenek fel
        formwarning = Ext.getCmp('formwarning');
        if (formwarning !== undefined) {
            formwarning.startWarningGroup();
        }
        
    /*    
		this.blockErrorChange = true;
        form.query('panel').forEach(function(item) {
        	if (item.errorFields !== undefined) {
        		item.errorFields = [];
        		if (item.iconCls !== undefined) {
					item.setIconCls(undefined);	
				}
			}
        });*/
		//console.time('delete fields and set value');
        // alábbi a trackResetOnload = true miatt van, hogy a reset töröljön:
        
        form.getForm().getFields().each(function(f){
            if (f.xtype !== 'displayfield' && !f.doNotSave) {
            //	f.suspendEvent('change');
                if (f.xtype === 'radiofield') {
                    if ( fieldValues[f.name] !== undefined && f.inputValue == fieldValues[f.name] ) {
                        f.setValue(true);
                    } else {
                        f.setValue(false);
                    }
                    
                } else if (f.xtype !== 'radiogroup') {
                    if ( fieldValues[f.name] !== undefined ) {
                        f.setValue(fieldValues[f.name]);
                    } else {
                        f.setValue(null);
                    }
                }
        //        f.resumeEvent('change');
                
                
            }
        });
        
      

       // console.timeEnd('delete fields and set value');
        //console.time('resetOriginalValue()');
        form.getForm().getFields().each(function(f){
            if(f.xtype!=='displayfield' ) {
               f.resetOriginalValue();
            }
        });
      // console.timeEnd('resetOriginalValue()');
	
			
	//	console.time('showFieldBtns');
		this.showFieldBtns(form, record); 
		//console.timeEnd('showFieldBtns');


		
		//console.time('set the form sizes, title and icons timer');
		//Set the form sizes, title and icons
		if (form !== undefined) {
            if (form.formWidth !== undefined) {
                this.width = form.formWidth;
            }
            if (form.formHeight !== undefined) {
                this.height = form.formHeight;
            }
            if (form.formTitle !== undefined) {

            	if ((record.get('formtype') === 'AdverseEvent' || record.get('formtype') === 'AdverseEventMACCE') && record.get('parentid') != 0  ) {
	            	var aeGrid = Ext.getCmp('aegrid');
	            	if ( aeGrid !== undefined) {
	            		var realParentRecord = aeGrid.store.getAt(aeGrid.store.find('instanceid', record.get('parentid')));
	            		if (realParentRecord !== undefined ) {
	            			var parentTitle = realParentRecord.getFieldValue('diagnosis');
	            			if (parentTitle === undefined ) {
	            				 parentTitle = Kron.macceTypeTexts[realParentRecord.getFieldValue('macceType')-1];
	            			}
	            			this.setTitle(Ext.String.format(_("{0} Follow-Up"), parentTitle ));
	            		} else {
	            			this.setTitle(_('Follow-Up'));
	            		}
	            	} else {
	            		this.setTitle(_('Follow-Up'));
	            	}
            	} else {
                	this.setTitle(form.formTitle);
            	}

            }
            if (form.formIconCls !== undefined) {
            	if ((record.get('formtype') === 'AdverseEvent' || record.get('formtype') === 'AdverseEventMACCE') && record.get('parentid') != 0) {
            		this.setIconCls('icon-followup-dlg_small');
            	} else {
            		this.setIconCls(form.formIconCls);
            	}
                
            }
        }

        //console.timeEnd('set the form sizes, title and icons timer');
     //   form.initForm();
		//console.time('buttons');
		// nyomogombok láthatóságát állítom itt be:
		patientView=Ext.getCmp('patientview');
       	if (patientView!== undefined) {
       		patientRecord = patientView.getPatientRecord()	
       	}
        if (patientRecord !== undefined ) { 
        	if ( patientRecord.get('isclosed')  ) {
        	 	isRO = true;
            } 
        }
        var permissionClass = "Forms";
        if (isRO === undefined) {
        	if (Kron.core.UserManagement.hasPermission(permissionClass, "createForm") ||
                Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
            {
            	isRO = false;
            } else {
            	isRO = true;
            }
            
        }
        // ez a validate indítja az error change eventeket
        isFromValid = form.getForm().isValid();
        
		// ha van signButton (jogosultságfüggő) akkor csak akkor jelenítem meg, ha valid a form, és a form nem readonly
		var signButton=Ext.getCmp('dlgSign');
		if (signButton !== undefined && signButton !== null )
		{
		    if (isFromValid && !record.hasOpenOrAnsweredQuery() && !isRO && !record.isSigned()) {
				signButton.show();
		    } else {
			   signButton.hide();
			}
		}
		// Ha readonly a form, és van OK, cancel gomb, azokat hideolom, a bezár gombot pedig megjelenítem
		var saveButton=Ext.getCmp('dlgAccept');
		if (saveButton !== undefined && saveButton !== null)
		{
		    if (!isRO && saveButton.isHidden()) {
				saveButton.show();
		    } else if (isRO && saveButton.isVisible()){
			   saveButton.hide();
			}
		}
		var cancelButton=Ext.getCmp('dlgCancel');
		if (cancelButton !== undefined && cancelButton !== null)
		{
		    if (!isRO && cancelButton.isHidden()) {
				cancelButton.show();
		    } else if (isRO && cancelButton.isVisible()){
			   cancelButton.hide();
			}
		}		
		var closeButton=Ext.getCmp('dlgClose');
		if (closeButton !== undefined && closeButton !== null)
		{
		    if (!isRO && closeButton.isVisible()) {
				closeButton.hide();
		    } else if (isRO && closeButton.isHidden()){
			   closeButton.show();
			}
		}
		// ha van monitor all gomb, azt is eltüntetem, ha a beteg lezárt
		var monitorallButton=Ext.getCmp('dlgMonitorAll');
		if (monitorallButton !== undefined && monitorallButton !== null)
		{
		    if (patientRecord.get('isclosed')) {
				monitorallButton.hide();
		    } else {
			   monitorallButton.show();
			}
		}

		//console.timeEnd('buttons');
        
        //console.time('setReadOnly timer');   
		this.setReadOnly(isRO, exceptions);
		// Ha readonly, jelzem ezt a headerben
		if (this.isDlgReadOnly){
			if (form.formTitle !== undefined) {
                this.setTitle( form.formTitle + " - " + _("csak olvasható") );
            }
       	}
       // console.timeEnd('setReadOnly timer');
        
        Ext.resumeLayouts(true);
        
        //console.time('setAutosaveEnabled');
		this.setAutosaveEnabled(true);
		//console.timeEnd('setAutosaveEnabled');
		//console.time('megjelenítés');
		this.show();
		//console.timeEnd('megjelenítés');
		//console.timeEnd('első field');
	


		this.toFront();	
		
		form.onAfterFormLoad(fieldValues);
		
        // Megmutatjuk az összes Warningot egyszerre (ha volt)
        if (formwarning !== undefined) {
            formwarning.stopWarningGroup();
        }
		
		// ráállok az megadott, vagy az első field-re
		if (fieldName !== null && fieldName !== undefined && fieldName !== '') {
			//field = this.getLayout().getActiveItem().getForm().findField(fieldName);
			field = this.getLayout().getActiveItem().down('[name=' + fieldName + ']');
        } else {
			field = this.getLayout().getActiveItem().getForm().getFields().getAt(0);
		}
	    if (field) {

	        tabPanel = field.up('tabpanel');
	        if (tabPanel!== undefined && tabPanel!== null) {
	            tabPanel.setActiveTab(field.up('panel'));
	        }
			field.focus(false);
			if (showQueries && fieldQueriesStoreLoaded) {
			   this.clickFieldDetailsBtn(form, fieldName);
			}
		}
		//console.timeEnd('első field');
		// start autosave timer
		if (!this.isDialogReadOnly()) {
		    this.startAutosaveTimer();
		}
		//console.timeEnd('maskedshowDialog timer');
		//this.blockErrorChange = false;
/*		 form.query('panel').forEach(function(item) {
        	if (item.errorFields !== undefined) {
        		console.log(item.title, item.errorFields);
			}
		});*/
	},
	
	startAutosaveTimer : function() {"use strict";
	   var userPrefsView = Ext.getCmp('userprefs'),
            intervalMinute = userPrefsView.getAutosaveFrequency();
        if (intervalMinute !== undefined && intervalMinute>=1){
            Ext.TaskManager.start({ 
                run: this.doAutoSave,
                interval: 1000 * 60 * intervalMinute, // convert to msec...
                scope: this
           });
       //    console.log('Autosave Started', intervalMinute);
        }
	},
	
	stopAutosaveTimer : function() {"use strict";
	    Ext.TaskManager.stopAll();
	  //  console.log('Autosave stopped');
	},
	
	doAutoSave: function() {"use strict";
      //  console.log('doAutoSave called');
        var record = this.getRecord(),
            userPrefsView = Ext.getCmp('userprefs');
        if (!this.isDialogReadOnly() && this.getAutosaveEnabled() && userPrefsView.isAutosaveEnabled()) { // ha read only, nem csinálok semmit
            record.formfields().sync({
                failure: function() { Kron.core.Alerts.connectionErrorShort();
                }
            });
        }
    },
    
	clickFieldDetailsBtn : function(form, fieldName) {"use strict";
	    var fieldDetailsBtn;
	    if (form) {
	        form.cascade(function(c){
                if (c.getXType()==='fielddetailsbtn') {
                    if (c.getFieldName() == fieldName) {
                        c.fireEvent('click', c);
                        
                    } 
                }
            });
        }
	},
	
/**
 * visszaadja a patientDialogban eltárolt form rekordot 
 */
	getRecord : function() {"use strict";
		return this.record;
	},
	
/**
 * Minden elemet readonly-ra állít az aktív formon, kivéve a megadott kivételeket
 * @param {Object} isRO
 * @param {Object} exceptions
 */
    setReadOnly : function(isRO, exceptions) { "use strict";
        var exceptxtypes = [], i, isexception, patientView, patientRecord;
        if (isRO === undefined || isRO === undefined ) {
            if (Kron.core.UserManagement.hasPermission("Forms", "updateForm")) {
                isRO = false;
            } else {
                isRO = true;
            }
        }
        // ha a beteg le van zárva, akkor mndenképp readonly lesz a form
        
       	patientView=Ext.getCmp('patientview');
       	if (patientView!== undefined) {
       		patientRecord = patientView.getPatientRecord()	
       	}
        if (patientRecord !== undefined ) { 
        	if ( patientRecord.get('isclosed')  ) {
        	 	isRO = true;
            } 
        }
        // kivételek kezelése
       if (exceptions !== undefined && exceptions !== null)
        {
            exceptxtypes = [].concat(exceptions);
        }
        
        Ext.suspendLayouts();
        this.getLayout().getActiveItem().getForm().getFields().each(function(item) {
            isexception = false;
            for(i = 0; i < exceptxtypes.length; i+=1) {
                if(exceptxtypes[i] === item.xtype) {
                    isexception = true;
                    break;
                }
            }
            if (!isexception && !item.readOnly) {
                item.setReadOnly(isRO);
            }
            /*if (exceptxtypes.indexOf(item.xtype) === -1)
            {
                item.setReadOnly(isRO);
            }*/
		});
		// extra fielad hozzáadása , törlése gombokat inaktiválni kell, Ezek azok a buttonok  amiknek buttonType : 'extrafield',
		var extraButtons = this.getLayout().getActiveItem().query('button[buttonType=extrafield]');
	//	console.log('extraButtons',extraButtons);
		extraButtons.forEach(function(item) {
			if (isRO && !item.isDisabled()) {
				item.setDisabled(isRO);
			} else if (!isRO && item.isDisabled()) {
				item.setDisabled(isRO);
			}
		});
	 	Ext.resumeLayouts(true);
		this.isDlgReadOnly = isRO;
	},
/**
 * visszaadja, hogy volt-e readonly-ra beállítva az aktív form 
 */
	isDialogReadOnly: function() { "use strict";
        return this.isDlgReadOnly;
	},

/**
 * Ha a fieldvalue == undefined - akkor még nics a storeban semmi - ekkor a form értékeét veszem
 *
 *	+ comment - radiofilednal ha nincs boxlabel, létrehoztam egy rediofieldVlaue attributumot, amibe be lehet tenni, hogy mit adjon vissz aa query ablakban, mint rediobutton option érték 
 */
    getFormattedFieldValue: function(fieldName, fieldValue) { "use strict";
        // change history field values in case of combos, etc. 
  //     console.log('patientdlg getFormattedFieldValue', fieldName, fieldValue);
        var returnValue = '', record,
            form=this.getLayout().getActiveItem(),
			field = form.down('[name=' + fieldName + ']');
		if (field.xtype === 'radiofield' ) {
			field = field.up('radiogroup[name=' + fieldName + ']');
			console.log('radiofield-et talált a name-re componens keresés');
		}
            //field=form.getForm().findField(fieldName);
		if (field !==  null) { // csak akkor adok vissza értéket, ha megvan a field
       // 	console.log('field.getXType()',field.getXType());
       		if ( field.getFormattedValue )  {
	            	returnValue = field.getFormattedValue(fieldValue);
	        } else {
	        	
				// nincs nekijje getFormattedValue-ja, akkor itt kiszámolom
	       		if (fieldValue === undefined) { // ilyenkor a formból vezsem az értéket
	       			if (field.getXType() === 'radiogroup') {
	       				// itt objectet kapok, de mindig csak 1 lehet kiválasztva, így az első elem értékét veszem:
	       				//console.log(field.getValue());
	       				fieldValue = Ext.Object.getValues(field.getValue())[0];
	       				
	       			} else {
	       				fieldValue = field.getValue();
	       			}
	       	//		console.log("formból kiszedett érték",fieldValue );
	       		}
       		
	       		if (fieldValue !== undefined) {
	           		
	           		if (field.getXType() === 'combobox') {
	           			if ( field.valueField == field.displayField) {
	           				returnValue = fieldValue;
	           			} else {
	           				returnValue = field.getStore().findRecord(field.valueField, fieldValue ).get(field.displayField); 
	           			}
	                    
		            } else if (field.getXType() === 'radiogroup' || field.getXType() === 'checkboxgroup') { 
		            	
		              	//  record= field.items.findBy( function(i){
		               	field.cascade( function(i){
		                   // return (i.inputValue == fieldValue);
		                   	if (i.xtype === 'radiofield' && i.name == fieldName && i.inputValue == fieldValue) {
		                   		record = i;
		                   		return false;
		                   	}
		                });
		                if (record){
		                	if (record.radiofieldValue !== undefined && record.radiofieldValue !== null && record.radiofieldValue != "") {
		                		returnValue = record.radiofieldValue;
		                	} else {
		                		returnValue = record.boxLabel;
		                	}
		                   
		                }
		            } else if (field.getXType() === 'checkboxfield') {
		            //	console.log('checkboxfield fieldValue',fieldValue );
		            	returnValue = (fieldValue === '1' || fieldValue === true ) ? _('Igen') : _('Nem');
		            } else if (field.getXType() === 'datefield' ) {
		            	if (Ext.isDate(fieldValue)) {
		            		returnValue = Ext.Date.format(fieldValue, "Y.m.d");
		            	} else {
		            		returnValue = Ext.Date.format(Ext.Date.parse(fieldValue, "Y-m-d H:i:s"), "Y.m.d");
		            	}
		               
		            } 
		            else {
		                returnValue = fieldValue;
		            }
		        }
	        }
        }
        return returnValue;
    },

    showFieldBtns : function(form, record) { "use strict";          
        form.cascade(function(c){
            if (c.getXType()==='fielddetailsbtn') {
            	
                record.updateFieldDetailsBtn( c); 
            }
           
      //      if (c.valueField ) {
            	
     //       	record.addFieldButton( c.up('panel'), c.id); 
            	
          /* 	var b= c.up('panel').items.items.add({
	                xtype : 'button',
	                margin : this.buttonMargin?this.buttonMargin:'0',
	                tabIndex: -1
	            });
	            b.getEl().alignTo(c.getEl(), "tr?",[10,0]);
				console.log('hi b',b);*/
    //        }
        });
    }
    
    
}); 
