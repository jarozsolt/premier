/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * AEToolbar.js
 *
 * Toolbar containing the adverse event buttons
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.AEToolbar', {
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.aetoolbar',
	requires : ['Ext.ux.form.SearchField'],
	defaults : {
		minWidth : 90
	},
	initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },    
    strictInitComponent : function() { "use strict";
        var permissionClass = "Forms";
        if (Kron.core.UserManagement.hasPermission(permissionClass, "createForm"))
        {
            // New AE button
            this.add({
                text : "<b>" + _("Új AE") + "</b>",
                itemId : 'aetoolbar-btn-new',
                tooltip : _("Új Nemkívánatos Esemény felvétele"),
                scale : 'medium',
                iconCls : 'icon-new-btn_medium',
                operationType: 'active'
            });
            // New MACCE button
            this.add({
                text : "<b>" + _("Új MACCE") + "</b>",
                itemId : 'aetoolbar-btn-new-macce',
                tooltip : _("Új MACCE felvétele"),
                scale : 'medium',
                iconCls : 'icon-new-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
        {
            // Edit button
            this.add({
                text : "<b>" + _("Módosítás") + "</b>",
                itemId : 'aetoolbar-btn-edit',
                tooltip : _("A kiválasztott Nemkívánatos Esemény módosítása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-edit-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "getForms"))
        {
            // View button
            this.add({
                text : "<b>" + _("Megnyitás") + "</b>",
                itemId : 'aetoolbar-btn-view',
                tooltip : _("A kiválasztott Nemkívánatos Esemény megnyitása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-view-btn_medium',
                operationType: 'notactive'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "createForm"))
        {
            // Follow-up button
            this.add({
                text : "<b>" + _("AE Follow-Up") + "</b>",
                itemId : 'aetoolbar-btn-followup',
                tooltip : _("AE Follow-Up hozzáadása a kiválasztott Nemkívánatos Eseményhez"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-followup-btn_medium',
                operationType: 'active'
            });
             this.add({
                text : "<b>" + _("MACCE Follow-Up") + "</b>",
                itemId : 'aetoolbar-btn-maccefollowup',
                tooltip : _("MACCE Follow-Up hozzáadása a kiválasztott Nemkívánatos Eseményhez"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-followup-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "deleteForm"))
        {
            // Delete button
            this.add({
                text : "<b>" + _("Törlés") + "</b>",
                itemId : 'aetoolbar-btn-delete',
                tooltip : _("A kiválasztott Nemkívánatos Esemény törlése"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-delete-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "printForm"))
        {
            // Print button
            this.add({
                text : "<b>" + _("Nyomtatás") + "</b>",
                itemId : 'aetoolbar-btn-print',
                tooltip : _("A Nemkívánatos Esemény adatlapjának nyomtatása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-print-btn_medium'
            });
        }   
    }
}); 