/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * SignatureGrid.js
 *
 * The main grid in the Medication view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.SignatureGrid', {
	extend : 'Kron.view.patient.GeneralPatientGrid',
	alias : 'widget.signaturegrid',
	id : 'signaturegrid',
	store : 'Forms',
	selType : 'checkboxmodel',
	multiSelect : true,
	border : false,
	
	
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent : function() { "use strict";
		this.columns = [{
				header : _("Adatlap típus"),
				width : 250,
				sortable : false,
				hideable : false,
				renderer : this.renderVisit
			}, {
				header : _("Státusz"),
				sortable : false,
				hideable : false,
				renderer : this.renderVisitStatus
			}, {
				header : _("Monitorozva?"),
				sortable : false,
				renderer: this.renderMonitored
			}, {
				header : _("Információ"),
				sortable : false,
				flex : 1,
				renderer : this.renderInformation
			}];
	}
}); 