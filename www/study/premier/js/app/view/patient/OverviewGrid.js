/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * OverviewGrid.js
 *
 * The main grid in the Medication view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.patient.OverviewGrid', {
	extend : 'Kron.view.patient.GeneralPatientGrid',
	alias : 'widget.overviewgrid',
	id : 'overviewgrid',
	store : 'Forms',

	border : false,
	
	
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
    renderSignedBy : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
        return record.get('signed_by');
    },
    
    renderSignedDate : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
        return Ext.Date.format(record.get('signed'), Kron.defaultDateFormat);
    },
    
	strictInitComponent : function() { "use strict";
		this.columns = [{
					header : _("Vizit típus"),
					width : 250,
					sortable : false,
					hideable : false,
					renderer : this.renderVisit
				},{
					header : _("Státusz"),
					sortable : false,
					hideable : false,
					renderer : this.renderVisitStatus
				},{
					header : _("Aláírva?"),
					sortable : false,
					renderer : this.renderSigned
				},{
					header : _("Aláíró"),
				//	dataIndex : 'signed_by',
					renderer : this.renderSignedBy
				},{
					xtype : 'datecolumn',
					header : _("Aláírás dátuma"),
				//	dataIndex : 'signed',
					renderer : this.renderSignedDate
				},{
					header : _("Monitorozva?"),
					sortable : false,
					renderer: this.renderMonitored
				},{
					header : _("Információ"),
					sortable : false,
					flex : 1,
					renderer : this.renderInformation
				}
				];
	}
}); 