/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MedicalHistoryGrid.js
 *
 * The main grid in Medical history view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.MedicalHistoryGrid', {
	extend : 'Kron.view.patient.GeneralPatientGrid',
	alias : 'widget.medicalhistorygrid',
	id : 'medicalhistorygrid',
//	store : 'MedicalHistory',
	store : 'Forms',
	/**
	 * Az amire szűrni kell a Forms store-t, hogy csak a megfelelő rekordok látszódjanak a grid-ben.
	 */
	formtypeFilter: 'MedicalHistory',
	
	
    initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
    renderStartDate : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
        return record.getFieldValue('startDate');
    },
	
	renderEndDate : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var returnValue,
        fieldValue = record.getFieldValue('endDate');
        if (fieldValue === 'o.g.') {
            returnValue = _("Folyamatban");
        } else {
            returnValue = Ext.Date.format(fieldValue, Kron.defaultDateFormat);
            if (returnValue === '') {
                returnValue = value;
            }
        }
        return returnValue;
	},
	

	strictInitComponent : function() { "use strict";
		this.columns = [/*{// @todo: ne felejtsem el kitörölni azt a két oszlopot majd...
			header : "id",
			dataIndex : 'id'
		},{
			header : "instanceid",
			dataIndex : 'instanceid'
		},*/{
			header : _("Diagnózis"),
			dataIndex : 'formfields',
			formIndex : 'diagnosis',
            width : 150,
			renderer : function(value, metaData, record, rowIndex, colIndex, store) {
				return record.getFieldValue('diagnosis');
			}
		},{
			xtype : 'datecolumn',
			header : _("Kezdete"),
			dataIndex : 'formfields',
			formIndex : 'startDate',
            width : 150,
            renderer : this.renderStartDate
		},{
			xtype : 'datecolumn',
			header : _("Vége"),
			dataIndex : 'formfields',
			formIndex : 'endDate',
            width : 150,
			renderer : this.renderEndDate
		}, {
			header : _("Státusz"),
			sortable : false,
			hideable : false,
			renderer : this.renderVisitStatus
		}, {
			header : _("Aláírva?"),
			sortable : false,
			renderer : this.renderSigned
		}, {
			header : _("Monitorozva?"),
			sortable : false,
			renderer: this.renderMonitored
		}, {
			header : _("Információ"),
			sortable : false,
			flex : 1,
			renderer : this.renderInformation
		}];
	}
	
}); 
