/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * AEGrid.js
 *
 * The main grid in Adverse Event view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.AEGrid', {
	extend : 'Kron.view.patient.GeneralPatientGrid',
	alias : 'widget.aegrid',
	id : 'aegrid',
	store : 'Forms',
	/**
	 * Az amire szűrni kell a Forms store-t, hogy csak a megfelelő rekordok látszódjanak a grid-ben.
	 */
	formtypeFilter: 'AdverseEvent',
	
	renderDiagnosis : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var diagnosis = record.getFieldValue('diagnosis'),
	        parentid = record.get('parentid'),
	        macceType = record.getFieldValue('macceType'),
			id = record.get('instanceid');
		if (record.isDeleted()) {
		    // TODO: lefordítani
			metaData.tdAttr = 'data-qtip="' + _("Törölt") + '"';
			if (parentid !== null) {// Follow-Up
				metaData.tdCls = 'grid-deletedfollowup-row-first-cell';
			} else {
				metaData.tdCls = 'grid-deleted-row-first-cell';
			}
		} else if (record.get('formtype') === 'AdverseEventMACCE' ) {
			diagnosis = _("MACCE");	
			if (macceType !== undefined && macceType !== null && macceType > 0) {
				diagnosis +=  ": " +  Kron.macceTypeTexts[macceType-1];
			}
			if (parentid > 0) {// Follow-Up
				metaData.tdAttr = 'data-qtip="' + _("MACCE Follow-Up") + '"';
				metaData.tdCls = 'grid-followup-row-first-cell';
			} else {
				metaData.tdAttr = 'data-qtip="' + _("MACCE") + '"';
				if(record.getFieldValue('ae_serious') === true || record.getFieldValue('ae_serious') == 1) {
					metaData.tdAttr = 'data-qtip="' + _("Súlyos MACCE") + '"';
				} else {
					metaData.tdAttr = 'data-qtip="' + _("MACCE") + '"';
				}
				metaData.tdCls = 'grid-sae-row-first-cell';
				
			}
		} else {
			if (parentid > 0) {// Follow-Up
				metaData.tdAttr = 'data-qtip="' + _("Follow-Up") + '"';
				metaData.tdCls = 'grid-followup-row-first-cell';
			} else {
				if(record.getFieldValue('ae_serious') === true || record.getFieldValue('ae_serious') == 1) {
					metaData.tdAttr = 'data-qtip="' + _("Súlyos Nemkívánatos Esemény") + '"';
				} else {
					metaData.tdAttr = 'data-qtip="' + _("Nemkívánatos Esemény") + '"';
				}
				metaData.tdCls = 'grid-sae-row-first-cell';
			}
		}
		return '<div>' + diagnosis + '</div>';
	},

	renderSerious : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var serious = '',
		fieldserious = record.getFieldValue('ae_serious');
		if (fieldserious !== undefined) {
            if (fieldserious === true || fieldserious === 1) {
                serious = _("Igen");
            }
            else if (fieldserious === false || fieldserious === 2) {
                serious = _("Nem");
            }
        }
		return serious;
	},

	renderOutcome : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var outcomeText = "",
        outcome = record.getFieldValue('outcome');
		if( outcome > 0 ) {
			outcomeText = Kron.outcomeTexts[outcome-1][1];
		}
		return outcomeText;
	},
    
    renderStartDate : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
        var startDate = record.getFieldValue('startDate');
        return Ext.Date.format(startDate, Kron.defaultDateFormat);
    },
	
	renderEndDate : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var returnValue,
		endDate = record.getFieldValue('endDate');
		if (endDate === 'o.g.') {
			returnValue = _("Folyamatban");
		} else {
			returnValue = Ext.Date.format(endDate, Kron.defaultDateFormat);
			if (returnValue === '') {
				returnValue = endDate;
			}
		}
		return returnValue;
	},
	
	

    initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent : function() {"use strict";
		this.columns = [/*{
			header : "instanceid",
			dataIndex : 'instanceid'
		},{
			header : "parentid",
			dataIndex : 'parentid'
		},*/{
			header : _("Diagnózis vagy tünet"),
			dataIndex : 'formfields',
            formIndex : 'diagnosis',
			width : 200,
			sortable : false,
			hideable : false,
			renderer : this.renderDiagnosis
		}, {
			header : _("Súlyos?"),
			dataIndex : 'formfields',
			formIndex : 'ae_serious',
            sortable : false,
            hideable : false,
			renderer : this.renderSerious
		}, {
			xtype : 'datecolumn',
			header : _("Kezdete"),
			sortable : false,
			dataIndex : 'formfields',
			formIndex : 'startDate',
			renderer : this.renderStartDate
		}, {
			header : _("Vége"),
			dataIndex : 'formfields',
            formIndex : 'endDate',
			sortable : false,
			renderer : this.renderEndDate
		}, {
			header : _("Kimenetel"),
			dataIndex : 'formfields',
			formIndex : 'outcome',
			sortable : false,
			renderer : this.renderOutcome
		}, {
			header : _("Státusz"),
			sortable : false,
			hideable : false,
			renderer : this.renderVisitStatus
		}, {
			header : _("Aláírva?"),
			sortable : false,
			renderer : this.renderSigned
		}, {
			header : _("Monitorozva?"),
			sortable : false,
			renderer: this.renderMonitored
		}, {
			header : _("Információ"),
			sortable : false,
			flex : 1,
			renderer : this.renderInformation
		}];
	}
}); 