/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MedicalHistoryToolbar.js
 *
 * Toolbar containing the medical history buttons
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.MedicalHistoryToolbar', {
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.medicalhistorytoolbar',
	requires : ['Ext.ux.form.SearchField'],
	defaults : {
		minWidth : 90
	},
	initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },    
    strictInitComponent : function() { "use strict";
        var permissionClass = "Forms";
        if (Kron.core.UserManagement.hasPermission(permissionClass, "createForm"))
        {
            // New button
            this.add({
                text : "<b>" + _("Új") + "</b>",
                itemId : 'medicalhistorytoolbar-btn-new',
                tooltip : _("Új 'Kórelőzmény, egyéb betegség' bejegyzés felvétele"),
                scale : 'medium',
                iconCls : 'icon-new-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
        {
            // Edit button
            this.add({
                text : "<b>" + _("Módosítás") + "</b>",
                itemId : 'medicalhistorytoolbar-btn-edit',
                tooltip : _("A kiválasztott 'Kórelőzmény, egyéb betegség' bejegyzés módosítása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-edit-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "getForms"))
        {
            // View button
            this.add({
                text : "<b>" + _("Megnyitás") + "</b>",
                itemId : 'medicalhistorytoolbar-btn-view',
                tooltip : _("A kiválasztott 'Kórelőzmény, egyéb betegség' bejegyzés megnyitása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-view-btn_medium',
                operationType: 'notactive'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "deleteForm"))
        {
            // Delete button
            this.add({
                text : "<b>" + _("Törlés") + "</b>",
                itemId : 'medicalhistorytoolbar-btn-delete',
                tooltip : _("A kiválasztott 'Kórelőzmény, egyéb betegség' bejegyzés törlése"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-delete-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "printForm"))
        {
            // Print button
            this.add({
                text : "<b>" + _("Nyomtatás") + "</b>",
                itemId : 'medicalhistorytoolbar-btn-print',
                tooltip : _("A 'Kórelőzmény, egyéb betegség' bejegyzések nyomtatása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-print-btn_medium'
            });
        }   
    }
});
