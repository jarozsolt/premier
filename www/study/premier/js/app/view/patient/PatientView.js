/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * PatientView.js
 *
 * View containing the selected patient
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.PatientView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.patientview',
    layout: 'border',
    patient: '',
    patientRecord : {},
    border: false,
	id: 'patientview',
    requires: [ 
                'Kron.view.patient.PatientDialog',
               'Kron.view.patient.PatientMenu',
               'Kron.view.patient.VisitGrid',
               'Kron.view.patient.VisitToolbar',
               'Kron.view.patient.MedicalHistoryGrid',
               'Kron.view.patient.MedicalHistoryToolbar',
               'Kron.view.patient.AEGrid',
               'Kron.view.patient.AEToolbar',
               'Kron.view.patient.MedicationGrid', 
               'Kron.view.patient.MedicationToolbar',
			   'Kron.view.patient.SignatureView',
			   'Kron.view.patient.OverviewView'
	],

	items:[{
		xtype : 'panel',
		id : 'patientmenu',
		region : 'west',
		layout : 'fit',
		title : _("Nincs kijelölt páciens"),
		width : 212,
		//margins: '0 0 0 5',
		lbar : {
			xtype : 'patientmenu'
		}
	}, {
		region : 'center',
		id : 'patientview-center',
		//frame: true,
		border : false,
		//margins: '0 0 0 5',
		layout : 'card',
		activeItem : 0,
		items : [{
			itemId : 'visitview',
			xtype : 'panel',
			title : _("Vizitek"),
			layout : 'fit',
			items : {
				xtype : 'visitgrid',
				border: false
			},
			dockedItems : [{
				dock : 'top',
				xtype : 'visittoolbar'
			}]
		}, {
			itemId : 'medicationview',
			xtype : 'panel',
			title : _("Egyéb kezelések"),
			layout : 'fit',
			items : {
				xtype : 'medicationgrid',
				border: false
			},
			dockedItems : [{
				dock : 'top',
				xtype : 'medicationtoolbar'
			}]
		}, {
			itemId : 'aeview',
			xtype : 'panel',
			title : _("Nemkívánatos Események"),
			layout : 'fit',
			items : {
				xtype : 'aegrid',
				border: false
			},
			dockedItems : [{
				dock : 'top',
				xtype : 'aetoolbar'
			}]
		}, {
			itemId : 'medicalhistoryview',
			xtype : 'panel',
			title : _("Kórelőzmény, egyéb betegségek"),
			layout : 'fit',
			items : {
				xtype : 'medicalhistorygrid',
				border: false
			},
			dockedItems : [{
				dock : 'top',
				xtype : 'medicalhistorytoolbar'
			}]
		}, {
			itemId : 'overviewview',
			xtype : 'overviewview',
			title : _("Áttekintés")
		}, {
			itemId : 'signatureview',
			title : _("Aláírható adatlapok"),
			xtype : 'signatureview'
		}]
	}],
	
	counter: 0,
	

	showForm: function (grid, formid, fieldname, showQueries, isRO, exceptions) { "use strict";
		var store = grid.getStore(),
			nr = store.findBy(function(record) {
                return record.data.instanceid == formid;
            }),
            selectedRecord,
            patientdlg;
		if(nr >= 0) {
			selectedRecord = store.getAt(nr);
			patientdlg = Ext.getCmp('patientdlg');
			//isRO = true;
			//exceptions = ['monitorcheck'];
			grid.getSelectionModel().select(selectedRecord);
			patientdlg.setRecord(selectedRecord, fieldname, showQueries, isRO, exceptions) ;
		}
	},
	
	updateStores: function(formtype, formid, fieldname, showQueries, isRO, exceptions) {"use strict";
	//	console.log('updateStores');
	//	console.log(formtype);console.log(formid);console.log(fieldname);console.log(showDetails);
		var me = this,
			formsStore = Ext.data.StoreManager.lookup('Forms');
		
		formsStore.proxy.setExtraParam('patientid', this.patient);
		formsStore.proxy.setExtraParam('instanceid', '');
		formsStore.load({
			scope:this,
			callback: function(records, operation, success) {
				if (success) {
					var grid, actView, patientdlg, center;
					
					// loading mask leszedése
					if( me.patientLoadMask !== undefined && me.patientLoadMask !== null) {
     					me.patientLoadMask.hide();	
					}
					
					//me.updateSignStoreAndOverViewStore();
										
					if (formtype === 'AdverseEvent' || formtype === 'AdverseEventMACCE' ) {
						grid=Ext.getCmp('aegrid');
						actView='aeview';
					} else if (formtype === 'Medication'  ) {
						grid=Ext.getCmp('medicationgrid');
						actView='medicationview';
					} else if (formtype === 'MedicalHistory'  ) {
						grid=Ext.getCmp('medicalhistorygrid');
						actView='medicalhistory';
					} else {
						grid=Ext.getCmp('visitgrid');
						actView='visitview';
					}
			//		console.log('grid');console.log(grid);
					if (grid) {
//						store = grid.getStore();
						formsStore.clearFilter();
						formsStore.filterBy(function(record, id){
							return (record.get('formtype').indexOf(grid.formtypeFilter) != -1 ? true : false);
						});
					}	
					center = Ext.getCmp('center');
					if (center && center.getLayout().getActiveItem().itemId != 'queriesview' ) {
						center.getLayout().setActiveItem('patientview');
						Ext.getCmp('patientview-center').getLayout().setActiveItem(actView);
					}		
					if (formid !== null && formid !== undefined){
						me.showForm(grid, formid, fieldname, showQueries, isRO, exceptions);
					}

				}
				
				
			}

		});
	},
///////////////////////////////////////////////////////////////////////////////////////		

// tanulság - ha változik a form rekord - a sync viszi a formfields-et is - de eredeti adattartalommal - miért ???? hiába vált. a field - az nem látszik
// ha változik egy rekord a formfields-en belül - attól még nem változik a form rekord

	
	patientLoadMask : null,
	
	setPatient : function(record, formtype, formid, fieldname, showQueries, isRO, exceptions) {
//		console.log('PatientviewsetPatient');
//		console.log(record);console.log(formtype);console.log(formid);console.log(fieldname);console.log(showDetails);
		// calculate view from formtype
		var store, grid, actview, center;
		
		// ha a todoview-ről jövök ide, akkor nincs meg a teljes patient rekord - meg kell keresnem
		//console.log('isclosed',record.get('isclosed'));
		if (record.get('isclosed') === undefined) {
			var patientsStore = Ext.data.StoreManager.lookup('Patients');
			if(patientsStore !== undefined && patientsStore!== null) {
				var originalRecord = patientsStore.findRecord('patientid',record.get('patientid'));
				if (originalRecord !== undefined && originalRecord !== null) {
					record = originalRecord;
				}
			}
		}
		

		if (this.patient != record.get('patientid') ) { // the case of new patient
			// loading mask megjelenítése  
			 if (Kron.startingMaskRemoved) {
				 var viewport = Ext.getCmp('viewport');
				 if (viewport) {
				 	if( this.patientLoadMask === undefined || this.patientLoadMask === null) {
		         		this.patientLoadMask = new Ext.LoadMask(viewport, {msg:_("Betöltés...")});
		         	}
		            this.patientLoadMask.show();				 	
				 }
			}
			
			
			this.patient = record.get('patientid');
			this.patientRecord = record;
			
			var patientMenu=Ext.getCmp('patientmenu');
			patientMenu.setTitle(_("Páciens: ") + this.patient);
			////////////////////////////////////////
			
			var patientdlg = Ext.getCmp('patientdlg');
			if(!patientdlg) {

				patientdlg = Ext.create('Kron.view.patient.PatientDialog');
				//patientdlg.addEvents('showForm');
			}
			///////////////////////////////////////////

			this.updateStores(formtype, formid, fieldname, showQueries, isRO, exceptions);

			// collect signer's name based on patient's site + addressee store
			var investigators = new Array();
			var addresseeStore = Ext.data.StoreManager.lookup('Addressee');
			
			addresseeStore.getRootNode().cascadeBy(function(rec) {
			//	if(rec.get('siteid') == record.get('siteid') && rec.get('usergroup') == 'investigator') {
			    if(rec.get('siteid') == record.get('siteid') && rec.get('issigner') ) {
			        	investigators.push({
						'name' : rec.get('name'),
						'uiid' : rec.get('uiid')
					});
				}
			});
			// create signers store if not exists and load signers
			var signersStore = Ext.data.StoreManager.lookup('Signers');
			if(!signersStore) {
				signersStore = Ext.create('Ext.data.Store', {
					storeId : 'Signers',
					fields : ['uiid', 'name'],
					data : investigators
				});
			}
			
			
			// lezárt betegek lekezelése
			var permissionClass = "Forms";
            if (isRO === undefined) {
            	if (Kron.core.UserManagement.hasPermission(permissionClass, "createForm") ||
	                Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
	            {
	            	isRO = false;
	            } else {
	            	isRO = true;
	            }
	            
            }
            
            
			var isFormEditable = false,
			//	activeButtons= this.query('button[operationType="active"]');
				buttons= this.query('button');
			if ( !record.get('isclosed')  && Kron.core.UserManagement.hasPermission("Forms", "updateForm")){
	        		isFormEditable = true;	        		
	        }
			Ext.Array.each(buttons, function(button){
				
				if (button.operationType=="active" ) {
					if (!isFormEditable && button.isVisible()){
						button.hide();
					} else if (isFormEditable && button.isHidden()){
						button.show();
					}
				} else if (button.operationType=="notactive" ) {
					if (isFormEditable && button.isVisible()){
						button.hide();
					} else if (!isFormEditable && button.isHidden()){
						button.show();
					}
				}
			});
			
			////////////////////////////
			
			
			
		} else { //  the patient's form store has been allready loaded
			if (formtype == 'AdverseEvent' || formtype == 'AdverseEventMACCE') {
				grid=Ext.getCmp('aegrid');
				actView='aeview';
			} else if (formtype == 'Medication'  ) {
				grid=Ext.getCmp('medicationgrid');
				actView='medicationview';
			} else if (formtype == 'MedicalHistory'  ) {
				grid=Ext.getCmp('medicalhistorygrid');
				actView='medicalhistory';
			} else {
				grid=Ext.getCmp('visitgrid');
				actView='visitview';
			}
	
			if (grid) {
				store = grid.getStore();
				store.clearFilter();
				store.filterBy(function(record, id){
					return (record.get('formtype').indexOf(grid.formtypeFilter) != -1 ? true : false);
				});

			}	
			
			
			center = Ext.getCmp('center');
		//	console.log(center.getLayout().getActiveItem());console.log(center.getLayout().getActiveItem());
			if (center && center.getLayout().getActiveItem().itemId != 'queriesview' ) {
				center.getLayout().setActiveItem('patientview');
				Ext.getCmp('patientview-center').getLayout().setActiveItem(actView);
			}			
			if (formid !== null && formid !== undefined){
				this.showForm(grid, formid, fieldname, showQueries, isRO, exceptions);
			}
		}

	},
	
	getPatient: function() {"use strict";
		return this.patient;
	},
	getPatientRecord: function() {"use strict";
		return this.patientRecord;
	}
	
});