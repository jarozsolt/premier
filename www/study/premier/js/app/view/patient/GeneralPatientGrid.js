/*jslint nomen: true*/
/*global console: false, Kron: false, _:false, Demo: false*/
/**
 *---------------------------------------------------------------
 * GeneralPatientGrid.js
 *
 * The main grid in Visit view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.GeneralPatientGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.generalpatientgrid',
	bodyStyle: Demo===true?"background-image:url(resources/images/demo.png) !important":undefined,
    cls: 'patient-grid', 

	
	/*	viewConfig : {
        //stripeRows: false,
        //Return CSS class to apply to rows depending upon data values
        getRowClass: function(record, rowIndex, rowParams, store){"use strict";
            if(record.isDeleted()) {
                return 'grid-deleted-visit-row';
            }
            return 'grid-normal-visit-row';
        }
    },*/
		
	renderInformation : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var info,
		cells = '',
		images = record.getInformation();
		// Build the information bar
        for (info in images) { // Do not correct this jslint warning, we must use 'in' for associative array
            if (cells !== '') { cells += ',&nbsp'; }
            cells += '<img src="' + images[info] + '"></img>&nbsp<a>' + info + '</a>';
        }
		//return '<div>' + cells + '</div>';
		return cells;
	},
	
	renderSigned : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var text = '';
		if (record.isSigned())
		{
			text = _("Igen");
			metaData.tdCls = 'grid-signed-cell';
		}
		else
		{
			text = _("Nem");
			metaData.tdCls = 'grid-notsigned-cell';
		}
		return text;
	},
	
	renderMonitored : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
        var text = '';
        if (record.isApproved())
        {
            text = _("Igen");
            metaData.tdCls = 'grid-monitored-yes-cell';
        }
        else
        {
            text = _("Nem");
            metaData.tdCls = 'grid-monitored-no-cell';
        }
        return text;
    },
	
	renderVisitType : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		return record.getFormName();
	},
	
	   renderVisit : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
        var name='',
	        returnValue = record.getFormName(),
	        macceType ,
	        formType = record.get('formtype');
        
        if ( formType === 'Medication') {
            name = record.getFieldValue('tradename');
        } else if ( formType === 'MedicalHistory') {
            name = record.getFieldValue('diagnosis');
        } else if ( formType === 'AdverseEvent') {
            name = record.getFieldValue('diagnosis');
        } else if ( formType === 'AdverseEventMACCE') {
        	macceType = record.getFieldValue('macceType');
			if (macceType !== undefined && macceType !== null && macceType > 0) {
				name =  Kron.macceTypeTexts[macceType-1];
			}
        }

        if (returnValue === undefined || returnValue === null) {
                returnValue = '';
        } else if (name !== undefined && name !== null && name !== '') {
                returnValue += ': ' + name;
        }
        return returnValue;
    },

	
	
	renderVisitStatus : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var visitStatus = "";
		if ( record.isEmpty()){ // ha nincs benne egy rekord sem, akkor is empty
            metaData.tdCls = 'grid-empty-cell';
            visitStatus = Kron.visitStatusTexts[Kron.visitStatus.Empty];
        }
		if (record.isNew() ) {
			metaData.tdCls = 'grid-empty-cell';
			visitStatus = Kron.visitStatusTexts[Kron.visitStatus.New];
		} else if ( record.isStored() || record.isApproved() || record.isSigned()){
			if (record.isFormValid()) {
                metaData.tdCls = 'grid-completed-cell';
                visitStatus = Kron.visitStatusTexts[Kron.visitStatus.Completed];
            } else {
                metaData.tdCls = 'grid-ongoing-cell';
			    visitStatus = Kron.visitStatusTexts[Kron.visitStatus.Stored];
			}

		} 
		else if (record.isCancelled()) {
		    metaData.tdCls = 'grid-empty-cell';
            visitStatus = Kron.visitStatusTexts[Kron.visitStatus.Cancelled];
		}
		else if ( record.isMissing()){
			metaData.tdCls = 'grid-missing-cell';
			visitStatus = Kron.visitStatusTexts[Kron.visitStatus.Missing];
		} 
		if (visitStatus === ""){//@TODO - szebbíteni
			metaData.tdCls = 'grid-ongoing-cell';
		    visitStatus = Kron.visitStatusTexts[Kron.visitStatus.Stored];
		}
		
		return visitStatus;
	}
//	columns: []
}); 