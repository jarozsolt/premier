/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * OverviewView.js
 *
 * View contains every forms
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.patient.OverviewView', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.overviewview',
	id : 'overviewview',
	requires : ['Kron.view.patient.OverviewGrid'],

	layout : 'fit',

	items : [{
			xtype : 'overviewgrid'
		}],
	

    initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent : function() { "use strict";
	}



}); 