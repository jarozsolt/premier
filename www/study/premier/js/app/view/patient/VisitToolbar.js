/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * VisitToolbar.js
 *
 * Toolbar containing the visit buttons
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.VisitToolbar', {
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.visittoolbar',
	requires : ['Ext.ux.form.SearchField'],
	defaults : {
		minWidth : 90
	},
	initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },    
    strictInitComponent : function() { "use strict";
        var permissionClass = "Forms";
    /*    if (Kron.core.UserManagement.hasPermission(permissionClass, "createForm"))
        {
            // New button
            this.add({
                text : "<b>" + _("Új vizit") + "</b>",
                itemId : 'visittoolbar-btn-new',
                tooltip : _("Új vizit felvétele"),
                scale : 'medium',
                iconCls : 'icon-new-btn_medium',
                operationType: 'active' // csak akkor látszik, ha a beteg szerkeszthető (nincs lezárva)
            });
        }*/
        if (Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
        {
            // Edit button
            this.add({
                text : "<b>" + _("Módosítás") + "</b>",
                itemId : 'visittoolbar-btn-edit',
                tooltip : _("A kiválasztott vizit módosítása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-edit-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "getForms"))
        {
            // View button
            this.add({
                text : "<b>" + _("Megnyitás") + "</b>",
                itemId : 'visittoolbar-btn-view',
                tooltip : _("A kiválasztott vizit megnyitása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-view-btn_medium',
                operationType: 'notactive' // csak akkor látszik, ha a beteg nem szerkeszthető
            });
        }
/*        if (Kron.core.UserManagement.hasPermission(permissionClass, "deleteForm"))
        {
            // Delete button
            this.add({
                text : "<b>" + _("Törlés") + "</b>",
                itemId : 'visittoolbar-btn-delete',
                tooltip : _("A kiválasztott vizit törlése"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-delete-btn_medium',
                operationType: 'active'
            });
        }*/
        if (Kron.core.UserManagement.hasPermission(permissionClass, "printForm"))
        {
            // Print button
            this.add({
                text : "<b>" + _("Nyomtatás") + "</b>",
                itemId : 'visittoolbar-btn-print',
                tooltip : _("A kiválasztott vizit nyomtatása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-print-btn_medium'
            });
        }   
    }
}); 