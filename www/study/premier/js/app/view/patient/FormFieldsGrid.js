/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FormFieldsGrid.js
 *
 * Egyelőre csak teszt célra egy gridre is kiteszem a formfields store tartalmát
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.FormFieldsGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.formfieldsgrid',
	id : 'formfieldsgrid',
	store : 'FormFields',
	
	
	
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
        
    },
    
   
	strictInitComponent : function() { "use strict";
	   //console.log('gridstore',this.getStore());
		this.columns = [
            { header: 'id',  dataIndex: 'id' , width : 50},
            { header: 'form_instanceid', dataIndex: 'form_instanceid', width : 50},
            { header: 'fieldname',  dataIndex: 'fieldname' , width : 150},
            { header: 'fieldvalue',  dataIndex: 'fieldvalue' , flex : 1},
            { header: 'fieldtype', dataIndex: 'fieldtype', width : 50},
            { header: 'status',  dataIndex: 'status', width : 50 },
            { header: 'parent', dataIndex: 'parent', width : 50},
            { header: 'istmp', dataIndex: 'istmp' , width : 50},
            { header: 'querystatus', dataIndex: 'querystatus' , width : 50}
        ];
	}
}); 