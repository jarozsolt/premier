/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MedicationToolbar.js
 *
 * Toolbar containing the concomitant medication buttons
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.MedicationToolbar', {
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.medicationtoolbar',
	requires : ['Ext.ux.form.SearchField'],
	defaults : {
		minWidth : 90
	},
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },    
    strictInitComponent : function() { "use strict";
        var permissionClass = "Forms";
        if (Kron.core.UserManagement.hasPermission(permissionClass, "createForm"))
        {
            // New button
            this.add({
                text : "<b>" + _("Új") + "</b>",
                itemId : 'medicationtoolbar-btn-new',
                tooltip : _("Új 'Egyéb kezelés' bejegyzés felvétele"),
                scale : 'medium',
                iconCls : 'icon-new-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "updateForm"))
        {
            // Edit button
            this.add({
                text : "<b>" + _("Módosítás") + "</b>",
                itemId : 'medicationtoolbar-btn-edit',
                tooltip : _("A kiválasztott 'Egyéb kezelés' bejegyzés módosítása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-edit-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "getForms"))
        {
            // View button
            this.add({
                text : "<b>" + _("Megnyitás") + "</b>",
                itemId : 'medicationtoolbar-btn-view',
                tooltip : _("A kiválasztott 'Egyéb kezelés' bejegyzés megnyitása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-view-btn_medium',
                operationType: 'notactive'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "deleteForm"))
        {
            // Delete button
            this.add({
                text : "<b>" + _("Törlés") + "</b>",
                itemId : 'medicationtoolbar-btn-delete',
                tooltip : _("A kiválasztott 'Egyéb kezelés' bejegyzés törlése"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-delete-btn_medium',
                operationType: 'active'
            });
        }
        if (Kron.core.UserManagement.hasPermission(permissionClass, "printForm"))
        {
            // Print button
            this.add({
                text : "<b>" + _("Nyomtatás") + "</b>",
                itemId : 'medicationtoolbar-btn-print',
                tooltip : _("Az 'Egyéb kezelés' bejegyzések nyomtatása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-print-btn_medium'
            });
        }   
    }
});
