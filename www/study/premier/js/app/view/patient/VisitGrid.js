/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * VisitGrid.js
 *
 * The main grid in Visit view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.VisitGrid', {
	extend : 'Kron.view.patient.GeneralPatientGrid',
	alias : 'widget.visitgrid',
	id : 'visitgrid',
	store : 'Forms',
	/**
	 * Az amire szűrni kell a Forms store-t, hogy csak a megfelelő rekordok látszódjanak a grid-ben.
	 */
	formtypeFilter: 'Visit',

	renderVisitDate : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var visitDateText = '',
			visitDate = record.getFieldValue('visitDate');
		
		if (visitDate) {
			visitDateText = Ext.Date.format( visitDate, Kron.defaultDateFormat);
		} 
		return visitDateText;
	},
	
	renderDueDate : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
        var dueDateText = '',
            dueDate = record.get('duedate');//record.getFieldValue('visitDate');
        
        if (dueDate) {
            dueDateText = Ext.Date.format( dueDate, Kron.defaultDateFormat);
        }
        return dueDateText;
    },

	
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent : function() {"use strict";
		this.columns = [{
			xtype : 'datecolumn',
			format : Kron.defaultDateFormat,
			header : _("Esedékes"),
			sortable : false,
			hideable : false,
			//dataIndex : 'duedate'
			renderer : this.renderDueDate
		}, {
			xtype : 'datecolumn',
		//	format : Kron.defaultDateFormat,
			header : _("Vizit dátum"),
			sortable : false,
			hideable : false,
		//	dataIndex : 'duedate',
			renderer : this.renderVisitDate
		}, {
			header : _("Vizit típus"),
			width : 150,
			sortable : false,
			hideable : false,
			renderer : this.renderVisitType
		}, {
			header : _("Státusz"),
			sortable : false,
			hideable : false,
			renderer : this.renderVisitStatus
		}, {
			header : _("Aláírva?"),
			sortable : false,
			renderer : this.renderSigned
		}, {
			header : _("Monitorozva?"),
			sortable : false,
			renderer: this.renderMonitored
		}, {
			header : _("Információ"),
			sortable : false,
			flex : 1,
			renderer : this.renderInformation
		}];
	}
}); 