/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * QueryWindow.js
 *
 * Query-t kezelő és a mező history-t megmutató ablak
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.QueryWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.querydlg',
	title :_("Mező részletes adatai"),
	requires : ['Kron.view.patient.forms.QueryForm', 'Kron.view.patient.QueryGrid', 'Kron.view.patient.FieldHistoryGrid'],

	closable : true,
	closeAction : 'hide',
	width : 800,
	modal : true,
	resizable : false,
	//plain : true,

	border : false,
	id : 'querydlg',

	items : [{
			id : 'queryform',
			xtype : 'queryForm',
			autoScroll : true
		}, {
		    xtype : 'tabpanel',
		    items: [{
                    xtype : 'panel',
                    title : _("Query lista"),
                    layout : 'fit',
                
                    items : {
                        xtype : 'querygrid',
                        border : false,
                        height: 200,
                        scroll : 'vertical',
                        autoScroll: true
                    },
                    dockedItems: [{
                        xtype: 'toolbar',
                        dock: 'bottom',
                        layout: {
                            pack: 'center',
                            padding: '0 5 0 5'
                        },
                        items: [{
                                text: _("Bezárás"),
                                tooltip : _("Ablak bezárása"),
                                scale : 'medium',
                                iconCls : 'icon-cancel-btn_medium',
                                handler : function() {"use strict";
                                    this.up('window').hide();
                                }
                            },{
                                xtype: 'tbfill',
                                flex : 1
                            },{
                                text : _("Query lezárása"),
                                id : 'closequerybtn',
                                icon: 'resources/images/tick_24.png',
                                tooltip : _("Query lezárása"),
                                scale : 'medium'
                            },{
                                text : _("Válasz query-re"),
                                icon: 'resources/images/plus_24.png',
                                id : 'addcommentbtn',
                                tooltip : _("Válasz adása queryhez"),
                                scale : 'medium'
                            },{
                                text : _("Query törlése"),
                                icon: 'resources/images/delete_24.png',
                                id : 'deletequerybtn',
                                tooltip : _("Query törlése"),
                                scale : 'medium'
                            },{
                                text : _("Query hozzáadása"),
                                 icon: 'resources/images/addquery_24.png',
                                id : 'addquerybtn',
                                tooltip : _("Új query hozzáadása"),
                                scale : 'medium'
                                
                            }]
                    }]
                }, {
                    xtype : 'panel',
                    title : _("Mező történet"),
                    layout : 'fit',
                
                    items : {
                        xtype : 'fieldhistorygrid',
                        border: false,
                        height: 200,
                        scroll : 'vertical',
                        autoScroll: true
                    },
                    dockedItems: [{
                        xtype: 'toolbar',
                        dock: 'bottom',
                        layout: {
                            pack: 'center'
                        },
                        items: [{
                                text: _("Bezárás"),
                                tooltip : _("Ablak bezárása"),
                                scale : 'medium',
                                iconCls : 'icon-cancel-btn_medium',
                                handler : function() {"use strict";
                                    this.up('window').hide();
                                }
                        }]
                    }]
                }]
		}],
	
	
    selectedFieldNames: [],
    selectedFieldsValue : '',
    selectedFieldsType : '',
 //   selectedBtnContainer : {},
 //   referencedField : {},
    fieldDetailsBtn : {},
//    monitorBtn : {},
    
    setSelectedFieldNames: function(fieldNames){"use strict";
        var dlg = Ext.getCmp('patientdlg'),
            userprefs = Ext.getCmp('userprefs'),
            record = dlg.getRecord(),
            n, i, field,
            sep = '',
            fieldsValues = [],
            fieldsTypes = [],
            selectedField;
            
        for (i= 0; i<fieldNames.length; i= i+1) {
            n =  record.formfields().findBy(function(record) {
                return ((record.get('fieldname') == fieldNames[0] && record.get('istmp') == 0 ) ? true: false);
            });
            if  ( n>= 0 ) { 
                field = record.formfields().getAt(n); 
                if (field) {
                    fieldsValues.push(Ext.htmlEncode(field.get('fieldvalue')));
                    fieldsTypes.push(field.get('fieldtype'));
                }    
            }
        }
        this.selectedFieldsValue =  fieldsValues.join('&');// @todo - jó ez így & karakterrel ? 
        this.selectedFieldsType =  fieldsTypes.join('&');
        this.selectedFieldNames = fieldNames;  
        
        if (fieldNames.length == 1) {
            selectedField =Ext.getCmp('selectedfield');
            if (selectedField) {
                 selectedField.setValue(dlg.getFormattedFieldValue(fieldNames[0] ));//, fieldsValues[0])); - mert mindig a formból kell venni ide a fieldvalue értékét
            }
        }
        
        
    },
    
    getSelectedFieldNames: function(){"use strict";
        return this.selectedFieldNames;
    },
    
    getSelectedFieldsValue: function(){"use strict";
        return this.selectedFieldsValue;
    },
    getSelectedFieldsType: function(){"use strict";
        return this.selectedFieldsType;
    },
    
    historyLabel: null,
    
    setHistoryLabel: function(label){"use strict";
        var selectedField;
        this.historyLabel = label;
        if (label !== undefined || label !== null  ) {
            selectedField = Ext.getCmp('selectedfield');
            if (selectedField) {
                selectedField.setFieldLabel(label);
            }
        }
    },

    getHistoryLabel: function(){"use strict";
        return this.historyLabel;
    },
    
 /*   getSelectedBtnContainer: function(){"use strict";
        return this.selectedBtnContainer;
    },
    
    setSelectedBtnContainer: function(btnContainer){"use strict";
        this.selectedBtnContainer = btnContainer;
    }, 
    
    setReferencedField: function(field){"use strict";
        this.referencedField = field;
    }, 
    getReferencedField: function(){"use strict";
        return this.referencedField;
    }, 
    setMonitorBtn: function(btn){"use strict";
        this.monitorBtn = btn;
    }, 
    getMonitorBtn: function(){"use strict";
        return this.monitorBtn;
    },*/
   	setFieldDetailsBtn: function(btn){"use strict";
        this.fieldDetailsBtn = btn;
    }, 
    getFieldDetailsBtn: function(){"use strict";
        return this.fieldDetailsBtn; 
    },
    
    setFieldStatusText: function(status) {"use strict";
        var //approveBtn = Ext.getCmp('approveFieldBtn'),
            fieldStatusText = '',
            statusField = Ext.getCmp('queryform_field_status');
                                        
        if (status !== '' /* && approveBtn*/ && statusField) {
            fieldStatusText = Kron.fieldStatusTexts[status];

            statusField.setValue(fieldStatusText);

       /*     if (status === Kron.fieldStatus.MonitorApproved) {
                approveBtn.setText(_("Jóváhagyás törlése"));
            } else {
                approveBtn.setText(_("Jóváhagyás"));
            }*/
        }
    }
});
