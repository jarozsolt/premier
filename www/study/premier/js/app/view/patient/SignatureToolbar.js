/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * SignatureToolbar.js
 *
 * Toolbar containing the concomitant medication buttons
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.SignatureToolbar', {
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.signaturetoolbar',

	defaults : {
		minWidth : 90
	},
	items : [{
		text : "<b>" + _("Aláírás") + "</b>",
		itemId : 'signaturetoolbar-btn-sign',
		tooltip : _("A kiválasztott adatlapok aláírása"),
		scale : 'medium',
		iconCls : 'icon-sign-btn_small',
        operationType: 'active'
	}]
}); 