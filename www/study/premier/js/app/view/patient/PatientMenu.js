/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * PatientMenu.js
 *
 * Tool-bar containing the patient sub-menu on the left
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 * @todo Add tool-tips for the buttons.
 * @todo Implement tool-bar overflow.
 * @todo Change the style of the tool-bar to simple or 'large icons'
 * based on the user preference settings.
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.PatientMenu', {
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.patientmenu',
	//enableOverflow: true,
	defaults : {
		scale : 'large',
		height : 40,
		width : 206
	},
	initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },    
    strictInitComponent : function() { "use strict";
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms"))
        {
            // Visits button
            this.add({
                text : "<b>" + _("Vizitek") + "</b>",
                itemId : 'patientmenu-btn-visits',
                iconCls : 'icon-visits-menu_large'
            });
            // Mediciations button
            this.add({
                text : "<b>" + _("Egyéb kezelések") + "</b>",
                itemId : 'patientmenu-btn-medication',
                iconCls : 'icon-medication-menu_large'
            });
            // Medical histories button
            this.add({
                text : "<b>" + _("Kórelőzmény,<br>egyéb betegségek") + "</b>",
                itemId : 'patientmenu-btn-medicalhistory',
                iconCls : 'icon-medicalhistory-menu_large'
            });
            // Adverse events button
            this.add({
                text : "<b>" + _("Nemkívánatos Esemény") + "</b>",
                itemId : 'patientmenu-btn-ae-sae',
                iconCls : 'icon-ae-sae-menu_large'
            });
            // SAE warning text
            this.add({
                xtype : 'displayfield',
                fieldCls : 'patient-sae-warning',
                //padding : '2 2 2 4',
                value : _("A Súlyos Nemkívánatos Eseményeket a tudomásul vételt követően 24 órán belül jelenteni kell!")
            });
        }
        // Filler
        this.add({
            xtype : 'tbfill'
        });
        if (Kron.core.UserManagement.hasPermission("Forms", "getForms"))
        {
            // Overview button
            this.add({
                text : "<b>" + _("Áttekintés") + "</b>",
                itemId : 'patientmenu-btn-overview',
                //disabled: true,
                iconCls : 'icon-overview-menu_large'
            });
        }
        if (Kron.core.UserManagement.hasPermission("Forms", "signForms"))
        {
            // Sign button
            this.add({
                text : "<b>" + _("Aláírás") + "</b>",
                itemId : 'patientmenu-btn-sign',
                //disabled: true,
                iconCls : 'icon-sign-menu_large'
            });
        }
    }
}); 