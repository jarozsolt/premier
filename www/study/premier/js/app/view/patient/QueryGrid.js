/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/*
var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1
    });
    */
/**
 *---------------------------------------------------------------
 * QueryGrid.js
 *
 * The main grid in the Medication view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.QueryGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.querygrid',
	requires : ['Kron.core.Alerts'],
	id : 'querygrid',
	store : 'FieldQueries',
	sortableColumns : false,
	
	
	renderAction : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		return  Kron.queryActionTexts[value];
	},
	
	renderStatus : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
        return  Kron.queryStatusTexts[value];
    },
	renderValue : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var patientDlg = Ext.getCmp('patientdlg');
        return patientDlg.getFormattedFieldValue(record.get('fieldname'), value);
	},		
	
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },

	strictInitComponent : function() { "use strict";
		this.columns = [/*{
                header : _("id"),
                dataIndex : 'id',
                width : 30
            },{
                header : _("iid"),
                dataIndex : 'query_iid',
                width : 30
            },*/{
                xtype : 'datecolumn',
                format : 'Y.m.d H:i:s',
                header : _("Dátum"),
                dataIndex : 'created',
                 width : 80
            },{
                header : _("Esemény"),
                dataIndex : 'action',
                renderer : this.renderAction,
                width : 80
           //     renderer : this.renderAction
            },{
                header : _("Név"),
                dataIndex : 'name'
            },{
                header : _("Eredeti érték"),
                dataIndex : 'fieldvalue',
                renderer : this.renderValue,
                width : 120
            }, {
                header : _("Query szövege"),
                dataIndex : 'text',
                shrinkWrap : 2,
                flex : 1
               // width : 200
            },{
                header : _("Státusz"),
                dataIndex : 'status',
                renderer : this.renderStatus,
                width : 70
            },{
            xtype: 'actioncolumn',
            id: 'queryaction',
            width: 60,
            sortable: false,
           /* renderer : function(value, metaData, record, rowIndex, colIndex, store, view) {
                console.log("actionrendere",Ext.getCmp('querygrid_accept'));
                alert("press button");
                if (Ext.getCmp('querygrid_accept')) {
                    Ext.getCmp('querygrid_accept').setVisible(false);
                    Ext.getCmp('querygrid_accept').disable(); 
                }
               
            },*/
            items: [{
                    icon: 'resources/images/tick_24.png',
                    id : 'querygrid_accept',
                    tooltip: _("Query lezárása"),
                    scope: this,
                    handler: function(gridview, rowIndex, colIndex) {
                        this.closeQuery(gridview, rowIndex, colIndex);
                    },
                    getClass: function(v, meta, rec) {        
                        if (rec.get('action') !== 'Q' || !Kron.core.UserManagement.hasPermission("Queries", "X_closeQuery") || rec.get('status') === Kron.queryStatus.Closed ) {                                                                      
                            return 'x-hide-display';
                        }
                        var patientView=Ext.getCmp('patientview');
				       	if (patientView!== undefined) {
				       		var patientRecord = patientView.getPatientRecord()	
				       	}
				        if (patientRecord !== undefined ) { 
				        	if ( patientRecord.get('isclosed')  ) {
				        	 	return 'x-hide-display';
				            } 
				        }  
                   }
                },{
                    icon: 'resources/images/plus_24.png',
                    tooltip: _("Query válasz hozzáadása"),
                    scope: this,
                    handler: function(gridview, rowIndex, colIndex) {
                         this.onAddQueryComment(gridview, rowIndex, colIndex);
                    },
                    getClass: function(v, meta, rec) {          
                      	if(rec.get('action') !== 'Q' || !Kron.core.UserManagement.hasPermission("Queries", "X_addComment") || (rec.get('status') === Kron.queryStatus.Closed && !Kron.core.UserManagement.hasPermission("Queries", "X_addQuery") )) {                                                                      
                          	return 'x-hide-display';
                      	}
                        var patientView=Ext.getCmp('patientview');
				       	if (patientView!== undefined) {
				       		var patientRecord = patientView.getPatientRecord()	
				       	}
				        if (patientRecord !== undefined ) { 
				        	if ( patientRecord.get('isclosed')  ) {
				        	 	return 'x-hide-display';
				            } 
				        } 
                   }
                },{
                    icon: 'resources/images/delete_24.png',
                    tooltip: _("Query törlése"),
                    scope: this,
                    handler: function(gridview, rowIndex, colIndex) {
                     //   console.log('Query törlése',gridview, rowIndex, colIndex);
                        this.onDeleteQuery(gridview, rowIndex, colIndex);
                        
                    },
                    getClass: function(v, meta, rec) {           
                        if(rec.get('action') !== 'Q' || !Kron.core.UserManagement.hasPermission("Queries", "X_deleteQuery")  ) {                                                                      
                            return 'x-hide-display';
                        }
                        var patientView=Ext.getCmp('patientview');
				       	if (patientView!== undefined) {
				       		var patientRecord = patientView.getPatientRecord()	
				       	}
				        if (patientRecord !== undefined ) { 
				        	if ( patientRecord.get('isclosed')  ) {
				        	 	return 'x-hide-display';
				            } 
				        } 

                   }
                }
            ]
        }
        ];
	},
	
	
	 addQuery : function(text) {"use strict";
        var store = this.getStore(),
            Qdlg = Ext.getCmp('querydlg'),
            dlg = Ext.getCmp('patientdlg'),
            userprefs = Ext.getCmp('userprefs'),
            formRecord = dlg.getRecord(),
            fieldNames = Qdlg.getSelectedFieldNames(),
            date = new Date(),
            queryRecord =  Ext.create('Kron.model.FieldQuery', {
                form_instanceid : formRecord.get('instanceid'),
                fieldname : fieldNames[0],
                fieldlabel : Qdlg.getHistoryLabel(),
                fieldvalue : Qdlg.getSelectedFieldsValue(),
                fieldtype : Qdlg.getSelectedFieldsType(),
                status : Kron.queryStatus.Open,
                text : text,
                action :  Kron.queryAction.Query,
                created : date,
                name : userprefs.getName() 
            }),
          //  originalQueryStatus = formRecord.getFieldQueryStatus(fieldNames[0]),
            //btnContainer = this.up('window').getSelectedBtnContainer(),
           
            fieldDetailsBtn =  this.up('window').getFieldDetailsBtn(),
      //      monitorBtn = fieldDetailsBtn.getMonitorCheckBtn(),
     //       referencedField = fieldDetailsBtn.getReferencedField(),
            fieldStatus='';

        store.insert(0, queryRecord);
        store.sync({
            success: function() {
                this.reloadFieldHistoryStore();
                this.reloadPatientQueriesStore();
            },
            failure: function() { 
                Kron.core.Alerts.connectionError();
            },
            scope: this
        });

        formRecord.updateFieldQueryStatus(fieldDetailsBtn );
        this.setMainStoresDirty();

    },
   
   
    onAddQueryComment: function (gridview, rowIndex, colIndex){"use strict";
        
       if( rowIndex !== undefined && rowIndex !== null && rowIndex >= 0) {
           Ext.Msg.show({
                title: _("Query válasz hozzáadása"),
                msg: _("Kérem írja be a megjegyzését:"),
                width: 400,
                buttons: Ext.Msg.OKCANCEL,
                multiline: true,
                fn: function(btn, text){
                        if (btn === 'ok'){
                            this.addQueryComment(text, gridview, rowIndex, colIndex);
                        }
                    },
                scope: this,
                icon: Ext.window.MessageBox.INFO
            }); 
        } 
            
   },
   
   
	addQueryComment : function (text, gridview, rowIndex, colIndex){"use strict";
	    var store = this.getStore(),
            userprefs = Ext.getCmp('userprefs'),
	        originalRec = store.getAt(rowIndex),  
	        date = new Date(),
            queryRecord =  Ext.create('Kron.model.FieldQuery', {
                form_instanceid : originalRec.get('form_instanceid'),
                fieldname : originalRec.get('fieldname'),
                fieldlabel :'',
                fieldvalue : '',
                fieldtype : '',
                status: '', 
                text : text,
                action : Kron.queryAction.Comment,
                query_iid : originalRec.get('query_iid'),
                created : date,
                name : userprefs.getName() 
            }),
         //   referencedField = this.up('window').getReferencedField(), 
          //	monitorBtn = this.up('window').getMonitorBtn(),
          	fieldDetailsBtn = this.up('window').getFieldDetailsBtn(),
            patientDlg = Ext.getCmp('patientdlg'),
            formRecord = patientDlg.getRecord();
        if ((originalRec.get('status')=== Kron.queryStatus.Open || originalRec.get('status')=== Kron.queryStatus.Reopened ) && !Kron.core.UserManagement.hasPermission("Queries", "X_addQuery") ) {
            originalRec.set('status', Kron.queryStatus.Answered);
            
        } else if (originalRec.get('status')!== Kron.queryStatus.Reopened && originalRec.get('status')!== Kron.queryStatus.Open  && Kron.core.UserManagement.hasPermission("Queries", "X_addQuery") ) {
            originalRec.set('status', Kron.queryStatus.Reopened);
        }
        rowIndex = rowIndex +1;
        while (store.getAt(rowIndex) !== undefined && store.getAt(rowIndex).get('action') !== Kron.queryAction.Query  ) {
            rowIndex = rowIndex+1;
        }
        store.insert(rowIndex, queryRecord);
        store.sync({
            success: function() {
                this.reloadFieldHistoryStore();
                this.reloadPatientQueriesStore();
            },
            failure: function() { 
                Kron.core.Alerts.connectionError();
            },
            scope: this
        });
        if(!gridview) {
           gridview = this.getView(); 
        }            
        gridview.refresh();
        formRecord.updateFieldQueryStatus(  fieldDetailsBtn);
        this.setMainStoresDirty();
	},
	
    closeQuery : function (gridview, rowIndex, colIndex){"use strict";
        var store = this.getStore(),
            originalRec = store.getAt(rowIndex),
          //  btnContainer = this.up('window').getSelectedBtnContainer(),
       //   	referencedField = this.up('window').getReferencedField(), 
      //    	monitorBtn = this.up('window').getMonitorBtn(),
          	fieldDetailsBtn = this.up('window').getFieldDetailsBtn(),
            patientDlg = Ext.getCmp('patientdlg'),
            formRecord = patientDlg.getRecord();
        if (originalRec.get('action')!== 'Q' ) {
            console.log('hiba',originalRec,rowIndex);
        }
            
            
        if (originalRec.get('status')!== Kron.queryStatus.Closed && Kron.core.UserManagement.hasPermission("Queries", "X_closeQuery") ) {
            originalRec.set('status', Kron.queryStatus.Closed);
            store.sync({
                success: function() {
                    this.reloadFieldHistoryStore();
                    this.reloadPatientQueriesStore();
                },
                failure: function() { 
                    Kron.core.Alerts.connectionError();
                },
                scope: this
            });
            if(!gridview) {
               gridview = this.getView(); 
            } 
            gridview.refresh();
            formRecord.updateFieldQueryStatus( fieldDetailsBtn );
            this.setMainStoresDirty();
        }
        
    },
    
    
    onDeleteQuery :  function (gridview, rowIndex, colIndex){"use strict";
        Ext.MessageBox.prompt({
            title : _("Megerősítés"),
            msg : _("Biztos, hogy törölni akarja a query-t?"),
            buttons : Ext.Msg.YESNO,
            buttonText: {yes: _("Igen"), no: _("Nem")},
            icon : Ext.MessageBox.QUESTION,
            scope : this,
            fn : function(btn, ev) {
                if (btn === 'yes') {
                    this.deleteQuery(gridview, rowIndex, colIndex);
                    return false;
                } else {
                    return false;
                }
            }
        });
    
    },


    deleteQuery : function (gridview, rowIndex, colIndex){"use strict";
        var store  = this.getStore(),
            fieldName = '',
            toDelete = [],
            userprefs = Ext.getCmp('userprefs'),
            queryiid = store.getAt(rowIndex).get('query_iid'),
       //    	referencedField = this.up('window').getReferencedField(), 
       //   	monitorBtn = this.up('window').getMonitorBtn(),
          	fieldDetailsBtn = this.up('window').getFieldDetailsBtn(),
            patientDlg = Ext.getCmp('patientdlg'),
            formRecord = patientDlg.getRecord();
        if ( Kron.core.UserManagement.hasPermission("Queries", "X_deleteQuery") )  {
            store.each(function(rec){
                if (rec.get('query_iid') == queryiid) {
                    toDelete.push(rec);
                }
            });  
            store.remove(toDelete);
            store.sync({
                success: function() {
                    this.reloadFieldHistoryStore();
                    this.reloadPatientQueriesStore();
                },
                failure: function() { 
                    Kron.core.Alerts.connectionError();
                },
                scope: this
            });
            formRecord.updateFieldQueryStatus(  fieldDetailsBtn);
            if(!gridview) {
               gridview = this.getView(); 
            } 
            gridview.refresh();
            
            this.setMainStoresDirty();
       }
    },
    reloadFieldHistoryStore : function(){"use strict";
        var fieldHistoriesStore = Ext.data.StoreManager.lookup('FieldHistories');
        if (fieldHistoriesStore) {
            fieldHistoriesStore.load();
        }

    },
    reloadPatientQueriesStore : function(){"use strict";
        var queriesView = Ext.getCmp('queriesview'),
            patientQueriesStore = Ext.data.StoreManager.lookup('PatientQueries');      
        if (queriesView && queriesView.selectedPatientId !== null && patientQueriesStore ) {
            patientQueriesStore.load();
        }

    },
     // a lenti azért kell, hogy újratöltödjenek majd a store-ok, ha kell
    setMainStoresDirty : function() {"use strict";
     /*  var todoView = Ext.getCmp('todoview');
        if (todoView) {
            todoView.setStoreDirty(true);
        } */
        var patientsView = Ext.getCmp('patientsview');
        if (patientsView) {
            patientsView.setStoreDirty(true);
        } 
        var queriesView = Ext.getCmp('queriesview');
        if (queriesView) {
            queriesView.setStoreDirty(true);
        }  
    }    
       
}); 