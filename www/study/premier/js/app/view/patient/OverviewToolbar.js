/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * OverviewToolbar.js
 *
 * Toolbar containing the Overview view's buttons
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patient.OverviewToolbar', {
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.overviewtoolbar',

	defaults : {
		minWidth : 120
	},
	items : [{
		text : "<b>" + _("Aláírás visszavonása") + "</b>",
		itemId : 'overviewtoolbar-btn-sign',
		tooltip : _("Aláírás visszavonása"),
		scale : 'medium',
        operationType: 'active'
		//iconCls : 'icon-sign-menu_large'
	}]
}); 