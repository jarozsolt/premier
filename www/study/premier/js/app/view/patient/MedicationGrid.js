/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MedicationGrid.js
 *
 * The main grid in the Medication view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.patient.MedicationGrid', {
	extend : 'Kron.view.patient.GeneralPatientGrid',
	alias : 'widget.medicationgrid',
	id : 'medicationgrid',
	store : 'Forms',
	/**
	 * Az amire szűrni kell a Forms store-t, hogy csak a megfelelő rekordok látszódjanak a grid-ben.
	 */
	formtypeFilter: 'Medication',
	
	/*viewConfig : {
		//trackOver: false,//kell?
		//stripeRows: false,
		//Return CSS class to apply to rows depending upon data values
		getRowClass : function(record, index) {"use strict";
			if(record.get('status') === 'D') {
				return 'grid-deleted-row';
			}
			return 'grid-normal-row';
		}
	},*/
	
	renderName : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var medicationName = record.getFieldValue('tradename');
		if(record.get('status') === 'D') {
			metaData.tdAttr = 'data-qtip="' + _("Törölt") + '"';
			metaData.tdCls = 'grid-deleted-row-first-cell';
		}
		return '<div>' + medicationName + '</div>';
	},

	renderDose : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		/*var dose = record.getFieldValue('dosevalue'),
			unit = record.getFieldValue('doseunit');
		if (unit > 0) {
			dose += ' ' + Kron.doseUnits[unit-1][1];
		}*/
		var valueTokens, comboValue, dose,
			displayValue = '',
			value = record.getFieldValue('dose');
			
		if (value !== undefined && value !== null) {
			dose = '' + value;
            valueTokens = dose.split('|');
            if (valueTokens[0] !== undefined)
            {
                displayValue = valueTokens[0];
            }
            else
            {
                displayValue = dose;
            }
            if (valueTokens[1] !== undefined && valueTokens[1] !== null)
            {
                comboValue = parseInt(valueTokens[1], 10);
                if (comboValue > 0 && comboValue !== 9) {// nem n.a.
                    displayValue += ' ' + Kron.doseUnits[comboValue-1][1];
                }
            }
        }
		return displayValue;
	},

	renderIndication : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var indication = record.getFieldValue('indication');
		return indication;
	},


	
	renderStartDate : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		//return  Ext.Date.format(record.getFieldValue('startDate'), Kron.defaultDateFormat);
		return record.getFieldValue('startDate');
	},
	
	renderEndDate : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		var returnValue,
		fieldValue = record.getFieldValue('endDate');
		if (fieldValue === 'o.g.') {
			returnValue = _("Folyamatban");
		} else {
			returnValue = Ext.Date.format(fieldValue, Kron.defaultDateFormat);
			if (returnValue === '') {
				returnValue = value;
			}
		}
		return returnValue;
	},

	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent : function() { "use strict";
		this.columns = [{
			header : _("Kezelés/gyógyszer megnevezése"),
		//	width : 150,
			renderer : this.renderName
		}, {
			xtype : 'datecolumn',
			format : Kron.defaultDateFormat,
			header : _("Kezelés kezdete"),
			renderer : this.renderStartDate
		}, {
			header : _("Kezelés vége"),
			renderer : this.renderEndDate
		}, {
			/*header : _("Teljes napi dózis"),
			renderer : this.renderDose
		}, {*/
			header : _("Indikáció"),
			renderer : this.renderIndication
		}, {
			header : _("Státusz"),
			sortable : false,
			hideable : false,
			renderer : this.renderVisitStatus
		}, {
			header : _("Aláírva?"),
			sortable : false,
			renderer : this.renderSigned
		}, {
			header : _("Monitorozva?"),
			sortable : false,
			renderer: this.renderMonitored
		}, {
			header : _("Információ"),
			sortable : false,
			flex : 1,
			renderer : this.renderInformation
		}];
	}
}); 