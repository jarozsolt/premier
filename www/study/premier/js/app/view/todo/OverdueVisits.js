/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * OverdueVisits.js
 *
 * Overdue Visits View  in Todo view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.todo.OverdueVisits', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.OverdueVisits',
    
    title: _("Elmaradt vizitek"),
    autoScroll: true,
		
    bookTplMarkup: [
   		'<tpl for=".">',
   			'<div class="todo-overdue-date">{[Ext.Date.format(values.calendardate, "Y-m-d")]}</div>',
   			'<tpl for="calendarvisits">',
	   			'<div class="todo-visit">',
		   			'<table class="todo-item" style="cursor:pointer;" width="100%" cellspacing="0" cellpadding="0" border="0" id="{patientid},{siteid},{formtype},{instanceid}" ><tbody>',
		   				'<tr>',
				        	'<td class="todo-visit-top-left-red"></td>',
				        	'<td class="todo-visit-top-red"></td>',
				            '<td class="todo-visit-top-right-red"></td>',
			            '</tr>',
			            '<tr>',
				        	'<td class="todo-visit-left-red"></td>',
				        	'<td class="todo-visit-background-red" data-qtip="<b><u>{patientid}</u></b><br>{[Kron.formNames[values.formtype]]}">',
				        		'<b>{#} <u>{patientid}</u></b><br>{[Kron.formNames[values.formtype]]}',
				        	'</td>',
				            '<td class="todo-visit-right-red"></td>',
			            '</tr>',
			            '<tr>',
				        	'<td class="todo-visit-bottom-left-red"></td>',
				        	'<td class="todo-visit-bottom-red"></td>',
				            '<td class="todo-visit-bottom-right-red"></td>',
			            '</tr>',
			        '</tbody></table>',
   				'</div',
   			'</tpl>',
   		'</tpl>'
   		],
   		
	
	//startingMarkup: _("Betöltés..."),

	bodyPadding: 7,
	
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent: function() { "use strict";
		this.tpl = Ext.create('Ext.XTemplate', this.bookTplMarkup);
		this.html = this.startingMarkup;
		this.bodyStyle = {
			background : '#ffffff'
		};
	},
	
	eventFlag: true,
	
	/**
	 * Custom function used to update panel
	 * @param {Object} data
	 */ 
	 updateDetail: function(data) {
		this.tpl.overwrite(this.body, data);
		if (this.eventFlag) {
			this.eventFlag=false;
			this.getEl().on('click', function(e, t) {
			    e.stopEvent();
			    if (t.id.length>=7) {// because id="{patientid},{siteid},{formtype},{instanceid}"
			    	var itemParams=t.id.split(',');
				    var record = Ext.create('Kron.model.Form');
					record.set('patientid',itemParams[0]);
					record.set('siteid',itemParams[1]);
					var patientView=Ext.getCmp('patientview');
				    patientView.setPatient(record, itemParams[2], itemParams[3]);
			   }
			}, null, {delegate: 'table.todo-item'});	
		}	
	}
	});
