/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * WeekView.js
 *
 *  Todo panel WeekView panelje
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, DatDatanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.todo.WeekView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.WeekView',
    
    title: _("Heti nézet"),
    autoScroll: true,
	/**
	 * Template a heti nézethez  
	 */		
    weekTplMarkup: [
		'<table width="100%" height="100%" border="1" cellspacing="0"><tr height="10%">',
		'<tpl for=".">',
			'<th style="padding:5px;"  width="14%">',
				'<div class="todo-calendar-month">{year} {month}</div>',
				'<tpl if="day==0"><div class="todo-calendar-sunday">{date}  </div></tpl>',
				'<tpl if="day!=0"><div class="todo-calendar-day">{date}  </div></tpl>',
				'<tpl if="day==0"><div class="todo-calendar-weekend">{weekday}</div></tpl>',
				'<tpl if="day!=0"><div class="todo-calendar-weekday">{weekday}</div></tpl>',
				'</th>',
		'</tpl>',
		'</tr><tr >',
		'<tpl for=".">',
			'<td style="vertical-align:top;" width="14%">',
			'<tpl for="calendarvisits">',
				'<div class="todo-visit">',
					'<table class="todo-item" style="cursor:pointer;" width="100%" cellspacing="0" cellpadding="0" border="0" id="{patientid},{siteid},{formtype},{instanceid}" ><tbody>',
						'<tr>',
							'<td class="todo-visit-top-left-{color}"></td>',
							'<td class="todo-visit-top-{color}"></td>',
				            '<td class="todo-visit-top-right-{color}"></td>',
			            '</tr>',
			            '<tr>',
							'<td class="todo-visit-left-{color}"></td>',
							'<td class="todo-visit-background-{color}" data-qtip="<b><u>{patientid}</u></b><br>{[Kron.formNames[values.formtype]]}<br>{[Kron.visitStatusTexts[values.status]]}">',
								'<b>{#} <u>{patientid}</u></b><br> {[Kron.formNames[values.formtype]]}',
							'</td>',
				            '<td class="todo-visit-right-{color}"></td>',
			            '</tr>',
			            '<tr>',
							'<td class="todo-visit-bottom-left-{color}"></td>',
							'<td class="todo-visit-bottom-{color}"></td>',
				            '<td class="todo-visit-bottom-right-{color}"></td>',
			            '</tr>',
			        '</tbody></table>',
				'</div',
			'</tpl>',
			'</td>',
		'</tpl>',
	'</tr></table>'
	],
	
	/**
	 * A template frissítése előtt ez látszik
	 */
	startingMarkup: _("Betöltés..."),

	bodyPadding: 0,
		
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
	
	strictInitComponent: function() { "use strict";
        this.tpl = Ext.create('Ext.XTemplate', this.weekTplMarkup);
        this.html = this.startingMarkup;
        this.bodyStyle = {
            background : '#ffffff'
        };
    },
    /**
     * Itt tárolom el, hogy hozzáadtam-e már a click event-et a todo-item-ekhez
     */
	eventFlag: false,
	/**
	 * visszadja, hogy hozzáadtam-e már a click event-et a todo-item-ekhez
	 */
	isItemEventOn: function () { "use strict";
		return this.eventFlag;
	},
	
	setItemEventOn: function () { "use strict";
		this.eventFlag = true;
	},
	
	/**
	 * Frissíti a panel template-et
	 * @param {Object} data Egy heti adat
	 */
	 updateDetail: function(data) { "use strict";
		this.tpl.overwrite(this.body, data);
		if (!this.isItemEventOn()) {
			this.setItemEventOn();
			this.getEl().on('click', function(e, t) {
			    e.stopEvent();
			    if (t.id.length>=7) {// because id="{patientid},{siteid},{formtype},{instanceid}"
					var itemParams=t.id.split(','),
						record = Ext.create('Kron.model.Form'),
						patientView=Ext.getCmp('patientview');
					record.set('patientid',itemParams[0]);
					record.set('siteid',itemParams[1]);
				    patientView.setPatient(record, itemParams[2], itemParams[3]);
			   }
			}, null, {delegate: 'table.todo-item'});	
		}	
	}

});
