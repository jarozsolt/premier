/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * TodoView.js
 *
 * TodoView layout view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, DatDatanova Kft
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.todo.TodoView', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.todoview',
	id : 'todoview',
	requires : ['Kron.view.todo.WeekView', 'Kron.view.todo.OverdueVisits'],
	layout : 'border',
	border : false,
	
	storeDirty : false,
	
	isStoreDirty : function(){"use strict";
	    return this.storeDirty;
	},
	
	setStoreDirty : function(value){"use strict";
        this.storeDirty = value;
    },

	items : [{
		itemId : 'westpanel',
		region : 'west',
		preventHeader : true,
		width : 212,
		layout : {
			type : 'border'
		},
		items : [{
			title : _("Elmaradt vizitek"),
			border : false,
			region : 'center',
			itemId : 'overduevisits',
			xtype : 'OverdueVisits'
		}, {
			title : _("Naptár"),
			border : false,
			region : 'south',
			xtype : 'datepicker',
			itemId : 'datepicker',
			startDay : 1,
			handler : function(picker, date) {"use strict";
				var actWeek = Ext.Date.getWeekOfYear(date), cDate = new Date();
				this.cells.each(function(c) {
					var dt = c.dom.firstChild.dateValue;
					cDate.setTime(dt);
					if (actWeek <= Ext.Date.getWeekOfYear(cDate) && Ext.Date.getWeekOfYear(cDate) <= actWeek + 1) {
						c.addCls('x-mydate-selected');
					} else {
						c.removeCls('x-mydate-selected');
					}
				});

			}
		}]
	}, {
		itemId : 'CenterPanel',
		region : 'center',
		title : _("Teendők"),
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		items : [{
			itemId : 'weekview',
			xtype : 'WeekView',
			flex : 1,
			border : false,
			preventHeader : true
		}, {
			itemId : 'week2view',
			xtype : 'WeekView',
			flex : 1,
			border : false,
			preventHeader : true

		}]

	}],
	
	initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },
    
    strictInitComponent : function() { "use strict";
        this.addEvents('reloadStore');
    }
});
