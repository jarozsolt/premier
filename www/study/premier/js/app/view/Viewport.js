/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Viewport.js
 *
 * The application viewport
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.Viewport', {
    extend: 'Ext.container.Viewport',
    layout: 'border',
    id : 'viewport',
    requires: [
        'Kron.view.FileDownloader',
        'Kron.view.FormWarning',
        'Kron.view.Header',
        'Kron.view.todo.TodoView',
        'Kron.view.patients.PatientsView',
        'Kron.view.patient.PatientView',
        'Kron.view.queries.QueriesView',
        'Kron.view.messages.MessagesView',
        'Kron.view.DocumentsView',
        'Kron.view.ContactsView',
        'Kron.view.reports.ReportsView',
        'Kron.view.userprefs.UserPrefsView'],
    items: [{
            xtype: 'filedownloader',
            id: 'fileDownloader'
    }, {
            xtype: 'formwarning',
            id: 'formwarning'
    }, {
        xtype: 'mainheader',
        region: 'north',
        //margins: '0 0 3 0',
        border: false
    }, {
        region: 'center',
        id: 'center',
        //frame: true,
        border: false,
        //margins: '0 0 0 5',
        layout: 'card',
        activeItem: 0,
        items: [{
            itemId: 'todoview',
            xtype: 'todoview'
        }, {
            itemId: 'patientsview',
            xtype: 'patientsview'
        }, {
            itemId: 'patientview',
            xtype: 'patientview'
        }, {
            itemId: 'queriesview',
            xtype: 'queriesview'
        }, {
            itemId: 'MessagesView',
            xtype: 'MessagesView'
        }, {
            itemId: 'DocumentsView',
            xtype: 'panel',
            title : _("Dokumentumok letöltése"),
            items : [{ 
                xtype: 'DocumentsView' 
            }]
        }, {
            itemId: 'ContactsView',
            xtype: 'panel',
            title : _("Kapcsolatok"),
            items : [{ 
                xtype: 'ContactsView' 
            }]
        }, {
            itemId: 'ReportsView',
            xtype: 'ReportsView'
        }, {
            itemId: 'UserPrefsView',
            xtype: 'UserPrefsView'
        }]
    }]
});