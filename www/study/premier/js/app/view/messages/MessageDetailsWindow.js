/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MessageDetailsWindow.js
 *
 * New Message window in Message view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.messages.MessageDetailsWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.MessageDetailsWindow',
	
	requires : ['Kron.view.messages.SentMessageDetails', 'Kron.view.messages.ReceivedMessageDetails'],

	title : _('Message Details'),
	closable : true,
	closeAction : 'hide',
	width : 700,
	height : 400,
	layout : 'card',
	modal : true,
	dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        layout: {
		    pack: 'center'
		},
        items: [{
            text: _("Bezárás"),
        //    itemId : 'medicalhistorytoolbar-btn-delete',
			tooltip : _("Ablak bezárása"),
			scale : 'medium',
			iconCls : 'icon-cancel-btn_medium',
			handler : function() {
				this.up('window').hide();
			}
        }]
    }],
    activeItem : 0,
	items : [ {
		xtype : 'ReceivedMessageDetails',
		id : 'ReceivedMessageDetailsWindow'
	},{
		xtype : 'SentMessageDetails',
		id : 'SentMessageDetailsWindow'
	}]
	
	
});
