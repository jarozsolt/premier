/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * SentMessageDetails.js
 *
 * Sent Message details - panel in Message view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.messages.SentMessageDetails', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.SentMessageDetails',

	bookTplMarkup : ['<b>' + _('Subject') + ':</b> {subject}<br/>',
		'<b>' + _('To') + ':</b> {addressees}<br/>',
		'<b>' + _('Date') + ':</b> {created}<br/><hr />',
		'<b>' + _('Message') + ':</b><br/>{longText}<br/>'],
		
	startingMarkup : _('Please select a message to see additional details.'),

	bodyPadding : 7,
	autoScroll: true,
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent : function() { "use strict";

		this.tpl = Ext.create('Ext.Template', this.bookTplMarkup);
		this.html = this.startingMarkup;
		this.bodyStyle = {
			background : '#ffffff'
		};
	},

	/**
	 * Custom function used to update panel
	 * @param {Object} data
	 */
	updateDetail : function(data) {
		data.longText = data.text.replace(/\n/g, "<br />");
		this.tpl.overwrite(this.body, data);
	}
}); 