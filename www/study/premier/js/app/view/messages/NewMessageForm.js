/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * NewMessageForm.js
 *
 * New Message form - panel in Message view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.messages.NewMessageForm', {
	extend : 'Ext.form.Panel',
	alias : 'widget.NewMessageForm',
	requires : ['Kron.core.Alerts'],
	url : 'save-form.php',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	border : false,
	bodyPadding : 10,

	fieldDefaults : {
		labelAlign : 'top',
		labelWidth : 100,
		labelStyle : 'font-weight:bold'
	},
	defaults : {
		margins : '0 0 10 0'
	},

	frame : true,

	title : _('New Message'),

	items : [{
		fieldLabel : _('to'),
		name : 'to',
		xtype : 'textareafield',
		//grow      : true,
		readOnly : true,
		allowBlank : false,
		cls : "new-message-to",
		itemId : 'NewMessageDetailsTo'
	}, {
		xtype : 'checkboxfield',
		name : 'email',
		boxLabel : _('Send e-mail notification'),
		itemId : 'NewMessageDetailsEmail'
	}, {
		fieldLabel : _('subject'),
		xtype : 'textfield',
		name : 'subject',
		allowBlank : false,
		itemId : 'NewMessageDetailsSubject'
	}, {
		fieldLabel : _('message text'),
		name : 'messageText',
		xtype : 'textareafield',
		allowBlank : false,
		flex : 1,
		itemId : 'NewMessageDetailsMessageText'
	}],
	buttons : [{
		text : _('Save'),
		itemId : 'SaveMessage',

		handler : function() {
			if(this.up('form').getForm().isValid()) {
				//get addressees userids
				var tree = Ext.ComponentQuery.query('#Addressee');
				var records = tree[0].getView().getChecked(), ids = [];
				Ext.Array.each(records, function(rec) {
					if(rec.get('leaf')) {
						ids.push(rec.get('uiid'));
					}
				});

				var params = {
					'addressees' : Ext.ComponentQuery.query('#NewMessageDetailsTo')[0].getValue(),
					'addresseeids' : ids.join(','),
					'send_email' : (Ext.ComponentQuery.query('#NewMessageDetailsEmail')[0].getValue() ? "1" : "0"),
					'subject' : Ext.htmlEncode(Ext.ComponentQuery.query('#NewMessageDetailsSubject')[0].getValue()),
					'text' : Ext.htmlEncode(Ext.ComponentQuery.query('#NewMessageDetailsMessageText')[0].getValue())
				};
				Kron.Messages.postMessage(params, function(provider, response) {
					if(response === undefined || response.result === undefined || response.result.data === undefined || response.status === false) {// no response from the server
						//concole.log("postMessage: Connection Error");
						 Kron.core.Alerts.connectionError();
					} else {
						var SentMessagesStore = Ext.data.StoreManager.lookup('SentMessages');
						SentMessagesStore.add(response.result.data);
					}
				});
				this.up('form').getForm().reset();
				this.up('window').hide();
				//  Ext.MessageBox.alert(_('Thank you!'), _('Your message has been sent.'));
			}
		}
	}, {
		text : _('Cancel'),
		handler : function() {
			this.up('form').getForm().reset();
			this.up('window').hide();
		}
	}]

});
