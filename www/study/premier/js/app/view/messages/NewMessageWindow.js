/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * NewMessageWindow.js
 *
 * New Message window in Message view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.messages.NewMessageWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.NewMessageWindow',

	requires : ['Kron.view.messages.Addressee', 'Kron.view.messages.NewMessageForm'],

	title : _('New Message'),
	closable : true,
	closeAction : 'hide',
	width : 800,
	height : 600,
	layout : 'border',
	bodyStyle : 'padding: 5px;',
	x : 100,

	items : [{
		region : 'west',
		xtype : 'Addressee',
		title : _('Addressee'),
		itemId : 'Addressee',
		width : 300,
		split : true,
		collapsible : true
	}, {
		region : 'center',
		xtype : 'NewMessageForm',
		itemId : 'NewMessageForm'

	}]
});
