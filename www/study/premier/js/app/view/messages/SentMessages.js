/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Messages.js
 *
 * Messages - grid in Todo view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.messages.SentMessages', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.SentMessages',

	stateful : true,
	stateId : 'SentMessagesGridStateId',

	store : 'SentMessages',
	title : _('Outgoing Messages'),
	selType : 'checkboxmodel',
	multiSelect : true,

    initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent : function() {
		this.columns = [{
			header : _('Date'),
			dataIndex : 'created',
			renderer : this.statusRenderer
		}, {
			header : _('To'),
			dataIndex : 'addressees',
			renderer : this.statusRenderer
		}, {
			header : _('Subject'),
			dataIndex : 'subject',
			width : 150,
			renderer : this.statusRenderer
		}, {
			header : _('Message'),
			dataIndex : 'text',
			renderer : this.statusRenderer,
			flex : 1
		}];
	},

	/**
	 * Custom function used for status column renderer
	 * @param {Object} val
	 */

	statusRenderer : function(val) {
		val = val.replace(/\n/g, " ");
		return val;

	}
}); 