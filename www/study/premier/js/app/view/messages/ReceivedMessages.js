/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * ReceivedMessages.js
 *
 * Received Messages - grid in Messages view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.messages.ReceivedMessages', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.ReceivedMessages',

	stateful : true,
	stateId : 'ReceivedMessagesGridStateId',

	store : 'ReceivedMessages',
	title : _('Incoming Messages'),
	selType : 'checkboxmodel',
	multiSelect : true,

    initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent : function() { "use strict";
		this.columns = [{
			header : _('Status'),
			dataIndex : 'status',
			renderer : this.statusRenderer
		}, {
			header : _('Date'),
			dataIndex : 'created',
			renderer : this.statusRenderer
		}, {
			header : _('From'),
			dataIndex : 'author',
			renderer : this.statusRenderer
		}, {
			header : _('To'),
			dataIndex : 'addressees',
			renderer : this.statusRenderer
		}, {
			header : _('Subject'),
			dataIndex : 'subject',
			renderer : this.statusRenderer,
			width : 150
		}, {
			header : _('Message'),
			dataIndex : 'text',
			flex : 1,
			renderer : this.statusRenderer
		}];
	},

	/**
	 * Custom function used for  column renderer
	 * @param {Object} val
	 * @param {Object} meta
	 * @param {Object} record
	 *  */

	statusRenderer : function(val, meta, record, rowIndex, colIndex, store) {
		val = val.replace(/\n/g, " ");
		if(record.data.status == 'Unread') {//!!!
			return '<span class="messages-unread">' + val + '</span>';
		} else {
			return '<span class="messages-read">' + val + '</span>';
		}

	}
}); 