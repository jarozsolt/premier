/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MessagesView.js
 *
 * MessagesView layout view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.messages.MessagesView', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.MessagesView',

	requires : [	'Kron.view.messages.ReceivedMessages',
				 	'Kron.view.messages.ReceivedMessageDetails',
				 	'Kron.view.messages.SentMessages',
				 	'Kron.view.messages.SentMessageDetails',
				 	'Kron.view.messages.MessageDetailsWindow'
				 	],
	
	title : _('Messages'),
	layout : {
		type : 'border'
	},

	items : [{
		xtype : 'toolbar',
		region : 'north',
		border : false,
		items : [{
			text : "<b>" + _("Új üzenet") + "</b>",
			itemId : 'CreateNewMessage',
			tooltip : _("Új üzenet írása"),
			scale : 'medium',
			iconCls : 'icon-new-btn_medium'
		}, {
			text : "<b>" + _("Frissítés") + "</b>",
			itemId : 'RefreshMessages',
			tooltip : _("Üzenetek frissítése"),
			scale : 'medium',
			iconCls : 'icon-reload-btn_medium'
		}, {
			text : "<b>" + _("Törlés") + "</b>",
			itemId : 'DeleteMessages',
			tooltip : _("A kiválasztott üzenet törlése"),
			scale : 'medium',
			iconCls : 'icon-delete-btn_medium'
		}]

	}, {
		xtype : 'tabpanel',
		region : 'center',
		border : false,
		items : [{
			title : _('Received Messages'),
			layout : {
				type: 'vbox',
				pack: 'start',
				align: 'stretch' 
			},
			border : false,
			items : [{
				preventHeader : true,
				border : false,
				flex: 1,
				itemId : 'ReceivedMessages',
				xtype : 'ReceivedMessages'
			}, {
				title : _('Message Details'),
				stateful : true,
				stateId : 'MessagesViewReceivedMessageDetailsStateId',
				split : true,
				resizable : true,
				collapsible : true,
				itemId : 'ReceivedMessageDetails',
				xtype : 'ReceivedMessageDetails'
			}]

		}, {
			title : _('Sent Messages'),
			layout : {
				type: 'vbox',
				pack: 'start',
				align: 'stretch' 
			},
			border : false,
			items : [{
				preventHeader : true,
				border : false,
				flex: 1,
				itemId : 'SentMessages',
				xtype : 'SentMessages'
			}, {
				title : _('Message Details'),
				height : 250,
				stateful : true,
				stateId : 'MessagesViewSentMessageDetailsStateId',
				split : true,
				resizable : true,
				collapsible : true,
				itemId : 'SentMessageDetails',
				xtype : 'SentMessageDetails'
			}]
		}]
	}]

});
