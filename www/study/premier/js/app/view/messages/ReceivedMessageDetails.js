/**
 *---------------------------------------------------------------
 * ReceivedMessageDetails.js
 *
 * Received Message details - panel in Message view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */
Ext.define('Kron.view.messages.ReceivedMessageDetails', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.ReceivedMessageDetails',
	requires : ['Kron.core.Alerts'],
	bookTplMarkup : ['<b>' + _('Subject') + ':</b> {subject}<br/>',
		'<b>' + _('From') + ':</b> {author}<br/>',
		'<b>' + _('To') + ':</b> {addressees}<br/>',
		'<b>' + _('Date') + ':</b> {created}<br/><hr />',
		'<b>' + _('Message') + ':</b><br/>{longText}<br/>'],
		
	startingMarkup : _('Please select a message to see additional details.'),

	bodyPadding : 7,
	autoScroll: true,
	
	initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
    
	strictInitComponent : function() { "use strict";
		this.tpl = Ext.create('Ext.Template', this.bookTplMarkup);
		this.html = this.startingMarkup;
		this.bodyStyle = {
			background : '#ffffff'
		};
	},

	/**
	 * Custom function used to update panel
	 * @param {Object} data
	 */
	updateDetail : function(data) {

		data.longText = data.text.replace(/\n/g, "<br />");
		this.tpl.overwrite(this.body, data);
		var params = {
			'iid' : data.iid
		};
		if(data.status == 'Unread') {
			Kron.Messages.setRead(params, function(provider, response) {
				if(response === undefined || response.result === undefined || response.result.data === undefined || response.status === false) {// no response from the server
					//concole.log("setRead: Connection Error");
					 Kron.core.Alerts.connectionError();
				} else {
					data.status = "Read";
					Ext.ComponentQuery.query('#ReceivedMessages')[0].getView().refresh()
				}

			});
		}

	}
}); 