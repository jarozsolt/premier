/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**---------------------------------------------------------------
 * Addressee.js
 *
 * Addressees tree in Message view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.messages.Addressee', {
	extend : 'Ext.tree.Panel',
	alias : 'widget.Addressee',
	id : 'addressee',

	store : 'Addressee',
	title : _('Addressee'),

	rootVisible : false,
	useArrows : true,
	frame : true,
	/*initComponent : function() {
		//	tree = Ext.getCmp(this.id);
		this.callParent(arguments);
	},*/

	columns : [{
		xtype : 'treecolumn', //this is so we know which column will show the tree
		text : _('User Name'),
		flex : 3,
		sortable : true,
		dataIndex : 'name'
	}, {
		text : _('Role'),
		flex : 1,
		dataIndex : 'usergroup',
		sortable : true
	}],

	listeners : {
		checkchange : function(node, checked) {
			node.cascadeBy(function(n) {
				n.set('checked', checked);
			});
			if(!checked) {
				node.bubble(function(n) {
					n.set('checked', checked);
				});
			} else {
				this.checkParent(node);
			}
		},

		load : function() {
			this.updateChecked();
		}
	},

	checkParent : function(node) {
		if(node.parentNode) {
			var allChecked = true;
			Ext.Array.forEach(node.parentNode.childNodes, function(cn) {
				if(!cn.get('checked')) {
					allChecked = false;
				}
			});
			if(allChecked) {
				node.parentNode.set('checked', true);
				this.checkParent(node.parentNode);
			}
		}
	},

	updateChecked : function() {
		var toField = Ext.ComponentQuery.query('#NewMessageDetailsTo');
		var data = toField[0].getValue();
		var addresseArray = data.split(',');
		var view = this.getView();
		view.node.cascadeBy(function(rec) {
			if(Ext.Array.contains(addresseArray, rec.get('name'))) {
				rec.set('checked', true);
			} else {
				rec.set('checked', false);
			}
		});
	}
});
