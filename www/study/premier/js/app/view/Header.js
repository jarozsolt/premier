/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Header.js
 *
 * Header component containing the logo, top status bar and main menu
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */


Ext.define('Kron.view.Header', {
    extend: 'Ext.Panel',
    alias: 'widget.mainheader',
    requires: [
               'Kron.view.MainMenu',
               'Kron.view.InfoBar'
	],
	height: 74,
	
	layout: {
        type: 'hbox',
        align: 'stretch'
    },
	items: [{
		xtype: 'panel',
		width: 212,
		border: false,
        cls: 'logo-panel'
    },{
        xtype: 'panel',
        flex: 1,
        border: false,
        layout: {
	        type: 'vbox',
	        align: 'stretch'
        },
        items:[{
            xtype: 'infobar',
            height: 26//28
        },{
            xtype: 'mainmenu',
            height: 48//46
        }]
    }]
});