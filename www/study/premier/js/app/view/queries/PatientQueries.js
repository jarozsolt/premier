/*jslint nomen: true*/
/*global console: false, Kron: false, Ext: false, _:false*/
/**
 *---------------------------------------------------------------
 * PatientQueries.js
 *
 * Patient Queries - grid in queries view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.queries.PatientQueries', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.PatientQueries',
	requires : ['Kron.view.queries.QueriesToolbar'],
	store : 'PatientQueries',

	initComponent : function() {// Do not use "use strict" on functions containing callParent!
		this.strictInitComponent();
		this.callParent(arguments);
	},
	statusRenderer : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		return Kron.queryStatusTexts[value];
	},
	formtypeRenderer : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
		return Kron.formNames[value];
	},
	fieldnameRenderer : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
	/*	var returnValue = '',
			field,
			form,
			formId = record.get('formtype') + 'Form',
			patientdlg = Ext.getCmp('patientdlg');
			
		if (!patientdlg) {
			patientdlg = Ext.create('Kron.view.patient.PatientDialog');
		}
		form = patientdlg.getComponent(formId);
		
		if (!form) {
			form = Ext.create('Kron.view.patient.forms.' + formId);
			patientdlg.items.add(form);
		}
		if (form) {
			field = form.getForm().findField(record.get('fieldname'));
			if (field) {
				returnValue = field.historyLabel;
				//returnValue = Ext.util.Format.stripTags(field.getFieldLabel()).replace(" *", "");
			}
		}
		if (returnValue === '' && field) {// ha fieldcontainer -nek van csak label-e
			returnValue = Ext.util.Format.stripTags(field.up('container').getFieldLabel()).replace(" *", "");
		}*/
	/*	if (returnValue === '') {
			returnValue = value;
		}*/
		return _(value);
	},
	strictInitComponent : function() {"use strict";
		this.columns = [{
			header : _("Dátum"),
			dataIndex : 'created',
			xtype : 'datecolumn',
			format : 'Y.m.d H:i:s',
			width : 120
		}, {
			header : _("Adatlap"),
			dataIndex : 'formtype',
			renderer : this.formtypeRenderer,
			width : 200
		}, {
			header : _("Mező"),
			dataIndex : 'fieldlabel',
			renderer : this.fieldnameRenderer,
			width : 200

		}, {
			header : _("Státusz"),
			dataIndex : 'status',
			renderer : this.statusRenderer,
			width : 120

		}, {
			header : _("Szöveg"),
			dataIndex : 'text',
			flex : 1

		}];
	}/*,// kiszedem, nics rá idő beizzítani...
	dockedItems : [{
		dock : 'top',
		xtype : 'queriestoolbar'
	}]*/
});
