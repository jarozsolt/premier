/*jslint nomen: true*/
/*global console: false, Kron: false, Ext: false, _:false*/
/**
 *---------------------------------------------------------------
 * QueriesToolbar.js
 *
 * Toolbar containing the Queries search
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.queries.QueriesToolbar', {
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.queriestoolbar',
	requires : ['Ext.ux.form.SearchField'],

	items : [{
		width : 300,
		fieldLabel : _("Query keresése"),
		labelWidth : 90,
		xtype : 'searchfield',
		store : Ext.data.StoreManager.lookup('OpenQueries')
	}]
});
