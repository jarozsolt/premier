/*jslint nomen: true*/
/*global console: false, Kron: false, Ext: false, _:false*/
/**
 *---------------------------------------------------------------
 * OpenQueries.js
 *
 * Queries with open status - grid in queries view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.queries.OpenQueries', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.OpenQueries',

	store : 'OpenQueries',

	initComponent : function() {// Do not use "use strict" on functions containing callParent!
		this.strictInitComponent();
		this.callParent(arguments);
	},

	strictInitComponent : function() {"use strict";
		this.columns = [{
			header : _("Vizsgálóhely"),
			dataIndex : 'sitename',
			width : 300
		}, {
			header : _("Páciens azonosító"),
			dataIndex : 'patientid',
			width : 100
		}, {
			header : _("Nyitott query-k száma"),
			dataIndex : 'openqueries',
			width : 150
		}, {
			header : _("Megválaszolt query-k száma"),
			dataIndex : 'answeredqueries',
			width : 150
		}];
	}
});
