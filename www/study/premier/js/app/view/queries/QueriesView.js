/*jslint nomen: true*/
/*global console: false, Kron: false, Ext: false, _:false*/
/**
 *---------------------------------------------------------------
 * QueriesView.js
 *
 * QueriesView layout view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.queries.QueriesView', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.queriesview',

	requires : ['Kron.view.queries.OpenQueries', 'Kron.view.queries.PatientQueries', 'Kron.view.queries.QueriesToolbar'],

	title : _("Nyitott query-k"),
	id : 'queriesview',
	layout : 'card',
	activeItem : 0,
	
    selectedPatientId : null,
	
	storeDirty : false,
    
    isStoreDirty : function(){"use strict";
        return this.storeDirty;
    },
    
    setStoreDirty : function(value){"use strict";
        this.storeDirty = value;
    },
	
	items : [{
		itemId : 'OpenQueries',
		xtype : 'OpenQueries'
		//border : false,
		//flex : 1
	}, {
		itemId : 'PatientQueries',
		xtype : 'PatientQueries'
		//border : false,
		//split : true,
		//resizable : true,
		//collapsible : true,
	}],
	initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },
    
    strictInitComponent : function() { "use strict";
        this.addEvents('reloadStore');
    }

});
