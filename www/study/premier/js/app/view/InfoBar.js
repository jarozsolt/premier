/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * InfoBar.js
 *
 * InfoBar containing main informations in the header
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.InfoBar', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.infobar',
	requires : ['Kron.core.Alerts'],
	id : 'infobar',
	layout : {
		type : 'hbox',
		align : 'stretch',
		padding: '2 0 2 0'
	},
	bodyStyle : {
		    background: 'rgb(223,232,246)'
	},
	border: false,
	countryStore : null,
	items : [{
		xtype : 'displayfield',
		fieldLabel : '',
		name : 'studyName',
		id : 'studyName',
		value : 'PREMIER-VV001'+(Demo===true?' Demo':''),
		flex : 1,
		fieldCls : 'infobar-text'
    }, {
            xtype : 'splitter'
    }, {
		xtype : 'displayfield',
		fieldLabel : '',
		name : 'country',
		id : 'country',
		flex : 1,
		fieldCls : 'infobar-text'
	}, {
            xtype : 'splitter'
    }, {
		xtype : 'displayfield',
		fieldLabel : '',
		name : 'site',
		id : 'site',
		flex : 4,
		fieldCls : 'infobar-text'
	}, {
            xtype : 'splitter'
    }, {
		xtype : 'displayfield',
		fieldLabel : '',
		name : 'userName',
		id : 'userName',
		flex : 2,
		fieldCls : 'infobar-text'
	}],

	updateDetail : function(userDetails) {"use strict";
		if (userDetails.sites !== undefined && userDetails.sites !== null && userDetails.sites.length > 2) {

			var sitesStore = Ext.create('Ext.data.Store', {
				id : 'sitesStore',
				fields : ['sid', 'countryid', 'sitename'],
				data : userDetails.sites
			});

			var sitesCombo = Ext.create('Ext.form.ComboBox', {
				id : 'sites',
				flex : 4,
				//width : 80,
				editable : false,
				triggerAction : 'all',
				forceSelection : true,
				emptyText : _("Vizsgálóhely kiválasztása..."),
				allowBlank : false,
				store : sitesStore,
				valueField : 'sid',
				displayField : 'sitename',
				mode : 'local',
				enableKeyEvents: true,
				listeners : {
                    // megakadályozza a backspace billentyűre a browser back-et
                    keydown: function(obj, e) {
                        if (e.getCharCode() === e.BACKSPACE) {
                            e.preventDefault();
                        }
                    },
					select : {
						fn : function(combo, value) {
							Kron.UserManagement.changeSite(value[0].data, function(provider, response) {
								if(response === undefined || response.result === undefined || response.result.success === undefined || response.status === false) {// no response from the server
									//console.log("chageSite: Connection Error");
									 Kron.core.Alerts.connectionError();
								} else {// Create the app's viewport
									Ext.getCmp('infobar').fireEvent('infoBarChanged');
								}
							});

						}
					}
				}
			});

			this.getComponent('site').hide();
			this.insert(2, sitesCombo);
			sitesCombo.setValue(userDetails.sid);

		} else {
			this.getComponent('site').setValue(userDetails.sitename);
		}

		if (userDetails.countries !== undefined && userDetails.countries !== null && userDetails.countries.length > 2) {
			var countriesStore = Ext.create('Ext.data.Store', {
				id : 'countriesStore',
				fields : ['cid', 'country'],
				data : userDetails.countries
			});
			var countriesCombo = Ext.create('Ext.form.ComboBox', {
				id : 'countries',
				flex : 1,
				editable : false,
				triggerAction : 'all',
				forceSelection : true,
				emptyText : _("Ország kiválasztása..."),
				allowBlank : false,
				store : countriesStore,
				valueField : 'cid',
				displayField : 'country',
				mode : 'local',
				enableKeyEvents: true,
				listeners : {
				    // megakadályozza a backspace billentyűre a browser back-et
                    keydown: function(obj, e) {
                        if (e.getCharCode() === e.BACKSPACE) {
                            e.preventDefault();
                        }
                    },
					select : {
						fn : function(combo, value) {
							var sitesCombo = Ext.getCmp('sites');
							if(combo.getValue() === "0") {
								sitesCombo.disable();
							} else {
								if(sitesCombo.isDisabled()) {
									sitesCombo.enable();
								}
								sitesCombo.clearValue();
								sitesCombo.store.clearFilter(true);
								sitesCombo.store.filter({
									filterFn : function(item) {
										return (item.get("countryid") === combo.getValue() || item.get("countryid") === "0" );
									}
								});
							}

							Kron.UserManagement.changeCountry(value[0].data, function(provider, response) {
								if(response === undefined || response.result === undefined || response.result.success === undefined || response.status === false) {// no response from the server
									//console.log("chageSite: Connection Error");
									 Kron.core.Alerts.connectionError();
								} else {// Create the app's viewport
									// reload data if all countries option selected
									var combo = Ext.getCmp('countries');
									if(combo.getValue() === "0") {
										Ext.getCmp('infobar').fireEvent('infoBarChanged');
									}
								}
							});
						}
					}
				}
			});
			this.getComponent('country').hide();
			this.insert(1, countriesCombo);
			countriesCombo.setValue(userDetails.cid);
			if(userDetails.cid === "0") {
				sitesCombo.disable();
			} else {
				sitesCombo.store.clearFilter(true);
				sitesCombo.store.filter({
					filterFn : function(item) {
						return (item.get("countryid") === userDetails.cid || item.get("countryid") === "0" );
					}
				});
			}
		} else {
			this.getComponent('country').setValue(userDetails.country);
		}
		this.getComponent('userName').setValue(userDetails.name);
	}
});
