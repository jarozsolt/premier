/*jslint nomen: true*/
/*global console: false, Kron: false, _:false, Demo: false*/
/**
 *---------------------------------------------------------------
 * ReportsGrid.js
 *
 * The main grid in Reports view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 * 
 *---------------------------------------------------------------
 */


Ext.define('Kron.view.reports.ReportsGrid', {
    extend : 'Ext.grid.Panel',
    requires : ['Kron.model.Form'],
    alias : 'widget.reportsgrid',
    store: 'Reports',
    id: 'reportsgrid',

    listeners: {
        selectionchange: function(model, records) {
            if (records[0]) {
                var reportsview=Ext.getCmp('reportsview'),
                    patientChart=Ext.getCmp('patientchart'),
                    visitChart = Ext.getCmp('visitchart');
                
                reportsview.updateChart(records[0], visitChart.getStore());
                reportsview.selectItem(records[0], patientChart);
                reportsview.selectedStoreItem = records[0];
               // var visitChart=Ext.getCmp('visitchart');
                if (visitChart.isHidden()) {
                    visitChart.show(); 
                }
            }
        }
    } ,

    getVisitName : function(visitNumber) { "use strict";
        var index = Ext.String.format("Visit{0}", visitNumber + 1),
        visitName = Kron.formNames[index];
        if (visitName !== undefined && visitName !== null) {
            return visitName;
        }
        // The name not found
        //console.log(Ext.String.format("Warning: no entry found in the formNames array for visit {0}", visitNumber + 1));
        return index;
    },



    renderMissingVisit : function(value, metaData, record, rowIndex, colIndex, store) {
        visitName = '';
        if (record.data.status != 'F' && record.data.status != 'D') {
            for ( i = 0; i < record.data.visit_status.length; i++) {
                if (record.data.visit_status.charAt(i) == Kron.visitStatus.Missing) {
                    if (visitName != '') {
                        visitName += ', ';
                    }
                    visitName += this.getVisitName(i);
                }
            }
        }
        return visitName;
    },
    
    renderInformation : function(value, metaData, record, rowIndex, colIndex, store) {
        var info,
			cells = '',
			images = [],
			numbers = [];
        if (record.get('openqueries') > 0)
        {
            images[_("Nyitott query")] = Kron.imagePath + 'open_queries_16.png';
			numbers[_("Nyitott query")] = record.get('openqueries');
        }
        if (record.get('answeredqueries') > 0)
        {
            images[_("Megválaszolt query")] = Kron.imagePath + 'answered_queries_16.png';
			numbers[_("Megválaszolt query")] = record.get('answeredqueries');
        }
		// Build the information bar
		for (info in images) { // Do not correct this jslint warning, we must use 'in' for associative array
            if (cells !== '') { cells += ',&nbsp'; }
            cells += '<img src="' + images[info] + '"></img>&nbsp<a>' + info + '</a>:&nbsp' + numbers[info] + _("&nbsp db.&nbsp");
        }
		//return '<div>' + cells + '</div>';
		return cells;
	},	

    renderStatus : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";

        if (record.data.status === Kron.patientStatus.Ongoing) {
            metaData.tdCls = 'icon-patient-status-ongoing';
        } else if (record.data.status === Kron.patientStatus.Dropout) {
            metaData.tdCls = 'icon-patient-status-dropout';
        } else if (record.data.status === Kron.patientStatus.Completed) {
            metaData.tdCls = 'icon-patient-status-completed';
        } else if (record.data.status === Kron.patientStatus.Screenfailure) {
            metaData.tdCls = 'icon-patient-status-screenfailure';
        }
        return (Ext.String.format('<span>{0}</span>', Kron.patientStatusTexts[record.data.status]));
    },

    renderVisitStatus : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
        var image, i, visitName, statusText, status, cells = '',
        currentStatus = "";
        for ( i = 0; i < record.data.visit_status.length; i+=1) {
            image = '';
            currentStatus = record.data.visit_status.charAt(i);
            visitName = this.getVisitName(i);
            statusText = Kron.visitStatusTexts[currentStatus];
            status = statusText;
            if (currentStatus === Kron.visitStatus.Completed) {
                image = 'green_19.png';
            } else if (currentStatus === Kron.visitStatus.Stored) {
                image = 'yellow_19.png';
            } else if (currentStatus === Kron.visitStatus.Missing) {
                image = 'red_19.png';
            } else {
                image = 'gray_19.png';
            }
            cells += '<img data-qtip="<b>' + visitName + '</b><br>' + status + '" src="' + Kron.imagePath + image + '"></img>';
        }
        if (currentStatus === Kron.visitStatus.Completed) {
            image = 'green_1.png';
        } else if (currentStatus === Kron.visitStatus.Ongoing) {

            image = 'yellow_1.png';
        } else if (currentStatus === Kron.visitStatus.Missing) {

            image = 'red_1.png';
        } else {

            image = 'gray_1.png';
        }
        cells += '<img src="' + Kron.imagePath + image + '"></img>';
        return '<div>' + cells + '</div>';
    },

 renderUSVisitStatus : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
        var image, i, visitName, statusText, status, cells = '',
        currentStatus = "";
        for ( i = 0; i < record.data.visit_status.length; i+=1) {
            image = '';
            currentStatus = record.data.visit_status.charAt(i);
            visitName = this.getVisitName(i);
            statusText = Kron.visitStatusTexts[currentStatus];
            status = statusText;
            if (currentStatus === Kron.visitStatus.Completed) {
                image = 'green_19.png';
            } else if (currentStatus === Kron.visitStatus.Stored) {
                image = 'yellow_19.png';
            } else if (currentStatus === Kron.visitStatus.Missing) {
                image = 'red_19.png';
            } else {
                image = 'gray_19.png';
            }
            cells += '<img data-qtip="<b>' + visitName + '</b><br>' + status + '" src="' + Kron.imagePath + image + '"></img>';
        }
        if (currentStatus === Kron.visitStatus.Completed) {
            image = 'green_1.png';
        } else if (currentStatus === Kron.visitStatus.Ongoing) {

            image = 'yellow_1.png';
        } else if (currentStatus === Kron.visitStatus.Missing) {

            image = 'red_1.png';
        } else {

            image = 'gray_1.png';
        }
        cells += '<img src="' + Kron.imagePath + image + '"></img>';
        return '<div>' + cells + '</div>';
    },


    initComponent : function() {// Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
   
    strictInitComponent : function() {"use strict";
        this.columns = [{
            header : _("Páciens azonosító"),
            dataIndex : 'patientid'
        }, {
            header : _("Páciens státusz"),
            renderer : this.renderStatus,
            width : 100
        }, {
            header : _("Bevonás dátuma"),
            dataIndex : 'enrollmentdate',
            xtype : 'datecolumn',
            format : Kron.defaultDateFormat
        }, {
            header : _("Vizitek"),
            align : 'center',
            renderer : this.renderVisitStatus,
            width : 180
        }, {
            header : _("Nemtervezett vizitek"),
            align : 'center',
            dataIndex : 'UnscheduledVisit',
         //   renderer : this.renderUSVisitStatus,
            width : 80
        }, {
            header : _("Egyéb kezelések"),
            align : 'center',
           // renderer : this.renderMedicationsStatus,
            dataIndex : 'Medication',
            width : 80
        }, {
            header : _("Kórelőzmények"),
            align : 'center',
            //renderer : this.renderMedicalHistoryStatus,
             dataIndex : 'MedicalHistory',
            width : 80
        }, {
            header : _("Nemkívánatos események"),
            align : 'center',
          //  renderer : this.renderAdverseEventsStatus,
           dataIndex : 'AdverseEvent',
            width : 80
        }];
    }
}); 

