/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * ReportsView.js
 *
 * ReportsView layout view
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.reports.ReportsView', {
    extend : 'Ext.panel.Panel',
    alias : 'widget.ReportsView',
    requires : ['Kron.core.Alerts'],
 //   requires: ['Kron.view.reports.ReportsGrid'],
	id : 'reportsview',
	title : _("Riportok"),
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
    //updates a chart from grid record
    selectedStoreItem : false,	
    updateChart: function(rec, store ) {"use strict";
        //console.log('rec',rec);
        var pid = rec.get('patientid');
        //console.log('pid',pid);
        var json = [{'visit': Kron.formNames.Visit1.substring(0,20),
        				'O': rec.get('fields')['Visit1'].O,
        				'Q': rec.get('fields')['Visit1'].Q,
        				'C': rec.get('fields')['Visit1'].C,
        				'A': rec.get('fields')['Visit1'].A,
        				'M': rec.get('fields')['Visit1'].M,
        				'R': rec.get('fields')['Visit1'].R
        			},
					{'visit': Kron.formNames.Visit2.substring(0,20),
        				'O': rec.get('fields')['Visit2'].O,
        				'Q': rec.get('fields')['Visit2'].Q,
        				'C': rec.get('fields')['Visit2'].C,
        				'A': rec.get('fields')['Visit2'].A,
        				'M': rec.get('fields')['Visit2'].M,
        				'R': rec.get('fields')['Visit2'].R
        			},
					{'visit': Kron.formNames.Visit3.substring(0,20),
        				'O': rec.get('fields')['Visit3'].O,
        				'Q': rec.get('fields')['Visit3'].Q,
        				'C': rec.get('fields')['Visit3'].C,
        				'A': rec.get('fields')['Visit3'].A,
        				'M': rec.get('fields')['Visit3'].M,
        				'R': rec.get('fields')['Visit3'].R
        			},
					{'visit': Kron.formNames.Visit4.substring(0,20),
        				'O': rec.get('fields')['Visit4'].O,
        				'Q': rec.get('fields')['Visit4'].Q,
        				'C': rec.get('fields')['Visit4'].C,
        				'A': rec.get('fields')['Visit4'].A,
        				'M': rec.get('fields')['Visit4'].M,
        				'R': rec.get('fields')['Visit4'].R
        			},
					{'visit': Kron.formNames.Visit5.substring(0,20),
        				'O': rec.get('fields')['Visit5'].O,
        				'Q': rec.get('fields')['Visit5'].Q,
        				'C': rec.get('fields')['Visit5'].C,
        				'A': rec.get('fields')['Visit5'].A,
        				'M': rec.get('fields')['Visit5'].M,
        				'R': rec.get('fields')['Visit5'].R
        			},
					{'visit': Kron.formNames.Visit6.substring(0,20),
        				'O': rec.get('fields')['Visit6'].O,
        				'Q': rec.get('fields')['Visit6'].Q,
        				'C': rec.get('fields')['Visit6'].C,
        				'A': rec.get('fields')['Visit6'].A,
        				'M': rec.get('fields')['Visit6'].M,
        				'R': rec.get('fields')['Visit6'].R
        			},
					{'visit': Kron.formNames.Visit7.substring(0,20),
        				'O': rec.get('fields')['Visit7'].O,
        				'Q': rec.get('fields')['Visit7'].Q,
        				'C': rec.get('fields')['Visit7'].C,
        				'A': rec.get('fields')['Visit7'].A,
        				'M': rec.get('fields')['Visit7'].M,
        				'R': rec.get('fields')['Visit7'].R
        			},
					{'visit': Kron.formNames.UnscheduledVisit.substring(0,20),
        				'O': rec.get('fields')['UnscheduledVisit'].O,
        				'Q': rec.get('fields')['UnscheduledVisit'].C,
        				'C': rec.get('fields')['UnscheduledVisit'].Q,
        				'A': rec.get('fields')['UnscheduledVisit'].A,
        				'M': rec.get('fields')['UnscheduledVisit'].M,
        				'R': rec.get('fields')['UnscheduledVisit'].R
        			},
					{'visit': Kron.formNames.AdverseEvent.substring(0,20),
        				'O': rec.get('fields')['AdverseEvent'].O,
        				'Q': rec.get('fields')['AdverseEvent'].Q,
        				'C': rec.get('fields')['AdverseEvent'].C,
        				'A': rec.get('fields')['AdverseEvent'].A,
        				'M': rec.get('fields')['AdverseEvent'].M,
        				'R': rec.get('fields')['AdverseEvent'].R
        			},
					{'visit': Kron.formNames.Medication.substring(0,20),
        				'O': rec.get('fields')['Medication'].O,
        				'Q': rec.get('fields')['Medication'].Q,
        				'C': rec.get('fields')['Medication'].C,
        				'A': rec.get('fields')['Medication'].A,
        				'M': rec.get('fields')['Medication'].M,
        				'R': rec.get('fields')['Medication'].R
        			},
					{'visit': Kron.formNames.MedicalHistory.substring(0,20),
        				'O': rec.get('fields')['MedicalHistory'].O,
        				'Q': rec.get('fields')['MedicalHistory'].Q,
        				'C': rec.get('fields')['MedicalHistory'].C,
        				'A': rec.get('fields')['MedicalHistory'].A,
        				'M': rec.get('fields')['MedicalHistory'].M,
        				'R': rec.get('fields')['MedicalHistory'].R
        			}
		];
		//console.log('json');console.log(json);
        store.loadData(json);
    },
         
    selectItem: function(storeItem, chart) {"use strict";
            var site = storeItem.get('site');
            var series = chart.series.get(0);
            var i, items, l;
            
            series.highlight = true;
            series.unHighlightItem();
            series.cleanHighlights();
            for (i = 0, items = series.items, l = items.length; i < l; i += 1) {
                if (site === items[i].storeItem.get('site')) {
                    series.highlightItem(items[i]);
                    break;
                }
            }
            series.highlight = false;
        },    
        
    initComponent: function() { // Do not use "use strict" on functions containing callParent!
        this.strictInitComponent();
        this.callParent(arguments);
    },
   
   strictInitComponent: function() { "use strict";
       this.items = [{
            preventHeader : true,
            xtype: 'panel',
            layout : {
                type : 'hbox',
                align : 'stretch'
            },
            flex : 1,
            items: [{
               preventHeader : true,
                xtype: 'panel',
                flex: 1,
                layout: 'fit',
            //    margin: '0 0 3 0',
                id : 'reports_patientspanel'
            }, {
                preventHeader : true,
                xtype: 'panel',
                id : 'reports_trendpanel',
                layout: 'fit',
                flex: 1
              //  items: trendChart
            }]
        }
       ];

       
       
       
         Ext.chart.theme.White = Ext.extend(Ext.chart.theme.Base, {
            constructor: function() {
               Ext.chart.theme.White.superclass.constructor.call(this, {
                   axis: {
                       stroke: 'rgb(8,69,148)',
                       'stroke-width': 1
                   },
                   axisLabel: {
                       fill: 'rgb(8,69,148)',
                       font: '12px Arial',
                       'font-family': '"Arial',
                       spacing: 2,
                       padding: 5,
                       renderer: function(v) { return v; }
                   },
                   axisTitle: {
                      font: 'bold 18px Arial'
                   }/*,
                   background: {
                      gradient: {
                        id: 'backgroundGradient',
                        angle: 45,
                        stops: {
                          0: {
                            color: '#ffffff'
                          },
                          100: {
                            color: '#eaf1f8'
                          }
                        }
                      }
                    },*/
               });
            }
        });


        var params = {};
        Kron.Reports.getPatientsData( function(provider, response) { //console.log('getPatientsData.result', response);
                    if( response === undefined || response.result === undefined || response.result.data === undefined || response.result.status === false ) {// no response from the server
                        //console.log("getPatientsData: Connection Error");
                         Kron.core.Alerts.connectionError();
                    } else if (Ext.isArray(response.result.data) && response.result.data.length > 0){

                    var pFields =  ['sitename', _("Folyamatban"), _("Kiesett"),_("Téves szűrés"), _("Befejezett") ];
                    var patientsReportStore = Ext.create('Ext.data.JsonStore', {
                        fields : pFields,
                        data: response.result.data
                    });
                        
                    var patientChart = Ext.create('Ext.chart.Chart', {
                            id:'patientchart',
                            animate: true,
                            shadow: true,
                    //      width: 300,
                            height: 250,
                            store: patientsReportStore,
                             legend: {
                                position: 'right'
                            },
                            axes: [{
                                type: 'Numeric',
                                position: 'bottom',
                                fields: pFields.slice(1),
                                label: {
                                    renderer: Ext.util.Format.numberRenderer('0,0')
                                },
                                title: _("Betegek száma"),
                                grid: true,
                                minimum: 0
                            }, {
                                type: 'Category',
                                position: 'left',
                                fields: ['sitename'],
                                title : false
        
                            }],
                       
                           theme: 'White',
        
                            series: [{
                                type: 'bar',
                                axis: 'bottom',
                                stacked: true,
                                highlight: true,
                                
                                tips: {
                                  trackMouse: true,
                                  width: 240,
                                  height: 28,
                                  renderer: function(storeItem, item) {// ongoing', 'dropout', 'screenfailure', 'completed'
                                    this.setTitle(item.yField +": " + item.value[1]);
                                  }
                                },
                                label: {
                                  display: 'insideEnd',
                                    field:  pFields.slice(1),
                                    renderer: Ext.util.Format.numberRenderer('0'),
                                    orientation: 'horizontal',
                                    color: '#333',
                                  'text-anchor': 'middle'
                                },
                                yField: pFields.slice(1),
                                xField: 'sitename'
                         /*       renderer: function(sprite, record, attr, index, store) {
                                    var color = ['rgb(253, 142, 24)', 
                                                 'rgb(172, 33, 47)', 
                                                 'rgb(33, 106, 172)', 
                                                 'rgb(155,179,27)'][3-index];
        
                                    return Ext.apply(attr, {
                                        fill: color
                                    });
                                }*/
                           /*     listeners: {
                                    'itemmouseup': function(item) {
                                         var series = patientChart.series.get(0);
                                         var index = Ext.Array.indexOf(series.items, item);
                                         var gridPanel=Ext.getCmp('reportgrid');
                                         var selectionModel = gridPanel.getSelectionModel();
                                         var reportsview=Ext.getCmp('reportsview');
                                         reportsview.selectedStoreItem = item.storeItem;
                                         selectionModel.select(index);
                                         var visitChart=Ext.getCmp('visitchart');
                                            if (visitChart.isHidden()) {
                                            visitChart.show(); 
                                        }
                                    }
                                }*/
                            }],
                          listeners: { 
                                beforerefresh: function() {
                                    var timer = false;
                                    clearTimeout(timer);
                                    var reportsview=Ext.getCmp('reportsview');
                                    if (reportsview.selectedStoreItem) {
                                            timer = setTimeout(function() {
                                                var reportsview=Ext.getCmp('reportsview');
                                                reportsview.selectItem(reportsview.selectedStoreItem, patientChart);
                                            }, 500);
                                    }
                                }
                            }   
                        });
                        var patientsPanel=Ext.getCmp('reports_patientspanel');
                        patientsPanel.add(patientChart);
                        patientChart.series.get(0).highlight = false;
                    }
                }); 

       
        /////////////////////////////////////////////////////////////////////////////////////

         Kron.Reports.getPatientsTrend(function(provider, response) {//console.log('getPatientsTrend.result', response);
                   if( response === undefined || response.result === undefined || response.result.data === undefined || response.result.status === false ) {// no response from the server
                        //console.log("getPatientsTrend: Connection Error");
                         Kron.core.Alerts.connectionError();
                    } else if ( response.result.sitenames !== undefined &&response.result.sitenames !== null){
                        //console.log("getPatientsTrend: Connection OK");
                        //console.log(response.result.data);
                        var trendData = response.result.data;
                        var trendFields = ['week'].concat( response.result.sitenames);
                         //console.log('trendFields', trendFields);
                        var trendReportStore = Ext.create('Ext.data.JsonStore', {
                         //   id: 'trendReportStore',
                            fields : trendFields,
                            data: trendData
                        });

                        var trendChart = Ext.create('Ext.chart.Chart', {
                            id: 'trendchart',
                            xtype: 'chart',
                       //     style: 'background:#fff',
                            animate: true,
                            store: trendReportStore,
                      //      width: 600,
                            height: 300, 
                            legend: {
                                position: 'right'
                            },
                            theme: 'White',
                            axes: [{
                                type: 'Numeric',
                               // grid: true,
                                position: 'left',
                                fields: trendFields.slice(1),
                                title: _("Betegek száma"),
                                grid: {
                                    odd: {
                                        opacity: 1,
                                        fill: '#ddd',
                                        stroke: '#bbb',
                                        'stroke-width': 1
                                    }
                                },
                                minimum: 0,
                                adjustMinimumByMajorUnit: 0
                            }, {
                                type: 'Category',
                                position: 'bottom',
                                fields: ['week'],
                                title: _("A vizsgálat hetei"),
                                grid: true,
                                label: {
                                    rotate: {
                                        degrees: 315
                                    }
                                }
                            }],
                            series: [{
                                type: 'area',
                                highlight: false,
                                axis: 'left',
                                xField: 'week',
                                yField: trendFields.slice(1),
                                style: {
                                    opacity: 0.93
                                }
                            }]
                        });
                        var trendPanel=Ext.getCmp('reports_trendpanel');
                        trendPanel.add(trendChart);

                   //     console.log('trendPanel',trendPanel);
                    }
                });  


     ///////////////////////////////
      
    /*   
       
        Kron.Reports.getVisitData( function(provider, response) {
            if(response === undefined || response.result.data === undefined || response.status === false || response.result.success === false) {// no response from the server
                //console.log("getVisitData: Connection Error");
                 Kron.core.Alerts.connectionError();
            } else {
                //console.log('getVisitData response.result', response.result.data);
               // var reportStore = Ext.data.StoreManager.lookup('Reports');
                var reportsGrid = Ext.getCmp('reportsgrid'),
                    reportStore = reportsGrid.getStore();
                reportStore.loadRawData(response.result.data);
 
              
                var vFields = ['visit', 'O','Q','A','C','M','R'];
                var visitReportStore = Ext.create('Ext.data.JsonStore', {
                     //   id: 'visitStore',
                        fields : vFields
                     //   data: response.result.data.fields
                    });
                //console.log( 'visitReportStore',visitReportStore);
            
                var visitChart = Ext.create('Ext.chart.Chart', {
                    flex: 1,
                    shadow: true,
                    animate: true,
                    store: visitReportStore,
                    width: 400,
                    height: 300, 
                    id: 'visitchart',
                    theme: 'White',
                   legend: {
                        position: 'bottom'
                    },
                    axes: [{
                        type: 'Numeric',
                        position: 'left',
                        fields: vFields.slice(1),
                        minimum: 0,
                        grid: true
                    }, {
                        type: 'Category',
                        position: 'bottom',
                        fields: ['visit'],
                        label: {
                            rotate: {
                                degrees: 270
                            }
                        }
                    }],
                    series: [{
                        type: 'column',
                        axis: 'left',
                        stacked: true,
                        highlight: true,
                        style: {
                            fill: '#456d9f'
                        },
                        highlightCfg: {
                            fill: '#a2b5ca'
                        },
                        label: {
                            contrast: true,
                            display: 'insideEnd',
                            field: 'patients',
                            color: '#000',
                            orientation: 'vertical',
                            'text-anchor': 'middle'
                        },
                        xField: 'visit',
                        yField: vFields.slice(1)
                    }]     
                });
                var mainPanel = Ext.getCmp('reports_mainpanel');

                mainPanel.add(visitChart);

                
            }
        });  

    */   
        
/////////////////////////////////////////////////////////////////////////////////////////


    //disable highlighting by default.

  //  Ext.getCmp('visitchart').hide();
    }

	/*

	sumVisit : function(value, metaData, record, rowIndex, colIndex, store) {"use strict";
	return value.O +value.Q + value.C + value.A +value.M + value.R;
	}
	*/
	
});
