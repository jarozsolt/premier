/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * AddPatientDialog.js
 *
 * Window for adding new patient
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patients.AddPatientDialog', {
	extend : 'Ext.window.Window',
	alias : 'widget.addpatientdlg',
	requires : ['Kron.core.Alerts'],
	id : 'addpatientdlg',
	title : _("Új beteg bevonása"),
	iconCls : 'icon-add-patient-wnd',
	width : 500,
	height : 200,
	modal : true,
	closable : false,
	resizable : false,
	plain : true,
	border : false,
	layout : 'fit',
	patientId : 'HU001435',
	items : {
		xtype : 'form',
		labelWidth : 250,
		width : 400,
		bodyStyle : 'padding: 10px',
		frame : true,
		monitorValid : true,
		buttonAlign : 'center',
		items : [{
			xtype : 'component',
			html : _("Az új páciens bevonásához kérjük, töltse ki a <b>Betegtájékoztató és Beleegyező nyilatkozat aláírásának dátumát</b> és válassza a <b>Bevonás</b> gombot!"),
			style : 'margin-bottom:10px;'
		}, {
			xtype : 'fieldset',
			title : _("Betegtájékoztató és Beleegyező nyilatkozat aláírásának dátuma"),
			defaultType : 'datefield',
			margin : '10 10 10 10',
			items : [{
				xtype : 'datefield',
				name : 'patientinformdate',
				itemId : 'patientinformdate',
				width : 150,
				format : Kron.defaultDateFormat,
				maxValue: new Date(),
				msgTarget : 'side',
				allowBlank : false,
				value : new Date(),
				margin : '10 0 10 10'
			}]
		}],
		buttons : [{
			text : _("Bevonás"),
			id : 'acceptPatient',
			formBind : true,
			handler : function() {
				if(this.up('form').getForm().isValid()) {
					var params = {
						'enrollmentDate' : Ext.ComponentQuery.query('#patientinformdate')[0].getValue()
					};
					Kron.Patients.addPatients(params, function(provider, response) { //console.log("addPatients response", response);
						if(response === undefined || response.result === undefined || response.result.data === undefined || response.status === false) {// no response from the server
							// console.log("addPatients: Connection Error");
                            Kron.core.Alerts.connectionError();
						} else if (!response.result.success){
							Ext.Msg.alert(_("Hiba történt"), _("Az alábbi hiba lépett fel a művelet végrehajtása során: <br>") + response.result.errorMsg);
						} else {
							var patientsGrid =Ext.getCmp('patientsgrid'),
							    patientsStore = patientsGrid.getStore(),
							    newPatientRec = patientsStore.add(response.result.data); // tömböt ad vissza!!!

							patientsGrid.getView().bufferedRenderer.scrollTo(patientsStore.indexOfTotal(newPatientRec[0]) , true);
							Ext.Msg.alert(_("Új beteg bevonása"), _("Az alábbi betegazonosítót kérjük, jegyezze fel a beteg dokumentációjába: ") + response.result.data.patientid);
						    
						    // a lenti azért kell, hogy újratöltödjenek majd a store-ok, ha kell
						    var todoView = Ext.getCmp('todoview');
						    if (todoView) {
						        todoView.setStoreDirty(true);
						    } 
						    var patientsView = Ext.getCmp('patientsview');
                            if (patientsView) {
                                patientsView.setStoreDirty(true);
                            } 
                        /*    var queriesView = Ext.getCmp('queriesview');
                            if (queriesView) {
                                queriesView.setStoreDirty(true);
                            }*/
						}
					});
					this.up('form').getForm().reset();
					this.up('window').hide();
				}
			}
		}, {
			text : _("Mégsem"),
			handler : function() {
				this.up('form').getForm().reset();
				this.up('window').hide();
			}
		}]
	}
}); 