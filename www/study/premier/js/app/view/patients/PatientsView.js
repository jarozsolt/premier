/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * PatientsView.js
 *
 * View containing the patients overview
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patients.PatientsView', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.patientsview',
	requires : ['Kron.view.patients.PatientsGrid', 'Kron.view.patients.PatientsToolbar', 'Kron.view.patients.PatientsPagingbar'],
	layout : 'fit',
	title : _("Páciensek"),
	id : 'patientsview',
	
	storeDirty : false,
    
    isStoreDirty : function(){
        return this.storeDirty;
    },
    
    setStoreDirty : function(value){
        this.storeDirty = value;
    },
	
	items : {
		itemId : 'patientsgrid',
		xtype : 'patientsgrid',
		border : false
	},
	dockedItems : [{
		dock : 'top',
		xtype : 'patientstoolbar'
	}/*, {
		dock : 'bottom',
		xtype : 'patientspagingbar'
	}*/],
	initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },
    
    strictInitComponent : function() { "use strict";
        this.addEvents('reloadStore');
    }
});
