/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * PatientsPagingbar.js
 *
 * Paging toolbar for the patients grid
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.patients.PatientsPagingbar', {
	extend : 'Ext.toolbar.Paging',
	alias : 'widget.patientspagingbar',
	store : 'Patients',
	pageSize : 25,
	displayInfo : true,
	displayMsg : _("Páciensek {0} - {1} of {2}"),
	emptyMsg : _("Nem található páciens")
}); 