/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * PatientsToolbar.js
 *
 * Toolbar containing the patient search
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */


Ext.define('Kron.view.patients.PatientsToolbar', {
    extend : 'Ext.toolbar.Toolbar',
    alias : 'widget.patientstoolbar',
  //  requires : ['Ext.ux.form.SearchField'],
    //enableOverflow: true,
    initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },    
    strictInitComponent : function() { "use strict";
        if (Kron.core.UserManagement.hasPermission("Patients", "addPatients"))
        {
            // Add new patient button
            this.add({
                text : "<b>" + _("Új beteg bevonása") + "</b>",
                itemId : 'patientstoolbar-btn-new',
                tooltip : _("Új beteg bevonása a vizsgálatba"),
                scale : 'medium',
                iconCls : 'icon-new-btn_medium'
            });
        }
        if (Kron.core.UserManagement.hasPermission("Patients", "printPatient"))
        {
            // Print button
            this.add({
                text : "<b>" + _("Nyomtatás") + "</b>",
                itemId : 'patientstoolbar-btn-print',
                tooltip : _("A páciens adatinak nyomtatása"),
                scale : 'medium',
                disabled : true,
                iconCls : 'icon-print-btn_medium'
            });
        } 
        // Spacer
        this.add({
            xtype : 'tbspacer',
            minWidth : 100,
            flex : 1
        });
        // Search field
        
        this.add({
            xtype : 'trigger',
            width : 250,
            fieldLabel : _("Páciens keresése"),
            labelWidth : 90,
            margin : '0 20 0 0',
            store : Ext.data.StoreManager.lookup('Patients'),
            trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
		    trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
		    // override onTriggerClick
		    onTrigger2Click: function() {
		        var me = this,
		        	value = me.getValue();
		
		        if (value.length > 0) {
		            me.store.clearFilter(true);
		            me.store.filter({
		                property: 'patientid',
		                value:  value,
		                anyMatch : true
		            });
		            me.updateLayout();
		    	} else {
		    		me.store.clearFilter();
	            	me.updateLayout();
		    	}
		    },
		    onTrigger1Click : function(){
		        var me = this;
		
	            me.setValue('');
	            me.store.clearFilter();
	            me.updateLayout();
		    },
		    listeners: {
                specialkey: function(field, e){
                    if (e.getKey() === e.ENTER) {
                        field.onTrigger2Click();
                    }
                }
            }

        });
    }
}); 

