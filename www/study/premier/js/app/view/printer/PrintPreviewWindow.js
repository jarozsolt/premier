/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * PrintPreviewWindow.js
 *
 * Print preview window containing an embeded PDF preview
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.printer.PrintPreviewWindow', {
    extend : 'Ext.window.Window',
    alias : 'widget.printpreviewwindow',
    title : _("Nyomtatási kép"),
    pdfFile : "",
    minWidth : 600,
    minHeight : 400,
    maximized : true,
    autoScroll : false,
    html : '<html><head><meta name="viewport" content="width=device-width; height=device-height;" /><title>' + this.title + '</title>' + '</head><body marginwidth="0" marginheight="0" style="background-color: rgb(38,38,38)"></body></html>',
    modal : true,
    maximizable : true,
    layout : 'fit',
    listeners : {
        render : function(me) {"use strict";
            me.header.items.insert(0, 'closebutton', Ext.create('Ext.button.Button', {
                text : "<b><center>"+_("Nyomtatási kép bezárása")+"</center></b>",
                scale :  'medium',
                iconCls : 'icon-close-btn_medium',
                style: "margin: 5 10 5 5",
                handler : function(btn) {
                    me.close();
                }
            }));
            me.header.doLayout();
        }
    },
    items : {
        xtype : 'component',
        id : 'pdfcontainer',
        autoEl : {
            tag : 'iframe',
            type : "application/pdf",
            style : 'height: 100%; width: 100%; border: none',
            src : Ext.SSL_SECURE_URL
        },
        listeners : {
            beforerender : function(component) {"use strict";
                var wnd = component.up('window');
                this.autoEl.src = wnd.pdfFile;
            }
        }
    }
});
