/*jslint nomen: true*/
/*global console: false, Kron: false, _:false, ActiveXObject: false*/
/**
 *---------------------------------------------------------------
 * Printer.js
 *
 * Printer class
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 *---------------------------------------------------------------
 */

Ext.define("Kron.view.printer.Printer", {

    requires: 'Kron.view.printer.PrintPreviewWindow',

    statics: {

        /**
         * Prints the passed component into a new Window.
         * @param component The component to print
         */
        printToWindow: function(component) {"use strict";
            var params, windowTitle, previewwnd, forceSave, downloader;
            windowTitle = this.mainTitle;
            forceSave = this.forcePDFSaveAs;
            params = this.printToHTML(component);
            previewwnd = Ext.create('Ext.window.Window', {
                title : windowTitle,
                minWidth : 600,
                minHeight : 400,
                maximized : true,
                autoScroll : false,
                html : params.html,
                modal : true,
                maximizable : true,
                layout : 'fit',
                listeners : {
                    render : function(me) {
                        me.header.items.insert(0, 'closebutton', Ext.create('Ext.button.Button', {
                            text : "<b><center>"+_("Nyomtatási kép bezárása")+"</center></b>",
                            scale :  'medium',
                            iconCls : 'icon-close-btn_medium',
                            style: "margin: 5 10 5 5",
                            handler : function(btn) {
                                me.close();
                            }
                        }));
                        me.header.doLayout();
                    }
                }
            });
            previewwnd.show();
        },


        /**
         * Prints the passed component into a PDF.
         * @param component The component to print
         */
        printToPDF: function(component) {"use strict";
            var params, windowTitle, previewwnd, forceSave, downloader;
            windowTitle = this.mainTitle;
            forceSave = this.forcePDFSaveAs;
            params = this.printToHTML(component);
            Kron.Forms.printForm(params, function(provider, response) {
                if (response === undefined || response.result === undefined || response.status === false) {// no response from the server
                    Ext.Msg.alert(_("Hiba"), _("Nem sikerült kapcsolódni a szerverhez!"));
                } else {
                    if (response.result.success === true) {
                        if (response.result.filename !== undefined && response.result.filename !== null && response.result.filename !== '') {
                            if (forceSave)
                            {
                                downloader = Ext.getCmp('fileDownloader');
                                if (downloader !== undefined)
                                {
                                    downloader.load({
                                        type : 'printout',
                                        file : response.result.filename,
                                        forcesave: 'true'
                                    });
                                }
                            }
                            else {
                                previewwnd = Ext.create('Kron.view.printer.PrintPreviewWindow', {
                                    title : windowTitle,
                                    pdfFile : Kron.phpPath+'download.php?type=printout&file='+ response.result.filename
                                });
                                 previewwnd.show();
                                 if (Ext.isIE) {
                                    previewwnd.close();
                                }
                            }
                        }
                    }
                }
            });
        },

        /**
         * Prints the passed form into a PDF.
         * @param formid The id of form to print
         */
        printFormToPDF: function(formid) {"use strict";
            var params, windowTitle, previewwnd, forceSave, downloader, waitMask;
            windowTitle = this.mainTitle;
            forceSave = this.forcePDFSaveAs;
            params = {
                'formid' : formid
            };
            waitMask = new Ext.LoadMask(Ext.getBody(), {msg:_("Nyomtatás...")});
            waitMask.show();
            Kron.Forms.printForm(params, function(provider, response) {
                waitMask.hide();
                if (response === undefined || response.result === undefined || response.status === false) {// no response from the server
                    Ext.Msg.alert(_("Hiba"), _("Nem sikerült kapcsolódni a szerverhez!"));
                } else {
                    if (response.result.success === true) {
                        if (response.result.filename !== undefined && response.result.filename !== null && response.result.filename !== '') {
                            if (forceSave)
                            {
                                downloader = Ext.getCmp('fileDownloader');
                                if (downloader !== undefined)
                                {
                                    downloader.load({
                                        type : 'printout',
                                        file : response.result.filename,
                                        forcesave: 'true'
                                    });
                                }
                            }
                            else {
                                previewwnd = Ext.create('Kron.view.printer.PrintPreviewWindow', {
                                    title : windowTitle,
                                    pdfFile : Kron.phpPath+'download.php?type=printout&file='+ response.result.filename
                                });
                                 previewwnd.show();
                                 if (Ext.isIE) {
                                    previewwnd.close();
                                }
                            }
                        }
                    }
                }
            });
        },

        /**
         * Prints the patient data into a PDF.
         * @param patientid The id of patient to print
         */
        printPatientToPDF: function(patientid) {"use strict";
            var params, windowTitle, previewwnd, forceSave, downloader, waitMask;
            windowTitle = this.mainTitle;
            forceSave = this.forcePDFSaveAs;
            params = {
                'patientid' : patientid
            };
            waitMask = new Ext.LoadMask(Ext.getBody(), {msg:_("Nyomtatás...")});
            waitMask.show();
            Kron.Patients.printPatient(params, function(provider, response) {
                waitMask.hide();
                if (response === undefined || response.result === undefined || response.status === false) {// no response from the server
                    Ext.Msg.alert(_("Hiba"), _("Nem sikerült kapcsolódni a szerverhez!"));
                } else {
                    if (response.result.success === true) {
                        if (response.result.filename !== undefined && response.result.filename !== null && response.result.filename !== '') {
                            if (forceSave)
                            {
                                downloader = Ext.getCmp('fileDownloader');
                                if (downloader !== undefined)
                                {
                                    downloader.load({
                                        type : 'printout',
                                        file : response.result.filename,
                                        forcesave: 'true'
                                    });
                                }
                            }
                            else {
                                previewwnd = Ext.create('Kron.view.printer.PrintPreviewWindow', {
                                    title : windowTitle,
                                    pdfFile : Kron.phpPath+'download.php?type=printout&file='+ response.result.filename
                                });
                                 previewwnd.show();
                                 if (Ext.isIE) {
                                    previewwnd.close();
                                }
                            }
                        }
                    }
                }
            });
        },


        /**
         * Prints the passed component into a html.
         * @param component The component to print
         */
        printToHTML: function(component) {"use strict";
            var html, params;
            /*component.hideMode = 'offsets';
            component.show();
            console.log(component.getEl());
            //html = '<html><head><title>' + this.mainTitle + '</title>' +'</head><body>'+'aaa'+'</body></html>';
            html = '<html><head><title>' + this.mainTitle + '</title>' + '</head><body>'+component.getEl().dom.innerHTML+'</body></html>';
            component.hide();*/
            params = {
                'html' : html
            };
            return params;
        },


        /**
         * Detects the browser and PDF plugin type and version.
         *
         */
        detectPDFPlugin: function() {"use strict";
            var version, key, acrobat,
            control = null,
            plugin_info = {
                browser_name: null,
                browser_ver: null,
                acrobat : null,
                acrobat_ver : null
            };

            if (Ext.isChrome) {
                plugin_info.browser_name = "chrome";
                plugin_info.browser_ver = Ext.chromeVersion;
            } else if (Ext.isIE) {
                plugin_info.browser_name = "ie";
                plugin_info.browser_ver = Ext.ieVersion;
            } else if (Ext.isSafari) {
                plugin_info.browser_name = "safari";
                plugin_info.browser_ver = Ext.safariVersion;
            } else if(Ext.isGecko) {
                plugin_info.browser_name = "firefox";
                plugin_info.browser_ver = Ext.firefoxVersion;
            } else if (Ext.isOpera) {
                plugin_info.browser_name = "opera";
                plugin_info.browser_ver = Ext.operaVersion;
            } else if (Ext.isWebKit) {
                plugin_info.browser_name = "webkit";
                plugin_info.browser_ver = Ext.webKitVersion;
            }

            try
            {
                if (plugin_info.browser_name === "ie")
                {
                    // load the activeX control
                    try
                    {// AcroPDF.PDF is used by version 7 and later
                        control = new ActiveXObject('AcroPDF.PDF');
                    }
                    catch (e1) {
                        plugin_info.acrobat = null;
                    }

                    if (!control)
                    {
                        try
                        {// PDF.PdfCtrl is used by version 6 and earlier
                            control = new ActiveXObject('PDF.PdfCtrl');
                        }
                        catch (e2) {
                            plugin_info.acrobat = null;
                        }
                    }

                    if (!control)
                    {
                        plugin_info.acrobat = null;
                        return plugin_info;
                    }

                    version = control.GetVersions().split(',');
                    version = version[0].split('=');
                    plugin_info.acrobat = "installed";
                    plugin_info.acrobat_ver = parseFloat(version[1]);
                }
                else if (plugin_info.browser_name === "chrome")
                {
                    for (key = 0; key < navigator.plugins.length; key += 1)
                    {
                        if (navigator.plugins[key].name === "Chrome PDF Viewer" || navigator.plugins[key].name === "Adobe Acrobat") {
                            plugin_info.acrobat = "installed";
                            plugin_info.acrobat_ver = parseInt(navigator.plugins[key].version, 10) || "Chome PDF Viewer";
                        }
                    }
                }
                else if (navigator.plugins !== null)
                {// NS3+, Opera3+, IE5+ Mac, Safari (support plugin array):  check for Acrobat plugin in plugin array
                    acrobat = navigator.plugins['Adobe Acrobat'];
                    if (acrobat === null)
                    {
                        plugin_info.acrobat = null;
                        return plugin_info;
                    }
                    plugin_info.acrobat = "installed";
                    version = acrobat.version.split(".");
                    plugin_info.acrobat_ver = parseInt(version[0], 10);
                }
            }
            catch(e)
            {
                plugin_info.acrobat_ver = null;
            }
            return plugin_info;
        },


        /**
         * @property forcePDFSaveAs
         * @type Boolean
         * True to force Save As the PDF instead of opening a print preview window.
         * (defaults to false)
         */
        forcePDFSaveAs: false,

        /**
         * @property mainTitle
         * @type String
         * Title to be used on print preview window
         * (defaults to empty)
         */
        mainTitle: ''
    }
});
