/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * DocumentsView.js
 *
 * Documents View containing the downloadable documents
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 * 
 *---------------------------------------------------------------
 */


Ext.define('Kron.view.DocumentsView', {
    extend : 'Ext.view.View',
    // TODO: atnevezni a widget nevet kisbetusre
    alias : 'widget.DocumentsView',
    requires: ['Kron.view.FileDownloader'],
    id : 'documentsview',
    store : 'Documents',
    cls: 'documents-view',
    tpl : ['<tpl for=".">',
        '<table class="documents-wrap" width="100%"><tr>',
            '<td width="120" align="center"><div class="download-button" id="download-button-{alias}"></div></td>',
            '<td>',
                '<h1>{subject}&nbsp;<b class="documents-doctype">{type}</b></h1><br/>',
                '{description}<br/><hr/>',
            '</td>',
        '</tr></table>',
    '</tpl>'],
    trackOver : true,
    autoScroll : true,
    autoHeight: false,
    height: Ext.getBody().getViewSize().height*0.85,
    overItemCls : 'contacts-item-over',
    itemSelector : 'table.documents-wrap',
    emptyText : _("Nincs letölthető dokumentum"), 
        
    listeners : {
        afterrender : function(dview) {"use strict";
            var documentStore = this.store;
            documentStore.load();
        },
        
        'refresh' : function(dview) {"use strict";
            var documentsStore = this.store;
            dview.getEl().select('div.download-button').each(function(item, items, index) {
                Ext.create('Ext.Button', {
                    scale : 'large',
                    iconCls : 'icon-download-btn_large',
                    iconAlign : 'top',
                    text : "<b>"+_("Letöltés")+"</b>",
                    renderTo : item,
                    handler : function(btn) {
                        var alias = documentsStore.getAt(index).get('alias'),
                        downloader = Ext.getCmp('fileDownloader');
                        if (downloader !== undefined)
                        {
                            downloader.load({
                                type : 'document',
                                file : alias
                            });
                        }
                    }
                });
            });
        }
    }
}); 
