/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * ContactsView.js
 *
 * Contacts View containing the Contacts' data
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.ContactsView', {
	extend : 'Ext.view.View',
	alias : 'widget.ContactsView',
	store : 'Contacts',
	tpl : ['<tpl for=".">',
		'<div class="contacts-wrap" >',
			'<table width="400" height="160" border="1" cellspacing="0">',
				'<tr height="10%"><th  colspan="2">{header}</th></tr>',
				'<tr><td width="30%" >' + 'Name:' + '</td><td class="contact-name">{name}</td></td>',
				'<tr><td>' + 'Position:' + '</td><td>{position}</td></td>',
				'<tr><td>' + 'Mobile:' + '</td><td>{mobile}</td></td>',
				'<tr><td>' + 'E-mail:' + '</td><td><a href="mailto:{email}" onclick="mailtoClicked=true;">{email}</a></td></td>',
				'<tr><td>' + 'Company:' + '</td><td>{company}</td></td>',
				'<tr><td>' + 'Address:' + '</td><td>{address1}</td></td>',
				'<tr><td></td><td>{address2}</td></td>',
				'<tr><td>' + 'Phone:' + '</td><td>{phone}</td></td>',
				'<tr><td>' + 'Fax:' + '</td><td>{fax}</td></td>',
				'</tr></table>',
		'</div>',
	'</tpl>'],
	trackOver : true,
	autoScroll : true,
    autoHeight: false,
    height: Ext.getBody().getViewSize().height*0.85,
	overItemCls : 'contacts-item-over',
	itemSelector : 'div.contacts-wrap',
	/*initComponent : function() {
		this.callParent(arguments);
	},*/
	listeners : {
		afterrender : function(dview) {"use strict";
			var contactStore = this.store;
			contactStore.load();
		}
	/*
	 * Töröltem, mert nem kell a messageing funkcionalitás
        'refresh' : function(dview) {"use strict";
			var contactStore = this.store;
			dview.getEl().select('div.send-message-button').each(function(item, items, index) {
				var button = new Ext.Button({
                    cls : 'genericbutton',
                    text : _("Üzenet küldése"),
                    renderTo : item,
                    handler : function(btn) {
                        var addresse, win, toField, tree;
                        addresse = contactStore.getAt(index).get('name');
                        win = Ext.ComponentQuery.query('NewMessageWindow');
                        if (!win[0]) {
                            win = Ext.create('widget.NewMessageWindow');
                            win.show();
                        } else {
                            win[0].show();
                        }
                        toField = Ext.ComponentQuery.query('#NewMessageDetailsTo');
                        toField[0].setValue(addresse);
                        tree = Ext.ComponentQuery.query('#Addressee');
                        tree[0].updateChecked();
                    }
                });
			});
		}*/
	}
});
