/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FileDownloader.js
 *
 * Ajax file downloader component
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.FileDownloader', {
    extend : 'Ext.Component',
    alias : 'widget.filedownloader',
    autoEl : {
        tag : 'iframe',
        cls : 'x-hidden',
        src : Ext.SSL_SECURE_URL
    },
    load : function(config) {"use strict";
        var e = this.getEl();
        e.dom.src = Kron.phpPath+'download.php' + ( config ? '?' + Ext.urlEncode(config) : '');
        e.dom.onload = function() {
            if (e.dom.contentDocument.body.childNodes[0].wholeText === '404') {
                Ext.Msg.show({
                    title : _("Hiba"),
                    msg : _("A fájl nem található a szerveren."),
                    buttons : Ext.Msg.OK,
                    icon : Ext.MessageBox.ERROR
                });
            }
        };
    }
}); 
