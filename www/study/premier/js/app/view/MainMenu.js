/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * MainMenu.js
 *
 * Toolbar containing the main menu of the application
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft
 *
 * @todo Add tooltip for the Todo, Patients, Queries, Messages,
 * Reports buttons.
 * @todo Implement toolbar overflow.
 * @todo Change the style of the toolbar to simple or 'large icons'
 * based on the user preference settings.
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.view.MainMenu', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.mainmenu',
    enableOverflow: true,
    defaults: {
		scale: 'large'
	},
	border: false,
	onLogout : function() { "use strict"; 
        this.fireEvent( 'logout');
    },
	initComponent : function() { // Do not use "use strict" on functions containing callParent!
        this.callParent(arguments);
        this.strictInitComponent();
    },    
    strictInitComponent : function() { "use strict";
        // Todo button
        this.add({
            text: "<b>"+_("Teendők")+"</b>",
            minWidth: 85,
            itemId: 'mainmenu-btn-todo',
            // TODO: tooltip-et hozzáadni
            iconCls: 'icon-todo-menu_large'
        });
        // Patients button
        if (Kron.core.UserManagement.hasPermission("Patients", "getPatients"))
        {
            this.add({
                text: "<b>"+_("Páciensek")+"</b>",
                minWidth: 95,
                itemId: 'mainmenu-btn-patients',
                // TODO: tooltip-et hozzáadni
                iconCls: 'icon-patients-menu_large'
            });
        }
        // Queries button
        if (Kron.core.UserManagement.hasPermission("Queries", "getOpenQueries"))
        {
            this.add({
                text: "<b>"+_("Query-k")+"</b>",
                minWidth: 90,
                itemId: 'mainmenu-btn-queries',
                // TODO: tooltip-et hozzáadni
                iconCls: 'icon-queries-menu_large'
            });
        }
        // Messages button
        //this.add({
        //    if (Kron.core.UserManagement.hasPermission("Messages", "getReceivedMessages") ||
        //        Kron.core.UserManagement.hasPermission("Messages", "getSentMessages"))
        //    {
        //    text: "<b>"+_("Üzenetek")+"</b>",
        //    minWidth: 100,
        //    itemId: 'mainmenu-btn-messages',
        //    iconCls: 'icon-messages-menu_large'
        //});
        //}
        // Documents button
        if (Kron.core.UserManagement.hasPermission("Documents", "getDocuments"))
        {
            this.add({
                text: "<b>"+_("Dokumentumok")+"</b>",
                minWidth: 110,
                itemId: 'mainmenu-btn-documents',
                tooltip: _("Dokumentumok és fájlok letöltése"),
                iconCls: 'icon-documents-menu_large'
            });
        }
        // Contacts button
        this.add({
            text: "<b>"+_("Kapcsolatok")+"</b>",
            minWidth: 100,
            itemId: 'mainmenu-btn-contacts',
            tooltip: _("Kapcsolatok és technikai segítségnyújtás"),
            iconCls: 'icon-contacts-menu_large'
        });
        
        // Reports button
       //  egyelőre kiszedve
        this.add({
            text: "<b>"+_("Riportok")+"</b>",
            minWidth: 90,
            //disabled: false,
            itemId: 'mainmenu-btn-reports',
            // TODO: tooltip-et hozzáadni
            iconCls: 'icon-reports-menu_large'
        });
        
        // Reports button
        this.add({
            text: "<b>"+_("Beállítások")+"</b>",
            minWidth: 90,
            itemId: 'mainmenu-btn-preferences',
            tooltip: _("A saját beállítások megváltoztatása"),
            iconCls: 'icon-preferences-menu_large'
        });
        
        this.add({
			xtype: 'tbfill'
        });
        // Logout button
        if (Kron.core.UserManagement.hasPermission("UserManagement", "logout"))
        {
            this.add({
                text: "<b>"+_("Kijelentkezés")+"</b>",
                minWidth: 90,
                itemId: 'mainmenu-btn-signout',
                tooltip: _("Kijelentkezés az alkalmazásból"),
                iconCls: 'icon-logout-menu_large',
                handler: this.onLogout
            });

        }
        
    }
});
