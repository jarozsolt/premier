/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FieldHistories.js
 *
 * Az összes form közös store-a
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
Ext.define('Kron.store.FieldHistories', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.FieldHistory',
    model : 'Kron.model.FieldHistory',
    storeId : 'fieldhistories',
	
	autoLoad : false,
    proxy : {
        type : 'direct',
        extraParams : {
            form_instanceid : '',
            fieldname: ''
        },
        api : {
   //         create : Kron.Forms.createForm,
            read : Kron.Forms.getFieldHistories
   //         update : Kron.Forms.updateForm,
  //          destroy : Kron.Forms.deleteForm
        },
        reader : {
            root : 'data'
         //   totalProperty : 'totalCount'
        }
    },
    sorters: [{
        property: 'datetime'
        }]
        
});
