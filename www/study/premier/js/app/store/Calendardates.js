/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Calendardates.js
 *
 * Calendardate store
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.store.Calendardates', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Calendardate',
    model : 'Kron.model.Calendardate',

    proxy : {
        type : 'direct',
        extraParams : {
            startDate : '',
            numberOfDays : 7
        },
        directFn : Kron.Visits.getCalendardates,
        reader : {
            root : 'calendardates'
        }
   }
});