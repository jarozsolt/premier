/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Overduedates.js
 *
 * Overdue visit dates store
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.store.Overduedates', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Calendardate',
    model : 'Kron.model.Calendardate',

    proxy : {
        type : 'direct',
        directFn : Kron.Visits.getOverdueVisits,
        reader : {
            root : 'overduedates'
        }
   }
});