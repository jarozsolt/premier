/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FormFields.js
 *
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
Ext.define('Kron.store.FormFields', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Field',
    model : 'Kron.model.Field',
    storeId : 'FormFields',
	
	autoLoad : false
  
});
