/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Patients.js
 *
 * Patients store
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.store.Patients', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Patient',
    model : 'Kron.model.Patient',

    storeId : 'Patients',

  /*  proxy : {
        type : 'direct',
        directFn : Kron.Patients.getPatients,
        reader : {
            root : 'results'
        }
    }*/
   
 //  buffered: true,
   pageSize: 100,
  // autoLoad: false,
   
   proxy : {
        type : 'direct',
       
        api : {
         //   create : 
            read : Kron.Patients.getPatients,
            update : Kron.Patients.updatePatient
         //   destroy : 
        },
        reader : {
            root : 'data',
            totalProperty : 'totalCount'
        }
   }
  

});
