/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FieldQueries.js
 *
 * Egy mező query-jeinek store-ja
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
Ext.define('Kron.store.FieldQueries', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.FieldQuery',
    model : 'Kron.model.FieldQuery',
    storeId : 'fieldqueries',
	
	autoLoad : false,
    proxy : {
        type : 'direct',
        extraParams : {
            form_instanceid : ''
        },
        api : {
            create : Kron.Queries.createQuery,
            read : Kron.Queries.getFormQueries,
            update : Kron.Queries.updateQuery,
            destroy : Kron.Queries.deleteQuery
        },
        reader : {
            root : 'data'
        }
    },
    sortOnFilter: true,
    sorters: [{
        property: 'query_iid',
        direction: 'DESC'
        },{
        property: 'created',
        direction: 'ASC'
        }]
        
});
