/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Documents.js
 *
 * Documents store
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.store.Documents', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Document',
    model : 'Kron.model.Document'
}); 

