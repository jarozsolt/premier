/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * OpenQueries.js
 *
 * OpenQueries store
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.store.OpenQueries', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.QuerySummary',
    model : 'Kron.model.QuerySummary',

    proxy : {
        type : 'direct',
        directFn : Kron.Queries.getOpenQueries
    }
});