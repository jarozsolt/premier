/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Forms.js
 *
 * Az összes form közös store-a
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.define('Kron.store.Forms', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Form',
    model : 'Kron.model.Form',
    storeId : 'Forms',

    proxy : {
        type : 'direct',
        extraParams : {
            patientid : '',
            instanceid : ''
        },
        api : {
            create : Kron.Forms.createForm,
            read : Kron.Forms.getForms,
            update : Kron.Forms.updateForm,
            destroy : Kron.Forms.deleteForm
        },
        reader : {
            root : 'data',
            totalProperty : 'totalCount'
        }
    },
    sorters: [{
        property: 'duedate'
        }]
        
});
