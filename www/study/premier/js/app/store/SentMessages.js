/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * SentMessages.js
 *
 * SentMessages store
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.store.SentMessages', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.SentMessage',
    model : 'Kron.model.SentMessage',

    storeId : 'SentMessages',

    proxy : {
        type : 'direct',
        directFn : Kron.Messages.getSentMessages
    }
});
