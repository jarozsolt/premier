/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Contacts.js
 *
 * Contacts store
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.store.Contacts', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Contact',
    model : 'Kron.model.Contact'
}); 

