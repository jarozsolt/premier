/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * ReceivedMessages.js
 *
 * ReceivedMessages store
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.store.ReceivedMessages', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Message',
    model : 'Kron.model.Message',

    storeId : 'ReceivedMessages',

    proxy : {
        type : 'direct',
        directFn : Kron.Messages.getReceivedMessages
    }
});
