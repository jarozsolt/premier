/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Reports.js
 *
 * Reports store
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.store.Reports', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Report',
    model : 'Kron.model.Report'
    
}); 

