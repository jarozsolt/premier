/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Messages.js
 *
 * Messages store
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.store.Messages', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Message',
    model : 'Kron.model.Message'
});
