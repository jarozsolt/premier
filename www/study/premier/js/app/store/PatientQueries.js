/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * PatientQueries.js
 *
 * PatientQueries store
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.store.PatientQueries', {
    extend : 'Ext.data.Store',
    requires : 'Kron.model.Query',
    model : 'Kron.model.Query',

	extraParams : {
            patientid : ''
    },
    proxy : {
        type : 'direct',
        directFn : Kron.Queries.getPatientQueries
    },
    autoLoad: false
});
