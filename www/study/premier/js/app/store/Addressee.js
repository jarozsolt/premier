/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Addressee.js
 *
 * Addressee  store
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.store.Addressee', {
    extend : 'Ext.data.TreeStore',
    requires : ['Kron.model.Addressee'],
    model : 'Kron.model.Addressee',

    storeId : 'addressee',

    proxy : {
        type : 'direct',
        directFn : Kron.Addressees.getAddressees
    }
});
