/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Query.js
 *
 * Query data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.model.Query', {
    extend : 'Ext.data.Model',
    fields : [{
        name : 'id',
        type : 'string'
    }, {
        name : 'siteid',
        type : 'string'
    }, {
        name : 'patientid',
        type : 'string'
    },{
        name : 'formtype',
        type : 'string'
    },{
        name : 'instanceid',
        type : 'int'
    },{
        name : 'fieldname',
        type : 'string'
    },{
        name : 'fieldlabel',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    },{
        name : 'created',
        type : 'date',
        dateFormat : "Y-m-d H:i:s"
    },{
        name : 'text',
        type : 'string'}
    ]
});