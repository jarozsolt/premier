/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Patient.js
 *
 * Patient data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 *---------------------------------------------------------------
 */


Ext.define('Kron.model.Patient', {
    extend : 'Ext.data.Model',
    fields : ['id', 'siteid', 'patientid', 'status',
    	{
	        name : 'isclosed',
	        type : 'boolean'
    	},{
			name : 'enrollmentdate',
			type : 'date',
			dateFormat : "Y-m-d"
		},/* {
			name : 'next_visit_date',
			type : 'date',
			dateFormat : "Y-m-d"
		}, */
		'visit_status',
		{
			name : 'openqueries',
			type : 'int'
		},{
			name : 'answeredqueries',
			type : 'int'
		}]
});
