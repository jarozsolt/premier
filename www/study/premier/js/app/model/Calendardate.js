/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Calendardate.js
 *
 * Todo Calendar Date model
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.model.Calendardate', {
    extend : 'Ext.data.Model',
    fields : ['id', 
    {
        name : 'calendardate',
        type : 'date',
        dateFormat : "Y-m-d"
    }],
    requires : 'Kron.model.Calendarvisit',
    hasMany: { model: 'Kron.model.Calendarvisit', name: 'calendarvisits' }
});
