/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FieldHistory.js
 *
 * Egy form field historyja 
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
Ext.define('Kron.model.FieldHistory', {
    extend : 'Ext.data.Model',
     fields : [	'id',
            'parent',
            'fieldname',
            {
		        name : 'datetime',
		        type : 'date',
		        dateFormat : "Y-m-d H:i:s"
            },
			'user',
			'action',
			'text',
			'type']

});
