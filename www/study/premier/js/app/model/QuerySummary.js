/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * QuerySummary.js
 *
 * QuerySummary data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.model.QuerySummary', {
    extend : 'Ext.data.Model',
    fields : [{
        name : 'id',
        type : 'string'
    }, {
        name : 'sitename',
        type : 'string'
    }, {
        name : 'patientid',
        type : 'string'
    },{
        name : 'openqueries',
        type : 'int'
    },{
        name : 'answeredqueries',
        type : 'int'
    }]
});
