/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * SentMessage.js
 *
 * SentMessage data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.model.SentMessage', {
    extend : 'Ext.data.Model',
    fields : ['msgid', 'created', 'subject', 'text', 'addressees']
});
