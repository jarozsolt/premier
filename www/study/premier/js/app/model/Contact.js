/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Contact.js
 *
 * Contact data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 *---------------------------------------------------------------
 */

Ext.define('Kron.model.Contact', {
    extend : 'Ext.data.Model',
    fields : ['id', 'header', 'name', 'position', 'mobile', 'email', 'company', 'address1', 'address2', 'phone', 'fax'],

    proxy : {
        type : 'direct',
        directFn : Kron.Contacts.getContacts
    }
});
