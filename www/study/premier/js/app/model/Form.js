/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Form.js
 *
 * General form data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */

Ext.namespace('Kron').recordStatus = {
    "Empty" : 'E',
    "New" : 'N',
    "Stored" : 'S',
    "Deleted" : 'D'
};

Ext.define('Kron.model.Form', {
    extend : 'Ext.data.Model',
    // Header info - small caps only
    fields : [{
        name : 'id',
        type : 'integer'
    }, {
        name : 'instanceid',
        type : 'integer'
    }, {
        name : 'patientid',
        type : 'string'
    }, {
        name : 'formtype',
        type : 'string'
    }, {
        name : 'istmp',
        type : 'integer',
        defaultValue : 1
    },{
        name : 'status',
        type : 'string',
        defaultValue : 'N'
    }, {
        name : 'validity',
        type : 'string',
        defaultValue : 'N'
    }, {
        name : 'created',
        type : 'date',
        dateFormat : "Y-m-d H:i:s"
    }, {
        name : 'duedate',
        type : 'date',
        dateFormat : "Y-m-d"
    }, {
        name : 'signed',
        type : 'date',
        dateFormat : "Y-m-d H:i:s"
    }, {
        name : 'signed_by',
        type : 'string'
    }, {
        name : 'reviewed',
        type : 'date',
        dateFormat : "Y-m-d H:i:s"
    }, {
        name : 'reviewed_by',
        type : 'string'
    }, {
        name : 'approved',
        type : 'date',
        dateFormat : "Y-m-d H:i:s"
    }, {
        name : 'approved_by',
        type : 'string'
    }, {
        name : 'parentid',
        type : 'integer'
    }],
    
    requires : 'Kron.model.Field',
    hasMany: { model: 'Kron.model.Field', name: 'formfields' },
    idProperty : 'id',
    clientIdProperty: 'clientid',   
    
/**
 *	A mező érték, mint objektum alapjén  visszaadja stringben az értéket és a típust
 *  @param {object} fieldValue 
 *  returns {object} value: string
 *						 type: D|B|I|S|	
 */
	getStringValueType : function(fieldValue) { "use strict";
		var returnValue = {
				value : '',
				type : ''
			};
		if (Ext.isDate(fieldValue)) {
			if (fieldValue.getHours() == 0 && fieldValue.getMinutes() == 0 && fieldValue.getSeconds() == 0 ) {
				returnValue.type = 'D';
			} else {
				returnValue.type = 'T';
			}
			returnValue.value = Ext.Date.format(fieldValue, "Y-m-d H:i:s" ); //'Y.m.d');
			
		} else if(Ext.isBoolean(fieldValue)) {
			returnValue.type = 'B';
			returnValue.value = (fieldValue ? '1' : '0');
		} else if(Ext.isNumber(fieldValue)) {
			returnValue.type = 'I';
			returnValue.value = fieldValue.toString();
		} else {
			returnValue.type = 'S';
			returnValue.value = fieldValue;
		}
		return returnValue;
	},
    
/**	A mező érték, mint objektum alapjén  visszaadja stringben az értéket és a típust
 * @param {string} fieldValue 
 * @param {string} fieldType D|B|I|S|	
 * returns {object} fieldValue
 */
	getObjectValue : function(fieldValue, fieldType) {"use strict";
		var returnValue;
		if (fieldType === 'D'  || fieldType === 'T' ) { // date
			returnValue = Ext.Date.parse(fieldValue, "Y-m-d H:i:s");
		} else if (fieldType === 'B') { // boolean
			returnValue = ( fieldValue === '1'? true : false ) ;
		} else if (fieldType === 'I') { // boolean
		    //Nem jó, mert a nem egész számoknál pl. testhőmérsékletnél levág
			//returnValue = parseInt(fieldValue, 10);
			returnValue = parseFloat(fieldValue);
		} else {
			returnValue = fieldValue;
		}
		return returnValue;
	},
	
	getFieldRecordValueAsObject : function(fieldRecord) {"use strict";
        var fieldValue, fieldType,
        returnValue = undefined;
        
        if (fieldRecord !== undefined) {
            fieldValue = fieldRecord.get('fieldvalue');
            if (fieldValue !== undefined) {
                fieldType = fieldRecord.get('fieldtype');
                
                if (fieldType === 'D'  || fieldType === 'T' ) { // date
                    returnValue = Ext.Date.parse(fieldValue, "Y-m-d H:i:s");
                } else if (fieldType === 'B') { // boolean
                    returnValue = ( fieldValue === '1'? true : false ) ;
                } else if (fieldType === 'I') { // boolean
                    //Nem jó, mert a nem egész számoknál pl. testhőmérsékletnél levág
                    //returnValue = parseInt(fieldValue, 10);
                    returnValue = parseFloat(fieldValue);
                } else {
                    returnValue = fieldValue;
                }
            }
        }
        return returnValue;
    },
    
/**
 * A form record forfields store-ából kiszedi a megadott nevű mező értékét (csak eredeti rekordnak)
 * @param {Object} fieldName : mező neve
 * @param {boolean} isTmp : ha true - temp rekordot próbálja kiszedni
 */
	getFieldValue : function(fieldName, isTmp) { "use strict";
	   // CSL a default értéket átírtam undefined-re
		var field, index,
		returnValue = undefined,
		isTemp = ( isTmp ? '1' : '0' );
		index =  this.formfields().findBy(function(record) {
			return ((record.get('fieldname') == fieldName && record.get('istmp') == isTemp ) ? true: false);
		});

		if  (index !== undefined && index >= 0 ) {
			field = this.formfields().getAt(index);
			if (field !== undefined) {
			    returnValue = this.getFieldRecordValueAsObject(field);
			    // CSL egyszerusitettem
				//returnValue = this.getObjectValue(field.get('fieldvalue'),field.get('fieldtype'));
			}
		} 
		return returnValue;
	},
	
 /**
 * A form record forfields store-ából kiszedi a megadott nevű mező statusát 
 * @param {Object} fieldName
 */
	getFieldStatus : function(fieldName) { "use strict";
		var returnValue = '',
			field,
			n;
		n =  this.formfields().findBy(function(record) {
			return ((record.get('fieldname') == fieldName && record.get('istmp') == 0 ) ? true: false);
		});
		if  ( n>= 0 ) { 
			field = this.formfields().getAt(n);
			if (field) {
				returnValue = field.get('status');
			}
		} 
		return returnValue;
	},
 /**
 * A form record forfields store-ából kiszedi a megadott nevű mező querystatusát 
 * @param {Object} fieldName
 */
    getFieldQueryStatus : function(fieldName) { "use strict";
        var returnValue = '',
            field,
            n;
        n =  this.formfields().findBy(function(record) {
            return ((record.get('fieldname') == fieldName && record.get('istmp') == 0 ) ? true: false);
        });
        if  ( n>= 0 ) { 
            field = this.formfields().getAt(n);
            if (field) {
                returnValue = field.get('querystatus');
            }
        } 
        return returnValue;
    },
/**
 * Ha nincs megadva a fieldname akkor a formra vonatkozik 
 */    
    hasOpenOrAnsweredQuery : function(fieldName) { "use strict";
        var queryStatus, 
            returnValue = false,
            n;
        if(fieldName !== undefined && fieldName !== null) {
            queryStatus = this.getFieldQueryStatus(fieldName);
            returnValue = ((queryStatus.indexOf(Kron.queryStatus.Open) != -1 || queryStatus.indexOf(Kron.queryStatus.Reopened) != -1 ||queryStatus.indexOf(Kron.queryStatus.Answered)!=-1 ) ? true: false);
        } else {
             
            n =  this.formfields().findBy(function(record) {
                var queryStatus = record.get('querystatus');
                return ((record.get('istmp') == 0 &&  (queryStatus.indexOf(Kron.queryStatus.Open) != -1 || queryStatus.indexOf(Kron.queryStatus.Reopened) != -1 ||queryStatus.indexOf(Kron.queryStatus.Answered)!=-1 ) ) ? true: false);
            });
            if  ( n>= 0 ) { 
                returnValue = true;
            } 
        }
        return returnValue;
        
    },
    
    isFieldMonitorable : function(fieldName) { "use strict";
        var fieldStatus = this.getFieldStatus(fieldName);
        // Ha már monitorozott, akkor nem
        if (fieldStatus === Kron.fieldStatus.MonitorApproved) {
            return false;
        }
        // Ha van hozzá query (nyitott vagy megválaszolt), akkor nem
        if (this.hasOpenOrAnsweredQuery(fieldName)) {
            return false;
        }
        //Egyébként monitorozható
        return true;
    },



/**
 * A form record forfields store-ában beállítja a megadott nevű mező querystatus-át 
 * @param {Object} fieldName
 */
    setFieldQueryStatus : function( status, fieldDetailsBtn) { "use strict";
        var returnValue = false,
            field,fieldName,
            n;
        if (fieldDetailsBtn)  {
            fieldName = fieldDetailsBtn.getFieldName();
            if (fieldName !== '') {
                n =  this.formfields().findBy(function(record) {
                    return ((record.get('fieldname') == fieldName && record.get('istmp') == 0 ) ? true: false);
                });
                if  ( n>= 0 ) { 
                    field = this.formfields().getAt(n);
                    if (field) {
                        field.set('querystatus', status);
                        this.updateFieldDetailsBtn( fieldDetailsBtn);
                        returnValue = true;
                    }
                } 
            }
        }
        return returnValue;
    },
	
	
	
/**
 * A form record forfields store-ában frissíti a megadott nevű mező querystatus-át a queriesStore alapján
 * Ha van open vagy reopen vagy answered benne, és monitor approved a field status, azt is visszateszi openre lokálisan (server oldalon az updatequery megteszi, amikor reopened-re áll a query)
 * Ha még üres rekordot queriztem, akkor csinálok egyet lokálisan (ez elvileg nem megy fel a szerverre, hisz a monitor nem módosít formot, más meg nem tud ilyet csinálni)
 * @param {Object} fieldName
 */
    updateFieldQueryStatus : function(fieldDetailsBtn ) { "use strict"; //btnContainer
        var returnValue = false,
            field, 
            n, fieldName,queryDlg,
            queryStatus='',
            fieldQueriesStore = Ext.data.StoreManager.lookup('FieldQueries'),
            referencedField,
            monitorBtn;
        if (fieldDetailsBtn)  {
        	referencedField = fieldDetailsBtn.getReferencedField(); 
            monitorBtn = fieldDetailsBtn.getMonitorCheckBtn();
            fieldName = referencedField.getName();
 
            if (fieldQueriesStore && fieldName) {
                fieldQueriesStore.each(function(rec){
                    if ( rec.get('fieldname') == fieldName) {
                        queryStatus = queryStatus + rec.get('status');
                    }
                });            
             
                n =  this.formfields().findBy(function(record) {
                    return ((record.get('fieldname') == fieldName && record.get('istmp') == 0 ) ? true: false);
                });
                if  ( n>= 0 ) { 
                    field = this.formfields().getAt(n);
                    if (field) {
                        field.set('querystatus', queryStatus);
                        if (queryStatus !== undefined && (queryStatus.indexOf(Kron.queryStatus.Open) != -1 || queryStatus.indexOf(Kron.queryStatus.Reopened) != -1 || queryStatus.indexOf(Kron.queryStatus.Answered) != -1 )){
                            if  (field.get('status') === Kron.fieldStatus.MonitorApproved ) {
                                field.set('status', Kron.fieldStatus.Reopened);
                                
                                queryDlg = Ext.getCmp('querydlg');
                                if (queryDlg) {
                                    queryDlg.setFieldStatusText(Kron.fieldStatus.Reopened);
                                }
                            }
                            // ha alá volt írva a form, le kell szedni róla
                            if (this.isSigned()) {
                                this.set('signed_by', null);
                                this.set('signed', null);

                            }
                        }                       
                        this.updateFieldDetailsBtn(fieldDetailsBtn);
                        returnValue = true;
                    } 
                } else { // üres mezőt queriztem 
                    if (queryStatus !== undefined && queryStatus !== '') {
                        
                        field = Ext.create('Kron.model.Field', {
                            form_instanceid : this.get('instanceid'),
                            fieldname : fieldName,
                            fieldvalue : '',
                            fieldtype : '',
                            istmp : 0,
                            status : '',
                            querystatus : queryStatus
                        });
                        this.formfields().add(field);
                        this.updateFieldDetailsBtn(fieldDetailsBtn );
                        returnValue = true;
                    }
               }
           }            
        }    
        return returnValue;
    },	
/*	
	addFieldButton : function( bContainer, targetId) {"use strict";
		if (bContainer) {
			console.log('haha',bContainer);
			console.log(bContainer.add({
	                xtype : 'button',
	                fdbutton: true,
	                isAligned: false,
	                targetId: targetId,
	                margin : this.buttonMargin?this.buttonMargin:'0',
	                tabIndex: -1,
	                width : 30
	           }).getEl());
		}
		
	},*/
	
	updateFieldDetailsBtn : function(fieldDetailsBtn ) {"use strict";//btnContainer
	    if (fieldDetailsBtn) {
	    	var referencedField = fieldDetailsBtn.getReferencedField(),
            	monitorChkBtn = fieldDetailsBtn.getMonitorCheckBtn(),
	           	fieldName,
	           	fieldStatus, queryStatus,
	           	showBtn = false;
			//console.log('fieldDetailsBtn',fieldDetailsBtn);
			//console.log('referencedField',referencedField);
	        if ( referencedField){
	        	fieldName = referencedField.getName();
	        }
	        if (!fieldName) {
	        	console.log("updateFieldDetailsBtn error itt: ", fieldDetailsBtn);
	        } else {
	            queryStatus = this.getFieldQueryStatus(fieldName);
	            fieldStatus = this.getFieldStatus(fieldName);
	            
                fieldDetailsBtn.removeCls('fielddetailsbutton');
                fieldDetailsBtn.removeCls('fielddetailsbutton-answered');
                fieldDetailsBtn.removeCls('fielddetailsbutton-queried'); 
                fieldDetailsBtn.removeCls('icon-fieldhistory-btn_small'); 
                
                if ((queryStatus.indexOf(Kron.queryStatus.Open) != -1 || queryStatus.indexOf(Kron.queryStatus.Reopened) != -1 ) && queryStatus.indexOf(Kron.queryStatus.Answered) != -1) {
                    fieldDetailsBtn.setIconCls('fielddetailsbutton-queried-answered'); // @todo change it
                    fieldDetailsBtn.setTooltip(_("Nyitott és megválaszolt query"));
                } else if (queryStatus.indexOf(Kron.queryStatus.Open) != -1 || queryStatus.indexOf(Kron.queryStatus.Reopened) != -1) {
                    fieldDetailsBtn.setIconCls('fielddetailsbutton-queried'); 
                    fieldDetailsBtn.setTooltip(_("Nyitott query"));
                } else if (queryStatus.indexOf(Kron.queryStatus.Answered) != -1) {
                    fieldDetailsBtn.setIconCls('fielddetailsbutton-answered');
                    
                } else {
                    fieldDetailsBtn.setIconCls('icon-fieldhistory-btn_small');
                    fieldDetailsBtn.setTooltip(_("Mező történet"));
                }   
           
           		
           
           /*		// closed patient lekezelése - el kell vennI A MENüBŐL AZ ADD QUERY-T (0. ELEM)
				var  patientView=Ext.getCmp('patientview');
		       	if (patientView!== undefined) {
		       		var patientRecord = patientView.getPatientRecord()	
		       	}

                if (fieldDetailsBtn.isHidden()) {
                    fieldDetailsBtn.show();
                }
                */
               

                // monitorcheck btn megjelenítése
                if (monitorChkBtn) {
                //	console.log('fieldStatus', fieldStatus,fieldStatus=='' );
                     if (fieldStatus == Kron.fieldStatus.MonitorApproved ) {
                        showBtn = true;
                        monitorChkBtn.toggle(true);
                        // Zavaró a checknél a tooltip
                        //monitorChkBtn.setTooltip(_("Monitor által jóváhagyva"));
                        
                    } else {
                        showBtn = false;
                        monitorChkBtn.toggle(false);
                        monitorChkBtn.setDisabled(true);
                        // Zavaró a checknél a tooltip
                        //monitorChkBtn.setTooltip(_("Monitor által nincs jóváhagyva"));
                    }
                    if ( Kron.core.UserManagement.hasPermission("Forms", "monitorForm") ) {
                        
                        if (queryStatus != undefined && (queryStatus.indexOf(Kron.queryStatus.Open) != -1 || queryStatus.indexOf(Kron.queryStatus.Reopened) != -1 || queryStatus.indexOf(Kron.queryStatus.Answered) != -1 ) && fieldStatus != Kron.fieldStatus.MonitorApproved ) {
                           showBtn = false;
                           monitorChkBtn.setDisabled(true); 
                         } else if (fieldStatus !== '' ){ 
                           showBtn = true;
                           monitorChkBtn.enable();
                        }
                    } else {
                        monitorChkBtn.setDisabled(true); 
                    }
                    // ha lezárt a beteg, akkor mindenképp disabled a monitorChkBtn
                   	var patientView=Ext.getCmp('patientview');
			       	if (patientView!== undefined) {
			       		var patientRecord = patientView.getPatientRecord()	
			       	}
			        if (patientRecord !== undefined ) { 
			        	if ( patientRecord.get('isclosed')  ) {
			        	 	monitorChkBtn.setDisabled(true); 
			            } 
			        }
                    if (showBtn) {
                    	// @todo kelle ez?
                     //   btnContainer.onControlFieldEnabled();
                     //         if (monitorChkBtn.isHidden()) { 
                   // console.log('monitorChkBtn hidden',monitorChkBtn);
                //        }
                    } else {
                    	// @todo kelle ez?
                     //  btnContainer.onControlFieldDisabled();
                    }

                }
            }
        // query status változás miatt is kellhet updatelni a tab fül ikkont
   		fieldDetailsBtn.up('form').updateTabIcon(fieldDetailsBtn.up('panel'));
	    }	    
	},
	
	
/**
 * monitorcheck vagy details button alapján visszaadja a státuszt
 */		
 
 /* nem kell		
	getStatusByBtn : function(btn) { "use strict";
		var fieldName;
		if (btn) {
			fieldName = btn.getFieldName();
			if (fieldName ) {
				return this.getFieldStatus(fieldName);
			}
		} 
		return undefined;	
	},*/
 /**
 * Egy eredeti form rekord statusát állítja át a megadottra, nem kezel temp rekordot
 * A monitor jóváhagyás után a lokális store adott rekordját aktualizálom vele, de ugye ez miatt más lesz id (mert a szerver oldalon új id-t kap a db rekord)
 * @param {Object} fieldName
 * @param {string} status
 * returns:
 *  true - ha siekres
 *  false - ha nem
 */
	setFieldStatus : function(fieldName, status) { "use strict";
		var returnValue = false, queryDlg, formField,
			patientDlg = Ext.getCmp('patientdlg'),
			activeForm = patientDlg.getLayout().getActiveItem(),
			field,formfield,
			monitorButton,
			detailsButton,
			n =  this.formfields().findBy(function(record) {
				return ((record.get('fieldname') == fieldName && record.get('istmp') == 0 ) ? true: false);
			});

		if  ( n>= 0 ) {
			field = this.formfields().getAt(n);
			
			if (field && field.get('status')!= status) {
				field.set('status', status);

			/*	// jelölöm a status váltást:
				if ( activeForm ) {
					formField = activeForm.getForm().findField(fieldName);
					if (formField) {
						monitorButton = formField.up('btncontainer').down('monitorcheck');
						if (monitorButton) {
							if (status === Kron.fieldStatus.MonitorApproved ) {
								monitorButton.toggle(true, true); //pressed = true;
							} else {
								monitorButton.toggle(false, true);//pressed = false;
							}
						}
						detailsButton=formField.up('btncontainer').down('fielddetailsbtn');
						if (detailsButton) {
							if (status === Kron.fieldStatus.Queried ) {
								detailsButton.removeCls('fielddetailsbutton');
								detailsButton.removeCls('fielddetailsbutton-answered');
								detailsButton.setIconCls('fielddetailsbutton-queried');
							} else if (status === Kron.fieldStatus.Answered || status === Kron.fieldStatus.Changed ){
								detailsButton.removeCls('fielddetailsbutton');
								detailsButton.removeCls('fielddetailsbutton-queried');
								detailsButton.setIconCls('fielddetailsbutton-answered');
							} else {	
								detailsButton.removeCls('fielddetailsbutton-queried');
								detailsButton.removeCls('fielddetailsbutton-answered');					
								detailsButton.setIconCls('fielddetailsbutton');
							}
						}
					}				
				}	*/	
			}
			returnValue = true;
		} 
		return returnValue;
	},   

 /**
 * A form record forfields store-ában módosítja a megadott nevű mezők statusát ha van temp, annak,
 * ha van orig más statusszal - akkor létrehoz egyet tempet új statussal, ha nincs orig se, akkor nem csinál semmi
 * @param {array} fieldNames
 * @param {string} status
 */
/*nem használjuk, törölni, a végén */
	setFieldStatuses : function(fieldNames, status) { "use strict";
		var storedFields,
			originalField = null,
			isUpdated = false,
			me = this,
			patientDlg = Ext.getCmp('patientdlg'),
			activeForm = patientDlg.getLayout().getActiveItem(),
			field,
			monitorButton,
			detailsButton,
			fieldRecord;
			
		
		Ext.Array.each(fieldNames, function(fieldName){
			storedFields = me.formfields().queryBy(function(record) {
				return (record.get('fieldname') == fieldName ? true: false);
			});
			storedFields.each(function(item, index, length){
				if (item.get('istmp') == 1) {
					if (!isUpdated) {
						// Temp rekord és még nem volt updatelve az változás
						if (item.get('status') != status ) { // csak akkor kell update-elni, ha változott az érték
							item.set('status', status);
						}
						isUpdated = true;
					} else {// biztos ami biztos ág: már volt updatelve, lehet törlni a többi temp rekordot, elég belőlük egy 
						me.formfields().remove(item);
					}
				} else { // van original field - megjegyzem
					originalField = item;
				}
			});
			
			if ((!isUpdated && originalField !== null && originalField.get('status') != status)) {
				fieldRecord = Ext.create('Kron.model.Field', {
					form_instanceid : originalField.get('form_instanceid'),
					fieldname : originalField.get('fieldname'),
					fieldvalue : originalField.get('fieldvalue'),
					fieldtype : originalField.get('fieldtype'),
					istmp : 1,
					status : status
				});
				
				me.formfields().add(fieldRecord);
			}
			
			
			
		});
		
		// jelölöm a status váltást:
		if ( activeForm && fieldNames[0] ) {
			//field = activeForm.getForm().findField(fieldNames[0]);
			field = activeForm.down('[name=' + fieldNames[0] + ']');
			if (field.xtype === 'radiofield' ) {
				field = field.up('radiogroup[name=' + fieldNames[0]+ ']');
				console.log('radiofield-et talált a name-re componens keresés');
			}
			if (field) {
				// @todo: javítani lehetne a performanciáján, ha kell...
				field.up('form').cascade(function(c){
	                if (c.getXType()==='fielddetailsbtn') {
	                    if (c.getFieldName() == fieldName) {
	                       detailsButton = c;
	                       return false;
	                    } 
	                }
	            });
				
				if (detailsButton) {
					monitorButton = detailsButton.getMonitorCheckBtn();
				}
				if (monitorButton) {
					if (status === Kron.fieldStatus.MonitorApproved ) {
						monitorButton.toggle(true, true); //pressed = true;
					} else {
						monitorButton.toggle(false, true);//pressed = false;
					}
				}
				
				if (detailsButton) {
					if (status === Kron.fieldStatus.Queried ) {
						detailsButton.removeCls('fielddetailsbutton');
						detailsButton.removeCls('fielddetailsbutton-answered');
						detailsButton.setIconCls('fielddetailsbutton-queried');
					} else if (status === Kron.fieldStatus.Answered || status === Kron.fieldStatus.Changed ){
						detailsButton.removeCls('fielddetailsbutton');
						detailsButton.removeCls('fielddetailsbutton-queried');
						detailsButton.setIconCls('fielddetailsbutton-answered');
					} else {	
						detailsButton.removeCls('fielddetailsbutton-queried');
						detailsButton.removeCls('fielddetailsbutton-answered');					
						detailsButton.setIconCls('fielddetailsbutton');
					}
				}
			}
			
		}	
			
	},   
	
	
	
/**
 * beállítja a megadott nevű form field értékét a form rekord formfields store-ában 
 * @param {String} fieldName
 * @param {String} fieldValue
 * @param {String} parentName
 */	
/*	setFieldValue : function(fieldName, fieldValue, parentName){ "use strict";
		//console.log('setFieldValue called');console.log(fieldName);console.log(fieldValue);console.log(parentName);
		var type, fieldRecord, originalField = null, me = this, origStatus, newStatus, n, parentRec,
		//	field = this.formfields().findRecord('fieldname',fieldName);
			storedFields =  this.formfields().queryBy(function(record) {
				return (record.get('fieldname') == fieldName ? true: false);
			});
		// Meghatározom a típust, módosítom a  value értékét
		if (Ext.isDate(fieldValue)) {
			type = 'D';
			fieldValue= Ext.Date.format(fieldValue, "Y-m-d H:i:s" ); //'Y.m.d');
		} else if(Ext.isBoolean(fieldValue)) {
			type = 'B';
			fieldValue = (fieldValue ? '1' : '0');
		} else if(Ext.isNumber(fieldValue)) {
			type = 'I';
			fieldValue = fieldValue.toString();
		} else {
			type = 'S';
		}
		var isUpdated = false;
		storedFields.each(function(field, index, length){
			if (field.get('istmp') == 1) {
				if (!isUpdated) {
					// Temp rekord és még nem volt updatelve az változás
					if (field.get('fieldvalue') != fieldValue ) {
						// csak akkor kell update-elni, ha változott az érték
						field.set('fieldvalue', fieldValue);
						field.set('fieldtype', type);
					}
					isUpdated = true;
				} else {// biztos ami biztos ág
					// már volt updatelve, lehet törlni a többi temp rekordot, elég belőlük egy 
					me.formfields().remove(field);
				}
			} else { // van original field - megjegyzem
				originalField = field;
			}
		});
		if ((!isUpdated && originalField === null ) || ( !isUpdated && originalField.get('fieldvalue') != fieldValue)) {// nem volt temp rekord, amit lehetett volna updatelni, új rekord kell (ami fixen istemp=1)
			fieldRecord = Ext.create('Kron.model.Field');
			fieldRecord.set('istmp', 1);
			fieldRecord.set('form_instanceid', this.get('instanceid'));
			fieldRecord.set('fieldname', fieldName);
			fieldRecord.set('fieldvalue', fieldValue);
			fieldRecord.set('fieldtype', type);
			// identify new status 
			
			if ( originalField === null && ( parentName === null || parentName === undefined) ) {
				fieldRecord.set('status', Kron.fieldStatus.Open ); // nem volt orig rekord, és nem gyerek
			} else { // open > open, queried > changed, answered > changed, changed > changed, monitor_approved > open, reopened > open
				if (parentName === null || parentName === undefined ) {// volt orig rekord, és nem gyerek
					origStatus = originalField.get('status');
					if ( origStatus === Kron.fieldStatus.Open || origStatus === Kron.fieldStatus.MonitorApproved || origStatus === Kron.fieldStatus.Reopened ) {
						fieldRecord.set('status', Kron.fieldStatus.Open );
					} else {
						fieldRecord.set('status', Kron.fieldStatus.Changed );
					}
				} else { // gyerek
					fieldRecord.set('parent', parentName);
					// megkeresem az original szülőjét (istmp = 0 rekordot)
					n = this.formfields().findBy(function(item) {
						return ((item.get('fieldname') == parentName && item.get('istmp') != 1) ? true: false);
					});
					if (n>=0) { // volt szülő
						parentRec = this.formfields().getAt(n) ;
						origStatus = parentRec.get('status');
						if ( origStatus === Kron.fieldStatus.Open || origStatus === Kron.fieldStatus.MonitorApproved || origStatus === Kron.fieldStatus.Reopened ) {
							newStatus = Kron.fieldStatus.Open;
						} else {
							newStatus = Kron.fieldStatus.Changed;
						}
					} else { // nem volt szülő
						newStatus = Kron.fieldStatus.Open;
					}
					// ha van istemp szülő rekord
					n = this.formfields().findBy(function(item) {
						return ((item.get('fieldname') == parentName && item.get('istmp') == 1) ? true: false);
					});
					if (n>=0) { // volt temp szülő - azt módosítom,
						parentRec = this.formfields().getAt(n) ;
						parentRec.set('status', newStatus);
					} else { // létrehozok egyet
						parentRec = Ext.create('Kron.model.Field');
						parentRec.set('istmp', 1);
						parentRec.set('form_instanceid', this.get('instanceid'));
						parentRec.set('fieldname', parentName);
						parentRec.set('status', newStatus);
						this.formfields().add(parentRec);
						
					}	
				}
			} // status meghatározás vége
			
			this.formfields().add(fieldRecord);
		}
		// most ugye van egy temp rekord és 0 vagy 1 db eredeti...
    },	
	*/
/**
 * beállítja a megadott nevű form field értékét a form rekord formfields store-ában 
 * legalább a fieldValue-t vagy a fieldStatus-t meg kell adni. 
 * @param {String} fieldName
 * @param {String} fieldValue
 */	
	setFieldValue : function(fieldName, fieldValue){ "use strict";
		//console.log('setFieldValue called');console.log(fieldName);console.log(fieldValue);console.log(parentName);
		var me = this,
			valueObject, 
			origRec = null,
			origStatus = '',
			newStatus = '', 
			n,
			fieldChanged = false;

		// Meghatározom a típust
		valueObject = this.getStringValueType(fieldValue);
		//console.log('this.formfields()');console.log(this.formfields());alert('t');
		// megkeresem a group rekordokat és kiszámolom az új státust
		n =  this.formfields().findBy(function(record) {
				return (record.get('fieldname') == fieldName && record.get('istmp') == 0 ? true: false);
			})
		if (n >= 0) {
			origRec = this.formfields().getAt(n);
			if (origRec !== undefined) {
				origStatus = origRec.get('status');
			}
		}
				
	/*	if ( friends.length > 0 ) {
			friendRecords =  this.formfields().queryBy(function(record) {
					return ( Ext.Array.contains(friends, record.get('fieldname')) && record.get('istmp') == 0 ? true: false);
				});
	//		console.log('friendRecords');console.log(fieldName);console.log(friendRecords);
			if ( origStatus === '' ) {	// ha nem volt original rekord, megnézem a barátait		
				friendRecords.each(function(item) {
					if ( origStatus === '' ) {
						origStatus = item.get('status');
					}
				});
			} 
		}*/
		
		newStatus = origStatus;	

		// módosítom vagy létrehozom az eredeti rekordot
		if (origRec !== undefined && origRec !== null) { // volt eredeti rekord
		    
			if (origRec.get('fieldvalue') != valueObject.value ) { 
				origRec.set('fieldvalue', valueObject.value);
				fieldChanged = true;
			}
			if (origRec.get('fieldtype') != valueObject.type ) { 
				origRec.set('fieldtype', valueObject.type);
				fieldChanged = true;
			}
			if (fieldChanged)  {
			   newStatus = Kron.fieldStatus.Open;
			   
			   if (origRec.get('status') != newStatus ) { 
                    origRec.set('status', newStatus);
                } 
			}
			
			
		}  else { // nem volt eredeti rekord
			origRec = Ext.create('Kron.model.Field', {
				form_instanceid : this.get('instanceid'),
				fieldname : fieldName,
				fieldvalue : valueObject.value,
				fieldtype : valueObject.type,
				istmp : 0,
				status : Kron.fieldStatus.Open
			});
			
			this.formfields().add(origRec);
		}
	/*	// beállítom a barátok státusát is, ha vannak
		if ( friends.length > 0 ) {
			friendRecords.each(function(item) {
				if ( item.get('status') !== newStatus ) {
					item.set('status', newStatus);
				}
			});
		}	*/
    },
    

/**
 * beállítja a megadott fieldname-ek státust 
 * Ha olyat választ ki, aminek nics rekordja (üres mező jóváhagyása) létrehozza lokálisan (serverre nem kell felküldeni,mert ott az approvefield ilyenkor létrehozza)
 * @param {Array} fieldNames tömbben a field nevek
 */
	setGroupStatus : function(fieldNames, newStatus) {"use strict";
		var patientDlg = Ext.getCmp('patientdlg'),
			activeForm = patientDlg.getLayout().getActiveItem(),
			field,
			monitorButton,
			detailsButton,
			n, index, fieldRecord = {},
			records,
			me = this;
		if ( fieldNames.length > 0 ) {
		    Ext.Array.each(fieldNames, function(fieldName){
		        index= me.formfields().findBy(function(record) {
                    return ( fieldName == record.get('fieldname') && record.get('istmp') == 0 ? true: false);
                });
                if  (index == -1 ) {
                // nem létezett, létrehozom 'form_instanceid', 'istmp', 'fieldname', 'parent', 'fieldvalue', 'fieldtype', 'status', 'querystatus'
                    fieldRecord =  Ext.create('Kron.model.Field', { 
                        form_instanceid : me.get('instanceid'),
                        istmp : 0,
                        fieldname : fieldName,
                        fieldvalue : '',
                        fieldtype : '',
                        status : Kron.fieldStatus.MonitorApproved,
                        querystatus : ''
                    });
                    me.formfields().add(fieldRecord);
                }
		    });
		   
		    
		    
			records =  this.formfields().queryBy(function(record) {
				return ( Ext.Array.contains(fieldNames, record.get('fieldname')) && record.get('istmp') == 0 ? true: false);
			});
			records.each(function(item) {
                if ( item.get('status') !== newStatus ) {
				    item.set('status', newStatus);
		        }
			});
			// jelölöm a status váltást:
     /*       if (fieldDetailsBtn) {
                monitorButton = fieldDetailsBtn.getMonitorCheckBtn();
                if (monitorButton) {
                    
                    if (newStatus === Kron.fieldStatus.MonitorApproved ) {
                        monitorButton.toggle(true, true); //pressed = true;
                    } else {
                        monitorButton.toggle(false, true);//pressed = false;
                    }
                }
            }*/
		}	
	},

    
/**
 * temp rekordokat menti el a store-ban
 * nem állít státust - csak arra használom, hogy baj esetén vissza tudjam tölteni a formba
 * @param {Object} fieldName - fieldvalue értékpárok
 */    
    saveTempValues : function(fieldValues){"use strict";
		var valueObject,
			storedFields,
			me = this,
			isUpdated,
			fieldRecord;
			
        Ext.Object.each(fieldValues,function(fieldName, value, myself){
			storedFields =  me.formfields().queryBy(function(record) {
				return ( record.get('fieldname') == fieldName && record.get('istmp') == 1 ? true: false);
			});
			valueObject = me.getStringValueType(value);
			isUpdated = false;
			storedFields.each(function(item, index, length){
				if (!isUpdated) {
					if (item.get('fieldvalue') != valueObject.value ) {
						item.set('fieldvalue', valueObject.value);
					}
					if (item.get('fieldtype') != valueObject.type ) {
						item.set('fieldtype', valueObject.type);
					}
					isUpdated = true;
				} else {// biztos ami biztos ág - ha már volt updatelve, lehet törlni a többi temp rekordot, elég belőlük egy 
					me.formfields().remove(item);
				}
			});
			if (!isUpdated ) {// nem volt temp rekord, amit lehetett volna updatelni, új rekord kell (ami fixen istemp=1)
				fieldRecord = Ext.create('Kron.model.Field', {
					form_instanceid : me.get('instanceid'),
					fieldname : fieldName,
					fieldvalue : valueObject.value,
					fieldtype : valueObject.type,
					istmp : 1,
					status : 'T' // T mint temp rekord - lehet, hogy nem is kellene az isTmp...
				});
				me.formfields().add(fieldRecord);	
			}
        });
    },
/**
 * Végleges mentés a form rekordokra.
 * Kezel státusz váltást is
 * @param {Object} fieldName - fieldvalue értékpárok
 */  
	saveFinalValues: function(fieldValues){"use strict";
	//	console.log('saveFinalValues fieldValues ', fieldValues);
		var me = this, storedFields,
			patientDlg = Ext.getCmp('patientdlg'),
			activeForm = patientDlg.getLayout().getActiveItem(),
			queryStatus ='';
		Ext.Object.each(fieldValues,function(fieldName, value, myself){
			if (value !== null && value !== '' ) { 
			//	me.setFieldValue(fieldName, value, activeForm.getFieldGroup(fieldName));
				me.setFieldValue(fieldName, value);
			} else {// vagy nincs megadva, vagy törölt rekord - utóbbi esetben törölöm a rekordot...
					// @todo: checkbox = false -azt még nem szedem vissza
			//	me.deleteFieldValue(fieldName, activeForm.getFieldGroup(fieldName));
				me.deleteFieldValue(fieldName);
			}
		});
		// végigmegyek a store-on, és ami nincs benne a fieldValues-ba, azt legyűjtöm
		
		storedFields =  this.formfields().queryBy(function(record) {
		//	console.log('storedFields', record.get('fieldname'), fieldValues[record.get('fieldname')]);
			return ( (fieldValues[record.get('fieldname')] === undefined  )? true: false);
		});
		
		// törölöm a storeból (pl. disabled, törölt radiobutton értéke)
		// kivéve azokata rekordokat, amiken van nyitott query -azokra kinullázom az értéket csak, mert különben eltünne a querystatusa is...
		storedFields.each(function(field, index, length){
		//	console.log('idesüss törlendő', field);
			queryStatus = field.get('querystatus');
			if (queryStatus.indexOf(Kron.queryStatus.Open) == -1 && queryStatus.indexOf(Kron.queryStatus.Answered) == -1 && queryStatus.indexOf(Kron.queryStatus.Reopened) == -1 ) {
				me.formfields().remove(field);
			} else {
				field.set('fieldvalue', '');
				field.set('fieldtype', '');
				field.set('status', '');
			}
			
		});
		
	},
	
/**
 * Töröl minden istemp rekordot a from formfields store-ából
 */
    cancelTempItems : function(){ "use strict";
		var me = this,
			storedFields = this.formfields().queryBy(function(record) {
				return ( record.get('istmp') == 1 ? true: false);
			});
		//console.log('cancelTempItems storedFields');console.log(storedFields);alert('t');
		storedFields.each(function(field, index, length){
			me.formfields().remove(field);
		});
	},
	   


/**
 * A megadott mező név alapján töröl minden istemp rekordot
 * @param {String} fieldName
 */
    cancelChanges : function(fieldName){ "use strict";
		var me = this,
			storedFields =  this.formfields().queryBy(function(record) {
				return ( (record.get('fieldname') == fieldName  && record.get('istmp') == 1 )? true: false);
			});
			
		storedFields.each(function(field, index, length){
			me.formfields().remove(field);
		});
	},
	   
/**
 * kitörli a megadott nevő form field-t a form rekord formfields store-ából
 * @param {String} fieldName 
 */    
    deleteFieldValue: function (fieldName) {"use strict";
    //	console.log('deleteFieldValue');console.log(fieldName);
		var origRec = null,
			origStatus = '',
			newStatus = '', 
		//	friends = Ext.Array.remove( group, fieldName),
		//	friendRecords,
			n;

		// megkeresem a group rekordokat és kiszámolom az új státust
		n =  this.formfields().findBy(function(record) {
				return (record.get('fieldname') == fieldName && record.get('istmp') == 0 ? true: false);
			})
		if (n >= 0) {
			origRec = this.formfields().getAt(n);
			if (origRec !== undefined && origRec !== null){
				origStatus = origRec.get('status');
			}
		}
				
	/*	if ( friends.length > 0 ) { // ha vannak barátait	, azoknak ekllhet statust változtani
			friendRecords =  this.formfields().queryBy(function(record) {
					return ( Ext.Array.contains(friends, record.get('fieldname')) && record.get('istmp') == 0 ? true: false);
				});
			if ( origStatus === '' ) {		
				friendRecords.each(function(item) {
					if ( origStatus === '' ) {
						origStatus = item.get('status');
					}
				});
			} 
			if (origStatus === '' || origStatus === Kron.fieldStatus.Open || origStatus === Kron.fieldStatus.MonitorApproved || origStatus === Kron.fieldStatus.Reopened ) { 
				newStatus = Kron.fieldStatus.Open;
			} else {
				newStatus = Kron.fieldStatus.Changed;
			}
		}
*/
		// módosítom vagy létrehozom az eredeti rekordot
		if (origRec !== undefined  && origRec !== null) { // volt eredeti rekord, törlöm
			this.formfields().remove(origRec);
			// beállítom a barátok státusát is, ha vannak, és töröltem
		/*	if ( friends.length > 0 ) {
				Ext.Array.each(friendRecords, function(item) {
					if ( item.get('status') !== newStatus ) {
						item.set('status', newStatus);
					}
				});
			}	
			*/
		}  

    },
/**
 * OK-ra kattintás után érvényesíti a változtatásokat: eredeti rekordokat törli, az temp-eket véglegesíti (istmp = 0)
 * @param {String} fieldName
 */    
	finalizeFieldValue : function(fieldName){ "use strict";
		var newField = null, n, parentRecord, 
			me = this,
			deleteField = false, 
			storedFields =  this.formfields().queryBy(function(record) {
				return (record.get('fieldname') == fieldName ? true: false);
			});
			
		storedFields.each(function(field, index, length){
			if (field.get('istmp') == 1 ) {
				if (field.get('status') === 'D' ) { //@todo: csinosítani
					deleteField = true; // törlés
				}
				else {
					// Temp rekord - véglegesítem
					field.set('istmp', 0);					
				}
				newField = field;
				return false;
			} 
		});
		storedFields.each(function(field, index, length){
			if ((field !== newField && newField !== null ) || (deleteField)) {
				// törlöm a többi rekordot - ha törlés volt - akkor mindet
				me.formfields().remove(field);
			}
		});
    },
   /* 
    addHistoryItem : function (field){
		var historyItem,
			queryGrid = Ext.getCmp('querygrid'),
			fieldHistoryStore = queryGrid.getStore(),
			userprefs = Ext.getCmp('userprefs'),
			action,
			text,
			date = new Date();  
			    
		// OPEN > SETVALUE, CHANGED > SETVALUE,  MONITOR_APPROVED > MONITOR_APPROVE , REOPENED > REOPEN
					
						if ($row['status'] == FrameworkConfig::FIELDSTATUS_OPEN or $row['status'] == FrameworkConfig::FIELDSTATUS_CHANGED ) {
							$history['action']= FrameworkConfig::FIELDACTION_SETVALUE; 
							$history['text'] = $this->convertFieldValue($row['fieldtype'], $row['fieldvalue']);
						} else if ($row['status'] == FrameworkConfig::FIELDSTATUS_MONITOR_APPROVED) {
							$history['action']= FrameworkConfig::FIELDACTION_MONITOR_APPROVE; 
							$history['text']= "";
						} else if ($row['status'] == FrameworkConfig::FIELDSTATUS_REOPENED) {
							$history['action']= FrameworkConfig::FIELDACTION_REOPEN; 
							$history['text']= "";	    
			      
		historyItem = Ext.create('Kron.model.FieldHistory', {
				'fieldname' : field.get('fieldname'),
				'datetime' : Ext.Date.format(date, 'Y-m-d H:i:s'),
                'user' : userprefs.getName(),
                'action' : field.get('status'),
                'text' : field.get('fieldvalue')
		});
        fieldHistoryStore.add(historyItem);
    },
    */

    
/**
 *  Visszaadja egy objektumban a form rekord store-ában lévő összes mezőt mezőt név - éték párokokként
 */
    getFieldValues : function(alsoTempItems) {"use strict";
		var me = this,
			fieldType,
			formFields = {};
		this.formfields().each(function(item) {
				if (!alsoTempItems) {
					if ( item.get('istmp')== 0) {
						formFields[item.get('fieldname')] = me.getObjectValue(item.get('fieldvalue'),item.get('fieldtype'));	
					}
				} else if ( item.get('istmp') == 1 || formFields[item.get('fieldname')] === undefined  ) {
					formFields[item.get('fieldname')] = me.getObjectValue(item.get('fieldvalue'),item.get('fieldtype'));	
				}
				
		});
        return formFields;
    },
    
 /**
 *  Visszaadja, hogy van-e temp rekord a a form rekord formfields store-ában 
 */
    hasTempField : function() {"use strict";
		var returnValue = false;
		this.formfields().each(function(item) {
			if (item.get('istmp') == 1) { 
				returnValue = true;
				return false;
				}
		});
        return returnValue;
    },
    
/**
 * visszaadta a form item fieldjeinek store-ját 
 */
    getFieldStore : function(){"use strict";
		return this.formfields();
	},
    

    isDeleted : function() {"use strict";
        if (this.get('status') === 'D') {
            return true;
        }
        return false;
    },
    

    isEmpty : function() {"use strict";
        if (this.formfields().getCount() == 0 || this.get('status') === Kron.visitStatus.Empty ) {
            return true;
        }
        return false;
    },
    
  
    isNew : function() {"use strict";
        if (this.get('status') === Kron.visitStatus.New) {
            return true;
        }
        return false;
    },
    
    isStored : function() {"use strict";
        if (this.get('status') === Kron.visitStatus.Stored || this.get('status') === Kron.visitStatus.Approved ) {
            return true;
        }
        return false;
    },
    

    
    // returns true if the visit's due date expired - státusz alapján működik
    isMissing : function() {"use strict";
        var actDate = new Date(),
        status = this.get('status'),
        dueDate = this.get('duedate');
        if (status === Kron.visitStatus.Missing) {
            return true;
        }
        if (status === Kron.visitStatus.Empty) {
            if (dueDate  && Ext.Date.clearTime(dueDate).getTime() < Ext.Date.clearTime(actDate).getTime() ) {
                return true;
            }
        }
        return false;
    },
    // returns true if the visit's due date expired - státusz alapján működik
    isCancelled : function() {"use strict";
        if (this.get('status') === Kron.visitStatus.Cancelled) {
            return true;
        }
        return false;
    },

    
    // @todo: replace the 1' with constant
    isTmp : function() {"use strict";
        if (this.get('istmp') == 1) {
            return true;
        }
        return false;
    },
    
    setTmp : function(isTmp) {"use strict";
        if (isTmp === true) {
            this.set('istmp', 1);
        }
        else {
            this.set('istmp', 0);
        }
    },




    // @todo: write the isValid function on server (php) side?
    // returns true for a valid record (all mandatory field are filled)
    // returns false for an invalid record
    isFormValid : function() {"use strict";
        if (this.get('validity') === 'Y') {
            return true;
        }
        return false;
    },
    
    setFormValid : function(isvalid) {"use strict";
        if (isvalid === true) {
            this.set('validity', 'Y');
        }
        else {
            this.set('validity', 'N');
        }
    },
    
    setStatusStored : function(isvalid) {"use strict";
        if (isvalid === true) {
            this.set('validity', 'Y');
        }
        else {
            this.set('validity', 'N');
        }
    },


    hasOpenQuery : function() {"use strict";
        var returnValue = false;
        this.formfields().each(function(record){
			if (record.get('querystatus').indexOf(Kron.queryStatus.Open)!= -1 && !returnValue) {
				returnValue = true;
				return false;
			}
        });
        return returnValue;
    },
    

    hasAnsweredQuery : function() {"use strict";
        var returnValue = false;
        this.formfields().each(function(record){
			if (record.get('querystatus').indexOf(Kron.queryStatus.Answered)!= -1 && !returnValue) {
				returnValue = true;
				return false;
			}
        });
        return returnValue;
    },


    isSigned : function() {"use strict";
        if (this.get('signed_by') !== null && this.get('signed_by') !== '') {
            return true;
        }
        return false;
    },
    
    getFormName: function() {"use strict";
    	var formType = this.get('formtype');
    	if ( (formType=== 'AdverseEvent' || formType=== 'AdverseEventMACCE')  && (this.get('parentid') > 0) ){
			return Kron.formNames[this.get('formtype')] + _(' Follow-Up');
    	}
        return Kron.formNames[this.get('formtype')];
    },

    isUnscheduledVisit : function() {"use strict";
        if (this.get('formtype') === 'UnscheduledVisit') {
            return true;
        }
        return false;
    },

    isNormalVisit : function() {"use strict";
        var formType = this.get('formtype');
        if (Ext.util.Format.substr(formType, 0, 5) === "Visit")
        {
            return true;
        }
        return false;
    },

    isVisit : function() {"use strict";
        return this.isUnscheduledVisit() || this.isNormalVisit();
    },

    isReviewed : function() {"use strict";
        if (this.get('reviewed_by') !== null && this.get('reviewed_by') !== '') {
            return true;
        }
        return false;
    },
    
    isApproved : function() {"use strict";
        var returnValue = false, queryStatus;
        if (this.isFormValid()) {
            returnValue = true;
            this.formfields().each(function(record){
                if (record.get('status') !== Kron.fieldStatus.MonitorApproved ) {
                	queryStatus = record.get('querystatus');
                	// vannak query miatt árnyék rekordok - azokat itt nem kell figyelembe venni
                	if (record.get('status') === '' && queryStatus.indexOf(Kron.queryStatus.Open) == -1 && queryStatus.indexOf(Kron.queryStatus.Answered) == -1 && queryStatus.indexOf(Kron.queryStatus.Reopened) == -1 ) {
                		// ezt a rekordot nem veszem itt figyelembe
                		//console.log('ezt a rekordot nem veszem itt figyelembe', record.data);
                	} else {
                		
	                    returnValue = false;
	                    return false;
                   	}
                }
            });
        }
       
        return returnValue;
    },

/*	getVisitDate : function() {"use strict";
		var returnValue = null;
		var ffields = this.getFormFields();
		if (this.isUnscheduledVisit()){
            if (ffields){
                returnValue = ffields.usvisit_visitdate;
            }
		} else if (this.isNormalVisit()){
			if (ffields){
				var visitid= this.get('formtype').substring(5);
                returnValue = ffields['visit'+visitid +'_visitdate'];
            }
		} 
		return null;
    },

*/
    getDeleteMedicationQuestion : function() {"use strict";
        var medicationname, dose, valueTokens, comboValue;
        medicationname = this.getFieldValue('tradename');
        dose = this.getFieldValue('dose');
        if (dose !== undefined) {
            valueTokens = dose.split('|');
            if (valueTokens[0] !== undefined)
            {
                medicationname += ' - ' + valueTokens[0];
            }
            else
            {
                medicationname += ' - ' + dose;
            }
            if (valueTokens[1] !== undefined && valueTokens[1] !== null)
            {
                comboValue = parseInt(valueTokens[1], 10);
                if (comboValue > 0 && comboValue !== 9) {// nem n.a.
                    medicationname += ' ' + Kron.doseUnits[comboValue-1][1];
                }
            }
        }
        return _("Biztosan törölni akarja ezt az 'Egyéb kezelés'-t?") + '<br><b>' + medicationname + '<b><br>';
    },
    getDeleteAEQuestion : function() {"use strict";
        var diagnosis = this.getFieldValue('diagnosis'),
        	formType = this.get('formtype');
        if ( formType === 'AdverseEventMACCE') {
        	return _("Biztosan törölni akarja ezt az MACCE-t?");
        } else if (this.getFieldValue('ae_serious') === true) {
            return _("Biztosan törölni akarja ezt a 'Súlyos Nemkívánatos Esemény'-t?") + '<br><b>' + diagnosis + '<b><br>';
        }
        return _("Biztosan törölni akarja ezt a 'Nemkívánatos Esemény'-t?") + '<br><b>' + diagnosis + '<b><br>';
    },

    getInformation : function() {"use strict";
        var images = [];
        if (!this.isEmpty() && !this.isNew() && !this.isMissing())
        {
            if (this.isFormValid())
            {
                if (!this.isSigned() && !this.hasOpenOrAnsweredQuery())
                {
                    images[_("Aláírható")] = Kron.imagePath + 'pen_16.png';
                }
            }
            else
            {
                images[_("Részben kitöltött")] = Kron.imagePath + 'warning_16.png';
            }
        }
        if (this.hasOpenQuery())
        {
            images[_("Nyitott query")] = Kron.imagePath + 'open_queries_16.png';
        }
        if (this.hasAnsweredQuery())
        {
            images[_("Megválaszolt query")] = Kron.imagePath + 'answered_queries_16.png';
        }
        return images;
    }
   
});

Ext.namespace('Kron').doseUnits = [
	["1", _("µg")], // microgram
	["2", _("µg/l")],
	["3", _("µl")],
	["4", _("µl/ml")],
	["5", _("ampulla")],
	["6", _("csepp")],
	["7", _("csomag")],
	["8", _("ezer IU")],
	["9", _("fiola")],
	["10", _("g")],
	["11", _("g/kg")], 
	["12", _("g/l")],
	["13", _("g/ml")],
	["14", _("g/mol")],
	["15", _("g/nap")],
	["16", _("gtt")],
	["17", _("IU")],
	["18", _("IU/kg")],
	["19", _("IU/l")],
	["20", _("kg")],
	["21", _("l")], 
	["22", _("l/h")],
	["23", _("l/min")],
	["24", _("l/sec")],
	["25", _("mg")],
	["26", _("mg/kg")],
	["27", _("mg/l")],
	["28", _("mg/ml")],
	["29", _("mg/mmol")],
	["30", _("millió IU")],
	["31", _("ml")], 
	["32", _("ml/h")],
	["33", _("ml/min")],
	["34", _("mmol")],
	["35", _("mmol/kg")],
	["36", _("mol")],
	["37", _("mol/mg")],
	["38", _("mol/ml")],
	["39", _("tabletta")],
	["40", _("U/l")],
	["41", _("U/ml")],
	["42", _("egyéb")],
	["43", _("n.a.")]];
Ext.namespace('Kron').doseFrequency = [["1", _("naponta")],["2", _("hetente")], ["3", _("havonta")], ["4", _("szükség szerint")], ["5", _("egyéb")], ["6", _("nem alkalmazható")]];
Ext.namespace('Kron').indications = {
    "adverse_event" : 1,
    "concomitant_disease" : 2,
    "other" : 3
};

Ext.namespace('Kron').outcomeIds = {
    "Recovered" : 1,
    "Improvement" : 2,
    "Ongoing" : 3,
    "Fatal" : 4,
    "Unknown" : 5,
    "WithSequale" : 6
};
Ext.namespace('Kron').macceTypeTexts = [_("Stroke"),_("Miokardiális infarktus"),_("Ismételt revaszkularizáció")];
Ext.namespace('Kron').outcomeTexts = [[Kron.outcomeIds.Recovered, _("megoldódott / befejeződött")],
	 [Kron.outcomeIds.Improvement, _("javul")], 
	 [Kron.outcomeIds.Ongoing, _("folyamatban")], 
	 [Kron.outcomeIds.Fatal, _("végzetes")], 
	 [Kron.outcomeIds.Unknown, _("ismeretlen")], 
	 [Kron.outcomeIds.WithSequale, _("megoldódott, de következménye van")]];
 
Ext.namespace('Kron').visitDateOffset = 7; //  a vizitek esedékessége közti távolság napban
Ext.namespace('Kron').formNames = [];

Ext.namespace('Kron').patientStatus = {
    "Ongoing" : 'O', 
    "Dropout" : 'D', 
    "Completed" : 'C', 
    "Screenfailure" : 'F'
};
Ext.namespace('Kron').patientStatusTexts = [];
Kron.patientStatusTexts[Kron.patientStatus.Ongoing] = _("Folyamatban");
Kron.patientStatusTexts[Kron.patientStatus.Dropout] = _("Kiesett");
Kron.patientStatusTexts[Kron.patientStatus.Completed] = _("Befejezett");
Kron.patientStatusTexts[Kron.patientStatus.Screenfailure] = _("Téves szűrés");

Kron.formNames.Visit1 = _("Vizit 1");
Kron.formNames.Visit2 = _("Vizit 2");
Kron.formNames.Visit3 = _("Vizit 3");
Kron.formNames.Visit4 = _("Vizsgálat vége oldal");
Kron.formNames.UnscheduledVisit = _("Nemtervezett vizit");
Kron.formNames.AdverseEvent = _("Nemkívánatos Esemény");
Kron.formNames.AdverseEventMACCE = _("MACCE");
Kron.formNames.Medication = _("Egyéb kezelések");
Kron.formNames.MedicalHistory = _("Kórelőzmény, egyéb betegségek");


Ext.namespace('Kron').visitStatus = {
	"New" : 'N', // Ures a vizit és még nem érkezett el az ideje
    "Empty" : 'E', // Ures a vizit és még nem érkezett el az ideje
    "Missing" : 'M', // Ures a vizit és már lejárt az ideje
    "Stored" : 'S', // Nem ures a vizit
    "Completed" : 'C', // valid vizit
    "Signed" : 'H', // valid vizit  - van ilyen ????
    "Approved" : 'A', // Nem ures a vizit és aláírt/monitorozott  - van ilyen ????
    "Locked" : 'L',	// Lezárt a vizit, nem módositható már az adat  - adatbázis zárás után
    "Cancelled" : 'K'
};

Ext.namespace('Kron').visitStatusTexts = [];
Kron.visitStatusTexts[Kron.visitStatus.New] = _("Új");
Kron.visitStatusTexts[Kron.visitStatus.Empty] = _("Üres");
Kron.visitStatusTexts[Kron.visitStatus.Missing] = _("Elmaradt");
Kron.visitStatusTexts[Kron.visitStatus.Stored] = _("Folyamatban");
Kron.visitStatusTexts[Kron.visitStatus.Completed] = _("Kitöltött");
Kron.visitStatusTexts[Kron.visitStatus.Signed] = _("Aláírt");
Kron.visitStatusTexts[Kron.visitStatus.Approved] = _("Jóváhagyott");
Kron.visitStatusTexts[Kron.visitStatus.Locked] = _("Zárolt");
Kron.visitStatusTexts[Kron.visitStatus.Cancelled] = _("Törölt");

Ext.namespace('Kron').queryAction = {
    "Query" : 'Q', 
    "Comment" : 'C'
};
Ext.namespace('Kron').queryActionTexts = [];
Kron.queryActionTexts[Kron.queryAction.Query] = _("Query");
Kron.queryActionTexts[Kron.queryAction.Comment] = _("Válasz");

Ext.namespace('Kron').queryStatus = {
    "Open" : 'O', 
    "Answered" : 'A',
    "Closed" : 'C',
    "Reopened" : 'R'
};
Ext.namespace('Kron').queryStatusTexts = [];
Kron.queryStatusTexts[Kron.queryStatus.Open] = _("Nyitott");
Kron.queryStatusTexts[Kron.queryStatus.Closed] = _("Lezárt");
Kron.queryStatusTexts[Kron.queryStatus.Answered] = _("Megválaszolt");
Kron.queryStatusTexts[Kron.queryStatus.Reopened] = _("Visszanyitott");

Ext.namespace('Kron').fieldStatus = {
    "Open" : 'O', 
    "Queried" : 'Q', 
    "Answered" : 'A',
    "Changed" : 'C', 
    "MonitorApproved" : 'M',
    "Reopened" : 'R'
};
Ext.namespace('Kron').fieldStatusTexts = [];
Kron.fieldStatusTexts[Kron.fieldStatus.Open] = _("Nem monitorozott");
Kron.fieldStatusTexts[Kron.fieldStatus.Queried] = _("Nyitott query");// /@ todo kell ez?
Kron.fieldStatusTexts[Kron.fieldStatus.Answered] = _("Megválaszolt query");
Kron.fieldStatusTexts[Kron.fieldStatus.Changed] = _("Érték megváltozott");
Kron.fieldStatusTexts[Kron.fieldStatus.MonitorApproved] = _("Monitorozva");
Kron.fieldStatusTexts[Kron.fieldStatus.Reopened] = _("Nem monitorozott");

Ext.namespace('Kron').fieldAction = {
    "SetValue" : 'V', 
    "DeleteValue" : 'D',
    "DeleteQuery" : 'K',
    "DeleteComment" : 'L',
    "CloseQuery" : 'F',
    "Query" : 'Q', 
    "Answer" : 'C',
    "MonitorApproval" : 'M',
    "Reopen" : 'R',
    "AnswerQuery" : 'A',
    "ReopenQuery" : 'B'
};	
Ext.namespace('Kron').fieldActionTexts = [];
Kron.fieldActionTexts[Kron.fieldAction.SetValue] = _("Érték megadás");
Kron.fieldActionTexts[Kron.fieldAction.DeleteValue] = _("Érték törlés");
Kron.fieldActionTexts[Kron.fieldAction.DeleteQuery] = _("Query törlés");
Kron.fieldActionTexts[Kron.fieldAction.DeleteComment] = _("Válasz törlés");
Kron.fieldActionTexts[Kron.fieldAction.CloseQuery] = _("Query lezárás");
Kron.fieldActionTexts[Kron.fieldAction.Query] = _("Query hozzáadása");
Kron.fieldActionTexts[Kron.fieldAction.Answer] = _("Válasz hozzáadás");
Kron.fieldActionTexts[Kron.fieldAction.MonitorApproval] = _("Monitor jóváhagyás");
Kron.fieldActionTexts[Kron.fieldAction.Reopen] = _("Monitor jóváhagyás visszavonás");
Kron.fieldActionTexts[Kron.fieldAction.AnswerQuery] = _("Query megválaszoltra állítás");
Kron.fieldActionTexts[Kron.fieldAction.ReopenQuery] = _("Query újranyitás");

Ext.namespace('Kron').userGroup = {
    "Investigator" : 'investigator', 
    "Monitor" : 'monitor',
    "Pharmacovigilance" : 'pharmacov',
    "Nurse" : 'nurse',
    "Sponsor" : 'sponsor',
    "Admin" : 'admin'
};

Ext.namespace('Kron').defaultDateFormat = 'Y.m.d';

Ext.namespace('Kron').treatmentTypeTexts = [[1, _("gyógyszer")], [2, _("műtét")], [3, _("egyéb eljárás (pl. gyógytorna)")], [4, _("egyéb (pl. homeopátia, fitoterápia, stb.)")]];

Ext.namespace('Kron').applicationTypeTexts = [[1, _("szájon át")], [2, _("intravénás")], [3, _("intramuszkuláris")],
    [4, _("szubkután")], [5, _("szublingvális")], [6, _("transdermális")], [7, _("inhalációs")],
    [8, _("nazális")], [9, _("lokális")], [10, _("egyéb")], [11, _("nem alkalmazható")]];

// 'Kezelés oka' combobox értékei
Ext.namespace('Kron').treatmentReasonIds = {
    "medicalhistory" : 1,
    "adverseevent" : 2,
    "other" : 3
};
Ext.namespace('Kron').treatmentReasonTexts = [[Kron.treatmentReasonIds.medicalhistory, _("Kórelőzmény, egyéb társbetegség")], [Kron.treatmentReasonIds.adverseevent, _("Nemkívánatos esemény")], [Kron.treatmentReasonIds.other, _("Egyéb (pl. prevenció)")]];

Ext.namespace('Kron').config = {
                    labelMandatoryMark : ' <span class="req" style="color:red">*</span>'
                };

