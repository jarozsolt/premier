/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Field.js
 *
 * Egy form field adatai
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
Ext.define('Kron.model.Field', {
    extend : 'Ext.data.Model',
     fields : ['id', 'form_instanceid', 'istmp', 'fieldname', 'parent', 'fieldvalue', 'fieldtype', 'status', 'querystatus'], 
     // @todo: kell a form_id?
    belongsTo: 'Kron.model.Form',
    
    idProperty : 'id',
    clientIdProperty: 'clientid',
    
     proxy : {
        type : 'direct',
        extraParams : {
            patientid : ''
        },
        api : {
            create : Kron.Forms.createField,
        //    read : Kron.Forms.logInput, // kell ez? elvileg a form-on keresztül töltöm csak le....
            update : Kron.Forms.updateField,
            destroy : Kron.Forms.deleteField
        },
        reader : {
            root : 'data',
            totalProperty : 'totalCount'
        }
    }
});

