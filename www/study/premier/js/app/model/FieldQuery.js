/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * FieldQuery.js
 *
 * Query data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.model.FieldQuery', {
    extend : 'Ext.data.Model',
    fields : [{
        name : 'id',
        type : 'integer'
    },{
        name : 'query_iid',
        type : 'integer'
    },{
        name : 'form_instanceid',
        type : 'integer'
    },{
        name : 'fieldname',
        type : 'string'
    },{
        name : 'fieldlabel',
        type : 'string'
    },{
        name : 'fieldvalue',
        type : 'string'
    },{
        name : 'fieldtype',
        type : 'string'
    },{
        name : 'text',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    },{
        name : 'action',
        type : 'string'
    },{
        name : 'name',
        type : 'string'
    },{
        name : 'created',
        type : 'date',
        dateFormat : "Y-m-d H:i:s"
    },{
        name : 'created_by',
        type : 'integer'
    }]
});