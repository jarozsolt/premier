/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Report.js
 *
 * Report data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.model.Report', {
    extend : 'Ext.data.Model',
    fields : ['sitename', 'siteid','patientid', 'status', 'enrollmentdate', 'visit_status', 'UnscheduledVisit', 'Medication', 'MedicalHistory', 'AdverseEvent',
        {
            name : 'fields',
            type : 'object' }
    ]

   
});
