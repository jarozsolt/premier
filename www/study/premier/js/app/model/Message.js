/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Message.js
 *
 * Message data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.model.Message', {
    extend : 'Ext.data.Model',
    fields : ['iid', 'created', 'text', 'subject', 'author', 'addressees', 'status'],

    proxy : {
        type : 'ajax',
        url : 'data/messages.json',
        reader : {
            type : 'json',
            root : 'results'
        }
    }
});
