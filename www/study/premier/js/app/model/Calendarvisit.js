/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Calendarvisit.js
 *
 * Todo Calendar Visit model
 *
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.model.Calendarvisit', {
    extend : 'Ext.data.Model',
     fields : ['id', 'calendardate_id', 'instanceid', 'duedate', 'formtype', 'patientid', 'siteid', 'status'],
     belongsTo: 'Kron.model.Calendardate'
});
