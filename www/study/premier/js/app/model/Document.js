/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Document.js
 *
 * Document data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */



Ext.define('Kron.model.Document', {
    extend : 'Ext.data.Model',
    fields : [{
        name : 'subject',
        type : 'string'
    }, {
        name : 'description',
        type : 'string'
    }, {
        name : 'alias',
        type : 'string'
    }, {
        name : 'type',
        type : 'string'
    }],

    proxy : {
        type : 'direct',
        directFn : Kron.Documents.getDocuments
    }
});
