/*jslint nomen: true*/
/*global console: false, Kron: false, _:false*/
/**
 *---------------------------------------------------------------
 * Addressee.js
 *
 * Addressee data model
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


Ext.define('Kron.model.Addressee', {
    extend : 'Ext.data.Model',
    fields : [{
        name : 'name',
        type : 'string'
    }, {
        name : 'usergroup',
        type : 'string'
    }, {
        name : 'uiid',
        type : 'string'
    }, {
        name : 'siteid',
        type : 'string'
    }, {
        name : 'issigner',
        type : 'boolean'
    }]
});

