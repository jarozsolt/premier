<?php
/**
 *---------------------------------------------------------------
 * visits.class.php
 *
 * This file contains the visits class functions
 *
 * @package Kron
 * @subpackage messages
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 * @todo:   filter input!!!!
 * @todo:   
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');

class Visits
{
	/**
     *  This function returns the name of the messages view
     */
	public function getFormsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'forms';
	}
	
	/**
     *  This function returns the name of the queries view
     */
	public function getPatientsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'patients';
	}

	/**
     *  This function returns the name of the users view
     */
	public function getUsersViewName()
	{
		return DatabaseConfig::db_tableprefix . 'users';
	}
	
	 /**
     *  This function returns the name of the sites view
     */
	public function getSitesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'sites';
	}
	 /**
     *  This function returns the name of the messages view
     */
	public function getFormFieldsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields';
	}
	/**
     *  This function returns the name of the messages table
     */
	public function getFormsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'forms_ts';
	}
	
	/**
     *  This function returns the name of the messages table
     */
	public function getFormFieldsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields_ts';
	}
	
	/**
	 *  This function returns the visits of the user's site by date  (in case of investigator, nurse)
	 *  input parameters: 
	 *	$params->startDate - date
	 *	$params->numberOfDays - integer
	 *  @remotable
	 */
	public function getCalendardates(stdClass $params)
    {
    	$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$userGroup=isset($_SESSION['auth_userclass']) ? $_SESSION['auth_userclass'] : '';
		$siteId = isset($_SESSION['auth_siteid']) ? $_SESSION['auth_siteid'] : 0 ;
		$selectedSiteId=isset($_SESSION['auth_selected_siteid']) ? $_SESSION['auth_selected_siteid'] : -1 ;
		$selectedCountryId=isset($_SESSION['auth_selected_countryid']) ? $_SESSION['auth_selected_countryid'] : -1 ;
		
		
 		$logger = Logger::getInstance();
		//$logger->logInfo('getCalendardates called');
		//$logger->logInfo($params);
		//$logger->logInfo($selectedCountryId); 
		//$logger->logInfo($selectedSiteId); 
		 //* @TODO : jogosultság ellenőrzés user láthatja-e patient*/

		$success = false;

		// initialize result calendar data
		
		$calendarData= Array();
		$date = date("Y-m-d", strtotime($params->startDate));
		for ($i=0; $i<$params->numberOfDays; $i++) {
			$oneDay['calendardate'] = $date;
			$oneDay['id'] = $date;
			$oneDay['calendarvisits']= array();
			array_push($calendarData,$oneDay);
			//$date = strtotime(date("Y-m-d", strtotime($date)) . " +1 day");
			$date = date("Y-m-d", strtotime($date . " +1 day"));
		}
		
		try
		{
			$db = new Database();
			if (UserManagement::userHasPermission("UserManagement", "changeSite")) {
		//	if ( $userGroup==FrameworkConfig::USERGROUP_MONITOR || $userGroup==FrameworkConfig::USERGROUP_PHARMACOVIGILANCE || $userGroup==FrameworkConfig::USERGROUP_SPONSOR || $userGroup==FrameworkConfig::USERGROUP_ADMIN ) {
				if 	( $selectedCountryId == 0 ) { // all countries' all sites' visits
					$sql = "SELECT f.id, f.instanceid, f.patientid, f.formtype, f.duedate, f.status, f.validity, p.siteid FROM " . $this->getFormsViewName() . " f INNER JOIN " . $this->getPatientsViewName() . " p ON f.duedate >= ? AND f.duedate< ? AND p.patientid = f.patientid ORDER BY duedate ";
					$db->query($sql, $params->startDate, $date);
				} else {
					if ( $selectedSiteId == 0 ) { // one country's all sites' visits
					$sql = "SELECT f.id, f.instanceid, f.patientid, f.formtype, f.duedate, f.status, f.validity, p.siteid FROM " . $this->getFormsViewName() . " f INNER JOIN " . $this->getPatientsViewName() . " p ON f.duedate >= ? AND f.duedate< ? AND p.patientid = f.patientid INNER JOIN " . $this->getSitesViewName() . " s ON s.sid = p.siteid AND s.countryid = ? ORDER BY f.duedate ";
					$db->query($sql, $params->startDate, $date, $selectedCountryId);
					} else { // one site's visits
						$sql = "SELECT f.id, f.instanceid, f.patientid, f.formtype, f.duedate, f.status, f.validity, p.siteid FROM " . $this->getFormsViewName() . " f INNER JOIN " . $this->getPatientsViewName() . " p ON f.duedate >= ? AND f.duedate< ? AND p.patientid = f.patientid AND p.siteid = ? ORDER BY f.duedate "  ;
						$db->query($sql, $params->startDate, $date, $selectedSiteId);
					}
				}
			} else // investigator, nurse => one site's visits
			{
				$sql = "SELECT f.id, f.instanceid, f.patientid, f.formtype, f.duedate, f.status, f.validity, p.siteid FROM " . $this->getFormsViewName() . " f INNER JOIN " . $this->getPatientsViewName() . " p ON f.duedate >= ? AND f.duedate< ? AND p.patientid = f.patientid AND p.siteid = ? ORDER BY f.duedate "  ;
				$db->query($sql, $params->startDate, $date, $siteId);
			}
			
			$rowcount=	$db->row_count();	
			if( $rowcount > 0) {
				$j=0;
				for ($i=1; $i<=$rowcount; $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) {
						if ($row['validity'] != 'Y' && $row['duedate'] != '' && $db->dbdatetime_to_phpdatetime($row['duedate']) < time()) {
								$row['status'] = FrameworkConfig::VISITSTATUS_MISSING;
						} 
						if ($row['validity'] == 'Y' ) {
							$row['status'] = FrameworkConfig::VISITSTATUS_COMPLETED;
						} 
						while ($row['duedate'] != $calendarData[$j]['calendardate'] ) { 
							$j++;
						}
						array_push($calendarData[$j]['calendarvisits'],$row);
					}
				}	
			}
			$success = true;		
			$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getCalendardates, Exception: $e");
		}
		//$logger->logInfo($calendarData);
		return Array('success' => $success, 'calendardates' => $calendarData);
	}


	/** This function returns the overdue visits of the user's site ordered by duedate desc (in case of investigator, nurse)
	 *  input parameters: 
	 *
	 *  @remotable
	 */
	public function getOverdueVisits(stdClass $params)
    {
    	$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$userGroup=isset($_SESSION['auth_userclass']) ? $_SESSION['auth_userclass'] : '';
		$siteId = isset($_SESSION['auth_siteid']) ? $_SESSION['auth_siteid'] : 0 ;
		$selectedSiteId=isset($_SESSION['auth_selected_siteid']) ? $_SESSION['auth_selected_siteid'] : -1 ;
		$selectedCountryId=isset($_SESSION['auth_selected_countryid']) ? $_SESSION['auth_selected_countryid'] : -1 ;
		
 		$logger = Logger::getInstance();
		
		/* @TODO : jogosultság ellenőrzés user láthatja-e patient*/

		$success = false;
		
		$overdueVisits= Array();
		$today = date("Y-m-d");
		
		try
		{
			$db = new Database();
			if (UserManagement::userHasPermission("UserManagement", "changeSite")) {
		//	if ( $userGroup==FrameworkConfig::USERGROUP_MONITOR || $userGroup==FrameworkConfig::USERGROUP_PHARMACOVIGILANCE || $userGroup==FrameworkConfig::USERGROUP_SPONSOR || $userGroup==FrameworkConfig::USERGROUP_ADMIN ) {
				if 	( $selectedCountryId == 0 ) { // all countries' all sites' visits
					$sql = "SELECT f.id, f.instanceid, f.patientid, f.formtype, f.duedate, f.status, p.siteid FROM " . $this->getFormsViewName() . " f INNER JOIN " . $this->getPatientsViewName() . " p ON f.status= ? AND f.duedate< ? AND p.patientid = f.patientid ORDER BY duedate DESC ";
					$db->query($sql, FrameworkConfig::VISITSTATUS_EMPTY,  $today);
				} else {
					if ( $selectedSiteId == 0 ) { // one country's all sites' visits
						$sql = "SELECT f.id, f.instanceid, f.patientid, f.formtype, f.duedate, f.status, p.siteid FROM " . $this->getFormsViewName() . " f INNER JOIN " . $this->getPatientsViewName() . " p ON f.status= ? AND f.duedate< ? AND p.patientid = f.patientid  INNER JOIN " . $this->getSitesViewName() . " s ON p.siteid = s.sid AND s.countryid = ? ORDER BY f.duedate DESC ";
						$db->query($sql, FrameworkConfig::VISITSTATUS_EMPTY, $today, $selectedCountryId);
					} else { // one site's visits
						$sql = "SELECT f.id, f.instanceid, f.patientid, f.formtype, f.duedate, f.status, p.siteid FROM " . $this->getFormsViewName() . " f INNER JOIN " . $this->getPatientsViewName() . " p ON f.status= ? AND f.duedate< ? AND p.patientid = f.patientid AND p.siteid = ? ORDER BY f.duedate DESC "  ;
						$db->query($sql, FrameworkConfig::VISITSTATUS_EMPTY, $today, $selectedSiteId);
					}
				}
			} else // investigator, nurse => one site's visits
			{
				$sql = "SELECT f.id, f.instanceid, f.patientid, f.formtype, f.duedate, f.status, p.siteid FROM " . $this->getFormsViewName() . " f INNER JOIN " . $this->getPatientsViewName() . " p ON f.status= ? AND f.duedate< ? AND p.patientid = f.patientid AND p.siteid = ? ORDER BY f.duedate DESC "  ;
				$db->query($sql, FrameworkConfig::VISITSTATUS_EMPTY, $today, $siteId);
			}
	
			$oneDay= Array();
			$oneDay['calendarvisits']= Array();
			
			$rowcount=	$db->row_count();	
			if( $rowcount > 0) {
				$j=0;
				for ($i=1; $i<=$rowcount; $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) {
						//$row['color']= 'yellow'; // @todo ellenőrizni, biztos mindig sárga az overdue??
						if (array_key_exists('calendardate',$oneDay) && $row['duedate'] == $oneDay['calendardate']) {
							array_push($oneDay['calendarvisits'], $row);
						} else
						{		
							if (array_key_exists('calendardate',$oneDay)) {
								array_push($overdueVisits, $oneDay);
								$oneDay['calendarvisits']= Array();
							}	
							$oneDay['id']= $row['duedate'];
							$oneDay['calendardate']= $row['duedate'];
							array_push($oneDay['calendarvisits'], $row);
						}
					}	
				}
				array_push($overdueVisits, $oneDay);
				
					
			}
			$success = true;		
			$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getOverdueVisits, Exception: $e");
		}
		//$logger->logInfo($overdueVisits);
		return Array('success' => $success, 'overduedates' => $overdueVisits);
	}

	
}
?>
