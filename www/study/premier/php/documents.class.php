<?php
/**
 *---------------------------------------------------------------
 *documents.class.php
 *
 * This file contains the Documents class
 *
 * @package Kron
 * @subpackage documents
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 *---------------------------------------------------------------
 */
require_once('logger.class.php');
require_once('database.class.php');


class Documents
{
  	/**
     *  This function returns the name of the documents table
     */
	public function getDocumentsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'documents';
	}
	
	/** This function returns the documents data
	 *  @remotable
	 */
	public function getDocuments()
    {
 		$logger = Logger::getInstance();
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		// Check for the permission
		/*$permissionname = 'Documents';
		$permissionaction = 'getDocuments';
		if (userHasPermission($permissionname, $permissionaction) != true)  
		{
			//Log the missing permission
			$logger->logPermissionError((object)array('function' => __FUNCTION__, 'permissionname' => $permissionname, 'permissionaction' => $permissionaction));
			return Array('success' => false, 'errormsg' => _("Permission is not granted: ") . $permissionaction . ' ' . $permissionname);
		}*/
		
		$documentsarray = array();
		$document = array();
		try
		{// Look for the user in the database
			$db = new Database();
			$sql = "SELECT * FROM " . $this->getDocumentsTableName();
			$db->query($sql);		
			if($db->row_count() > 0) {
				for ($i=1; $i<=$db->row_count(); $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) {
						$document['subject'] = $row['subject'];
						$document['description'] = $row['description'];
						$document['alias'] = $row['alias'];
						$document['type'] = strtolower(pathinfo($row['file'], PATHINFO_EXTENSION));
						array_push($documentsarray, $document);
					}				
				}					
			}
			
			// test area
			
			// transaction test
	/*		$logger->logInfo("transaction test started");
			$sql = "INSERT INTO messages_ts (subject, text ) VALUES (?,?) ";
			$logger->logInfo($sql);
			$db->begin_transaction();
			$db->command($sql, "teszt" ,  "03-a" );
			$sql = "INSERT INTO messages_ts (subject, text ) VALUES (?,?) ";
			$db->command($sql, "teszt" ,  "03-b" );
			//$params->msgid=$db->get_lastinsertid();
			
			$db->commit();
			$logger->logInfo("transaction test ended");*/
			
			
			/// performance test data
			/*
			$sTime = microtime();
			$logger->logInfo(sprintf("DB Peformance teszt - start time:  %s",  $sTime));
			$a2 = array();
			$ra = array();
			$rc='';


			$sql = "SELECT * FROM myTable WHERE pid=10 ";//and vid=1";
			$db->query($sql);		
			if($db->row_count() > 0) {
				for ($i=1; $i<=$db->row_count(); $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) {
						if ($rc==$row['vid']) {
						
							$ra[$row['iid']]= $row['value'];	
						}	 
						else {
							if ($rc!='') {
								array_push($a2,$ra);
							}	
							$ra = array();
							$rc = $row['vid'];
							$ra['vid']=  $row['vid'];
							$ra[$row['iid']]= $row['value'];			
							
						}	
					}				
				}					
			}
				
			$eTime = microtime();
			$logger->logInfo(sprintf("DB Peformance teszt - end time:  %s",  $eTime));
			$logger->logInfo(sprintf("Diffi :  %s",  $eTime-$sTime));
			$logger->logInfo("Result:");
			$logger->logInfo($a2);
			$logger->logInfo("DB Peformance teszt END");
			*/
			/// performance test end
			
			$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getDocuments, Exception: $e");
	//		$db->rollback();
	//		$logger->logError($e);
		}
		return $documentsarray;
	}
 }
?>
