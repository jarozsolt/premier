﻿<?php
/**
 *---------------------------------------------------------------
 * login.php
 *
 * Login page.
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
// Calculate the php path
$php_path = str_replace("\\", "/", realpath(dirname(__FILE__)));

// Calculate the study path
$study_path = join(array_slice(explode( "/" , $php_path), 0, -1), "/");

// Calculate the study directory
$study_dir = basename ($study_path ,"/");

// Calculate the subdomain path
$subdomain_path = join(array_slice(explode( "/" ,$study_path), 0, -1), "/");

// Calculate the public (www) path
$public_path = join(array_slice(explode( "/" ,$subdomain_path), 0, -1), "/");

// Calculate the base path
$base_path = join(array_slice(explode( "/" , $public_path), 0, -1), "/");

// Load the main configuration file
// Warning the 'configs' config_folder hardcoded here !!!
require_once($base_path . '/' . $study_dir . '/configs/app.conf');

$language = FrameWorkConfig::default_language;

// Initialize session
if (!isset($_SESSION)) 
{
	ini_set('session.use_cookies', 1);
	ini_set('session.use_only_cookies', 1);
	session_start();
}

if (isset ($_SESSION['auth_userlanguage']) && $_SESSION['auth_userlanguage'] != '')
{
	$language = $_SESSION['auth_userlanguage'];
}
$language_short = substr($language, 0, 2);

// We need to build the logout page?
if (isset ($_SESSION['auth_userloggedoutsuccessful']) && $_SESSION['auth_userloggedoutsuccessful'])
{// logout page
	require_once('logout.php');
	exit();
}

if (!isset ($_SESSION['auth_userloggedin']) || !$_SESSION['auth_userloggedin'] ||
	!isset ($_SESSION['auth_userclass']) || !$_SESSION['auth_userclass'])
{ // The user not logged in or the user class is not admin, we build the login page

}

// Build the login page
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">';
echo '<html>';
echo '<head>';
echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
echo '<title id="page-title">'._("KRON - Klinikai Adatkezelő Rendszer").'</title>';
//echo '<link rel="stylesheet" type="text/css" href="'.EXTJS_PATH_REL.'resources/css/ext-all.css"/>';
echo '<link rel="stylesheet" type="text/css" href="'.CSS_PATH_REL.'my-ext-theme.css"/>';
echo '<link rel="stylesheet" type="text/css" href="'.CSS_PATH_REL.'login_1.css"/>';
echo '<link rel="stylesheet" type="text/css" href="'.CSS_PATH_REL.'statusbar.css"/>';
echo '<link rel="shortcut icon" type="image/x-icon" href="'.IMAGE_PATH_REL.'favicon.ico"/>';
echo '<link rel="gettext" type="application/x-po" href="'.LOCALES_PATH_REL.$language.'.utf8'.'/LC_MESSAGES/server.po"/>';
echo '</head>';
echo '<body style="scroll:no">';
echo '<img class="background" src="'.IMAGE_PATH_REL.'background.png">';
echo '<div id="wait_indicator" style="position: absolute; top: 50%; left: 50%; background-image: url(data:image/gif;base64,R0lGODlhEAAQALMMAKqooJGOhp2bk7e1rZ2bkre1rJCPhqqon8PBudDOxXd1bISCef///wAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFAAAMACwAAAAAEAAQAAAET5DJyYyhmAZ7sxQEs1nMsmACGJKmSaVEOLXnK1PuBADepCiMg/DQ+/2GRI8RKOxJfpTCIJNIYArS6aRajWYZCASDa41Ow+Fx2YMWOyfpTAQAIfkEBQAADAAsAAAAABAAEAAABE6QyckEoZgKe7MEQMUxhoEd6FFdQWlOqTq15SlT9VQM3rQsjMKO5/n9hANixgjc9SQ/CgKRUSgw0ynFapVmGYkEg3v1gsPibg8tfk7CnggAIfkEBQAADAAsAAAAABAAEAAABE2QycnOoZjaA/IsRWV1goCBoMiUJTW8A0XMBPZmM4Ug3hQEjN2uZygahDyP0RBMEpmTRCKzWGCkUkq1SsFOFQrG1tr9gsPc3jnco4A9EQAh+QQFAAAMACwAAAAAEAAQAAAETpDJyUqhmFqbJ0LMIA7McWDfF5LmAVApOLUvLFMmlSTdJAiM3a73+wl5HYKSEET2lBSFIhMIYKRSimFriGIZiwWD2/WCw+Jt7xxeU9qZCAAh+QQFAAAMACwAAAAAEAAQAAAETZDJyRCimFqbZ0rVxgwF9n3hSJbeSQ2rCWIkpSjddBzMfee7nQ/XCfJ+OQYAQFksMgQBxumkEKLSCfVpMDCugqyW2w18xZmuwZycdDsRACH5BAUAAAwALAAAAAAQABAAAARNkMnJUqKYWpunUtXGIAj2feFIlt5JrWybkdSydNNQMLaND7pC79YBFnY+HENHMRgyhwPGaQhQotGm00oQMLBSLYPQ9QIASrLAq5x0OxEAIfkEBQAADAAsAAAAABAAEAAABE2QycmUopham+da1cYkCfZ94UiW3kmtbJuRlGF0E4Iwto3rut6tA9wFAjiJjkIgZAYDTLNJgUIpgqyAcTgwCuACJssAdL3gpLmbpLAzEQA7); width: 16px; height: 16px">&#160;</div><div class="kron-poweredby" style="position: absolute; bottom: 10px; right: 10px;"><img src="'.IMAGE_PATH_REL.'powered_by_kron.png"></div>';
if (FrameworkConfig::debug_mode)
{
	// Set the Debug global javascript variable to true
	// For IE we should define an empty console.log function, to avoid errors
	echo '<script type="text/javascript">var Debug=true;';
	if (FrameworkConfig::demo_mode)
	{
		echo 'var Demo=true;';
	}
	echo 'if (typeof window.console==="undefined"||typeof window.console.log==="undefined"){(function(){';
	echo 'window.console={};window.console.log=function(){};}());}</script>';
	// For the debug version use the ext-all-debug-w-comments.js
	echo '<script type="text/javascript" src="'.EXTJS_PATH_REL.'ext-all-debug-w-comments.js"></script>';
}
else
{
	// Set the Debug global javascript variable to false
	// For every browser we should define an empty console.log function, to hide the logs
	echo '<script type="text/javascript">var Debug=false;';
	if (FrameworkConfig::demo_mode)
	{
		echo 'var Demo=true;';
	}
	echo 'if (typeof window.console==="undefined"||typeof window.console.log==="undefined"){(function(){';
	echo 'window.console={};window.console.log=function(){};}());}else{window.console.log=function(){};}</script>';
	// For the production version use the ext-all.js
	echo '<script type="text/javascript" src="'.JAVASCRIPT_PATH_REL.'build/ext_login_1.js"></script>';
}
echo '<script type="text/javascript" src="'.EXTJS_PATH_REL.'locale/ext-lang-'.$language_short.'.js"></script>';
// Configure the Ext.Loader


if (FrameworkConfig::debug_mode)
{
	echo '<script type="text/javascript">';
	// Register namespaces and their corresponding paths to Ext.Loader
	echo 'Ext.Loader.setConfig({enabled:true,paths:{';
	// for the debug version use the ext and the app
	echo '"Ext":"'.EXTJS_PATH_REL.'src",';
	//echo '"Core":"'. JSCORE_PATH_REL .'",';
	echo '"Core":"'. $jscore_path_relative .'",';
	echo '"Ext.ux":"'. JSCORE_PATH_REL .'ux",';
	echo '"Kron":"'. JSAPP_PATH_REL . '"}});';
	echo '</script>';
	echo '<script type="text/javascript" src="'.JSCORE_PATH_REL.'locale/Gettext.js"></script>';
	echo '<script type="text/javascript" src="'.PHPAPP_PATH_REL.'login.extdir.php"></script>';
	echo '<script type="text/javascript" src="'.JSAPP_PATH_REL.'login.js"></script>';
} else {
	echo '<script type="text/javascript" src="'.JAVASCRIPT_PATH_REL.'build/gettext_1.js"></script>';
	echo '<script type="text/javascript" src="'.PHPAPP_PATH_REL.'login.extdir.php"></script>';
	echo '<script type="text/javascript" src="'.JAVASCRIPT_PATH_REL.'build/login_2.js"></script>';
}
echo '<div style="position: absolute; top: 100px; left: 50px;">';
echo '<div><img src="'.IMAGE_PATH_REL.'sponsor.png"></div>';
echo '<div style="position: relative; top: 10px; left: 10px;">';
echo '<h1>WELCOME TO<br>PREMIER-VV001 STUDY</h1>';
echo '<b><p>Please enter your user name and password to log in<br>Kérjük adja meg a felhasználónevét és jelszavát a belépéshez</p>';
if (FrameworkConfig::demo_mode)
{
	echo '<br><p>Training section - Gyakorló verzió</p>';
}
else
{
	echo '<br>';
}
echo '</b></div></div>';
echo '<div class="helpdesk"><table><tr><td>Technical help-desk:</td><td><b>info@datanova.hu</b></td></tr>';
echo '<tr><td>Technikai segítségnyújtás:</td><td><b>+36 30 316 1317</b></td></tr></table></div>';
if (FrameworkConfig::demo_mode)
{
	echo '<div style="position:absolute;bottom:0;right:0;"><img src="'.IMAGE_PATH_REL.'demo.png"></div>';
}
echo '<noscript><p>It appears that your web browser does not support JavaScript,
      or you have temporarily disabled scripting. Either way, this site
      won\'t work without it. Please take a look at our <a href="/support/browsers/">browser support page</a> for more help.</p></noscript>';
echo '</body>';
echo '</html>';
?>
