<?php
/**
 *---------------------------------------------------------------
 * usermanagement.class.php
 *
 * This file contains the UserManagement class
 *
 * @package Kron
 * @subpackage usermanagement
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 *---------------------------------------------------------------
 */
// Initialize session
ini_set('session.use_cookies', 1);
ini_set('session.use_only_cookies', 1);
session_start();

require_once('database.class.php');
require_once('logger.class.php');
require_once('localemanagement.class.php');

Const USERSTATUS_ACTIVE = 0;
Const USERSTATUS_READONLY = 1;
Const USERSTATUS_DISABLED = 2;


/**
 * UserManagement class
 *
 * User manangement class for Ext Direct
 *
 * @package Kron
 * @subpackage database
 * @todo Revise comments
 */
class UserManagement
{
		/**
         *  This function returns the name of the users view
         */
		public function getUsersViewName()
		{
			return DatabaseConfig::db_tableprefix . 'users';
		}
		
		/**
         *  This function returns the name of the users_ts table
         */
		public function getUsersTableName()
		{
			return DatabaseConfig::db_tableprefix . 'users_ts';
		}
		
		/**
         *  This function returns the name of the sites view
         */
		public function getSitesViewName()
		{
			return DatabaseConfig::db_tableprefix . 'sites';
		}
		
		/**
         *  This function returns the name of the countries view
         */
		public function getCountriesViewName()
		{
			return DatabaseConfig::db_tableprefix . 'countries';
		}
		
		
		/** This login function can be called as remote function, but it is
		 *	also automatically used by the user management when a remote function requires a login.
		 *  Returns true on success, otherwise false.
		 *  @remotable
		 *  @todo Maximize the login attempts
		 *  @todo Multi-language
		 */
    	public function login(stdClass $params)
    	{
    	    $logger = Logger::getInstance();
    	    $username = $params->username;
    	    $userlanguage = $params->language;
			//$logger->logDebug(__METHOD__ . '::line ' . __LINE__ . " called! username:'$username' language:'$userlanguage'"); 
    
			$success = false;
			$usergroup = 'guest';
			$passwordhash = '';
			$userid = 0;
			$userlastlogged = 0;
			$name='';
			$siteId=0;
			$status = USERSTATUS_ACTIVE;
			
			// @todo sanitize usename and password
			
			try
			{// Look for the user in the database
				$db = new Database();
				$db->begin_transaction();
				$sql = "SELECT * FROM " . $this->getUsersViewName() . " WHERE username = ? AND active = '1'";
				$db->query($sql, $params->username);		
				if($db->row_count() > 0)
				{
					$row = $db->fetch();
					if ($row != null)
					{// The user found
						$uid = $row['uid'];
						$userid = $row['uiid'];
						$usergroup = $row['usergroup'];
						$name= $row['name'];
						$passwordhash = strtoupper($row['password']);
						$language = $row['language'];
						$siteId = $row['siteid'];
						$status = $row['status'];
						$selectedSiteId = $row['selected_sid'];
						$selectedCountryId = $row['selected_cid'];
						if ($language == '')
						{
							if ($userlanguage == '')
							{
								$userlanguage = FrameworkConfig::default_language;
							}
						}
						else
						{
							$userlanguage = $language;
						}
						if ($userid > 0  && $params->password == $passwordhash)
						{// Found the user and the password is the same
							if ($status != USERSTATUS_DISABLED)
							{// The user is not disabled
								$success = true;
							}
						}
					}
				}
				if ($userid > 0 && $success == true)
				{// Update the 'last_logged' column
					$userlastlogged = $db->phpdatetime_to_dbdatetime();
					$sql = "UPDATE " . $this->getUsersTableName() . " SET last_logged = ? WHERE uid = ?";
					$db->command($sql, $userlastlogged, $uid);
				}
				$db->commit();
				$db->close(); // Closes the cursor to free up memory
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: login, Exception: $e");
				$db->rollback();
			}

    	    if ($success)
    	    {// The user logged in successfully
				// Init language
				LocaleManagement::initLanguage($userlanguage);
				$params->language = $userlanguage;
				$params->username = '';
				$params->password = '';
				
				// Set the default application for the user
				// The javascript framework will redirect the user to this php
				if ($usergroup == '')
				{
					$params->loggedin = false;
					$params->message = _("Nem sikerült kapcsolódni a szerverhez!");
					$logger->logWarn("Login: Invalid userclass='$usergroup' username='$username'");				
				}
				else
				{
					// Save the userid, class and language into the SESSION variables
					$_SESSION['auth_userloggedin'] = true;
					$_SESSION['auth_username'] = $username;
					$_SESSION['auth_name'] = $name;
					$_SESSION['auth_userid'] = $userid;
					$_SESSION['auth_userclass'] = $usergroup;
					$_SESSION['auth_userlanguage'] = $userlanguage;
					$_SESSION['auth_userlastlogged'] = $userlastlogged;
					$this->setUserPermissions();
					$_SESSION['auth_siteid'] = $siteId;
					$_SESSION['auth_selected_siteid'] = $selectedSiteId;
					$_SESSION['auth_selected_countryid'] = $selectedCountryId;
					if ($status == USERSTATUS_READONLY)
					{
						$_SESSION['auth_userreadonly'] = true;
					}
					$params->loggedin = true;
					$params->message = _("Sikeres bejelentkezés!");
					$logger->logInfo("Login successful: Username='$username'");
				}
			}
			else
			{
				// @todo check for the requested language is supported
				// Init language
				LocaleManagement::initLanguage($userlanguage);
				$params->language = $userlanguage;
				$params->loggedin = false;
				$params->username = "";
				$params->password = "";
				if ($status == USERSTATUS_DISABLED)
				{// The user found, the username and password correct, but the user disabled
					$params->message = _("A felhasználói fiókja letiltásra került, kérjük lépjen kapcsolatba a rendszeradminisztrátorral!");
					$logger->logWarn("Login: Rejecting login attempt. The user account was disabled. username='$username'");
	
				}
				else
				{// The user not found or the username or password incorrect
					$ipaddress = $_SERVER['REMOTE_ADDR'];
					if ($userid > 0)
					{// Incorrect password for an existing user
						//Log the erroneous login attempt
						$logger->logWarn("Login: The user entered invalid password. ip='$ipaddress' username='$username'");
					}
					else
					{// The username not found
						//Log the erroneous login attempt
						$logger->logWarn("Login: The user entered invalid username. ip='$ipaddress' username='$username'");
					}
					$params->message = _("Érvénytelen felhasználónév vagy jelszó!");
				}
			}
			return Array('success' => $success, 'data' => $params);
    	}

		/**	This logout function can be called as remote function, but it is
    	 * 	also automatically used by the user management when the login timed out.
    	 * 	Returns true on a successful logout, otherwise false
    	 * 	@remotable
    	 */
	    public function logout(stdClass $params)
        {
        	$success = true;
			$redirect = null;
            if (!isset($_SESSION['auth_userloggedin']))
            {
				//Log the erroneous logout attempt
				$logger = Logger::getInstance();
				$ipaddress = $_SERVER['REMOTE_ADDR'];
				$logger->logWarn("Logout: The user try to logout, but not logged in. ip='$ipaddress'");
			    $success = false;
			}
			else
			{
				unset($_SESSION['auth_userloggedin']);
				unset($_SESSION['auth_userid']);
				unset($_SESSION['auth_username']);
				unset($_SESSION['auth_userclass']);
				unset($_SESSION['auth_userpermissions']);
				//unset($_SESSION['auth_userlanguage']);
				unset($_SESSION['auth_userlastlogged']);
				unset($_SESSION['auth_userreadonly']);
				unset($_SESSION['auth_name'] );
				unset($_SESSION['auth_siteid'] );				
				$_SESSION['auth_userloggedoutsuccessful'] = true;
				
				$redirect = "index.php";
			}
           	return Array('success' => $success, 'redirect' => $redirect);
        }

        
        /**
         *  This function reports the actual logged in user or an empty string
         * @remotable
         */
        public function getUserName()
        {
            $username = $_SESSION['auth_username'];
            return (isset($username) ? $username : '');
        }

        
        /**
         *  This function reports true, if a user is logged in, otherwise false
         * @remotable
         */
        public function loggedIn()
        {
            return isset($_SESSION['auth_userloggedin']);
        }
		
		/**
		 *  The function reports true, if a user has the specified permission, otherwise false
		 */
		static public function userHasPermission($class, $method)
		{
			// Check the user logged in
		    if (isset($_SESSION['auth_userloggedin']))
			{
				if (isset($_SESSION['auth_userpermissions']))
				{
					// Check the permission is granted
					if (array_key_exists($class, $_SESSION['auth_userpermissions'])
						&& in_array($method, $_SESSION['auth_userpermissions'][$class]))
					{
						// Permission is granted!
						return true;
					}
				}
			}
			return false;
		}
		
		
		/**
		 *  The function remove the specified permission
		 */
		private function revokePermission($class, $method)
		{
			// Check the user logged in
		    if (isset($_SESSION['auth_userloggedin']))
			{
				if (isset($_SESSION['auth_userpermissions']))
				{
					// Check the permission is granted
					if (array_key_exists($class, $_SESSION['auth_userpermissions'])
						&& in_array($method, $_SESSION['auth_userpermissions'][$class]))
					{
						if (($key = array_search($method, $_SESSION['auth_userpermissions'][$class])) !== false)
						{
    						unset($_SESSION['auth_userpermissions'][$class][$key]);
						}
					}
				}
			}
		}
		
		static public function ExtDirect_Authorization_Callback($extdirect)
		{
			// Check for the permission
			if (UserManagement::userHasPermission($extdirect->action, $extdirect->method) != true)  
			{
				$logger = Logger::getInstance();
				//Log the missing permission
				$logger->logPermissionError((object)array('action' => $extdirect->action, 'method' => $extdirect->method));
				return false;
			}
			return true;
		}
		
		
		
		static public function checkUserPermissions($usergroup, $class, $method)
		{
			$permissions = UserManagement::getUserPermissions($usergroup);
			if (array_key_exists($class, $permissions) && in_array($method,$permissions[$class]))
			{
				// Permission is granted!
				return true;
			} else {
				return false;
			}
		}
		
		
		
		static private function getUserPermissions($usergroup)
		{
			if ($usergroup == FrameworkConfig::USERGROUP_NURSE)
			{
				// X_ -sal kezdődőek nem php függvények, hanem csak jogosultságot szabályoznak...
				//  "X_doSign" - aki tényleg alá is írhat (mert a signForm-ot kell bírnia a nurse-nek is, aki csak beküldi...)
				//  "X_closeQuery" - queryt zárhat le
				// "X_addQuery" - querit addhat hozzá
				// "X_deleteQuery" - query-t törölhet
				// "X_addComment" - add query comment
				$permissions = array(
					'Forms' => array("getForms", "printForm", "getFieldHistories", "createForm", "updateForm", "deleteForm", "signForms", "createField", "updateField", "deleteField", "X_signButton"),
					'Patients' => array("getPatients", "addPatients", "updatePatient", "printPatient"),
					'Queries' => array("getOpenQueries", "getPatientQueries","getFormQueries","createQuery","updateQuery", "X_addComment"),
					'UserManagement' => array("login", "logout", "getUserDetails", "changePassword", "changeUserDetails"),
					'Visits' => array("getOverdueVisits", "getCalendardates"),
					'Documents' => array("getDocuments", "download"),
					'Messages' => array("getReceivedMessages", "getSentMessages", "postMessage", "setRead", "deleteMessages"),
					'Addressees' => array("getAddressees"),
					'Contacts' => array("getContacts"),
					'Reports' => array("getPatientsData","getPatientsTrend","getVisitData")
				);
			}
			else if ($usergroup == FrameworkConfig::USERGROUP_INVESTIGATOR)
			{
				$permissions = array(
					'Forms' => array("getForms", "printForm", "getFieldHistories", "createForm", "updateForm", "deleteForm", "signForms",  "X_doSign", "createField", "updateField", "deleteField", "X_signButton"),
					'Patients' => array("getPatients", "addPatients", "updatePatient", "printPatient"),
					'Queries' => array("getOpenQueries", "getPatientQueries","getFormQueries","createQuery","updateQuery", "X_addComment"),
					'UserManagement' => array("login", "logout", "getUserDetails", "changePassword", "changeUserDetails"),
					'Visits' => array("getOverdueVisits", "getCalendardates"),
					'Documents' => array("getDocuments", "download"),
					'Messages' => array("getReceivedMessages", "getSentMessages", "postMessage", "setRead", "deleteMessages"),
					'Addressees' => array("getAddressees"),
					'Contacts' => array("getContacts"),
					'Reports' => array("getPatientsData","getPatientsTrend","getVisitData")
				);
			}
			else if ($usergroup == FrameworkConfig::USERGROUP_PRINCIPALINVESTIGATOR)
			{
				$permissions = array(
					'Forms' => array("getForms", "printForm", "getFieldHistories", "createForm", "updateForm", "deleteForm", "signForms", "createField", "updateField", "deleteField", "X_signButton"),
					'Patients' => array("getPatients", "addPatients", "updatePatient", "printPatient"),
					'Queries' => array("getOpenQueries", "getPatientQueries","getFormQueries","createQuery","updateQuery", "X_addComment"),
					'UserManagement' => array("login", "logout", "getUserDetails", "changePassword", "changeUserDetails"),
					'Visits' => array("getOverdueVisits", "getCalendardates"),
					'Documents' => array("getDocuments", "download"),
					'Messages' => array("getReceivedMessages", "getSentMessages", "postMessage", "setRead", "deleteMessages"),
					'Addressees' => array("getAddressees"),
					'Contacts' => array("getContacts"),
					'Reports' => array("getPatientsData","getPatientsTrend","getVisitData")
					
				);
			}
			else if ($usergroup == FrameworkConfig::USERGROUP_MONITOR)
			{
				$permissions = array(
					'Forms' => array("getForms", "printForm", "monitorForm", "getFieldHistories", "X_addQueryMenuItem"),
					'Patients' => array("getPatients", "printPatient"),
					'Queries' => array("approveField","getOpenQueries", "getPatientQueries", "getFormQueries", "createQuery", "updateQuery", "deleteQuery", "X_addQuery", "X_closeQuery", "X_deleteQuery", "X_addComment"),
					'UserManagement' => array("login", "logout", "getUserDetails", "changeCountry", "changeSite", "changePassword", "changeUserDetails"),
					'Visits' => array("getOverdueVisits", "getCalendardates"),
					'Documents' => array("getDocuments", "download"),
					'Messages' => array("getReceivedMessages", "getSentMessages", "postMessage", "setRead", "deleteMessages"),
					'Addressees' => array("getAddressees"),
					'Contacts' => array("getContacts"),
					'Reports' => array("getPatientsData","getPatientsTrend","getVisitData")
					
				);
			}
			else if ($usergroup == FrameworkConfig::USERGROUP_PHARMACOVIGILANCE)
			{
				$permissions = array(
					
					'Forms' => array("getForms", "printForm", "getFieldHistories", "X_addQueryMenuItem"),
					'Patients' => array("getPatients", "printPatient"),
					'Queries' => array("getOpenQueries", "getPatientQueries", "getFormQueries", "createQuery", "updateQuery", "deleteQuery", "X_addQuery", "X_closeQuery", "X_deleteQuery", "X_addComment"),
					'UserManagement' => array("login", "logout", "getUserDetails", "changeCountry", "changeSite", "changePassword", "changeUserDetails"),
					'Visits' => array("getOverdueVisits", "getCalendardates"),
					'Documents' => array("getDocuments", "download"),
					'Messages' => array("getReceivedMessages", "getSentMessages", "postMessage", "setRead", "deleteMessages"),
					'Addressees' => array("getAddressees"),
					'Contacts' => array("getContacts"),
					'Reports' => array("getPatientsData","getPatientsTrend","getVisitData")
					
				);
			}
			else if ($usergroup == FrameworkConfig::USERGROUP_SPONSOR)
			{
				$permissions = array(
					'Forms' => array("getForms", "printForm", "getFieldHistories"),
					'Patients' => array("getPatients", "printPatient"),
					'Queries' => array("getOpenQueries", "getPatientQueries", "getFormQueries"),
					'UserManagement' => array("login", "logout", "getUserDetails", "changeCountry", "changeSite", "changePassword", "changeUserDetails"),
					'Visits' => array("getOverdueVisits", "getCalendardates"),
					'Documents' => array("getDocuments", "download"),
					'Messages' => array("getReceivedMessages", "getSentMessages", "postMessage", "setRead", "deleteMessages"),
					'Addressees' => array("getAddressees"),
					'Contacts' => array("getContacts"),
					'Reports' => array("getPatientsData","getPatientsTrend","getVisitData")
					
				);
			}
			else if ($usergroup == FrameworkConfig::USERGROUP_ADMIN)
			{
				$permissions = array();
			}
			else if ($usergroup == FrameworkConfig::USERGROUP_SYSTEM)
			{
				$permissions = array();
			}
		return $permissions;
		}
		/**
         *  This function sets the user's permissions, based on the logged in user's group
         */
        private function setUserPermissions()
        {
        	// Check the user logged in
            if ($this->loggedIn() == true)
			{
				// Delete the existing permissions
				if (isset($_SESSION['auth_userpermissions']))
				{
					unset($_SESSION['auth_userpermissions']);
				}
				// Set the permissions
				if (isset($_SESSION['auth_userclass']))
				{
					$permissions = Array();
					$usergroup = $_SESSION['auth_userclass'];
					
					$permissions = UserManagement::getUserPermissions($usergroup);
					
					
					$_SESSION['auth_userpermissions'] = $permissions;
					// Remove write permissions on readonly user / after database lock
					if (isset($_SESSION['auth_userreadonly']))
					{
						$this->revokePermission('Forms', 'createForm');
						$this->revokePermission('Forms', 'updateForm');
						$this->revokePermission('Forms', 'deleteForm');
						$this->revokePermission('Forms', 'signForms');
						$this->revokePermission('Forms', 'signEndOfStudyForm');
						$this->revokePermission('Queries', 'addText');
						$this->revokePermission('Queries', 'approveField');
						$this->revokePermission('Patients', 'addPatients');
					}
				}
					
			}
			return false;
        }
        
        // @todo: ellenĹ‘rizni, kell-e egyĂˇltalĂˇn a getUserStatus, mert a loginnĂˇl kicserĂ©ltem mĂˇr a getUserDetails-re
        
		/** This function reports the status of the actual user
		 *  @remotable
		 */
    	public function getUserStatus()
    	{			
			$userloggedin = isset($_SESSION['auth_userloggedin']);
			$username = isset($_SESSION['auth_username']) ? $_SESSION['auth_username'] : '';
			$usergroup = isset($_SESSION['auth_userclass']) ? $_SESSION['auth_userclass'] : '';
			$userlanguage = isset($_SESSION['auth_userlanguage']) ? $_SESSION['auth_userlanguage'] : '';

			return array('loggedin' => $userloggedin,
						'name' => $username,
						'usergroup' => $usergroup,
						'language' => $userlanguage);
    	}
    	
		/** This function reports the detailed data of the actual user:
		 * from users db: username, usergroup, userroledescription, name, phone, email, picturefile, last_logged
		 * from sites db: sitename
		 * from countries db: country
		 * 
		 *  @remotable
		 */
    	public function getUserDetails()
    	{
    		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
			$userloggedin = isset($_SESSION['auth_userloggedin']);
			$usergroup=isset($_SESSION['auth_userclass']) ? $_SESSION['auth_userclass'] : '';
			$userPermissions = isset($_SESSION['auth_userpermissions']) ? $_SESSION['auth_userpermissions'] : '';
			$siteId=isset($_SESSION['auth_siteid']) ? $_SESSION['auth_siteid'] : 0 ;
			
	 		$logger = Logger::getInstance();
		//	$logger->logInfo('getUserDetails called');
		//	$logger->logInfo($siteId);
			$userDetails = array();

			try
			{// Look for the user in the database
				$db = new Database();
				if (UserManagement::userHasPermission("UserManagement", "changeSite")) {
			//	if ( $usergroup==FrameworkConfig::USERGROUP_MONITOR || $usergroup==FrameworkConfig::USERGROUP_PHARMACOVIGILANCE || $usergroup==FrameworkConfig::USERGROUP_SPONSOR || $usergroup==FrameworkConfig::USERGROUP_ADMIN) {
					$sql = "SELECT  username, uiid, usergroup, language, userroledescription, name, phone, email, picturefile, last_logged, password_last_changed, selected_sid sid, selected_cid cid, selected_pid pid, autosave_enabled, autosave_frequency FROM " . $this->getUsersViewName() . " WHERE uiid = ? ";
					$db->query($sql, $userId);
				} else { 
					$sql = "SELECT  u.username, u.uiid, u.usergroup, u.language, u.userroledescription, u.name, u.phone, u.email, u.picturefile, u.last_logged, u.password_last_changed, s.sitename, s.sid, c.cid, c.country, u.selected_pid pid, u.autosave_enabled, u.autosave_frequency FROM " . $this->getUsersViewName() . " u INNER JOIN " .  $this->getSitesViewName() . " s ON ( u.siteid = s.sid AND u.uiid = ? ) INNER JOIN " .  $this->getCountriesViewName() . " c ON s.countryid=c.cid ";
					$db->query($sql, $userId);
				}
				
				$rowcount=	$db->row_count();	
				if( $rowcount > 0)	{
					for ($i=1; $i<=$rowcount; $i++) {
						$userDetails = $db->fetch("assoc");
					}					
				}
				if (UserManagement::userHasPermission("UserManagement", "changeSite")) {
						
					if (isset ($_SESSION['auth_userlanguage']) && $_SESSION['auth_userlanguage'] != '')
					{
						$language = $_SESSION['auth_userlanguage'];
						LocaleManagement::initLanguage($language);
					}
					
				//if ( $usergroup==FrameworkConfig::USERGROUP_MONITOR || $usergroup==FrameworkConfig::USERGROUP_PHARMACOVIGILANCE || $usergroup==FrameworkConfig::USERGROUP_SPONSOR || $usergroup==FrameworkConfig::USERGROUP_ADMIN) {
					$countries = array();
					$allcountries = _("Összes ország");
					array_push($countries, array("cid" => "0", "country" => $allcountries));
					$sql = "SELECT  cid, country FROM " . $this->getCountriesViewName() ;
					$db->query($sql);
					$rowcount=	$db->row_count();	
					if( $rowcount > 0)	{
						for ($i=1; $i<=$rowcount; $i++) {
							$row = $db->fetch("assoc");
							if ($row != null) { 
								array_push($countries,$row);
							}
						}	
										
					}
					
					$userDetails['countries'] = $countries;
					
					$sites = array();
					$allsites = _("Összes vizsgálóhely");
					array_push($sites, array("sid" => "0", "countryid" =>"0", "sitename" => $allsites));
					$sql = "SELECT  sid, sitename, countryid  FROM " . $this->getSitesViewName() ;
					$db->query($sql);
					$rowcount=	$db->row_count();	
					if( $rowcount > 0)	{
						for ($i=1; $i<=$rowcount; $i++) {
							$row = $db->fetch("assoc");
							if ($row != null) { 
								array_push($sites,$row);
							}
						}					
					}
					
					$userDetails['sites']=$sites;
				}
				
			$db->close(); // Closes the cursor to free up memory
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: getUserDetails, Exception: $e");
			}
			if ($userDetails['password_last_changed'] == null) {
				$userDetails['passwordchangerequired'] = true;
			}
			else
			{
				$userDetails['passwordchangerequired'] = false;
			}

			$toshowform = isset($_SESSION['showform_fid']);
			if ($toshowform == true) {
				$pid = isset($_SESSION['showform_pid']) ? $_SESSION['showform_pid'] : '';
				$sid = isset($_SESSION['showform_sid']) ? $_SESSION['showform_sid'] : '';
				$ftype = isset($_SESSION['showform_ftype']) ? $_SESSION['showform_ftype'] : '';
				$fid = isset($_SESSION['showform_fid']) ? $_SESSION['showform_fid'] : '';
				if ($pid != '' && $sid != '' && $ftype != '' && $fid != '') {
					$showform['pid'] = $pid;
					$showform['sid'] = $sid;
					$showform['ftype'] = $ftype;
					$showform['fid'] = $fid;
					$userDetails['showForm'] = $showform;
				}
				unset($_SESSION['showform_pid']);
				unset($_SESSION['showform_sid']);
				unset($_SESSION['showform_ftype']);
				unset($_SESSION['showform_fid']);
			}
			
			
			$userDetails['loggedin'] = $userloggedin;
			$userDetails['permissions'] = $userPermissions;
			
			return $userDetails;
    	}
    
		/** This function changes the selected site of a Monitor/Sponsor
		 *  input params:
		 *				'cid' - country id
		 *  @remotable
		 */
    	public function changeCountry(stdClass $params)
    	{			
    		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
			$usergroup=isset($_SESSION['auth_userclass']) ? $_SESSION['auth_userclass'] : '';
			
			$success = false;
			$logger = Logger::getInstance();
			
		//	$logger->logInfo('changeCountry called');$logger->logInfo($params->cid);
			//if ( $usergroup==FrameworkConfig::USERGROUP_MONITOR || $usergroup==FrameworkConfig::USERGROUP_PHARMACOVIGILANCE || $usergroup==FrameworkConfig::USERGROUP_SPONSOR || $usergroup==FrameworkConfig::USERGROUP_ADMIN) {
			try
			{ 
				$db = new Database();
				$sql = "UPDATE " . $this->getUsersTableName() . " SET selected_cid = ? WHERE uiid = ? AND end_ts IS NULL ";
				$db->command($sql, $params->cid, $userId);
				
				$_SESSION['auth_selected_countryid'] = $params->cid;
				
				$db->close(); 
				$success = true;
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: changeCountry, Exception: $e");
			}		
				
			//}

			return Array('success' => $success);
    	}
   
		/** This function changes the selected site of a Monitor/Sponsor
		 *  input params:
		 *				'sid' - site id, 
		 *  @remotable
		 */
    	public function changeSite(stdClass $params)
    	{			
    		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
			$usergroup=isset($_SESSION['auth_userclass']) ? $_SESSION['auth_userclass'] : '';
			
			$success = false;
			$logger = Logger::getInstance();

		//	$logger->logInfo($userId);
			//if ( $usergroup==FrameworkConfig::USERGROUP_MONITOR || $usergroup==FrameworkConfig::USERGROUP_PHARMACOVIGILANCE || $usergroup==FrameworkConfig::USERGROUP_SPONSOR || $usergroup==FrameworkConfig::USERGROUP_ADMIN) {
			try
			{ 
				$db = new Database();
				$sql = "UPDATE " . $this->getUsersTableName() . " SET selected_sid = ? WHERE uiid = ? AND end_ts IS NULL ";
				$db->command($sql, $params->sid, $userId);
				
				$_SESSION['auth_selected_siteid'] = $params->sid;
				
				$db->close(); 
				$success = true;
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: changeSite, Exception: $e");
			}
				
			//}

			return Array('success' => $success);
    	}
   		
    	/** This function changes the actual user's password
		 *  input params:
		 *		'newpassword': string 8EB740FA9664FACC0DE77FBBAC225702
		 * 		'oldpassword': string 8eb740fa9664facc0de77fbbac225702
		 * result:
		 * 	'success' : boolean
		 * 	'errortxt : string
		 * 
		 *  @remotable
		 */
    	public function changePassword(stdClass $params)
    	{			
    		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
			
			$success = false;
			$errortxt = "";
			$userDetails = array();
			$passwordhash = "";
			$logger = Logger::getInstance();
			//$logger->logInfo($params);
			try
			{ 
				$db = new Database();
				$db->begin_transaction();
				$sql = "SELECT * FROM " . $this->getUsersViewName() . " WHERE uiid = ? AND active = '1'";
				$db->query($sql, $userId);		
				if($db->row_count() > 0) {
					$row = $db->fetch();
					if ($row != null) {// The user found
						$passwordhash = strtoupper($row['password']);
					}
				}
			//	$logger->logInfo($passwordhash);
				if ($passwordhash!= "" && $passwordhash==$params->oldpassword) {
					// delete old record
					$update_ts = $db->phpdatetime_to_dbdatetime();	
					$sql = "UPDATE " . $this->getUsersTableName() . " SET end_ts = ? , ended_by = ? WHERE  uid = ? ";
					$db->command($sql, $update_ts, $row['uiid'], $row['uid']);
					
 	
					// insert new user record
					$sql = "INSERT INTO " . $this->getUsersTableName() . " (uiid, username, password, active, usergroup, userroledescription, rights, name, phone, email, language, picturefile, status, last_logged, password_last_changed, siteid, selected_sid, selected_cid, selected_pid, autosave_enabled, autosave_frequency, last_activity, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					$db->command($sql, $row['uiid'], $row['username'], $params->newpassword, $row['active'], $row['usergroup'], $row['userroledescription'], $row['rights'], $row['name'], $row['phone'], $row['email'], $row['language'], $row['picturefile'], $row['status'], $row['last_logged'], $update_ts, $row['siteid'], $row['selected_sid'], $row['selected_cid'], $row['selected_pid'], $row['autosave_enabled'], $row['autosave_frequency'], $row['last_activity'], $update_ts , $userId);
					
					$lastinsertid=$db->get_lastinsertid();
				//	$_SESSION['auth_userid'] = $lastinsertid;
					
					$userDetails=$this->getUserDetails();
					$success = true;
					
					
				} else {
					$errortxt = _("A régi jelszavát hibásan adta meg!");
				}
				
				$db->commit();
				$db->close(); 
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: changePassword, Exception: $e");
				$db->rollback();
				$errortxt = _("Adatbátis hiba!");
			}
				
			//$logger->logInfo(Array('success' => $success, 'errortxt' => $errortxt, 'userDetails' => $userDetails));
			return Array('success' => $success, 'errortxt' => $errortxt, 'userDetails' => $userDetails);
    	}
   		
   		/** This function changes the actual user's details
		 *  input params:
		 *		'name' : string
		 * 		'language' : string
		 * 		'phone' : string
		 * 		'email' : string
		 *  
		 * result:
		 * 	'success' : boolean
		 * 	'errortxt : string
		 * 
		 *  @remotable
		 */
    	public function changeUserDetails(stdClass $params)
    	{			
    		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
			
			$success = false;
			$logger = Logger::getInstance();
		//	$logger->logInfo($params);
			try
			{ 
				$db = new Database();
				$db->begin_transaction();
				$sql = "SELECT * FROM " . $this->getUsersViewName() . " WHERE uiid = ? AND active = '1'";
				$db->query($sql, $userId);		
				if($db->row_count() > 0) {
					$row = $db->fetch();
					if ($row != null) {// The user found
						if ($row['name'] != $params->name || $row['language']!=$params->language || $row['email']!=$params->email || $row['phone']!=$params->phone || $row['autosave_enabled']!=$params->autosave_enabled || $row['autosave_frequency']!=$params->autosave_frequency ) {
							// delete old record
							$update_ts = $db->phpdatetime_to_dbdatetime();	
							$sql = "UPDATE " . $this->getUsersTableName() . " SET end_ts = ? , ended_by = ? WHERE  uid = ? ";
							$db->command($sql, $update_ts, $row['uiid'], $row['uid']);
 	
							// insert new user record
							$sql = "INSERT INTO " . $this->getUsersTableName() . " ( uiid, username, password, active, usergroup, userroledescription, rights, name, phone, email, language, picturefile, status, last_logged, password_last_changed, siteid, selected_sid, selected_cid, selected_pid, autosave_enabled, autosave_frequency, last_activity, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
							$db->command($sql, $row['uiid'], $row['username'], $row['password'], $row['active'], $row['usergroup'], $row['userroledescription'], $row['rights'], $params->name,  $params->phone,  $params->email, $params->language, $row['picturefile'], $row['status'], $row['last_logged'], $row['password_last_changed'], $row['siteid'], $row['selected_sid'], $row['selected_cid'], $row['selected_pid'], $params->autosave_enabled , $params->autosave_frequency, $row['last_activity'],$update_ts , $userId);
							
							$lastinsertid=$db->get_lastinsertid();
						//	$_SESSION['auth_userid'] = $lastinsertid;
							
						};
						
						$userDetails=$this->getUserDetails();
						$success = true;
					}
				}	
				$db->commit();
				$db->close(); 
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: changeUserDetails, Exception: $e");
				$db->rollback();
			}
				
		//	$logger->logInfo(Array('success' => $success, 'userDetails' => $userDetails));
			return Array('success' => $success, 'userDetails' => $userDetails);
    	}	    	
 }
?>
