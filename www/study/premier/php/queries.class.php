<?php
/**
 *---------------------------------------------------------------
 *queries.class.php
 *
 * This file contains the queries class functions
 *
 * @package Kron
 * @subpackage messages
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 * @todo:   filter input!!!!
 * @todo:   hardcoded statuses to constants
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');

class Queries
{
  	/**
     *  This function returns the name of the queries view
     */
	public function getQueriesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'queries';
	}
	
	/**
     *  This function returns the name of the queries table
     */
	public function getQueriesTableName()
	{
		return DatabaseConfig::db_tableprefix . 'queries_ts';
	}
	/**
     *  This function returns the name of the users view
     */
	public function getUsersViewName()
	{
		return DatabaseConfig::db_tableprefix . 'users';
	}
	
	 /**
     *  This function returns the name of the querytexts table
     */
	public function getQueryTextsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'querytexts';
	}
	
	 /**
     *  This function returns the name of the sites view
     */
	public function getSitesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'sites';
	}
	
	/**
     *  This function returns the name of the countries view
     */
	public function getPatientsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'patients';
	}

	/**
     *  This function returns the name of the messages view
     */
	public function getFormsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'forms';
	}
	/**
     *  This function returns the name of the messages table
     */
	public function getFormsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'forms_ts';
	}
	
	/**
     *  This function returns the name of the FormFields table
     */
	public function getFormFieldsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields_ts';
	}
	/**
     *  This function returns the name of the FormFields view
     */
	public function getFormFieldsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields';
	}
		
		/**
     *  This function returns the name of the countries view name
     */
	public function	getCountriesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'countries';
	}
		
		
	/** This function returns the open queries
	 *  @remotable
	 */
	public function getOpenQueries()
    {
    	$logger = Logger::getInstance();
		
    	$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$userGroup=isset($_SESSION['auth_userclass']) ? $_SESSION['auth_userclass'] : '';
		$siteId = isset($_SESSION['auth_siteid']) ? $_SESSION['auth_siteid'] : 0 ;
		$selectedSiteId=isset($_SESSION['auth_selected_siteid']) ? $_SESSION['auth_selected_siteid'] : 0 ;
		$selectedCountryId=isset($_SESSION['auth_selected_countryid']) ? $_SESSION['auth_selected_countryid'] : 0 ;
		
 		
		//$logger->logInfo('getOpenQueries called');
	//	$logger->logInfo('userGroup');$logger->logInfo($userGroup);
	//	$logger->logInfo('selectedCountryId');$logger->logInfo($selectedCountryId);
	//	$logger->logInfo('selectedSiteId');$logger->logInfo($selectedSiteId);
		
		/* @TODO : jogosultság ellenőrzés user - patient*/
		
		$a = array();
		try
		{
			$db = new Database();
			/* @TODO : szűrni az open-eket user kórházára*/
			if (UserManagement::userHasPermission("UserManagement", "changeSite")) {
		//	if ( $userGroup==FrameworkConfig::USERGROUP_MONITOR || $userGroup==FrameworkConfig::USERGROUP_PHARMACOVIGILANCE || $userGroup==FrameworkConfig::USERGROUP_SPONSOR || $userGroup==FrameworkConfig::USERGROUP_ADMIN ) {
				if 	( $selectedCountryId == 0 ) { // all countries' all sites' open queries
					$sql = "SELECT s.sitename, p.patientid, s.sid siteid, f.instanceid, f.formtype, q.fieldname, COUNT(IF(q.status = ? OR q.status = ? , 1, NULL)) as openqueries, COUNT(IF(q.status = ? , 1, NULL)) as answeredqueries FROM " . $this->getSitesViewName() . " s"; 
					$sql = $sql . " INNER JOIN " . $this->getPatientsViewName() . " p ON p.siteid = s.sid INNER JOIN " . $this->getFormsViewName() . " f ON p.patientid = f.patientid ";
					$sql = $sql . " INNER JOIN " . $this->getQueriesViewName() . " q ON q.form_instanceid = f.instanceid AND q.action = '" . FrameworkConfig::QUERYTYPE_QUERY . "' GROUP BY p.patientid ";
					$db->query($sql, FrameworkConfig::QUERYSTATUS_OPEN, FrameworkConfig::QUERYSTATUS_REOPENED, FrameworkConfig::QUERYSTATUS_ANSWERED);
				
				} else {
					if ( $selectedSiteId == 0 ) { // one country's all sites' patients
					
					$sql = "SELECT s.sitename, p.patientid, s.sid siteid, f.instanceid, f.formtype, q.fieldname, COUNT(IF(q.status = ? OR q.status = ?, 1, NULL)) as openqueries, COUNT(IF(q.status = ? , 1, NULL)) as answeredqueries FROM " . $this->getSitesViewName() . " s"; 
					$sql = $sql . " INNER JOIN " . $this->getPatientsViewName() . " p ON p.siteid = s.sid AND s.countryid = ? INNER JOIN " . $this->getFormsViewName() . " f ON p.patientid = f.patientid " ;
					$sql = $sql . " INNER JOIN " . $this->getQueriesViewName() . " q ON q.form_instanceid = f.instanceid AND q.action = '" . FrameworkConfig::QUERYTYPE_QUERY . "' GROUP BY p.patientid ";
					$db->query($sql, FrameworkConfig::QUERYSTATUS_OPEN, FrameworkConfig::QUERYSTATUS_REOPENED, FrameworkConfig::QUERYSTATUS_ANSWERED,  $selectedCountryId);
					
					
					} else { // one site's patients
					$sql = "SELECT s.sitename, p.patientid, s.sid siteid, f.instanceid, f.formtype, q.fieldname, COUNT(IF(q.status = ? OR q.status = ?, 1, NULL)) as openqueries, COUNT(IF(q.status = ? , 1, NULL)) as answeredqueries FROM " . $this->getSitesViewName() . " s"; 
					$sql = $sql . " INNER JOIN " . $this->getPatientsViewName() . " p ON p.siteid = s.sid AND p.siteid = ? INNER JOIN " . $this->getFormsViewName() . " f ON p.patientid = f.patientid ";
					$sql = $sql . " INNER JOIN " . $this->getQueriesViewName() . " q ON q.form_instanceid = f.instanceid AND q.action = '" . FrameworkConfig::QUERYTYPE_QUERY . "' GROUP BY p.patientid ";
					$db->query($sql, FrameworkConfig::QUERYSTATUS_OPEN, FrameworkConfig::QUERYSTATUS_REOPENED, FrameworkConfig::QUERYSTATUS_ANSWERED, $selectedSiteId);


					}
				}
			} else // investigator, nurse => one site's patients
			{
					$sql = "SELECT s.sitename, p.patientid, s.sid siteid, f.instanceid, f.formtype, q.fieldname, COUNT(IF(q.status = ? OR q.status = ?, 1, NULL)) as openqueries, COUNT(IF(q.status = ? , 1, NULL)) as answeredqueries FROM " . $this->getSitesViewName() . " s"; 
					$sql = $sql . " INNER JOIN " . $this->getPatientsViewName() . " p ON p.siteid = s.sid AND p.siteid = ? INNER JOIN " . $this->getFormsViewName() . " f ON p.patientid = f.patientid ";
					$sql = $sql . " INNER JOIN " . $this->getQueriesViewName() . " q ON q.form_instanceid = f.instanceid AND q.action = '" . FrameworkConfig::QUERYTYPE_QUERY . "' GROUP BY p.patientid ";
					$db->query($sql, FrameworkConfig::QUERYSTATUS_OPEN, FrameworkConfig::QUERYSTATUS_REOPENED, FrameworkConfig::QUERYSTATUS_ANSWERED, $siteId);
			}	

			$rowcount=	$db->row_count();	
			if( $rowcount > 0) {
				for ($i=1; $i<=$rowcount; $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) {
						if ($row['openqueries']>0 || $row['answeredqueries']>0 ) {
							array_push($a,$row);
						}
					}				
				}					
			}
				
			$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getOpenQueries, Exception: $e");
		}
		//$logger->logInfo($a);
		return $a;
	}


	/** This function returns all queries of a patient
	 *  params:
	 *  patientid - patient id
	 * instanceid : form insatnce id - 
	 *  @remotable
	 */
	public function getPatientQueries(stdClass $params)
    {
    	$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		
 		$logger = Logger::getInstance();
 		//$logger->logInfo('getPatientQueries called');
 		//$logger->logInfo($params);
		
		/* @TODO : jogosultság ellenőrzés user - patient*/
		
		$a = array();
		$b = array();
		try
		{
			$db = new Database();
			
			$sql = "SELECT p.siteid, f.patientid, f.instanceid, f.formtype, q.fieldname, q.fieldlabel, q.status, q.created, q.text FROM " . $this->getFormsViewName() . " f ";
			$sql = $sql . " INNER JOIN " . $this->getPatientsViewName() . " p ON p.patientid = ? AND f.patientid = p.patientid INNER JOIN " . $this->getQueriesViewName() . " q "; 
			$sql = $sql . " ON q.form_instanceid = f.instanceid AND q.action = '" . FrameworkConfig::QUERYTYPE_QUERY . "' AND q.status IN( ?, ?, ?) ORDER BY instanceid, fieldname, created ";
			$db->query($sql, $params->patientid, FrameworkConfig::QUERYSTATUS_OPEN, FrameworkConfig::QUERYSTATUS_REOPENED, FrameworkConfig::QUERYSTATUS_ANSWERED);


			$rowcount=	$db->row_count();	
			if( $rowcount > 0) {
				for ($i=1; $i<=$rowcount; $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) { 
						array_push($a,$row);
					}				
				}					
			}
			
			
					
		$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getPatientQueries, Exception: $e");
		}
		//$logger->logInfo($a);
		return $a;
	}

	
   	
	/** This approveField function can be called as remote function,
	 *  Returns true on success, otherwise false.
	 *  Set Field status to Monitor Approved
	 *  ifdfield is allready approved, cancel the approval  = set field status to Reopened
	 *  @remotable
	 *  @todo 
	 * 	$params->
				form_instanceid 
				fieldnames : [fieldname1, fieldname2]
	 * returns:
	 * 		'success' => $success, 
	 * 		'data' => $params
	 * 				->input fields
	 * 				->history: [ fieldname => {
	 *                    	'datetime' : response.result.data.datetime,
	 *                      'user' : response.result.data.name,
	 *                      'action' : response.result.data.action,
	 *                      'text' : ''
	 * 						'newStatus'
	 * 							}]
	 */
   	public function approveField(stdClass $params)
   	{
    	$logger = Logger::getInstance();
		
	 	$userid=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$userGroup=isset($_SESSION['auth_userclass']) ? $_SESSION['auth_userclass'] : '';
		$userName=isset($_SESSION['auth_name']) ? $_SESSION['auth_name'] : '';
		$success = false;
		//$logger->logInfo("approveField input:");
		//$logger->logInfo($params);
		
		// todo: sanitize input
		
		// filter redundant names
		$fieldNames= array();
		foreach ($params->fieldnames as $value) {
		    $fieldNames[$value] = $value;
		}
		$fieldNameStr = join("','", $fieldNames );
//		$logger->logInfo($fieldNameStr);
		if ($fieldNameStr!= "") {
			$fieldNameStr = "'" . $fieldNameStr . "'";
		}
	//	$logger->logInfo($fieldNameStr);
		
		if ( UserManagement::userHasPermission("Forms", "monitorForm") && $params->form_instanceid >= 0 && $fieldNameStr!= "") { 
			try
			{
				$db = new Database();
				$db->begin_transaction();
				// lezárt páciensre nem szabad semmit csinálni
				$sql = "SELECT p.isclosed FROM " . $this->getPatientsViewName() . " p INNER JOIN " . $this->getFormsViewName() . " f ON f.instanceid = ? AND f.patientid = p.patientid ";
				$db->query($sql, $params->form_instanceid);
				$row = $db->fetch("assoc");
				if ($row != null) {
					if ($row['isclosed'] == 0 ) {// not closed patient
						//$logger->logInfo('nem closed patient!');
				
						$datetime = $db->phpdatetime_to_dbdatetime();
						// get fields
						$fieldRecords = array();	
						$sql = "SELECT *  FROM " . $this->getFormFieldsViewName() . " WHERE form_instanceid = ? AND fieldname IN ( " . $fieldNameStr . " )";
						$db->query($sql, $params->form_instanceid );
						$rowcount=	$db->row_count();
						if( $rowcount > 0) {
							for ($i=1; $i<=$db->row_count(); $i++) {
								$row = $db->fetch("assoc");
								if ($row != null) { 
									$fieldRecords[$i] = $row;
								}
							}
		
							if ( $fieldRecords[1]['status'] == FrameworkConfig::FIELDSTATUS_MONITOR_APPROVED )  {
									 $params->newstatus = FrameworkConfig::FIELDSTATUS_REOPENED;
							} else {
								 $params->newstatus = FrameworkConfig::FIELDSTATUS_MONITOR_APPROVED;
							}
		
							foreach ($fieldRecords as $key => $rec) {
		
								$sql = "UPDATE " . $this->getFormFieldsTableName() . " SET end_ts = ? , ended_by = ? WHERE id = ?";
								$db->command($sql, $datetime, $userid, $rec['id']);
								// create new record with new status
								$sql = "INSERT INTO " . $this->getFormFieldsTableName() . " (form_instanceid, fieldname, istmp, parent, fieldvalue, fieldtype, status, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?)";
								$db->command($sql, $rec['form_instanceid'], $rec['fieldname'], $rec['istmp'], $rec['parent'], $rec['fieldvalue'], $rec['fieldtype'], $params->newstatus, $datetime, $userid );
			
								$success = TRUE;
							}
						} else {
							/* nincs is ilyen rekord, létre kell hozni üresen - @TODO: ellenőrizni  jó-e így?
							$params->newstatus = FrameworkConfig::FIELDSTATUS_MONITOR_APPROVED;
							foreach ($params->fieldnames as $fieldname) {
							    $fieldNames[$value] = $value;
								$sql = "INSERT INTO " . $this->getFormFieldsTableName() . " (form_instanceid, fieldname, istmp, parent, fieldvalue, fieldtype, status, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?)";
								$db->command($sql, $params->form_instanceid, $fieldname, 0, NULL, '', '', FrameworkConfig::FIELDSTATUS_REOPENED, $datetime, $userid );
			
								$success = TRUE;*/
		
						//	}
									$logger->logError("Userid: $userid , ERROR: approveField - Missing field record to approve");				
							
						}
					}
				}
				$db->commit();
				$db->close(); 
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userid , Operation: approveField, Exception: $e");
				$db->rollback();
			}	
		} else { 
			$success = FALSE;
			$logger->logError("Userid: $userid , ERROR: approveField - Only Monitor allowed to approve queries");
		}
		//$logger->logInfo('approveField válasz');$logger->logInfo($params);
		return Array('success' => $success, 'data' => $params);
   	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** Egy form nak az össszes query-jét adja vissza
	 *  params:
	 *  instanceid : form insatnce id - 
	 *  @remotable
	 */
	public function getFormQueries(stdClass $params)
    {
    	$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		
 		$logger = Logger::getInstance();
 		//$logger->logInfo('getFormQueries called');
 		//$logger->logInfo($params);
		$success = false;
		/* @TODO : jogosultság ellenőrzés user - patient*/
		
		$results = array();
		if ( isset($params) && $params->form_instanceid <> '' ){
			try
			{
				$db = new Database();
				$sql = "SELECT q.*, u.name FROM " . $this->getQueriesViewName() . " q INNER JOIN " . $this->getUsersViewName() . " u ON q.created_by = u.uiid AND form_instanceid = ? ";
				
				$db->query($sql, $params->form_instanceid );
				$rowcount=	$db->row_count();	
				if( $rowcount > 0) {
					for ($i=1; $i<=$rowcount; $i++) {
						$row = $db->fetch("assoc");
						if ($row != null) { 
							array_push($results,$row);
							
						}				
					}					
				}

				$db->close(); // Closes the cursor to free up memory
				$success = true;			
				
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: getFormQueries, Exception: $e");
			}
		}
				
		//$logger->logInfo("getFormQueries response");
		//$logger->logInfo(Array('success' => $success, 'data' => $results));

		return Array('success' => $success, 'data' => $results);
	}

	/**  Egy új query-t hoz létre
	 * 	Inputs:
	 * 	$params
	 * 		->patientid : Patient Id 
	 *  	->formtype : Visit Type
	 * 		->istmp : temporary record?
	 * 		->status : status
	 * Eample imnput:
	 * stdClass(
	 *		id =0
	 *		query_iid =0
	 *		form_instanceid =1894
	 *		fieldname ='visitDate'
	 *		fieldlabel ='Vizit dátuma'
	 *		fieldvalue ='2014-02-11 00:00:00'
	 *		fieldtype ='D'
	 *		text ='ujquery'
	 *		status ='O'
	 *		action ='Q'
	 *		name ='Szepesi Dóra Mária'
	 *		created ='2014-02-16 22:22:19'
	 *		created_by =0
	 *		)
	 * 
	 * 
	 *  Return array:
	 *  success: boolean 
	 *  data :  input values including the new id, query_iid
	 *
	 *  @remotable
	 */
   	public  function createQuery(stdClass $params)
   	{
    	$logger = Logger::getInstance();
		
		//$logger->logInfo(" createQuery input fields:");	
		//$logger->logInfo($params);
	//	$logger->logInfo(UserManagement::userHasPermission("Queries", "createQuery"));
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$success = false;
		$lastinsertid= null;		
		
		if (isset($params)){
			if ( $params->form_instanceid <> '' && $params->fieldname <> ''  && (UserManagement::userHasPermission("Queries", "createQuery") || $params->action <> FrameworkConfig::QUERYTYPE_QUERY) )
			{
				try
				{ 
					$db = new Database();
					$db->begin_transaction();
					// lezárt páciensre nem szabad semmit csinálni
					$sql = "SELECT p.isclosed FROM " . $this->getPatientsViewName() . " p INNER JOIN " . $this->getFormsViewName() . " f ON f.instanceid = ? AND f.patientid = p.patientid ";
					$db->query($sql, $params->form_instanceid);
					$row = $db->fetch("assoc");
					if ($row != null) {
						if ($row['isclosed'] == 0 ) {// not closed patient
							//$logger->logInfo('nem closed patient!');
							
					
						 	if ($params->action == FrameworkConfig::QUERYTYPE_COMMENT) {
								$started= $db->phpdatetime_to_dbdatetime();
								$sql = "INSERT INTO " . $this->getQueriesTableName() . " ( query_iid, form_instanceid, fieldname, fieldlabel, fieldvalue, fieldtype, text, status, action, created, created_by, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
								$db->command($sql, $params->query_iid, $params->form_instanceid, $params->fieldname, $params->fieldlabel, $params->fieldvalue, $params->fieldtype, $params->text, $params->status, $params->action, $started, $userId,  $started , $userId);	
								
								$lastinsertid=$db->get_lastinsertid();
						 	} else if ($params->action == FrameworkConfig::QUERYTYPE_QUERY){
								$started= $db->phpdatetime_to_dbdatetime();
								$sql = "INSERT INTO " . $this->getQueriesTableName() . " ( form_instanceid, fieldname, fieldlabel, fieldvalue, fieldtype, text, status, action, created, created_by, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
								$db->command($sql, $params->form_instanceid, $params->fieldname, $params->fieldlabel, $params->fieldvalue, $params->fieldtype, $params->text, $params->status, $params->action, $started, $userId,  $started , $userId);	
								
								$lastinsertid=$db->get_lastinsertid();
		
								$sql = "UPDATE " . $this->getQueriesTableName() . "  SET query_iid = ? WHERE id = ? ";
								$db->command($sql, $lastinsertid, $lastinsertid);
								$params->query_iid= $lastinsertid;	
								$params->id = $lastinsertid;
								$params->created = $started;
								
								// ha monitor által jóváhagyott volt a mező - vissza kell tenni a statust open-re
								$sql = "SELECT * FROM " . $this->getFormFieldsViewName() . " WHERE form_instanceid = ? AND fieldname = ? AND istmp = 0 "  ;
								$db->query($sql, $params->form_instanceid, $params->fieldname );
								$update_ts = $db->phpdatetime_to_dbdatetime();
								if ( $db->row_count() > 0) {
									$row = $db->fetch("assoc");
									if ($row!= null) {
										if ($row['status']== FrameworkConfig::FIELDSTATUS_MONITOR_APPROVED) {
											$sql = "UPDATE " . $this->getFormFieldsTableName() . " SET end_ts = ? , ended_by = ? WHERE id = ? ";
											$db->command($sql, $update_ts, $userId, $row['id']);
											
											// insert new record with new values
											$sql = "INSERT INTO " . $this->getFormFieldsTableName() . " (form_instanceid, fieldname, istmp,  parent, fieldvalue, fieldtype, status, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?)";
											$db->command($sql, $row['form_instanceid'], $row['fieldname'], $row['istmp'], $row['parent'], $row['fieldvalue'], $row['fieldtype'], FrameworkConfig::FIELDSTATUS_REOPENED , $update_ts , $userId);
										}
									}
								} 						
								// meg kell néznem, mi a form statusa, mert ha alá van írva akkor azt le kell szednem ilyenkor (meg query update-nél is...)
		
								$sql = "SELECT * FROM " . $this->getFormsViewName() . " WHERE instanceid = ? AND istmp = '0' ";
								$db->query($sql, $params->form_instanceid );
								if ( $db->row_count() > 0) {
									$formRow = $db->fetch("assoc");
									if ($formRow!= null) {
										if ($formRow['signed'] != null || ( $formRow['signed_by'] != null && $formRow['signed_by'] != '' )) {// alá van írva
											// eredetit törlöm, létrehozok egy új példányt az új adatokkal
										$sql = "UPDATE " . $this->getFormsTableName() . " SET end_ts = ? , ended_by = ? WHERE id = ? ";
										$db->command($sql, $update_ts, $userId, $formRow['id']);
		
										
										$sql = "INSERT INTO " . $this->getFormsTableName() . " (instanceid, parentid, patientid, formtype, istmp,  status, validity, duedate, created, signed_by, signed, reviewed_by, reviewed, approved_by, approved, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
										$db->command($sql, $formRow['instanceid'], $formRow['parentid'], $formRow['patientid'], $formRow['formtype'], $formRow['istmp'],  $formRow['status'], $formRow['validity'], $formRow['duedate'], $formRow['created'], "", null, $formRow['reviewed_by'], $formRow['reviewed'], $formRow['approved_by'], $formRow['approved'], $update_ts , $userId);
		
										}
									}
								}
							}
						$success = true;
						}
					}
					$db->commit();	  
					$db->close(); 
					
				}
				catch (Exception $e)
				{// Log the error
					$logger->logError("Userid: $userId , Operation: createQuery, Exception: $e");
					$db->rollback();
				}	
			} 
		}//end if 	
		//$logger->logInfo(" createQuery response:");	
		//$logger->logInfo(Array('success' => $success, 'data' => $params ));	
		return  Array('success' => $success, 'data' => $params );
   	}

   	/** Módosít egy Query rekord státusát (mást nem!)
	 *  klinens oldalon store-ban zárol le/nyitom úja/teszem megválaszolttá
	 * 	Inputs:
	 * 	$params->status
	 * 	
	 *  Return array:
	 *  success: boolean
	 *  data :  input values including 
	 * 
	 *   @remotable
	 */
   	public  function updateQuery( $input)
   	{
    	$logger = Logger::getInstance();
		
		//$logger->logInfo(" updateQuery input:");	
		//$logger->logInfo($input);
		if (is_array($input) ) {
			$inputArray = $input;	
		} else {
			$inputArray = array($input);
		}
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$success = false;
		
		try
		{ 
			$db = new Database();
			$db->begin_transaction();
			// lezárt páciensre nem szabad semmit csinálni
			$firstParam = reset($inputArray);
			$sql = "SELECT p.isclosed FROM " . $this->getPatientsViewName() . " p INNER JOIN " . $this->getFormsViewName() . " f ON f.instanceid = ? AND f.patientid = p.patientid ";
			$db->query($sql, $firstParam->form_instanceid);
			$row = $db->fetch("assoc");
			if ($row != null) {
				if ($row['isclosed'] == 0 ) {// not closed patient
					//$logger->logInfo('nem closed patient!');

					$update_ts = $db->phpdatetime_to_dbdatetime();	
					foreach ($inputArray as $key => $params) {
						if ($params->query_iid>=0 ) {
							// csak queryt update-elek, comment esetében csak create és delete játszik...
							$sql = "SELECT * FROM " . $this->getQueriesViewName() . " WHERE query_iid = ? and action = ? "  ;
							$db->query($sql, $params->query_iid, FrameworkConfig:: QUERYTYPE_QUERY);
							if ( $db->row_count() > 0) {
								$row = $db->fetch("assoc");
								if ($row != null) {
										
			
									// eredetit törlöm, létrehozok egy új példányt az új adatokkal
									$sql = "UPDATE " . $this->getQueriesTableName() . " SET end_ts = ? , ended_by = ? WHERE query_iid = ? and action = ? and ended_by IS NULL ";
									$db->command($sql, $update_ts, $userId, $params->query_iid, FrameworkConfig:: QUERYTYPE_QUERY);
									
									$sql = "INSERT INTO " . $this->getQueriesTableName() . " ( query_iid, form_instanceid, fieldname, fieldlabel, fieldvalue, fieldtype, text, status, action, created, created_by, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
									$db->command($sql, $row['query_iid'], $row['form_instanceid'], $row['fieldname'], $row['fieldlabel'], $row['fieldvalue'], $row['fieldtype'], $row['text'], $params->status, $row['action'], $row['created'], $row['created_by'], $update_ts , $userId);	
									
									$lastinsertid=$db->get_lastinsertid();
									$params->id = $lastinsertid;
									
								// ha az új status reopened, meg kell nézni, hogy az eredeti form mező státusa mi volt, mert ha monitor approved, akkor azt is át kell tenni reopened-re
									if ($params->status == FrameworkConfig::QUERYSTATUS_REOPENED) {
										$sql = "SELECT * FROM " . $this->getFormFieldsViewName() . " WHERE form_instanceid = ? AND fieldname = ? AND istmp = 0 "  ;
										$db->query($sql, $row['form_instanceid'], $row['fieldname']);
										if ( $db->row_count() > 0) {
											$fieldRow = $db->fetch("assoc");
											if ($fieldRow!= null) {
	
												if ($fieldRow['status'] == FrameworkConfig::FIELDSTATUS_MONITOR_APPROVED) {
													// set old record deleted 
													$sql = "UPDATE " . $this->getFormFieldsTableName() . " SET end_ts = ? , ended_by = ? WHERE form_instanceid = ? AND fieldname = ? AND istmp = 0 AND ended_by IS NULL ";
													$db->command($sql, $update_ts, $userId, $row['form_instanceid'], $row['fieldname']);
													
													// insert new record with new values
													$sql = "INSERT INTO " . $this->getFormFieldsTableName() . " (form_instanceid, fieldname, istmp,  parent, fieldvalue, fieldtype, status, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?)";
													$db->command($sql, $fieldRow['form_instanceid'], $fieldRow['fieldname'], 0, 0, $fieldRow['fieldvalue'], $fieldRow['fieldtype'], FrameworkConfig::FIELDSTATUS_REOPENED, $update_ts , $userId);
													
												}
												
											}
										}	
										// meg kell néznem, mi a form statusa, mert ha alá van írva akkor azt le kell szednem ilyenkor (meg query update-nél is...)
								
										$sql = "SELECT * FROM " . $this->getFormsViewName() . " WHERE instanceid = ? AND istmp = '0' ";
										$db->query($sql, $row['form_instanceid'] );
										if ( $db->row_count() > 0) {
											$formRow = $db->fetch("assoc");
											if ($formRow!= null) {
												if ($formRow['signed'] != null || $formRow['signed_by'] != null || $formRow['signed_by'] != '' ) {// alá van írva
													// eredetit törlöm, létrehozok egy új példányt az új adatokkal
												$sql = "UPDATE " . $this->getFormsTableName() . " SET end_ts = ? , ended_by = ? WHERE instanceid = ? AND istmp = '0' AND ended_by IS NULL ";
												$db->command($sql, $update_ts, $userId, $row['form_instanceid'] );
												
												$sql = "INSERT INTO " . $this->getFormsTableName() . " (instanceid, parentid, patientid, formtype, istmp,  status, validity, duedate, created, signed_by, signed, reviewed_by, reviewed, approved_by, approved, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
												$db->command($sql, $formRow['instanceid'], $formRow['parentid'], $formRow['patientid'], $formRow['formtype'], $formRow['istmp'],  $formRow['status'], $formRow['validity'], $formRow['duedate'], $formRow['created'], null, null, $formRow['reviewed_by'], $formRow['reviewed'], $formRow['approved_by'], $formRow['approved'], $update_ts , $userId);
												
												}
											}
										}
									}
			
									$success = true;
								}
							}	
						}
					}
				}
			}	
		$db->commit();
		$db->close(); 
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: updateQuery, Exception: $e");
			$db->rollback();
		}	
		
		//$logger->logInfo(" updateQuery response:");	
		//$logger->logInfo(Array('success' => $success, 'data' => $params));	
			
		return  Array('success' => $success, 'data' => $input);
   	}
	
   	/** Töröltre állítja a query rekordot az query_iid alapján (ha van kommnet az összes kommentet is törlni
	 *  A kliens oldalról tömb jön, query + commentjei - elvileg uaz mindnem a query_iid-ja
	 * 
	 * 	Inputs:
	 * 	$params->query_iid :form instance id
	 * 	
	 *  Return array:
	 *  success: boolean
	 *  data :  input paraméterek 
	 *
	 *  @remotable
	 */
   	public  function deleteQuery( $input)
   	{
    	$logger = Logger::getInstance();
		
		//$logger->logInfo(" deleteQuery input params");	
		//$logger->logInfo($input);	
		if (is_array($input) ) {
			$inputArray = $input;	
		} else {
			$inputArray = array($input);
		}
		// todo access control if necessary???
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		
		$success = false;

		try
		{ 
			$db = new Database();
			$db->begin_transaction();
			// lezárt páciensre nem szabad semmit csinálni
			$firstParam = reset($inputArray);
			$sql = "SELECT p.isclosed FROM " . $this->getPatientsViewName() . " p INNER JOIN " . $this->getFormsViewName() . " f ON f.instanceid = ? AND f.patientid = p.patientid ";
			$db->query($sql, $firstParam->form_instanceid);
			$row = $db->fetch("assoc");
			if ($row != null) {
				if ($row['isclosed'] == 0 ) {// not closed patient
					//$logger->logInfo('nem closed patient!');
				
					foreach ($inputArray as $key => $params) {
						if ($params->query_iid >= 0) {
							
							$ended = $db->phpdatetime_to_dbdatetime();
							$sql = "UPDATE " . $this->getQueriesTableName() . " SET end_ts = ? , ended_by = ? WHERE query_iid = ? AND ended_by IS NULL";
							$db->command($sql, $ended, $userId, $params->query_iid);
							$success = true;

						}
					}	
				}
			}	
			$db->commit();		
			$db->close(); 
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: deleteQuery, Exception: $e");
			$db->rollback();
		}	
		 //end if 	
	
		return  Array('success' => $success, 'data' => $input);
   	}	
	
	
}
?>