<?php
/**
 *---------------------------------------------------------------
 * download.php
 *
 * This php serves the file download requests.
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 * @todo Sanitize the file parameter for SQL injection
 * @todo Log the invalid download attempts
 * 
 *---------------------------------------------------------------
 */

// Initialize session
ini_set('session.use_cookies', 1);
ini_set('session.use_only_cookies', 1);
session_start();
if (!isset ($_SESSION['auth_userloggedin']) || !$_SESSION['auth_userloggedin'] ||
	!isset ($_SESSION['auth_userclass']) || !$_SESSION['auth_userclass'])
{ // The user not logged in
	die('Direct script access is not allowed!');
    //return "404";
}

// Calculate the php path
$php_path = str_replace("\\", "/", realpath(dirname(__FILE__)));

// Calculate the study path
$study_path = join(array_slice(explode( "/" ,$php_path), 0, -1), "/");

// Calculate the study directory
$study_dir = basename ($study_path ,"/");

// Calculate the subdomain path
$subdomain_path = join(array_slice(explode( "/" ,$study_path), 0, -1), "/");

// Calculate the public (www) path
$public_path = join(array_slice(explode( "/" ,$subdomain_path), 0, -1), "/");

// Calculate the base path
$base_path = join(array_slice(explode( "/" ,$public_path), 0, -1), "/");

// Load the main configuration file
// Warning the 'configs' config_folder hardcoded here !!!
require_once($base_path . '/' . $study_dir . '/configs/app.conf');

require_once('logger.class.php');
require_once('database.class.php');
require_once('usermanagement.class.php');

function getDocumentsTableName()
{
	return DatabaseConfig::db_tableprefix . 'documents';
}

$logger = Logger::getInstance();

$get_parameters = array();
if (isset($_SERVER['QUERY_STRING'])) {
	$pairs = explode('&', $_SERVER['QUERY_STRING']);
	foreach($pairs as $pair) {
		$part = explode('=', $pair);
		$get_parameters[$part[0]] = urldecode($part[1]);
	}
}

if (!isset($get_parameters['type']) || !isset($get_parameters['file']))
{// No parameters passed to the php
	echo '404';
	return "404";
}

// TODO: sanitize the file parameter for SQL injection
$type = $get_parameters['type'];
$file = $get_parameters['file'];
$forcesave = 'false';
if (isset($_GET['forcesave']))
{
	$forcesave = $get_parameters['forcesave'];
}
$filename = "404";
$isprint = false;
if ($type == 'document' )
{// We need to download a document
	// Check for the permission
	$class = 'Documents';
	$method = 'download';
	if (UserManagement::userHasPermission($class, $method) != true)  
	{
		//Log the missing permission
		$logger->logPermissionError((object)array('action' => $class, 'method' => $method));
		echo '404';
		return "404";
	}
		
	$alias = $file;
	try
	{// Look for the user in the database
		$db = new Database();
		$sql = "SELECT file FROM " . getDocumentsTableName() . " WHERE alias='". $alias . "'";
		$db->query($sql);	
		if ($db->row_count() > 0) {
			if ($db->row_count() > 1)
			{ // More than one file found, log a warning
				$logger->logWarn("Download: More than one file found in the documents table for the following alias: '$alias'");
			}
			$row = $db->fetch("assoc");
			if ($row != null) { 
				$filename = $row['file'];
			}								
		}
		else {// File not exists in the database
			$db->close(); // Closes the cursor to free up memory
			echo '404';
			return "404";
		}
		$db->close(); // Closes the cursor to free up memory
	}
	catch (Exception $e)
	{// Log the error
		$logger->logError($e);
		echo '404';
		return "404";
	}
	$fullname = DOCUMENTS_PATH. $filename;
}
else if ($type == 'printout')
{// We need to download a pdf print
	$class = 'Forms';
	$method = 'printForm';
	if (UserManagement::userHasPermission($class, $method) != true)  
	{
		//Log the missing permission
		$logger->logPermissionError((object)array('action' => $class, 'method' => $method));
		echo '404';
		return "404";
	}
	$filename = $file;
	$fullname = PRINTOUTS_PATH . $filename;
	$isprint = true;
	//$logger->logInfo($fullname);
}
else
{// Unknown type
	echo '404';
	return "404";
}
if (file_exists($fullname)) {
	
	$fileextension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));;

	if ($isprint == false || $forcesave == 'true')
	{
		header('Content-Description: File Transfer');
		header('Content-Type: application/force-download');
		header('Content-Type: application/octet-stream');
		header('Content-Type: application/download');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
		header('Pragma: public');
		header('Content-Length: ' . filesize($fullname));
		header('Content-Disposition: attachment; filename=' . urlencode(basename($fullname)));
		header('Content-Transfer-Encoding: binary');
	}
	else {
		header('Content-Type: application/pdf');
		header('Content-Length: ' . filesize($fullname));
		header('Content-Disposition: inline');
	}
	ob_clean();
    flush();
	readfile($fullname);
	if ($isprint == true)
	{// Delete printout pdf file
		ob_flush();
		flush();
		unlink($fullname);
	}
	exit;
}
else {// File not exists
	echo '404';
	return "404";
}
?>