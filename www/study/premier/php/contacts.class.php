<?php
/**
 *---------------------------------------------------------------
 *contacts.class.php
 *
 * This file contains the Contacts class
 *
 * @package Kron
 * @subpackage contacts
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');

class Contacts
{
  	/**
     *  This function returns the name of the users table
     */
	public function getContactsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'contacts';
	}
	
	/** This function returns the contacts data
	 *  @remotable
	 */
	public function getContacts()
    {
 		$logger = Logger::getInstance();
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$a = array();
		try
		{// Look for the user in the database
			$db = new Database();			
			$sql = "SELECT * FROM " . $this->getContactsTableName() . " WHERE active = 1 " ;
			$db->query($sql);		
			if($db->row_count() > 0) {
				for ($i=1; $i<=$db->row_count(); $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) { 
						array_push($a,$row);
					}				
				}					
			}
			$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getContacts, Exception: $e");
		}
		return $a;
	}
 }
?>
