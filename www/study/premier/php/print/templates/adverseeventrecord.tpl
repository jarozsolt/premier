<div>
<h4 style="text-align: center; width: 100%; page-break-before: always;">[@title]</h4>
<fieldset class="nobreak">
<legend>A nemkívánatos esemény diagnózisa vagy tünete (amennyiben lehetséges adja meg a diagnózist)</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td class="label"><label for="diagnosis">Diagnózis vagy tünet:</label></td>
			<td class="value"><input type="text" name="diagnosis" [@diagnosis] /></td>
		</tr>
	</tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>A nemkívánatos esemény kezdete és vége</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td class="label"><label for="startDate">Kezdete:</label></td>
			<td class="value" width="15%"><input type="text" name="startDate" [@startDate] /></td>
			<td><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
		</tr>
		<tr>
    		<td class="label"><label for="endDate">Vége:</label></td>
			<td class="value" width="15%"><input type="text" name="endDate" [@endDate] /></td>
			<td><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
			<td><input type="checkbox" name="endDateog" [@endDateog] />&nbsp;<label style="font-size: 7pt;" for="endDateog">folyamatban</label></td>
		</tr>
	</tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Súlyosság</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td>
				<input type="radio" name="severity" id="severity1" value="1" [@severity1]/>&nbsp;<label for="severity1">enyhe</label>
			</td>
			<td>
				<input type="radio" name="severity" id="severity2" value="2" [@severity2]/>&nbsp;<label for="severity2">közepesen súlyos</label>
			</td>
			<td>
				<input type="radio" name="severity" id="severity3" value="3" [@severity3]/>&nbsp;<label for="severity3">súlyos</label>
			</td>
		</tr>
	</tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Kapcsolat a beültetett eszközzel</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td>
				<input type="radio" name="relation"  id="relation1" value="1" [@relation1]/>&nbsp;<label for="relation1">nincs kapcsolat</label>
			</td>
			<td>
				<input type="radio" name="relation"  id="relation2" value="2" [@relation2]/>&nbsp;<label for="relation2">lehetséges</label>
			</td>
			<td>
				<input type="radio" name="relation"  id="relation3" value="3" [@relation3]/>&nbsp;<label for="relation3">valószínűsíthető</label>
			</td>
			<td>
				<input type="radio" name="relation"  id="relation4" value="4" [@relation4]/>&nbsp;<label for="relation4">egyértelmű</label>
			</td>
		</tr>
	</tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Megtett intézkedések</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td colspan="3">
				<input type="checkbox" name="actionTaken-DeviceRemoved"  [@actionTaken-DeviceRemoved] />&nbsp;<label for="actionTaken-DeviceRemoved">A beültetett eszköz eltávolítása</label>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="checkbox" name="actionTaken-DeviceNotChanged"  [@actionTaken-DeviceNotChanged] />&nbsp;<label for="actionTaken-DeviceNotChanged">A beültetett eszköz változatlan</label>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="checkbox" name="actionTaken-Unknown"  [@actionTaken-Unknown] />&nbsp;<label for="actionTaken-Unknown">Ismeretlen</label>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="checkbox" name="actionTaken-NotApplicable"  [@actionTaken-NotApplicable] />&nbsp;<label for="actionTaken-NotApplicable">Nem alkalmazható, a beteg nem kapta meg az eszközt</label>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="checkbox" name="actionTaken-OtherMedication"  [@actionTaken-OtherMedication] />&nbsp;<label for="actionTaken-OtherMedication">Egyéb kezelést is kapott a beteg</label>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="checkbox" name="actionTaken-Other"  [@actionTaken-Other] />&nbsp;<label for="actionTaken-Other">Egyéb</label>
			</td>
		</tr>
		<tr>
    		<td class="label"><label for="otherActionDetails">Egyéb intézkedés részletezése:</label></td>
			<td class="value" colspan="2"><input type="text" name="otherActionDetails" [@otherActionDetails] /></td>
		</tr>
	</tbody>
</table>
</fieldset>
</div>
<div>
<fieldset class="nobreak">
<legend>Az esemény kimenetele</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td colspan="2">
				<input type="radio" name="outcome" id="outcome1" value="1" [@outcome1]/>&nbsp;<label for="outcome1">megoldódott / befejeződött</label>
			</td>
		</tr>
		<tr>
    		<td colspan="2">
				<input type="radio" name="outcome" id="outcome2" value="2" [@outcome2]/>&nbsp;<label for="outcome2">javul</label>
			</td>
		</tr>
		<tr>
    		<td colspan="2">
				<input type="radio" name="outcome" id="outcome3" value="3" [@outcome3]/>&nbsp;<label for="outcome3">folyamatban</label>
			</td>
		</tr>
		<tr>
    		<td colspan="2">
				<input type="radio" name="outcome" id="outcome4" value="4" [@outcome4]/>&nbsp;<label for="outcome4">végzetes</label>
			</td>
		</tr>
		<tr>
    		<td colspan="2">
				<input type="radio" name="outcome" id="outcome5" value="5" [@outcome5]/>&nbsp;<label for="outcome5">ismeretlen</label>
			</td>
		</tr>
		<tr>
    		<td colspan="2">
				<input type="radio" name="outcome" id="outcome6" value="6" [@outcome6]/>&nbsp;<label for="outcome6">megoldódott, de következménye van</label>
			</td>
		</tr>
		<tr>
    		<td class="label"><label for="outcomewithConsequenceDescription">Következmény leírása:</label></td>
			<td class="value"><input type="text" name="outcomewithConsequenceDescription" [@outcomewithConsequenceDescription] /></td>
		</tr>
	</tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Súlyos nemkívánatos esemény</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td width="65%"><label for="ae_serious">Súlyos nemkívánatos esemény?:</label></td>
    		<td>
				<input type="radio" name="ae_serious"  id="ae_serious1" value="1" [@ae_serious1]/>&nbsp;<label for="ae_serious1">Igen</label>
			</td>
			<td>
				<input type="radio" name="ae_serious"  id="ae_serious2" value="2" [@ae_serious2]/>&nbsp;<label for="ae_serious2">Nem</label>
			</td>
		</tr>
	</tbody>
</table>
<fieldset class="nobreak">
<legend>Súlyosság kritériumai</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td colspan="3">
				<input type="radio" name="seriousType" id="seriousType1" value="1" [@seriousType1]/>&nbsp;<label for="seriousType1">hospitalizáció vagy annak meghosszabbodása</label>
			</td>
		</tr>
		<tr>
    		<td colspan="3">
				<input type="radio" name="seriousType" id="seriousType2" value="2" [@seriousType2]/>&nbsp;<label for="seriousType2">tartós vagy jelentős rokkantság/munkaképtelenség</label>
			</td>
		</tr>
		<tr>
    		<td colspan="3">
				<input type="radio" name="seriousType" id="seriousType3" value="3" [@seriousType3]/>&nbsp;<label for="seriousType3">veleszületett rendellenesség / születési károsodás</label>
			</td>
		</tr>
		<tr>
    		<td colspan="3">
				<input type="radio" name="seriousType" id="seriousType4" value="4" [@seriousType4]/>&nbsp;<label for="seriousType4">jelentős orvosi esemény</label>
			</td>
		</tr>
		<tr>
    		<td class="label"><label for="comments">Esemény leírása:</label></td>
			<td class="value" colspan="2"><input type="text" name="medicalEventDescription" [@medicalEventDescription] /></td>
		</tr>
		<tr>
    		<td colspan="3">
				<input type="radio" name="seriousType" id="seriousType5" value="5" [@seriousType5]/>&nbsp;<label for="seriousType5">életveszélyes állapot</label>
			</td>
		</tr>
		<tr>
    		<td colspan="3">
				<input type="radio" name="seriousType" id="seriousType6" value="6" [@seriousType6]/>&nbsp;<label for="seriousType6">halál</label>
			</td>
		</tr>
		<tr>
    		<td class="label"><label for="dateofDeath">Halál dátuma:</label></td>
			<td class="value" width="15%"><input type="text" name="dateofDeath" [@dateofDeath] /></td>
			<td><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
		</tr>
		<tr>
    		<td class="label"><label for="causeofDeath">Halál okának leírása:</label></td>
			<td class="value" colspan="2"><input type="text" name="causeofDeath" [@causeofDeath] /></td>
		</tr>
	</tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Az eszköz alkalmazásának kezdete</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td class="label"><label for="firstAdministrationDate">Alkalmazás kezdete:</label></td>
			<td class="value" width="15%"><input type="text" name="firstAdministrationDate" [@firstAdministrationDate] /></td>
			<td><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
			<td width="35%"><input type="checkbox" name="firstAdministrationDateog" [@firstAdministrationDateog] />&nbsp;<label style="font-size: 7pt;" for="firstAdministrationDateog">nem&nbsp;kezdte&nbsp;el&nbsp;alkalmazni&nbsp;az&nbsp;eszközt</label></td>
		</tr>
	</tbody>
</table>
</fieldset>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>További információk, megjegyzések, az esemény leírása</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td class="label"><label for="comments">Megjegyzések, információk:</label></td>
			<td class="value"><input type="text" style="font-family: DejaVu Sans, sans-serif;" readonly name="comments" [@comments] /></td>
		</tr>
	</tbody>
</table>
</fieldset>
</div>