<div>
<h4 style="text-align: center; width: 100%; page-break-before: always;">[@title]</h4>
<fieldset class="nobreak">
<legend>Kórelőzmény, egyéb betegség - [@diagnosisValue]</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td class="label"><label for="diagnosis">Diagnózis:</label></td>
			<td class="value" colspan="4"><input type="text" name="diagnosis" [@diagnosis] /></td>
		</tr>
		<tr>
    		<td class="label"><label for="startDate">Kezedete:</label></td>
			<td class="value" width="15%"><input type="text" name="startDate" [@startDate] /></td>
			<td colspan="3"><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
		</tr>
		<tr>
    		<td class="label"><label for="endDate">Vége:</label></td>
			<td class="value" width="15%"><input type="text" name="endDate" [@endDate] /></td>
			<td colspan="3"><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
		</tr>
		<tr>
			<td class="label"><label for="serious">Súlyosság:</label></td>
    		<td>
				<input type="radio" name="serious" id="serious1" value="1" [@serious1]/>&nbsp;<label for="serious1">enyhe</label>
			</td>
			<td>
				<input type="radio" name="serious" id="serious2" value="2" [@serious2]/>&nbsp;<label for="serious2">mérsékelt</label>
			</td>
			<td>
				<input type="radio" name="serious" id="serious3" value="3" [@serious3]/>&nbsp;<label for="serious3">súlyos</label>
			</td>
			<td>
				<input type="radio" name="serious" id="serious4" value="4" [@serious4]/>&nbsp;<label for="serious4">nem alk.</label>
			</td>
		</tr>
		<tr>
    		<td class="label"><label for="requireTreatment">Kezelést igényel?:</label></td>
    		<td>
				<input type="radio" name="requireTreatment"  id="requireTreatment1" value="1" [@requireTreatment1]/>&nbsp;<label for="requireTreatment1">Igen</label>
			</td>
			<td>
				<input type="radio" name="requireTreatment"  id="requireTreatment2" value="2" [@requireTreatment2]/>&nbsp;<label for="requireTreatment2">Nem</label>
			</td>
			<td></td>
		</tr>
	</tbody>
</table>
</fieldset>
</div>