<html>
<head>
<title>[@studyname] Clinical Study / [@aename] Report / Patient no.: [@patientid]</title>
</head>
<body>
<p>Tisztelt Címzett!</p>
Tájékoztatjuk, hogy új [@aename] vagy [@aename] Follow-up történt a [@studyname] vizsgálatban. Mellékelten küldjük a [@aename] oldalt és a beteg adataira vonatkozó információkat pdf file-ban. Amennyiben rendelkezik eCRF hozzáféréssel, az alábbi link-re kattintva közvetlenül meg tudja nézni a [@aename] oldalt a rendszerben:<br>
<a href="[@link]">[@link]</a><p>Üdvözlettel,<br>KRON CDMS / [@studyname]<br>info@datanova.hu</p>
</body>
</html>