<div class="header">
<table style="width: 100%; border: solid 1px #888; outline: solid 1px #EEE; padding: 10px;">
    <tbody>
		<tr>
    		<td width="20%">Páciens:</td>
    		<td width="32%">[@patientid]</td>
    		<td width="18%">Vizsgálat:</td>
    		<td>[@studyname]</td>
		</tr>
		<tr>
    		<td>Vizsgálóhely:</td>
    		<td>[@siteid]</td>
    		<td>Dátum:</td>
    		<td>[@formtime]</td>
		</tr>
	</tbody>
</table>
</div>
