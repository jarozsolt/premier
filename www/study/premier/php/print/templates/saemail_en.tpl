<html>
<head>
<title>[@studyname] Clinical Study / [@aename] Report / Patient no.: [@patientid]</title>
</head>
<body>
<p>Dear SAFETY,</p>
Please be informed that new [@aename] or [@aename] Follow-up has been occured at [@studyname] study. Please find attached the [@aename] page and Patient data information in pdf. If you have access to the eCRF system, please find here the direct link to the AE/SAE pages for your reference:<br>
<a href="[@link]">[@link]</a><p>Best regards,<br>KRON CDMS / [@studyname]<br>info@datanova.hu</p>
</body>
</html>