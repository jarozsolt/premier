<div>
<h4 style="text-align: center; width: 100%; page-break-before: always;">[@title]</h4>
<fieldset class="nobreak">
<legend>A MACCE kezdete és vége</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td class="label"><label for="startDate">Esemény kezdete:</label></td>
			<td class="value" width="15%"><input type="text" name="startDate" [@startDate] /></td>
			<td><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
		</tr>
		<tr>
    		<td class="label"><label for="endDate">Esemény vége:</label></td>
			<td class="value" width="15%"><input type="text" name="endDate" [@endDate] /></td>
			<td><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
			<td><input type="checkbox" name="endDateog" [@endDateog] />&nbsp;<label style="font-size: 7pt;" for="endDateog">folyamatban</label></td>
		</tr>
	</tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>MACCE típusa</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="macceType" id="macceType1" value="1" [@macceType1]/>&nbsp;<label for="macceType1">Stroke</label>
            </td>
            <td>
                <input type="radio" name="macceType" id="macceType2" value="2" [@macceType2]/>&nbsp;<label for="macceType2">Miokardiális infarktus (MI)</label>
            </td>
            <td>
                <input type="radio" name="macceType" id="macceType3" value="3" [@macceType3]/>&nbsp;<label for="macceType3">Ismételt revaszkularizáció</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>A MACCE kapcsolata a beültetett eszközzel</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="MACCEKapcsolataAzEszkozzel"  id="MACCEKapcsolataAzEszkozzel1" value="1" [@MACCEKapcsolataAzEszkozzel1]/>&nbsp;<label for="MACCEKapcsolataAzEszkozzel1">Nincs kapcsolat</label>
            </td>
            <td>
                <input type="radio" name="MACCEKapcsolataAzEszkozzel"  id="MACCEKapcsolataAzEszkozzel2" value="2" [@MACCEKapcsolataAzEszkozzel2]/>&nbsp;<label for="MACCEKapcsolataAzEszkozzel2">Lehetséges</label>
            </td>
            <td>
                <input type="radio" name="MACCEKapcsolataAzEszkozzel"  id="MACCEKapcsolataAzEszkozzel3" value="3" [@MACCEKapcsolataAzEszkozzel3]/>&nbsp;<label for="MACCEKapcsolataAzEszkozzel3">Valószínűsíthető</label>
            </td>
            <td>
                <input type="radio" name="MACCEKapcsolataAzEszkozzel"  id="MACCEKapcsolataAzEszkozzel4" value="4" [@MACCEKapcsolataAzEszkozzel4]/>&nbsp;<label for="MACCEKapcsolataAzEszkozzel4">Egyértelmű</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>A MACCE kapcsolata a beavatkozással (beültetéssel)</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="MACCEKapcsolataABeavatkozassal"  id="MACCEKapcsolataABeavatkozassal1" value="1" [@MACCEKapcsolataABeavatkozassal1]/>&nbsp;<label for="MACCEKapcsolataABeavatkozassal1">Nincs kapcsolat</label>
            </td>
            <td>
                <input type="radio" name="MACCEKapcsolataABeavatkozassal"  id="MACCEKapcsolataABeavatkozassal2" value="2" [@MACCEKapcsolataABeavatkozassal2]/>&nbsp;<label for="MACCEKapcsolataABeavatkozassal2">Lehetséges</label>
            </td>
            <td>
                <input type="radio" name="MACCEKapcsolataABeavatkozassal"  id="MACCEKapcsolataABeavatkozassal3" value="3" [@MACCEKapcsolataABeavatkozassal3]/>&nbsp;<label for="MACCEKapcsolataABeavatkozassal3">Valószínűsíthető</label>
            </td>
            <td>
                <input type="radio" name="MACCEKapcsolataABeavatkozassal"  id="MACCEKapcsolataABeavatkozassal4" value="4" [@MACCEKapcsolataABeavatkozassal4]/>&nbsp;<label for="MACCEKapcsolataABeavatkozassal4">Egyértelmű</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Az eszköz alkalmazásának kezdete</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td class="label"><label for="firstAdministrationDate">Alkalmazás kezdete:</label></td>
            <td class="value" width="15%"><input type="text" name="firstAdministrationDate" [@firstAdministrationDate] /></td>
            <td><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
            <td width="35%"><input type="checkbox" name="firstAdministrationDateog" [@firstAdministrationDateog] />&nbsp;<label style="font-size: 7pt;" for="firstAdministrationDateog">nem&nbsp;kezdte&nbsp;el&nbsp;alkalmazni&nbsp;az&nbsp;eszközt</label></td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Súlyos nemkívánatos esemény</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td width="65%"><label for="ae_serious">Súlyos nemkívánatos esemény?:</label></td>
            <td>
                <input type="radio" name="ae_serious"  id="ae_serious1" value="1" [@ae_serious1]/>&nbsp;<label for="ae_serious1">Igen</label>
            </td>
            <td>
                <input type="radio" name="ae_serious"  id="ae_serious2" value="2" [@ae_serious2]/>&nbsp;<label for="ae_serious2">Nem</label>
            </td>
        </tr>
    </tbody>
</table>
<fieldset class="nobreak">
<legend>Súlyosság kritériumai</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td colspan="3">
                <input type="radio" name="saeTipus" id="saeTipus1" value="1" [@saeTipus1]/>&nbsp;<label for="saeTipus1">Halál</label>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="saeHalalOka">A halál oka:</label></td>
            <td colspan="2">
                <table width="100%">
                    <tr>
                        <td><input type="radio" name="saeHalalOka" id="saeHalalOka1" value="1" [@saeHalalOka1]/>&nbsp;<label for="saeHalalOka1">Kardiális</label></td>
                        <td><input type="radio" name="saeHalalOka" id="saeHalalOka2" value="2" [@saeHalalOka1]/>&nbsp;<label for="saeHalalOka2">Nem kardiális</label></td>
                        <td><input type="radio" name="saeHalalOka" id="saeHalalOka3" value="3" [@saeHalalOka1]/>&nbsp;<label for="saeHalalOka3">Ismeretlen</label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="saeHalalStentTrombozisa">Stent trombózis:</label></td>
            <td colspan="2">
                <table width="100%">
                    <tr>
                        <td><input type="radio" name="saeHalalStentTrombozisa" id="saeHalalStentTrombozisa1" value="1" [@saeHalalStentTrombozisa1]/>&nbsp;<label for="saeHalalStentTrombozisa1">Igen</label></td>
                        <td><input type="radio" name="saeHalalStentTrombozisa" id="saeHalalStentTrombozisa2" value="2" [@saeHalalStentTrombozisa1]/>&nbsp;<label for="ssaeHalalStentTrombozisa2">Nem</label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="comments">Ha nem kardiális:</label></td>
            <td class="value" colspan="2"><input type="text" name="saeHalalNemKardialis" [@saeHalalNemKardialis] /></td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="radio" name="saeTipus" id="saeTipus2" value="2" [@saeTipus2]/>&nbsp;<label for="saeTipus2">Kórházi tartózkodást vagy a kórházi tartózkodás meghosszabbítását vonja maga után</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="radio" name="saeTipus" id="saeTipus3" value="3" [@saeTipus3]/>&nbsp;<label for="saeTipus3">Olyan esemény, amely az életet veszélyezteti</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="radio" name="saeTipus" id="saeTipus4" value="4" [@saeTipus4]/>&nbsp;<label for="saeTipus4">Tartós vagy jelentős cselekvőképtelenséget eredményez</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="radio" name="saeTipus" id="saeTipus5" value="5" [@saeTipus5]/>&nbsp;<label for="saeTipus5">Egyéb jelentős orvosi esemény</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="radio" name="saeTipus" id="saeTipus6" value="6" [@saeTipus6]/>&nbsp;<label for="saeTipus6">Születési rendellenesség</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>A MACCE kimenetele</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td colspan="2">
                <input type="radio" name="MACCEKimenetele" id="MACCEKimenetele1" value="1" [@MACCEKimenetele1]/>&nbsp;<label for="MACCEKimenetele1">megoldódott / befejeződött</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="radio" name="MACCEKimenetele" id="MACCEKimenetele2" value="2" [@MACCEKimenetele2]/>&nbsp;<label for="MACCEKimenetele2">javul</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="radio" name="MACCEKimenetele" id="MACCEKimenetele3" value="3" [@MACCEKimenetele3]/>&nbsp;<label for="MACCEKimenetele3">folyamatban</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="radio" name="MACCEKimenetele" id="MACCEKimenetele4" value="4" [@MACCEKimenetele4]/>&nbsp;<label for="MACCEKimenetele4">végzetes</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="radio" name="MACCEKimenetele" id="MACCEKimenetele5" value="5" [@MACCEKimenetele5]/>&nbsp;<label for="MACCEKimenetele5">ismeretlen</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="radio" name="MACCEKimenetele" id="MACCEKimenetele6" value="6" [@MACCEKimenetele6]/>&nbsp;<label for="MACCEKimenetele6">megoldódott, de következménye van</label>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="MACCEMegoldodottKovetkezmeny">Következmény leírása:</label></td>
            <td class="value"><input type="text" name="MACCEMegoldodottKovetkezmeny" [@MACCEMegoldodottKovetkezmeny] /></td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>További információk, megjegyzések, az esemény leírása</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td class="label"><label for="comments">Megjegyzések, információk:</label></td>
            <td class="value"><input type="text" style="font-family: DejaVu Sans, sans-serif;" readonly name="comments" [@comments] /></td>
        </tr>
    </tbody>
</table>
</fieldset>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Stroke</legend>
<fieldset class="nobreak">
<legend>Stroke típusa</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td>
				<input type="radio" name="strokeType" id="strokeType1" value="1" [@strokeType1]/>&nbsp;<label for="strokeType1">Iszkémiás</label>
			</td>
			<td>
                <input type="radio" name="strokeType" id="strokeType2" value="2" [@strokeType2]/>&nbsp;<label for="strokeType2">Hemorrhagiás</label>
			</td>
			<td>
				<input type="radio" name="strokeType" id="strokeType3" value="3" [@strokeType3]/>&nbsp;<label for="strokeType3">Silent</label>
			</td>
			<td>
                <input type="radio" name="strokeType" id="strokeType4" value="4" [@strokeType4]/>&nbsp;<label for="strokeType4">Nem ismert</label>
            </td>
		</tr>
	</tbody>
</table>
</fieldset>
</fieldset>
<br>
<fieldset>
<legend>Miokardiális infarktus</legend>
<fieldset class="nobreak">
<legend>MI lokalizációja</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="MILokalizacioja" id="MILokalizacioja1" value="1" [@MILokalizacioja1]/>&nbsp;<label for="MILokalizacioja1">Anterior</label>
            </td>
            <td>
                <input type="radio" name="MILokalizacioja" id="MILokalizacioja2" value="2" [@MILokalizacioja2]/>&nbsp;<label for="MILokalizacioja2">Posterior</label>
            </td>
            <td>
                <input type="radio" name="MILokalizacioja" id="MILokalizacioja3" value="3" [@MILokalizacioja3]/>&nbsp;<label for="MILokalizacioja3">Laterális</label>
            </td>
            <td>
                <input type="radio" name="MILokalizacioja" id="MILokalizacioja4" value="4" [@MILokalizacioja4]/>&nbsp;<label for="MILokalizacioja4">Inferior</label>
            </td>
            <td>
                <input type="radio" name="MILokalizacioja" id="MILokalizacioja5" value="5" [@MILokalizacioja5]/>&nbsp;<label for="MILokalizacioja5">Nem azonosítható</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Cél lézióval kapcsolatba hozható?</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="celLezioKapcsolat" id="celLezioKapcsolat1" value="1" [@celLezioKapcsolat1]/>&nbsp;<label for="celLezioKapcsolat1">Igen</label>
            </td>
            <td>
                <input type="radio" name="celLezioKapcsolat" id="celLezioKapcsolat2" value="2" [@celLezioKapcsolat2]/>&nbsp;<label for="celLezioKapcsolat2">Nem</label>
            </td>
            <td>
                <input type="radio" name="celLezioKapcsolat" id="celLezioKapcsolat3" value="3" [@celLezioKapcsolat3]/>&nbsp;<label for="celLezioKapcsolat3">Nem ismert</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Stent trombózis?</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="stentTrombozis" id="stentTrombozis1" value="1" [@stentTrombozis1]/>&nbsp;<label for="stentTrombozis1">Igen</label>
            </td>
            <td>
                <input type="radio" name="stentTrombozis" id="stentTrombozis2" value="2" [@stentTrombozis2]/>&nbsp;<label for="stentTrombozis2">Nem</label>
            </td>
            <td>
                <input type="radio" name="stentTrombozis" id="stentTrombozis3" value="3" [@stentTrombozis3]/>&nbsp;<label for="stentTrombozis3">Nem ismert</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Vérvétel</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
            <table class="fieldset-inner-table">
                <tbody>
                    <tr>
                        <td class="label"><label for="vervetelTortent">Vérvétel történt?:</label></td>
                        <td>
                            <input type="radio" name="vervetelTortent" id="vervetelTortent1" value="1" [@vervetelTortent1]/>&nbsp;<label for="vervetelTortent1">Igen</label>
                        </td>
                        <td>
                            <input type="radio" name="vervetelTortent" id="vervetelTortent2" value="2" [@vervetelTortent2]/>&nbsp;<label for="vervetelTortent2">Nem</label>
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td>
            <table class="fieldset-inner-table">
                <tbody>
                    <tr>
                        <td class="label"><label for="laborDatum">Vérvétel dátuma:</label></td>
                        <td class="value" width="15%"><input type="text" name="laborDatum" [@laborDatum] /></td>
                        <td><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td>
            <fieldset class="nobreak">
            <legend>Legmagasabb laborértékek</legend>
            <table class="fieldset-inner-table">
                <tbody>
                    <tr>
                        <td class="label"><label for="laborCk">CK:</label></td>
                        <td class="value" width="15%"><input type="text" name="laborCk" [@laborCk] /></td>
                        <td>IU/L</td>
                        <td><input type="checkbox" name="laborCkna" [@laborCkna] />&nbsp;<label style="font-size: 7pt;" for="laborCkna">Nem vizsgált</label></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="laborCkmb">CKMB:</label></td>
                        <td class="value" width="15%"><input type="text" name="laborCkmb" [@laborCkmb] /></td>
                        <td>UI/L</td>
                        <td><input type="checkbox" name="laborCkmbna" [@laborCkmbna] />&nbsp;<label style="font-size: 7pt;" for="laborCkmbna">Nem vizsgált</label></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="laborTroponinI">Troponin I:</label></td>
                        <td class="value" width="15%"><input type="text" name="laborTroponinI" [@laborTroponinI] /></td>
                        <td>ng/ml</td>
                        <td><input type="checkbox" name="laborTroponinIna" [@laborTroponinIna] />&nbsp;<label style="font-size: 7pt;" for="laborTroponinIna">Nem vizsgált</label></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="laborTroponinT">Troponin T:</label></td>
                        <td colspan="3">
                            <table class="fieldset-inner-table" id="laborTroponinT">
                            <tbody>
                                <tr>
                                <td>
                                    <input type="radio" name="laborTroponinT" id="laborTroponinT1" value="1" [@laborTroponinT1]/>&nbsp;<label for="laborTroponinT1">Pozitív</label>
                                </td>
                                <td>
                                    <input type="radio" name="laborTroponinT" id="laborTroponinT2" value="2" [@laborTroponinT2]/>&nbsp;<label for="laborTroponinT2">Negatív</label>
                                </td>
                                <td>
                                    <input type="radio" name="laborTroponinT" id="laborTroponinT3" value="3" [@laborTroponinT3]/>&nbsp;<label for="laborTroponinT3">Nem történt</label>
                                </td>
                                </tr>
                            </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            </fieldset>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Szívgyógyszerek</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
            <table class="fieldset-inner-table">
                <tbody>
                    <tr>
                        <td width="70%"><label for="szivgyogyszer">Szed-e a beteg valamilyen szívgyógyszert jelenleg?:</label></td>
                        <td>
                            <input type="radio" name="szivgyogyszer" id="szivgyogyszer1" value="1" [@szivgyogyszer1]/>&nbsp;<label for="szivgyogyszer1">Igen</label>
                        </td>
                        <td>
                            <input type="radio" name="szivgyogyszer" id="szivgyogyszer2" value="2" [@szivgyogyszer2]/>&nbsp;<label for="szivgyogyszer2">Nem</label>
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td>Kérjük, jelöljön minden szívgyógyszert, amit a beteg aktuálisan szed!</td>
        </tr>
        <tr>
            <td>
                <table class="fieldset-inner-table">
                <tbody>
                    <tr>
                        <td colspan="3">
                            <input type="checkbox" name="aszpirin"  [@aszpirin] />&nbsp;<label for="aszpirin">Aszpirin</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="checkbox" name="clopidogrel"  [@clopidogrel] />&nbsp;<label for="clopidogrel">Clopidogrel</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="checkbox" name="egyebtrombocitaaggregaciogatlo"  [@egyebtrombocitaaggregaciogatlo] />&nbsp;<label for="egyebtrombocitaaggregaciogatlo">Egyéb trombocita aggregációt gátló gyógyszer</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="checkbox" name="acegatlo"  [@acegatlo] />&nbsp;<label for="acegatlo">ACE gátló</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="checkbox" name="sztatin"  [@sztatin] />&nbsp;<label for="sztatin">Sztatin</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="checkbox" name="antikoagulans"  [@antikoagulans] />&nbsp;<label for="antikoagulans">Antikoaguláns</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="checkbox" name="betablokkolo"  [@betablokkolo] />&nbsp;<label for="betablokkolo">Béta-blokkoló</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="checkbox" name="angiotensiniireceptorblokkolo"  [@angiotensiniireceptorblokkolo] />&nbsp;<label for="angiotensiniireceptorblokkolo">Angiotensin II receptor blokkoló</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="checkbox" name="nemsztatinlipidcsokkento"  [@nemsztatinlipidcsokkento] />&nbsp;<label for="nemsztatinlipidcsokkento">Nem-sztatin lipidcsökkentő szer</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="checkbox" name="egyebszivgyogyszer"  [@egyebszivgyogyszer] />&nbsp;<label for="egyebszivgyogyszer">Egyéb</label>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><label for="egyebSzivGyogyszerReszletek">Egyéb szívgyógyszer részletezése:</label></td>
                        <td class="value" colspan="2"><input type="text" name="egyebSzivGyogyszerReszletek" [@egyebSzivGyogyszerReszletek] /></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</fieldset>
</fieldset>
<br>
<fieldset>
<legend>Ismételt revaszkularizáció</legend>
<fieldset class="nobreak">
<legend>Iszkémiás státusz</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="iszkemiasStatus" id="iszkemiasStatus1" value="1" [@iszkemiasStatus1]/>&nbsp;<label for="iszkemiasStatus1">Nincs angina</label>
            </td>
            <td>
                <input type="radio" name="iszkemiasStatus" id="iszkemiasStatus2" value="2" [@iszkemiasStatus2]/>&nbsp;<label for="iszkemiasStatus2">Stabil angina</label>
            </td>
            <td>
                <input type="radio" name="iszkemiasStatus" id="iszkemiasStatus3" value="3" [@iszkemiasStatus3]/>&nbsp;<label for="iszkemiasStatus3">Instabil angina</label>
            </td>
            <td>
                <input type="radio" name="iszkemiasStatus" id="iszkemiasStatus4" value="4" [@iszkemiasStatus4]/>&nbsp;<label for="MiszkemiasStatus4">Néma Iszkémia</label>
            </td>
            <td>
                <input type="radio" name="iszkemiasStatus" id="iszkemiasStatus5" value="5" [@iszkemiasStatus5]/>&nbsp;<label for="iszkemiasStatus5">Nem ismert</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>CCS</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="stabilAngina" id="stabilAngina1" value="1" [@stabilAngina1]/>&nbsp;<label for="stabilAngina1">I</label>
            </td>
            <td>
                <input type="radio" name="stabilAngina" id="stabilAngina2" value="2" [@stabilAngina2]/>&nbsp;<label for="stabilAngina2">II</label>
            </td>
            <td>
                <input type="radio" name="stabilAngina" id="stabilAngina3" value="3" [@stabilAngina3]/>&nbsp;<label for="stabilAngina3">III</label>
            </td>
            <td>
                <input type="radio" name="stabilAngina" id="stabilAngina4" value="4" [@stabilAngina4]/>&nbsp;<label for="stabilAngina4">IV</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Braunwald osztályozás</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="instabilAngina" id="instabilAngina1" value="1" [@instabilAngina1]/>&nbsp;<label for="instabilAngina1">IA</label>
            </td>
            <td>
                <input type="radio" name="instabilAngina" id="instabilAngina2" value="2" [@instabilAngina2]/>&nbsp;<label for="instabilAngina2">IB</label>
            </td>
            <td>
                <input type="radio" name="instabilAngina" id="instabilAngina3" value="3" [@instabilAngina3]/>&nbsp;<label for="instabilAngina3">IC</label>
            </td>
        </tr>
        <tr>
            <td>
                <input type="radio" name="instabilAngina" id="instabilAngina4" value="4" [@instabilAngina4]/>&nbsp;<label for="instabilAngina4">IIA</label>
            </td>
            <td>
                <input type="radio" name="instabilAngina" id="instabilAngina5" value="5" [@instabilAngina5]/>&nbsp;<label for="instabilAngina5">IIB</label>
            </td>
            <td>
                <input type="radio" name="instabilAngina" id="instabilAngina6" value="6" [@instabilAngina6]/>&nbsp;<label for="instabilAngina6">IIC</label>
            </td>
        </tr>
        <tr>
            <td>
                <input type="radio" name="instabilAngina" id="instabilAngina7" value="7" [@instabilAngina7]/>&nbsp;<label for="instabilAngina7">IIIA</label>
            </td>
            <td>
                <input type="radio" name="instabilAngina" id="instabilAngina8" value="8" [@instabilAngina8]/>&nbsp;<label for="instabilAngina8">IIIB</label>
            </td>
            <td>
                <input type="radio" name="instabilAngina" id="instabilAngina9" value="9" [@instabilAngina9]/>&nbsp;<label for="instabilAngina9">IIIC</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Revaszkularizáció típusa</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="revaszkularizacioTipus" id="revaszkularizacioTipus1" value="1" [@revaszkularizacioTipus1]/>&nbsp;<label for="revaszkularizacioTipus1">CABG</label>
            </td>
            <td>
                <input type="radio" name="revaszkularizacioTipus" id="revaszkularizacioTipus2" value="2" [@revaszkularizacioTipus2]/>&nbsp;<label for="revaszkularizacioTipus2">PCI</label>
            </td>
            <td>
                <input type="radio" name="revaszkularizacioTipus" id="revaszkularizacioTipus3" value="3" [@revaszkularizacioTipus3]/>&nbsp;<label for="revaszkularizacioTipus3">Nem ismert</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Sürgős CABG</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="surgosCABG" id="surgosCABG1" value="1" [@surgosCABG1]/>&nbsp;<label for="surgosCABG1">Igen</label>
            </td>
            <td>
                <input type="radio" name="surgosCABG" id="surgosCABG2" value="2" [@surgosCABG2]/>&nbsp;<label for="surgosCABG2">Nem</label>
            </td>
            <td>
                <input type="radio" name="surgosCABG" id="surgosCABG3" value="3" [@surgosCABG3]/>&nbsp;<label for="surgosCABG3">Nem ismert</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Oka</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td>
                <input type="radio" name="surgosCABGOka" id="surgosCABGOka1" value="1" [@surgosCABGOka1]/>&nbsp;<label for="surgosCABGOka1">Ér disszekció</label>
            </td>
            <td>
                <input type="radio" name="surgosCABGOka" id="surgosCABGOka2" value="2" [@surgosCABGOka2]/>&nbsp;<label for="surgosCABGOka2">Ér elzáródás</label>
            </td>
            <td>
                <input type="radio" name="surgosCABGOka" id="surgosCABGOka3" value="3" [@surgosCABGOka3]/>&nbsp;<label for="surgosCABGOka3">Kezelés sikertelensége</label>
            </td>
            <td>
                <input type="radio" name="surgosCABGOka" id="surgosCABGOka4" value="4" [@surgosCABGOka4]/>&nbsp;<label for="surgosCABGOka4">Egyéb</label>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<br>
<fieldset class="nobreak">
<legend>Ismételt revaszkularizáció az alábbiakon alapul:</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td colspan="3">
                <input type="checkbox" name="anginasStatusz"  [@anginasStatusz] />&nbsp;<label for="anginasStatusz">Anginás státusz</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="checkbox" name="pozitivStressz"  [@pozitivStressz] />&nbsp;<label for="pozitivStressz">pozitív stressz teszt vagy azzal egyenértékű</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="checkbox" name="EKGvaltozas"  [@EKGvaltozas] />&nbsp;<label for="EKGvaltozas">EKG változás</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="checkbox" name="ismeteltAngiografia"  [@ismeteltAngiografia] />&nbsp;<label for="ismeteltAngiografia">Ismételt angiográfia</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="checkbox" name="egyebRevaszkAlap"  [@egyebRevaszkAlap] />&nbsp;<label for="egyebRevaszkAlap">Egyéb</label>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="egyebRevaszkAlapReszletek">Egyéb részletezése:</label></td>
            <td class="value" colspan="2"><input type="text" name="egyebRevaszkAlapReszletek" [@egyebRevaszkAlapReszletek] /></td>
        </tr>
    </tbody>
</table>
</fieldset>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td width="35%"><label for="celLezioErintettseg">Cél lézió érintettsége:</label></td>
            <td>
                <input type="radio" name="celLezioErintettseg" id="celLezioErintettseg1" value="1" [@celLezioErintettseg1]/>&nbsp;<label for="celLezioErintettseg1">Igen</label>
            </td>
            <td>
                <input type="radio" name="celLezioErintettseg" id="celLezioErintettseg2" value="2" [@celLezioErintettseg2]/>&nbsp;<label for="celLezioErintettseg2">Nem</label>
            </td>
            <td>
                <input type="radio" name="celLezioErintettseg" id="celLezioErintettseg3" value="3" [@celLezioErintettseg3]/>&nbsp;<label for="celLezioErintettseg3">Nem ismert</label>
            </td>
        </tr>
    </tbody>
</table>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td width="35%"><label for="oldalagiStenozisAtmero">% átmérő oldalági sztenózis:</label></td>
            <td class="value" width="15%"><input type="text" name="oldalagiStenozisAtmero" [@oldalagiStenozisAtmero] /></td>
            <td>%</td>
            <td><input type="checkbox" name="oldalagiStenozisAtmerona" [@oldalagiStenozisAtmerona] />&nbsp;<label style="font-size: 7pt;" for="oldalagiStenozisAtmerokna">Nem vizsgált</label></td>
        </tr>
        <tr>
            <td width="35%"><label for="foagiStenozisAtmero">% átmérő főági sztenózis</label></td>
            <td class="value" width="15%"><input type="text" name="foagiStenozisAtmero" [@foagiStenozisAtmero] /></td>
            <td>%</td>
            <td><input type="checkbox" name="foagiStenozisAtmerona" [@foagiStenozisAtmerona] />&nbsp;<label style="font-size: 7pt;" for="foagiStenozisAtmerona">Nem vizsgált</label></td>
        </tr>
    </tbody>
</table>
</fieldset>
</div>
