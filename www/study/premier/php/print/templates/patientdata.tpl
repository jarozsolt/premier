<html><head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<link rel="stylesheet" type="text/css" href="print/templates/css/common.css" />
<title>Páciens adatok</title>
</head>
<body>
<div class="header">
<table style="width: 100%; border: solid 1px #888; outline: solid 1px #EEE; padding: 10px;">
    <tbody>
        <tr>
            <td width="20%">Páciens:</td>
            <td width="32%">[@patientid]</td>
            <td width="18%">Vizsgálat:</td>
            <td>[@studyname]</td>
        </tr>
        <tr>
            <td>Vizsgálóhely:</td>
            <td>[@siteid]</td>
            <td>Dátum:</td>
            <td>[@formtime]</td>
        </tr>
    </tbody>
</table>
</div>
<h2 style="text-align: center; width: 100%;">Páciens adatok</h2>
<div>
[@recordlist]
</div>
</body></html>