<div>
<h4 style="text-align: center; width: 100%; page-break-before: always;">[@title]</h4>
<fieldset>
<legend>Egyéb kezelések - [@tradenameValue]</legend>
<table class="fieldset-inner-table">
    <tbody>
    	<tr>
    		<td class="label"><label for="tradename">Kezelés / gyógyszer megnevezése:</label></td>
			<td class="value" colspan="4"><input type="text" name="tradename" [@tradename] /></td>
		</tr>
		<tr>
    		<td class="label"><label for="treatmentType">Kezelés típusa:</label></td>
    		<td class="value" colspan="4">
    			<select charset="UTF-8">
    				<option value="1" [@treatmentType1]>gyógyszer</option>		
					<option value="2" [@treatmentType2]>műtét</option>
					<option value="3" [@treatmentType3]>egyéb eljárás (pl. gyógytorna)</option>
					<option value="4" [@treatmentType4]>egyéb (pl. homeopátia, fitoterápia, stb.)</option>
				</select>
			</td>
		</tr>
		<tr>
    		<td class="label"><label for="otherTreatmentTypeDescription">Egyéb kezelés leírása:</label></td>
			<td class="value" colspan="4"><input type="text" name="otherTreatmentTypeDescription" [@otherTreatmentTypeDescription] /></td>
		</tr>
		<tr>
            <td colspan="5">
        		<fieldset class="nobreak">
                <legend>A készítmény</legend>
                <table class="fieldset-inner-table">
                    <tbody>
                        <tr>
                            <td>
                                <input type="radio" name="medicationType" id="medicationType1" value="1" [@medicationType1]/>&nbsp;<label for="medicationType1">Egy hatóanyagot tartalmaz</label>
                            </td>
                            <td>
                                <input type="radio" name="medicationType" id="medicationType2" value="2" [@medicationType2]/>&nbsp;<label for="medicationType2">Több hatóanyagot tartalmaz</label>
                            </td>
                            <td>
                                <input type="radio" name="medicationType" id="medicationType3" value="3" [@medicationType3]/>&nbsp;<label for="medicationType3">n.a.</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td class="label"><label for="dose">Teljes dózis:</label></td>
            <td class="value"><input type="text" name="dosevalue" [@dosevalue] /></td>
            <td class="value">
                <select>
                    <option value="1" [@doseunit1]>µg</option>
                    <option value="2" [@doseunit2]>µg/l</option>
                    <option value="3" [@doseunit3]>µl</option>
                    <option value="4" [@doseunit4]>µl/ml</option>
                    <option value="5" [@doseunit5]>ampulla</option>
                    <option value="6" [@doseunit6]>csepp</option>
                    <option value="7" [@doseunit7]>csomag</option>
                    <option value="8" [@doseunit8]>ezer IU</option>
                    <option value="9" [@doseunit9]>fiola</option>
                    <option value="10" [@doseunit10]>g</option>
                    <option value="11" [@doseunit11]>g/kg</option>
                    <option value="12" [@doseunit12]>g/l</option>
                    <option value="13" [@doseunit13]>g/ml</option>
                    <option value="14" [@doseunit14]>g/mol</option>
                    <option value="15" [@doseunit15]>g/nap</option>
                    <option value="16" [@doseunit16]>gtt</option>
                    <option value="17" [@doseunit17]>IU</option>
                    <option value="18" [@doseunit18]>IU/kg</option>
                    <option value="19" [@doseunit19]>IU/l</option>
                    <option value="20" [@doseunit20]>kg</option>
                    <option value="21" [@doseunit21]>l</option>
                    <option value="22" [@doseunit22]>l/h</option>
                    <option value="23" [@doseunit23]>l/min</option>
                    <option value="24" [@doseunit24]>l/sec</option>
                    <option value="25" [@doseunit25]>mg</option>
                    <option value="26" [@doseunit26]>mg/kg</option>
                    <option value="27" [@doseunit27]>mg/l</option>
                    <option value="28" [@doseunit28]>mg/ml</option>
                    <option value="29" [@doseunit29]>mg/mmol</option>
                    <option value="30" [@doseunit30]>millió IU</option>
                    <option value="31" [@doseunit31]>ml</option>
                    <option value="32" [@doseunit32]>ml/h</option>
                    <option value="33" [@doseunit33]>ml/min</option>
                    <option value="34" [@doseunit34]>mmol</option>
                    <option value="35" [@doseunit35]>mmol/kg</option>
                    <option value="36" [@doseunit36]>mol</option>
                    <option value="37" [@doseunit37]>mol/mg</option>
                    <option value="38" [@doseunit38]>mol/ml</option>
                    <option value="39" [@doseunit39]>tabletta</option>
                    <option value="40" [@doseunit40]>U/l</option>
                    <option value="41" [@doseunit41]>U/ml</option>
                    <option value="42" [@doseunit42]>egyéb</option>
                    <option value="43" [@doseunit43]>n.a.</option>
                </select>
            </td>
            <td colspan="2"><input type="checkbox" name="dosena" [@dosena] />&nbsp;<label style="font-size: 7pt;" for="dosena">Nem&nbsp;alkalm.</label></td>
        </tr>
        [@doselist]
        <tr>
            <td class="label"><label for="frequency">Gyakoriság:</label></td>
            <td class="value" colspan="4">
                <select>
                    <option value="1" [@frequency1]>naponta</option>        
                    <option value="2" [@frequency2]>hetente</option>
                    <option value="3" [@frequency3]>havonta</option>
                    <option value="4" [@frequency4]>szükség szerint</option>
                    <option value="5" [@frequency5]>egyéb</option>
                    <option value="6" [@frequency6]>nem alkalmazható</option>
                </select>
            </td>
        </tr>
		<tr>
    		<td class="label"><label for="indication">Indikáció:</label></td>
			<td class="value" colspan="4"><input type="text" name="indication" [@indication] /></td>
		</tr>
		<tr>
    		<td class="label"><label for="applicationType">Alkalmazás módja:</label></td>
			<td class="value" colspan="4">
				<select>
    				<option value="1" [@applicationType1]>szájon át</option>		
					<option value="2" [@applicationType2]>intravénás</option>
					<option value="3" [@applicationType3]>intramuszkuláris</option>
					<option value="4" [@applicationType4]>szubkután</option>
					<option value="5" [@applicationType5]>szublingvális</option>
					<option value="6" [@applicationType6]>transdermális</option>		
					<option value="7" [@applicationType7]>inhalációs</option>
					<option value="8" [@applicationType8]>nazális</option>
					<option value="9" [@applicationType9]>lokális</option>
					<option value="10" [@applicationType10]>egyéb</option>
					<option value="11" [@applicationType11]>nem alkalmazható</option>
				</select>
			</td>
		</tr>
		<tr>
    		<td class="label"><label for="otherApplicationTypeDescription">Egyéb alkalmazási mód részletei:</label></td>
			<td class="value" colspan="4"><input type="text" name="otherApplicationTypeDescription" [@otherApplicationTypeDescription] /></td>
		</tr>
		<tr>
    		<td class="label"><label for="startDate">Kezelés kezdete:</label></td>
			<td class="value" width="15%"><input type="text" name="startDate" [@startDate] /></td>
			<td colspan="3"><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
		</tr>
		<tr>
    		<td class="label"><label for="endDate">Kezelés vége:</label></td>
			<td class="value" width="15%"><input type="text" name="endDate" [@endDate] /></td>
			<td><img src="print/templates/images/datepicker.png" height="22" width="22"></td>
			<td colspan="2"><input type="checkbox" name="endDateog" [@endDateog] />&nbsp;<label style="font-size: 7pt;" for="endDateog">folyamatban</label></td>
		</tr>
		<tr>
    		<td class="label"><label for="treatmentReason">Kezelés oka:</label></td>
			<td class="value" colspan="4">
				<select>
    				<option value="1" [@treatmentReason1]>Kórelőzmény, egyéb társbetegség</option>		
					<option value="2" [@treatmentReason2]>Nemkívánatos esemény</option>
					<option value="3" [@treatmentReason3]>Egyéb (pl. prevenció)</option>
				</select>
			</td>
		</tr>
		<tr>
    		<td class="label"><label for="otherTreatmentReasonDescription">Egyéb kezelési ok leírása:</label></td>
			<td class="value" colspan="4"><input type="text" name="otherTreatmentReasonDescription" [@otherTreatmentReasonDescription] /></td>
		</tr>
		<tr>
            <td class="label"><label for="comments">Megjegyzések, információk:</label></td>
            <td class="value" colspan="4"><input type="text" name="comments" [@comments] /></td>
        </tr>
	</tbody>
</table>
</fieldset>
</div>
