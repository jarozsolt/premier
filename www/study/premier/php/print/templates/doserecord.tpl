<tr>
<td colspan="5">
<fieldset class="nobreak">
<legend>Hatóanyag</legend>
<table class="fieldset-inner-table">
    <tbody>
        <tr>
            <td class="label"><label for="ingredientname">Hatóanyag neve:</label></td>
            <td class="value" colspan="5"><input type="text" name="ingredientname" [@ingredientname] /></td>
        </tr>
        <tr>
            <td class="label"><label for="dose">Teljes dózis:</label></td>
            <td class="value"><input type="text" name="dosevalue" [@dosevalue] /></td>
            <td width="5%">
                <select>
                    <option value="1" [@doseunit1]>µg</option>
                    <option value="2" [@doseunit2]>µg/l</option>
                    <option value="3" [@doseunit3]>µl</option>
                    <option value="4" [@doseunit4]>µl/ml</option>
                    <option value="5" [@doseunit5]>ampulla</option>
                    <option value="6" [@doseunit6]>csepp</option>
                    <option value="7" [@doseunit7]>csomag</option>
                    <option value="8" [@doseunit8]>ezer IU</option>
                    <option value="9" [@doseunit9]>fiola</option>
                    <option value="10" [@doseunit10]>g</option>
                    <option value="11" [@doseunit11]>g/kg</option>
                    <option value="12" [@doseunit12]>g/l</option>
                    <option value="13" [@doseunit13]>g/ml</option>
                    <option value="14" [@doseunit14]>g/mol</option>
                    <option value="15" [@doseunit15]>g/nap</option>
                    <option value="16" [@doseunit16]>gtt</option>
                    <option value="17" [@doseunit17]>IU</option>
                    <option value="18" [@doseunit18]>IU/kg</option>
                    <option value="19" [@doseunit19]>IU/l</option>
                    <option value="20" [@doseunit20]>kg</option>
                    <option value="21" [@doseunit21]>l</option>
                    <option value="22" [@doseunit22]>l/h</option>
                    <option value="23" [@doseunit23]>l/min</option>
                    <option value="24" [@doseunit24]>l/sec</option>
                    <option value="25" [@doseunit25]>mg</option>
                    <option value="26" [@doseunit26]>mg/kg</option>
                    <option value="27" [@doseunit27]>mg/l</option>
                    <option value="28" [@doseunit28]>mg/ml</option>
                    <option value="29" [@doseunit29]>mg/mmol</option>
                    <option value="30" [@doseunit30]>millió IU</option>
                    <option value="31" [@doseunit31]>ml</option>
                    <option value="32" [@doseunit32]>ml/h</option>
                    <option value="33" [@doseunit33]>ml/min</option>
                    <option value="34" [@doseunit34]>mmol</option>
                    <option value="35" [@doseunit35]>mmol/kg</option>
                    <option value="36" [@doseunit36]>mol</option>
                    <option value="37" [@doseunit37]>mol/mg</option>
                    <option value="38" [@doseunit38]>mol/ml</option>
                    <option value="39" [@doseunit39]>tabletta</option>
                    <option value="40" [@doseunit40]>U/l</option>
                    <option value="41" [@doseunit41]>U/ml</option>
                    <option value="42" [@doseunit42]>egyéb</option>
                    <option value="43" [@doseunit43]>n.a.</option>
                </select>
            </td>
            <td colspan="2"><input type="checkbox" name="dosena" [@dosena] />&nbsp;<label style="font-size: 7pt;" for="dosena">Nem&nbsp;alkalm.</label></td>
        </tr>
    </tbody>
</table>
</fieldset>
</td>
</tr>