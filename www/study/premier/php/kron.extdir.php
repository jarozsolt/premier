<?php
// Calculate the app path
$app_path = str_replace("\\", "/", realpath(dirname(__FILE__)));

// Calculate the study path
$study_path = join(array_slice(explode( "/" ,$app_path), 0, -1), "/");

// Calculate the study directory
$study_dir = basename ($study_path ,"/");

// Calculate the subdomain path
$subdomain_path = join(array_slice(explode( "/" ,$study_path), 0, -1), "/");

// Calculate the public (www) path
$public_path = join(array_slice(explode( "/" ,$subdomain_path), 0, -1), "/");

// Calculate the base path
$base_path = join(array_slice(explode( "/" ,$public_path), 0, -1), "/");
// Load the main configuration file
require_once($base_path . '/' . $study_dir . '/configs/app.conf');
require('extdirect.class.php');
require('usermanagement.class.php');
require('documents.class.php');
require('contacts.class.php');
require('addressees.class.php');
require('messages.class.php');
//require('formfields.class.php');
require('forms.class.php');
require('queries.class.php');
require('patients.class.php');
require('visits.class.php');
require('reports.class.php');
ExtDirect::$namespace = 'Kron';
ExtDirect::$descriptor = 'Kron.APIDesc';
ExtDirect::$default_api_output = 'javascript';

ExtDirect::$authorization_function='UserManagement::ExtDirect_Authorization_Callback';
ExtDirect::provide(array('UserManagement','Documents','Contacts', 'Addressees', 'Messages', /*'FormFields',*/ 'Queries', 'Patients', 'Visits', 'Forms', 'Reports'));
?>