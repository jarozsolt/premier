<?php
/**
 *---------------------------------------------------------------
 *reports.class.php
 *
 * This file contains the reports class functions
 *
 * @package Kron
 * @subpackage reports
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');
require_once('patients.class.php');
require_once('localemanagement.class.php');

class Reports
{
  	/**
     *  This function returns the name of the queries view
     */
	public function getQueriesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'queries';
	}
	
	
	 /**
     *  This function returns the name of the sites view
     */
	public function getSitesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'sites';
	}
	
	/**
     *  This function returns the name of the countries view
     */
	public function getPatientsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'patients';
	}

	/**
     *  This function returns the name of the messages view
     */
	public function getFormsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'forms';
	}
	 /**
     *  This function returns the name of the messages view
     */
	public function getFormFieldsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields';
	}

	
	/** This function returns the numbers of patients statuses by site 
     *  @remotable
	 */
	public function getPatientsData()
    {
    	$logger = Logger::getInstance();
		$userlanguage = isset($_SESSION['auth_userlanguage']) ? $_SESSION['auth_userlanguage'] : FrameworkConfig::default_language;
		LocaleManagement::initLanguage($userlanguage);
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		//$logger->logInfo('getPatientsData called');
		$success = false;
		$a = array();
		try
		{
			$db = new Database();
			$sql = "SELECT p.siteid, s.sitename, COUNT(IF(p.status = ? , 1, NULL)) as '"._("Folyamatban")."', COUNT(IF(p.status = ? , 1, NULL)) as '"._("Kiesett")."', COUNT(IF(p.status = ? , 1, NULL)) as '"._("Téves szűrés");
			$sql = $sql . "', COUNT(IF(p.status = ? , 1, NULL)) as '"._("Befejezett")."' FROM " . $this->getPatientsViewName() . " p ";
			$sql = $sql . " INNER JOIN " . $this->getSitesViewName() . " s ON p.siteid = s.sid GROUP BY siteid ";
			$db->query($sql, FrameworkConfig::PATIENTSTATUS_ONGOING, FrameworkConfig::PATIENTSTATUS_DROPOUT, FrameworkConfig::PATIENTSTATUS_FAILURE, FrameworkConfig::PATIENTSTATUS_COMPLETED);

			$rowcount=	$db->row_count();	
			if( $rowcount > 0) {
				for ($i=1; $i<=$rowcount; $i++) {
					$row = $db->fetch("assoc");
					if ($row !== null ) {
						array_push($a,$row);
					}				
				}					
				$success = true;	
			}
		
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getPatientsData, Exception: $e");
		}
		//$logger->logInfo( Array('success' => $success, 'data' => $a));
		return Array('success' => $success, 'data' => $a);
		
	}
	
	/** This function returns the numbers of patients by site  per week nr
     *  @remotable
	 */
	public function getPatientsTrend()
    {
    	$logger = Logger::getInstance();
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		//$logger->logInfo('getPatientsTrend called');
		$success = false;
		$sitenames= array();
		$trendData = array();
		$a = array();
		try
		{
			$db = new Database();
			$sql = "SELECT p.siteid, s.sitename, YEAR(enrollmentdate) as year, WEEK(enrollmentdate) as week, count(*) as weektotal FROM " . $this->getPatientsViewName();
			$sql = $sql." p INNER JOIN " . $this->getSitesViewName() . " s ON p.siteid = s.sid GROUP BY siteid, YEAR(enrollmentdate)+WEEK(enrollmentdate) ";
			$db->query($sql);
		
			$rowcount=	$db->row_count();	
			if( $rowcount > 0) {
				for ($i=1; $i<=$rowcount; $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) { 
						$index = $row['year'] . " - " . $row['week'];
						if (!array_key_exists($index, $a)) {
							$a[$index] = array();
						}
						$a[$index][$row['sitename']] = $row['weektotal'];
					}				
				}			
				ksort($a);		
				//$logger->logInfo($a);
				$cumValue= array();
				$sitenames= array();
				$trendData = array();
				
				// összeszedem a siteneveket
				foreach ($a as $key => $value) {
					foreach ($value as $key2 => $value2) {
						if (!in_array($key2, $sitenames)) {
							array_push($sitenames,$key2);

						}							
					}
				}
				
				// előállítom a válasz formátumot
				foreach ($a as $key => $value) {
					$a[$key]['week']= $key;
					foreach ($sitenames as $site) {
						if (!array_key_exists($site, $cumValue)) {
							$cumValue[$site]=0;
						}	
						if (!array_key_exists($site, $a[$key])) {
							$a[$key][$site] = 0;
						}	
						$cumValue[$site]=$cumValue[$site] +$a[$key][$site];
						
						$a[$key][$site]=$cumValue[$site];
						
					}
					array_push($trendData,$a[$key]);
				}
				
				
				$success = true;	
			}
		
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getPatientsTrend, Exception: $e");
		}
		//$logger->logInfo( Array('success' => $success, 'data' => $trendData, 'sitenames'=> $sitenames));
		return Array('success' => $success, 'data' => $trendData, 'sitenames'=> $sitenames);
		
	}
	
	/** This function returns the patients data
     *  @remotable
	 */
	public function getVisitData()
    {
    	$logger = Logger::getInstance();
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		//$logger->logInfo('getVisitData called');		
		$success = false;		
		// get form statuses ( all form types )
		$Patients = new Patients();
		$params = new stdClass;
		$params->allForms =  true;
		$patientsResult = $Patients->getPatients($params); 
		$patientsData = $patientsResult['data'];
		$patientFields = array();
		//$logger->logInfo( $patientsData);		

		$formNames = array( 'Visit1', 'Visit2', 'Visit3', 'Visit4', 'Visit5', 'Visit6', 'Visit7', 'UnscheduledVisit', 'AdverseEvent', 'Medication', 'MedicalHistory');
		$formStatuses = array('O','Q','C','A','M','R');
		try
		{
			$db = new Database();
			
			foreach ($patientsData as $value) {
				// nulla értékeket beállítok mindenre, aztán majd ahol van , azzal felülírom a query alapján
				$fields= array();
				foreach($formNames as $form){
					foreach($formStatuses as $fstatus){
						$fields[$form][$fstatus] = 0;
					}
				}
				
				
				// collect form fields number 
				$pid = (string)$value['patientid'];
				$sql = "SELECT f.formtype, i.status, COUNT(*) as formtotal FROM " . $this->getFormsViewName() . " f INNER JOIN " . $this->getFormFieldsViewName() . " i ON f.patientid = ? AND f.instanceid = i.form_instanceid AND f.istmp = 0  AND i.istmp = 0 GROUP BY formtype, status ";
				$db->query($sql, $pid);
				
				if( $db->row_count() > 0) {

					for ($i=1; $i<=$db->row_count(); $i++) {
						$row = $db->fetch("assoc");
						if ($row != null) {
							if (!ISSET($fields[$row['formtype']])) {
								$fields[$row['formtype']] = array( /*'visit' => $row['formtype'] */);
							}
							$fields[$row['formtype']][$row['status']] = intval( $row['formtotal'], 10);
						}		
					}
					$patientFields[$pid]= $fields;
					$success = true;		
				}
			}
				
			$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getVisitData, Exception: $e");
		}
	//	$result = array();
		//$logger->logInfo($patientFields);
		foreach ($patientsData as $key => $value) {
				$pid = $value['patientid'];
				$patientsData[$key]['fields'] = $patientFields[$pid];
		}
	//	$result['visits']=$patientsData;
	/*	$result['fields']= $patientFields;
		$result['fields']= array();
		foreach ($patientFields as $key => $value) {
			$rec= array();
			foreach ($value as $key2 => $value2) {
				array_push($rec,$value2);
			}
			$result['fields'][$key] = $rec;
		}*/
		//$logger->logInfo( Array('success' => $success, 'data' =>$patientsData));
		return Array('success' => $success, 'data' =>$patientsData);
	}

	
	
}
?>