<?php
/**
 *---------------------------------------------------------------
 * logout.php
 *
 * Logout page.
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
// Calculate the php path
$php_path = str_replace("\\", "/", realpath(dirname(__FILE__)));

// Calculate the study path
$study_path = join(array_slice(explode( "/" , $php_path), 0, -1), "/");

// Calculate the study directory
$study_dir = basename ($study_path ,"/");

// Calculate the subdomain path
$subdomain_path = join(array_slice(explode( "/" ,$study_path), 0, -1), "/");

// Calculate the public (www) path
$public_path = join(array_slice(explode( "/" ,$subdomain_path), 0, -1), "/");

// Calculate the base path
$base_path = join(array_slice(explode( "/" , $public_path), 0, -1), "/");

// Load the main configuration file
// Warning the 'configs' config_folder hardcoded here !!!
require_once($base_path . '/' . $study_dir . '/configs/app.conf');

$language = FrameWorkConfig::default_language;

if (isset($_SESSION)) 
{
	if (isset ($_SESSION['auth_userlanguage']) && $_SESSION['auth_userlanguage'] != '')
	{
		$language = $_SESSION['auth_userlanguage'];
	}
	$language_short = substr($language, 0, 2);
	
	// clean-up session variables
	unset($_SESSION['auth_userloggedoutsuccessful'] );
	unset($_SESSION['auth_userlanguage'] );
}

// Build the logout page
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">';
echo '<html>';
echo '<head>';
echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
echo '<title id="page-title">'._("KRON - Klinikai Adatkezelő Rendszer").'</title>';
echo '<link rel="stylesheet" type="text/css" href="'.CSS_PATH_REL.'logout_1.css"/>';
echo '<link rel="shortcut icon" type="image/x-icon" href="'.IMAGE_PATH_REL.'favicon.ico"/>';
echo '<link rel="gettext" type="application/x-po" href="'.LOCALES_PATH_REL.$language.'.utf8'.'/LC_MESSAGES/server.po"/>';
echo '</head>';
echo '<body style="scroll">';
echo '<img class="background" src="'.IMAGE_PATH_REL.'background.png">';
echo '<div class="logoutmsg"><table>';
echo '<tr><td>&nbsp;<img src="'.IMAGE_PATH_REL.'tick_32.png">&nbsp;</td><td><h3 style="padding: 0px 16px;">'._("Sikeres kijelentkezés").'</h3></td></tr>';
echo '<tr><td>&nbsp;</td><td style="padding: 0px 16px;">'._("Sikeresen kijelentkezett a KRON - Klinikai Adatkezelő Rendszerből.").'</td></tr>';
echo '<tr><td>&nbsp;</td><td style="padding: 48px 16px;"><div style="align:middle;"><a href="index.php" class="button">'._("Kattintson ide az újra bejelentkezéshez").'</a></div></td></tr>';
echo '</table></div>';
echo '<div style="position: absolute; bottom: 10px; right: 10px;"><img src="'.IMAGE_PATH_REL.'powered_by_kron.png"></div>';
echo '</body>';
echo '</html>';
?>
