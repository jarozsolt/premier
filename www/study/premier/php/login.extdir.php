<?php
// Calculate the app path
$app_path = str_replace("\\", "/", realpath(dirname(__FILE__)));

// Calculate the study path
$study_path = join(array_slice(explode( "/" ,$app_path), 0, -1), "/");

// Calculate the study directory
$study_dir = basename ($study_path ,"/");

// Calculate the subdomain path
$subdomain_path = join(array_slice(explode( "/" ,$study_path), 0, -1), "/");

// Calculate the public (www) path
$public_path = join(array_slice(explode( "/" ,$subdomain_path), 0, -1), "/");

// Calculate the base path
$base_path = join(array_slice(explode( "/" ,$public_path), 0, -1), "/");
// Load the main configuration file
require_once($base_path . '/' . $study_dir . '/configs/app.conf');
require('extdirect.class.php');
require('usermanagement.class.php');

ExtDirect::$namespace = 'Kron';
ExtDirect::$descriptor = 'Kron.APIDesc';
ExtDirect::$default_api_output = 'javascript';
//ExtDirect::provide(array('UserManagement','Server'));
ExtDirect::$declare_method_function='declare_methods_callback_function';

function declare_methods_callback_function($class, $method)
{
	// only the UserManagement::login is remotable
	$remotable_functions = array('UserManagement::login');
	$key = $class . '::' . $method;
	return in_array( $key, $remotable_functions );
}

ExtDirect::provide('UserManagement');
?>