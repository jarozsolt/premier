<?php
/**
 *---------------------------------------------------------------
 * template.class.php
 *
 * This file contains a simple template engine class
 * (use [@tag] tags in your templates).
 *
 * @package Kron
 * @subpackage print
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */


class Template {
	/**
	 * The filename of the template to load.
	 *
	 * @access protected
	 * @var string
	 */
    protected $file;

    /**
     * An array of values for replacing each tag on the template (the key for each value is its corresponding tag).
     *
     * @access protected
     * @var array
     */
    protected $values = array();

    /**
     * Creates a new Template object and sets its associated file.
     *
     * @param string $file the filename of the template to load
     */
    public function __construct($file) {
        $this->file = $file;
    }

    function __destruct() {
        unset($this->values);
    }
    /**
     * Sets a value for replacing a specific tag.
     *
     * @param string $key the name of the tag to replace
     * @param string $value the value to replace
     */
    public function set($key, $value) {
        $this->values[$key] = $value;
    }

    /**
     * Gets the value of a tag.
     *
     * @param string $key the name of the tag
     * @return string
     */
	public function get($key) {
        return $this->values[$key];
    }


    /**
     * Check the tag exists.
     *
     * @param string $key the name of the tag
     * @return bool
     */
    public function isFieldExists($key) {
        return array_key_exists($key, $this->values);
    }

    public function isFieldNameExistsWildcard ( $search ) {
        $search = str_replace( '\*', '.*?', preg_quote( $search, '/' ) );
        $result = preg_grep( '/^' . $search . '$/i', array_keys( $this->values ) );
        return array_intersect_key( $this->values, array_flip( $result ) );
    }

    public function isFieldValueExistsWildcard ( $search) {
        $search = str_replace( '\*', '.*?', preg_quote( $search, '/' ) );
        $result = preg_grep( '/^' . $search . '$/i', array_values( $this->values ) );
        return array_intersect( $this->values, $result );
    }

	/**
     * Sets a value and checked state.
     *
     * @param string $key the name of the tag to replace
     * @param string $value the value to replace
     */
    public function setField($key, $htmlvalue, $type) {
		$value = htmlentities($htmlvalue, ENT_QUOTES, "UTF-8");
    	if ($type == "S") { //string
    		$this->values[$key] = 'value="'.$value.'"';
			$this->values[$key."Value"] = $value;
    	}
		else if ($type == "B") {//checkbox
			if ($value == "0") {
				$this->values[$key] = 'value="'.$value.'"';
			}
			else {
				$this->values[$key] = 'value="'.$value.'" checked="checked"';
			}
		}
		else if ($type == "D") {//date
			if ($value == "o.g." || $value == "n.a.") {
				$this->values[$key] = 'value="'.$value.'"';
				$this->values[$key."Value"] = $value;
			}
			else {

				$datevalue = substr($value, 0, 10);
				$this->values[$key] = 'value="'.$datevalue.'"';
				$this->values[$key."Value"] = $datevalue;
			}
		}
        else if ($type == "T") {//time
            $this->values[$key] = 'value="'.$value.'"';
            $this->values[$key."Value"] = $value;
        }
		else if ($type == "I") {//radio
			$radiokey = $key . $value;
			$this->values[$key] = 'value="'.$value.'"';
			$this->values[$radiokey] = ' checked="checked" selected';
		}
		else {//text
			$this->values[$key] = $value;
		}
    }

    /**
     * Outputs the content of the template, replacing the keys for its respective values.
     *
     * @return string
     */
    public function output($removevariables) {
    	/**
    	 * Tries to verify if the file exists.
    	 * If it doesn't return with an error message.
    	 * Anything else loads the file contents and loops through the array replacing every key for its value.
    	 */
        if (!file_exists($this->file)) {
        	return "Error loading template file ($this->file).<br />";
        }
        $output = file_get_contents($this->file);

        foreach ($this->values as $key => $value) {
        	$tagToReplace = "[@$key]";
        	$output = str_replace($tagToReplace, $value, $output);
        }

        if ($removevariables)
        {
            // kiszedjük a maradék, 'nem használt' változókat
            $output = preg_replace('/\[\@\S+\]/s', '', $output);
        }

        return $output;
    }


    /**
     * Merges the content from an array of templates and separates it with $separator.
     *
     * @param array $templates an array of Template objects to merge
     * @param string $separator the string that is used between each Template object
     * @return string
     */
    static public function merge($templates, $separator = "\n") {
    	/**
    	 * Loops through the array concatenating the outputs from each template, separating with $separator.
    	 * If a type different from Template is found we provide an error message.
    	 */
        $output = "";

        foreach ($templates as $template) {
            if (isset($template) && $template != '') {
            	$content = (get_class($template) !== "Template")
            		? "Error, incorrect type - expected Template."
            		: $template->output(false);
            	$output .= $content . $separator;
            }
        }

        return $output;
    }
}
?>