<?php
/**
 *---------------------------------------------------------------
 * forms.class.php
 *
 * This file contains the Forms class, implementing the general form CRUD functions
 *
 * @package Kron
 * @subpackage formfields
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 * @todo: filter input!!!!
 *
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');
require_once('template.class.php');
require_once('printer.class.php');
require_once("print/dompdf_config.inc.php");


class Forms
{
  	/**
     *  This function returns the name of the messages view
     */
	public function getFormsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'forms';
	}

	/**
     *  This function returns the name of the messages table
     */
	public function getFormsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'forms_ts';
	}

	 /**
     *  This function returns the name of the messages view
     */
	public function getFormFieldsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields';
	}

	/**
     *  This function returns the name of the messages table
     */
	public function getFormFieldsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields_ts';
	}

	/**
     *  This function returns the name of the messages table
     */
	public function getFormFieldsTableHistoryName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields_ht';
	}

   /**
     *  This function returns the name of the users view
     */
	public function getUsersViewName()
	{
		return DatabaseConfig::db_tableprefix . 'users';
	}
	/**
     *  This function returns the name of the queries table
     */
	public function getQueriesTableName()
	{
		return DatabaseConfig::db_tableprefix . 'queries_ts';
	}

	/**
     *  This function returns the name of the queries view
     */
	public function getQueriesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'queries';
	}

	 /**
     *  This function returns the name of the sites view
     */
	public function getSitesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'sites';
	}
	/**
     *  This function returns the name of the countries view
     */
	public function getPatientsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'patients';
	}
	/**
     *  This function returns the name of the queries table
     */
	public function getPatientsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'patients_ts';
	}

	/** Lekérdezi egy patient összes form-ját fieldestül
	 *  Inputs:
	 * 	$params->patientid : Patient Id
	 *  Return array:
	 *  totalCount: number of forms
	 *  data: form fields data
	 *
	 *  @remotable
	 */
   	public function getForms(stdClass $params)
   	{
    	$logger = Logger::getInstance();
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
	//	$logger->logInfo("getForms called with");
	//	$logger->logInfo($params);
		$success = false;
		$response = array();
		$results=array();
		$allFieldApproved=array();
		$formInstanceIds =array();
		$formInstanceIdsString = '';
		$responseFormfields=array();
		if ($params->patientid <> '' )
		{
			try
			{
				$db = new Database();

				if ($params->instanceid == '') {
					$sql = "SELECT * FROM " . $this->getFormsViewName() . " WHERE patientid = ? ";
					$db->query($sql, $params->patientid );
				} else {
					$sql = "SELECT * FROM " . $this->getFormsViewName() . " WHERE patientid = ? AND instanceid = ? ";
					$db->query($sql, $params->patientid, $params->instanceid );
				}

				if( $db->row_count() > 0) {
					$today  = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
					for ($i=1; $i<=$db->row_count(); $i++) {
						$row = $db->fetch("assoc");
						if ($row != null) {
							$response[$row['instanceid']]=$row;
							$response[$row['instanceid']]['formfields']=array();
							$responseFormfields[$row['instanceid']]=array();
							$allFieldApproved[$row['instanceid']] = false ;
							if ($response[$row['instanceid']]['validity'] == 'Y') {
								$allFieldApproved[$row['instanceid']] = true; // de majd ha lesz egy mező,a mai nem jóváhagyott, törlöm
							} else if ($response[$row['instanceid']]['duedate'] != '') {
								if ( $response[$row['instanceid']]['status'] == FrameworkConfig::VISITSTATUS_EMPTY && $db->dbdatetime_to_phpdatetime($response[$row['instanceid']]['duedate']) < $today) {
									$response[$row['instanceid']]['status'] = FrameworkConfig::VISITSTATUS_MISSING;
								}
							}
							array_push($formInstanceIds, $row['instanceid']);
						}
					}

					$formInstanceIdsString = join(",", $formInstanceIds);

				 	// collect form fields status
					$sql = "SELECT * FROM " . $this->getFormFieldsViewName() . " WHERE form_instanceid IN( " . $formInstanceIdsString . " ) ";
					$db->query($sql);

					if( $db->row_count() > 0) {
						for ($i=1; $i<=$db->row_count(); $i++) {
							$row = $db->fetch("assoc");
							if ($row != null) {
								$row['querystatus'] = '';
								//array_push($response[$row['form_instanceid']]['formfields'],$row);
								$responseFormfields[$row['form_instanceid']][$row['fieldname']]= $row;
								if($row['status'] != FrameworkConfig::FIELDSTATUS_MONITOR_APPROVED){
									$allFieldApproved[$row['form_instanceid']] = false;
								}
							}
						}
					}

					// collect form fields querystatus
					$sql = "SELECT * FROM " . $this->getQueriesViewName() . " WHERE form_instanceid IN( " . $formInstanceIdsString . " ) AND action = '" . FrameworkConfig::QUERYTYPE_QUERY . "' ";
					$db->query($sql);

					if( $db->row_count() > 0) {
						for ($i=1; $i<=$db->row_count(); $i++) {
							$row = $db->fetch("assoc");
							if ($row != null) {
								//$row['querystatus'] = '';
								if (array_key_exists($row['fieldname'], $responseFormfields[$row['form_instanceid']])) {
									$qStatus = $row['status'];
									//  kell a lenti? elvileg a form save után answeredre állítom az eltérő értékű field querieit, amit sync()  elek is...
									//ha más az eredeti querizett fieldvalue, mint a mostani, akkor ha open /reopened a query - át kell tenni answeredre
									if ($row['status'] == FrameworkConfig::QUERYSTATUS_OPEN || $row['status'] == FrameworkConfig::QUERYSTATUS_REOPENED) {
										if ($responseFormfields[$row['form_instanceid']][$row['fieldname']]['fieldvalue'] <> $row['fieldvalue'] || $responseFormfields[$row['form_instanceid']][$row['fieldname']]['fieldtype'] <> $row['fieldtype']  ) {
											$qStatus = FrameworkConfig::QUERYSTATUS_ANSWERED;
										}
									}
									$responseFormfields[$row['form_instanceid']][$row['fieldname']]['querystatus'] .= $qStatus ;
								} else { // ha nincs field rekord(pl. törölték) akkor is kell valamit leküldeni, hogy lemenjen a query status
									$responseFormfields[$row['form_instanceid']][$row['fieldname']] =  array(
 										'form_instanceid' => $row['form_instanceid'],
										'fieldname' => $row['fieldname'],
										'istmp' => 0,
										'fieldvalue' => '',
										'fieldtype' => '',
										'status' => '', //FrameworkConfig::FIELDSTATUS_REOPENED ,
										'querystatus' => $row['status']
									);
								}
							}
						}
					}




				}
				$db->close();
			// @TODO : kell ez a lenti? nem kliens oldalon nézem? amúgy hibás, mert lehet open query is a formon, amit itt nem veszek figyelembe
		  		foreach ($allFieldApproved as $key => $value) {
					if($value) {
						$response[$key]['status'] = FrameworkConfig::VISITSTATUS_APPROVED;
					}
				}

				foreach ($responseFormfields as $key => $formInstance) {
					foreach ($formInstance as $key2 => $value) {
						if($value) {
							array_push($response[$key]['formfields'], $value);
						}
					}
				}


				foreach ($response as $key => $value) {
					array_push($results,$value);
				}
				$success = true;
			}
			catch (Exception $e)
			{ //Log the error
				$logger->logError("Userid: $userId , Operation: getForms, Exception: $e");
			}
		} //end if

	//$logger->logInfo("getForms response");
	//$logger->logInfo(Array('success' => $success, 'data' => $results));

	return Array('success' => $success, 'data' => $results);
	}


// ez talán már nem is kell
	 public function identifyFieldType($key, $value)
	 {
	 	if ( substr($key, -4) == "Date" ) {
			 return 'D'; // date
		} else if (is_bool($value)) {
			 return 'B'; // boolean
		} else if (is_int($value)) {
			 return 'I'; // integer
		}  else {
			 return 'S'; // string
		}
	 }
// ez talán már nem is kell
	  public function convertFieldValue($type, $value)
	 {
	 	if ($type == 'D') {
			return date('Y-m-dTH:i:s', strtotime($value)) ;
		} else if ($type == 'B') {
			return ( $value == '1'? true : false ) ;
		} else if ($type == 'I') {
			return intval($value);
		} else {
			return $value;
		}
	 }
	 // csak azért csináltam , mert updateform nál az original data nem volt jó az előzővel
	   public function convertFieldValue2($type, $value)
	 {
	 	if ($type == 'D') {
			return $value ;
		} else if ($type == 'B') {
			return ( $value == '1'? true : false ) ;
		} else if ($type == 'I') {
			return intval($value);
		} else {
			return $value;
		}
	 }


	 	/** This function returns the input form's history
	 *  params:
	 *  form_instanceid - form_instanceid
	 *  fieldname - field name
	 *  @remotable
	 */
	public function getFieldHistories(stdClass $params)
    {
    	$userId = isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
 		$logger = Logger::getInstance();

		//$logger->logInfo(' getFieldHistories params');
		//$logger->logInfo($params);

		 /* @TODO : jogosultság ellenőrzés user - patient*/
		$success = false;
		$result = array();
		$history = array();
		$prevItemEnded = null;
		$prevItemEndedName = null;
		if ($params->form_instanceid >= 0 && $params->fieldname <> '') {
			try
			{
				$db = new Database();
				// a mező érték változások legyűjtése
				$sql = "SELECT f.form_instanceid , f.fieldname, f.parent, f.start_ts, f.end_ts, u.name, f.fieldvalue, f.fieldtype, f.status, u2.name as ended_name FROM " . $this->getFormFieldsTableHistoryName() . " f INNER JOIN " .  $this->getUsersViewName() . " u ON f.started_by = u.uiid AND f.form_instanceid = ? AND f.fieldname = ? LEFT JOIN " .  $this->getUsersViewName() . " u2 ON f.ended_by = u2.uiid ORDER BY start_ts " ;
				$db->query($sql, $params->form_instanceid, $params->fieldname );
				$rowcount =	$db->row_count();
				if( $rowcount > 0)	{
					for ($i=1; $i<=$rowcount; $i++) {
						$row = $db->fetch("assoc");
						if ($prevItemEnded <> null) {// akkor kell a törlést listázni, ha nem követi update ugyanarra a mezőre uazzal az idővel
							if  (( $row['fieldname'] == $history['fieldname'] &&  $row['start_ts'] <> $prevItemEnded ) || ($row['fieldname'] <> $history['fieldname'])){
								$history['action'] = FrameworkConfig::FIELDACTION_DELETEVALUE;
								$history['datetime']= $prevItemEnded;
								$history['user']= $prevItemEndedName;
								$history['text'] = "";
								$history['type'] = "";

								array_push($result, $history);
							}
						}
						$prevItemEnded = $row['end_ts'];
						$prevItemEndedName =$row['ended_name'];
						$history['datetime'] = $row['start_ts'];
						$history['user']= $row['name'];
						$history['fieldname'] = $row['fieldname'];
						// identify action from status:
						// OPEN > SETVALUE,  MONITOR_APPROVED > MONITOR_APPROVE , reopened > reopen
						if ($row['status'] == FrameworkConfig::FIELDSTATUS_OPEN ) {
							$history['action']= FrameworkConfig::FIELDACTION_SETVALUE;
							$history['type'] = $row['fieldtype'];
							$history['text'] = $row['fieldvalue'];
							array_push($result, $history);
						} else if ($row['status'] == FrameworkConfig::FIELDSTATUS_MONITOR_APPROVED) {
							$history['action']= FrameworkConfig::FIELDACTION_MONITOR_APPROVE;
							$history['type']= "";
							$history['text']= "";
							array_push($result, $history);
						} else if ($row['status'] == FrameworkConfig::FIELDSTATUS_REOPENED) {
							$history['action']= FrameworkConfig::FIELDACTION_REOPEN;
							$history['type']= "";
							$history['text']= "";
							array_push($result, $history);
						} //
					}
				}
//////////////////////////////////////////////////////////////////////////////////////////
// a mező érték változások legyűjtése
				$sql = "SELECT f.form_instanceid , f.fieldname, f.parent, f.start_ts, f.end_ts, u.name, f.fieldvalue, f.fieldtype, f.status, u2.name as ended_name FROM " . $this->getFormFieldsTableName() . " f INNER JOIN " .  $this->getUsersViewName() . " u ON f.started_by = u.uiid AND f.form_instanceid = ? AND f.fieldname = ? LEFT JOIN " .  $this->getUsersViewName() . " u2 ON f.ended_by = u2.uiid ORDER BY start_ts " ;
				$db->query($sql, $params->form_instanceid, $params->fieldname );
				$rowcount =	$db->row_count();
				if( $rowcount > 0)	{
					for ($i=1; $i<=$rowcount; $i++) {
						$row = $db->fetch("assoc");
						if ($prevItemEnded <> null) {// akkor kell a törlést listázni, ha nem követi update ugyanarra a mezőre uazzal az idővel
							if  (( $row['fieldname'] == $history['fieldname'] &&  $row['start_ts'] <> $prevItemEnded ) || ($row['fieldname'] <> $history['fieldname'])){
								$history['action'] = FrameworkConfig::FIELDACTION_DELETEVALUE;
								$history['datetime']= $prevItemEnded;
								$history['user']= $prevItemEndedName;
								$history['text'] = "";
								$history['type'] = "";

								array_push($result, $history);
							}
						}
						$prevItemEnded = $row['end_ts'];
						$prevItemEndedName =$row['ended_name'];
						$history['datetime'] = $row['start_ts'];
						$history['user']= $row['name'];
						$history['fieldname'] = $row['fieldname'];
						// identify action from status:
						// OPEN > SETVALUE,  MONITOR_APPROVED > MONITOR_APPROVE , reopened > reopen
						if ($row['status'] == FrameworkConfig::FIELDSTATUS_OPEN ) {
							$history['action']= FrameworkConfig::FIELDACTION_SETVALUE;
							$history['type'] = $row['fieldtype'];
							$history['text'] = $row['fieldvalue'];
							array_push($result, $history);
						} else if ($row['status'] == FrameworkConfig::FIELDSTATUS_MONITOR_APPROVED) {
							$history['action']= FrameworkConfig::FIELDACTION_MONITOR_APPROVE;
							$history['type']= "";
							$history['text']= "";
							array_push($result, $history);
						} else if ($row['status'] == FrameworkConfig::FIELDSTATUS_REOPENED) {
							$history['action']= FrameworkConfig::FIELDACTION_REOPEN;
							$history['type']= "";
							$history['text']= "";
							array_push($result, $history);
						} //
					}
				}

/////////////////////////////////////////////////////////////////////////////////////////
				// ha az utolsó rekord törölt, azt is jelezni kell
				if ($prevItemEnded <> null) {
					$history['action'] = FrameworkConfig::FIELDACTION_DELETEVALUE;
					$history['datetime']= $prevItemEnded;
					$history['user'] = $row['ended_name'];
					$history['text'] = "";
					$history['type'] = "";
					array_push($result, $history);
				}

				$prevItemEnded = null;
				$prevItemAction = null;
				$prevItemQIId = null;
				$prevItemEndedName = null;
				// query-k legyűjtése

				$sql = "SELECT t.created, u.name, t.fieldname, t.text, t.action, t.status, t.start_ts, t.end_ts, t.query_iid, u2.name as ended_name FROM " . $this->getQueriesTableName() . " t INNER JOIN " .  $this->getUsersViewName() . " u ON t.started_by = u.uiid AND t.form_instanceid = ? AND t.fieldname = ? LEFT JOIN " .  $this->getUsersViewName() . " u2 ON t.ended_by = u2.uiid ORDER BY query_iid, action, start_ts " ;
				$db->query($sql, $params->form_instanceid, $params->fieldname);
				$rowcount=	$db->row_count();
				if( $rowcount > 0)	{
					for ($i=1; $i<=$rowcount; $i++) {
						$row = $db->fetch("assoc");
						if ($row != null) {
							if ($prevItemEnded <> null) {// akkor kell a törlést listázni (a prev itemre), ha nem követi update ugyanarra a mezőre uazzal az idővel
								if  (( $row['query_iid'] <> $prevItemQIId ) || ($row['query_iid'] == $prevItemQIId && $row['action'] <> $prevItemAction ) ||($row['query_iid'] ==  $prevItemQIId && $row['action'] == $prevItemAction && $row['start_ts'] <> $prevItemEnded ) ) {
									if ($prevItemAction == FrameworkConfig::QUERYTYPE_QUERY ) {
										$history['action'] = FrameworkConfig::FIELDACTION_DELETEQUERY; // K
									} else {
										$history['action'] = FrameworkConfig::FIELDACTION_DELETECOMMENT; // L
									}
									$history['user']= $prevItemEndedName;
									$history['datetime']= $prevItemEnded;
									array_push($result, $history);
								}
							}

							$prevItemEnded = $row['end_ts'];
							$prevItemAction = $row['action'];
							$prevItemQIId = $row['query_iid'];
							$prevItemEndedName =$row['ended_name'];
						//	if ($row['action'] == FrameworkConfig::QUERYTYPE_COMMENT || $row['status']== FrameworkConfig::QUERYSTATUS_CLOSED || $row['status']== FrameworkConfig::QUERYSTATUS_OPEN) {
						// vagy mégis:		// mert a commentelés miatti query statusváltást nem listázom ki, duplán lenne...
								if ( $row['action'] == FrameworkConfig::QUERYTYPE_QUERY ) {
									if ($row['status']== FrameworkConfig::QUERYSTATUS_CLOSED) {
										$history['action'] = FrameworkConfig::FIELDACTION_CLOSEQUERY; // F = close query
									} else if ($row['status']== FrameworkConfig::QUERYSTATUS_OPEN) {
										$history['action'] = FrameworkConfig::FIELDACTION_OPENQUERY;// Q = open query,
									} else if ($row['status']== FrameworkConfig::QUERYSTATUS_ANSWERED) {
										$history['action'] = FrameworkConfig::FIELDACTION_ANSWER_QUERY;// A = open query,
									} else if ($row['status']== FrameworkConfig::QUERYSTATUS_REOPENED) {
										$history['action'] = FrameworkConfig::FIELDACTION_REOPEN_QUERY;// B = open query,
									}
								} else {
									$history['action']= FrameworkConfig::FIELDACTION_COMMENT;  //  C = comment
								}
								$history['datetime']= $row['start_ts'];
								$history['user']= $row['name'];
								$history['text']= $row['text'];
								$history['fieldname'] = $row['fieldname'];
								$history['type']= "S";
								array_push($result, $history);
							//}
						}
					}
					if ($prevItemEnded <> null) {// akkor kell a törlést listázni, ha nem követi update ugyanarra a mezőre uazzal az idővel
						if ($prevItemAction == FrameworkConfig::QUERYTYPE_QUERY ) {
							$history['action'] = FrameworkConfig::FIELDACTION_DELETEQUERY; // K
						} else {
							$history['action'] = FrameworkConfig::FIELDACTION_DELETECOMMENT; // L
						}
						$history['user'] = $row['ended_name'];
						$history['datetime']= $prevItemEnded;
						array_push($result, $history);
					}
				}

			$success = true;
			$db->close(); // Closes the cursor to free up memory
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: getFieldHistories, Exception: $e");

			}
		}
		//$logger->logInfo('getfieldhistory válasz');
		//$logger->logInfo(Array('success' => $success, 'data' => $result ));
		return Array('success' => $success, 'data' => $result );
	}



	/**  creates new form the given fields
	 * 	Inputs:
	 * 	$params
	 * 		->patientid : Patient Id
	 *  	->formtype : Visit Type
	 * 		->istmp : temporary record?
	 * 		->status : status
	 *  Return array:
	 *  success: boolean
	 *  data :  input values including the new id, instanceid
	 *
	 *  @remotable
	 */
   	public  function createForm(stdClass $params)
   	{
    	$logger = Logger::getInstance();

		//$logger->logInfo(" createForm input fields:");
		//$logger->logInfo($params);

		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$success = false;
		if (isset($params)){
			if ($params->patientid <> '' and $params->formtype<> '' )
			{
				try
				{
					$db = new Database();
					// lezárt páciensre nem szabad semmit csinálni
					$sql = "SELECT isclosed FROM " . $this->getPatientsViewName() . " WHERE patientid = ? ";
					$db->begin_transaction();
					$db->query($sql, $params->patientid);
					$row = $db->fetch("assoc");
					if ($row != null) {
						if ($row['isclosed'] == 0 ) {// not closed patient
							//$logger->logInfo('nem closed patient!');

							// inserts general data into the froms table
							$started= $db->phpdatetime_to_dbdatetime();
							$sql = "INSERT INTO " . $this->getFormsTableName() . " (instanceid, parentid, patientid, formtype, status, istmp, validity, duedate, created, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

							$db->command($sql, 0, $params->parentid, $params->patientid, $params->formtype, $params->status, $params->istmp, $params->validity, $params->duedate, $started, $started , $userId);

							$lastinsertid=$db->get_lastinsertid();

							// new record inserted, therefore the instanceid = last inserted record id
							$sql = "UPDATE " . $this->getFormsTableName() . "  SET instanceid = ? WHERE id = ? ";
							$db->command($sql, $lastinsertid, $lastinsertid);


							$params->id = $lastinsertid;
							$params->instanceid= $lastinsertid;
							$params->created = $started;

							$success = true;
						}
					}
					$db->commit();
					$db->close();
				}
				catch (Exception $e)
				{// Log the error
					//$logger->logError($e);
					$logger->logError("Userid: $userId , Operation: createForm, Exception: $e");
					$db->rollback();
				}
			}
		}//end if
		////$logger->logInfo(" createForm response:");
		////$logger->logInfo(Array('success' => $success, 'data' => $params ));
		return  Array('success' => $success, 'data' => $params );
   	}

	/**  Létrehozza a field rekordokat
	 * 	Inputs: stdClass vagy [stdClass] $params->
	 *  	'form_instanceid',
	 * 		'istmp',
	 * 		'fieldname',
	 * 		'parent',
	 * 		'fieldvalue',
	 * 		'fieldtype',
	 * 		'status'
	 *
	 * 	 	'clientid', clientIdProperty
	 *  Return array:
	 *  success: boolean
	 *  data :  [stdClass] :	input values + id
	 *
	 *  @remotable
	 */
   	public  function createField( $input)
   	{
    	$logger = Logger::getInstance();

		//$logger->logInfo(" createfield input fields:");
		//$logger->logInfo($input);
		if (is_array($input) ) {
			$inputArray = $input;
		} else {
			$inputArray = array($input);
		}
		////$logger->logInfo("inputArray");////$logger->logInfo($inputArray);

		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$success = false;
		$SAEForm_instanceid = null;
		$serious = false;

		try
		{
			$db = new Database();

			// lezárt páciensre nem szabad semmit csinálni
			$firstParam = reset($inputArray);
			$sql = "SELECT p.isclosed FROM " . $this->getPatientsViewName() . " p INNER JOIN " . $this->getFormsViewName() . " f ON f.instanceid = ? AND f.patientid = p.patientid ";
			$db->begin_transaction();
			$db->query($sql, $firstParam->form_instanceid);
			$row = $db->fetch("assoc");
			if ($row != null) {
				if ($row['isclosed'] == 0 ) {// not closed patient
					//$logger->logInfo('nem closed patient!');

					foreach ($inputArray as $key => $params) {
						if( isset($params->form_instanceid) && $params->form_instanceid >0 ) {
							// inserts general data into the froms table
							$started= $db->phpdatetime_to_dbdatetime();

							$sql = "INSERT INTO " . $this->getFormFieldsTableName() . " (form_instanceid, fieldname, istmp, parent, fieldvalue, fieldtype, status, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?)";
							$db->command($sql, $params->form_instanceid, $params->fieldname, $params->istmp, $params->parent, $params->fieldvalue, $params->fieldtype, $params->status, $started , $userId);

							$lastinsertid=$db->get_lastinsertid();
							$params->id = $lastinsertid;

							/// ha SAE van, ide jön a mail küldés

							if ($params->fieldname== 'ae_serious' &&  $params->istmp == '0') {
								$SAEForm_instanceid = $params->form_instanceid;
								if ($params->fieldvalue == '1') {
									$serious = true;
								}
							}
						}

					} 	// foreach end
					$success = true;
				}
			}
			$db->commit();
			$db->close();
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: createField, Exception: $e");
			$db->rollback();
		}

		if ($SAEForm_instanceid <> null) {
			$params->serious = $serious;
			$this->sendSAENotification($params);
		}
		//$logger->logInfo(" createField response:");
		//$logger->logInfo(Array('success' => $success, 'data' => $inputArray ));
		return  Array('success' => $success, 'data' => $inputArray );
   	}



	/** Módosít egy form rekordot
	 *  Ha temp rekord - sima update, míg ha már rendes, akkor historizált módosítás
	 *  Form rekordból mindig csak egy db van, ami az első autosave-től addig lehet csak temp a db-ben, amig az első form OK (=save) meg nem történik
	 *
	 * 	Inputs:
	 * 	$params->patientid : Patient Id
	 *  $params->formtype : Visit Type
	 * 	$params->id : form instance number
	 *  $params->* : all the form fields
	 *
	 *  Return array:
	 *  success: boolean
	 *  data :  input values including
	 *
	 *   @remotable
	 */
   	public  function updateForm( $input)
   	{
    	$logger = Logger::getInstance();

	//	$logger->logInfo(" updateForm input:");
	//	$logger->logInfo($input);
		if (is_array($input) ) {
			$inputArray = $input;
		} else {
			$inputArray = array($input);
		}

		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$success = false;
		$lastinsertid = '';
		try
		{
			$db = new Database();
			// lezárt páciensre nem szabad semmit csinálni
			$firstParam = reset($inputArray);
			$sql = "SELECT isclosed FROM " . $this->getPatientsViewName() . " WHERE patientid = ? ";
			$db->begin_transaction();
			$db->query($sql, $firstParam->patientid);
			$row = $db->fetch("assoc");
			if ($row != null) {
				if ($row['isclosed'] == 0 ) {// not closed patient
					//$logger->logInfo('nem closed patient!');

					$update_ts = $db->phpdatetime_to_dbdatetime();
					foreach ($inputArray as $key => $params) {
						if ($params->instanceid>=0 ) {
							$sql = "SELECT * FROM " . $this->getFormsViewName() . " WHERE instanceid = ? ORDER BY id DESC "  ;
							$db->query($sql, $params->instanceid);
							$rowCount = $db->row_count();
							if ( $rowCount  > 0) {
								$row = $db->fetch("assoc");
								if ($row != null) {

									if ( $row['istmp'] == 1 ) {
										// simán egy tmp rekordot érvényesítek, vagy még ideiglenest írok felül ideiglenesen
										$sql = "UPDATE " . $this->getFormsTableName() . " SET istmp = ? , status = ? , validity = ? , duedate = ? , created = ? , signed_by = ? , signed = ? , reviewed_by = ? , reviewed = ? , approved_by = ? , approved = ? , start_ts = ? , started_by = ? WHERE id = ? ";
										$db->command($sql, $params->istmp, $params->status, $params->validity, $params->duedate, $params->created, $params->signed_by, $params->signed, $params->reviewed_by, $params->reviewed, $params->approved_by, $params->approved, $update_ts, $userId, $row['id']);
										$lastinsertid = $row['id'];
									} else {

										// eredetit törlöm, létrehozok egy új példányt az új adatokkal
										$sql = "UPDATE " . $this->getFormsTableName() . " SET end_ts = ? , ended_by = ? WHERE id = ? ";
										$db->command($sql, $update_ts, $userId, $row['id']);

										$sql = "INSERT INTO " . $this->getFormsTableName() . " (instanceid, parentid, patientid, formtype, istmp,  status, validity, duedate, created, signed_by, signed, reviewed_by, reviewed, approved_by, approved, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
										$db->command($sql, $params->instanceid , $params->parentid, $params->patientid, $params->formtype, $params->istmp,  $params->status, $params->validity, $params->duedate, $params->created, $params->signed_by, $params->signed, $params->reviewed_by, $params->reviewed, $params->approved_by, $params->approved, $update_ts , $userId);

										$lastinsertid=$db->get_lastinsertid();
										$params->newid = $lastinsertid; // kell ez?
									}


									// ha visit formot updatelek, és változott a from stítusa, a patient rekord visit_status-értékét is módosítani kell, A validity eltérés is számít!
									if ( substr($params->formtype, 0, 5) == "Visit" && ( $params->status != $row['status'] || $params->validity != $row['validity'])) {

										//$logger->logInfo(" patient update ágon vagyok");
										// valamiért ha alább a view van, akkor a szerveren az nem frissul olyan hamar, és az előbbiekben módosított rekord kétszer látszik....
										$sql = "SELECT id, formtype, duedate, status, validity  FROM " . $this->getFormsTableName() . " WHERE formtype LIKE 'Visit%' AND patientid = ? AND ended_by IS NULL ORDER BY formtype " ;
										$db->query($sql,  $firstParam->patientid);
										$rowcount2=	$db->row_count();
										if( $rowcount2 > 0 ) {
											$visitStatuses = '';
											for ($i=1; $i<=$rowcount2; $i++) {
												$row2 = $db->fetch("assoc");
												if ($row2 != null) {

													$today  = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
													if ($row2['validity'] == 'Y') {
														$visitStatuses .= FrameworkConfig::VISITSTATUS_COMPLETED;
													} else if ($row2['duedate'] != '' && ( $row2['status'] == FrameworkConfig::VISITSTATUS_EMPTY || $row2['status'] == FrameworkConfig::VISITSTATUS_MISSING) ){
														if ( $db->dbdatetime_to_phpdatetime($row2['duedate']) < $today) {
															$visitStatuses .= FrameworkConfig::VISITSTATUS_MISSING;
														} else {
															$visitStatuses .= FrameworkConfig::VISITSTATUS_EMPTY;
														}
													} else {
														$visitStatuses .= $row2['status'];
													}
												}
											}
											// update patient record
											$sql = "UPDATE " . $this->getPatientsTableName() . " SET visit_status = ?  WHERE patientid = ? AND ended_by IS NULL ";
											$db->command($sql, $visitStatuses, $firstParam->patientid);
											//$logger->logInfo(" patientrekord updated $visitStatuses");
										}
									}

									$success = true;
								}
							}
							if ( $rowCount > 1)  { // javító ág: ha valamiért lenne több rekord ugyanazzal a névvel + istmp státusszal, azokat kitörlöm
								if ( $lastinsertid != ''){
									$sql = "UPDATE " . $this->getFormsTableName() . " SET end_ts = ? , ended_by = ? WHERE instanceid = ? AND id <> ? AND ended_by IS NULL ";
									$db->command($sql, $update_ts, $userId, $params->instanceid, $lastinsertid);
								}
							}
						}
					}
				}
			}
			$db->commit();
			$db->close();
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: updateForm, Exception: $e");
			$db->rollback();
			$success = false;
		}

		//$logger->logInfo(" updateForm response:", $visitStatuses);
	//	$logger->logInfo(Array('success' => $success, 'data' => $inputArray));

		return  Array('success' => $success, 'data' => $inputArray);
   	}


	/** Módosítja a már létező  formfield rekordot
	 *  Ha istmp volt - akkor sima update
	 *  Különben töröltre állít + újat hoz létre a megadott értékekkel
	 *
	 * 	Inputs: stdClass vagy [stdClass]
	 * 	$params->
	 *  	'id',
	 * 		'form_instanceid',
	 * 		'istmp',
	 * 		'fieldname',
	 * 		'parent',
	 * 		'fieldvalue',
	 * 		'fieldtype',
	 * 		'status'
	 * 	 	'clientid' :clientIdProperty
	 *  Return array:
	 *  success: boolean
	 *  data :  [stdclass] input values
	 *
	 *   @remotable
	 */
   	public  function updateField( $input)
   	{
    	//$inputArray =  array();
    	$logger = Logger::getInstance();

		//$logger->logInfo(" updateField input:");
		//$logger->logInfo($input);
		if (is_array($input) ) {
			$inputArray = $input;
		} else {
			$inputArray = array($input);
		}
		////$logger->logInfo("inputArray");////$logger->logInfo($inputArray);

		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$SAEForm_instanceid = null;
		$success = false;

		try
		{
			$db = new Database();
			// lezárt páciensre nem szabad semmit csinálni
			$firstParam = reset($inputArray);
			$sql = "SELECT p.isclosed FROM " . $this->getPatientsViewName() . " p INNER JOIN " . $this->getFormsViewName() . " f ON f.instanceid = ? AND f.patientid = p.patientid ";
			$db->begin_transaction();
			$db->query($sql, $firstParam->form_instanceid);
			$row = $db->fetch("assoc");
			$lastinsertid = '';
			if ($row != null) {
				if ($row['isclosed'] == 0 ) {// not closed patient
					//$logger->logInfo('nem closed patient!');

					foreach ($inputArray as $key => $params) {
						$sql = "SELECT * FROM " . $this->getFormFieldsViewName() . " WHERE form_instanceid = ? AND fieldname = ? AND istmp = ? ORDER BY id DESC"  ;
						$db->query($sql, $params->form_instanceid, $params->fieldname, $params->istmp );
						$rowCount = $db->row_count();
						if ( $rowCount > 0) {
							$row = $db->fetch("assoc");
							if ($row!= null) {
								$update_ts = $db->phpdatetime_to_dbdatetime();
								////$logger->logInfo($row);
								if ($row['istmp']== 1) {
									// temp rekord volt - simán update-elem az összes mezőt
									$sql = "UPDATE " . $this->getFormFieldsTableName() . " SET istmp = ? , parent = ? , fieldvalue = ? , fieldtype = ? , status = ? , start_ts = ? , started_by = ? WHERE id = ? ";
									$db->command($sql, $params->istmp, $params->parent, $params->fieldvalue, $params->fieldtype, $params->status, $update_ts, $userId, $row['id']);
									$lastinsertid = $row['id'];
								} else {
									 if ($row['fieldvalue'] <> $params->fieldvalue || $row['fieldtype'] <> $params->fieldtype || $row['status'] <> $params->status) {
										// set old record deleted
										$sql = "UPDATE " . $this->getFormFieldsTableName() . " SET end_ts = ? , ended_by = ? WHERE id = ? ";
										$db->command($sql, $update_ts, $userId, $row['id']);

										// insert new record with new values
										$sql = "INSERT INTO " . $this->getFormFieldsTableName() . " (form_instanceid, fieldname, istmp,  parent, fieldvalue, fieldtype, status, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?)";
										$db->command($sql, $row['form_instanceid'], $row['fieldname'], $row['istmp'], $row['parent'], $params->fieldvalue, $params->fieldtype, $params->status, $update_ts , $userId);
										// kell egyáltalán a lenti???
										$lastinsertid=$db->get_lastinsertid();
							//			$params->newid = $lastinsertid;
										$params->id = $lastinsertid;

										/// ha SAE van, ide jön a mail küldés

										if ($params->fieldname== 'ae_serious' &&  $params->fieldvalue == '1') {
											$SAEForm_instanceid = $params->form_instanceid;
										}

									 }
								}
							}
						}
						if ( $rowCount > 1)  { // javító ág: ha valamiért lenne több rekord ugyanazzal a névvel + istmp státusszal, azokat kitörlöm
							if ( $lastinsertid != ''){
								$sql = "UPDATE " . $this->getFormFieldsTableName() . " SET end_ts = ? , ended_by = ? WHERE form_instanceid = ? AND fieldname = ? AND istmp = ? AND id <> ? AND ended_by IS NULL ";
								$db->command($sql, $update_ts, $userId, $params->form_instanceid, $params->fieldname, $params->istmp, $lastinsertid);

							}
						}
						if ($rowCount == 0) { // ilyen akkor van, amikor üres mezőre ment query, mert akkor a mezők lekérdezésekor létrehozok egy dummy field rekordot a query statussal, ami le is megy
						// 	aztán ha a user ad neki értéket, akkor már nem create, hanem update-ként jön fel.
							$started= $db->phpdatetime_to_dbdatetime();

							$sql = "INSERT INTO " . $this->getFormFieldsTableName() . " (form_instanceid, fieldname, istmp,  fieldvalue, fieldtype, status, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?)";
							$db->command($sql, $params->form_instanceid, $params->fieldname, $params->istmp, $params->fieldvalue, $params->fieldtype, $params->status, $started , $userId);

							$lastinsertid=$db->get_lastinsertid();
							$params->id = $lastinsertid;

							/// @TODO :ha SAE van, ide jön a mail küldés
							
						}
					} // foreach end
					$success = true;
				}
			}
			$db->commit();
			$db->close();

		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: updateField, Exception: $e");
			$db->rollback();
		}
		/* kiszedve, elvileg nem kell ide
		if ($SAEForm_instanceid <> null) {
			$this->sendSAENotification($params);
		}
		*/
		//$logger->logInfo(" updateField response:");
		//$logger->logInfo(Array('success' => $success, 'data' => $inputArray));

		return  Array('success' => $success, 'data' => $inputArray);
   	}

	/** Töröltre állítja a form rekordot az insatceid alapján
	 *  ugyanígy a hozzá tartozó formfields rekordokat is
	 *
	 * 	Inputs:
	 * 	$params->instanceid :form instance id
	 *
	 *  Return array:
	 *  success: boolean
	 *  data :  input paraméterek
	 *
	 *  @remotable
	 */
   	public  function deleteForm( stdClass $params)
   	{
    	$logger = Logger::getInstance();

		//$logger->logInfo(" deleteForm params and fields:");
		//$logger->logInfo($params);

		// todo access control if necessary???
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';

		$success = false;
		if ($params->instanceid >=0 )
		{
			try
			{
				$db = new Database();

				// lezárt páciensre nem szabad semmit csinálni
				$sql = "SELECT p.isclosed FROM " . $this->getPatientsViewName() . " p INNER JOIN " . $this->getFormsViewName() . " f ON f.instanceid = ? AND f.patientid = p.patientid ";
				$db->begin_transaction();
				$db->query($sql, $params->instanceid);
				$row = $db->fetch("assoc");
				if ($row != null) {
					if ($row['isclosed'] == 0 ) {// not closed patient
						//$logger->logInfo('nem closed patient!');

						// temp form rekord - tényleges törlés:
						$sql = "DELETE FROM " . $this->getFormsTableName() . " WHERE istmp = 1 AND instanceid = ? ";
						$db->command($sql,  $params->instanceid);
						// egyébként végleges törlés
						$ended = $db->phpdatetime_to_dbdatetime();
						$sql = "UPDATE " . $this->getFormsTableName() . " SET end_ts = ? , ended_by = ? WHERE ended_by IS NULL AND istmp = 0 AND instanceid = ?  ";
						$db->command($sql, $ended, $userId, $params->instanceid);

						// törlöm a form-field-eket is (logikai)
						$sql = "UPDATE " . $this->getFormFieldsTableName() . " SET end_ts = ? , ended_by = ? WHERE ended_by IS NULL AND form_instanceid = ? ";
						$db->command($sql, $ended, $userId,  $params->instanceid);
						$success = true;

					}
				}
				$db->commit();
				$db->close();
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: deleteForm, Exception: $e");
				$db->rollback();
			}
		} //end if

		return  Array('success' => $success, 'data' => $params);
   	}

   	/** Töröltre állítja a field rekordot az id alapján
	 *
	 * 	Inputs: stdClass vagy [stdClass] $params->
	 * 	$params->id : field id
	 *
	 *  Return array:
	 *  success: boolean
	 *  data : [stdClass] input
	 *
	 *  @remotable
	 */
   	public  function deleteField(  $input)
   	{
    	$logger = Logger::getInstance();

		//$logger->logInfo(" deleteFields input:");
		//$logger->logInfo($input);
		if (is_array($input) ) {
			$inputArray = $input;
		} else {
			$inputArray = array($input);
		}
		////$logger->logInfo("inputArray");////$logger->logInfo($inputArray);

		// todo access control if necessary???
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';

		$success = false;

		try
		{
			$db = new Database();

			// lezárt páciensre nem szabad semmit csinálni
			$firstParam = reset($inputArray);
			$sql = "SELECT p.isclosed FROM " . $this->getPatientsViewName() . " p INNER JOIN " . $this->getFormsViewName() . " f ON f.instanceid = ? AND f.patientid = p.patientid ";
			$db->begin_transaction();
			$db->query($sql, $firstParam->form_instanceid);
			$row = $db->fetch("assoc");
			if ($row != null) {
				if ($row['isclosed'] == 0 ) {// not closed patient
					//$logger->logInfo('nem closed patient!');

					foreach ($inputArray as $key => $params) {
						if ($params->istmp == 1) {
							// temp form rekord - tényleges törlés:
							$sql = "DELETE FROM " . $this->getFormFieldsTableName() . " WHERE istmp = 1 AND form_instanceid = ? AND fieldname = ? ";
							$db->command($sql, $params->form_instanceid, $params->fieldname );
						} else if ($params->istmp == 0) {
							// logikai törlés
							$ended = $db->phpdatetime_to_dbdatetime();
							$sql = "UPDATE " . $this->getFormFieldsTableName() . " SET end_ts = ? , ended_by = ? WHERE istmp = 0 AND form_instanceid = ? AND fieldname = ? AND ended_by IS NULL ";
							$db->command($sql, $ended, $userId, $params->form_instanceid, $params->fieldname);
						}

					}
					$success = true;
				}
			}
			$db->commit();
			$db->close();

		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: deleteField, Exception: $e");
			$db->rollback();
		}
		//$logger->logInfo(" deleteFields response:");
		//$logger->logInfo( Array('success' => $success, 'data' => $inputArray));

		return  Array('success' => $success, 'data' => $inputArray);
   	}


	/** Sign form
	 * 	Inputs:
	 * 	$params->'uiid': signer user id
	 * 	$params->'password' : signer user password,
	 * 	$params->'formsToSign' array of formids
	 *  Return array:
	 *  success: boolean
	 *  data :  input values
	 *
	 *  @remotable
	 */
   	public  function signForms( stdClass $params)
   	{
    	$logger = Logger::getInstance();

		//$logger->logInfo(" signForm  params");
		//$logger->logInfo($params);

		// todo access control if necessary???
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';

		$success = TRUE;
		if (count($params->formsToSign) >0 )
		{
			try
			{
				$db = new Database();

				// lezárt páciensre nem szabad semmit csinálni
				$firstParam = reset($params->formsToSign);
				$sql = "SELECT p.isclosed FROM " . $this->getPatientsViewName() . " p INNER JOIN " . $this->getFormsViewName() . " f ON f.instanceid = ? AND f.patientid = p.patientid ";
				$db->begin_transaction();
				$db->query($sql, $firstParam);
				$row = $db->fetch("assoc");
				if ($row != null) {
					if ($row['isclosed'] == 0 ) {// not closed patient
						//$logger->logInfo('nem closed patient!');

						$update_ts = $db->phpdatetime_to_dbdatetime();
						//check signer password

						$sql = "SELECT password, name, usergroup FROM " . $this->getUsersViewName() . " WHERE uiid = ? AND active = '1'";
						$db->query($sql, $params->uiid);
						if($db->row_count() > 0)
						{
							$row = $db->fetch();
							if ($row != null){// The user found
								$passwordhash = strtoupper($row['password']);
								$signerName=$row['name'];
								//if ($params->uiid > 0  && $params->password == $passwordhash && $row['usergroup'] == FrameworkConfig::USERGROUP_INVESTIGATOR ){// Found the user and the password is the same and investigator
								if ($params->uiid > 0  && $params->password == $passwordhash & UserManagement::checkUserPermissions($row['usergroup'],"Forms", "signForms") ){// Found the user and the password is the same and investigator

									//$logger->logInfo('signform password ok');
									$params->password = '';
									$params->signedForms = array();
									foreach ($params->formsToSign as $key => $value) {
										// get original value
										$sql = "SELECT * FROM " . $this->getFormsViewName() . " WHERE instanceid = ?  "  ;
										$db->query($sql,  $value );
										if( $db->row_count() > 0) {
											$row = $db->fetch("assoc");
											if ($row != null) {
												// set to deleted this record

												$sql = "UPDATE " . $this->getFormsTableName() . " SET end_ts = ? , ended_by = ? WHERE id = ? ";
												$db->command($sql, $update_ts, $userId, $row['id']);

												// insert new record with new values
												$sql = "INSERT INTO " . $this->getFormsTableName() . " (instanceid, parentid, patientid, formtype, istmp, status, validity, duedate, created, signed_by, signed, reviewed_by, reviewed, approved_by, approved, start_ts, started_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
												$db->command($sql, $row['instanceid'], $row['parentid'] , $row['patientid'], $row['formtype'],$row['istmp'],$row['status'],$row['validity'], $row['duedate'], $row['created'], $signerName, $update_ts, $row['reviewed_by'], $row['reviewed'], $row['approved_by'], $row['approved'], $update_ts , $userId);

												array_push($params->signedForms, $row['instanceid']);
											}
										} else {
											$success = FALSE;
										}
									}
								}
							}
						}
							}
				}
				$db->commit();
				$db->close();
				$params->signed = $update_ts;
				$params->signed_by = $signerName;

			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: signForms, Exception: $e");
				$db->rollback();
				$success = FALSE;
			}
		} //end if
	//$logger->logInfo(Array('success' => $success, 'data' => $params));
		return  Array('success' => $success, 'data' => $params);
   	}

	/**
     *  Print the form to PDF file
	 * 	Inputs:
	 * 	$params->formid: the form instance id
	 *
	 *  Return array:
	 *  success: boolean
	 *  filename: name of the generated temporary PDF file
	 *
	 *  @remotable
	 */
   	public function printForm( stdClass $params)
   	{
    	$logger = Logger::getInstance();

		$success = false;
		$tempfilename = '';
		$tempfile = '';
		$userId = isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : 'unknown userid';
		if ($params != null)
		{
			$formid = isset($params->formid) ? $params->formid : null;
			if ($formid !== null && is_numeric($formid))
			{
				// @TODO: Ellenőrizni, hogy a felhasználónak van-e joga a formhoz
				try
				{
					// Generating temporary filename
					$tempdir = PRINTOUTS_PATH;
					while (true) {
	 					$tempfilename = uniqid('print', true) . '.pdf';
						$tempfile = $tempdir . $tempfilename;
						if (!file_exists($tempfile)) break;
					}
					// Create pdf file
					$params->filename = $tempfile;
					$prn = new Printer();
					$prn->createPDFfromForm($params);
					$prn->Close();
					$success = true;
				}
				catch (Exception $e)
				{// Log the error
					$logger->logError("Userid: $userId , Operation: Forms::printForm, Exception: $e");
				}
			}
			else {
				// A formid paraméter nincs kitöltve, vagy nem szám
				// @TODO: az érvénytelen paraméter logolása
			}
		} //end if
		else {
			// Null paraméterrel hívták meg a függvényt, ez lehet hiba, de lehet próbálkozás is
			// @TODO: a lehetséges próbálkozás logolása
		}
		return  Array('success' => $success, 'filename' => $tempfilename);
   	}



	/**
	 *  Send a AE/SAE notification email
     *  Inputs:
     *  $params->formid: the AE/SAE form instance id
	 */
	public function sendSAENotification(stdClass $params)
   	{
    	$logger = Logger::getInstance();
		//$logger->logInfo("Forms::sendSAENotification called with params:$params");
		//$logger->logInfo($params);
		$tempdir = PRINTOUTS_PATH;
		$currtime = time();
		$todaydate = date("Y-m-d", $currtime);
		$todaytime = date("Y-m-d H:i:s", $currtime);
		$filetime = date("YmdHis", $currtime);
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$params->formtime = $todaytime;
		// @TODO: Átnevezni a hívóknál a $params->form_instanceid-t $params->formid-vé, és akkor ez nem kell
		$params->formid = $params->form_instanceid;
		$row = null;

		if (isset($params->form_instanceid) && $params->form_instanceid != null && $params->form_instanceid > 0)
		{

			try
			{
				$db = new Database();
				// Lekérdezzük a form adatait (patientid, formtype, parentid)
				$sql = "SELECT *  FROM " . Database::getFormsViewName() . " WHERE instanceid = ? AND istmp='0'";
				$db->query($sql, $params->formid);
				if ($db->row_count() > 0)
				{
					$row = $db->fetch("assoc");
					if ($row != null) {
						$params->patientid = $row['patientid'];
						$params->formtype = $row['formtype'];
						$params->parentid = $row['parentid'];
					}
					else
					{
						// @TODO: Hibát logolni, nem találtuk a formot
					}
				}
				else
				{
					// @TODO: Hibát logolni, nem találtuk a formot
				}
				// Lekérdezzük a site-ot a páciensből (siteid)
				if (isset($params->patientid) && $params->patientid != null && $params->patientid != '')
				{
					$sql = "SELECT * FROM " . Database::getPatientsViewName() . " WHERE patientid = ?";
					$db->query($sql, $params->patientid );
					if ($db->row_count() > 0)
					{
						$row = $db->fetch("assoc");
						if ($row != null) {
							$params->siteid = $row['siteid'];
						}
					}
				}
				else {
					// @TODO: Hibát logolni, nincs patientid
				}
				//Lekérdezzük a súlyosságot (ae_serious)
				$sql = "SELECT fieldvalue FROM " . $this->getFormFieldsViewName() . " WHERE form_instanceid = ? AND fieldname = 'ae_serious' AND istmp = '0'";
				$db->query($sql, $params->formid);
				// Alapból, ha esetleg nincs kitöltve még, nem súlyosra állítjuk
				$params->serious = 0;
				if ($db->row_count() > 0)
				{
					$row = $db->fetch("assoc");
					if ($row != null) {
						$params->serious = $row['fieldvalue'];
					}
				}

				$db->close(); // Closes the cursor to free up memory
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: sendSAENotification, Exception: $e");
			}

			if (isset($params->formid) && isset($params->siteid) && isset($params->patientid))
			{
				$link = 'https://study.datanova.hu/'.FrameworkConfig::study_folder.'/index.php?cmd=showform&pid='.$params->patientid.'&sid='.$params->siteid.'&fid='.$params->formid;

				// Az email recipient-ek és a form rövid nevének beállítása
				$to = FrameworkConfig::sae_email_recipients;
				if ($params->formtype == "AdverseEvent") {
					$aename = 'SAE';
					if (!isset($params->serious) || $params->serious == null || $params->serious != 1) {
						$aename = 'AE';
						$to = FrameworkConfig::ae_email_recipients;
					}
				}
                else {
                    $aename = 'MACCE';
                }

				// from
				$studyname = FrameworkConfig::STUDY_NAME;
				$from = 'KRON CDMS /' . $studyname .' <info@datanova.hu>';
				// subject
				$subject = $studyname . ' Clinical Study / ' . $aename . ' Report / Patient no.: ' . $params->patientid;

				// message
				$messageTemplate = new Template("print/templates/saemail.tpl");
				$messageTemplate->set("aename", $aename);
				$messageTemplate->set("studyname", $studyname);
				$messageTemplate->set("link", $link);
				$messageTemplate->set("patientid", $params->patientid);
				$message = $messageTemplate->output(true);

				// a random hash will be necessary to send mixed content
				$separator = md5(time());
				// carriage return type (we use a PHP end of line constant)
				$eol = PHP_EOL;
				// set the boundary
				//$boundary = "b_".strtoupper(md5(uniqid(time())));
				// To send HTML mail, the Content-type header must be set
				$headers = 'From: ' . $from . $eol;
				$headers .= 'MIME-Version: 1.0' . $eol;
				$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;

				$body = "Content-Transfer-Encoding: 7bit".$eol;
				$body .= "This is a MIME encoded message.".$eol;

				// message
				$body .= "--".$separator.$eol;
				$body .= "Content-Type: text/html; charset=\"utf-8\"".$eol;
				$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
				$body .= $message.$eol;

				$prn = new Printer();
				// add SAE pdf attachment
				$pdf_file_name = FrameworkConfig::STUDY_NAME .'_'.$aename.'Page_' . $params->patientid . '_' . $filetime . '.pdf';

				$attachment = chunk_split(base64_encode($prn->createPDFfromForm($params)));
				$body .= "--".$separator.$eol;
				$body .= "Content-Type: application/octet-stream; name=\"".$pdf_file_name."\"".$eol;
				$body .= "Content-Transfer-Encoding: base64".$eol;
				$body .= "Content-Disposition: attachment".$eol.$eol;
				$body .= $attachment.$eol;

                // Kikommentezve, mert nagyon lassú is lehet a páciens nyomtatás
				// add Patient Data pdf attachment
				//$pdf_file_name = FrameworkConfig::STUDY_NAME.'_' . $params->patientid . '_' . $filetime . '.pdf';
				//$attachment = chunk_split(base64_encode($prn->createPDFfromPatient($params)));
				//$body .= "--".$separator.$eol;
				//$body .= "Content-Type: application/octet-stream; name=\"".$pdf_file_name."\"".$eol;
				//$body .= "Content-Transfer-Encoding: base64".$eol;
				//$body .= "Content-Disposition: attachment".$eol.$eol;
				//$body .= $attachment.$eol;

				$body .= "--".$separator."--";
				$prn->Close();
				// Mail it
				//$logger->logInfo( $to);$logger->logInfo( $subject);$logger->logInfo( $message);
				mail($to, $subject, $body, $headers);
			}
		}
		else
		{
			// @TODO: Hibát logolni, nincs kitöltve az input paraméterben a formid
		}
	}
}
?>
