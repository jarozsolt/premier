<?php
/**
 *---------------------------------------------------------------
 * study.class.php
 *
 * This file contains the Study class for study specific functions
 *
 * @package Kron
 * @subpackage study
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');
require_once('template.class.php');

class Study
{

	public function __construct()
	{
	}




	static public function createSpecialPrintFields($record)
	{
		// @TODO: a form elnevezéseket valamilyen közös helyre tenni
		$formtype = $record->get("formtype");
		if ($formtype == "MedicalHistory")
		{
            Study::createMedicalHistorySpecialPrintFields($record);
		}
		else if ($formtype == "Medication")
		{
            Study::createMedicationSpecialPrintFields($record);
		}
		else if ($formtype == "AdverseEvent")
		{
            Study::createAdverseEventSpecialPrintFields($record);
		}
		else if ($formtype == "AdverseEventMACCE")
		{
            Study::createMACCESpecialPrintFields($record);
		}
		else if ($formtype == "Visit1")
		{
            Study::createVisit1SpecialPrintFields($record);
		}
		else if ($formtype == "Visit2")
		{
            Study::createVisit2SpecialPrintFields($record);
		}
		else if ($formtype == "Visit3")
		{
            Study::createVisit3SpecialPrintFields($record);
		}
		else if ($formtype == "Visit4")
		{
            Study::createVisit4SpecialPrintFields($record);
		}
	}

	public static function createMedicalHistorySpecialPrintFields($record)
    {
        // title
        $record->setField("title", _("Kórelőzmény, egyéb betegségek"), "V");
    }

    public static function createMedicationSpecialPrintFields($record)
    {
        // title
        $record->setField("title", _("Egyéb kezelések"), "V");

        // endDate - A végét speciálisan kezeljük
        Study::createOngoingPrintField($record, "endDate");

        // simpleDose - A dózist speciálisan kezeljük
        if ($record->isFieldExists("simpleDoseValue"))
        {
            $dose = $record->get("simpleDoseValue");


            if ($dose == "n.a.") {
                $dosevalue = "";
                $unitvalue = "";
                $dosenavalue = 1;
            }
            else {
                $dosenavalue = 0;
                $pieces = explode("|", $dose);
                $dosevalue = $pieces[0];
                $unitvalue = $pieces[1];
            }
            $record->setField("dosevalue", $dosevalue, "S");
            $record->setField("doseunit", $unitvalue, "I");
            $record->setField("dosena", $dosenavalue, "B");
        }

        // Több dózis
        // Lekérdezzük a dózis mezőket wildcard-dal
        $dosearray = $record->isFieldNameExistsWildcard("dose_*");
        ksort($dosearray);
        //$logger = Logger::getInstance();
        $doserecord = null;
        $doserecordTemplates = array();
        $prevdoserecordnumber = '';
        foreach ($dosearray as $key => $value) {
            // Szétvágjuk darabokra a kulcsot (mezőnevet), az első darab:dose, a második darab:a sorszám, a harmadik a típus
            $pieces = explode("_", $key);
            $doserecordnumber = $pieces[1];
            $dosefieldtype = $pieces[2];

            if ($prevdoserecordnumber != $doserecordnumber)
            { // Új rekordot hozunk létre
                $prevdoserecordnumber = $doserecordnumber;
                $doserecordTemplates[$doserecordnumber] = new Template("print/templates/doserecord.tpl");
                //$doserecordTemplates[$doserecordnumber] = $doserecord;

            }
            // Hozzáadjuk a mezőket
            if ($dosefieldtype == 'ingredientname')
            {
                $doserecordTemplates[$doserecordnumber]->set('ingredientname', $value);//html_entity_decode($value));
            }
            else if ($dosefieldtype == 'fieldValue')
            {
                if ($value == "n.a.") {
                    $dosevalue = "";
                    $unitvalue = "";
                    $dosenavalue = 1;
                }
                else {
                    $dosenavalue = 0;
                    $pieces = explode("|", $value);
                    // @TODO: A szétvágott darabok ellenőrzése
                    $dosevalue = $pieces[0];
                    $unitvalue = $pieces[1];
                }
                $doserecordTemplates[$doserecordnumber]->setField("dosevalue", $dosevalue, "S");
                $doserecordTemplates[$doserecordnumber]->setField("doseunit", $unitvalue, "I");
                $doserecordTemplates[$doserecordnumber]->setField("dosena", $dosenavalue, "B");
            }
            else { // @TODO: Hibaüzenet, érvénytelen mezőnév

            }
        }
        if ($doserecordTemplates != null)
        {
            // A dózis listához adjuk a rekordokat
            $doserecordContents = Template::merge($doserecordTemplates);
            //$logger->logInfo("doserecordContents");
            //$logger->logInfo($doserecordContents);
            $record->set("doselist", $doserecordContents);
        }
    }

    private static function createDosePrintField($record, $fieldname, $fieldvalue)
    {

    }

    public static function createAdverseEventSpecialPrintFields($record)
    {
        // title
        $title = _("Nemkívánatos Esemény");
        if ($record->isFieldExists("ae_serious"))
        {
            $serious = $record->get("ae_serious");
            if ($serious == 'value="1"')
            {
                $title = _("Súlyos Nemkívánatos Esemény");
            }
        }
        $parentid = $record->get("parentid");
        if (isset($parentid) && $parentid != null && $parentid != 0) {
            $title .= " - Follow-up";
        }
        $record->setField("title", $title, "V");

        // endDate - Az AE végét speciálisan kezeljük
        Study::createOngoingPrintField($record, "endDate");

        // firstAdministrationDate - Az alkalmazás kezdetét speciálisan kezeljük
        Study::createOngoingPrintField($record, "firstAdministrationDate");
    }

    private static function createOngoingPrintField($record, $datefieldname)
    {
        if ($record->isFieldExists($datefieldname))
        {
            $value = $record->get($datefieldname);
            if ($value == 'value="o.g."') {
                // A mező értékét töröljük
                $record->setField($datefieldname, "", "D");
                // Az ongoing-ot bekapcsoltra állítjuk
                $ogvalue = 1;
            }
            else {
                // Az ongoing-ot kikapcsoltra állítjuk
                $ogvalue = 0;
            }
            // Beállítjuk a ongoing checkbox értékét (a checkbox neve a mező neve+og)
            $record->setField($datefieldname."og", $ogvalue, "B");
        }
    }

    private static function createNotApplicablePrintField($record, $fieldname)
    {
        if ($record->isFieldExists($fieldname))
        {
            $value = $record->get($fieldname);
            if ($value == 'value="n.a."') {
                $record->setField($fieldname, "", "S");
                $navalue = 1;
            }
            else {
                $navalue = 0;
            }
            $record->setField($fieldname."na", $navalue, "B");
        }
    }

    public static function createMACCESpecialPrintFields($record)
    {
        // title
        $serious = $record->get("ae_serious");
        if ($serious == 'value="1"')
        {
            $title = _("Súlyos MACCE");
        }
        else
        {
            $title = _("MACCE");
        }
        $parentid = $record->get("parentid");
        if (isset($parentid) && $parentid != null && $parentid != 0) {
            $title .= " - Follow-up";
        }
        $record->setField("title", $title, "V");

        // endDate - Az AE végét speciálisan kezeljük
        Study::createOngoingPrintField($record, "endDate");

        // firstAdministrationDate - Az alkalmazás kezdetét speciálisan kezeljük
        Study::createOngoingPrintField($record, "firstAdministrationDate");

        // laborCk
        Study::createNotApplicablePrintField($record, "laborCk");

        // laborCkmb
        Study::createNotApplicablePrintField($record, "laborCkmb");

        // laborTroponinI
        Study::createNotApplicablePrintField($record, "laborTroponinI");

        // oldalagiStenozisAtmero
        Study::createNotApplicablePrintField($record, "oldalagiStenozisAtmero");

        // foagiStenozisAtmero
        Study::createNotApplicablePrintField($record, "foagiStenozisAtmero");
    }

    public static function createVisit1SpecialPrintFields($record)
    {
        // title
        $record->setField("title", _("Vizit 1"), "V");
    }

    public static function createVisit2SpecialPrintFields($record)
    {
        // title
        $record->setField("title", _("Vizit 2"), "V");
    }

    public static function createVisit3SpecialPrintFields($record)
    {
        // title
        $record->setField("title", _("Vizit 3"), "V");
    }

    public static function createVisit4SpecialPrintFields($record)
    {
        // title
        $record->setField("title", _("Vizsgálat vége oldal"), "V");
    }
 }
?>
