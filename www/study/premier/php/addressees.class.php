<?php
/**
 *---------------------------------------------------------------
 *addressees.class.php
 *
 * This file contains the Contacts class
 *
 * @package Kron
 * @subpackage addressees
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');

class Addressees
{
  	/**
     *  This function returns the name of the users table
     */
	public function getUsersViewName()
	{
		return DatabaseConfig::db_tableprefix . 'users';
	}
	 
	 /**
     *  This function returns the name of the users table
     */
	public function getUsersTableName()
	{
		return DatabaseConfig::db_tableprefix . 'users';
	}
	
	/**
     *  This function returns the name of the sites table
     */
	public function getSitesTableName()
	{
		return DatabaseConfig::db_tableprefix . 'sites';
	}
	
	/**
     *  This function returns the name of the countries table
     */
	public function getCountriesTableName()
	{
		return DatabaseConfig::db_tableprefix . 'countries';
	}
	
	
	public function getSiteUsers( $siteid, $db , $logger) {
		
		$response=Array();
		
		$sql = "SELECT name, usergroup, uiid, siteid FROM " . $this->getUsersViewName() . " WHERE siteid = ". $siteid ;
		$db->query($sql);		
		
		if($db->row_count() > 0)
			{
				for ($i=1; $i<=$db->row_count(); $i++)
	 			{
					$row = $db->fetch("assoc");
					if ($row != null)
					{
						$row['leaf']="true";
						$row['checked']="false"; 
						$row['issigner'] = UserManagement::checkUserPermissions($row['usergroup'],"Forms", "X_doSign");
						array_push($response,$row);
					}				
				}					
			}
	
		
		return $response; 
	}
	
	
	/** This function returns the contacts data
	 *  @remotable
	 */
	public function getAddressees()
    {
 		$logger = Logger::getInstance();
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		try
		{
			// fill up  countries 
			$countries = array();
			$db = new Database();
			$sql = "SELECT cid, country  FROM " . $this->getCountriesTableName()  ;
			$db->query($sql);		
			$country_count=$db->row_count();
			if($country_count > 0) {
				for ($i=1; $i<=$country_count; $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) { 
						array_push($countries,$row);
					}				
				}					
			}
			
			// fill up country_sites 	
			
			$country_sites = array();
			for ($j=0; $j<$country_count; $j++)
	 		{
				$sql = "SELECT sid, sitename FROM " . $this->getSitesTableName() . " WHERE countryid = " . $countries[$j]['cid']  ;
				$db->query($sql);		
				$site_count[$j]=$db->row_count();
				if($site_count[$j] > 0)	{
					$sites = array();
					for ($i=1; $i<=$site_count[$j]; $i++) {
						$row = $db->fetch("assoc");
						if ($row != null) { 
							array_push($sites,$row);
						}				
					}					
				}
				array_push($country_sites, $sites);
			}
		
			
			// fill up addressees response
			$addressees = array();
			
			//  users without sites siteid="0"
			$addressees= $this->getSiteUsers("0", $db, $logger); 
					
			//  users per country per sites	
			for ($j=0; $j<$country_count; $j++) {
	 			$countryChildren=Array();
				
				for ($k=0; $k<$site_count[$j]; $k++) {
					$siteRow=Array();
					$siteRow['name'] =  $country_sites[$j][$k]['sitename'];
					$siteRow['cls'] =  "folder";
					$siteRow['checked'] = "false";
					$siteRow['children'] =  $this->getSiteUsers($country_sites[$j][$k]['sid'], $db, $logger );
					
					array_push($countryChildren,$siteRow);
				}
				
				$countryRow=Array();
				$countryRow['name'] =  $countries[$j]['country'];
				$countryRow['cls'] =  "folder";
				$countryRow['expanded'] =  "true";
				$countryRow['checked'] = "false";
				$countryRow['children'] = $countryChildren;
				
				array_push( $addressees,$countryRow);
			}	
				
			$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getAddressees, Exception: $e");
		}

		return $addressees;
	}
 }
?>
