<?php
/**
 *---------------------------------------------------------------
 * patients.class.php
 *
 * This file contains the patients class functions
 *
 * @package Kron
 * @subpackage messages
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 * @todo:   filter input!!!!
 * @todo:   
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');

class Patients
{
	/**
     *  This function returns the name of the messages view
     */
	public function getFormsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'forms';
	}
	
	/**
     *  This function returns the name of the messages table
     */
	public function getFormsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'forms_ts';
	}

  	/**
     *  This function returns the name of the queries view
     */
	public function getPatientsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'patients';
	}
	
	/**
     *  This function returns the name of the queries table
     */
	public function getPatientsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'patients_ts';
	}

	 /**
     *  This function returns the name of the visits view
     */
	public function getVisitsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'visits';
	}
	 
	 /**
     *  This function returns the name of the visits db
     */
	public function getVisitsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'visits_ts';
	}
	
	
	
	 /**
     *  This function returns the name of the sites view
     */
	public function getSitesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'sites';
	}
	/**
     *  This function returns the name of the messages table
     */
	public function getFormFieldsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields_ts';
	}
	/**
     *  This function returns the name of the queries view
     */
	public function getQueriesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'queries';
	}
	/**
     *  This function returns a new patient identifier
     */
	public function generateNewPatientId($siteId, $siteIdName)
	{
		// TODO: most nics vizsgálva az ütközés, ha egyszerre ketten akarnak új pácienst
		$patientNumber = 1;
		try
		{
			$db = new Database();
			$sql = "SELECT id FROM " . $this->getPatientsViewName() . " WHERE siteid = ? " ;
			$db->query($sql, $siteId );
			$rowcount =	$db->row_count();
			$patientNumber = $rowcount+1;
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError($e);
		}
		$patientNumberAsText = (string)$patientNumber;
		$patientId = $siteIdName . substr('000' . $patientNumberAsText , -3);
		return $patientId;
	}

	/** This function returns the patients of a requested site (in case of monitor, sponsor, admin)
	 *  without input parameter siteid returns all patient related to user's site (in case of investigator, nurse)
	 *  @remotable
	 */
	public function getPatients(stdClass $params)
    {
    	$logger = Logger::getInstance();

    	$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$userGroup=isset($_SESSION['auth_userclass']) ? $_SESSION['auth_userclass'] : '';
		$siteId = isset($_SESSION['auth_siteid']) ? $_SESSION['auth_siteid'] : 0;
		$selectedSiteId=isset($_SESSION['auth_selected_siteid']) ? $_SESSION['auth_selected_siteid'] : 0 ;
		$selectedCountryId=isset($_SESSION['auth_selected_countryid']) ? $_SESSION['auth_selected_countryid'] : 0 ;
		//$logger->logInfo( 'getPatients called at ' . date("Y-m-d H:i:s"));
 		//$logger->logInfo($params);
		
		/* @TODO : jogosultság ellenőrzés user lĂˇthatja-e patient*/
		
		$success = false;
		if(ISSET($params->allForms)){
			$allForms = $params->allForms;
		} else {
			$allForms = false;
		}
		
		if ( $params->start ) {
			$offset = $params->start;
		} else {
			$offset = 0;
		}
			
		
		/* elavult comment:
		 * Minden patienthez a visit statusokat leszedni  nagyon erĹ‘forrĂˇsigĂ©nyes lene - inkĂˇbb bevezetek egy mezĹ‘t errĹ‘l userenkĂ©nt: visit_status
		 * ha kliens oldalon el lesz tĂˇrolva a visit-ek neve, akkor ez elĂ©g is lesz, hisz ebbĹ‘l a visitek szĂˇma is adĂłdik (string leght)  
		 * A next visit date-et is eltĂˇrolom,  - de ezt ellenĹ‘rzĂ¶m, Ă©s ha lejĂˇrt (< mai nap,  akkor frissĂ­tem azt is (= veszem a kĂ¶vetkezĹ‘t)
		 * de Ă­gy nem kell minden esetben kiszĂˇmolni - nagy patientszĂˇmnĂˇl klb ez is lassĂş lenne....
		*/
						
		$a = array();
		try
		{
			$db = new Database();
			if (UserManagement::userHasPermission("UserManagement", "changeSite")) {
	//		if ( $userGroup==FrameworkConfig::USERGROUP_MONITOR || $userGroup==FrameworkConfig::USERGROUP_SPONSOR || $userGroup==FrameworkConfig::USERGROUP_ADMIN ) {
				if 	( $selectedCountryId == 0 ) { // all countries' all sites' patients

					$sql = "SELECT p.id, p.siteid, p.patientid, p.status, p.isclosed, p.enrollmentdate, p.visit_status, COUNT(IF(q.status = ? OR q.status = ? , 1, NULL)) as openqueries, COUNT(IF(q.status = ? , 1, NULL)) as answeredqueries FROM " . $this->getSitesViewName() . " s"; 
					$sql = $sql . " INNER JOIN " . $this->getPatientsViewName() . " p ON p.siteid = s.sid INNER JOIN " . $this->getFormsViewName() . " f ON p.patientid = f.patientid ";
					$sql = $sql . " LEFT JOIN " . $this->getQueriesViewName() . " q ON q.form_instanceid = f.instanceid AND q.action = '" . FrameworkConfig::QUERYTYPE_QUERY . "' GROUP BY p.patientid ";
					$db->query($sql, FrameworkConfig::QUERYSTATUS_OPEN, FrameworkConfig::QUERYSTATUS_REOPENED, FrameworkConfig::QUERYSTATUS_ANSWERED);

				} else {
					if ( $selectedSiteId == 0 ) { // one country's all sites' patients

					$sql = "SELECT p.id, p.siteid, p.patientid, p.status, p.isclosed, p.enrollmentdate, p.visit_status, COUNT(IF(q.status = ? OR q.status = ?, 1, NULL)) as openqueries, COUNT(IF(q.status = ? , 1, NULL)) as answeredqueries FROM " . $this->getSitesViewName() . " s"; 
					$sql = $sql . " INNER JOIN " . $this->getPatientsViewName() . " p ON p.siteid = s.sid AND s.countryid = ? INNER JOIN " . $this->getFormsViewName() . " f ON p.patientid = f.patientid " ;
					$sql = $sql . " LEFT JOIN " . $this->getQueriesViewName() . " q ON q.form_instanceid = f.instanceid AND q.action = '" . FrameworkConfig::QUERYTYPE_QUERY . "' GROUP BY p.patientid ";
					$db->query($sql, FrameworkConfig::QUERYSTATUS_OPEN, FrameworkConfig::QUERYSTATUS_REOPENED, FrameworkConfig::QUERYSTATUS_ANSWERED, $selectedCountryId);
					
					} else { // one site's patients

						$sql = "SELECT p.id, p.siteid, p.patientid, p.status, p.isclosed, p.enrollmentdate, p.visit_status, COUNT(IF(q.status = ? OR q.status = ?, 1, NULL)) as openqueries, COUNT(IF(q.status = ? , 1, NULL)) as answeredqueries FROM " . $this->getSitesViewName() . " s"; 
						$sql = $sql . " INNER JOIN " . $this->getPatientsViewName() . " p ON p.siteid = s.sid AND p.siteid = ? INNER JOIN " . $this->getFormsViewName() . " f ON p.patientid = f.patientid ";
						$sql = $sql . " LEFT JOIN " . $this->getQueriesViewName() . " q ON q.form_instanceid = f.instanceid AND q.action = '" . FrameworkConfig::QUERYTYPE_QUERY . "' GROUP BY p.patientid ";
						$db->query($sql, FrameworkConfig::QUERYSTATUS_OPEN, FrameworkConfig::QUERYSTATUS_REOPENED, FrameworkConfig::QUERYSTATUS_ANSWERED, $selectedSiteId);

					}
				}
			} else // investigator, nurse => one site's patients
			{

				$sql = "SELECT p.id, p.siteid, p.patientid, p.status, p.isclosed, p.enrollmentdate, p.visit_status, COUNT(IF(q.status = ? OR q.status = ?, 1, NULL)) as openqueries, COUNT(IF(q.status = ? , 1, NULL)) as answeredqueries FROM " . $this->getSitesViewName() . " s"; 
				$sql = $sql . " INNER JOIN " . $this->getPatientsViewName() . " p ON p.siteid = s.sid AND p.siteid = ? INNER JOIN " . $this->getFormsViewName() . " f ON p.patientid = f.patientid ";
				$sql = $sql . " LEFT JOIN " . $this->getQueriesViewName() . " q ON q.form_instanceid = f.instanceid AND q.action = '" . FrameworkConfig::QUERYTYPE_QUERY . "' GROUP BY p.patientid ";
				$db->query($sql, FrameworkConfig::QUERYSTATUS_OPEN, FrameworkConfig::QUERYSTATUS_REOPENED, FrameworkConfig::QUERYSTATUS_ANSWERED, $siteId );

			}	
			
			$rowcount=	$db->row_count();	
			if( $rowcount > 0 ) {
				for ($i=1; $i<=$rowcount; $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) { 
						array_push($a,$row);
					}				
				}					
			}
	/*		foreach ($a as $key => $value) {
				$sql = "SELECT duedate, status, validity  FROM " . $this->getFormsViewName() . " WHERE formtype LIKE 'Visit%' AND patientid = ? ORDER BY formtype " ;
				$db->query($sql, $a[$key]['patientid'] );
				$rowcount=	$db->row_count();	
				$visitStatus='';
				if( $rowcount > 0)	{
					for ($i=1; $i<=$rowcount; $i++) {
						$row = $db->fetch("assoc");
						if ($row != null) {
							if ($row['validity'] == 'Y') {
								$visitStatus = $visitStatus . FrameworkConfig::VISITSTATUS_COMPLETED;
							} else if ($row['duedate'] != '' && $row['status'] == FrameworkConfig::VISITSTATUS_EMPTY) {
								if ( $db->dbdatetime_to_phpdatetime($row['duedate']) < time()) {
									$visitStatus = $visitStatus . FrameworkConfig::VISITSTATUS_MISSING;
								} else {
									$visitStatus = $visitStatus . $row['status'];
								}
							} else {
								$visitStatus = $visitStatus . $row['status'];
							}
						}				
					}			
				}

				$a[$key]['visit_status'] = $visitStatus;
				if ($allForms) { //  // nem csak a viziteket szedem le, hanem minden form típust

					$sql = "SELECT formtype, status, validity, created  FROM " . $this->getFormsViewName() . " WHERE formtype NOT LIKE 'Visit%' AND patientid = ? ORDER BY formtype, created " ;
					$db->query($sql, $a[$key]['patientid'] );
					$rowcount=	$db->row_count();	
					$visitStatuses=array();
					if( $rowcount > 0)	{
						for ($i=1; $i<=$rowcount; $i++) {
							$row = $db->fetch("assoc");
							if ($row != null) {
								if (!array_key_exists($row['formtype'], $visitStatuses)) {
									$visitStatuses[$row['formtype']] = "";
								}
								if ($row['validity'] == 'Y') {
									$visitStatuses[$row['formtype']] = $visitStatuses[$row['formtype']] . FrameworkConfig::VISITSTATUS_COMPLETED;
								} else {
									$visitStatuses[$row['formtype']] = $visitStatuses[$row['formtype']] . $row['status'];
								}
							}				
						}			
					}
					foreach ($visitStatuses as $key2 => $value2) {
						$a[$key][$key2] = $value2;				
					}
					
					
					
				}
			}	*/
			$success = true;		
			$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError($e);
		}
		//$logger->logInfo( Array('success' => $success, 'data' => $a));
		//$logger->logInfo( 'getPatients ended at ' . date("Y-m-d H:i:s"));
		return Array('success' => $success, 'data' => $a);
	}


	/** This function creates the patients and the mandatory visits 
	 *  @remotable
	 */
	public function addPatients(stdClass $params)
    {
		$logger = Logger::getInstance();
		
    	$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$userGroup=isset($_SESSION['auth_userclass']) ? $_SESSION['auth_userclass'] : '';

		$success = false;
		$errorMsg = '';
		$response= array();
	//	if ( $userGroup==FrameworkConfig::USERGROUP_INVESTIGATOR  || $userGroup==FrameworkConfig::USERGROUP_NURSE   )   {
		if ( UserManagement::userHasPermission("Patients", "addPatients"))   {
			$siteId = isset($_SESSION['auth_siteid']) ? $_SESSION['auth_siteid'] : '';
			if ($siteId > 0)	{
				
				try
				{
					
					$db = new Database();
		//		for ( $k = 1; $k<1000;$k++)	 {
				    $started = $db->phpdatetime_to_dbdatetime();
					$enrollmentDate = date("Y-m-d 00:00:00", strtotime($params->enrollmentDate));
					$enrollmentTime = strtotime($enrollmentDate);
					//$enrollmentTime = time();
					//$enrollmentDate = $db->phpdatetime_to_dbdatetime($enrollmentTime);

					$visitStatuses = "";
					for ($i=0; $i< FrameworkConfig::NUMBER_OF_VISITS; $i++) {
						$visitStatuses = $visitStatuses . FrameworkConfig::VISITSTATUS_EMPTY;
					}	
					
					// insert new row for new patient 
					$nextVisitDate= $db->phpdatetime_to_dbdatetime($enrollmentTime + FrameworkConfig::VISITDATE_OFFSET(0));
					$sql = "INSERT INTO " . $this->getPatientsTableName() . " ( siteid, status, enrollmentdate, visit_status, next_visit_date, start_ts, started_by ) VALUES (?,?,?,?,?,?,?)";
					$db->begin_transaction();
					$db->command($sql, $siteId, FrameworkConfig::PATIENTSTATUS_ONGOING, $enrollmentDate, $visitStatuses, $nextVisitDate, $started, $userId );
						
					$pid=$db->get_lastinsertid();
					
					// get the identifier of site
					$siteIdName = -1;
					
					$sql = "SELECT identifier FROM " . $this->getSitesViewName() . " WHERE sid = ? " ;
					$db->query($sql, $siteId );
					$rowcount=	$db->row_count();	
					if( $rowcount > 0)	{
						$row = $db->fetch("assoc");
						if ($row != null) {
							$siteIdName=$row['identifier'];
						}
					}
					if ( $siteIdName != null && $siteIdName != '') {
						// create new patient id
						$patientid = $this->generateNewPatientId($siteId, $siteIdName);
						
						// update patient record with this patientid
						$sql = "UPDATE " . $this->getPatientsTableName() . " SET patientid = ? WHERE id = ? ";
						$db->command($sql, $patientid, $pid);
				
						// - - - - -  create visit form records - - - - -
						$visitStatuses = '';
						for ($i=0; $i< FrameworkConfig::NUMBER_OF_VISITS; $i++) {
							
							$duedate = $db->phpdatetime_to_dbdatetime($enrollmentTime + FrameworkConfig::VISITDATE_OFFSET($i));
							$today  = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
							if ( $db->dbdatetime_to_phpdatetime($duedate) < $today) {
								$visitStatus =  FrameworkConfig::VISITSTATUS_MISSING;
							} else {
								$visitStatus = FrameworkConfig::VISITSTATUS_EMPTY;
							}
							$visitStatuses = $visitStatuses . $visitStatus;

							$sql = "INSERT INTO " . $this->getFormsTableName() . " ( formtype, patientid, duedate, status, istmp, created, start_ts, started_by ) VALUES (?,?,?,?,?,?,?,?)";
							$db->command($sql, FrameworkConfig::VISIT_NAMES($i), $patientid , $duedate , $visitStatus , 0, $started, $started, $userId );
							
							$lastinsertid=$db->get_lastinsertid();
			
							// new record inserted, therefore the instanceid = last inserted record id
							$sql = "UPDATE " . $this->getFormsTableName() . "  SET instanceid = ? WHERE id = ? ";
							$db->command($sql, $lastinsertid, $lastinsertid);
							// enrollment date-et bele kell tenni a visit1 'informDate' mezőjébe
							// ez így azért kicsit csúnyácska függés lett ...
							if ($i== 0)	{
								// inserts form fields data into the formfields table
								$sql = "INSERT INTO " . $this->getFormFieldsTableName() . " (form_instanceid, fieldname, fieldvalue, fieldtype, istmp, start_ts, started_by) VALUES (?,?,?,?,?,?,?)";
								$db->command($sql, $lastinsertid, 'informDate', $enrollmentDate, 'D' , 0, $started , $userId);
							}
								
						}
						// update patients record's visit status
						$sql = "UPDATE " . $this->getPatientsTableName() . " SET visit_status = ? WHERE id = ? ";
						$db->command($sql, $visitStatuses, $pid);
						
						
						$response = array("id"=>$pid, "siteid"=>$siteId, "patientid"=>$patientid, "status"=>FrameworkConfig::PATIENTSTATUS_ONGOING,"enrollmentdate"=>$params->enrollmentDate, "visit_status"=>$visitStatuses, "openqueries"=>0, "answeredqueries"=>0); 
						$success = true;		
		
					}
					$db->commit();
				//	}
					$db->close(); // Closes the cursor to free up memory
				}
				catch (Exception $e)
				{// Log the error
					$errorMsg = $e->getMessage(); 
					$logger->logError("Userid: $userId , Operation: addPatients, Exception: $e");
					$db->rollback();
					$success = false;
					
				}
			
			}
		}
						
		return Array('success' => $success, 'data' => $response, 'errorMsg' => $errorMsg);
	}
	
	
	/** Módosít egy Patinet rekordot
	 *  Azért kell, mert állítom a patient statusát 
	 *  Mást nem is updatel
	 * 
	 * 	Inputs:
	 * 	$params->patientid : patient id 
	 *  $params->* : all the form fields
	 * 	
	 *  Return array:
	 *  success: boolean
	 *  data :  input values including 
	 * 
	 *   @remotable
	 */
   	public  function updatePatient( $input)
   	{
    	$logger = Logger::getInstance();
		
		//$logger->logInfo(" updatePatient input:");	
		//$logger->logInfo($input);
		if (is_array($input) ) {
			$inputArray = $input;	
		} else {
			$inputArray = array($input);
		}
		
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$success = false;
		$errorMsg = '';
		try
		{ 
			$db = new Database();
			$update_ts = $db->phpdatetime_to_dbdatetime();	
			foreach ($inputArray as $key => $params) {
				if (property_exists($params, 'patientid')) {					
					$sql = "SELECT * FROM " . $this->getPatientsViewName() . " WHERE patientid = ? AND isclosed = '0' ";
					$db->begin_transaction();
					$db->query($sql, $params->patientid);
					if ( $db->row_count() > 0) {
						$row = $db->fetch("assoc");
						if ($row != null) {
							
							// eredetit törlöm, létrehozok egy új példányt az új adatokkal
							$sql = "UPDATE " . $this->getPatientsTableName() . " SET end_ts = ? , ended_by = ? WHERE patientid = ? AND isclosed = '0' ";
							$db->command($sql, $update_ts, $userId, $params->patientid);
							
							$sql = "INSERT INTO " . $this->getPatientsTableName() . " (siteid, patientid, status, enrollmentdate, visit_status, start_ts, started_by) VALUES (?,?,?,?,?,?,?)";
							$db->command($sql, $row['siteid'], $row['patientid'], $params->status, $row['enrollmentdate'], $row['visit_status'], $update_ts , $userId);
							
							$lastinsertid=$db->get_lastinsertid();
							$params->newid = $lastinsertid; // kell ez?

							$success = true;
						}
					} 
					$db->commit();
				}
			}	
			$db->close(); 
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: updatePatient, Exception: $e");
			$db->rollback();
			$success = false;
			$errorMsg = $e->getMessage(); 
		}	

		//$logger->logInfo(" updatePatient response:");	
		//$logger->logInfo(Array('success' => $success, 'data' => $inputArray));	
			
		return  Array('success' => $success, 'data' => $inputArray, 'errorMs	g' => $errorMsg);
   	}


	/** This function prints the patient tp PDF file 
	* 	Inputs:
	 * 	$params->patientid: the patient id
	 * 	
	 *  Return array:
	 *  success: boolean
	 *  filename : name of the generated temporary PDF file
	 *  @remotable
	 */
	public function printPatient(stdClass $params)
    {
		$logger = Logger::getInstance();
		
		$logger->logInfo("printPatient called with params:");	
		$logger->logInfo($params);
		
		$success = false;
		$tempfilename = '';
		$tempfile = '';
		$userId = isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : 'unknown userid';
		if ($params != null)
		{
			$patientid = isset($params->patientid) ? $params->patientid : null;
			if ($patientid !== null)
			{
				// @TODO: Ellenőrizni, hogy a felhasználónak van-e joga a pácienshez
				try
				{
					// Generating temporary filename
					$tempdir = PRINTOUTS_PATH;
					while (true) {
	 					$tempfilename = uniqid('print', true) . '.pdf';
						$tempfile = $tempdir . $tempfilename;
						if (!file_exists($tempfile)) break;
					}
					// Create pdf file
					$params->filename = $tempfile;
					$prn = new Printer;
					$prn->createPDFfromPatient($params);
					$prn->Close();
					$success = true;
				}
				catch (Exception $e)
				{// Log the error
					$logger->logError("Userid: $userId , Operation: Forms::printForm, Exception: $e");
				}
			}
			else {
				// A patientid paraméter nincs kitöltve
				// @TODO: az érvénytelen paraméter logolása
			}
		} //end if 
		else {
			// Null paraméterrel hívták meg a függvényt, ez lehet hiba, de lehet próbálkozás is
			// @TODO: a lehetséges próbálkozás logolása
		}
		return  Array('success' => $success, 'filename' => $tempfilename);
	}
}
?>
