<?php
/**
 *---------------------------------------------------------------
 *messages.class.php
 *
 * This file contains the Contacts class
 *
 * @package Kron
 * @subpackage messages
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 * @todo:
 *  filter input!!!!
 *  Hogyan kezeljük a túl sok címzettet (sok név lehet hosszú)
 *  Bevezetni limitett a szöveg hosszára (mennyi legyen ? varchar 2048 jó?)
 *  A html tag karaktereket ki kellene szűrni....
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');

class Messages
{
  	/**
     *  This function returns the name of the messages view
     */
	public function getMessagesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'messages';
	}
	
	/**
     *  This function returns the name of the messages table
     */
	public function getMessagesTableName()
	{
		return DatabaseConfig::db_tableprefix . 'messages_ts';
	}
	/**
     *  This function returns the name of the users view
     */
	public function getUsersViewName()
	{
		return DatabaseConfig::db_tableprefix . 'users';
	}
	
	 /**
     *  This function returns the name of the inbox view
     */
	public function getInboxViewName()
	{
		return DatabaseConfig::db_tableprefix . 'inbox';
	}
	
	/**
     *  This function returns the name of the inbox table
     */
	public function getInboxTableName()
	{
		return DatabaseConfig::db_tableprefix . 'inbox_ts';
	}

	
	/** This function returns the received messages
	 *  @remotable
	 */
	public function getReceivedMessages()
    {
    	$userId = isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';		
 		$logger = Logger::getInstance();
		
		$a = array();
		try
		{// Look for the user in the database
			$db = new Database();
			$sql = "SELECT  m.created, m.text, m.subject, m.author, m.addressees, i.status, i.iid FROM " . $this->getMessagesViewName() . " m INNER JOIN " .  $this->getInboxViewName() . " i ON m.msgid = i.msgid AND i.userid =" . $userId ;
			$db->query($sql);
			$rowcount=	$db->row_count();	
			if( $rowcount > 0)	{
				for ($i=1; $i<=$rowcount; $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) { 
						array_push($a,$row);
					}				
				}					
			}
			
		$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getReceivedMessages, Exception: $e");
		}
		return $a;
	}
	
	/** This function returns the sent messages
	 *  @remotable
	 */
	public function getSentMessages()
    {
    	$userId = isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
 		$logger = Logger::getInstance();
		
		$a = array();
		
		try
		{
			$db = new Database();
			$sql = "SELECT msgid, created, subject, text, addressees FROM " . $this->getMessagesViewName() . " WHERE authorid = " . $userId ;
			$db->query($sql);
			$rowcount=	$db->row_count();	
			if( $rowcount > 0) {
				for ($i=1; $i<=$rowcount; $i++) {
					$row = $db->fetch("assoc");
					if ($row != null) { 
						array_push($a,$row);
					}				
				}					
			}
			
			$db->close(); // Closes the cursor to free up memory
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: getSentMessages, Exception: $e");
		}
		return $a;
	}

	/** This postMessage function can be called as remote function,
	 *  Returns true on success, otherwise false.
	 *  @remotable
	 *  @todo 
	 */
   	public function postMessage(stdClass $params)
   	{
    	$logger = Logger::getInstance();
	 	$authorid = isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$userId = isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$success = false;
		
		try
		{ // get author user name
			$db = new Database();
			$params->created = $db->phpdatetime_to_dbdatetime();
			$params->author = "Not defined";
			$sql = "SELECT name FROM " . $this->getUsersViewName() . " WHERE uiid = ? ";
			$db->begin_transaction();
			$db->query($sql, $authorid);
			if( $db->row_count() > 0) {
				$row = $db->fetch("assoc");
				if ($row != null) { 
					$params->author= $row['name'];
				}	
			}
			
			// insert message into the database

						//$started= $db->phpdatetime_to_dbdatetime();
			$sql = "INSERT INTO " . $this->getMessagesTableName() . " (addressees, addresseeids, send_email, subject, text, created, authorid, author,  started_by) VALUES (?,?,?,?,?,?,?,?,?)";
			$db->command($sql, $params->addressees , $params->addresseeids , $params->send_email , $params->subject, $params->text, $params->created, $authorid, $params->author , $authorid);
			
			$params->msgid=$db->get_lastinsertid();
			
			// deliver messages
			
			$addresseeids=explode(",", $params->addresseeids);
			while (list($key, $val) = each($addresseeids)) {
			 	$started= $db->phpdatetime_to_dbdatetime();
			 	$sql = "INSERT INTO " . $this->getInboxTableName() . " (msgid, userid, status, started_by) VALUES (?,?,?,?)";
				$db->command($sql, $params->msgid , $val , "Unread", $authorid);
			}
			$db->commit();  
			$db->close(); 
			$success = true;
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: postMessage, Exception: $e");
			$db->rollback();
		}		
		return Array('success' => $success, 'data' => $params);
   	}

	/** This setRead function can be called as remote function,
	 *  Returns true on success, otherwise false.
	 *  @remotable
	 *  @todo 
	 */
   	public function setRead(stdClass $params)
   	{
    	$logger = Logger::getInstance();
	 	$userid = isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$success = false;
	
		try
		{ 
			$db = new Database();
			$sql = "UPDATE " . $this->getInboxTableName() . " SET status = ? WHERE iid = ? and userid = ?";
			$db->command($sql, "Read", $params->iid, $userid);
			
			$db->close(); 
			$success = true;
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: setRead, Exception: $e");
		}		
		return Array('success' => $success, 'data' => $params);
   	}

	/** This deleteMessages function can be called as remote function,
	 *  Returns true on success, otherwise false.
	 *  @remotable
	 *  @todo 
	 */
   	public function deleteMessages(stdClass $params)
   	{
    	$logger = Logger::getInstance();
	 	$userid = isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$success = false;
			
		try
		{ 
			$db = new Database();
			$db->begin_transaction();
			// delete inbox items (received messages)
			while (list($key, $val) = each($params->inboxids)) {
			 	$ended = $db->phpdatetime_to_dbdatetime();
			 	$sql = "UPDATE " . $this->getInboxTableName() . " SET end_ts = ? , ended_by = ? WHERE iid = ? and userid = ?";
				$db->command($sql, $ended, $userid, $val, $userid );
			}
			
			// delete messages items (sent messages)
			while (list($key, $val) = each($params->msgids)) {
			 	//$logger->logInfo(" $key => $val ");	
			 	$ended = $db->phpdatetime_to_dbdatetime();
			 	$sql = "UPDATE " . $this->getMessagesTableName() . " SET end_ts = ? , ended_by = ? WHERE msgid = ? and authorid = ?";
				$db->command($sql, $ended, $userid, $val, $userid);
			 	
			 	// also delete message references from  inbox
				$sql = "UPDATE " . $this->getInboxTableName() . " SET end_ts = ? , ended_by = ? WHERE msgid = ? ";
				$db->command($sql, $ended, $userid, $val );
			}
			$db->commit();	  
			$db->close(); // Closes the cursor to free up memory
			$success = true;
		}
		catch (Exception $e)
		{// Log the error
			$logger->logError("Userid: $userId , Operation: deleteMessages, Exception: $e");
			$db->rollback();
		}		
		return Array('success' => $success, 'data' => $params);
   	}

 }
?>
