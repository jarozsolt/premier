<?php
/**
 *---------------------------------------------------------------
 * Logger.php
 *
 * This file contains the Logger class
 *
 * @package Kron
 * @subpackage log
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 * @todo Error correction: It creates an empty log file, if nothing to log.
 *---------------------------------------------------------------
 */

// Load the framework configuration file
require_once('app.conf');

require_once('firephp.class.php');


/**
 * Logger class
 *
 * general logging class
 *
 * @package Kron
 * @subpackage log
 * @todo Add rolling logfile handling
 * @todo Add filename and line number handling
 * @todo Add exception handling
 */
class Logger
{
	/**
	 * Error severity, from low to high.
	 *
	 */
    const SEVERITY_ERROR =		0;		// Error: error conditions
    const SEVERITY_WARNING =	1;		// Warning: warning conditions
    const SEVERITY_INFO =		2;		// Informational: informational messages
    const SEVERITY_LOG =		3;		// Log: normal log messages
    const SEVERITY_DEBUG =		4;		// Debug: debug messages
    const OFF =					5;		// Off: log nothing at all
	
    
	/**
	 * Gets singleton instance of Logger
	 *
	 * @param boolean $AutoCreate
	 * @return Logger
	 */
    public static function getInstance($AutoCreate = true)
    {
        if ($AutoCreate === true && !self::$instance)
        {
            self::init();
        }
        return self::$instance;
    }
    
	/**
     * Creates Logger object and stores it for singleton access
     *
     * @return Logger
     */
    public static function init()
    {
        return self::setInstance(new self());
    }

    /**
     * Set the instance of the Logger singleton
     * 
     * @param Logger $instance The Logger object instance
     * @return Logger
     */
    public static function setInstance($instance)
    {
        return self::$instance = $instance;
    }
    
	/**
	 * Internal status codes
	 */
    const STATUS_LOG_OPEN = 	1; // The log file opened
    const STATUS_OPEN_FAILED = 	2; // Failed to open log file
    const STATUS_LOG_CLOSED = 	3; // The log file closed
    
    /**
	 * Returns (and removes) the last message from the queue.
	 * @return string
	 */
    public function getMessage()
    {
        return array_pop($this->_messageQueue);
    }

    /**
	 * Returns the entire message queue (leaving it intact)
	 * @return array
	 */
    public function getMessages()
    {
        return $this->_messageQueue;
    }

    /**
	 * Empties the message queue
	 * @return void
	 */
    public function clearMessages()
    {
        $this->_messageQueue = array();
    }
    
	/**
	 * Octal notation for default permissions of the log file
	 * @var integer
	 */
    private static $_defaultPermissions = 0777;
    
    /**
     * Register Logger as your error handler
     * 
     * Will throw exceptions for each php error.
     * 
     * @return mixed Returns a string containing the previously defined error handler (if any)
     */
    public function registerErrorHandler($throwErrorExceptions = false)
    {
        //NOTE: The following errors will not be caught by this error handler:
        //      E_ERROR, E_PARSE, E_CORE_ERROR,
        //      E_CORE_WARNING, E_COMPILE_ERROR,
        //      E_COMPILE_WARNING, E_STRICT
    
        $this->throwErrorExceptions = $throwErrorExceptions;
    
        return set_error_handler(array($this,'errorHandler'));     
    }
    
	/**
     * Register Logger as your exception handler
     * 
     * @return mixed Returns the name of the previously defined exception handler,
     *               or NULL on error.
     *               If no previous handler was defined, NULL is also returned.
     */
    public function registerExceptionHandler()
    {
        return set_exception_handler(array($this,'exceptionHandler'));     
    }
    
	/**
     * Register Logger driver as your assert callback
     * 
     * @param boolean $convertAssertionErrorsToExceptions
     * @param boolean $throwAssertionExceptions
     * @return mixed Returns the original setting or FALSE on errors
     */
    public function registerAssertionHandler($convertAssertionErrorsToExceptions = true, $throwAssertionExceptions = false)
    {
        $this->convertAssertionErrorsToExceptions = $convertAssertionErrorsToExceptions;
        $this->throwAssertionExceptions = $throwAssertionExceptions;
        
        if ($throwAssertionExceptions && !$convertAssertionErrorsToExceptions)
        {
            throw $this->newException('Cannot throw assertion exceptions as assertion errors are not being converted to exceptions!');
        }
        
        return assert_options(ASSERT_CALLBACK, array($this, 'assertionHandler'));
    }
    
    
	/**
	 * Class constructor
	 *
	 * @return void
	 */
    public function __construct()
    {
    	// In debug mode set the severity treshold to DEBUG
    	if (FrameworkConfig::debug_mode)
    	{
    		$this->_severityThreshold = self::SEVERITY_DEBUG;
    	}
    	
    	if (FrameworkConfig::debug_mode && LoggerConfig::use_firephp)
    	{
			$firephp = FirePHP::getInstance(true);
			$firephp->setEnabled(true);
			$firephp->registerErrorHandler();
			$firephp->registerExceptionHandler();
			$firephp->registerAssertionHandler();
    	}
    	else
    	{
    		$firephp = FirePHP::getInstance(true);
    		$firephp->setEnabled(false);
	    	$logDirectory = BASE_STUDY_PATH . rtrim(LoggerConfig::log_dir, '\\/');
	        $logDirectory = rtrim($logDirectory, '\\/');
	
	        $this->_logFilePath = $logDirectory . '/' . LoggerConfig::log_prefix . date('Y-m-d') . '.txt';

	        if (!file_exists($logDirectory))
	        {
	            mkdir($logDirectory, self::$_defaultPermissions, true);
	        }
	
	        if (file_exists($this->_logFilePath) && !is_writable($this->_logFilePath))
	        {
	            $this->_logStatus = self::STATUS_OPEN_FAILED;
	            $this->_messageQueue[] = $this->_messages['writefail'];
	            return;
	        }
	
	        if (($this->_fileHandle = fopen($this->_logFilePath, 'a')))
	        {
	            $this->_logStatus = self::STATUS_LOG_OPEN;
	            $this->_messageQueue[] = $this->_messages['opensuccess'];
	        }
	        else
	        {
	            $this->_logStatus = self::STATUS_OPEN_FAILED;
	            $this->_messageQueue[] = $this->_messages['openfail'];
	        }
	        if ($this->_logStatus === self::STATUS_LOG_OPEN)
	        {
	        	$this->registerErrorHandler();
				$this->registerExceptionHandler();
				$this->registerAssertionHandler();
	        }
    	}
	        
    }
    
    /**
	 * Class destructor
	 */
    public function __destruct()
    {
        if ($this->_fileHandle)
        {
            fclose($this->_fileHandle);
        }
    }
    
	/**
     * Logger's error handler
     * 
     * Throws exception for each php error that will occur.
     *
     * @param int $errno
     * @param string $errstr
     * @param string $errfile
     * @param int $errline
     * @param array $errcontext
     */
    public function errorHandler($errno, $errstr, $errfile, $errline, $errcontext)
    {
        // Don't throw exception if error reporting is switched off
        if (error_reporting() == 0)
        {
            return;
        }
        // Only throw exceptions for errors we are asking for
        if (error_reporting() & $errno)
        {

            $exception = new ErrorException($errstr, 0, $errno, $errfile, $errline);
            if ($this->throwErrorExceptions)
            {
                throw $exception;
            }
            else
            {
                $this->logError($exception);
            }
        }
    }
 
  
    /**
     * Logger's exception handler
     * 
     * Logs all exceptions to your firebug console and then stops the script.
     *
     * @param Exception $Exception
     * @throws Exception
     */
    function exceptionHandler($Exception)
    {
    
        $this->inExceptionHandler = true;
    
        header('HTTP/1.1 500 Internal Server Error');
    
        try
        {
            $this->logError($Exception);
        }
        catch (Exception $e)
        {
            echo 'We had an exception: ' . $e;
        }
        $this->inExceptionHandler = false;
    }
    
  
    /**
     * Logger's assertion handler
     *
     * Logs all assertions to your firebug console and then stops the script.
     *
     * @param string $file File source of assertion
     * @param int    $line Line source of assertion
     * @param mixed  $code Assertion code
     */
    public function assertionHandler($file, $line, $code)
    {
        if ($this->convertAssertionErrorsToExceptions)
        {
          
          $exception = new ErrorException('Assertion Failed - Code[ '.$code.' ]', 0, null, $file, $line);
    
          if ($this->throwAssertionExceptions)
          {
              throw $exception;
          }
          else
          {
              $this->logError($exception);
          }
        
        }
        else
        {
            $this->logError($code, 'Assertion Failed', FirePHP::ERROR, array('File'=>$file,'Line'=>$line));
        }
    }
    
    
    /**
	 * Sets the date format used by the Logger
	 *
	 * @param string $dateFormat Valid format string for date()
	 */
    public static function setDateFormat($dateFormat)
    {
        self::$_dateFormat = $dateFormat;
    }
    
    /**
	 * Writes a $line to the log with a severity level of DEBUG
	 *
	 * @param string $line Information to log
	 * @return void
	 */
    public function logDebug($line)
    {
    	if (FrameworkConfig::debug_mode)
    	{
    		$severity = self::SEVERITY_DEBUG;
	    	if ($this->_severityThreshold >= $severity)
	        {
				if (LoggerConfig::use_firephp)
	    		{
	    			if ($this->_severityThreshold != self::OFF)
	    			{
						$firephp = FirePHP::getInstance(true);
						$firephp->log($line);
	    			}
	    		}
	    		else
	    		{							
					$this->log($line, $severity);
	    		}
	        }
    	}
    }
    
    /**
	 * Writes a $line to the log with a severity level of INFO. Generally
	 * corresponds to E_STRICT, E_NOTICE, or E_USER_NOTICE errors
	 *
	 * @param string $line Information to log
	 * @return void
	 */
    public function logInfo($line)
    {
    	$severity = self::SEVERITY_INFO;
    	if ($this->_severityThreshold >= $severity)
        {
	    	if (FrameworkConfig::debug_mode && LoggerConfig::use_firephp)
	    	{
	    		if ($this->_severityThreshold != self::OFF)
	    		{
					$firephp = FirePHP::getInstance(true);
					$firephp->info($line);
	    		}
			}
			else
			{							
				$this->log($line, $severity);
			}
        }
    }

	/**
	 * Writes a $line to the log with a severity level of LOG. Any information
	 * can be used here, or it could be used with E_STRICT errors
	 *
	 * @param string $line Information to log
	 * @return void
	 */
    public function logLog($line)
    {
    	$severity = self::SEVERITY_LOG;
    	if ($this->_severityThreshold >= $severity)
        {
	    	if (FrameworkConfig::debug_mode && LoggerConfig::use_firephp)
	    	{
	    		if ($this->_severityThreshold != self::OFF)
	    		{
					$firephp = FirePHP::getInstance(true);
					$firephp->log($line);
	    		}
			}
			else
			{							
				$this->log($line, $severity);
			}
        }
    }
    
	/**
	 * Writes a $line to the log with a severity level of WARN. Generally
	 * corresponds to E_WARNING, E_USER_WARNING, E_CORE_WARNING, or
	 * E_COMPILE_WARNING
	 *
	 * @param string $line Information to log
	 * @return void
	 */
    public function logWarn($line)
    {
    	$severity = self::SEVERITY_WARNING;
    	if ($this->_severityThreshold >= $severity)
        {
	    	if (FrameworkConfig::debug_mode && LoggerConfig::use_firephp)
	    	{
	    		if ($this->_severityThreshold != self::OFF)
	    		{
					$firephp = FirePHP::getInstance(true);
					$firephp->warn($line);
	    		}
			}
			else
			{							
				$this->log($line, $severity);
			}
        }
    }

	/**
	 * Writes a $line to the log with a severity level of ERR. Most likely used
	 * with E_RECOVERABLE_ERROR
	 *
	 * @param mixed $object The variable to be logged
	 * @return void
	 */
    public function logError($object)
    {
    	$severity = self::SEVERITY_ERROR;
    	if ($this->_severityThreshold >= $severity)
        {
	    	if (FrameworkConfig::debug_mode && LoggerConfig::use_firephp)
	    	{
	    		if ($this->_severityThreshold != self::OFF)
	    		{
					$firephp = FirePHP::getInstance(true);
					$firephp->error($object);
	    		}
			}
			else
			{
				if ($this->headersSent($filename, $linenum))
				{// If we are logging from within the exception handler we cannot throw another exception
            		if ($this->inExceptionHandler)
            		{// Simply echo the error out to the page
                		echo '<div style="border: 2px solid red; font-family: Arial; font-size: 12px; background-color: lightgray; padding: 5px;"><span style="color: red; font-weight: bold;">FirePHP ERROR:</span> Headers already sent in <b>'.$filename.'</b> on line <b>'.$linenum.'</b>. Cannot send log data to FirePHP. You must have Output Buffering enabled via ob_start() or output_buffering ini directive.</div>';
            		}
            		else
            		{
                		throw $this->newException('Headers already sent in '.$filename.' on line '.$linenum.'. Cannot send log data to FirePHP. You must have Output Buffering enabled via ob_start() or output_buffering ini directive.');
            		}
        		}							
				$this->log($object, $severity);
			}
        }
    }
    
	/**
	 * Writes a permission error to the log with a severity level of ERR.
	 *
	 * @param stdClass $params The variable to be logged
	 * 		'function' - function reporting the error,
	 * 		'permissionname' - name of the missing permission,
	 * 		'permissionaction' - action of the missing permission,
	 * @return void
	 */
    public function logPermissionError(stdClass $params)
    {
		$action = isset($params->action) ? $params->action : '';
		$method = isset($params->method) ? $params->method : '';
		$ipaddress = $_SERVER['REMOTE_ADDR'];
		$username = $_SESSION['auth_username'];
    	$this->logError("The user has no permission for '$action::$method'. ip='$ipaddress' username='$username'");
    }
    
	/**
	 * Writes a $line to the log with the given severity
	 *
	 * @param mixed $object The variable to be logged
	 * @param integer $severity Severity level of log message (use constants)
	 */
    public function log($object, $severity)
    {
        if ($this->_severityThreshold >= $severity)
        {
            $status = $this->_getTimeLine($severity);
            
         	if ($this->headersSent($filename, $linenum))
         	{// If we are logging from within the exception handler we cannot throw another exception
            	if ($this->inExceptionHandler)
            	{// Simply echo the error out to the page
                	echo '<div style="border: 2px solid red; font-family: Arial; font-size: 12px; background-color: lightgray; padding: 5px;"><span style="color: red; font-weight: bold;">FirePHP ERROR:</span> Headers already sent in <b>'.$filename.'</b> on line <b>'.$linenum.'</b>. Cannot send log data to FirePHP. You must have Output Buffering enabled via ob_start() or output_buffering ini directive.</div>';
            	}
            	else
            	{
                	throw $this->newException('Headers already sent in '.$filename.' on line '.$linenum.'. Cannot send log data to FirePHP. You must have Output Buffering enabled via ob_start() or output_buffering ini directive.');
            	}
        	}
         	if ($object instanceof Exception)
         	{
         	 	if (FrameworkConfig::debug_mode && LoggerConfig::write_trace)
                {
                	$this->writeFreeFormLine("$status $object \n");
                }
                else
                {
	            	$trace = $object->getTrace();
	            	if ($object instanceof ErrorException && isset($trace[0]['function'])
	               		&& $trace[0]['function']=='errorHandler' && isset($trace[0]['class'])
	               		&& $trace[0]['class']=='Logger')
	               	{
						$severity = false;
		                switch($object->getSeverity())
		                {
		                    case E_WARNING: $severity = 'E_WARNING'; break;
		                    case E_NOTICE: $severity = 'E_NOTICE'; break;
		                    case E_USER_ERROR: $severity = 'E_USER_ERROR'; break;
		                    case E_USER_WARNING: $severity = 'E_USER_WARNING'; break;
		                    case E_USER_NOTICE: $severity = 'E_USER_NOTICE'; break;
		                    case E_STRICT: $severity = 'E_STRICT'; break;
		                    case E_RECOVERABLE_ERROR: $severity = 'E_RECOVERABLE_ERROR'; break;
		                    case E_DEPRECATED: $severity = 'E_DEPRECATED'; break;
		                    case E_USER_DEPRECATED: $severity = 'E_USER_DEPRECATED'; break;
		                }
	                   
		                $object = array('Class'=>get_class($object),
		                                'Message'=>$severity.': '.$object->getMessage(),
		                                'File'=>$this->_escapeTraceFile($object->getFile()),
		                                'Line'=>$object->getLine(),
		                                'Type'=>'trigger',
		                                'Trace'=>$this->_escapeTrace(array_splice($trace,2)));
					}
					else
	            	{
	                	$object = array('Class'=>get_class($object),
	                                'Message'=>$object->getMessage(),
	                                'File'=>$this->_escapeTraceFile($object->getFile()),
	                                'Line'=>$object->getLine(),
	                                'Type'=>'throw',
	                                'Trace'=>$this->_escapeTrace($trace));
	            	}
	          		$line = $status . " exception '" . $object['Class'] . "' with message '" . $object['Message'] .
	          				"' in " . $object['File']  . ":" .  $object['Line'] . "\n";
	                $this->writeFreeFormLine($line);
                }
        	}
        	else
        	{
            	$this->writeFreeFormLine("$status $object \n");
        	}
        }
    }
    
    
    /**
	 * Writes a line to the log without prepending a status or timestamp
	 *
	 * @param string $line Line to write to the log
	 * @return void
	 */
    public function writeFreeFormLine($line)
    {
        if ($this->_logStatus == self::STATUS_LOG_OPEN && $this->_severityThreshold != self::OFF)
        {
            if (fwrite($this->_fileHandle, $line) === false)
            {
                $this->_messageQueue[] = $this->_messages['writefail'];
            }
        }
    }
    
	/**
     * Escape trace path for windows systems
     *
     * @param array $Trace
     * @return array
     */
    protected function _escapeTrace($Trace)
    {
        if (!$Trace) return $Trace;
        for( $i=0 ; $i<sizeof($Trace) ; $i++ ) {
            if (isset($Trace[$i]['file'])) {
                $Trace[$i]['file'] = $this->_escapeTraceFile($Trace[$i]['file']);
            }
            /*if (isset($Trace[$i]['args'])) {
                $Trace[$i]['args'] = $this->encodeObject($Trace[$i]['args']);
            }*/
        }
        return $Trace;    
    }
  
    /**
     * Escape file information of trace for windows systems
     *
     * @param string $File
     * @return string
     */
    protected function _escapeTraceFile($File)
    {
        /* Check if we have a windows filepath */
        if (strpos($File,'\\')) {
            /* First strip down to single \ */
          
            $file = preg_replace('/\\\\+/','\\',$File);
          
            return $file;
        }
        return $File;
    }
    
 	/**
     * Check if headers have already been sent
     *
     * @param string $Filename
     * @param integer $Linenum
     */
    protected function headersSent(&$Filename, &$Linenum)
    {
        return headers_sent($Filename, $Linenum);
    }
    
	/**
     * Flag whether we are logging from within the exception handler
     * 
     * @var boolean
     */
    protected $inExceptionHandler = false;
    
    /**
     * Flag whether to throw PHP errors that have been converted to ErrorExceptions
     * 
     * @var boolean
     */
    protected $throwErrorExceptions = true;
    
    /**
     * Flag whether to convert PHP assertion errors to Exceptions
     * 
     * @var boolean
     */
    protected $convertAssertionErrorsToExceptions = true;
    
    /**
     * Flag whether to throw PHP assertion errors that have been converted to Exceptions
     * 
     * @var boolean
     */
    protected $throwAssertionExceptions = false;
    
	/**
	 * Standard messages produced by the class. Can be modified for il8n
	 * @var array
	 */
    private $_messages = array
    (
        'writefail' => 'The file could not be written to. Check that appropriate permissions have been set.',
        'opensuccess' => 'The log file was opened successfully.',
        'openfail' => 'The file could not be opened. Check permissions.',
    );
    
	/**
	 * Default severity of log messages, if not specified
	 * @var integer
	 */
    private static $_defaultSeverity = self::SEVERITY_DEBUG;
    /**
    
	/**
	 * Holds messages generated by the class
	 * @var array
	 */
    private $_messageQueue = array();
    
    /** This holds the file handle for the log file
	 * @var resource
	 */
    private $_fileHandle = null;
    
	/**
	 * Path to the log file
	 * @var string
	 */
    private $_logFilePath = null;
    
	/**
	 * Current status of the log file
	 * @var integer
	 */
    private $_logStatus = self::STATUS_LOG_CLOSED;
    
    /** Valid PHP date() format string for log timestamps
	 * @var string
	 */
    private static $_dateFormat = 'Y-m-d G:i:s';
    
	/**
	 * Singleton instance of Logger
	 *
	 * @var Logger
	 */
	protected static $instance = null;
    
    /**
	 * Current minimum logging threshold
	 * @var integer
	 */
    private $_severityThreshold = self::SEVERITY_INFO;

    
	private function _getTimeLine($level)
    {
        $time = date(self::$_dateFormat);

        switch ($level)
        {
            case self::SEVERITY_LOG:
                return "$time - LOG   -->";
            case self::SEVERITY_INFO:
                return "$time - INFO  -->";
            case self::SEVERITY_WARNING:
                return "$time - WARN  -->";
            case self::SEVERITY_DEBUG:
                return "$time - DEBUG -->";
            case self::SEVERITY_ERROR:
                return "$time - ERROR -->";
            default:
                return "$time - LOG   -->";
        }
    }

}
?>