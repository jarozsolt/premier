<?php
/**
 *---------------------------------------------------------------
 * printer.class.php
 *
 * This file contains the Printer class
 *
 * @package Kron
 * @subpackage printer
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');
require_once('study.class.php');
require_once('template.class.php');
require_once("print/dompdf_config.inc.php");

class Printer
{

	public function __construct()
	{
	}

	public function Close()
	{
		//cleanPrintoutsFolder();
	}

	/**
     *  This function deletes the old unused pdf printouts
     */
	private function cleanPrintoutsFolder()
	{

		// Filetypes to check (you can also use *.*)
		$fileTypes = '*.pdf';

		// Here you can define after how many
		// minutes the files should get deleted
		$expire_time = 20;

		// Find all files of the given file type
		foreach (PRINTOUTS_PATH . $fileTypes as $Filename) {

		    // Read file creation time
		    $FileCreationTime = filectime($Filename);

		    // Calculate file age in seconds
		    $FileAge = time() - $FileCreationTime;

		    // Is the file older than the given time span?
		    if ($FileAge > ($expire_time * 60)){
		        // Delete file
		        unlink($Filename);
		    }

		}
	}

    /** create a header template from form template
     *  Inputs:
     *  $form: the form template
     *
     *  Return the header template
     *
     */
    private function createHeaderforForm($formtemplate)
    {
        $headertemplatename = 'print/templates/formheader.tpl';
        $header = new Template($headertemplatename);

        if (isset($formtemplate)) {
            if ($formtemplate->isFieldExists("title"))
            {
                $title = $formtemplate->get("title");
                if (isset($title) && $title != null && $title != '') {
                    // Vissza kell kódolni
                    $decodedtitle = html_entity_decode($title, ENT_QUOTES, "UTF-8");
                    $header->setField("headertitle", $decodedtitle, "V");
                }
            }

            if ($formtemplate->isFieldExists("patientid"))
            {
                $patientid = $formtemplate->get("patientid");
                if (isset($patientid) && $patientid != null && $patientid != '') {
                    // Vissza kell kódolni
                    $decodedpatientid = html_entity_decode($patientid, ENT_QUOTES, "UTF-8");
                    $header->setField("patientid", $decodedpatientid, "V");
                }
            }

            if ($formtemplate->isFieldExists("siteid"))
            {
                $siteid = $formtemplate->get("siteid");
                if (isset($siteid) && $siteid != null && $siteid != '') {
                    // Vissza kell kódolni
                    $decodedsiteid = html_entity_decode($siteid, ENT_QUOTES, "UTF-8");
                    $header->setField("siteid", $decodedsiteid, "V");
                }
            }

            if ($formtemplate->isFieldExists("formtime"))
            {
                $formtime = $formtemplate->get("formtime");
                if (isset($formtime) && $formtime != null && $formtime != '') {
                    // Vissza kell kódolni
                    $decodedformtime = html_entity_decode($formtime, ENT_QUOTES, "UTF-8");
                    $header->setField("formtime", $decodedformtime, "V");
                }
            }
        }
        $header->setField("studyname", FrameworkConfig::STUDY_NAME, "V");
        return $header;
    }

        /** create a template from form
     *  Inputs:
     *  $params->formid: the form instance id
     *
     *  Return a boolean
     *
     */
    private function createTemplatefromForm($formid)
    {
        $logger = Logger::getInstance();

        // @TODO Ellenőrizni a formid változót

        //$form = null;
        $patientid = '';
        $siteid = '';
        $formtype = '';
        $parentid = 0;
        // Beállítjuk a form nyomtatásának idejét
        date_default_timezone_set("Europe/Budapest");
        $formtime = date("Y-m-d H:i:s", time());

        $template = null;

        if ($formid != '' )
        {
            // Lekérdezzük a nyomtatandó formra vonatkozó adatokat: formtype, patientid, parentid
            try
            {
                $db = new Database();
                $sql = "SELECT *  FROM " . Database::getFormsViewName() . " WHERE instanceid='" . $formid . "' AND istmp='0'";
                $db->query($sql);
                if ($db->row_count() > 0)
                {
                    $row = $db->fetch("assoc");
                    if ($row != null) {
                        $patientid = $row['patientid'];
                        $siteid = substr($patientid, 0, 4);
                        $formtype = $row['formtype'];
                        $parentid = $row['parentid'];
                    }
                }
                $db->close(); // Closes the cursor to free up memory
            }
            catch (Exception $e)
            {// Log the error
                $logger->logError("Userid: $userId , Operation: Printer::createPDFfromForm, Exception: $e");
            }
            if (isset($formtype))
            {

                // Betöltjük a template-et
                $templatename = 'print/templates/'. strtolower($formtype) . 'record.tpl';
                if (!file_exists($templatename)) {
                    // Error: The template file not exists
                    $logger->logError("Userid: $userId , Operation: Printer::createPDFfromForm, Template file not exists: $templatename");
                }
                else {
                    $template = new Template($templatename);

                    // Beolvassuk a rekord változókat az adatbázisból
                    $db = new Database();
                    // Ez azért kellett, mert ugyanaz a formfield több értékkel is szerepel az adatbázisban.
                    // Remélhetőleg az automentés kiszedése ezt a problémát majd megszünteti
                    $sql = "SELECT * FROM " . Database::getFormFieldsViewName() . " WHERE id IN (SELECT MAX(id) FROM " . Database::getFormFieldsViewName() . " WHERE form_instanceid='" . $formid . "' AND istmp='0' GROUP BY fieldname)";
                    //$sql = "SELECT fieldname, fieldvalue, fieldtype  FROM " . Database::getFormFieldsViewName() . " WHERE form_instanceid='" . $formid . "' AND istmp='0' ORDER BY id GROUP BY fieldname";
                    $db->query($sql);
                    if ($db->row_count() > 0)
                    {
                        for ($i = 1; $i <= $db->row_count(); $i++)
                        {
                            $row = $db->fetch("assoc");
                            if ($row != null) {
                                $template->setField($row['fieldname'], $row['fieldvalue'], $row['fieldtype']);
                            }
                        }
                    }

                    // Beállítjuk a különleges változókat a formban
                    /*$form->setField("formid", $formid, "V");
                    $form->setField("parentid", $parentid, "V");
                    $form->setField("formtype", $formtype, "V");
                    $form->setField("patientid", $patientid, "V");
                    $form->setField("siteid", $siteid, "V");
                    $form->setField("studyname", FrameworkConfig::STUDY_NAME, "V");
                    $form->setField("formtime", $formtime, "V");*/

                    // Beállítjuk a különleges változókat a template-ben
                    $template->setField("formid", $formid, "V");
                    $template->setField("parentid", $parentid, "V");
                    $template->setField("formtype", $formtype, "V");
                    $template->setField("patientid", $patientid, "V");
                    $template->setField("siteid", $siteid, "V");
                    $template->setField("studyname", FrameworkConfig::STUDY_NAME, "V");
                    $template->setField("formtime", $formtime, "V");

                    // Legeneráljuk a különleges változókat a rekordban
                    // Ez study specifikus, ezért a Study osztályban van megírva
                    Study::createSpecialPrintFields($template);

                    // Ha a rekord tudott 'title'-t adni azt beállítjuk a formra is.
                    /*if ($record->isFieldExists("title"))
                    {
                        $title = $record->get("title");
                        if (isset($title) && $title != null && $title != '') {
                            // Vissza kell kódolni
                            $decodedtitle = html_entity_decode($title, ENT_QUOTES, "UTF-8");
                            $this->setFormTitle($form, $decodedtitle);
                        }
                    }

                    // Behelyetesítjük a rekordot a formban
                    $recordContent = Template::merge($recordTemplate);
                    $form->set("record", $recordContent);*/
                }
            }
            else
            {
                $logger->logError("Userid: $userId , Operation: Printer::createPDFfromForm, Missing formtype");
            }
        }
        return $template;
    }

	/** create a PDF file from form
	 * 	Inputs:
	 * 	$params->formid: the form instance id
	 *  $params->filename: the PDF file name
	 *
	 *  Return a boolean
	 *
	 */
   	public function createPDFfromForm( stdClass $params)
   	{
   		$logger = Logger::getInstance();

		//$logger->logInfo("Printer::createPDFfromForm called with params:");
		//$logger->logInfo($params);

		$filename = isset($params->filename) ? $params->filename : '';
		$formid = isset($params->formid) ? $params->formid : '';
		// @TODO Ellenőrizni a formid változót
		// @TODO Ellenőrizni a filename változót

		if ($formid != '' )
		{
		    $formtemplatename = 'print/templates/generalform.tpl';
            $form = new Template($formtemplatename);
            // Megkreáljuk a rekord template-et
            $record = $this->createTemplatefromForm($formid);
            $recordTemplate[] = $record;
            // Behelyettesitjuk a rekordot a formban
            $recordContent = Template::merge($recordTemplate);
            $form->set("record", $recordContent);

            //Megkreáljuk a header template-et
            $header = $this->createHeaderforForm($record);
            $headerTemplate[] = $header;
            // Behelyettesitjuk a rekordot a formban
            $headerContent = Template::merge($headerTemplate);
            $form->set("header", $headerContent);

		    // Megkreáljuk a nyomtatható HTML-t a template-ből
		    // Meghívjuk a renderelést és a true-val kiszedjük a maradék változóneveket a template-ből
            $html = $form->output(true);

            // Megkreáljuk PDF dokumentumot a HTML-ből
            $footertitle = '';
            if ($record->isFieldExists("title"))
            {
                $headertitle = $record->get("title");
                // @TODO: ellenőrizni, hogy van-e értéke
                $footertitle = html_entity_decode($headertitle, ENT_QUOTES, "UTF-8");
            }
			$output = $this->createPDFfromHTML($html, $footertitle);

            //Ha volt fájlnév megadva, akkor kiírjuk bele a PDF tartalmát
			if (isset($params->filename) && $params->filename != '') {

	    		file_put_contents($params->filename, $output);
                // Ha valami nem jó a pdf-el lementjük a generált html-t, abban könnyebb megtalálni a hibát
				//if (FrameworkConfig::debug_mode == true)
				//{
				//	file_put_contents($params->filename.".html", $html);
				//}
			}
		}
 		return $output;
	}

    private function setFormTitle($form, $title)
    {
        if (isset($title) && $title != null && $title != '') {

            if (!$form->isFieldExists("title"))
            {// Ha nem létezik még, beállítjuk
                $form->setField("title", $title, "V");
            }
            else
            {// Ha létezik, de nincs értéke, beállítjuk
                $formtitle = $form->get("title");
                if (!isset($formtitle) || $formtitle == null || $formtitle == '') {
                    $form->setField("title", $title, "V");
                }
            }

            if (!$form->isFieldExists("footertitle"))
            {// Ha nem létezik még, beállítjuk
                $form->setField("footertitle", $title, "V");
            }
            else
            {// Ha létezik, de nincs értéke, beállítjuk
                $footertitle = $form->get("footertitle");
                if (!isset($footertitle) || $footertitle == null || $footertitle == '') {
                    $form->setField("footertitle", $title, "V");
                }
            }
        }
    }



	/** create a PDF file from patient data
	 * 	Inputs:
	 * 	$params->patientid: the patient id
	 *  $params->filename: the PDF file name
	 *
	 *  Return a boolean
	 *
	 */
   	public function createPDFfromPatient( stdClass $params)
   	{
 		$logger = Logger::getInstance();
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';

		//$logger->logInfo("Printer::createPDFfromPatient called with params:");
		//$logger->logInfo($params);

		$patientform = new Template("print/templates/patientdata.tpl");

		$filename = isset($params->filename) ? $params->filename : '';
		$patientid = isset($params->patientid) ? $params->patientid : '';
		// @TODO Ellenőrizni a patientid változót
		$siteid = substr($patientid, 0, 4);
		// Beállítjuk a form nyomtatásának idejét
        date_default_timezone_set("Europe/Budapest");
        $formtime = date("Y-m-d H:i:s", time());

        $visits = array();
        $forms = array();
		if ($patientid != '' )
		{
			try
			{

				$db = new Database();
                // Leválogatjuk a pácienshez tartozó vizit formokat
                $sql = "SELECT * FROM " . Database::getFormsViewName() . " WHERE patientid='" . $patientid . "' AND formtype LIKE 'Visit%' AND istmp='0' ORDER BY formtype ASC";
                $db->query($sql);
                if ($db->row_count() > 0)
                {
                    for ($i = 1; $i <= $db->row_count(); $i++)
                    {
                        $row = $db->fetch("assoc");
                        if ($row != null) {
                            //$forms[$row['instanceid']] = (object)array('instanceid'=> $row['instanceid']);
                            $visits[$row['instanceid']] = $row['formtype'];
                        }
                    }
                }
                // Leválogatjuk a pácienshez tartozó egyéb formokat
 				$sql = "SELECT * FROM " . Database::getFormsViewName() . " WHERE patientid='" . $patientid . "' AND formtype NOT LIKE 'Visit%' AND istmp='0' ORDER BY formtype DESC";
                $db->query($sql);
                if ($db->row_count() > 0)
                {
                    for ($i = 1; $i <= $db->row_count(); $i++)
                    {
                        $row = $db->fetch("assoc");
                        if ($row != null) {
                            //$forms[$row['instanceid']] = (object)array('instanceid'=> $row['instanceid']);
                            $forms[$row['instanceid']] = $row['formtype'];
                        }
                    }
                }

				$db->close(); // Closes the cursor to free up memory
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError("Userid: $userId , Operation: Printer::createPDFfromPatientData, Exception: $e");
			}
		}

        $patientform->setField("patientid", $patientid, "V");
        $patientform->setField("siteid", $siteid, "V");
        $patientform->setField("formtime", $formtime, "V");
        $patientform->setField("studyname", FrameworkConfig::STUDY_NAME, "V");

        $recordTemplates = array();

        // Kinyomtatjuk a viziteket
        // ideiglenesen kiszedve, mert nagyon lassú
        foreach ($visits as $key => $value) {
            // Megkreáljuk a template-et
            $recordTemplates[$key] = $this->createTemplatefromForm($key);
        }

        //$logger->logInfo("Printer::createPDFfromPatient called with forms:");
        //$logger->logInfo($forms);

        if ($forms != null && !empty($forms))
        {
            // Kinyomtatjuk a többi formot
            foreach ($forms as $key => $value) {
                $recordTemplates[$key] = $this->createTemplatefromForm($key);
            }
        }
        //$logger->logInfo("Printer::createPDFfromPatient called with recordTemplates:");
        //$logger->logInfo($recordTemplates);
        // A rekord listához adjuk a rekordokat
        $recordContents = Template::merge($recordTemplates);
        $patientform->set("recordlist", $recordContents);

        $html = $patientform->output(true);

		// create new PDF document
		$output = $this->createPDFfromHTML($html, _("Páciens adatok"));
		if ($filename != '') {

    		file_put_contents($filename, $output);
		}
		return $output;
	}

	private function createPDFfromHTML($html, $footertitle)
	{
		// create new PDF document
		$pdf = new DOMPDF();
		$pdf->load_html($html);
		$pdf->set_paper( 'A4' , 'portrait' );
		$pdf->render();


		$font = Font_Metrics::get_font("verdana");
		$size = 9;
		$color = array(0,0,0);

		$text_height = Font_Metrics::get_font_height($font, $size);
		$canvas = $pdf->get_canvas();
		$foot = $canvas->open_object();
		$w = $canvas->get_width();
		$h = $canvas->get_height();
		$y = $h - $text_height - 20;
		$canvas->close_object();
		// ------------- FOOTER START -------------------
		//Page number
		$text = _("Page {PAGE_NUM} of {PAGE_COUNT}");
		// Center the text
		$width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
		$canvas->page_text($w / 2 - $width / 2, $y+10, $text, $font, $size, $color);
   		$canvas->page_text( 20, $y+10, $footertitle, $font, $size, $color);
		$text = FrameworkConfig::STUDY_NAME;
		$width = Font_Metrics::get_text_width($text, $font, $size);
		$canvas->page_text( $w - 20 - $width, $y+10, $text, $font, $size, $color);
		// ------------- FOOTER END --------------------
		return $pdf->output();
	}
 }
?>
