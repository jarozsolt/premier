<?php
/**
 *---------------------------------------------------------------
 * Database.php
 *
 * This file contains the Database class
 *
 * @package Kron
 * @subpackage database
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 *---------------------------------------------------------------
 */


/**
 * Database class
 *
 * general database abstraction layer class, built on PDO
 *
 * @package Kron
 * @subpackage database
 * @todo Revise comments
 */
class Database
{
	//database handle
	private		$dbh;

	public		$result;
 
	/**
	 * Stores the number of affected rows on a sql command
	 * @var unknown_type
	 */
	public		$affected_rows = 0;
	private		$stmt;
 
	public function __construct()
	{
		try
		{
 			$this->dbh = new PDO(DatabaseConfig::db_dbms . ':host=' . DatabaseConfig::db_host . ';dbname=' . DatabaseConfig::db_name, DatabaseConfig::db_username, DatabaseConfig::db_password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
 
			if (FrameworkConfig::debug_mode == true)
			{
				$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			else
			{
				$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
			}
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
	}
 
	/**
	 * Sends a command to sql
	 * 
	 * @param unknown_type $sql
	 */
	public function command($sql)
	{
		if (!$this->dbh->getAttribute(PDO::ATTR_CONNECTION_STATUS))
		{
			$this->connect();
		}
 
		if ($sql == "{prev}")
		{
			$numargs = func_num_args();
 
			for ($i = 1; $i < $numargs; $i++)
			{
				$this->{"arg$i"} = func_get_arg($i);
			}	
 
			$this->stmt->execute();
 
			if (intval($this->stmt->errorCode()) > 0)
			{
				if (FrameworkConfig::debug_mode == true)
				{
					$err = $this->stmt->errorInfo();
					throw new Exception($err[2]);
				}
				else
				{
					throw new Exception('There was an error in your sql statement');
				}
			}
		}
		else
		{
			$count = count(explode('?', $sql));
			$numargs = func_num_args();
 
			if ($count != $numargs)
			{
				throw new Exception('Number of arguments does not match the number of required parameters');
			}
 
			try
			{
				$this->stmt = $this->dbh->prepare($sql);
			}
			catch (PDOException $e)
			{
				echo $e->getMessage();
				die();
			}
 
			for ($i = 1; $i < $numargs; $i++)
			{
				$this->stmt->bindParam($i, $this->{"arg$i"});
			}
 
			for ($i = 1; $i < $numargs; $i++)
			{
				$this->{"arg$i"} = func_get_arg($i);
			}
 
			$this->affected_rows = $this->stmt->execute();
 
			if (intval($this->stmt->errorCode()) > 0)
			{
				if (FrameworkConfig::debug_mode == true)
				{
					$err = $this->stmt->errorInfo();
					throw new Exception($err[2]);
				}
				else
				{
					throw new Exception('There was an error in your sql statement');
				}
			}
		}
	}
 
	/**
	 * Sends a sql query to the database
	 * 
	 * @param string $sql
	 */
	public function query($sql)
	{
		$count = count(explode('?', $sql));
		$numargs = func_num_args();
		if ($count != $numargs)
		{
			throw new Exception('Number of arguments does not match the number of required parameters');
		}
		try
		{
			$this->stmt = $this->dbh->prepare($sql);
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
 
		for ($i = 1; $i < $numargs; $i++)
		{
			$this->stmt->bindParam($i, ${"arg$i"});
		}
 
		for ($i = 1; $i < $numargs; $i++)
		{
			${"arg$i"} = func_get_arg($i);
		}
 
		$this->stmt->execute();
 
		if (intval($this->stmt->errorCode()) > 0)
		{
			if (FrameworkConfig::debug_mode == true)
			{
				$err = $this->stmt->errorInfo();
				throw new Exception($err[2]);
			}
			else
			{
				throw new Exception('There was an error in your sql statement');
			}
		}
	}
 
	/**
	 * Returns a column as an array
	 * 
	 * @param unknown_type $column_num
	 */
	public function get_column($column_num)
	{
		return $this->stmt->fetchColumn($column_num);
	}
 
	/**
	 * Gets the most recent inserted ID
	 * 
	 */
	public function get_lastinsertid()
	{
		return $this->dbh->lastInsertId();
	}
 
	/**
	 * Gets the next row in a returned result
	 * 
	 * @param $type
	 * @param $direction
	 */
	public function fetch($type = "both", $direction = "forward")
	{
		if ($type=="both")
		{
			return $this->stmt->fetch(PDO::FETCH_BOTH);
		}
		elseif ($type=="assoc")
		{
			return $this->stmt->fetch(PDO::FETCH_ASSOC);
		}
		elseif ($type=="num")
		{
			return $this->stmt->fetch(PDO::FETCH_NUM);
		}
	}
 
	/**
	 * Gets the number of the rows in a returned result
	 * 
	 * @return int
	 */
	public function row_count()
	{
		return $this->stmt->rowCount();	
	}
 
	/**
	 * Gets the number of columns in a returned result
	 * 
	 * @return int
	 */
	public function column_count()
	{
		return $this->stmt->columnCount();	
	}
 
	/**
	 * Begins the transaction
	 */
	public function begin_transaction()
	{
        $this->dbh->beginTransaction();
	}
 
 	/**
	 * Commits the transaction
	 */
	public function commit()
	{
        $this->dbh->commit();
	}

	/**
	 * Rollbacks the transaction
	 */
	public function rollback()
	{
        $this->dbh->rollBack();
	}
 
	/**
	 * Closes the current cursor
	 */
	public function close()
	{
		$this->stmt->closeCursor();
	}
	
	/**
	 * Converts the php datetime to database datetime
	 * 
	 * @param   string $date (optional)
	 * @return  string
	 */
	public function phpdatetime_to_dbdatetime($date = null)
	{
		if (DatabaseConfig::db_dbms == 'mysql')
		{
			if (!$date)
			{
        		// use now() instead of time() to adhere to user setting
        		$date = time();
        	}
    		if (is_numeric($date) && strlen($date) == 10)
    		{
				return date("Y-m-d H:i:s", $date);
        	}
        	else
        	{// try to use now()
				return date("Y-m-d H:i:s", time());
        	}
		}
		else
		{
			$dbtype = DatabaseConfig::db_dbms;
			throw new Exception("phpdatetime_to_dbdatetime function is not supperted for the '$dbtype' database type");
			return '';
		}
	}
	
	/**
	 * Converts the database datetime to PHP's Unix Epoch time
	 * 
	 * @param   string $date
	 * @return  int
	 */
	public function dbdatetime_to_phpdatetime($date)
	{
		if (DatabaseConfig::db_dbms == 'mysql')
		{
			if (!$date)
			{
				return false;
	        }
	        else
	        {
	        	return strtotime($date);
	        }
		}
		else
		{
			$dbtype = DatabaseConfig::db_dbms;
			throw new Exception("dbdatetime_to_phpdatetime function is not supperted for the '$dbtype' database type");
			return false;
		}
	}
 
	/**
	 * Destructor
	 */
	public function __destruct()
	{
		// Closes the database
		if (isset($this->stmt))
		{	
			$this->close();
		}
	}
	
	/**
     *  This function returns the name of the messages view
     */
	static public function getFormsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'forms';
	}
	
	/**
     *  This function returns the name of the messages table
     */
	static public function getFormsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'forms_ts';
	}
	
	/**
     *  This function returns the name of the queries view
     */
	static public function getPatientsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'patients';
	}
	
	/**
     *  This function returns the name of the queries table
     */
	public function getPatientsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'patients_ts';
	}	

	/**
     *  This function returns the name of the users view
     */
	static public function getUsersViewName()
	{
		return DatabaseConfig::db_tableprefix . 'users';
	}
	
	/**
     *  This function returns the name of the visits view
     */
	public function getVisitsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'visits';
	}
	 
	 /**
     *  This function returns the name of the visits db
     */
	public function getVisitsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'visits_ts';
	}
	
	 /**
     *  This function returns the name of the sites view
     */
	static public function getSitesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'sites';
	}
	 /**
     *  This function returns the name of the messages view
     */
	static public function getFormFieldsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields';
	}
	
	/**
     *  This function returns the name of the messages table
     */
	static public function getFormFieldsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields_ts';
	}
	
	/**
     *  This function returns the name of the messages table
     */
	static public function getFormFieldsTableHistoryName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields_ht';
	}
	
	/**
     *  This function returns the name of the queries table
     */
	public function getQueriesTableName()
	{
		return DatabaseConfig::db_tableprefix . 'queries_ts';
	}
	
	/**
     *  This function returns the name of the queries view
     */
	public function getQueriesViewName()
	{
		return DatabaseConfig::db_tableprefix . 'queries';
	}
	
	
}
?>