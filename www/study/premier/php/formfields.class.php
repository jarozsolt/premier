<?php
/**
 *---------------------------------------------------------------
 *formfields.class.php
 *
 * This file contains the FormFields class, implementing the general form CRUD functions
 *
 * @package Kron
 * @subpackage formfields
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 * 
 * @todo: filter input!!!!
 *  
 *---------------------------------------------------------------
 */
require_once('database.class.php');
require_once('logger.class.php');

class FormFields
{
  	/**
     *  This function returns the name of the messages view
     */
	public function getFormFieldsViewName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields';
	}

	/**
     *  This function returns the name of the messages table
     */
	public function getFormFieldsTableName()
	{
		return DatabaseConfig::db_tableprefix . 'formfields_ts';
	}
	
	/** query all form fields with input parameters
	 *  Inputs:
	 * 	$params->patientid : Patient Id 
	 *  $params->formtype : Visit Type
	 *  $params->* : all the form fields
	 *  Return array:
	 *  totalCount: number of forms 
	 *  medications : form fields 
	 * 
	 *  @remotable
	 */
   	public  function getFormFields(stdClass $params)
   	{
    	$logger = Logger::getInstance();
		// access control ???
	//	$logger->logInfo("getMedications params");
	//	$logger->logInfo($params);
		$success = false;
		$response = array();
		$a=array();
		if ($params->patientid <> '' and $params->formtype <> '' )
		{
			try
			{ // get author user name
				$db = new Database();
				$sql = "SELECT fieldname, fieldvalue, formnr  FROM " . $this->getFormFieldsViewName() . " WHERE patientid ='" . $params->patientid . "' AND formtype ='" . $params->formtype . "'"  ;
				$db->query($sql);
				if( $db->row_count() > 0) {
					for ($i=1; $i<=$db->row_count(); $i++) {
						$row = $db->fetch("assoc");
						if ($row != null) {
							$response[$row['formnr']][$row['fieldname']]=$row['fieldvalue'];
						}	
					}
					// put formnr as id
					foreach ($response as $key => $value) {
						$response[$key]['id']=$key;
						$response[$key]['formtype']=$params->formtype;
					}
					foreach ($response as $key => $value) {
						array_push($a,$value);
					}
				}
				$db->close(); 
				$success = true;
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError($e);
			}	
		} //end if 	

//	$logger->logInfo("getFormFields called, response");
//	$logger->logInfo(array("totalCount"=> count($a),	"data" => $a));
	return  array("totalCount"=> count($a),	"data" => $a);
   	}

	/** identifies max formnr of the given patientid & formtype then creates new form the given fields 
	 * 	Inputs:
	 * 	$params->patientid : Patient Id 
	 *  $params->formtype : Visit Type
	 *  $params->* : all the form fields
	 * 	
	 *  Return array:
	 *  success: boolean 
	 *  data :  input values including new id
	 *
	 *  @remotable
	 */
   	public  function createFormFields(stdClass $fields)
   	{
    	$logger = Logger::getInstance();
		//$logger->logInfo(" createFormFields   fields:");	
		//$logger->logInfo($fields);
		// todo access control if necessary???
		
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		$success = false;
		$newFormNr =  1;
		if (isset($fields)){
		
			if ($fields->patientid <> '' and $fields->formtype<> '' )
			{
				try
				{ 
					$db = new Database();
					// identifies if formnr exists
					
					$nrcount = 0;
					$sql = "SELECT COUNT(formnr) as nrcount  FROM " . $this->getFormFieldsViewName() . " WHERE patientid ='" . $fields->patientid  . "' AND formtype ='" . $fields->formtype . "'"  ;
					$db->query($sql);
					if( $db->row_count() > 0) {
						$row = $db->fetch("assoc");
						if ($row != null) {
							$nrcount = $row['nrcount'] ;
						}	
					}
					if ($nrcount > 0) {
						// identifies max formnr of the given patientid & formtype
						$sql = "SELECT MAX(formnr) as newnr  FROM " . $this->getFormFieldsViewName() . " WHERE patientid ='" . $fields->patientid  . "' AND formtype ='" . $fields->formtype . "'"  ;
						$db->query($sql);
						if( $db->row_count() > 0) {
							$row = $db->fetch("assoc");
							if ($row != null) {
								$newFormNr = $row['newnr'] + 1;
							}
						}
					}
					//$logger->logInfo(" newFormNr");	
					//$logger->logInfo($newFormNr);	
					
					// inserts form fields into the database
					$started= $db->phpdatetime_to_dbdatetime();
					$sql = "INSERT INTO " . $this->getFormFieldsTableName() . " (patientid, formtype, formnr, fieldname, fieldvalue, start_ts, started_by) VALUES (?,?,?,?,?,?,?)";
					foreach ($fields as $key => $value) {
						if ($value<>'' and $value <> NULL) {
							$db->command($sql, $fields->patientid, $fields->formtype, $newFormNr , $key, $value, $started , $userId);
						}	
					}
					$fields->id= $newFormNr;
												  
					$db->close(); 
					$success = true;
				}
				catch (Exception $e)
				{// Log the error
					$logger->logError($e);
				}	
			} 
		
			
		}//end if 	
		//$logger->logInfo(" createFormFields fields:");	
		//$logger->logInfo($fields);	
		return  Array('success' => $success, 'data' => $fields);
   	}
	
	
	
	/** update (and add if it is new) form fields
	 * 	Inputs:
	 * 	$params->patientid : Patient Id 
	 *  $params->formtype : Visit Type
	 * 	$params->id : form Number
	 *  $params->* : all the form fields
	 * 	
	 *  Return array:
	 *  success: boolean
	 *  data :  input values including 
	 * 
	 *   @remotable
	 */
   	public  function updateFormFields(stdClass $fields)
   	{
    	$logger = Logger::getInstance();

		// todo access control if necessary???
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		
		$success = false;
		if ($fields->patientid <> '' and $fields->formtype <> '' and $fields->id>=0 )
		{
			try
			{ 
				$db = new Database();
				// query original field values
				$originalData =  array();
				$sql = "SELECT fieldname, fieldvalue FROM " . $this->getFormFieldsViewName() . " WHERE patientid = '" . $fields->patientid . "' AND formtype ='" . $fields->formtype . "' AND formnr = '" . $fields->id . "'";
				$db->query($sql);
				if( $db->row_count() > 0) {
					for ($i=1; $i<=$db->row_count(); $i++) {
						$row = $db->fetch("assoc");
						if ($row != null) { 
							$originalData[$row['fieldname']]=$row['fieldvalue'];
						}	
					}
					$originalData['id']=$fields->id;
					$originalData['formtype']=$fields->formtype;
				}
			//	$logger->logError('originalData');
			//	$logger->logError($originalData);
				// iterate on new fields, update if value is different from original value
				foreach ($fields as $key => $value) {
					$orgiginalValue='';
					if (array_key_exists($key, $originalData) ) {
						$orgiginalValue=$originalData[$key];
					}
					
					if ($value<>'' and $value <> NULL and $value <> $orgiginalValue ) {
						$update_ts = $db->phpdatetime_to_dbdatetime();	
						if (array_key_exists($key, $originalData) ){// set field deleted if exist 
							$sql = "UPDATE " . $this->getFormFieldsTableName() . " SET end_ts = ? , ended_by = ? WHERE  patientid = ? and formtype = ? and formnr = ? and fieldname = ? ";
							$db->command($sql, $update_ts, $userId,  $fields->patientid ,  $fields->formtype, $fields->id,  $key );
						}
						// insert form field into the database
						$sql = "INSERT INTO " . $this->getFormFieldsTableName() . " (patientid, formtype, formnr, fieldname, fieldvalue, start_ts, started_by) VALUES (?,?,?,?,?,?,?)";
						$db->command($sql, $fields->patientid, $fields->formtype, $fields->id , $key, $value, $update_ts , $userId);
					}	
				}
											  
				$db->close(); 
				$success = true;
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError($e);
			}	
		} //end if 	
	
		return  Array('success' => $success, 'data' => $fields);
   	}
	
	
	/** delete form fields
	 * 	Inputs:
	 * 	$params->patientid : Patient Id 
	 *  $params->formtype : Visit Type
	 * 	$params->id :form number
	 *  $params->* : all the form fields
	 * 	
	 *  Return array:
	 *  success: boolean
	 *  data :  input values including 
	 *
	 *  @remotable
	 */
   	public  function deleteFormFields( stdClass $fields)
   	{
    	$logger = Logger::getInstance();
	//	$logger->logInfo(" deleteFormFields  params and fields:");	
	//	$logger->logInfo($fields);	

		// todo access control if necessary???
		$userId=isset($_SESSION['auth_userid']) ? $_SESSION['auth_userid'] : '';
		
		$success = false;
		if ($fields->patientid <> '' and $fields->formtype <> '' and $fields->id>=0 )
		{
			try
			{ 
				$db = new Database();
				// set field deleted 
				$ended = $db->phpdatetime_to_dbdatetime();
				$sql = "UPDATE " . $this->getFormFieldsTableName() . " SET end_ts = ? , ended_by = ? WHERE  patientid = ? and formtype = ? and formnr = ? ";
				$db->command($sql, $ended, $userId,  $fields->patientid ,  $fields->formtype, $fields->id );
				$db->close(); 
				$success = true;
			}
			catch (Exception $e)
			{// Log the error
				$logger->logError($e);
			}	
		} //end if 	
	
		return  Array('success' => $success, 'data' => $fields);
   	}	
	
 }
?>
