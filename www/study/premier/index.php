﻿<?php
// Initialize session
if (!isset($_SESSION)) 
{
ini_set('session.use_cookies', 1);
ini_set('session.use_only_cookies', 1);
session_start();
} 
/**
 *---------------------------------------------------------------
 * index.php
 *
 * This is the entry point to the application.
 *
 * @package Kron
 * @version 1.2.0
 * @copyright Copyright (c) 2013, Datanova Kft.
 *
 *---------------------------------------------------------------
 */
// Calculate the study path
$study_path = str_replace("\\", "/", realpath(dirname(__FILE__)));

// Calculate the study directory
$study_dir = basename ($study_path ,"/");

// Calculate the subdomain path
$subdomain_path = join(array_slice(explode( "/" ,$study_path), 0, -1), "/");

// Calculate the public (www) path
$public_path = join(array_slice(explode( "/" ,$subdomain_path), 0, -1), "/");

// Calculate the base path
$base_path = join(array_slice(explode( "/" ,$public_path), 0, -1), "/");

// Load the main configuration file
// Warning the 'configs' config_folder hardcoded here !!!
require_once($base_path . '/' . $study_dir . '/configs/app.conf');
require_once('localemanagement.class.php');

$language = FrameWorkConfig::default_language;


/*$sid = session_id();
if(empty($sid)) {
	//There is no active session
	ini_set('session.use_cookies', 1);
	ini_set('session.use_only_cookies', 1);
	session_start();
}*/

if (isset ($_SESSION['auth_userlanguage']) && $_SESSION['auth_userlanguage'] != '')
{
	$language = $_SESSION['auth_userlanguage'];
	LocaleManagement::initLanguage($language);
}
$language_short = substr($language, 0, 2);

// We need to build the logout page?
if (isset ($_SESSION['auth_userloggedoutsuccessful']) && $_SESSION['auth_userloggedoutsuccessful'])
{// logout page
	require_once('logout.php');
	exit();
}
//localhost/kron/www/study/keplat/index.php?cmd=showform&pid=HU01101&sid=1&fid=1222
$get_parameters = array();
if (isset($_SERVER['QUERY_STRING'])) {
	$pairs = explode('&', $_SERVER['QUERY_STRING']);
	if (isset($pairs)) {
		foreach($pairs as $pair) {
			$part = explode('=', $pair);
			if (isset($part) && isset($part[0]) && isset($part[1])) {
				$get_parameters[$part[0]] = urldecode($part[1]);
			}
		}
	}
}
if (isset($get_parameters['cmd']))
{
	$cmd = $get_parameters['cmd'];
	if ($cmd == 'showform') {
		if (isset($get_parameters['pid']) && isset($get_parameters['sid']) && isset($get_parameters['fid'])) {
			$_SESSION['showform_fid'] = $get_parameters['fid'];
			$_SESSION['showform_pid'] = $get_parameters['pid'];
			$_SESSION['showform_sid'] = $get_parameters['sid'];
		}
		if (isset($get_parameters['ftype'])) {
			$_SESSION['showform_ftype'] = $get_parameters['ftype'];
		}
		else {
			$_SESSION['showform_ftype'] = 'AdverseEvent';
		}
	}
}

if (!isset ($_SESSION['auth_userloggedin']) || !$_SESSION['auth_userloggedin'] ||
	!isset ($_SESSION['auth_userclass']) || !$_SESSION['auth_userclass'])
{ // The user not logged in or the user class is not admin, we build the login page
	require_once('login.php');
	exit();
}

// Build the application page
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">';
echo '<html>';
echo '<head>';
echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
//echo '<meta http-equiv="X-UA-Compatible" content="IE=8" />';
echo '<title id="page-title">'._("KRON - Klinikai Adatkezelő Rendszer").'</title>';
//echo '<link rel="stylesheet" type="text/css" href="'.EXTJS_PATH_REL.'resources/css/ext-all.css"/>';
echo '<link rel="stylesheet" type="text/css" href="'.CSS_PATH_REL.'my-ext-theme.css"/>';
echo '<link rel="stylesheet" type="text/css" href="'.CSS_PATH_REL.'app_2.css"/>';
echo '<link rel="stylesheet" type="text/css" href="'.CSS_PATH_REL.'statusbar.css"/>';
echo '<link rel="shortcut icon" type="image/x-icon" href="'.IMAGE_PATH_REL.'favicon.ico"/>';
echo '<link rel="gettext" type="application/x-po" href="'.LOCALES_PATH_REL.$language.'.utf8'.'/LC_MESSAGES/server.po"/>';
echo '<link rel="gettext" type="application/x-po" href="'.LOCALES_PATH_REL.$language.'.utf8'.'/LC_MESSAGES/kron.po"/>';
echo '</head>';
echo '<body style="scroll:no">';

if (FrameworkConfig::debug_mode)
{
	// Set the Debug global javascript variable to true
	// For IE we should define an empty console.log function, to avoid errors
	echo '<script type="text/javascript">var Debug=true;';
	if (FrameworkConfig::demo_mode)
	{
		echo 'var Demo=true;';
	}
	else
	{
		echo 'var Demo=false;';
	}
	echo 'if (typeof window.console==="undefined"||typeof window.console.log==="undefined"){(function(){';
	echo 'window.console={};window.console.log=function(){};}());}</script>';
	// For the debug version use the ext-all-debug-w-comments.js
	echo '<script type="text/javascript" src="'.EXTJS_PATH_REL.'ext-all-debug-w-comments.js"></script>';
}
else
{
	// Set the Debug global javascript variable to false
	// For every browser we should define an empty console.log function, to hide the logs
	echo '<script type="text/javascript">var Debug=false;';
	if (FrameworkConfig::demo_mode)
	{
		echo 'var Demo=true;';
	}
	else
	{
		echo 'var Demo=false;';
	}
	//	echo 'if (typeof window.console==="undefined"||typeof window.console.log==="undefined"){(function(){';
	//	echo 'window.console={};window.console.log=function(){};}());}else{window.console.log=function(){};}
	echo '</script>';
	// For the production version use the ext-all.js
	echo '<script type="text/javascript" src="'.JAVASCRIPT_PATH_REL.'build/ext_app_1.js"></script>';
}
echo '<script type="text/javascript" src="'.EXTJS_PATH_REL.'locale/ext-lang-'.$language_short.'.js"></script>';
// Configure the Ext.Loader
echo '<script type="text/javascript">';
echo 'Ext.namespace("Kron").imagePath="'.IMAGE_PATH_REL.'";';
echo 'Ext.namespace("Kron").phpPath="'.PHPAPP_PATH_REL.'";';
echo '</script>';

// Register namespaces and their corresponding paths to Ext.Loader

if (FrameworkConfig::debug_mode)
{
	// for the debug version use the plain ext and the app
	echo '<script type="text/javascript">';
	echo 'Ext.Loader.setConfig({enabled:true,paths:{';
	echo '"Ext":"'.EXTJS_PATH_REL.'src",';

	//echo '"Core":"'. JSCORE_PATH_REL .'",';
	echo '"Core":"'. $jscore_path_relative .'",';
	echo '"Ext.ux":"'. JSCORE_PATH_REL .'ux",';
	echo '"Kron":"'. $jsapp_path_relative . '"}});';
	echo '</script>';
	echo '<script type="text/javascript" src="'.JSCORE_PATH_REL.'locale/Gettext.js"></script>';
	echo '<script type="text/javascript" src="'.JSAPP_PATH_REL.'config/configs.js"></script>';
	// Load the ExtDirect api
	echo '<script type="text/javascript" src="'.PHPAPP_PATH_REL.'kron.extdir.php"></script>';
	echo '<script type="text/javascript" src="'.JSAPP_PATH_REL.'app.js"></script>';	
} else {
	// for production compressed files 

	echo '<script type="text/javascript" src="'.JAVASCRIPT_PATH_REL.'build/gettext_1.js"></script>';
	echo '<script type="text/javascript" src="'.JAVASCRIPT_PATH_REL.'build/configs_1.js"></script>';
	// Load the ExtDirect api
	echo '<script type="text/javascript" src="'.PHPAPP_PATH_REL.'kron.extdir.php"></script>';
	echo '<script type="text/javascript" src="'.JAVASCRIPT_PATH_REL.'build/app_8.js"></script>';
}
echo '<noscript><p>It appears that your web browser does not support JavaScript,
      or you have temporarily disabled scripting. Either way, this site
      won\'t work without it. Please take a look at our <a href="/support/browsers/">browser support page</a> for more help.</p></noscript>';
echo '</body>';
echo '</html>';
?>
